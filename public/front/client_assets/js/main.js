$(window).on("load", function() {
    $(".loading").delay(600).fadeOut()
    $("body").css("overflow-y", "scroll");
});
$(document).ready(function() {

    $('#register-modal').on('shown.bs.modal', function() {
        // Load up a new modal...
        $('#login-modal').modal('hide');
        $('#activate-modal').modal('hide');
        $('#forget-modal').modal('hide');
        $('#address-modal').modal('hide');
        $('#forget-update-modal').modal('hide');
    });

    $('#login-modal').on('shown.bs.modal', function() {
        // Load up a new modal...
        $('#register-modal').modal('hide');
        $('#activate-modal').modal('hide');
        $('#forget-modal').modal('hide');
        $('#address-modal').modal('hide');
        $('#forget-update-modal').modal('hide');
    });

    $('#activate-modal').on('shown.bs.modal', function() {
        // Load up a new modal...
        $('#register-modal').modal('hide');
        $('#login-modal').modal('hide');
        $('#forget-modal').modal('hide');
        $('#address-modal').modal('hide');
        $('#forget-update-modal').modal('hide');
    });

    $('#forget-modal').on('shown.bs.modal', function() {
        // Load up a new modal...
        $('#register-modal').modal('hide');
        $('#login-modal').modal('hide');
        $('#activate-modal').modal('hide');
        $('#address-modal').modal('hide');
        $('#forget-update-modal').modal('hide');
    });

    $('#forget-update-modal').on('shown.bs.modal', function() {
        // Load up a new modal...
        $('#register-modal').modal('hide');
        $('#login-modal').modal('hide');
        $('#activate-modal').modal('hide');
        $('#address-modal').modal('hide');
        $('#forget-modal').modal('hide');
    });

    $('#address-modal').on('shown.bs.modal', function() {
        // Load up a new modal...
        $('#register-modal').modal('hide');
        $('#login-modal').modal('hide');
        $('#activate-modal').modal('hide');
        $('#forget-modal').modal('hide');
        $('#forget-update-modal').modal('hide');
    });

    $('#rate-modal').on('shown.bs.modal', function() {
        // Load up a new modal...
        $('#order-modal').modal('hide');
        $('#rate2-modal').modal('hide');
    });

    $('#order-modal').on('shown.bs.modal', function() {
        // Load up a new modal...
        $('#rate-modal').modal('hide');
        $('#rate2-modal').modal('hide');
    });

    $('#rate2-modal').on('shown.bs.modal', function() {
        // Load up a new modal...
        $('#rate-modal').modal('hide');
        $('#order-modal').modal('hide');
    });

    $(".flex-inputs .form-control").keyup(function() {
        if (this.value.length == this.maxLength) {
            $(this).next('.flex-inputs .form-control').focus();
        }
    });

    $(document).on('shown.bs.modal', '.modal', function() {
        setTimeout(function() {
            $(this).find('input:visible:first').focus();
        }, 2000);
    });

    // like section
    // $(".fa-heart").click(function() {
    //     $(this).toggleClass("far");
    //     $(this).toggleClass("fas")
    // });

    // $("#datepicker").datepicker({
    //     uiLibrary: "bootstrap4"
    // });

    //upload file
    $('.chooseFile').bind('change', function() {
        var filename = $(this).val();
        if (/^\s*$/.test(filename)) {
            $(this).parent().parent().removeClass('active');
            $(this).parent().find(".file-select-name").text("No file chosen...");
        } else {
            $(this).parent().parent().addClass('active');
            $(this).parent().find(".file-select-name").text(filename.replace("C:\\fakepath\\", ""));
        }
    });


    var scrollButton = $("#scroll-top");

    $(window).scroll(function() {
        if ($(this).scrollTop() >= 500) {
            scrollButton.show();
        } else {
            scrollButton.hide();
        }
    });

    scrollButton.click(function() {
        $("html,body").animate({
                scrollTop: 0
            },
            600
        );
    });

    $(".links").on("click", function() {
        //  $(".ul-sec").find(".active").removeClass("active");
        $(this).addClass("active");
    });

    $("#nav-men").click(function() {
        $("#s-nav").addClass("nav-go");
        $("#sa").show();
    });

    $("#overlay , header .men-cl").click(function() {
        $("#s-nav").removeClass("nav-go");
    });

    $(".men-cl").on("click", function() {
        $("#s-nav").removeClass("nav-go");
        $(".nav-go").hide();
        $(".men-cl").hide();
    });

    $(".navbar-nav>li.nav-item .nav-link").on("click", function() {
        $(this).parent().find(".sub-menu").toggleClass("go-open");
    });

    $(".navbar-nav>li.nav-item>.nav-link").click(function(e) {
        e.preventDefault();
    });

    //file upload
    // upload and preview image
    $(function() {
        function maskImgs() {
            //$('.img-wrapper img').imagesLoaded({}, function() {
            $.each($(".img-wrapper img"), function(index, img) {
                var src = $(img).attr("src");
                var parent = $(img).parent();
                parent
                    .css("background", "url(" + src + ") no-repeat center center")
                    .css("background-size", "cover");
                $(img).remove();
            });
            //});
        }

        var preview = {
            init: function() {
                preview.setPreviewImg();
                preview.listenInput();
            },
            setPreviewImg: function(fileInput) {
                var path = $(fileInput).val();
                var uploadText = $(fileInput).siblings(".file-upload-text");

                if (!path) {
                    $(uploadText).val("");
                } else {
                    path = path.replace(/^C:\\fakepath\\/, "");
                    $(uploadText).val(path);

                    preview.showPreview(fileInput, path, uploadText);
                }
            },
            showPreview: function(fileInput, path, uploadText) {
                var file = $(fileInput)[0].files;

                if (file && file[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        var previewImg = $(fileInput)
                            .parents(".file-upload-wrapper")
                            .siblings(".preview");
                        var img = $(previewImg).find("img");

                        if (img.length == 0) {
                            $(previewImg).html('<img src="' + e.target.result + '" alt=""/>');
                        } else {
                            img.attr("src", e.target.result);
                        }

                        uploadText.val(path);
                        maskImgs();
                    };

                    reader.onloadstart = function() {
                        $(uploadText).val("uploading..");
                    };

                    reader.readAsDataURL(file[0]);
                }
            },
            listenInput: function() {
                $(".file-upload-native").on("change", function() {
                    preview.setPreviewImg(this);
                });
            }
        };
        preview.init();
    });
});


// window.oncontextmenu = function () {
//     return false;
// };

// document.addEventListener("keydown", function(event){
//     var key = event.key || event.keyCode;

//     if (key == 123) {
//         return false;
//     } else if ((event.ctrlKey && event.shiftKey && key == 73) || (event.ctrlKey && event.shiftKey && key == 74)) {
//         return false;
//     }
// }, false);

// mnm
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
// submit form general
$(".btnSubmitForm").on('click', function(event) {

    //stop submit the form, we will post it manually.
    event.preventDefault();

    // disable save button untill response
    var button = $(this);
    button.prop('disabled', true);

    // Get form
    var form = $(this).parents('form')[0];

    // Create an FormData object
    var data = new FormData(form);

    var url = form.action;
    var method = form.method;
    $.ajax({
        type: method,
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function(data) {
            if (data.key == 'success') {
                if (data.redirect != undefined) {
                    //form.reset();
                    window.location.replace(data.redirect);
                } else if (data.modal_id != undefined) {
                    form.reset();
                    $('#' + data.modal_id).modal('show');;
                    toastr.success(data.msg);
                } else {
                    form.reset();
                    toastr.success(data.msg);
                }
            } else if (data.key == 'fail') {
                toastr.error(data.msg);
            }
            button.prop('disabled', false);
        },
        error: function(e) {
            // console.log("ERROR : ", e.responseJSON);
            toastr.error(e.responseJSON.message);
            button.prop('disabled', false);
        }
    });
});