function readURL(input) {
    var id = input.target.id;
    console.log(id);
    var image = $('#output-img-' + id);
    var link = $('#output-link-' + id);
    if (input.target.files && input.target.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            console.log(image, e.target.result);
            image.attr('src', e.target.result);
            if (link) {
                link.attr('href', e.target.result);
            }

        }

        reader.readAsDataURL(input.target.files[0]); // convert to base64 string
    }
}
