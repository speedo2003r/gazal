<?php

namespace Database\Seeders;

use App\Entities\Banner;
use App\Entities\Branch;
use App\Entities\CarType;
use App\Entities\Category;
use App\Entities\City;
use App\Entities\Country;
use App\Entities\Delegate;
use App\Entities\Group;
use App\Entities\Item;
use App\Entities\ItemBranch;
use App\Entities\Provider;
use App\Entities\Question;
use App\Entities\Slider;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Country::factory(10)->create();
        City::factory(10)->create();
        Category::factory(5)->create();
        Category::factory(5,[
            'parent_id' => rand(1,5)
        ])->create();
        Question::factory(5)->create();
        Group::factory(5)->create();
        User::factory(10)->create();
        Provider::factory(10)->create();
        Branch::factory(30)->create();
        Item::factory(30)->create();
        ItemBranch::factory(30)->create();
        CarType::factory(5)->create();
        Delegate::factory(10)->create();
        Banner::factory(10)->create();
        Slider::factory(10)->create();
		$this->command->info('all tables seeded!');
    }
}
