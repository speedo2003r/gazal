<?php

namespace Database\Factories;

use App\Entities\Banner;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class BannerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Banner::class;
    public function definition()
    {
        return [
            'title' => ['ar' => $this->faker->word,'en'=>$this->faker->word],
            'image'=>$this->faker->imageUrl,
            'country_id'=>rand(1,9),
            'city_id'=>rand(1,9),
            'category_id'=>rand(1,5),
            'user_id'=>rand(1,10),
            'start_date'=>Carbon::now()->format('Y-m-d'),
            'end_date'=>Carbon::now()->addDay(90)->format('Y-m-d'),
            'item_id'=>rand(1,30),
            'active'=>1,
            'type'=>'item',
        ];
    }
}
