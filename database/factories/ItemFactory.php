<?php

namespace Database\Factories;

use App\Entities\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Item::class;
    public function definition()
    {
        return [
            'title' => ['ar' => $this->faker->word,$this->faker->word],
            'description' => ['ar' => $this->faker->paragraph,$this->faker->paragraph],
            'price' => rand(1111,9999),
            'status' => 1,
            'exist' => 1,
            'category_id' => rand(1,5),
            'subcategory_id' => rand(6,10),
            'user_id' => rand(2,10),
        ];
    }
}
