<?php

namespace Database\Factories;

use App\Entities\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Category::class;
    public function definition()
    {
        return [
            'title' => ['ar'=>$this->faker->word,'en'=>$this->faker->word],
            'icon' => $this->faker->imageUrl,
        ];
    }
}
