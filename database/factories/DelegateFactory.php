<?php

namespace Database\Factories;

use App\Entities\Delegate;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class DelegateFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Delegate::class;
    public function definition()
    {
        return [
            'name'       => $this->faker->name,
            'email'      => $this->faker->unique()->safeEmail,
            'share_code'      => $this->faker->unique()->text,
            'password'   => Hash::make('123456'), // password
            'phone'      => rand(1111111111, 9999999999),
            'birth_date'=>'1984-11-10',
            'address' => $this->faker->address,
            'lat' => '24.7135517',
            'lng' => '46.67529569999999',
            'id_avatar'=>$this->faker->imageUrl,
            'car_type_id' => rand(1,5),
            'licence_image'=>rand(1111111111,9999999999),
            'licence_car_image' => $this->faker->imageUrl,
            'car_front_image'=>$this->faker->imageUrl,
            'car_back_image'=>$this->faker->imageUrl,
        ];
    }
}
