<?php

namespace Database\Factories;

use App\Entities\Country;
use Illuminate\Database\Eloquent\Factories\Factory;

class CountryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Country::class;
    public function definition()
    {
        return [
            'title' => ['ar'=>$this->faker->country,'en'=>$this->faker->country],
            'code' => $this->faker->postcode,
        ];
    }
}
