<?php

namespace Database\Factories;

use App\Entities\CarType;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarTypeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = CarType::class;
    public function definition()
    {
        return [
            'title' => ['ar' => $this->faker->word,'en' => $this->faker->word],
            'image' => $this->faker->imageUrl(640,480,'cars'),
        ];
    }
}
