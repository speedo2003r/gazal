<?php

namespace Database\Factories;

use App\Entities\ItemBranch;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemBranchFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = ItemBranch::class;
    public function definition()
    {
        return [
            'branch_id' => rand(1,30),
            'item_id' => rand(1,30),
        ];
    }
}
