<?php

namespace Database\Factories;

use App\Entities\Question;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Question::class;
    public function definition()
    {
        return [
            'key' => ['ar' => $this->faker->paragraph,'en'=>$this->faker->paragraph],
            'value' => ['ar' => $this->faker->paragraph,'en'=>$this->faker->paragraph],
        ];
    }
}
