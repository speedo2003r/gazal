<?php

namespace Database\Factories;

use App\Entities\Branch;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class BranchFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Branch::class;
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'title' => ['ar' => $this->faker->word,'en'=>$this->faker->word],
            'email' => $this->faker->unique()->safeEmail,
            'password' => Hash::make('123456'), // password
            'phone' => rand(1111111111,9999999999),
            'address' => $this->faker->address,
            'v_code' => '123456',
            'status' => 1,
            'country_id' => rand(1,9),
            'city_id' => rand(1,9),
            'provider_id' => rand(2,10),
            'lat' => '24.7135517',
            'lng' => '46.67529569999999',
        ];
    }
}
