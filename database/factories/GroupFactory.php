<?php

namespace Database\Factories;

use App\Entities\Group;
use Illuminate\Database\Eloquent\Factories\Factory;

class GroupFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Group::class;
    public function definition()
    {
        return [
            'title' => $this->faker->word,
            'category_id' => rand(1,5),
        ];
    }
}
