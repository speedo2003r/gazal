<?php

namespace Database\Factories;

use App\Entities\City;
use Illuminate\Database\Eloquent\Factories\Factory;

class CityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = City::class;
    public function definition()
    {
        return [
            'title' => ['ar' => $this->faker->city,'en' => $this->faker->city],
            'country_id' => rand(1,9),
        ];
    }
}
