<?php

namespace Database\Factories;

use App\Entities\City;
use App\Entities\Country;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory {
  protected $model = User::class;

  public function definition() {
    return [
      'name'       => $this->faker->name,
      'email'      => $this->faker->unique()->safeEmail,
      'password'   => Hash::make('123456'), // password
      'phone'      => rand(1111111111, 9999999999),
      'v_code'     => '123456',
      'birth_date' => '1984-11-10',
      'active'     => 1,
      'banned'     => 0,
      'accepted'   => 1,
      'country_id' => Country::first()->id ?? null,
      'city_id'    => City::first()->id ?? null,
      //'category_id' => rand(1,5),
      //'group_id' => rand(1,5),
      //'address' => $this->faker->address,
      //'lat' => '24.7135517',
      //'lng' => '46.67529569999999',
      'share_code' => Str::random(10),
    ];
  }

  public function unverified() {
    // return $this->state(function (array $attributes) {
    //   return [
    //     'email_verified_at' => null,
    //   ];
    // });
  }
}
