<?php

namespace Database\Factories;

use App\Entities\Provider;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class ProviderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Provider::class;
    public function definition()
    {
        return [
            'store_name'   => ['ar'=>$this->faker->name,'en'=>$this->faker->name],
            'name'=>$this->faker->name,
            'email'      => $this->faker->unique()->safeEmail,
            'password'   => Hash::make('123456'), // password
            'phone'      => rand(1111111111, 9999999999),
            'username'=>$this->faker->name,
            'logo'=>$this->faker->imageUrl,
            'banner'=>$this->faker->imageUrl,
            'id_avatar'=>$this->faker->imageUrl,
            'commercial_image'=>$this->faker->imageUrl,
            'municipal_license'=>$this->faker->imageUrl,
            'tax_certificate'=>$this->faker->imageUrl,
            'processing_time' => rand(1,90),
            'commercial_num' => rand(1111111111,9999999999),
            'acc_bank' => $this->faker->bankAccountNumber,
            'lat' => '31.555454545',
            'lng' => '31.555454545',
            'address' => $this->faker->address,
        ];
    }
}
