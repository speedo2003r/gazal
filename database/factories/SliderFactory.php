<?php

namespace Database\Factories;

use App\Entities\Slider;
use Illuminate\Database\Eloquent\Factories\Factory;

class SliderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Slider::class;
    public function definition()
    {
        return [
            'title'=>['ar'=>$this->faker->word,'en'=>$this->faker->word],
            'image'=>$this->faker->imageUrl,
            'country_id'=>rand(1,9),
            'city_id'=>rand(1,9),
            'category_id'=>rand(1,5),
            'user_id'=>rand(1,10),
            'item_id'=>rand(1,30),
            'active'=>1,
            'type'=>'item',
        ];
    }
}
