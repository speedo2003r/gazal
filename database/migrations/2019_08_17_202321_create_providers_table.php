<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateDelegatesTable.
 */
class CreateProvidersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('providers', function(Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('store_name')->nullable();
            $table->string('avatar')->default('/default.png');
            $table->string('slogan')->nullable();
            $table->string('email');
            $table->string('username')->nullable();
            $table->integer('wallet')->default(0);
            $table->tinyInteger('commission_status')->default(0);
            $table->double('income')->nullable()->default(0);
            $table->double('balance')->nullable()->default(0);
            $table->integer('commission')->nullable()->default(0);
            $table->string('phone');
            $table->unique(['phone', 'email', 'deleted_at']);
            $table->string('replace_phone')->nullable();
            $table->string('v_code')->nullable();
            $table->string('password');
            $table->string('logo')->nullable();
            $table->string('banner')->nullable();
            $table->string('commercial_num')->nullable();
            $table->tinyInteger('free_ship')->default(0);
            $table->tinyInteger('processing_time')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('address')->nullable();
            $table->foreignId('category_id')->nullable()->constrained('categories');
            $table->foreignId('group_id')->nullable()->constrained('groups');
            $table->foreignId('country_id')->nullable()->constrained('countries');
            $table->foreignId('city_id')->nullable()->constrained('cities');
            $table->string('id_avatar')->nullable()->comment('صورة الهويه الوطنيه');
            $table->string('acc_bank')->nullable()->comment('حساب بنكي');
            $table->string('commercial_image')->nullable()->comment('سجل الشركه');
            $table->string('tax_certificate')->nullable()->comment('الشهاده الضريبيه');
            $table->string('municipal_license')->nullable()->comment('رخصة البلديه');
            $table->date('end_date_contract')->nullable()->comment('تاريخ انتهاء التعاقد');
            $table->string('lang')->default('ar');
            $table->boolean('active')->default(0)->comment('mobile activation');
            $table->boolean('banned')->default(0);
            $table->boolean('accepted')->default(1)->comment('Admin approval');
            $table->boolean('notify')->default(1);
            $table->boolean('online')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('providers');
	}
}
