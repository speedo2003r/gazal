<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateReviewRatesTable.
 */
class CreateReviewRatesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    // هيتعمل تجميع للتقييم في جدول view لوحده sql
		Schema::create('review_rates', function(Blueprint $table) {
            $table->id();
            $table->morphs('model');
            $table->foreignId('order_id')->nullable()->constrained('orders')->onDelete('cascade');
            $table->foreignId('rating_list_id')->constrained('rating_lists')->onDelete('cascade');
            $table->morphs('rateable');
            $table->unsignedTinyInteger('rate');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('review_rates');
	}
}
