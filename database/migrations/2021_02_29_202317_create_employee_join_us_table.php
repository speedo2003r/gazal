<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeJoinUsTable extends Migration {
  public function up() {
    Schema::create('employee_join_us', function (Blueprint $table) {
      $table->id();
      $table->string('name',30)->nullable();
      $table->string('phone',20)->nullable();
      $table->string('email',30)->nullable();
      $table->string('job_description',500)->nullable();
      $table->foreignId('city_id')->nullable()->constrained('cities');
      $table->string('cv',50)->nullable();
      $table->text('desc')->nullable();
      $table->tinyInteger('status')->default(0);
      $table->timestamp('created_at')->useCurrent();
      $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
      $table->softDeletes();
    });
  }

  public function down() {
    Schema::dropIfExists('employee_join_us');
  }
}
