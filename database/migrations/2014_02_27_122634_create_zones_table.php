<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZonesTable extends Migration {
  public function up() {
    Schema::create('zones', function (Blueprint $table) {
      $table->id();
      $table->text('title')->nullable();
      $table->foreignId('city_id')->constrained('cities')->onDelete('cascade');
      $table->longText('geometry')->nullable();
      $table->polygon('p_geometry')->nullable();
      $table->timestamp('created_at')->useCurrent();
      $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
    });
  }

  public function down() {
    Schema::drop('zones');
  }
}
