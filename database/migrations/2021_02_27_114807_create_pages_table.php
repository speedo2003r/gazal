<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration {
  public function up() {
    Schema::create('pages', function (Blueprint $table) {
      $table->id();
      $table->text('title')->nullable();
      $table->text('desc')->nullable();
      $table->timestamp('created_at')->useCurrent();
      $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
      $table->softDeletes();
    });
  }

  public function down() {
    Schema::drop('pages');
  }
}
