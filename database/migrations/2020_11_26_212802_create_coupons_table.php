<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCouponsTable.
 */
class CreateCouponsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->enum('type',['public','private'])->nullable();
            $table->enum('kind',['percent','fixed'])->nullable();
            $table->foreignId('country_id')->nullable()->constrained('countries');
            $table->foreignId('city_id')->nullable()->constrained('cities');
            $table->integer('min_order_amount')->default(0); // الحد الادني لقيمة الطلب
            $table->string('code')->nullable();
            $table->double('value',8,2)->nullable();
            $table->integer('count')->default(0);
            $table->integer('max_percent_amount')->default(0); // الحد الأقصي للخصم
            $table->integer('user_use_count')->default(0);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('coupons');
	}
}
