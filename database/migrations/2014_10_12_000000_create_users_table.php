<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

  public function up() {
    Schema::create('users', function (Blueprint $table) {
      $table->id();
      $table->string('name');
      $table->string('avatar')->default('/default.png');
      $table->string('email');
      $table->integer('wallet')->default(0);
      $table->string('country_code', 10)->nullable();
      $table->string('phone');
      $table->string('replace_phone')->nullable();
      $table->date('birth_date')->nullable();
      $table->enum('gender', ['male', 'female'])->nullable();
      $table->unique(['phone', 'email', 'deleted_at']);
      $table->string('v_code')->nullable();
      $table->string('password');
      $table->string('lang')->default('ar');
      $table->boolean('active')->default(0)->comment('mobile activation');
      $table->boolean('banned')->default(0);
      $table->boolean('accepted')->default(1)->comment('Admin approval');
      $table->boolean('notify')->default(1);
      $table->boolean('online')->default(0);

      $table->foreignId('country_id')->nullable()->constrained();
      $table->foreignId('city_id')->nullable()->constrained();


      $table->text('address')->nullable();
      $table->string('lat')->nullable();
      $table->string('lng')->nullable();

      $table->string('share_code', 50)->unique();
      $table->string('friend_share_code', 50)->nullable();
      $table->tinyInteger('is_friend_share_code_used')->default(0);
      $table->timestamp('created_at')->useCurrent();
      $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
      $table->softDeletes();
    });

  }

  public function down() {
    Schema::dropIfExists('users');
  }
}
