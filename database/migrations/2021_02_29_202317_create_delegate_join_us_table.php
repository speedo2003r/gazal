<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDelegateJoinUsTable extends Migration {
  public function up() {
    Schema::create('delegate_join_us', function (Blueprint $table) {
      $table->id();
      $table->string('avatar',50)->nullable();
      $table->string('name',50)->nullable();
      $table->string('phone',20)->nullable();
      $table->string('phone2',20)->nullable();
      $table->string('email',30)->nullable();
      $table->foreignId('car_type_id')->nullable()->constrained('car_types');
      $table->date('birth_date')->nullable();
      $table->foreignId('city_id')->nullable()->constrained('cities');
      $table->foreignId('category_id')->nullable()->constrained('categories');
      $table->string('password',191)->nullable();
      $table->string('licence_car_image',50)->nullable();
      $table->string('licence_image',50)->nullable();
      $table->string('car_back_image',50)->nullable();
      $table->tinyInteger('status')->default(0);
      $table->timestamp('created_at')->useCurrent();
      $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
      $table->softDeletes();
    });
  }

  public function down() {
    Schema::dropIfExists('delegate_join_us');
  }
}
