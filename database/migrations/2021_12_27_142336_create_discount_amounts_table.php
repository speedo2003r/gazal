<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountAmountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_amounts', function (Blueprint $table) {
            $table->id();
            $table->double('amount',8,2)->default(0);
            $table->tinyInteger('pay_type')->nullable();
            $table->date('created_date')->nullable();
            $table->foreignId('discount_reason_id')->nullable()->constrained();
            $table->foreignId('provider_id')->nullable()->constrained('providers');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_amounts');
    }
}
