<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateDelegatesTable.
 */
class CreateCompaniesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('avatar')->default('/default.png');
            $table->string('email');
            $table->string('phone');
            $table->unique(['phone', 'email', 'deleted_at']);
            $table->string('replace_phone')->nullable();
            $table->string('v_code')->nullable();
            $table->string('password');
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('address')->nullable();
            $table->string('tax_num')->nullable();
            $table->string('commercial_num')->nullable();
            $table->string('contract_num')->nullable();
            $table->foreignId('country_id')->nullable()->constrained('countries');
            $table->foreignId('city_id')->nullable()->constrained('cities');
            $table->string('id_avatar')->nullable()->comment('صورة الهويه الوطنيه');
            $table->string('lang')->default('ar');
            $table->boolean('active')->default(0)->comment('mobile activation');
            $table->boolean('banned')->default(0);
            $table->boolean('accepted')->default(1)->comment('Admin approval');
            $table->boolean('notify')->default(1);
            $table->boolean('online')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('operating_companies');
	}
}
