<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateOrdersTable.
 */
class CreateOrdersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
            $table->id();
            $table->string('uuid', 50)->nullable();
            $table->string('order_num', 50);

            $table->foreignId('user_id')->nullable()->constrained()->onDelete('cascade');
            $table->foreignId('provider_id')->nullable()->constrained('providers')->onDelete('cascade');
            $table->foreignId('delegate_id')->nullable()->constrained('delegates')->onDelete('cascade');
            $table->foreignId('branch_id')->nullable()->constrained('branches')->onDelete('cascade');


            $table->integer('total_items')->default('0');
            $table->foreignId('coupon_id')->nullable()->constrained('coupons');
            $table->string('coupon_num')->nullable();
            $table->string('coupon_type')->nullable();
            $table->double('coupon_amount', 9, 2)->default(0);
            $table->double('vat_per', 9, 2)->default(0);
            $table->double('vat_amount', 9, 2)->default(0);
            $table->double('shipping_price', 9, 2)->default(0);
            $table->double('final_total', 9, 2)->default(0);
            $table->double('commission', 9, 2)->default(0);

            $table->tinyInteger('live')->default(0);

//            $table->enum('status', ['new','Prepare', 'received', 'onWay','delivered','user_cancel'])->default('new');
//            $table->enum('delegate_status', ['arrived', 'received', 'onWay','delivered'])->nullable();
//            $table->enum('provider_status', ['accepted','refused','pending','shipped','delivered'])->nullable();

            $table->integer('order_status')->default(0);

            $table->enum('order_type', ['deliver','branch','postponed','free'])->nullable();
            $table->tinyInteger('order_value')->nullable();

            $table->enum('pay_type', [ 'cash', 'bank', 'online'])->nullable();
            $table->enum('pay_status', ['pending', 'done'])->default('pending');
            $table->json('pay_data')->nullable();

            $table->double('receiving_lat', 15, 8)->default(24.68773);
            $table->double('receiving_lng', 15, 8)->default(46.72185);
            $table->string('receiving_map_desc', 255)->default('المنصوره');

            $table->double('lat', 15, 8)->default(24.68773);
            $table->double('lng', 15, 8)->default(46.72185);
            $table->string('map_desc', 255)->default('المنصوره');

            $table->foreignId('address_id')->nullable()->constrained('user_addresses');

            $table->dateTime('accepted_date')->nullable();
            $table->dateTime('pickup_date')->nullable();
            $table->dateTime('delivered_date')->nullable();

            $table->time('time')->nullable(); // for delivered
            $table->date('date')->nullable(); // for delivered
            $table->text('notes')->nullable();
            $table->text('refused_notes')->nullable();

            $table->date('created_date')->nullable();

            $table->time('progress_start')->nullable();
            $table->time('progress_end')->nullable();

            $table->enum('device_type', ['web', 'ios', 'android'])->default('ios');

            $table->boolean('user_delete')->default(0);
            $table->boolean('provider_delete')->default(0);
            $table->boolean('admin_delete')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}
}
