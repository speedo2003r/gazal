<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateOrdersCountViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

//        $data = User::query()->select('branches.id as branch_id','users.id as user_id',DB::raw('SUM(orders.id) as count'))
//            ->leftJoin('orders','orders.provider_id','=','users.id')
//            ->where('orders.delivered_date','!=',null)
//            ->leftJoin('branches','branches.id','=','orders.branch_id')
//            ->groupBy(['branch_id','user_id'])
//            ->toSql();
//        dd($data);
        DB::statement("
          CREATE VIEW orders_count_view AS
          (
            select `branches`.`id` as `branch_id`, `users`.`id` as `user_id`, SUM(orders.id) as count from `users` left join `orders` on `orders`.`provider_id` = `users`.`id` left join `branches` on `branches`.`id` = `orders`.`branch_id` where `orders`.`delivered_date` is not null and `users`.`deleted_at` is null group by `branch_id`, `user_id`
          )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SellingViews');
    }
}
