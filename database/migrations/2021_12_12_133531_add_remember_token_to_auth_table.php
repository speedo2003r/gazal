<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRememberTokenToAuthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('providers', function (Blueprint $table) {
            $table->rememberToken();
        });
        Schema::table('delegates', function (Blueprint $table) {
            $table->rememberToken();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->rememberToken();
        });
        Schema::table('branches', function (Blueprint $table) {
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('providers', function (Blueprint $table) {
            $table->dropColumn('remember_token');
        });
        Schema::table('delegates', function (Blueprint $table) {
            $table->dropColumn('remember_token');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('remember_token');
        });
        Schema::table('branches', function (Blueprint $table) {
            $table->dropColumn('remember_token');
        });
    }
}
