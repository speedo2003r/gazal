<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchZoneTable extends Migration {
  public function up() {
    Schema::create('branch_zone', function (Blueprint $table) {
      $table->id();
      $table->foreignId('branch_id')->constrained()->onDelete('cascade');
      $table->foreignId('zone_id')->constrained()->onDelete('cascade');
      $table->timestamp('created_at')->useCurrent();
      $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
    });
  }

  public function down() {
    Schema::drop('branch_zone');
  }
}
