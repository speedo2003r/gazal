<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product_types', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_product_id')->nullable()->constrained('order_products')->onDelete('cascade');
            $table->foreignId('item_types_detail_id')->nullable()->constrained('item_types_details')->onDelete('cascade');
            $table->double('price',9,2)->nullable(); // هيبقي ده السعر الزايد عن الاساسي
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product_types');
    }
}
