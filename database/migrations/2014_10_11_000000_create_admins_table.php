<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration {

  public function up() {
    Schema::create('admins', function (Blueprint $table) {
      $table->id();
      $table->string('name');
      $table->string('avatar')->default('/default.png');
      $table->string('email');
      $table->string('phone');
      $table->unique(['phone', 'email', 'deleted_at']);
      $table->string('password');
      $table->boolean('banned')->default(0);

      $table->foreignId('country_id')->nullable()->constrained();
      $table->foreignId('city_id')->nullable()->constrained();

      $table->foreignId('role_id')->nullable()->constrained();

      $table->text('address')->nullable();

      $table->rememberToken();
      $table->timestamp('created_at')->useCurrent();
      $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
      $table->softDeletes();
    });

      $user            = new \App\Entities\Admin();
      $user->name      = 'المدير العام';
      $user->email     = 'info@gazal.sa';
      $user->password  = '123456';
      $user->role_id  = 1;
      $user->phone     = '0541867992';
      $user->address   = 'المنصوره - ساميه الجمل';
      $user->banned    = 0;
      $user->save();
  }

  public function down() {
    Schema::dropIfExists('users');
  }
}
