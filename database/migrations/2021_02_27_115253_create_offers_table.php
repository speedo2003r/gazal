<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateOffersTable.
 */
class CreateOffersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offers', function(Blueprint $table) {
            $table->id();
            $table->string('image');
            $table->text('title');
            $table->text('desc')->nullable();
            $table->tinyInteger('discount')->default(0);
            $table->foreignId('country_id')->nullable()->constrained('countries');
            $table->foreignId('city_id')->nullable()->constrained('cities');
            $table->foreignId('category_id')->nullable()->constrained('categories');
            $table->foreignId('user_id')->nullable()->constrained('providers');
            $table->foreignId('item_id')->nullable()->constrained('items');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->boolean('active')->default(true);
            $table->enum('type',['seller','item'])->nullable(); // 1 = seller | 2 = item
            $table->enum('page',['special','offer'])->nullable(); // 1 = special | 2 = offer
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offers');
	}
}
