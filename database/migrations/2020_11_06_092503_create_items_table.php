<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->double('price',9,2)->default(0);
            $table->double('discount_price')->nullable();
            $table->date('from')->nullable();
            $table->date('to')->nullable();
            $table->foreignId('user_id')->nullable()->constrained('providers')->onDelete('cascade');
            $table->boolean('exist')->nullable();
            $table->integer('views')->nullable()->default(0);
            $table->tinyInteger('status')->default(0);
            $table->foreignId('category_id')->nullable()->constrained('categories')->onDelete('cascade');
            $table->foreignId('subcategory_id')->nullable()->constrained('categories')->onDelete('cascade');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });


        Schema::create('features', function (Blueprint $table) {
            $table->id();
            $table->text('title')->nullable();
            $table->double('price',9,2)->default(0);
            $table->foreignId('item_id')->nullable()->constrained('items');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
