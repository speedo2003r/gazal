<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateDelegatesTable.
 */
class CreateDelegatesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delegates', function(Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('avatar')->default('/default.png');
            $table->string('email');
            $table->integer('wallet')->default(0);
            $table->tinyInteger('commission_status')->default(0);
            $table->double('income')->nullable()->default(0);
            $table->double('balance')->nullable()->default(0);
            $table->integer('commission')->nullable()->default(0);
            $table->string('country_code', 10)->nullable();
            $table->string('phone');
            $table->unique(['phone', 'email', 'deleted_at']);
            $table->string('replace_phone')->nullable();
            $table->string('v_code')->nullable();
            $table->string('password');
            $table->date('birth_date')->nullable();
            $table->string('address')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->foreignId('country_id')->nullable()->constrained('countries')->onDelete('set null');
            $table->foreignId('city_id')->nullable()->constrained('cities')->onDelete('set null');
            $table->foreignId('company_id')->nullable()->constrained('companies')->onDelete('set null');
            $table->string('id_avatar')->nullable()->comment('صورة البطاقه التعريفيه');
            $table->string('licence_car_image')->nullable()->comment('رخصة المركبه');
            $table->string('licence_image')->nullable()->comment('رخصة القياده');
            $table->string('car_front_image')->nullable()->comment('صورة المركبه من الأمام');
            $table->string('car_back_image')->nullable()->comment('صورة المركبه من الخلف');
            $table->foreignId('car_type_id')->nullable()->constrained('car_types')->onDelete('set null');
            $table->integer('max_dept')->default(0);
            $table->string('lang')->default('ar');
            $table->boolean('active')->default(0)->comment('mobile activation');
            $table->boolean('banned')->default(0);
            $table->boolean('accepted')->default(1)->comment('Admin approval');
            $table->boolean('notify')->default(1);
            $table->boolean('online')->default(0);

            $table->string('share_code', 50)->unique();
            $table->string('friend_share_code', 50)->nullable();
            $table->tinyInteger('is_friend_share_code_used')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('delegates');
	}
}
