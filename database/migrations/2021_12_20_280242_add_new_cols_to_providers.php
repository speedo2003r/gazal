<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColsToProviders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('providers', function (Blueprint $table) {
            $table->tinyInteger('user_type')->default(1); // 1 == provider && 2 == employee
            $table->foreignId('parent_id')->nullable()->constrained('providers');
            $table->foreignId('representative_id')->nullable()->constrained('employees');
            $table->foreignId('account_manager_id')->nullable()->constrained('admins');
            $table->foreignId('active_responsible_id')->nullable()->constrained('admins');
            $table->foreignId('accountant_id')->nullable()->constrained('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('provider_categories', function (Blueprint $table) {
            //
        });
    }
}
