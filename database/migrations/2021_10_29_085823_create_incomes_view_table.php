<?php

use App\Entities\Income;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateIncomesViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

//        $data = Income::query()->select('incomes.user_id',DB::raw('SUM(incomes.income) as income'),DB::raw('SUM(incomes.debtor) - SUM(incomes.creditor) as balance'))
//            ->where('incomes.status',1)
//            ->groupBy('incomes.user_id')
//            ->toSql();
//        dd($data);
        DB::statement("
          CREATE VIEW incomes_view AS
          (
            select `incomes`.`user_id`, SUM(incomes.income) as income, SUM(incomes.debtor) - SUM(incomes.creditor) as balance from `incomes` where `incomes`.`status` = 1 and `incomes`.`deleted_at` is null group by `incomes`.`user_id`
          )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SellingViews');
    }
}
