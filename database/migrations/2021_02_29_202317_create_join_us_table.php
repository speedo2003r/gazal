<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJoinUsTable extends Migration {
  public function up() {
    Schema::create('join_us', function (Blueprint $table) {
      $table->id();
      $table->string('name',50)->nullable();
      $table->string('store_name',50)->nullable();
      $table->foreignId('category_id')->nullable()->constrained('categories');
      $table->string('phone',20)->nullable();
      $table->string('email',50)->nullable();
      $table->foreignId('city_id')->nullable()->constrained('cities');
      $table->tinyInteger('status')->default(0);
      $table->timestamp('created_at')->useCurrent();
      $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
      $table->softDeletes();
    });
  }

  public function down() {
    Schema::dropIfExists('join_us');
  }
}
