<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateRatingViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

//        DB::statement("
//          CREATE VIEW rating AS
//          (
//            select `review_rates`.`rateable_type`, `review_rates`.`rateable_id`, `review_rates`.`rate`,`review_rates`.`order_id`, COUNT('review_rates.rateable_id') as followers from `orders` left join `review_rates` on `review_rates`.`rateable_id` = `orders`.`id` WHERE `review_rates`.`rateable_type` = 'App\\\Entities\\\Order' and `orders`.`deleted_at` is null group by `review_rates`.`rateable_type`, `review_rates`.`rateable_id`, `review_rates`.`rate`, `review_rates`.`order_id`
//          )
//        ");
//        $review = App\Entities\ReviewRate::query()->select('rateable_type','rateable_id',DB::raw('COUNT(review_rates.model_id) AS followers'),DB::raw('(SUM(review_rates.rate) / COUNT(review_rates.model_id)) AS rates'))->toSql();
//        dd($review);

        DB::statement("
          CREATE VIEW rating AS
          (
            select `rateable_type`, `rateable_id`, COUNT(review_rates.model_id) AS followers, (SUM(review_rates.rate) / COUNT(review_rates.model_id)) AS rates from `review_rates`
          )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating');
    }
}
