<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellerPaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller_pays', function (Blueprint $table) {
            $table->id();
            $table->string('type')->nullable();
            $table->string('acc_owner_name')->nullable();
            $table->string('acc_number')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('price')->nullable();
            $table->string('image')->nullable();
            $table->foreignId('order_id')->nullable()->constrained('orders');
            $table->foreignId('user_id')->nullable()->constrained('users');
            $table->foreignId('parent_id')->nullable()->constrained('seller_pays');
            $table->foreignId('income_id')->nullable()->constrained('incomes');
            $table->tinyInteger('pay_status')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seller_pays');
    }
}
