<?php

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateOrdersPerDayViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

//        $data = User::query()->select('branches.id as branch_id','users.id as user_id',DB::raw('SUM(orders.final_total) as total'))
//            ->leftJoin('orders','orders.provider_id','=','users.id')
//            ->where('orders.live','=',1)
//            ->leftJoin('branches','branches.id','=','orders.branch_id')
//            ->groupBy(['branch_id','user_id','orders.delivered_date'])
//            ->toSql();
//        dd($data);
        DB::statement("
          CREATE VIEW orders_per_day_view AS
          (
              select `branches`.`id` as `branch_id`, `users`.`id` as `user_id`, SUM(orders.final_total) as total from `users` left join `orders` on `orders`.`provider_id` = `users`.`id` left join `branches` on `branches`.`id` = `orders`.`branch_id` where `orders`.`live` = 1 and `users`.`deleted_at` is null group by `branch_id`, `user_id`, `orders`.`delivered_date`
          )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SellingViews');
    }
}
