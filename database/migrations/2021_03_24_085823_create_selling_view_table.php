<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateSellingViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        $data = App\Entities\OrderProduct::query()->select('branches.id as branch_id','items.id as item_id','orders.id as order_id','order_products.id as order_product_id','order_products.price as price','order_products.qty as count')
//            ->leftJoin('items','items.id','=','order_products.item_id')
//            ->leftJoin('orders','orders.id','=','order_products.order_id')
//            ->leftJoin('branches','branches.id','=','orders.branch_id')
//            ->where('orders.delivered_date','!=',null)
//            ->groupBy(['branch_id','items.id','order_id','order_products.id','price','count'])
//            ->toSql();
//        dd($data);
        DB::statement("
          CREATE VIEW selling_view AS
          (
            select `branches`.`id` as `branch_id`, `items`.`id` as `item_id`, `orders`.`id` as `order_id`, `order_products`.`id` as `order_product_id`, `order_products`.`price` as `price`, `order_products`.`qty` as `count` from `order_products` left join `items` on `items`.`id` = `order_products`.`item_id` left join `orders` on `orders`.`id` = `order_products`.`order_id` left join `branches` on `branches`.`id` = `orders`.`branch_id` where `orders`.`delivered_date` is not null and `order_products`.`deleted_at` is null group by `branch_id`, `items`.`id`, `order_id`, `order_products`.`id`, `price`, `count`
          )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SellingViews');
    }
}
