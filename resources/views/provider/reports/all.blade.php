@extends('provider.layouts.master')

@section('title', __('Reports'))

@section('content')
    @push('css')
        <style>
            .dataTables_filter{
                float: right;
            }
            div.dataTables_wrapper div.dataTables_filter label:after, div.dataTables_wrapper div.dataTables_length label:after{
                height: 50px;
                width: 45px;
                bottom: 0;
                top: auto;
            }
            .card-filter{
                text-align: center;
                margin: 30px auto;
            }
            .card-filter .btn-fil{
                background: #2c4167;
                padding: 10px 15px;
                margin: 0 20px;
            }
        </style>
    @endpush

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">@lang('Reports')</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('Main')</a></li>
                        <li class="breadcrumb-item active">@lang('Reports')</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <div class="table-responsive">
{{--                    <form action="">--}}
{{--                        <div class="row">--}}
{{--                            <div class="form-group col-md-5">--}}
{{--                                <label for="">{{awtTrans('الحالة')}}</label>--}}
{{--                                <select name="order_status" class="form-control">--}}
{{--                                    <option value="">{{awtTrans('الكل')}}</option>--}}
{{--                                    <option value="{{\App\Entities\Order::STATUS_WAITING}}" {{request('order_status') === (string) \App\Entities\Order::STATUS_WAITING ? 'selected' : ''}}>@lang('order.'.\App\Entities\Order::STATUS_WAITING)</option>--}}
{{--                                    <option value="{{\App\Entities\Order::STATUS_ACCEPTED}}" {{request('order_status') == \App\Entities\Order::STATUS_ACCEPTED ? 'selected' : ''}}>@lang('order.'.\App\Entities\Order::STATUS_ACCEPTED)</option>--}}
{{--                                    <option value="{{\App\Entities\Order::STATUS_PREPARED}}" {{request('order_status') == \App\Entities\Order::STATUS_PREPARED ? 'selected' : ''}}>@lang('order.'.\App\Entities\Order::STATUS_PREPARED)</option>--}}
{{--                                    <option value="{{\App\Entities\Order::STATUS_GIVE_TO_DELEGATE}}" {{request('order_status') == \App\Entities\Order::STATUS_GIVE_TO_DELEGATE ? 'selected' : ''}}>@lang('order.'.\App\Entities\Order::STATUS_GIVE_TO_DELEGATE)</option>--}}
{{--                                    <option value="{{\App\Entities\Order::STATUS_ON_WAY}}" {{request('order_status') == \App\Entities\Order::STATUS_ON_WAY ? 'selected' : ''}}>@lang('order.'.\App\Entities\Order::STATUS_ON_WAY)</option>--}}
{{--                                    <option value="{{\App\Entities\Order::STATUS_ARRIVED_TO_CLIENT}}" {{request('order_status') == \App\Entities\Order::STATUS_ARRIVED_TO_CLIENT ? 'selected' : ''}}>@lang('order.'.\App\Entities\Order::STATUS_ARRIVED_TO_CLIENT)</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <div class="form-group col-md-5">--}}
{{--                                <label for="">{{awtTrans('الفرع')}}</label>--}}
{{--                                <select name="branch" class="form-control">--}}
{{--                                    <option value="">{{awtTrans('الكل')}}</option>--}}
{{--                                    @foreach($branches as $branch)--}}
{{--                                    <option value="{{$branch->id}}" {{request('branch') == $branch->id ? 'selected' : ''}}>{{$branch->name}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <div class="form-group col-md-2">--}}
{{--                               <button class="btn btn-primary btn-fil" style="margin-top: 30px">{{awtTrans('بحث')}}</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </form>--}}
                    {!! $dataTable->table(["class"=> "table table-striped table-bordered table-hover table-checkable dataTable no-footer"],true) !!}
                </div>


    </div>
    </div>
    </div>



@endsection

@push('js')

    {!! $dataTable->scripts() !!}

@endpush
