@extends('provider.layouts.master')
@section('title', $item->title)

@push('css')
    <style>
        .item-info .col-md-6 , .item-info .col-md-12 {
            margin-top: 20px;
        }
    </style>
@endpush

@section('content')


    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">{{$item->title}}</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('provider')}}">{{awtTrans('الرئيسيه')}}</a></li>
                        <li class="breadcrumb-item "><a href="{{ route('provider.product') }}">{{__('Menu')}}</a></li>
                        <li class="breadcrumb-item active">{{$item->title}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <div class="mt-10 pag-requ">

                    <div class="row item-info">

                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>@lang('Name (ar)')</aside>
                                <aside class="main-color">{{ $item->getTranslation('title', 'ar') }} </aside>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>@lang('Name (en)')</aside>
                                <aside class="main-color">{{ $item->getTranslation('title', 'en') }} </aside>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>{{__('Price')}}</aside>
                                <aside class="main-color">{{$item->price}}</aside>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>{{__('Price after discount')}}</aside>
                                <aside class="main-color">{{$item->price()}}</aside>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>{{__('Discount')}}</aside>
                                <aside class="main-color">{{$item->discount()}}</aside>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>{{__('Main Category')}}</aside>
                                <aside class="main-color">
                                    {{ $item->category->title ?? '' }}
                                </aside>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>{{__('Sub Category')}}</aside>
                                <aside class="main-color">{{ $item->subcategory->title ?? '' }}</aside>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>@lang('Total revenues')</aside>
                                <aside class="main-color">
                                    {{ $item->orders_sum_price }}
                                </aside>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>@lang('Description')</aside>
                                <aside class="main-color">
                                    {{ $item->description }}
                                </aside>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>@lang('Images')</aside>
                                <aside class="main-color">
                                    <div class="row">
                                        @foreach($item->files as $img)
                                            <div class="colmd-3">
                                                <img src="{{ url('storage/'.$img->image) }}" alt="" height="120">
                                            </div>

                                        @endforeach
                                    </div>

                                </aside>
                            </div>
                        </div>




                    </div>

                </div>

            </div>
        </div>
    </div>


@endsection
