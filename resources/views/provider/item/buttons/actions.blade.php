<div class="btn-group">

    <a href="{{ route('provider.product.edit', $id) }}" class="btn btn-primary" ><i class="fas fa-edit"></i> {{trans('Edit')}}</a>
    <a href="{{ route('provider.product.show', $id) }}" class="btn btn-info" ><i class="fa fa-eye"></i> {{trans('Show')}}</a>
    <a data-toggle="modal" data-target="#delete_record{{$id}}" href="#" class="btn btn-danger">
        <i class="fas fa-trash"></i> {{trans('Delete')}}</a>

</div>
<div class="modal fade" id="delete_record{{$id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{trans('Delete')}}</h4>
                <button class="close" data-dismiss="modal">x</button>
            </div>
            <div class="modal-body">
                <i class="fa fa-exclamation-triangle"></i> {{trans('Are you sure?')}}
            </div>
            <div class="modal-footer">
                <form action="{{ route('provider.product.destroy', $id) }}">@csrf @method('DELETE')
                    <button class="btn btn-danger btn-flat">@lang('Yes')</button>
                    <a class="btn btn-default btn-flat" data-dismiss="modal">{{trans('Cancel')}}</a>
                </form>
            </div>
        </div>
    </div>
</div>