@extends('provider.layout.master')
@section('title',awtTrans('تعديل منتج'))
@section('content')

    <div class="page_add_P">

        <div>
            <span>{{awtTrans('المنتجات')}}</span> /
            <span>{{awtTrans('تعديل منتج')}}</span>
        </div>

        <form action="{{route('seller.product.update',$item['id'])}}" id="formSave" enctype="multipart/form-data" method="post">
            @csrf
        <div class="row text-center">
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{awtTrans('بسيط')}}</label>
                    <input type="radio" value="single" @if($item['type'] == 'single') checked @endif name="type" class="form-control mx-auto">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>{{awtTrans('متعدد')}}</label>
                    <input type="radio" value="multiple" @if($item['type'] == 'multiple') checked @endif name="type" class="form-control mx-auto">
                </div>
            </div>
        </div>
        <div class="uplooad_all_img" style="@if($item['type'] == 'multiple') display:none @endif">

            <div>
                <h6>{{awtTrans('صورة المنتج الرئيسية')}}</h6>
                <div class="images-upload-block upload-1-img">
                    <img src="{{$item->main_image}}" style="width: 100%;height: 100%">
                    <input type="file" id="image" name="image">
                </div>
            </div>

            <div class="d-flex align-items-center">

                <div>
                    <h6>{{awtTrans('صور المنتج الفرعية')}}</h6>
                    <div class="images-upload-block">
                        <img src="{{dashboard_url('waset/imgs/surface1.png')}}">
                    </div>
                </div>


                <div class="images-uploader">+ <input type="file" id="up-img-1"  accept="image/*" name="images[]" multiple></div>

            </div>

        </div>

        <div class="upload-area d-flex flex-wrap">
            @foreach($item->files->where('main',0) as $image)
                <div class="uploaded-block-imges">
                    <img src="{{dashboard_url('storage/images/items').'/'.$image['image']}}">
                    <label class="remove-close close" data-id="{{$image['id']}}"> x </label>
                </div>
            @endforeach

        </div>


        <div>

            <div class="row">
                <div class="col-md-6 multiple">
                    <div class="form-group">
                        <label>{{awtTrans('اسم المنتج بالعربي')}}</label>
                        <input type="text" class="form-control" name="title_ar" value="{{$item->title != null ? $item->getTranslations()['title']['ar'] : ''}}" placeholder="اسم المنتج بالعربي">
                    </div>
                </div>
                <div class="col-md-6 multiple">
                    <div class="form-group">
                        <label>{{awtTrans('اسم المنتج بالانجليزي')}}</label>
                        <input type="text" class="form-control" name="title_en" value="{{$item->title != null ? $item->getTranslations()['title']['en'] : ''}}" placeholder="اسم المنتج بالانجليزي">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{awtTrans('تصنيف المنتج الرئيسي')}}</label>
                        <select class="form-control e1" name="category_id">
                            <option selected hidden disabled>{{awtTrans('تصنيف المنتج الرئيسي')}}</option>
                            @foreach($categories as $category)
                                <option value="{{$category['id']}}" @if($item['category_id'] == $category['id']) selected @endif>{{$category['title']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{awtTrans('تصنيف المنتج الفرعي')}}</label>
                        <select class="form-control e1" name="subcategory_id">
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{awtTrans('ماركة المنتج')}}</label>
                        <select class="form-control e1" name="brand_id">
                        </select>
                    </div>
                </div>


                    <div class="col-md-6 multiple">
                        <div class="form-group">
                            <label>{{awtTrans('السعر')}}</label>
                            <input type="number" name="price" class="form-control" value="{{$item['price']}}">
                        </div>
                    </div>
                    <div class="col-md-6 multiple">
                        <div class="form-group">
                            <label>{{awtTrans('قابل للاعاده')}}</label>
                            <select name="reclaim" class="form-control">
                                <option selected hidden disabled>{{awtTrans('قابلية المنتج للاعاده')}}</option>
                                <option value="0" @if($item['reclaim'] == 0) selected @endif>{{awtTrans('غير قابل للاعاده')}}</option>
                                <option value="1" @if($item['reclaim'] == 1) selected @endif>{{awtTrans('قابل للاعاده')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 multiple">
                        <div class="form-group">
                            <label>({{awtTrans('اختياري')}}) {{awtTrans('الخصم')}}</label>
                            <input type="number" value="{{$item['discount_price']}}" name="discount_price" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4 multiple">
                        <div class="form-group">
                            <label>{{awtTrans('من')}}</label>
                            <input type="date" value="{{$item['from']}}" name="from" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4 multiple">
                        <div class="form-group">
                            <label>{{awtTrans('الي')}}</label>
                            <input type="date" value="{{$item['to']}}" name="to" class="form-control">
                        </div>
                    </div>

                <div class="col-6 ">

                    <div class="form-group multiple">
                        <label>{{awtTrans('نظرة عامة بالعربي')}}</label>
                        <textarea class="form-control mytextarea" name="description_ar" id="description_ar" placeholder="{{awtTrans('وصف المنتج بالعربي')}}">
                            @if($item['type'] == 'single') {{isset($item->getTranslations()['description']['ar']) ? $item->getTranslations()['description']['ar'] : ''}} @endif
                        </textarea>

                    </div>

                </div>

                <div class="col-6 ">
                    <div class="form-group multiple">
                        <label>{{awtTrans('نظرة عامة بالانجليزي')}}</label>
                        <textarea class="form-control mytextarea" name="description_en" id="description_en" placeholder="{{awtTrans('وصف المنتج بالانجليزي')}}">
                                @if($item['type'] == 'single') {{isset($item->getTranslations()['description']['en']) ? $item->getTranslations()['description']['en'] : ''}} @endif
                            </textarea>

                    </div>
                </div>


                <div class="col-12 multiple">

                    <label>{{awtTrans('مواصفات المنتج')}}</label>
                    <div class="details">
                        @foreach($item->details()->where('type','item')->get() as $detail)
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>{{awtTrans('السمه')}}</label>
                                    <select name="specification_id[]" class="form-control specification_id">
                                        <option value="">{{awtTrans('اختر')}}</option>
                                        @foreach($specifications as $specification)
                                            <option value="{{$specification['id']}}" @if($detail['specification_id'] == $specification['id']) selected @endif>{{$specification['title']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5 text-details">
                                @if($detail->specification['type'] == 'text')
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{awtTrans('الوصف بالعربي')}}</label>
                                            <input type="text" name="text_ar[]" value="{{isset($detail->getTranslations()['title']['ar']) ? $detail->getTranslations()['title']['ar'] : ''}}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{awtTrans('الوصف بالانجليزي')}}</label>
                                            <input type="text" name="text_en[]" value="{{isset($detail->getTranslations()['title']['en']) ? $detail->getTranslations()['title']['en'] : ''}}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                @else
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{awtTrans('الوصف بالعربي')}}</label>
                                                <select name="text_ar[]" class="form-control">
                                                    <option value="">{{awtTrans('اختر')}}</option>
                                                    @foreach($detail->specification->details as $data)
                                                        <option value="{{$data['id']}}" @if($detail['detail_id'] == $data['id']) selected @endif>{{$data['title']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-danger delete-option" style="margin-top: 30px;">
                                    {{awtTrans('حذف')}}</button>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-success add-option">{{('اضافه سمه')}}</button>
                        </div>
                    </div>

                </div>


{{--                <div class="col-12">--}}

{{--                    <label>خصائص المنتج</label>--}}

{{--                    <div class="row">--}}

{{--                        <div class="col-md-6">--}}
{{--                            <div class="form-group">--}}
{{--                                <input type="text" class="form-control" placeholder="الحجم">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="col-md-6">--}}
{{--                            <div class="form-group">--}}
{{--                                <input type="text" class="form-control" placeholder="اللون">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="col-md-6">--}}
{{--                            <div class="form-group">--}}
{{--                                <input type="text" class="form-control" placeholder="السعر">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="col-12">--}}
{{--                            <div class="form-group">--}}
{{--                                <label>صورة لون المنتج</label>--}}
{{--                                <div class="images-upload-block upload-1-img">--}}
{{--                                    <img src="{{dashboard_url('waset/imgs/surface1.png')}}">--}}
{{--                                    <input type="file" id="image2">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}


{{--                    <button class="btn-main">اضافة خاصيه جديدة</button>--}}


{{--                </div>--}}



                <div class="col-md-12 multiple">
                    <div class="form-group">
                        <label>{{awtTrans('الكمية المتاحة')}}</label>
                        <input type="number" name="count" class="form-control" value="{{$item->groups()->first() != null ? $item->groups()->first()['count'] : $item['count']}}" placeholder="{{awtTrans('الكمية المتاحة')}}">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{awtTrans('مدة الضمان')}}</label>
                        <input type="text" name="warranty" value="{{$item['warranty']}}" class="form-control" placeholder="{{awtTrans('مدة الضمان')}}">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{awtTrans('كوبون الخصم')}}</label>
                        <input type="text" name="code" value="{{$item->coupon['code']}}" class="form-control" placeholder="{{awtTrans('كوبون الخصم')}}">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{awtTrans('عدد مرات الاستخدام')}}</label>
                        <input type="text" name="coupon_count" value="{{$item->coupon['count']}}" class="form-control" placeholder="{{awtTrans('عدد مرات الاستخدام')}}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{awtTrans('قيمة الكوبون')}}</label>
                        <input type="text" name="coupon_value" value="{{$item->coupon['value']}}" class="form-control" placeholder="{{awtTrans('قيمة الكوبون')}}">
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{awtTrans('نوع السيارة المسموح لها بنقل المنتج')}}</label>
                        <select class="form-control e1" name="car_type_id">
                            <option selected hidden disabled>{{awtTrans('نوع السيارة')}}</option>
                            @foreach($carTypes as $cartype)
                                <option value="{{$cartype['id']}}" @if($item['car_type_id'] == $cartype['id']) selected @endif>{{$cartype['title']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="col-12">

                    <div class="form-group">
                        <label>{{awtTrans('اختيار الفروع')}}</label>
                        <div class="d-flex justify-content-between flex-wrap">
                            @foreach(auth()->user()->branches as $branch)
                            <label><input type="checkbox" name="branches[]" @if($item->branches()->where('branches.id',$branch['id'])->exists()) checked @endif  value="{{$branch['id']}}"> <span>{{$branch['title']}}</span></label>
                            @endforeach
                        </div>

                    </div>

                </div>

                <div class="col-12">
                    <button type="submit" class="btn-main save" onclick="save()">
                        {{awtTrans('تحديث المنتج')}}
                        <i class="fa fa-spinner fa-spin spinner-ajax" style="display: none"></i>
                    </button>

                </div>

            </div>

        </div>

        </form>
    </div>

@endsection
@push('js')
    <script>

        $(document).ready(function() {
            getCategories({{$item['category_id']}},{{$item['subcategory_id']}});
            getCategories({{$item['subcategory_id']}},{{$item['brand_id']}},'[name=brand_id]');
            getSpecifications({{$item['subcategory_id']}});
            @if($item['type'] == 'multiple')
            $('.multiple').css({'display':'none'});
            $('.uplooad_all_img').css({'display':'none'});
            @else
            $('.multiple').css({'display':'grid'});
            $('.uplooad_all_img').css({'display':'flex'});
            @endif
        });
        $(document).on('change','[name=type]',function (){
            var type = $(this).val();
            if(type == 'multiple'){
                $('.multiple').css({'display':'none'});
                $('.uplooad_all_img').css({'display':'none'});
            }else{
                $('.multiple').css({'display':'grid'});
                $('.uplooad_all_img').css({'display':'flex'});
            }
        });
        $(document).on('change','[name=category_id]',function (){
            var type = $(this).val();
            $('[name=brand_id]').empty();
            getCategories(type);
        });
        $(document).on('change','[name=subcategory_id]',function (){
            var type = $(this).val();
            getCategories(type,'','[name=brand_id]');
            $('.details').empty();
            getSpecifications(type);
        });
        $('body').on('click','.remove-close',function (){
            var id = $(this).data('id');
            $.ajax({
                type        : 'POST',
                url         : `{{route('ajax.removeImage','ob')}}`.replace('ob',id),
                datatype    : 'json' ,
                success     : function(data){

                }
            });
        })
        $(document).on('change','.specification_id',function (){
            var type = $(this).val();
            getSpecificationDetails($(this),type);
        });

        function save() {
            event.preventDefault();

            //disable the submitted button and show he spinner
            $('.save').prop('disabled', true).css({opacity: '0.5'});
            $('.spinner-ajax').css({display: 'inline-block'});
            var description_ar = tinymce.get("description_ar").getContent();
            var description_en = tinymce.get("description_en").getContent();
            var data = new FormData($("#formSave")[0]);
            data.append('description_en',description_en);
            data.append('description_ar',description_ar);
            $.ajax({
                type        : 'POST',
                url         : `{{route('seller.product.update',$item['id'])}}` ,
                datatype    : 'json' ,
                processData : false,
                contentType : false,
                data        : data,
                success     : function(data){


                    if(data.key == 'fail'){
                        $('.save').removeAttr("disabled").css({opacity: '1'});
                        $('.spinner-ajax').css({display: 'none'});
                        toastr.error(data.msg);
                    }else{
                        toastr.success(data.msg);
                        setInterval(function(){ location.assign(data.url); }, 2000);

                    }

                }
            });
        }
        $(function (){
            'use strict'
            $('body').on('click','.delete-option',function (){
                $(this).parent().parent().remove();
            });
            $('body').on('click','.add-option',function (){
                var elem = $(this).parent().parent().prev().find('.col-md-5');
                if(elem.length > 0){
                    var html = '';
                    html += `
                    <div class="row">
                        <div class="col-md-5">
                            ${elem.html()}
                        </div>
                        <div class="col-md-5 text-details">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{awtTrans('الوصف بالعربي')}}</label>
                                        <input type="text" name="text_ar[]" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{awtTrans('الوصف بالانجليزي')}}</label>
                                        <input type="text" name="text_en[]" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-danger delete-option" style="margin-top: 30px;">{{awtTrans('حذف')}}</button>
                        </div>
                    </div>
                `;
                    $('.details').append(html);
                }else{
                    getSpecifications($('[name=subcategory_id]').val());
                }


            });
        });
        $('body').on('change','.specification_id',function (){
            var that = $(this);
            $('.specification_id').not(this).each(function(){
                if($(this).val() == that.val()){
                    toastr.error("{{awtTrans('لا يمكن اختيار هذه السمه مره أخري لأنه تم اختيارها بالفعل من قبل')}}");
                    that.val('');
                }else{
                    // that.parent().next().next().empty();
                }
            })
        });
    </script>
@endpush
