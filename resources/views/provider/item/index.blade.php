@extends('provider.layouts.master')
@section('title',awtTrans('المنتجات'))
@section('content')
@push('css')
    <style>
        div.dataTables_wrapper div.dataTables_filter label:after, div.dataTables_wrapper div.dataTables_length label:after{
            height: 50px;
            width: 45px;
            bottom: 0;
            top: auto;
        }
        .card-filter{
            text-align: center;
            margin: 30px auto;
        }
        .card-filter .btn-fil{
            background: #2c4167;
            padding: 10px 15px;
            margin: 0 20px;
        }
    </style>
@endpush

<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="card-filter">
                <a href="{{route('provider.product')}}" class="btn-fil @if(request()->query('type') == null) active @endif text-center">{{awtTrans('الكل')}}</a>
                <a href="{{route('provider.product',['type'=>'online'])}}" class="btn-fil @if(request()->query('type') == 'online') active @endif text-center">{{awtTrans('منتجات معروضة')}}</a>
                <a href="{{route('provider.product',['type'=>'notActive'])}}" class="btn-fil @if(request()->query('type') == 'notActive') active @endif text-center">{{awtTrans('منتجات معلقة')}}</a>
                <a href="{{route('provider.product',['type'=>'banned'])}}" class="btn-fil @if(request()->query('type') == 'banned') active @endif text-center">{{awtTrans('منتجات مرفوضة')}}</a>

            </div>






            <div class="table-content">

        <a href="{{route('provider.product.create')}}" class="btn btn-lighten-primary waves-effect waves-primary  width-md">{{awtTrans('إضافة منتج جديد')}}</a>

        {!! $dataTable->table([
         'class' => "table dt-responsive nowrap dataTable no-footer dtr-inline collapsed",
         'id' => "itemdatatable-table",
         ],true) !!}
    </div>
        </div>
    </div>
</div>



@endsection
@push('js')

    {!! $dataTable->scripts() !!}

@endpush
