@extends('provider.layout.master')
@section('title',awtTrans('الحسابات الماليه'))
@section('content')
@push('css')
    <style>
        .modal-submit {
            margin: auto;
            display: block;
        }
        .modal-header{
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 10px 15px;
        }
        .modal-header .close{
            padding: 0;
            margin: 0;
            opacity: 1;
            color: #fff;
        }
        .bank-trans{
            background: #f3f3f3;
            padding: 10px;
            border-radius: 10px;
            margin-bottom: 15px;
        }
        .form-items .form-item {
            padding: 5px 0;
            position: relative;
        }
        .form-items .checkcontainer {
            display: block;
            position: relative;
            padding-right: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 18px;
            font-weight: bold;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .form-items .checkcontainer input {
            position: relative;
            opacity: 1;
            cursor: pointer;

        }
        .form-items .checkcontainer input:checked~.radiobtn {
            background-color: transparent;
        }
        .form-items .checkcontainer .radiobtn {
            position: absolute;
            top: 0;
            right: 0;
            height: 25px;
            width: 25px;
            background-color: transparent;
            border: 2px solid var(--main-color);
            border-radius: 50%;
        }
        .form-items .checkcontainer input:checked~.radiobtn:after {
            background-color: var(--main-color);
        }
        .form-items .checkcontainer .radiobtn:after {
            content: "";
            position: absolute;
        }
        .form-items .checkcontainer .radiobtn:after {
            top: 50%;
            transform: translateY(-50%);
            left: 0;
            right: 0;
            width: 70%;
            height: 70%;
            margin: auto;
            border-radius: 50%;
            background: transparent;
        }
        .form-items .form-item .pay-img {
            position: absolute;
            left: 0;
            top: 50%;
            transform: translateY(-50%);
            width: 35px;
            height: 35px;
        }
    </style>
@endpush

    <div class="table-content reques_table po_R">

        <div class="title_table"><span>{{awtTrans('الطلبات')}} </span> / <span class="main-color">{{awtTrans('الحسابات الماليه')}}</span></div>

        {!! $dataTable->table([
         'class' => "table dt-responsive w-100",
         'id' => "sellerpaydatatable-table",
         ],true) !!}


        <div class="d-flex justify-content-between flex-wrap mt-10">

            <div>{{awtTrans('اجمالي المبيعات')}} : <span class="main-color">{{$allselling}} {{awtTrans('ر.س')}}</span></div>
            <div>{{awtTrans('المبلغ المطلوب دفعه')}} : <span class="main-color">{{$sellerPayPrice}} {{awtTrans('ر.س')}}</span></div>

        </div>

    </div>

    <button class="btn-main allpay" data-toggle="modal" data-target="#exampleModal">{{awtTrans('تسوية')}}</button>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{awtTrans('وسيلة الدفع')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('seller.pay')}}" method="post" enctype="multipart/form-data">
                        @csrf()
                        <input type="hidden" value="" name="pay_id">
                        <div class="form-items">
                            <div class="form-item">
                                <label class="checkcontainer">
                                    <input type="radio" name="payment" checked value="wallet">
                                    <span class="radiobtn"></span>
                                    {{awtTrans('المحفظة')}}
                                </label>
                                <img src="https://coder.aait.sa/El_Adawy/shopness/img/digital-wallet.png" alt="" class="pay-img">
                            </div>
                            <div class="form-item " id="bank_show">
                                <label class="checkcontainer ">
                                    <input type="radio" name="payment" value="bank">
                                    <span class="radiobtn"></span>
                                    {{awtTrans('تحويل بنكي')}}
                                </label>
                                <img src="https://coder.aait.sa/El_Adawy/shopness/img/pay1.png" alt="" class="pay-img">
                            </div>
                            <div class="bank-trans d-none">
                                <div class="form-group">
                                    <label for="bank_name">{{awtTrans('اسم البنك')}}</label>
                                    <input type="text" class="form-control"  name="bank_name" placeholder="اسم البنك">
                                </div>
                                <div class="form-group">
                                    <label for="bank_name">{{awtTrans('اسم صاحب الحساب')}}</label>
                                    <input type="text" class="form-control" name="acc_owner_name" placeholder="اسم صاحب الحساب">
                                </div>
                                <div class="form-group">
                                    <label for="bank_name">{{awtTrans('رقم الحساب')}}</label>
                                    <input type="text" class="form-control" name="acc_number" placeholder="رقم الحساب">
                                </div>
                                <div class="form-group">
                                    <label for="bank_name">{{awtTrans('المبلغ المراد دفعه')}}</label>
                                    <input type="text" class="form-control" name="price" placeholder="المبلغ المراد دفعه" value="{{$sellerPayPrice}}" readonly>
                                </div>
                                <div class="form-group" style="margin-bottom: 0">
                                    <label for="bank_name">{{awtTrans('صورة الايصال')}}</label>
                                    <input type="file" class="form-control" name="image" placeholder="صورة الايصال">
                                </div>
                            </div>
                            <div class="form-item">
                                <label class="checkcontainer">
                                    <input type="radio" name="payment" value="online">
                                    <span class="radiobtn"></span>
                                    {{awtTrans('دفع اون لاين')}}
                                </label>
                                <img src="https://coder.aait.sa/El_Adawy/shopness/img/pay1.png" alt="" class="pay-img">
                            </div>
                        </div>
                        <button class="modal-submit btn-login" type="submit">{{awtTrans('تاكيد')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
@push('js')

    {!! $dataTable->scripts() !!}

    <script>
        $(function () {
            'use strict'
            $(document).on('click','#bank_show',function (event) {
                $('.bank-trans').removeClass('d-none');
            })
            $(document).on('click','.form-item:not(#bank_show)',function (event) {
                $('.bank-trans').addClass('d-none');
            })
            $('body').on('click','.singlepay',function () {
                var id = $(this).data('id');
                var price = $(this).data('price');
                $('[name=pay_id]').val(id);
                $('[name=price]').val(price);
            })
            $('body').on('click','.allpay',function () {
                $('[name=pay_id]').val('');
            })
        })
    </script>
@endpush
