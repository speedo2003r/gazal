@extends('provider.layouts.masterLogin')
@section('title',awtTrans('تسجيل الدخول'))
@section('content')


    <div class="account-pages mt-5 mb-5">

        <div class="container">

        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6 padd-0 d-flex align-items-center">



                <div class="form-content form-login w-100">

                    <div class="title_form-content text-center mb-20 f-b">

                        <h1 class="f-b">{{settings('site_name')}}</h1>

                    </div>

                    <form action="{{route('provider.activation')}}" id="formLogin" method="post">
                        @csrf
                        <div class="form-group po_R">
                            <input type="number" class="form-control" name="code" placeholder="{{awtTrans('الرجاء ادخال رمز التفعيل')}}">
                        </div>

                        <button type="submit" class="btn-main form-group"><i class="fa fa-spinner fa-spin spinner-ajax" style="display: none"></i>
                            {{awtTrans('تفعيل')}}</button>

                        <a href="{{route('provider.activeCode')}}" class="d-block text-center text-color">
                            {{awtTrans('اعادة ارسال كود التفعيل')}}
                        </a>
                    </form>


                </div>

            </div>

        </div>

        </div>
    </div>




@endsection
