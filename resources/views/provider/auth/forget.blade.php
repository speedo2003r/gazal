@extends('provider.layouts.masterLogin')
@section('title',awtTrans('استرجاع كلمة المرور'))
@section('content')

    <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card">
                        <div class="card-body p-4">
                            <div class="text-center mb-4">
                                <h4 class="text-uppercase mt-0"> {{awtTrans('استرجاع كلمة المرور')}}</h4>
                            </div>
                            <form action="{{route('provider.sendActiveCode')}}" id="formLogin" method="post">
                        @csrf
                        <div class="form-group po_R">
                            <input type="number" class="form-control" name="phone" placeholder="{{awtTrans('الرجاء ادخال رقم الجوال')}}">
                        </div>

                        <button type="submit" class="btn-main form-group"><i class="fa fa-spinner fa-spin spinner-ajax" style="display: none"></i>
                            {{awtTrans('تأكيد')}}</button>

                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
