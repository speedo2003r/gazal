@extends('provider.layouts.masterLogin')
@section('title',awtTrans('تسجيل الدخول'))
@section('content')
    @push('css')
        <style>
        body{
            font-family: 'Almarai', sans-serif;
        }
        .steps {
            display: flex;
            justify-content: center;
            align-items: center;
            margin-bottom: 20px;
            color: #fff
        }


        .steps > div {
            margin: 0px 10px;
            flex-shrink: 0;
        }

        .steps > div > span {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            border: 1px solid #b61c1c
        }

        .steps > div.active > span {
            background: #b61c1c
        }

        .steps aside {
            flex-basis: 100px;
            height: 1px;
            background: #b61c1c
        }


        .btn-file-reg {

            background: #b61c1c;
            color: #FFFFFF;
            padding: 10px 20px;
            position: relative;
            overflow: hidden;
            text-align: center;


        }


        .btn-file-reg > input {
            position: absolute;
            width: 100%;
            opacity: 0;
            height: 100%;
        }




    </style>
@endpush



<div class="account-pages mt-5 mb-5">

    <div class="container">

        <div class="steps">

            <div class="active">
                <span></span>
                {{awtTrans('البيانات الاساسية')}}
            </div>

            <aside></aside>

            <div class="active">
                <span></span>
                {{awtTrans('المستندات')}}
            </div>

        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ awtTrans('سجل معنا كتاجر لدينا') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('provider.register') }}" id="formLogin">
                            @csrf
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    {{awtTrans('صورة الهويه الوطنيه')}}
                                </label>

                                <div class="col-md-6">

                                    <div class="btn-file-reg">
                                        <input type="file" name="id_avatar"><span>{{awtTrans('استعراض الملفات')}}</span>
                                    </div>
                                    <div></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    {{awtTrans('حساب بنكي')}}
                                </label>

                                <div class="col-md-6">

                                    <div class="btn-file-reg">
                                        <input type="file" name="acc_bank"><span>{{awtTrans('استعراض الملفات')}}</span>
                                    </div>
                                    <div></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    {{awtTrans('سجل الشركة')}}
                                </label>

                                <div class="col-md-6">

                                    <div class="btn-file-reg">
                                        <input type="file" name="commercial_image"><span>{{awtTrans('استعراض الملفات')}}</span>
                                    </div>
                                    <div></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    {{awtTrans('الشهادة الضريبة')}}
                                </label>

                                <div class="col-md-6">

                                    <div class="btn-file-reg">
                                        <input type="file" name="tax_certificate"><span>{{awtTrans('استعراض الملفات')}}</span>
                                    </div>
                                    <div></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    {{awtTrans('رخصة البلدية')}}
                                </label>

                                <div class="col-md-6">

                                    <div class="btn-file-reg">
                                        <input type="file" name="municipal_license"><span>{{awtTrans('استعراض الملفات')}}</span>
                                    </div>
                                    <div></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    {{awtTrans('بانر')}}
                                </label>

                                <div class="col-md-6">

                                    <div class="btn-file-reg">
                                        <input type="file" name="banner"><span>{{awtTrans('استعراض الملفات')}}</span>
                                    </div>
                                    <div></div>
                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-4"></div>
                                <div class="offset-md-2 col-md-6">
                                    <button type="submit" onclick="register()" class="btn btn-primary btn-block login">
                                        <i class="fa fa-spinner fa-spin spinner-ajax" style="display: none"></i>
                                        {{ awtTrans('حفظ') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end page -->

    @push('js')
<script>

    $(".btn-file-reg > input").on("change", function () {

        $(this).parent(".btn-file-reg").next("div").text($(this).val().replace("C:\\fakepath\\", ""));

    });

</script>
<script>

    function register() {
        event.preventDefault();

        //disable the submitted button and show he spinner
        $('.login').prop('disabled', true).css({opacity: '0.5'});
        $('.spinner-ajax').css({display: 'inline-block'});

        $.ajax({
            type        : 'POST',
            url         : `{{route('provider.postRegister')}}` ,
            datatype    : 'json' ,
            processData : false,
            contentType : false,
            data        : new FormData($("#formLogin")[0]),
            success     : function(data){

                // undisable the submitted button and hide the spinner
                $('.login').removeAttr("disabled").css({opacity: '1'});
                $('.spinner-ajax').css({display: 'none'});

                if(data.value == '0'){
                    toastr.error(data.msg);
                }else{
                    location.assign(data.url);
                }

            }
        });
    }
</script>
    @endpush
@endsection
