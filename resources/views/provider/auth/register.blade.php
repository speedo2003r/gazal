@extends('provider.layouts.masterLogin')
@section('title',awtTrans('تسجيل الدخول'))
@section('content')
    @push('css')
    <style>
        body{
            font-family: 'Almarai', sans-serif;
        }
        .steps {
            display: flex;
            justify-content: center;
            align-items: center;
            margin-bottom: 20px;
            color: #fff
        }


        .steps > div {
            margin: 0px 10px;
            flex-shrink: 0;
        }

        .steps > div > span {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            border: 1px solid #b61c1c
        }

        .steps > div.active > span {
            background: #b61c1c
        }

        .steps aside {
            flex-basis: 100px;
            height: 1px;
            background: #b61c1c
        }



    </style>
    @endpush


<div class="account-pages mt-5 mb-5">

<div class="container">

    <div class="steps">

        <div class="active">
            <span></span>
            {{awtTrans('البيانات الاساسية')}}
        </div>

        <aside></aside>

        <div>
            <span></span>
            {{awtTrans('المستندات')}}
        </div>

    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ awtTrans('سجل معنا كتاجر لدينا') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('provider.register') }}" id="formLogin">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ awtTrans('الاسم الشخصي') }}</label>
                            <div class="col-md-6">
                                <input type="text" name="name" class="form-control" placeholder="{{awtTrans('الاسم الشخصي')}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="store_name" class="col-md-4 col-form-label text-md-right">{{ awtTrans('اسم متجرك') }}</label>
                            <div class="col-md-6">
                                <input type="text" name="store_name" class="form-control" placeholder="{{awtTrans('اسم متجرك')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ awtTrans('البريد الالكتروني') }}</label>
                            <div class="col-md-6">
                                <input type="email" name="email" class="form-control" placeholder="{{awtTrans('البريد الالكتروني')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ awtTrans('رقم الجوال') }}</label>
                            <div class="col-md-6">
                                <input type="number" name="phone" class="form-control" placeholder="{{awtTrans('رقم الجوال')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category_id" class="col-md-4 col-form-label text-md-right">{{ awtTrans('القسم') }}</label>
                            <div class="col-md-6">
                                <select name="category_id" class="form-control">
                                    <option value="" hidden selected>{{awtTrans('اختر القسم')}}</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category['id']}}">{{$category['title']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="country_id" class="col-md-4 col-form-label text-md-right">{{ awtTrans('الدوله') }}</label>
                            <div class="col-md-6">
                                <select name="country_id" class="form-control">
                                    <option value="" hidden selected>{{awtTrans('اختر الدوله')}}</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country['id']}}">{{$country['title']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city_id" class="col-md-4 col-form-label text-md-right">{{ awtTrans('المدينه') }}</label>
                            <div class="col-md-6">
                                <select name="city_id" class="form-control">
                                    <option value="" hidden selected>{{awtTrans('اختر المدينه')}}</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ awtTrans('كلمة المرور') }}</label>
                            <div class="col-md-6">
                                <input type="password" name="password" class="form-control" placeholder="{{awtTrans('كلمة المرور')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">{{ awtTrans('تأكيد كلمة المرور') }}</label>
                            <div class="col-md-6">
                                <input type="password" name="password_confirmation" class="form-control" placeholder="{{awtTrans('تأكيد كلمة المرور')}}">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-4"></div>
                            <div class="offset-md-2 col-md-6">
                                <button type="submit" onclick="check()" class="btn btn-primary btn-block login">
                                    <i class="fa fa-spinner fa-spin spinner-ajax" style="display: none"></i>
                                    {{ awtTrans('التالي') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
@push('js')
    <script>
        $('body').on('change','[name=country_id]',function () {
            var id = $(this).val();
            getCities(id);
        });

        function getCities(country_id,type = '',placeholder = 'اختر'){
            var html = '';
            var city_id = '';
            $('[name=city_id]').empty();
            if(country_id){
                $.ajax({
                    url: `{{route('admin.ajax.getCities')}}`,
                    type: 'post',
                    dataType: 'json',
                    data:{id: country_id},
                    success: function (res) {
                        if(type != ''){
                            city_id = type;
                        }
                        html += `<option value="" hidden selected>${placeholder}</option>`;
                        $.each(res.data,function (index,value) {
                            html += `<option value="${value.id}" ${city_id == value.id ? 'selected':'' }>${value.title.ar}</option>`;
                        });
                        $('[name=city_id]').append(html);
                    }
                });
            }
        }
        function check() {
            event.preventDefault();

            //disable the submitted button and show he spinner
            $('.login').prop('disabled', true).css({opacity: '0.5'});
            $('.spinner-ajax').css({display: 'inline-block'});

            $.ajax({
                type        : 'POST',
                url         : `{{route('provider.checkUser')}}` ,
                datatype    : 'json' ,
                processData : false,
                contentType : false,
                data        : new FormData($("#formLogin")[0]),
                success     : function(data){

                    // undisable the submitted button and hide the spinner
                    $('.login').removeAttr("disabled").css({opacity: '1'});
                    $('.spinner-ajax').css({display: 'none'});

                    if(data.value == '0'){
                        toastr.error(data.msg);
                    }else{
                        location.assign(data.url);
                    }

                }
            });
        }
    </script>
@endpush
