@extends('provider.layouts.masterLogin')
@section('title',awtTrans('تسجيل الدخول'))
@section('content')


    <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card">

                        <div class="card-body p-4">

                            <div class="text-center mb-4">
                                <h4 class="text-uppercase mt-0">تسجيل الدخول</h4>
                            </div>

                            <form method="POST" action="{{ route('provider.login') }}">
                                @csrf

                                <div class="form-group mb-3">
                                    <label for="emailaddress">رقم الجوال أو البريد الالكتروني</label>
                                    <input id="emailaddress" type="text" class="form-control @error('userName') is-invalid @enderror" name="userName" value="{{ old('userName') }}" placeholder="ادخل اسم المستخدم" autocomplete="userName" autofocus>
                                </div>

                                <div class="form-group mb-3">
                                    <label for="password">كلمة المرور</label>
                                    <input id="password" type="password" class="form-control  @error('password') is-invalid @enderror" placeholder="كلمه المرور" name="password" autocomplete="current-password">
                                </div>

                                <div class="form-group mb-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="remember" class="custom-control-input" id="checkbox-signin" checked>
                                        <label class="custom-control-label" for="checkbox-signin">تذكرني</label>
                                    </div>
                                </div>

                                <div class="form-group mb-0 text-center">
                                    <button class="btn btn-primary btn-block" type="submit"> تسجيل الدخول </button>
                                </div>

                            </form>

                        </div> <!-- end card-body -->
                    </div>
                    <!-- end card -->

                    <div class="row mt-3">
                        <div class="col-12 text-center">
                            <p> <a href="{{route('provider.forget')}}" class="text-muted ml-1"><i class="fa fa-lock mr-1"></i>هل نسيت كلمة المرور؟?</a></p>
                            <p class="text-muted">لا تملك حساب انضم معنا من هنا? <a href="{{route('provider.register')}}" class="text-dark ml-1"><b>تسجيل جديد</b></a></p>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->

                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end page -->
@endsection
