@extends('provider.layouts.masterLogin')
@section('title',awtTrans('اعادة تعيين كلمة المرور'))
@section('content')

    <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card">
                        <div class="card-body p-4">
                            <div class="text-center mb-4">
                                <h4 class="text-uppercase mt-0"> {{awtTrans('استرجاع كلمة المرور')}}</h4>
                            </div>
                                <form action="{{route('provider.resetPassword')}}" id="formLogin" method="post">
                        @csrf
                        <div class="form-group po_R">
                            <input type="number" class="form-control" name="code" placeholder="{{awtTrans('كود التفعيل')}}">
                        </div>
                        <div class="form-group po_R">
                            <input type="password" class="form-control" name="password" placeholder="{{awtTrans('الرجاء ادخال كلمة المرور الجديده')}}">
                        </div>
                        <div class="form-group po_R">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="{{awtTrans('الرجاء اعادة ادخال كلمة المرور الجديده')}}">
                        </div>

                        <button type="submit" class="btn-main form-group"><i class="fa fa-spinner fa-spin spinner-ajax" style="display: none"></i>
                            {{awtTrans('تأكيد')}}</button>

                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
