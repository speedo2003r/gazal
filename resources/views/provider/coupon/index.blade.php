@extends('provider.layouts.master')
@section('title',awtTrans('كوبونات'))
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">الفروع</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">الرئيسيه</a></li>
                        <li class="breadcrumb-item active">الفروع</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <button type="button" data-toggle="modal" data-target="#exampleModal4" class="btn btn-lighten-primary waves-effect waves-primary  width-md add-user">{{awtTrans('إضافة كوبون')}}</button>
                <table class="table dt-responsive nowrap dataTable no-footer dtr-inline collapsed" id="datatable-table" style="width:100%;">

            <thead>
            <tr>
                <th>{{awtTrans('الكوبون')}}</th>
                <th>{{awtTrans('نوع الخصم')}}</th>
                <th>{{awtTrans('قيمة الخصم')}}</th>
                <th>{{awtTrans('تاريخ البداية')}}</th>
                <th>{{awtTrans('تاريخ النهاية')}}</th>
                <th>{{awtTrans('الكمية')}}</th>
                <th>{{awtTrans('تعديل')}}</th>
            </tr>

            </thead>
            <tbody>
            @foreach($coupons as $coupon)
            <tr>
                <td class="d-flex align-items-center flex-wrap justify-content-center">
                    <span class="m-3">{{$coupon['code']}}</span>
                </td>
                <td>{{$coupon['value']}}</td>
                <td>{{$coupon['kind'] == 'percent' ? 'نسبه' : 'قيمه'}}</td>
                <td>{{$coupon['start_date']}}</td>
                <td>{{$coupon['end_date']}}</td>
                <td>{{$coupon['count']}}</td>
                <td>
                    <button type="button" data-toggle="modal" data-target="#exampleModal4" onclick="edit({{$coupon}})" class="btn btn-icon waves-effect waves-light btn-success">
                        <i class="fas fa-edit"></i>
                    </button>
                    <a href="javascript:void(0)" class="btn btn-icon waves-effect waves-light btn-danger" onclick="confirmDelete('{{route('provider.coupon.destroy',$coupon['id'])}}')" data-toggle="modal" data-target="#delete-model">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal4" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{awtTrans('اضافة كوبون')}}</h5>
                </div>
                <form action="{{route('provider.coupon.store')}}" id="editForm" method="post">
                    @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{awtTrans('رمز الكوبون')}}</label>
                        <input type="text" placeholder="{{awtTrans('رمز الكوبون')}}" name="code" id="code" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>{{awtTrans('تحديد نوع الكوبون')}}</label>
                        <select name="kind" id="kind" class="form-control">
                            <option value="" selected hidden>{{awtTrans('اختر')}}</option>
                            <option value="percent">{{awtTrans('نسبه')}}</option>
                            <option value="fixed">{{awtTrans('قيمه')}}</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>{{awtTrans('القيمه')}}</label>
                        <input type="text" name="value" id="value" placeholder="{{awtTrans('قيمة الخصم')}}" class="form-control">
                    </div>



                    <div class="row">

                        <div class="form-group col-md-6">
                            <label>{{awtTrans('تاريخ البداية')}}</label>
                            <input type="date" max="9999-12-31" name="start_date" id="start_date" placeholder="{{awtTrans('تاريخ البداية')}}" class="form-control">
                        </div>



                        <div class="form-group col-md-6">
                            <label>{{awtTrans('تاريخ النهاية')}}</label>
                            <input type="date" max="9999-12-31" name="end_date" id="end_date" placeholder="{{awtTrans('تاريخ النهاية')}}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>{{awtTrans('عدد الاستخدامات')}}</label>
                        <input type="text" name="count" id="count" placeholder="{{awtTrans('عدد الاستخدامات')}}" class="form-control">
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success save">إرسال</button>
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@push('js')
    <script>
        $(function (){
            'use strict'
            $('body').on('click','.add-user',function (){
                $('#editForm').attr('action',"{{route('provider.coupon.store')}}")
                $('.modal-title').html('{{awtTrans('اضافة كوبون')}}')
                $('#editForm :input:not([type=checkbox],[type=radio],[type=hidden])').val('');
            });
        });
        function edit(ob){
            $('#editForm').attr('action',"{{route('provider.coupon.update','obId')}}".replace('obId',ob.id))
            $('.modal-title').html('{{awtTrans('تعديل كوبون')}}')
            $('#code').val(ob.code);
            $('#value').val(ob.value);
            $('#kind').val(ob.kind).change;
            $('#count').val(ob.count);
            $('#start_date').val(ob.start_date).change;
            $('#end_date').val(ob.end_date).change;
        }
    </script>
@endpush
