@extends('provider.layout.master')
@section('title',awtTrans('طلبات ارجاع'))
@section('content')


    <div class="table-content reques_table po_R">

        <div class="table-content reques_table po_R">

            <div class="title_table"><span>{{awtTrans('الطلبات')}} </span> / <span class="main-color">{{awtTrans('طلبات الارجاع')}}</span></div>

            <table class="table dt-responsive mytable table-return" style="width:100%;">

                <thead>
                <tr>
                    <th>{{awtTrans('رقم الطلب')}}</th>
                    <th>{{awtTrans('اسم العميل')}}</th>
                    <th>{{awtTrans('تاريخ الطلب')}}</th>
{{--                    <th>{{awtTrans('الاجمالي')}}</th>--}}
                    <th>{{awtTrans('التفاصيل')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                <tr>
                    <td class="d-flex align-items-center flex-wrap justify-content-center">
                        <input type="checkbox">
                        <span class="m-3">{{$order['order_num']}}</span>
                    </td>
                    <td>{{$order->user['full_name']}}</td>
                    <td>{{$order->created_at->format('Y-m-d')}}</td>
{{--                    <td class="main-color">{{$order->reclaims()->sum('price') * $order->reclaims()->sum('count')}} {{awtTrans('ر.س')}}</td>--}}
                    <td><a href="{{route('seller.returnDetails',$order)}}" class="main-color">{{awtTrans('عرض التفاصيل')}}</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div>

    </div>



@endsection
