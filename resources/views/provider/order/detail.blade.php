@extends('provider.layout.master')
@section('title',awtTrans('طلبات ارجاع'))
@section('content')


    <div class="mt-10 pag-requ">


        <div>
            <span>{{awtTrans('الطلبات')}}</span> /
            <span>{{awtTrans('طلبات الارجاع')}}  </span> /
            <span class="main-color">{{awtTrans('تفاصيل الطلب')}}</span>
        </div>

        <div class="title-dash">
            <h5>{{awtTrans('ارجاع الطلبية رقم')}} {{$order['order_num']}}</h5>
        </div>


{{--        <div class="mt-20">--}}

{{--            <div class="row">--}}
{{--                @foreach($order->reclaims as $reclaim)--}}
{{--                    <div class="col-md-6">--}}

{{--                        <div class="box-requ d-flex align-items-center">--}}

{{--                            <img src="{{$reclaim->orderProduct->group->group_image()}}">--}}

{{--                            <div>--}}
{{--                                <p>{{$reclaim->orderProduct->item['type'] == 'single' ? $reclaim->orderProduct->item['title'] : $reclaim->orderProduct->group['title']}}</p>--}}

{{--                                <ul>--}}
{{--                                    <li>{{awtTrans('الكمية')}} : <span>{{$reclaim->orderProduct['qty']}}</span></li>--}}
{{--                                    @if($reclaim->orderProduct->item['type'] == 'single')--}}
{{--                                        @foreach($reclaim->orderProduct->item->features as $child)--}}
{{--                                            <li>{{$child->feature['title']}} : <span>{{$child['title']}}</span></li>--}}
{{--                                        @endforeach--}}
{{--                                    @else--}}
{{--                                        @foreach($reclaim->orderProduct->group->children as $child)--}}
{{--                                            <li>{{\App\Entities\Feature::find($child['feature_id'])['title']}} : <span>{{$child['title']}}</span></li>--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}
{{--                                </ul>--}}

{{--                            </div>--}}

{{--                        </div>--}}

{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}

{{--        </div>--}}
        <div class="form-group">

            <label>{{awtTrans('سبب الارجاع')}}</label>

            <textarea readonly placeholder="{{awtTrans('سبب الارجاع')}}" class="form-control details_R_T">
                {{$order->reclaims()->first()['notes']}}
            </textarea>

        </div>

        @if($order->reclaims()->first()['approve'] == null)
        <div class="d-flex justify-content-center flex-wrap">

            <button type="button" class="btn-main btn-store" data-target="#store-model" data-toggle="modal">{{awtTrans('قبول الطلب')}}</button>
            <button type="button" class="btn-main text-back  btn-dis-store" data-target="#disapprove-store-model" data-toggle="modal">{{awtTrans('رفض الطلب')}}</button>

        </div>
            @elseif($order->reclaims()->first()['approve'] == 1)
            <div class="d-flex justify-content-center flex-wrap">
                {{awtTrans('تم الموافقه علي الطلب')}}
            </div>
            @elseif($order->reclaims()->first()['approve'] == 2)

            <div class="d-flex justify-content-center flex-wrap">
                {{awtTrans('تم رفض الطلب')}}
            </div>
        @endif
    </div>



    <div class="modal fade" id="store-model">
        <div class="modal-dialog model-sm">
            <div class="modal-content">
                <form action="{{route('seller.postReturnOrder')}}" method="post">
                    @csrf
                    <input type="hidden" name="order_id" value="{{$order['id']}}">
                    <input type="hidden" name="type" value="1">
                    <div class="modal-header"><h6 class="modal-title">{{awtTrans('تأكيد الطلب')}}</h6></div>
                    <div class="modal-body p-5"><h5 class="text-center">{{awtTrans('هل انت متاكد من عملية اعادة الطلب ؟')}}</h5></div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{awtTrans('الغاء')}}</button>
                        <button type="submit" class="btn btn-danger" style="background: #b62e28 !important;">{{awtTrans('تأكيد')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="disapprove-store-model">
        <div class="modal-dialog model-sm">
            <div class="modal-content">
                <form action="{{route('seller.postReturnOrder')}}" method="post">
                    @csrf
                    <input type="hidden" name="order_id" value="{{$order['id']}}">
                    <input type="hidden" name="type" value="2">
                    <div class="modal-header"><h6 class="modal-title">{{awtTrans('تأكيد رفض الطلب')}}</h6></div>
                    <div class="modal-body p-5"><h5 class="text-center">{{awtTrans('هل انت متاكد من عملية رفض اعادة الطلب ؟')}}</h5></div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{awtTrans('الغاء')}}</button>
                        <button type="submit" class="btn btn-danger" style="background: #b62e28 !important;">{{awtTrans('تأكيد')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
