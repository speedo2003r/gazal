@extends('provider.layouts.master')
@section('title',awtTrans('الطلبات الجديده'))
@section('content')


    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">{{awtTrans('الطلبات')}}</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">الرئيسيه</a></li>
                        <li class="breadcrumb-item ">{{awtTrans('الطلبات')}}</li>
                        <li class="breadcrumb-item active">{{awtTrans('تفاصيل الطلب')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <div class="mt-10 pag-requ">


                    <div class="title-dash">
                        <h5>{{awtTrans('تفاصيل الطلبية رقم')}} {{$order['order_num']}}</h5>
                    </div>

                    <div class="" style="text-align: left">
                        @if($order->order_status == \App\Entities\Order::STATUS_WAITING)
                            <button class="btn btn-success change-status" data-status="{{$order->order_status}}" data-type="accept">{{__('Accept')}}</button>
                            <button class="btn btn-danger change-status" data-status="{{$order->order_status}}" data-type="refuse">{{__('Refuse')}}</button>
                        @endif
                    </div>

                    <div class="stips_requ">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="item active">
                                    <aside></aside>
                                    <div>
                                        <i class="fa fa-check-circle" aria-hidden="true"></i> <span>{{ __('order.'.$order->order_status) }}</span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>


                    <div class="mt-20">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped">
                                    <thead>
                                    <th>#</th>
                                    <th>{{awtTrans('صورة المنتج')}}</th>
                                    <th>{{awtTrans('اسم المنتج')}}</th>
                                    <th>{{awtTrans('الكمية')}}</th>
                                    <th>{{awtTrans('السعر')}}</th>
                                    </thead>
                                    <tbody>
                                    @foreach($order->orderProducts as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td> <img src="{{ $item->item['main_image'] ?? '' }}" height="60" width="60"></td>
                                            <td>{{$item->item['title'] ?? ''}}</td>
                                            <td>{{$item->qty}}</td>
                                            <td>{{$item->price}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>

                    <div>

                        <div class="title-dash">
                            <h5 class="text-color">{{awtTrans('ملخص الطلبية')}}</h5>
                        </div>


                        <div class="row">

                            <div class="col-md-6">
                                <div class="d-flex justify-content-between mb-20">
                                    <aside>{{awtTrans('المجموع الفرعي')}}</aside>
                                    <aside class="main-color">{{$order['final_total']}} {{awtTrans('ر.س')}}</aside>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="d-flex justify-content-between mb-20">
                                    <aside>{{awtTrans('ضريبة القيمة المضافة')}}</aside>
                                    <aside class="main-color">{{$order['vat_amount']}} {{awtTrans('ر.س')}}</aside>
                                </div>
                            </div>
                            @if($order['coupon_amount'] > 0)
                                <div class="col-md-4">
                                    <div class="d-flex justify-content-between mb-20">
                                        <aside>{{awtTrans('رسوم الشحن')}}</aside>
                                        <aside class="main-color">{{$order['shipping_price']}} {{awtTrans('ر.س')}}</aside>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="d-flex justify-content-between mb-20">
                                        <aside>{{awtTrans('قيمة خصم الكوبون')}}</aside>
                                        <aside class="main-color">{{$order['coupon_amount']}} {{awtTrans('ر.س')}}</aside>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="d-flex justify-content-between mb-20">
                                        <aside>{{awtTrans('الاجمالي')}}</aside>
                                        <aside class="main-color">
                                            {{$order->_price()}} {{awtTrans('ر.س')}}
                                            @if($order['cash_pay'] > 0)
                                                <br>
                                                <small>{{awtTrans('عمولة الدفع عند الاستلام')}} : {{$order['cash_pay']}} {{awtTrans('ر.س')}}</small>
                                            @endif
                                        </aside>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-6">
                                    <div class="d-flex justify-content-between mb-20">
                                        <aside>{{awtTrans('رسوم الشحن')}}</aside>
                                        <aside class="main-color">{{$order['shipping_price']}} {{awtTrans('ر.س')}}</aside>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="d-flex justify-content-between mb-20">
                                        <aside>{{awtTrans('الاجمالي')}}</aside>
                                        <aside class="main-color">
                                            {{$order->_price()}} {{awtTrans('ر.س')}}</aside>

                                        @if($order['cash_pay'] > 0)
                                            <br>
                                            <small> عمولة الدفع عند الاستلام{{$order['cash_pay']}} {{awtTrans('ر.س')}}</small>
                                        @endif
                                    </div>
                                </div>
                            @endif


                        </div>

                        <hr />

                    </div>


                    <div>

                        <div class="title-dash">
                            <h5>{{awtTrans('بيانات العميل')}}</h5>
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="d-flex justify-content-between mb-20">
                                    <aside>{{awtTrans('اسم العميل')}}</aside>
                                    <aside class="main-color">{{$order->user['name']}}</aside>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="d-flex justify-content-between mb-20">
                                    <aside>{{ awtTrans('رقم الجوال') }}</aside>
                                    <aside class="main-color">{{$order->address['phone'] ?? $order->user['phone']}}</aside>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="d-flex justify-content-between mb-20">
                                    <aside>{{awtTrans('العنوان')}}</aside>
                                    <aside class="main-color">
                                        @if($order->address)
                                            {{$order->address['map_desc']}} - {{$order->address['address_details']}}
                                        @endif
                                    </aside>
                                </div>

                            </div>


                            <div class="col-md-6">
                                <div class="d-flex justify-content-between mb-20">
                                    <aside>{{awtTrans('طريقة الدفع')}}</aside>
                                    <aside class="main-color">{{$order['pay_type'] == 'cash' ? awtTrans('الدفع عند الاستلام') : awtTrans('أونلاين')}}</aside>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex justify-content-between mb-20">
                                    <aside>{{awtTrans('وسيلة الشحن')}}</aside>
                                    <aside class="main-color"></aside>
                                </div>
                            </div>
                        </div>
                        @if($order['delegate_id'] != null)
                            <div class="title-dash">
                                <h5>{{awtTrans('بيانات المندوب')}}</h5>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-20">
                                        <label>{{awtTrans('اسم المندوب')}}</label>
                                        <aside class="p-2 div-back">{{$order->delegate['full_name']}}</aside>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="mb-20">
                                        <label>{{awtTrans('رقم الجوال')}}</label>
                                        <aside class="p-2 div-back">{{$order->delegate['phone']}}</aside>
                                    </div>
                                </div>
                            </div>
                        @endif


                    </div>


                    @if($order['status'] == 'new')
                        <div class="d-flex justify-content-center flex-wrap">

                            <form action="{{route('provider.orderAccept')}}" method="post">
                                @csrf
                                <input type="hidden" name="order_id" value="{{$order['id']}}">
                                <button type="submit" class="btn-main">{{awtTrans('قبول الطلب')}}</button>
                            </form>
                            <button type="button" class="btn-main text-back" data-toggle="modal" data-target="#exampleModal2">{{awtTrans('رفض الطلب')}}</button>

                        </div>
                    @endif
                    @if($order['status'] == 'pending')
                        <div class="d-flex justify-content-center flex-wrap">
                            <form action="{{route('provider.orderOnWay')}}" method="post">
                                @csrf
                                <input type="hidden" name="order_id" value="{{$order['id']}}">
                                <button class="btn-main">تأكيد التسليم للمندوب</button>
                            </form>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>





    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{awtTrans('اختيار نوع سيارة النقل')}}</h5>
                </div>
                <form action="{{--route('seller.changeCarType')--}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="order_id" value="{{$order['id']}}">
                            <select class="form-control select_model" name="car_type_id">
                                <option selected hidden>{{awtTrans('نوع السياره')}}</option>
{{--                                @foreach($cars as $car)--}}
{{--                                    <option value="{{$car['id']}}" @if($order['car_type_id'] == $car['id']) selected @endif>{{$car['title']}}</option>--}}
{{--                                @endforeach--}}
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn-main">{{awtTrans('تأكيد')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{awtTrans('سبب الرفض')}}</h5>
                </div>
                <form action="{{--route('seller.orderRefuse')--}}" method="post">
                    @csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <input type="hidden" name="order_id" value="{{$order['id']}}">
                            <textarea placeholder="{{awtTrans('سبب الرفض')}}" name="notes" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn-main">{{awtTrans('تأكيد')}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


@endsection

@push('js')
    <script>
        $(document).on('click', '.change-status', function () {
            let status = parseInt($(this).data('status'));

            switch (status) {
                case 0:
                    let type = $(this).data('type');
                    if (type == 'accept') {
                        Swal.fire({
                            title: "{{__('Are you sure?')}}",
                            text: '{{__("You won't be able to revert this!")}}',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: "{{__('Yes, change it!')}}"
                        }).then((result) => {
                            console.log('Result', result)
                            if (result.value) {
                                $.ajax({
                                    url: "{{route('provider.orderAccept')}}",
                                    type: 'post',
                                    data:{order_id: {{$order->id}} },
                                    datatype: 'json',
                                    success(response) {
                                        Swal.fire(
                                            '{{__("Changed!")}}',
                                            '{{__("Your order status has been changed.")}}',
                                            '{{__("Success")}}'
                                        )
                                        setTimeout(() => {
                                            window.location.reload();
                                        }, 1200)
                                    },
                                    error(error) {
                                        console.log('error', error)
                                    }
                                })

                            }
                        });
                    } else {
                        const myinput = Swal.fire({
                            input: 'textarea',
                            inputLabel: '{{__("Reason")}}',
                            inputPlaceholder: "{{__('Type your reasons here...')}}",
                            inputAttributes: {
                                'aria-label': 'Type your message here'
                            },
                            showCancelButton: true
                        }).then((result) => {
                            if (result.value) {
                                $.ajax({
                                    url: "{{route('provider.orderRefuse')}}",
                                    type: 'post',
                                    data:{order_id: {{$order->id}} , 'notes': result.value },
                                    datatype: 'json',
                                    success(response) {
                                        Swal.fire(
                                            '{{__("Refused!")}}',
                                            '{{__("Your order status has been changed.")}}',
                                            '{{__("Success")}}'
                                        )
                                        setTimeout(() => {
                                            window.location.reload();
                                        }, 1200)
                                    },
                                    error(error) {
                                        console.log('error', error)
                                    }
                                })

                            }
                        })


                    }
                     break;
            }

        })
    </script>
@endpush
