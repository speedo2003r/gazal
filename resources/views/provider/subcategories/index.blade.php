@extends('provider.layouts.master')
@section('title',awtTrans('الأقسام الفرعيه'))
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">{{awtTrans('الأقسام الفرعيه')}}</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">الرئيسيه</a></li>
                        <li class="breadcrumb-item active">{{awtTrans('الأقسام الفرعيه')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <button type="button" data-toggle="modal" data-target="#exampleModal4" class="btn btn-lighten-primary waves-effect waves-primary  width-md add-user">{{awtTrans('إضافة قسم')}}</button>
                <table class="table dt-responsive nowrap dataTable no-footer dtr-inline collapsed" id="datatable-table" style="width:100%;">

            <thead>
            <tr>
                <th>#</th>
                <th>{{__('Category')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>

            </thead>
            <tbody>
            @foreach($items as $item)
            <tr>

                <td>{{ $loop->iteration }}</td>
                <td>{{ $item->title }}</td>
                <td>
                    @if($item->pivot->is_active)
                        <button class="btn btn-success active-record" data-id="{{ $item->id }}" data-active="0">{{__('Active')}}</button>
                    @else
                        <button class="btn btn-danger active-record" data-id="{{ $item->id }}" data-active="1">{{__('Detective')}}</button>
                    @endif
                </td>
                <td>
                    <a href="javascript:void(0)" class="btn btn-icon waves-effect waves-light btn-danger" onclick="confirmDelete('{{route('provider.subcategories.destroy',$item['id'])}}')" data-toggle="modal" data-target="#delete-model">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal4" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{awtTrans('اضافة قسم')}}</h5>
                </div>
                <form action="{{route('provider.subcategories.store')}}" id="editForm" method="post">
                    @csrf
                <div class="modal-body">

                    <div class="form-group">
                        <label>{{awtTrans('القسم الرئيسي')}}</label>
                        <select name="categories" id="kind" class="form-control categories" required>
                            <option value="">{{__('Choose')}}</option>
                            @foreach(\App\Entities\Category::whereNull('parent_id')->whereNotIn('id', $items->pluck('id')->toArray())->get() as $category)
                            <option value="{{$category->id}}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>{{awtTrans('الأقسام')}}</label>
                        <select name="subcategories[]" id="kind" class="form-control subcategories" multiple required>

                        </select>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success save">إرسال</button>
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@push('js')
    <script>
        $(document).on('click', '.active-record', function () {
            let id = $(this).data('id');
            let is_active = $(this).data('active');

            Swal.fire({
                title: "{{__('Are you sure?')}}",
                text: '{{__("You won't be able to revert this!")}}',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: "{{__('Yes, change it!')}}"
            }).then((result) => {

                if (result.value) {
                    $.ajax({
                        url: "{{url('provider/subcategories')}}/"+id,
                        type: 'put',
                        data:{is_active},
                        datatype: 'json',
                        success(response) {
                            Swal.fire(
                                '{{__("Changed!")}}',
                                '{{__("Your order status has been changed.")}}',
                                '{{__("Success")}}'
                            )
                            setTimeout(() => {
                                window.location.reload();
                            }, 1200)
                        },
                        error(error) {
                            console.log('error', error)
                        }
                    })

                }
            });
        })

        $(document).on('change', '.categories', function () {
            let id = $(this).val();
            let subcategories = $('.subcategories');
            subcategories.empty();

            $.ajax({
                url: "{{url('provider/subcategories')}}/"+id,
                success(response) {
                    response.forEach(subcategory => {
                        subcategories.append(`<option value="${subcategory.id}">${subcategory.title.ar}</option>`)
                    })
                },
                error(error) {
                    console.log('error', error)
                }
            })
        })

    </script>
@endpush
