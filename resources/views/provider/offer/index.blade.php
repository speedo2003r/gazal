@extends('provider.layout.master')
@section('title',awtTrans('العروض'))
@section('content')
@push('css')
    <style>
        div.dataTables_wrapper div.dataTables_filter label:after, div.dataTables_wrapper div.dataTables_length label:after{
            height: 50px;
            width: 45px;
            bottom: 0;
            top: auto;
        }
    </style>
@endpush







    <div class="table-content">
        {!! $dataTable->table([
         'class' => "table dt-responsive w-100",
         'id' => "itemdatatable-table",
         ],true) !!}

    </div>



<div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{awtTrans('اضافة عرض')}}</h5>
            </div>
            <form action="{{route('seller.postOffers')}}" method="post">
                @csrf
                <input type="hidden" name="item_id" id="item_id" class="form-control">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{awtTrans('الخصم بالنسبه')}}</label>
                            <input type="number" max="99" id="discount_price" name="discount_price" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{awtTrans('من')}}</label>
                            <input type="date" id="from" name="from" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{awtTrans('الي')}}</label>
                            <input type="date" id="to" name="to" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn-main">{{awtTrans('تأكيد')}}</button>
            </div>
            </form>
        </div>
    </div>
</div>



@endsection
@push('js')

    {!! $dataTable->scripts() !!}
    <script>
        function edit(ob){
            $('#discount_price').val(ob.discount_price);
            $('#from').val(ob.from);
            $('#to').val(ob.to);
            $('#item_id').val(ob.id);
        }
    </script>
@endpush
