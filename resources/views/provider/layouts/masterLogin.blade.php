<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="/assets/images/favicon.ico">

    <!-- App css -->
    <link href="{{dashboard_url('provider_assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{dashboard_url('provider_assets/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Toastr -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Almarai:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{dashboard_url('provider_assets/assets/toastr/toastr.min.css')}}">
    <link href="{{dashboard_url('provider_assets/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{dashboard_url('provider_assets/assets/css/app-rtl.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        body{
            font-family: 'Almarai', sans-serif;
        }
        .text-color {
            color: #fff !important;
        }

        .btn-main {
            display: block;
            background: #b61c1c;
            color: #fff;
            width: 300px;
            max-width: 100%;
            padding: 0;
            border-radius: 2px;
            text-align: center;
            text-shadow: 0 0;
            font-weight: 600;
            margin: 20px auto;
            height: 50px;
            border: 0px;
            line-height: 50px;
        }
    </style>
</head>

<body class="authentication-bg">

@yield('content')

<!-- Vendor js -->
<script src="{{dashboard_url('provider_assets/assets/js/vendor.min.js')}}"></script>
<!-- Toastr -->
<script src="{{dashboard_url('provider_assets/assets/toastr/toastr.min.js')}}"></script>

<!-- App js-->
<script src="{{dashboard_url('provider_assets/assets/js/app.min.js')}}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@include('admin.partial.alert')
@include('admin.partial.confirm_delete')
@stack('js')
@stack('css')
</body>
</html>
