<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{provider_dashboard_path()}}/assets/js/jquery.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/js/bootstrap.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/js/detect.js"></script>
<script src="{{provider_dashboard_path()}}/assets/js/fastclick.js"></script>
<script src="{{provider_dashboard_path()}}/assets/js/jquery.slimscroll.js"></script>
<script src="{{provider_dashboard_path()}}/assets/js/jquery.blockUI.js"></script>
<script src="{{provider_dashboard_path()}}/assets/js/waves.js"></script>
<script src="{{provider_dashboard_path()}}/assets/js/jquery.nicescroll.js"></script>
<script src="{{provider_dashboard_path()}}/assets/js/jquery.scrollTo.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/plugins/select2/dist/js/select2.js"></script>

<!-- KNOB JS -->
<!--[if IE]>
<script type="text/javascript" src="{{provider_dashboard_path()}}/assets/plugins/jquery-knob/excanvas.js"></script>
<![endif]-->
<script src="{{provider_dashboard_path()}}/assets/plugins/jquery-knob/jquery.knob.js"></script>

<!--Morris Chart-->
<script src="{{provider_dashboard_path()}}/assets/plugins/morris/morris.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/plugins/raphael/raphael-min.js"></script>

<!-- Dashboard init -->
<script src="{{provider_dashboard_path()}}/assets/pages/jquery.dashboard.js"></script>

<!-- App js -->
<script src="{{provider_dashboard_path()}}/assets/js/jquery.core.js"></script>
<script src="{{provider_dashboard_path()}}/assets/js/jquery.app.js"></script>

<!-- third party js -->
<!-- Toastr -->
<script src="{{provider_dashboard_path()}}/assets/toastr/toastr.min.js"></script>
<!-- Validation js (Parsleyjs) -->
<script src="{{provider_dashboard_path()}}/assets/libs/parsleyjs/parsley.min.js "></script>

<!-- validation init -->
<script src="{{provider_dashboard_path()}}/assets/js/pages/form-validation.init.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/buttons.flash.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/buttons.print.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/dataTables.keyTable.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/dataTables.select.min.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/datatables/buttons.server-side.js"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="{{dashboard_url('dashboard/assets/js/filepond.min.js')}}"></script>
<script src="{{provider_dashboard_path()}}/assets/libs/pdfmake/vfs_fonts.js"></script>
<!-- third party js ends -->

<!-- Sweet Alert js-->


<!-- Datatables init -->
<script src="{{provider_dashboard_path()}}/assets/js/pages/datatables.init.js"></script>
<!-- App js-->
<script src="{{provider_dashboard_path()}}/assets/js/app.js"></script>
<script src="{{dashboard_path()}}/sweet_alert/sweetalert2.all.min.js"></script>
<script src="{{asset('dashboard/assets/plugins/parsleyjs/dist/parsley.min.js')}}"></script>
@if(app()->getLocale() == 'ar')
<script src="{{asset('dashboard/assets/plugins/parsleyjs/src/i18n/ar.js')}}"></script>
@endif

<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form').parsley()


    function getCities(country_id, type = '', placeholder = 'اختر') {
        var html = '';
        var city_id = '';
        $('[name=city_id]').empty();
        if (country_id) {
            $.ajax({
                url: `{{route('admin.ajax.getCities')}}`,
                type: 'post',
                dataType: 'json',
                data: {id: country_id},
                success: function (res) {
                    if (type != '') {
                        city_id = type;
                    }
                    html += `<option value="">${placeholder}</option>`;
                    $.each(res.data, function (index, value) {
                        html += `<option value="${value.id}" ${city_id == value.id ? 'selected' : ''}>${value.title.ar}</option>`;
                    });
                    $('[name=city_id]').append(html);
                }
            });
        }
    }

    function confirmDelete(url) {
        $('#confirm-delete-form').attr('action', url);
    }

    $(function () {
        'use strict'
        var a = $("#datatable-table").DataTable({
            dom: 'Blfrtip',
            pageLength: 10,
            lengthMenu: [
                [10, 25, 50, 100], [10, 25, 50, 100]
            ],
            buttons: [
                {
                    extend: 'excel',
                    text: 'ملف Excel',
                    className: "btn btn-success"

                },
                {
                    extend: 'excel',
                    text: 'ملف Excel',
                    className: "btn btn-info"
                },
                {
                    extend: 'copy',
                    text: 'نسخ',
                    className: "btn btn-inverse"
                },
                {
                    extend: 'print',
                    text: 'طباعه',
                    className: "btn btn-success"
                },
            ],

            "language": {
                "sEmptyTable": "ليست هناك بيانات متاحة في الجدول",
                "sLoadingRecords": "جارٍ التحميل...",
                "sProcessing": "جارٍ التحميل...",
                "sLengthMenu": "أظهر _MENU_ مدخلات",
                "sZeroRecords": "لم يعثر على أية سجلات",
                "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                "sInfoPostFix": "",
                "sSearch": "ابحث:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "الأول",
                    "sPrevious": "السابق",
                    "sNext": "التالي",
                    "sLast": "الأخير"
                },
                "oAria": {
                    "sSortAscending": ": تفعيل لترتيب العمود تصاعدياً",
                    "sSortDescending": ": تفعيل لترتيب العمود تنازلياً"
                }
            }
        });
        a.buttons().container().appendTo("#datatable-table_wrapper .col-md-6:eq() ")


    });

    $(document).ready(function () {

        "use strict";

        // // ADD IMAGE

        $("#image").change(function (event) {
            $(this).parents('.images-upload-blockS').append('<div class="uploaded-block"><img src="' + URL.createObjectURL(event.target.files[0]) + '"><label class="close"><i class="fa fa-times-circle" aria-hidden="true"></i></label></div>');
        });

        $("#logo").change(function (event) {
            $(this).parents('.images-upload-blockS').append('<div class="uploaded-block"><img src="' + URL.createObjectURL(event.target.files[0]) + '"><label class="close"><i class="fa fa-times-circle" aria-hidden="true"></i></label></div>');
        });

        $("#banner").change(function (event) {
            $(this).parents('.images-upload-blockS').append('<div class="uploaded-block"><img src="' + URL.createObjectURL(event.target.files[0]) + '"><label class="close"><i class="fa fa-times-circle" aria-hidden="true"></i></label></div>');
        });

        // REMOVE IMAGE
        $('.images-upload-blockS').on('click', '.close', function () {
            $(this).parents('.uploaded-block').remove();
        });

    });
</script>
@stack('js')
