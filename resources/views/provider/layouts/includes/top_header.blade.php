<div class="navbar-custom">
    <ul class="list-unstyled topnav-menu float-right mb-0">


        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle  waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false" href="{{route('provider.notification')}}">
                <i class="fe-bell noti-icon"></i>
                <span class="badge badge-danger rounded-circle noti-icon-badge">{{auth('provider')->user()->Notifications()->where('seen',0)->count()}}</span>
            </a>
        </li>

        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="{{auth('provider')->user()['avatar']}}" alt="user-image" class="rounded-circle">
                <span class="pro-user-name ml-1">
                        {{auth('provider')->user()['name']}} <i class="mdi mdi-chevron-down"></i>
                    </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <!-- item-->
                <div class="dropdown-header noti-title">
                    <h6 class="text-overflow m-0">مرحبا !</h6>
                </div>

                <!-- item-->
                <a href="{{route('provider.setting')}}" class="dropdown-item notify-item">
                    <i class="fe-user"></i>
                    <span>حسابي</span>
                </a>

                <div class="dropdown-divider"></div>

                <!-- item-->
                <a href="{{route('provider.logout')}}" class="dropdown-item notify-item">
                    <i class="fe-log-out"></i>
                    <span>تسجيل الخروج</span>
                </a>

            </div>
        </li>



    </ul>

    <!-- LOGO -->
    <div class="logo-box">
        <a href="index.html" class="logo logo-dark text-center">
                        <span class="logo-lg">
                           <img src="{{dashboard_url('greenLogo.png')}}" style="width: 125px" alt="" >
                        </span>
            <span class="logo-sm">
                            <img src="{{dashboard_url('greenLogo.png')}}" style="width: 125px" alt="" >
                        </span>
        </a>
        <a href="index.html" class="logo logo-light text-center">
                        <span class="logo-lg">
                            <img src="{{dashboard_url('greenLogo.png')}}" style="width: 125px" alt="" >
                        </span>
            <span class="logo-sm">
                            <img src="{{dashboard_url('greenLogo.png')}}" style="width: 125px" alt="" >
                        </span>
        </a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left mb-0">
        <li>
            <button class="button-menu-mobile disable-btn waves-effect">
                <i class="fe-menu"></i>
            </button>
        </li>

        <li>
            <h4 class="page-title-main">لوحة التحكم</h4>
        </li>

    </ul>

</div>
