<meta charset="utf-8" />
<title>GreepApp</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
<meta content="Coderthemes" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- App favicon -->
<link rel="shortcut icon" href="{{dashboard_url('greenLogo.png')}}">

<!--Morris Chart-->
<link rel="stylesheet" href="{{provider_dashboard_path()}}/assets/libs/morris-js/morris.css" />

<!-- App css -->
<link href="{{provider_dashboard_path()}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{{provider_dashboard_path()}}/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
<link href="{{provider_dashboard_path()}}/assets/css/core.css" rel="stylesheet" type="text/css" />
<link href="{{provider_dashboard_path()}}/assets/css/components.css" rel="stylesheet" type="text/css" />
<link href="{{provider_dashboard_path()}}/assets/css/pages.css" rel="stylesheet" type="text/css" />
<link href="{{provider_dashboard_path()}}/assets/css/menu.css" rel="stylesheet" type="text/css" />
<link href="{{provider_dashboard_path()}}/assets/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="{{asset('dashboard/assets/plugins/parsleyjs/src/parsley.css')}}" rel="stylesheet" type="text/css" />


@if(app()->getLocale() == 'ar')
<link href="{{provider_dashboard_path()}}/assets/css/app-rtl.min.css" rel="stylesheet" type="text/css" />
@else
<link href="{{provider_dashboard_path()}}/assets/css/app.min.css" rel="stylesheet" type="text/css" />
@endif

<!-- third party css -->
<link href="{{provider_dashboard_path()}}/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="{{provider_dashboard_path()}}/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="{{provider_dashboard_path()}}/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="{{provider_dashboard_path()}}/assets/libs/datatables/select.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="{{provider_dashboard_path()}}/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
<!-- third party css end -->

<!-- Toastr -->
<link rel="stylesheet" href="{{provider_dashboard_path()}}/assets/toastr/toastr.min.css">
<link rel="stylesheet" href="{{dashboard_url('dashboard/assets/css/filepond.min.css')}}">
<link rel="stylesheet" href="{{dashboard_path()}}/sweet_alert/sweetalert2.min.css">

<link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Almarai&display=swap" rel="stylesheet">
<script src="{{provider_dashboard_path()}}/assets/js/modernizr.min.js"></script>
<style>
    #datatable-table_filter{
        float: right !important;
    }
    .filepond--drop-label.filepond--drop-label label{
        color:#333;
    }
    .dataTables_wrapper{
        margin-top: 20px;
    }
    .page-item:first-child .page-link,.page-item:last-child .page-link{
        border-radius: 0;
    }
    .pagination{
        justify-content: center !important;
    }
    .dt-buttons.btn-group{
        float: left !important;
    }
    .images-upload-block {
        display: inline-block;
        vertical-align: top;
        position: relative;
        margin: 15px 0;
    }


    /*--------------------------------*/

    .images-upload-blockS {

        width: 85px;
        height: 85px;
        border-radius: 50%;
        border: 3px solid #EEE;
        position: relative
    }


    .images-upload-blockS .upload-img,
    .images-upload-blockS .uploaded-block .close {
        position: absolute;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        background: var(--main);
        bottom: -17px;
        overflow: hidden;
        display: flex;
        color: #FFFFFF;
        font-size: 12px;
        opacity: 1;
        justify-content: center;
        align-items: center;
        z-index: 3;
        left: 50%;
        transform: translatex(-50%);
    }


    .images-upload-blockS .image-uploader {

        position: absolute;
        width: 100%;
        height: 100%;
        opacity: 0
    }

    .images-upload-blockS img {
        border-radius: 50%;
        max-width: 100%;
        width: 100%;
        height: 100%;
    }


    .images-upload-blockS .uploaded-block {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        border-radius: 50%;
    }


    /*--------------------------------*/


    #itemdatatable-table_filter{
        float: right;
    }
    .upload-img {
        display: inline-block;
        width: 130px;
        height: 120px;
        border: 1px dashed #5B5B5B;
        line-height: 140px;
        text-align: center;
        position: relative;
        border-radius: 50%;
        margin: 5px;
    }

    .upload-img i {
        font-size: 50px;
        color: #5B5B5B;
    }

    .upload-img input {
        position: absolute;
        /*opacity: 0;*/
        z-index: -1;
        /*display: none*/
    }

    .uploaded-block {
        position: absolute;
        top: 0;
        width: 140px;
        height: 135px;
    }

    .uploaded-block img {
        width: 100%;
        height: 100%;
        margin: 0;
        border-radius: 50%;
    }

    .uploaded-block .close {
        position: absolute;
        top: -8px;
        color: #FFF;
        background: #03A9F4;
        font-size: 18px;
        width: 25px;
        height: 25px;
        text-align: center;
        border-radius: 50%;
        opacity: 1;
        left: 0;
        right: 0;
        margin: auto;
        padding: 0
    }

    .block-img .images-upload-block {
        display: block;
    }

    .block-img .upload-img {
        display: table;
        margin: 5px auto;
    }

    .block-img .uploaded-block {
        position: relative;
        display: inline-block;
        margin: 5px;
    }

    .noti_message{
        padding: 20px;
        border-radius: 20px;
        display: flex;
        justify-content: space-between;
        align-items: flex-start;
        box-shadow: 0px 3px 3px 0px #EEE;
        margin: 10px 0px;}

    .noti_message img{
        width: 75px;
        height: 75px;
        max-width: 100%;
        border-radius: 50%;
        margin-inline-end: 20px;}


    .noti_message > div p{width: 70%;
        max-height: 70px;
        overflow: hidden;
        text-overflow: ellipsis;}


    .noti_message > aside{

        display: flex;
        flex-direction: column;
        align-items: flex-end;
        flex-shrink: 0;
    }


    .noti_message > aside span{width: 20px;
        height: 20px;
        background-color: crimson;
        color: #FFFFFF;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center}

    .fade:not(.show) {
        opacity: 1;
    }
</style>
@stack('css')
