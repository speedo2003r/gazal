<div class="left-side-menu">

    <div class="slimScrollDiv">
        <div class="slimscroll-menu">
            <!-- User box -->
            <div class="user-box text-center">
                <img src="{{auth('provider')->user()['avatar']}}" alt="user-img" title="Mat Helme" class="rounded-circle img-thumbnail avatar-md">
                <div class="dropdown">
                    <a href="javascript:void(0)" class="user-name dropdown-toggle h5 mt-2 mb-1 d-block">{{auth('provider')->user()['name']}}</a>
                </div>
                <ul class="list-inline">

                    <li class="list-inline-item">
                        <a href="{{route('provider.logout')}}">
                            <i class="mdi mdi-power"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <!--- Sidemenu -->
            <div id="sidebar-menu" class="mm-active">

                <ul class="metismenu mm-show" id="side-menu">

                <li class="has-submenu">
                    <a href="{{route('provider.home')}}"><i class="mdi mdi-view-dashboard"></i>الرئيسيه</a>
                </li>



                <li class="has-submenu active">
                    <a href="#" > <i class="fas fa-cube"></i>{{ __('Menu') }} <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="{{route('provider.categories.index')}}">

                                {{awtTrans('الأقسام الرئيسيه')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{route('provider.subcategories.index')}}">

                                {{awtTrans('الأقسام الفرعيه')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{route('provider.product')}}">

                                {{awtTrans('المنتجات')}}</a>
                        </li>

                    </ul>
                </li>



                <li class="has-submenu active">
                    <a href="#" > <i class="mdi mdi-chart-donut-variant"></i>{{awtTrans('الطلبات')}} <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="{{route('provider.orders.index')}}">

                                {{awtTrans('الطلبات')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{route('provider.orders.history')}}">

                                {{awtTrans('الارشيف')}}</a>
                        </li>


                    </ul>
                </li>

                    @if(auth('provider')->user()->isProvider())
                        <li>
                            <a href="{{route('provider.accounts')}}">
                                <i class="fas fa-money-check-alt"></i>
                                {{awtTrans('الحسابات المالية')}}
                            </a>
                        </li>
                    @endif





                {{--        <li>--}}
                {{--            <a href="{{route('provider.offer')}}">--}}
                {{--                <i class="fa fa-tags" aria-hidden="true"></i>--}}
                {{--                {{awtTrans('العروض')}}--}}
                {{--            </a>--}}
                {{--        </li>--}}


                <li>
                    <a href="{{route('provider.coupon')}}">
                        <i class="fa fa-percentage" aria-hidden="true"></i>
                        {{awtTrans('الكوبونات')}}
                    </a>
                </li>


                <li>
                    <a href="{{route('provider.branch.index')}}">
                        <i class="fa fa-university" aria-hidden="true"></i>
                        {{ __('Branches') }}
                    </a>
                </li>

                    @if(auth('provider')->user()->isProvider())
                        <li>
                            <a href="{{route('provider.employees.index')}}">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                {{ __('Employees') }}
                            </a>
                        </li>
                    @endif

                    <li>
                        <a href="{{route('provider.branch.index')}}">
                            <i class="fa fa-university" aria-hidden="true"></i>
                            {{ __('Reports') }}
                        </a>
                    </li>

                    <li class="has-submenu active">
                        <a href="#" > <i class="mdi mdi-chart-donut-variant"></i>{{ __('Reports') }} <div class="arrow-down"></div></a>
                        <ul class="submenu">
                            <li>
                                <a href="{{route('provider.reports.today')}}">

                                    {{ __('Today sales') }}
                                </a>
                            </li>


                        </ul>
                    </li>

            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
            </div>
        <!-- end #navigation -->
        </div>

        <div class="slimScrollBar"></div>
        <div class="slimScrollRail"></div>
    <!-- end container -->
    </div>
</div>
