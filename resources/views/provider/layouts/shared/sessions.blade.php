@push('js')

@if(session('success'))

<script>
    toastr["success"]("My name is Inigo Montoya. You killed my father. Prepare to die!")

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>

@endif

@if(session('wrong'))

<script>
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: "{{ session('wrong') }}",
        showConfirmButton: false,
        timer: 1500
    });
</script>

@endif

@if(session('not_active'))

<script>
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: "{{ session('not_active') }}",
        showConfirmButton: false,
        timer: 1500
    });
</script>

@endif


<script>
    $('.delete').click(function(e) {
        var that = $(this)
        e.preventDefault();
        Swal.fire({
            title: "{{trans('site.confirm_delete')}}",
            text: "{{trans('site.want_to_remove')}}",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "{{ trans('site.delete_it') }}",
            cancelButtonText: "{{ trans('site.cancel') }}",
        }).then((result) => {
            if (result.value) {
                that.closest('form').submit();
            }
        })
    });
</script>

<!-- ============= Image Preview -->

<script>
    $(".image").change(function() {

        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.image-preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }

    });
</script>

@endpush