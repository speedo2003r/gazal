<!DOCTYPE html>
<html lang="en">

<head>
    @include('provider.layouts.includes.head')
</head>

<body>

    <div id="wrapper">
        @include('provider.layouts.includes.top_header')

        <!-- nav Start -->
        @include('provider.layouts.includes.nav')
        <div class="content-page">
            <div class="content">
                <div class="container-fluid">

                    <!-- start page title -->

                    <!-- end page title -->
                    @yield('content')
                <!-- Footer Start -->
                    @include('provider.layouts.includes.footer')
                <!-- end Footer -->
                </div> <!-- end container -->
            </div>
        </div>
    </div>
    <!-- end wrapper -->

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->



    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    @include('provider.layouts.includes.scripts')

    @include('admin.partial.alert')
    @include('admin.partial.confirm_delete')
</body>

</html>
