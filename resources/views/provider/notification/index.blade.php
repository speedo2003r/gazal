@extends('provider.layouts.master')
@section('title',awtTrans('الاشعارات'))
@section('content')

    <div class="title-dash">

        <h5 class="text-color">{{awtTrans('الاشعارات')}}</h5>

    </div>

    <div>
        @foreach($notifications as $notification)
        <a href="{{$notification->order ? route('seller.showOrders',$notification->order['id']) : '#'}}" class="noti_message">
            <div class="d-flex align-items-start">
                <img src="{{$notification->From['avatar']}}">
                <div>
                    <h5 class="main-color">{{$notification->From['name']}}</h5>
                    <p>{{$notification['message_'.app()->getLocale()]}}</p>
                </div>
            </div>

            <aside class="flex-shrink-0">
                <p>{{$notification->created_at->format('Y-m-d h:i a')}}</p>
                <!--<span>1</span>-->
            </aside>
        </a>
        @endforeach
{!! $notifications->links() !!}



    </div>

@endsection
