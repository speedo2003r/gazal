@extends('provider.layouts.master')
@section('title',awtTrans('تغيير  كلمة المرور'))
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">تغيير  كلمة المرور</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">الرئيسيه</a></li>
                        <li class="breadcrumb-item active">تغيير  كلمة المرور</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
            <div class="row">
                <div class="col-lg-8">
                    <div class="form-content">
                        <form action="{{route('provider.postChangePassword')}}" id="formLogin" method="post">
                            @csrf
                            <div class="form-group po_R">
                                <input type="password" class="form-control" name="old_password" placeholder="{{awtTrans('الرجاء ادخال كلمة المرور القديمه')}}">
                            </div>
                            <div class="form-group po_R">
                                <input type="password" class="form-control" name="password" placeholder="{{awtTrans('الرجاء ادخال كلمة المرور الجديده')}}">
                            </div>
                            <div class="form-group po_R">
                                <input type="password" class="form-control" name="password_confirmation" placeholder="{{awtTrans('الرجاء اعادة ادخال كلمة المرور الجديده')}}">
                            </div>
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-spinner fa-spin spinner-ajax" style="display: none"></i>
                                {{awtTrans('تأكيد')}}</button>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>




@endsection
