@extends('provider.layouts.master')
@section('title',awtTrans('الاعدادات'))
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">اعدادات حسابي</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">الرئيسيه</a></li>
                        <li class="breadcrumb-item active">اعدادات حسابي</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">

        <form class="info_setting" action="{{route('provider.setting.update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="d-flex align-items-center">
                <div class="images-upload-blockS">
                    <label class="upload-img">
                        <input type="file" name="image" id="image" accept="image/*" class="image-uploader form-control">
                        <i class="fa fa-camera" aria-hidden="true"></i>
                    </label>
                    <img src="{{$user['avatar']}}">
                </div>

                <div class="m-3">
                    <p>{{$user['name']}}</p>
                    <span>{{$user['phone']}}</span>
                </div>

            </div>

            <div class="mt-20">

                <div class="form-group">
                    <label>{{awtTrans('اسم المستخدم')}}</label>
                    <div class="Modification-info-btn po_R">
                        <input type="text" class="form-control"  name="name" placeholder="{{awtTrans('اسم المستخدم')}}" value="{{$user['name']}}">
                        <span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                    </div>
                </div>


                <div class="form-group">
                    <label>{{awtTrans('رقم الجوال')}}</label>
                    <div class="Modification-info-btn po_R">
                        <input type="text" class="form-control"  name="phone" placeholder="{{awtTrans('رقم الجوال')}}" value="{{$user['phone']}}">
                        <span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                    </div>
                </div>

                <div class="form-group">
                    <label>{{awtTrans('البريد الالكتروني')}}</label>
                    <div class="Modification-info-btn po_R">
                        <input type="email" class="form-control" name="email"  placeholder="{{awtTrans('البريد الالكتروني')}}" value="{{$user['email']}}">
                        <span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                    </div>
                </div>


                <div class="form-group">
                    <label>{{awtTrans('الموقع علي الخريطة')}}</label>
                    <div class="Modification-info-btn po_R">
                        <input type="text" class="form-control" name="address"  placeholder="{{awtTrans('الموقع علي الخريطة')}}" value="{{$user['address']}}"  data-toggle="modal" data-target="#map-modal">
                    </div>
                </div>



                <div class="img_shop form-group">
                    <h6>{{awtTrans('شعار المتجر')}}</h6>

                    <div class="images-upload-blockS">
                        <label class="upload-img">
                            <input type="file" name="logo" id="logo" accept="image/*" class="image-uploader form-control">
                            <i class="fa fa-camera" aria-hidden="true"></i>
                        </label>
                        <img src="{{$user->provider['logo']}}">
                    </div>

                </div>
                <div class="img_shop form-group">
                    <h6>{{awtTrans('بانر المتجر')}}</h6>
                    <div class="images-upload-blockS">
                        <label class="upload-img">
                            <input type="file" name="banner" id="banner" accept="image/*" class="image-uploader form-control">
                            <i class="fa fa-camera" aria-hidden="true"></i>
                        </label>
                        <img src="{{$user->provider['banner']}}">
                    </div>
                </div>






                <div class="modal-footer justify-content-between">
                    <a href="{{route('provider.changePassword')}}" class="btn btn-sm btn-default">{{awtTrans('تغيير كلمة المرور')}}</a>
                    <button type="submit" class="btn btn-sm btn-success">{{awtTrans('حفظ التعديلات')}}</button>
                </div>

            </div>

            <div class="modal fade " id="map-modal" tabindex="-1 " role="dialog " aria-labelledby="exampleModalLabel " aria-hidden="true ">
                <div class="modal-dialog modal-lg" role="document ">
                    <div class="modal-content ">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{{awtTrans('عنوانك')}}</h5>
                        </div>

                        <form>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="hidden" name="lat" id="lat" value="{{$user['lat']}}">
                                    <input type="hidden" name="lng" id="lng" value="{{$user['lng']}}">
                                </div>
                                <div id="map" style="width: 100%; height: 400px;"> </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm btn-success" data-dismiss="modal"> {{awtTrans('تأكيد')}}</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </form>

            </div>
        </div>
    </div>

@endsection
@push('js')

    <script src="{{dashboard_url('dashboard/js/map.js')}}"></script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key={{settings('map_key')}}&libraries=places&callback=initMap&language=ar"
            async defer></script>

@endpush
