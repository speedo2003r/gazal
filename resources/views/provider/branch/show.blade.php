@extends('provider.layouts.master')
@section('title', $item->title)

@push('css')
    <style>
        .item-info .col-md-6 , .item-info .col-md-12 {
            margin-top: 20px;
        }
    </style>
@endpush

@section('content')


    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">{{$item->title}}</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('provider')}}">{{awtTrans('الرئيسيه')}}</a></li>
                        <li class="breadcrumb-item "><a href="{{ route('provider.branch.index') }}">{{awtTrans('الفروع')}}</a></li>
                        <li class="breadcrumb-item active">{{awtTrans('تفاصيل الفرع')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <div class="mt-10 pag-requ">

                    <div class="row item-info">

                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>@lang('Name (ar)')</aside>
                                <aside class="main-color">{{ $item->getTranslation('title', 'ar') }} </aside>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>@lang('Name (en)')</aside>
                                <aside class="main-color">{{ $item->getTranslation('title', 'en') }} </aside>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>{{__('phone')}}</aside>
                                <aside class="main-color">{{$item->phone}}</aside>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>{{__('email')}}</aside>
                                <aside class="main-color">{{$item->email}}</aside>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>{{__('Close from')}}</aside>
                                <aside class="main-color">
                                    {{ $item->from ? $item->from->format('Y-m-d') : '' }}
                                </aside>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>{{__('To')}}</aside>
                                <aside class="main-color">{{ $item->to ? $item->to->format('Y-m-d') : '' }}</aside>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>@lang('Country')</aside>
                                <aside class="main-color">
                                    {{ $item->country->title ?? '' }}
                                </aside>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>@lang('City')</aside>
                                <aside class="main-color">
                                    {{ $item->city->title ?? '' }}
                                </aside>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>@lang('Status')</aside>
                                <aside class="main-color">
                                    {{ $item->current_status }}
                                </aside>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="d-flex justify-content-between mb-20">
                                <aside>@lang('Address')</aside>
                                <aside class="main-color">
                                    {{ $item->address }}
                                    <input type="hidden" name="lat" id="address" value="{{ $item->address }}">
                                    <input type="hidden" name="lat" id="lat" value="{{ $item->lat }}">
                                    <input type="hidden" name="lng" id="lng" value="{{ $item->lng }}">
                                </aside>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="d-flex justify-content-between mb-20">
                                <div id="map" style="height: 600px;width: 100%"></div>
                            </div>
                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>


@endsection

@push('js')
    <script src="{{dashboard_url('dashboard/assets/js/map.js')}}"></script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key={{settings('map_key')}}&libraries=places&callback=initMap&language=ar"
            async defer></script>
@endpush