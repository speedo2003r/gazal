@extends('provider.layouts.master')
@section('title',awtTrans('الفروع'))
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">الفروع</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">الرئيسيه</a></li>
                        <li class="breadcrumb-item active">الفروع</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                @if(auth('provider')->user()->isProvider())
                <a href="{{route('provider.branch.create')}}"
                        class="btn btn-lighten-primary waves-effect waves-primary  width-md add-user">{{awtTrans('إضافة فرع')}}</a>
                @endif

                <form action="">

                    <div class="row">

                        <div class="col-6">
                            <div class="form-group">
                                <label for="">@lang('Status')</label>
                                <select name="status" id="status-filter" class="form-control">
                                    <option value="" {{ is_null(request('status')) ? 'selected' : '' }}>@lang('All')</option>
                                    <option value="open" {{ request('status') == 'open' ? 'selected' : '' }}>@lang('Open')</option>
                                    <option value="closed" {{ request('status') == 'closed' ? 'selected' : '' }}>@lang('Closed')</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>

                {!! $dataTable->table(["class"=> "table table-striped table-bordered table-hover table-checkable dataTable no-footer"],true) !!}

{{--                <table class="table dt-responsive nowrap dataTable no-footer dtr-inline collapsed" id="datatable-table" style="width:100%;">--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                        <th>#</th>--}}
{{--                        <th>{{awtTrans('اسم الفرع')}}</th>--}}
{{--                        <th>{{awtTrans('العنوان')}}</th>--}}
{{--                        <th>{{awtTrans('التحكم')}}</th>--}}
{{--                    </tr>--}}
{{--                    </thead>--}}
{{--                    <tbody>--}}
{{--                    @foreach($branches as $key => $branch)--}}
{{--                    <tr>--}}
{{--                        <td>{{$key+1}}</td>--}}
{{--                        <td>{{$branch['title']}}</td>--}}
{{--                        <td>{{$branch['address']}}</td>--}}
{{--                        <td>--}}
{{--                            <button type="button" data-toggle="modal" data-target="#exampleModal5" onclick="edit({{$branch}})" class="btn btn-icon waves-effect waves-light btn-success">--}}
{{--                                <i class="fas fa-edit"></i>--}}
{{--                            </button>--}}
{{--                            <button class="btn btn-icon waves-effect waves-light btn-primary times" data-dates="{{$branch->branchDates}}" data-toggle="modal" data-target="#dateModal" data-branch_id="{{$branch['id']}}">--}}
{{--                                <i class="fa fa-clock"></i>--}}
{{--                            </button>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    @endforeach--}}
{{--                    </tbody>--}}
{{--                </table>--}}
            </div>
        </div>




    </div>





    <div class="modal fade" id="dateModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 1000px !important;">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        {{ awtTrans('اضافة وقت الفتح والاغلاق') }} <span
                                class="userName"></span>
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="branches">
                        <button type="button"
                                class="btn btn-primary waves-effect waves-light addmoredate mb-2"
                                data-user_id="" data-toggle="modal" data-target="#addDateModal">اضافه
                            ميعاد جديد</button>
                        <table class="table table-border">
                            <tr>
                                <th>{{ awtTrans('اليوم') }}</th>
                                <th colspan="2">{{ awtTrans('الشيفت الأول') }}</th>
                                <th colspan="2">{{ awtTrans('الشيفت الثاني') }}</th>
                                <th colspan="2">{{ awtTrans('الشيفت الثالث') }}</th>
                                <th colspan="2">{{ awtTrans('الشيفت الرابع') }}</th>
                                <th>{{ __('Appointment type') }}</th>
                                <th>{{ awtTrans('التحكم') }}</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>{{ awtTrans('من') }}</th>
                                <th>{{ awtTrans('الي') }}</th>
                                <th>{{ awtTrans('من') }}</th>
                                <th>{{ awtTrans('الي') }}</th>
                                <th>{{ awtTrans('من') }}</th>
                                <th>{{ awtTrans('الي') }}</th>
                                <th>{{ awtTrans('من') }}</th>
                                <th>{{ awtTrans('الي') }}</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addDateModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ awtTrans('اضافة ميعاد') }}
                        <span class="userName"></span>
                    </h5>
                </div>
                <div class="modal-body">

                    <form action="#" method="POST"
                          id="saveBranchDate">
                        {{-- <form action="" method="POST" class="saveBranch"> --}}
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="time" name="timefrom" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="time" name="timeto" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="shift" required style="width: 100% !important;"
                                            class="form-control">
                                        <option value="" hidden selected>اختر شيفت</option>
                                        <option value="1">الشيفت الأول</option>
                                        <option value="2">الشيفت الثاني</option>
                                        <option value="3">الشيفت الثالث</option>
                                        <option value="4">الشيفت الرابع</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="type" required style="width: 100% !important;"
                                            class="form-control">
                                        <option value="" hidden selected>نوع الموعد</option>
                                        <option value="1">مواعيد الايام العادية</option>
                                        <option value="2">مواعيد رمضان</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="work_days[]" required multiple
                                            style="width: 100% !important;" class="form-control select2">
                                        @foreach (\App\Models\User::workDays() as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-sm btn-success save">إرسال</button>
                            <button type="button" class="btn btn-default" id="notifyClose"
                                    data-dismiss="modal">اغلاق</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection



@push('js')

    {!! $dataTable->scripts() !!}


    <script>
        $(document).ready(function () {

            $(document).on('click', '.branch-appear', function () {
                let id = $(this).data('id');

                Swal.fire({
                    title: "{{__('Are you sure?')}}",
                    text: '{{__("You won't be able to revert this!")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: "{{__('Yes, change it!')}}"
                }).then((result) => {

                    if (result.value) {
                        $.ajax({
                            url: "{{url('provider/branch/appear')}}/"+id,
                            type: 'post',
                            datatype: 'json',
                            success(response) {
                                Swal.fire(
                                    '{{__("Changed!")}}',
                                    '{{__("Your order status has been changed.")}}',
                                    '{{__("Success")}}'
                                )
                                setTimeout(() => {
                                    window.location.reload();
                                }, 1200)
                            },
                            error(error) {
                                console.log('error', error)
                            }
                        })

                    }
                });
            })

            $(document).on('change', '#status-filter', function () {
                $(this).closest('form').submit()
            })
        })
    </script>

    <script>
        $('body').on('click', '.times', function() {
            var url = $(this).data('url');
            var dates = $(this).data('dates');
            $('#dateModal table tbody').empty();
            $('#addDateModal').find('form').attr('action', url);

            var html = `
                            <tr>
                                <th>{{ awtTrans('اليوم') }}</th>
                                <th colspan="2">{{ awtTrans('الشيفت الأول') }}</th>
                                <th colspan="2">{{ awtTrans('الشيفت الثاني') }}</th>
                                <th colspan="2">{{ awtTrans('الشيفت الثالث') }}</th>
                                <th colspan="2">{{ awtTrans('الشيفت الرابع') }}</th>
                                <th>{{ __('Appointment type') }}</th>
                                <th>{{ awtTrans('التحكم') }}</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>{{ awtTrans('من') }}</th>
                                <th>{{ awtTrans('الي') }}</th>
                                <th>{{ awtTrans('من') }}</th>
                                <th>{{ awtTrans('الي') }}</th>
                                <th>{{ awtTrans('من') }}</th>
                                <th>{{ awtTrans('الي') }}</th>
                                <th>{{ awtTrans('من') }}</th>
                                <th>{{ awtTrans('الي') }}</th>
                                <th></th>
                                <th></th>
                            </tr>`;
            if (dates.length) {
                result = dates.reduce(function(r, a) {
                    r[a.day] = r[a.day] || [];
                    r[a.day].push(a);
                    return r;
                }, Object.create(null));
                console.log(result);
                $.each(result, (index, value) => {
                    html += `
                <tr>
                <td>${days(index)}</td>

                <td>${value.find(x => x.shift === '1') ? value.find(x => x.shift === '1').timefrom : ''}</td>
                <td>${value.find(x => x.shift === '1') ? value.find(x => x.shift === '1').timeto : ''}</td>

                <td>${value.find(x => x.shift === '2') ? value.find(x => x.shift === '2').timefrom : ''}</td>
                <td>${value.find(x => x.shift === '2') ? value.find(x => x.shift === '2').timeto : ''}</td>

                <td>${value.find(x => x.shift === '3') ? value.find(x => x.shift === '3').timefrom : ''}</td>
                <td>${value.find(x => x.shift === '3') ? value.find(x => x.shift === '3').timeto : ''}</td>

                <td>${value.find(x => x.shift === '4') ? value.find(x => x.shift === '4').timefrom : ''}</td>
                <td>${value.find(x => x.shift === '4') ? value.find(x => x.shift === '4').timeto : ''}</td>
                <td>${value[0].type == 1 ? "{{__('Normal appointments')}}" : "{{__('Ramadan appointments')}}"}</td>
                <td>
                <form action="{{ url('/') }}/provider/branch/${value[0].branch_id}/times/${value[0].id}" method="post">
                @csrf() @method('DELETE')
                    <input type="hidden" value="${index}" name="date">
                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                </form>
                </td>
                    </tr>
                  `;
                });

            } else {
                html += `
                <tr class="text-center">
                <td colspan="9">{{ awtTrans('لا يوجد مواعيد للفروع') }}</td>
                </tr>`
            }
            $('#dateModal table tbody').append(html);
        });

        function days(day) {
            var arrs = new Array(`{{ awtTrans('السبت') }}`, `{{ awtTrans('الأحد') }}`,
                `{{ awtTrans('الاثنين') }}`, `{{ awtTrans('الثلاثاء') }}`,
                `{{ awtTrans('الاربعاء') }}`, `{{ awtTrans('الخميس') }}`,
                `{{ awtTrans('الجمعه') }}`);
            return arrs[day - 1];
        }
    </script>

@endpush