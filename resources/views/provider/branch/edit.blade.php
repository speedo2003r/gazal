
@extends('provider.layouts.master')
@section('title', __('Edit').' '.$item->title)
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">{{awtTrans('الفروع')}}</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{awtTrans('الرئيسيه')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('provider.branch.index') }}">{{awtTrans('الفروع')}}</a></li>
                        <li class="breadcrumb-item active">{{__('Edit')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <form action="{{route('provider.branch.update', $item->id)}}" method="POST" id="editForm" class="saveBranch" enctype="multipart/form-data">
                @csrf @method('PUT')

                    <div class="row">

                        <div class = "col-md-12 text-center">
                            <x-alert />
                        </div>

                        <div class = "col-sm-12 text-center">
                            <label class = "mb-0">{{__('avatar')}}</label>
                            <div class = "text-center">
                                <div class = "images-upload-block single-image">
                                    <label class = "upload-img">
                                        <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader" >
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </label>
                                    <div class = "upload-area" id="upload_area_img">
                                        <img src="{{$item->avatar}}" alt="" width="60" height="60">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group col-md-6">
                            <label>@lang('Name (ar)')</label>
                            <input type="text" class="form-control input"  value="{{$item->getTranslation('title', 'ar')}}" id="title_ar" name="title[ar]" style="padding-right: 2rem !important;" placeholder="@lang('Name (ar)')" required maxlength="191">
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('Name (en)')</label>
                            <input type="text" class="form-control input"  value="{{$item->getTranslation('title', 'en')}}" id="title_en" name="title[en]" style="padding-right: 2rem !important;" placeholder="@lang('Name (en)')" required maxlength="191">
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('phone')}}</label>
                                <input type="number" name="phone" id="phone"
                                       value="{{$item->phone}}" class="form-control"
                                       placeholder="05xxxxxxxx" required
                                       data-parsley-length="[9,10]" pattern="05\d{8}">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('email')}}</label>
                                <input type="email" name="email" id="email" value="{{$item->email}}" class="form-control" required maxlength="191">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password">{{__('password')}}</label>
                                <input type="password" name="password" class="form-control" autocomplete="off" id="password"  minlength="6" maxlength="191">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@lang('Password Confirmation')</label>
                                <input type="password" name="password_confirmation" class="form-control" autocomplete="off"  equalTo="#password" minlength="6" maxlength="191">
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">{{__('Close from')}}</label>
                                <input type="date" name="from" class="form-control" id="fromDate" value="{{ $item->from ? $item->from->format('Y-m-d') : '' }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">{{__('To')}}</label>
                                <input type="date" name="to" class="form-control" value="{{ $item->to ? $item->to->format('Y-m-d') : '' }}">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@lang('Country')</label>
                                <select name="country_id" id="country_id" class="form-control" required>
                                    <option value="">@lang('Choose')</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country['id']}}" @if($item->country_id == $country['id']) selected @endif>{{$country->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@lang('City')</label>
                                <select name="city_id" id="city" class="form-control" required>
                                    <option value="">@lang('Choose')</option>
                                    @foreach($item->country->Cities as $city)
                                        <option value="{{ $city->id }}" {{ $city->id == $item->city_id ? 'selected' : '' }}>{{ $city->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>@lang('Address')</label>
                            <input type="hidden" name="lat" id="lat" value="{{ $item->lat }}">
                            <input type="hidden" name="lng" id="lng" value="{{ $item->lng }}">
                            <input type="text" name="address" id="address" class="form-control" value="{{ $item->address }}" required>
                        </div>

                        <div class="col-md-12">
                         <div id="map" style="height: 300px;width: 100%"></div>
                        </div>


                        <div class="col-md-12" style="margin-top: 20px">
                            <button class="btn btn-primary">@lang('Save')</button>
                        </div>

                    </div>


                </form>
            </div>
        </div>
    </div>


@endsection

@push('js')
    <script src="{{dashboard_url('dashboard/assets/js/map.js')}}"></script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key={{settings('map_key')}}&libraries=places&callback=initMap&language=ar"
            async defer></script>
    <script>
        $(function (){
            'use strict'
            $('body').on('click','.add-user',function (){
                $('#editForm').attr('action',"{{route('provider.branch.store')}}")
                $('.modal-title').html('{{awtTrans('اضافة فرع')}}')
                $('#editForm :input:not([type=checkbox],[type=radio],[type=hidden])').val('');
            });
        });
        $(function () {
            'use strict'
            $('body').on('change','[name=country_id]',function () {
                var id = $(this).val();
                getCities(id);
            });
        });
        $('.add-user').on('click',function () {
            $('.modal-title').text('اضافة فرع');
            $('[name=city_id]').empty();
            $('#editForm :input:not([type=checkbox],[type=radio],[type=hidden])').val('');
            $( '#upload_area_img' ).empty();
            $('#editForm')      .attr("action","{{route('provider.branch.store')}}");
        });
        $(document).on('click','.featureEdit',function () {
            var id = $(this).data('id');
            $('.branch_id2').val(id);
        });
        function edit(ob){
            $('[name=password]')    .val('');
            $('#editForm').attr('action',"{{route('provider.branch.update','obId')}}".replace('obId',ob.id))
            $('.modal-title').html('{{awtTrans('تعديل الفرع')}}')
            $('.saveBranch').children('input').empty();
            $('#branch_id')    .val(ob.id);
            $('#name')    .val(ob.name);
            $('#title_ar')    .val(ob.title.ar);
            $('#title_en')     .val(ob.title.en);
            $('#phone')     .val(ob.phone);
            $('#email')     .val(ob.email);
            $('#country_id')     .val(ob.country_id).change;
            $('#lat')     .val(ob.lat);
            $('#lng')     .val(ob.lng);
            $('#address')     .val(ob.address);
            getCities(ob.country_id,ob.city_id);
            initMap();
            let image = $( '#upload_area_img' );
            image.empty();
            image.append( '<div class="uploaded-block" data-count-order="1"><a href="' + ob.avatar + '"  data-fancybox data-caption="' + ob.avatar + '" ><img src="' + ob.avatar + '"></a><button class="close">&times;</button></div>' );
        }
        function show(ob){
            $('#featureModel :input:not([type=checkbox],[type=hidden])').val('');
            $('#featureModel :input([type=checkbox])').prop('checked',false);
            $.each(ob,(index,value)=>{
                if(value.feature == 'ship'){
                    $('.ship textarea').val(value.desc);
                    $('.ship [type=checkbox]').prop('checked',value.status == 1 ? true : false);
                }
                if(value.feature == 'return'){
                    $('.return textarea').val(value.desc);
                    $('.return [type=checkbox]').prop('checked',value.status == 1 ? true : false);
                }
                if(value.feature == 'shop'){
                    $('.shop textarea').val(value.desc);
                    $('.shop [type=checkbox]').prop('checked',value.status == 1 ? true : false);
                }
            })
        }
        $('body').on('click','.times',function () {
            var branch = $(this).data('branch_id');
            var dates = $(this).data('dates');
            $('#dateModal table tbody').empty();
            $("input[name='branch_id']").val(branch);
            var html = `
             <tr>
                                <th>من</th>
                                <th>الي</th>
                                <th>اليوم</th>
                                <th>التحكم</th>
                            </tr>`;
            if(dates.length){
                $.each(dates,(index,value)=>{
                    html += `
                <tr>
                <td>${value.timefrom}</td>
                <td>${value.timeto}</td>
                <td>${days(value.day)}</td>
                <td>
                <ul>
                <form action="{{route('provider.branch.deletedatebranches')}}" method="post">
                @csrf()
                    <input type="hidden" value="${value.id}" name="date">
                    <input type="hidden" value="${branch}" name="branch_id">
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                </form>
                </td>
                    </tr>
                `;
                });

            }else{
                html += `
                <tr class="text-center">
                <td colspan="4">لا يوجد مواعيد للفروع</td>
                </tr>`
            }
            $('#dateModal table tbody').append(html);
        });
        function days(day){
            var arrs = new Array("السبت", "الأحد", "الاثنين","الثلاثاء","الاربعاء","الخميس","الجمعه");
            return arrs[day - 1];
        }

    </script>
@endpush