<div class="btn-group">
    <a href="javascript:void(0)" class="btn btn-warning"
       data-toggle="modal"
       data-target="#rates_{{$id}}">
        <i class="fa fa-star"></i>
        {{trans('Rates')}}
    </a>
    <a href="javascript:void(0)" class="btn btn-success times"
       data-dates="{{ json_encode($all_dates) }}" data-toggle="modal"
       data-target="#dateModal" data-url="{{ route('provider.branch.times.store', $id) }}">
        <i class="fa fa-clock"></i>
        {{trans('Work Times')}}
    </a>
    <a href="{{ route('provider.branch.show', $id) }}" class="btn btn-info" ><i class="fa fa-eye"></i> {{trans('Show')}}</a>
    @if(auth('provider')->user()->isProvider())
    <a href="{{ route('provider.branch.edit', $id) }}" class="btn btn-primary" ><i class="fas fa-edit"></i> {{trans('Edit')}}</a>
    <a data-toggle="modal" data-target="#delete_record{{$id}}" href="#" class="btn btn-danger">
        <i class="fas fa-trash"></i> {{trans('Delete')}}</a>
    @endif
</div>
<div class="modal fade" id="delete_record{{$id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{trans('Delete')}}</h4>
                <button class="close" data-dismiss="modal">x</button>
            </div>
            <div class="modal-body">
                <i class="fa fa-exclamation-triangle"></i> {{trans('Are you sure?')}}
            </div>
            <div class="modal-footer">
                <form action="{{ route('provider.branch.destroy', $id) }}">@csrf @method('DELETE')
                    <button class="btn btn-danger btn-flat">@lang('Yes')</button>
                    <a class="btn btn-default btn-flat" data-dismiss="modal">{{trans('Cancel')}}</a>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Show rates -->
<div class="modal fade" id="rates_{{$id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{trans('Rates')}}</h4>
                <button class="close" data-dismiss="modal">x</button>
            </div>
            <div class="modal-body">
                {{__('Total')}} : {{ count($reviews) }}
                <div class="form-group">
                    <table class="table table-striped">
                        <thead>
                        <td>#</td>
                        <td>{{__('Name')}}</td>
                        <td>{{__('Rate')}}</td>
                        </thead>
                        <tbody>
                        @foreach($reviews as $review)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $review->user_name }}</td>
                                <td>{{ $review->rate }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal-footer">
                <a class="btn btn-default btn-flat" data-dismiss="modal">{{trans('Close')}}</a>
            </div>
        </div>
    </div>
</div>