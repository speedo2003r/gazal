@extends('provider.layouts.master')
@section('title', __('Employees'))
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">{{__('Employees')}}</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('provider')}}">{{__('Main')}}</a></li>
                        <li class="breadcrumb-item active">{{__('Employees')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <a href="{{route('provider.employees.create')}}"
                        class="btn btn-lighten-primary waves-effect waves-primary  width-md add-user">{{ __('Add new') }}</a>


                <table class="table dt-responsive nowrap dataTable no-footer dtr-inline collapsed" id="datatable-table" style="width:100%;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('Name')}}</th>
                        <th>{{__('Phone')}}</th>
                        <th>{{ __('Email')  }}</th>
                        <th>{{ __('Address')  }}</th>
                        <th>{{ __('Status')  }}</th>
                        <th>{{ __('Actions')  }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->phone }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->country->title.' - '. $item->city->title.' - '.$item->address }}</td>
                        <td>
                            @if($item->active)
                                <button type="button" class="btn btn-success active-row" data-url="{{ route('provider.employees.status', $item->id) }}">@lang('Active')</button>
                            @else
                                <button type="button" class="btn btn-danger active-row" data-url="{{ route('provider.employees.status', $item->id) }}">@lang('Detective')</button>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('provider.employees.show', $item->id) }}" class="btn btn-icon waves-effect waves-light btn-primary">
                                <i class="fas fa-eye"></i>
                            </a>

                            <a href="{{ route('provider.employees.edit', $item->id) }}" class="btn btn-icon waves-effect waves-light btn-success">
                                <i class="fas fa-edit"></i>
                            </a>
                            <button class="btn btn-icon waves-effect waves-light btn-danger" title="{{__('Delete')}}"
                                    onclick="confirmDelete('{{ route('provider.employees.destroy', $item->id) }}')" data-toggle="modal" data-target="#delete-model">
                                <i class="fa fa-trash"></i>
                            </button>



                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>




    </div>


@endsection



@push('js')

    <script>
        $(document).ready(function () {

            $(document).on('click', '.active-row', function () {
                let url = $(this).data('url');

                Swal.fire({
                    title: "{{__('Are you sure?')}}",
                    text: '{{__("You won't be able to revert this!")}}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: "{{__('Yes, change it!')}}"
                }).then((result) => {

                    if (result.value) {
                        $.ajax({
                            url,
                            type: 'post',
                            datatype: 'json',
                            success(response) {
                                Swal.fire(
                                    '{{__("Changed!")}}',
                                    '{{__("Your item status has been changed.")}}',
                                    '{{__("Success")}}'
                                )
                                setTimeout(() => {
                                    window.location.reload();
                                }, 1200)
                            },
                            error(error) {
                                console.log('error', error)
                            }
                        })

                    }
                });
            })


        })
    </script>



@endpush