
@extends('provider.layouts.master')
@section('title', __('Edit').' '.$item->title)
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title">{{ __('Employees') }}</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">{{awtTrans('الرئيسيه')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('provider.employees.index') }}">{{__('Employees')}}</a></li>
                        <li class="breadcrumb-item active">{{__('Edit')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <form action="{{route('provider.employees.update', $item->id)}}" method="POST" id="editForm" class="saveBranch" enctype="multipart/form-data">
                @csrf @method('PUT')

                    <div class="row">

                        <div class = "col-md-12 text-center">
                            <x-alert />
                        </div>

                        <div class = "col-sm-12 text-center">
                            <label class = "mb-0">{{__('avatar')}}</label>
                            <div class = "text-center">
                                <div class = "images-upload-block single-image">
                                    <label class = "upload-img">
                                        <input type = "file" name = "avatar" id = "image" accept = "image/*" class = "image-uploader" >
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </label>
                                    <div class = "upload-area" id="upload_area_img">
                                        <img src="{{$item->avatar}}" alt="" width="60" height="60">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <div class="form-group">
                                <label>@lang('Name')</label>
                                <input type="text" class="form-control input"  value="{{old('name', $item->name)}}" id="name"
                                       name="name" placeholder="{{__('Name')}}" required maxlength="191">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('phone')}}</label>
                                <input type="number" name="phone" id="phone"
                                       value="{{$item->phone}}" class="form-control"
                                       placeholder="05xxxxxxxx" required
                                       data-parsley-length="[9,10]" pattern="05\d{8}">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('email')}}</label>
                                <input type="email" name="email" id="email" value="{{$item->email}}" class="form-control" required maxlength="191">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password">{{__('password')}}</label>
                                <input type="password" name="password" class="form-control" autocomplete="off" id="password"  minlength="6" maxlength="191">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@lang('Password Confirmation')</label>
                                <input type="password" name="password_confirmation" class="form-control" autocomplete="off"  equalTo="#password" minlength="6" maxlength="191">
                            </div>
                        </div>


                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@lang('Country')</label>
                                <select name="country_id" id="country_id" class="form-control" required>
                                    <option value="">@lang('Choose')</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country['id']}}" @if($item->country_id == $country['id']) selected @endif>{{$country->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@lang('City')</label>
                                <select name="city_id" id="city" class="form-control" required>
                                    <option value="">@lang('Choose')</option>
                                    @foreach($item->country->Cities as $city)
                                        <option value="{{ $city->id }}" {{ $city->id == $item->city_id ? 'selected' : '' }}>{{ $city->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@lang('Branches')</label>
                                <select name="branches[]" class="form-control" multiple required>
                                    @foreach(\App\Entities\Branch::where('provider_id', auth('provider')->id())->get() as $branch)
                                        <option value="{{ $branch->id }}" {{ in_array($branch->id, $item->employee_branches()->pluck('branches.id')->toArray()) ? 'selected' : '' }}>{{$branch->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group col-sm-12">
                            <label>@lang('Address')</label>
                            <input type="hidden" name="lat" id="lat" value="{{ $item->lat }}">
                            <input type="hidden" name="lng" id="lng" value="{{ $item->lng }}">
                            <input type="text" name="address" id="address" class="form-control" value="{{ $item->address }}" required>
                        </div>

                        <div class="col-md-12">
                         <div id="map" style="height: 300px;width: 100%"></div>
                        </div>


                        <div class="col-md-12" style="margin-top: 20px">
                            <button class="btn btn-primary">@lang('Save')</button>
                        </div>

                    </div>


                </form>
            </div>
        </div>
    </div>


@endsection

@push('js')
    <script src="{{dashboard_url('dashboard/assets/js/map.js')}}"></script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key={{settings('map_key')}}&libraries=places&callback=initMap&language=ar"
            async defer></script>
    <script>

        $(function () {
            'use strict'
            $('body').on('change','[name=country_id]',function () {
                var id = $(this).val();
                getCities(id);
            });
        });

    </script>
@endpush