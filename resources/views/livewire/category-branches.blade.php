<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="second-title">
        <h3>
          {{ $category_title }}
          {{ awtTrans(' بالقرب منك') }}
        </h3>
      </div>
    </div>
  </div>
  <div class="row">
    @foreach ($categoryBranches as $branch)
      <div class="col-lg-4 col-md-6">
        <a href="{{ route('client.providerDetails', $branch->branch_id) }}">
          @include('front.client.layouts.parts.food-card',['branch' => $branch])
        </a>
      </div>
    @endforeach

    <div class="col-sm-12">
      <div class="center-align">
        <a href="{{ route('client.filter', array_merge(['category_id' => $category_id],(array)$requestLatLngCity)) }}"
           class="stand-link">
          <span>{{ awtTrans('عرض اكثر') }}</span>
        </a>
      </div>
    </div>
  </div>
</div>
