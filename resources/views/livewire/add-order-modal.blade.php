<div wire:ignore.self class="modal add-order fade" id="add-order-modal" tabindex="-1"
     role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @if ($choosenItem)
          <div class="add-to-order">
            <div class="row">
              <!-- item -->
              <div class="col-lg-5">
                <div class="right-add">
                  <div class="img">
                    <img src="{{ $choosenItem->main_image }}" alt="item">
                  </div>
                  <div class="exp">
                    <div class="exp-text">
                      <h5>{{ $choosenItem->title ?? '' }}</h5>
                      <p>{{ $choosenItem->description ?? '' }}</p>
                    </div>
                    <div class="exp-price">
                      <h5 class="price">
                        {{ $choosenItem->price() }}
                        {{ awtTrans('ر.س') }}
                      </h5>
                    </div>
                  </div>
                </div>
              </div>

              <!-- options -->
              <div class="col-lg-7">
                <div class="left-add">
                  @if (count($choosenItem->types))
                    <div class="sizes">
                      @foreach ($choosenItem->types as $i => $itemType)
                        <h4>{{ $itemType->type->title }}</h4>
                        <ul class="list-inline">
                          @foreach ($itemType->children as $ii => $child)
                            <li class="ckbox">
                              <div>

                                <input type="radio" name="d" id="radio"
                                       wire:model.defer="itemTypes.{{ $i }}"
                                       value="{{ $child->id }}"
                                       @if (collect($itemTypes)->contains($child->id)) checked @endif>

                                <label for="radio">{{ $child->title }}</label>
                              </div>
                              <div class="price">
                                <h5>
                                  {{ $child->price }}
                                  {{ awtTrans('ر.س') }}
                                </h5>
                              </div>
                            </li>
                          @endforeach
                        </ul>
                      @endforeach
                    </div>
                  @endif

                  @if (count($choosenItem->features))
                    <div class="extras">
                      <h4>{{ awtTrans('الاضافات') }}</h4>
                      <ul class="list-inline">
                        @foreach ($choosenItem->features as $feature)
                          <li class="ckbox">
                            <div>

                              <input type="checkbox" name="c" id="check"
                                     wire:model.defer="itemFeatures"
                                     value="{{ $feature->id }}"
                                     @if (collect($itemFeatures)->contains($feature->id)) checked @endif>


                              <label for="check">{{ $feature->title }}</label>
                            </div>
                            <div class="price">
                              <h5>
                                {{ $feature->price }}
                                {{ awtTrans('ر.س') }}
                              </h5>
                            </div>
                          </li>
                        @endforeach
                      </ul>
                    </div>
                  @endif

                  <div class="notes">
                    <h4>{{ awtTrans('ملاحظات اضافية') }}</h4>
                    <textarea class="form-control" wire:model="notes"
                              placeholder="{{ awtTrans('اكتب ملاحظات على الطلب') }}"></textarea>
                  </div>
                  <div>
                    <button class="stand-link btn-block" wire:click="addToCart">
                      <span>
                        {{ awtTrans('أضف الى السلة') }}
                      </span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
