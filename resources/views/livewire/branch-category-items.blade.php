<div class="retu-filter">
  <div class="row">
    <div class="col-md-2">
      <div class="retu-right">
        <h4>{{ awtTrans('الفلتر') }}</h4>
        <ul class="list-unstyled">
          <li class="active">
            <a href="" wire:click.prevent="updateItems(0)">
              {{ awtTrans('الاكثر مبيعا') }}
            </a>
          </li>
          @foreach ($providerCategories as $category)
            <li>
              <a href="" wire:click.prevent="updateItems({{ $category->id }})">
                {{ $category->title }}
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    </div>
    <div class="col-md-10">
      <div class="returant-meals">
        <div class="search">
          <input type="text" class="form-control" placeholder="{{ awtTrans('ابحث') }}"
                 wire:model="search">
          <i class="fas fa-search"></i>
        </div>
        <div class="row">
          @foreach ($items as $item)
            <div class="col-md-6">
              <div class="meal-card">
                <div class="top-meal">
                  <di class="media">
                    <img src="{{ $item->main_image }}" alt="item">
                    <div class="media-body">
                      <span>{{ $item->title ?? '' }}</span>
                      <p>{{ $item->description ?? '' }}</p>
                    </div>
                  </di>
                  <button class="love {{ $item->is_fav ? 'fas' : 'far' }} fa-heart"
                          wire:click="toggleFav({{ $item->id }})"></button>
                </div>
                <div class="bottom-meal">
                  <h5 class="price">
                    {{ $item->price() }}
                    {{ awtTrans('ر.س') }}
                  </h5>
                  <button class="btn"
                          wire:click="$emit('add-item', {{ $item->id }}, {{ $branch->id }})">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
