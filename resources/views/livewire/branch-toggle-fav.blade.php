<div>
  <button class="love {{ !$branch->is_fav ? 'fas' : 'far' }} fa-heart"
          wire:click="toggleFav({{ $branch->id }})"></button>
</div>
