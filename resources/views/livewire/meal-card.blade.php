<div class="meal-card {{ $hide ? 'd-none' : '' }}">
  <div class="top-meal">
    <di class="media">
      <img src="{{ $item->main_image }}" alt="item">
      <div class="media-body">
        <span>{{ $item->title ?? '' }}</span>
        <p>{{ $item->description ?? '' }}</p>
      </div>
    </di>
    <button class="love {{ $item->is_fav ? 'fas' : 'far' }} fa-heart"
            wire:click="toggleFav({{ $item->id }})"></button>
  </div>
  <div class="bottom-meal">
    <h5 class="price">
      {{ $item->price() }}
      {{ awtTrans('ر.س') }}
    </h5>
  </div>
</div>
