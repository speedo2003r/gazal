<div class="list-icons">
  <div class="main-icon">
    <img src="{{ client_path() }}/images/icons/greep-logo.png">
    <span>{{ awtTrans('اي شي') }}</span>
  </div>
  <ul class="list-unstyled">
    @foreach ($mainCategories as $mainCategory)
      <li>
        <a href=""
           wire:click.prevent="$emit('updateCategoryBranches', {{ collect($mainCategory) }}, {{ collect(request()->query()) }})">
          <img src="{{ $mainCategory->icon }}">
          <span>
            {{ $mainCategory->title }}
          </span>
        </a>
      </li>
    @endforeach
  </ul>
</div>
