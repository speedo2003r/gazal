<div class="your-order {{ !$showCart ? 'd-none' : '' }}">
  <h6>{{ awtTrans('طلبك') }}</h6>
  @foreach ($orderProducts as $orderProduct)
    <div class="order-element">
      <div class="order-flex">
        <span>
          {{ $orderProduct->qty }}
          x
          {{ $orderProduct->item->title }}
        </span>
        <h5>
          {{ round($orderProduct->_price(), 2) }}
          {{ awtTrans('ر.س') }}
        </h5>
      </div>
      <div class="order-flex">
        <button class="btn">
          <i class="fas fa-plus" wire:click="increaseItemQuantity({{ $orderProduct->id }})"></i>
        </button>
        <button class="btn" wire:click="decreaseItemQuantity({{ $orderProduct->id }})">
          <i class="fas fa-minus"></i>
        </button>
      </div>
    </div>
  @endforeach

  <form action="{{ route('client.orderSummary') }}">
    <button type="submit" class="stand-link btn-block">
      <span>
        {{ awtTrans('اطلب') }}
        {{ $count }}
        {{ awtTrans('بـــ') }}
        {{ $cartPrice }}
        {{ awtTrans('ر.س') }}
      </span>
    </button>
  </form>
</div>
