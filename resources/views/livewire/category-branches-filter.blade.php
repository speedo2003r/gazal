<div class="row">
  <div class="col-md-3">
    <div class="filter-right">
      <ul class="list-unstyled">
      @foreach ($providersCategories as $category)
        <li>
          <a href="" wire:click.prevent="updateBranches({{ $category->id }})">
            <span class="icon">
              <img src="{{ $mainCategory->icon }}">
            </span>
            {{ $category->title }}
          </a>
        </li>
      @endforeach
      </ul>
    </div>
  </div>
  <div class="col-md-9">
    <div class="row">
      {{-- <div class="col-sm-12">
        <div class="bradcrumb">
          <ul class="list-unstyled">
            <li><a href="#">الحسينية</a></li>
            <li><i class="fas fa-angle-left"></i></li>
            <li><a href="">الطعام</a></li>
            <li><i class="fas fa-angle-left"></i></li>
            <li><a href="">المطعم</a></li>
          </ul>
        </div>
      </div> --}}

      @foreach ($categoryBranches as $branch)
        <div class="col-lg-4 col-md-6">
          <a href="{{ route('client.providerDetails', $branch->branch_id) }}">
            @include('front.client.layouts.parts.food-card',['branch' => $branch])
          </a>
        </div>
      @endforeach

      <!-- pagination links -->
      {{-- <div class="col-sm-12">
        <div class="center-align">
          <ul class="list-unstyled pagination">
            <li class="prev">
              <a href=""><i class="fas fa-long-arrow-alt-right"></i></a>
            </li>
            <li><span>1-7</span></li>
            <li class="next"><a href=""><i
                   class="fas fa-long-arrow-alt-left"></i></a></li>
          </ul>
        </div>
      </div> --}}
    </div>
  </div>
</div>
