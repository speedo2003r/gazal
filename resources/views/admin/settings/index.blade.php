@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.Settings')}}</span></a>
    </div>
@endsection
    <section class="content-header row">


        <div class="card">
          <div class="container">
            <div class="row">
              <div class="col-md-4 col-12">
                <!-- Default box -->
                <div class="card card card-outline card-info">
                  <div class="card-body p-0">
                    <div class="set-tabs">
                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="setting-tab" data-toggle="tab" href="#setting" role="tab" aria-controls="setting" aria-selected="true">
                            <img src="{{dashboard_url('presentation.png')}}" alt="">
                            <span>{{__('admin.PrepareApplication')}}</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="location-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">
                            <img src="{{dashboard_url('map-placeholder.png')}}" alt="">
                            <span>{{__('admin.MapSettings')}}</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="true">
                            <img src="{{dashboard_url('presentation.png')}}" alt="">
                            <span>{{__('admin.SearchSettings')}}</span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="profile5-tab" data-toggle="tab" href="#profile5" role="tab" aria-controls="profile5" aria-selected="false">
                            <img src="{{dashboard_url('headphone.png')}}" alt="">
                            <span>{{__('admin.SupportSettings')}} </span>
                          </a>
                        </li>
                      </ul>

                    </div>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
              <div class="col-md-8 col-12">
                <!-- Default box -->
                <div class="card card card-outline card-info">
                  <div class="card-body p-0">
                    <div class="set-tabsContent">
                      <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="setting" role="tabpanel" aria-labelledby="setting-tab">
                          <form action="{{route('admin.settings.update')}}" method="post" id="updatesettingForm" class="dropzone" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                              <div class="form-group">
                                <label for="exampleInputEmail2">{{__('admin.ApplicationName')}}</label>
                                <input type="text" name="keys[site_name]" value="{{settings('site_name')}}" class="form-control" id="exampleInputEmail2" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">{{__('email')}}</label>
                                <input type="email" name="keys[email]" value="{{settings('email')}}" class="form-control" id="exampleInputEmail1" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">{{__('whatsapp')}}</label>
                                <input type="number" name="keys[whatsapp]" value="{{settings('whatsapp')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">{{__('phone')}}</label>
                                <input type="number" name="keys[phone]" value="{{settings('phone')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">{{__('admin.ServiceCommissionRelative')}}</label>
                                <input type="number" min="0" max="100" name="keys[provider_commission]" value="{{settings('provider_commission')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword2">{{__('phone')}} 2</label>
                                <input type="number" name="keys[phone2]" value="{{settings('phone2')}}" class="form-control" id="exampleInputPhone2" placeholder="">
                              </div>
{{--                              <div class="form-group">--}}
{{--                                <label for="exampleInputPassword2">{{__('tax')}}</label>--}}
{{--                                <input type="number" name="keys[tax]" value="{{settings('tax')}}" class="form-control" id="exampleInputPhone2" placeholder="">--}}
{{--                              </div>--}}
                              <div class="form-group">
                                <label for="exampleInputPassword2">{{__('admin.ShippingPerKilometer')}}</label>
                                <input type="number" name="keys[shippingValue]" value="{{settings('shippingValue')}}" class="form-control" id="exampleInputPhone2" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword2">{{__('admin.InitialShippingPrice')}}</label>
                                <input type="number" name="keys[shippingPrice]" value="{{settings('shippingPrice')}}" class="form-control" id="exampleInputPhone2" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword2">{{__('admin.InitialDeliveryRange')}}</label>
                                <input type="number" name="keys[minkm_shippingPrice]" value="{{settings('minkm_shippingPrice')}}" class="form-control" id="exampleInputPhone2" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputMapkey">{{__('admin.mapKey')}}</label>
                                <input type="text" name="keys[map_key]" value="{{settings('map_key')}}" class="form-control" id="exampleInputMapkey" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="input-file-now-custom-1">{{__('admin.logo')}}</label>
                                <input type="file" name="logo" id="input-file-now-custom-1" class="dropify" data-default-file="{{dashboard_url('storage/images/settings/'.settings('logo'))}}" />
                                @if(settings('logo'))
                                <div class="upload-area" id="upload_area_img">
                                  <div class="uploaded-block" data-count-order="1" style="position:relative;"><img class="logo" src="{{dashboard_url('storage/images/settings/'.settings('logo'))}}"><button class="close">&times;</button></div>
                                </div>
                                @endif
                              </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                              <button type="submit" class="btn btn-success save" style="width:100%">{{__('save')}}</button>
                            </div>
                          </form>
                        </div>
                        <div class="tab-pane fade show" id="location" role="tabpanel" aria-labelledby="location-tab">
                          <form action="{{route('admin.settings.update')}}" id="updatelocationForm" method="POST" class="dropzone">
                            @csrf
                            <div class="card-body">
                              <div class="form-group" style="position: relative;">
                                <input class="controls" id="pac-input" name="pac-input" value="" placeholder="{{__('admin.WriteLocation')}}" />
                                <input type="hidden" name="keys[lat]" id="lat" value="{{settings('lat')}}" readonly />
                                <input type="hidden" name="keys[lng]" id="lng" value="{{settings('lng')}}" readonly />
                                <div class="col-sm-12 add_map" id="map"></div>
                              </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                              <button type="submit" class="btn btn-success save" style="width:100%">{{__('save')}}</button>
                            </div>
                          </form>
                        </div>
                        <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                          <form action="{{route('admin.settings.update')}}" id="updateseoForm" method="POST" class="dropzone">
                            @csrf
                            <div class="card-body">
                              <div class="form-group pad">
                                <label>{{__('admin.description')}}</label>
                                <textarea class="form-control" name="keys[description]" placeholder="" rows="4">{{settings('description')}}</textarea>
                              </div>
                              <div class="form-group pad">
                                <label>{{__('admin.keyWords')}}</label>
                                <textarea class="form-control" name="keys[key_words]" placeholder="" rows="4">{{settings('key_words')}}</textarea>
                              </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                              <button type="submit" class="btn btn-success save" style="width:100%">{{__('save')}}</button>
                            </div>
                          </form>
                        </div>
                        <div class="tab-pane fade" id="profile5" role="tabpanel" aria-labelledby="mail-tab">
                          <form action="{{route('admin.settings.update')}}" id="updateseoForm" method="POST" class="dropzone">
                            @csrf
                            <div class="card-body">
                              <div class="form-group">
                                <label>{{__('admin.Type')}}</label>
                                <input type="text" name="keys[smtp_type]" value="{{settings('smtp_type')}}" class="form-control" required>
                              </div>
                              <div class="form-group">
                                <label>{{__('admin.username')}}</label>
                                <input type="text" name="keys[smtp_username]" value="{{settings('smtp_username')}}" class="form-control" required>
                              </div>
                              <div class="form-group">
                                <label>{{__('admin.passwordNum')}}</label>
                                <input type="text" name="keys[smtp_password]" value="{{settings('smtp_password')}}" class="form-control" required>
                              </div>
                              <div class="form-group">
                                <label>{{__('admin.EmailSender')}}</label>
                                <input type="text" name="keys[smtp_sender_email]" value="{{settings('smtp_sender_email')}}" class="form-control" required>
                              </div>
                              <div class="form-group">
                                <label>{{__('admin.SenderName')}}</label>
                                <input type="text" name="keys[smtp_sender_name]" value="{{settings('smtp_sender_name')}}" class="form-control" required>
                              </div>
                              <div class="form-group">
                                <label>{{__('admin.Port')}}</label>
                                <input type="text" name="keys[smtp_port]" value="{{settings('smtp_port')}}" class="form-control" required>
                              </div>
                              <div class="form-group">
                                <label>{{__('admin.host')}}</label>
                                <input type="text" name="keys[smtp_host]" value="{{settings('smtp_host')}}" class="form-control" required>
                              </div>
                              <div class="form-group">
                                <label>{{__('admin.Encryption')}}</label>
                                <input type="text" name="keys[smtp_encryption]" value="{{settings('smtp_encryption')}}" class="form-control" required>
                              </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                              <button type="submit" class="btn btn-success save" style="width:100%">{{__('save')}}</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</section>

@push('js')

<script>
  var map, infoWindow, geocoder;

  function initMap() {

      geocoder = new google.maps.Geocoder();
      infowindow = new google.maps.InfoWindow();

      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(showPosition);
      } else {
          innerHTML = {{__('admin.SiteOccurred')}};
      }

      function showPosition(position) {
          var Latitude = document.getElementById("lat").value;
          var Longitude = document.getElementById("lng").value;

          if((Latitude == '' || Latitude == null) && (Longitude == '' || Longitude == null)){
              Latitude = position.coords.latitude;
              Longitude = position.coords.longitude;
              document.getElementById("lat").value = Latitude;
              document.getElementById("lng").value = Longitude;
          }

          var latlng = new google.maps.LatLng(Latitude, Longitude);
          var map = new google.maps.Map(document.getElementById('map'), {
              center: latlng,
              zoom: 16,
              disableDefaultUI: true,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          });
          var marker = new google.maps.Marker({
              position: latlng,
              map: map,
              draggable: true
          });

          var searchBox = new google.maps.places.SearchBox( document.getElementById( 'pac-input' ) );
          google.maps.event.addListener( searchBox, 'places_changed', function () {
              var places = searchBox.getPlaces();
              var bounds = new google.maps.LatLngBounds();
              var i, place;
              for ( i = 0; place = places[ i ]; i++ ) {

                  bounds.extend( place.geometry.location );
                  marker.setPosition( place.geometry.location );

              }
              map.fitBounds( bounds );
              map.setZoom( 12 );
          } );
          geocoder.geocode({'latLng': latlng}, function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                  if (results[0]) {
                      $("input[name='pac-input']").val(results[0].formatted_address);
                      infowindow.setContent(results[0].formatted_address);
                      //infowindow.open(map, marker);
                  }
              }
          });

          google.maps.event.addListener(marker, 'dragend', function (event) {

              document.getElementById("lat").value = this.getPosition().lat();
              document.getElementById("lng").value = this.getPosition().lng();

              geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                      if (results[0]) {
                          $("input[name='pac-input']").val(results[0].formatted_address);
                          infowindow.setContent(results[0].formatted_address);
                          //infowindow.open(map, marker);
                      }
                  }
              });
          });
      }
  }
</script>
<script
      src="https://maps.googleapis.com/maps/api/js?key={{settings('map_key')}}&libraries=places&callback=initMap"
      async defer></script>
<script>
  console.log(true);
  $(document).on('click', '.close', function() {
    event.preventDefault();
    var that = $(this);
    var logo = 'remove';
    $.ajax({
      type: "POST",
      url: "{{route('admin.settings.update')}}",
      data: {
        logo: logo,
        _token: '{{csrf_token()}}'
      },
      success: function(msg) {
        $('.upload-area').remove();
      }
    });
  })
</script>

@endpush


@endsection
