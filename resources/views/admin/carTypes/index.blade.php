@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.typesOfCars')}}</span></a>
    </div>
@endsection

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <button type="button" data-toggle="modal" data-target="#addModel" class="btn btn-primary btn-wide waves-effect waves-light add-user">
                    <i class="fas fa-plus"></i> {{__('admin.AddCarType')}}
                </button>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                    <thead>
                    <tr>
                        <th>
                            <label class="custom-control material-checkbox" style="margin: auto">
                                <input type="checkbox" class="material-control-input" id="checkedAll">
                                <span class="material-control-indicator"></span>
                            </label>
                        </th>
                        <th>{{__('name')}}</th>
                        <th>{{__('admin.photo')}}</th>
                        <th>{{__('control')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cartypes as $ob)
                        <tr>
                            <td>
                                <label class="custom-control material-checkbox" style="margin: auto">
                                    <input type="checkbox" class="material-control-input checkSingle" id="{{$ob->id}}">
                                    <span class="material-control-indicator"></span>
                                </label>
                            </td>
                            <td>{{$ob->title}}</td>
                            <td><div style="width: 100px;height: 100px"><a href="{{$ob['image']}}"  data-fancybox data-caption="{{$ob['image']}}" ><img src="{{$ob['image']}}" style="border-radius: 50%;width: 100%;height: 100%"></a></div></td>
                            <td>
                                <a href="javascript:void(0)" onclick="confirmDelete('{{route('admin.carTypes.destroy',$ob->id)}}')" data-toggle="modal" data-target="#delete-model">
                                    <i class="feather icon-trash"></i>
                                </a>
                                <a href="javascript:void(0)" onclick="edit({{$ob}})" data-toggle="modal" data-target="#editModel"><i class="feather icon-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    @if(count($cartypes) > 0)
                    <tr>
                        <td colspan="3">
                            <button class="btn btn-danger confirmDel" disabled onclick="deleteAllData('more','{{route('admin.carTypes.destroy',$ob->id)}}')" data-toggle="modal" data-target="#confirm-all-del">
                                <i class="fas fa-trash"></i>
                                {{__('admin.DeleteSpecified')}}
                            </button>
                        </td>
                    </tr>
                    @endif
                </table>
                </div>
            </div>
        </div>
    </div>


 <!-- add model -->
 <div class="modal fade" id="addModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">{{__('admin.AddCarType')}}</h4></div>
            <form action="{{route('admin.carTypes.store')}}" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="uuid" value="uuid">
                <div class="modal-body">
                    <div class="row">
                        <div class = "col-sm-12 text-center">
                            <label class = "mb-0">{{__('avatar')}}</label>
                            <div class = "text-center">
                                <div class = "images-upload-block single-image">
                                    <label class = "upload-img">
                                        <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </label>
                                    <div class = "upload-area"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('name_ar')}}</label>
                                <input type="text" name="title_ar" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('name_en')}}</label>
                                <input type="text" name="title_en" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                    <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">{{__('close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end add model -->

<!-- edit model -->
<div class="modal fade" id="editModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">{{__('admin.ModifyTheCarType')}}</h4></div>
            <form action=""  id="editForm" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class = "col-sm-12 text-center">
                            <label class = "mb-0">{{__('avatar')}}</label>
                            <div class = "text-center">
                                <div class = "images-upload-block single-image">
                                    <label class = "upload-img">
                                        <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </label>
                                    <div class = "upload-area" id="upload_area_img"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('name_ar')}}</label>
                                <input type="text" id="title_ar" name="title_ar" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('name_en')}}</label>
                                <input type="text" id="title_en" name="title_en" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                    <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">{{__('close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit model -->
@endsection
@push('js')
    <script>
        function edit(ob){
            $('#editForm')      .attr("action","{{route('admin.carTypes.update','obId')}}".replace('obId',ob.id));
            $('#title_ar')    .val(ob.title.ar);
            $('#title_en')     .val(ob.title.en);

            let image = $( '#upload_area_img' );
            image.empty();
            image.append( '<div class="uploaded-block" data-count-order="1"><a href="' + ob.image + '"  data-fancybox data-caption="' + ob.image + '" ><img src="' + ob.image + '"></a><button class="close">&times;</button></div>' );
        }
    </script>
@endpush
