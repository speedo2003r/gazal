@extends('admin.layout.master')
@section('title',__('admin.EditProvider'))
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{__('providers')}}</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.providers.index') }}">{{__('providers')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.EditProvider')}} </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin.providers.update',$provider['id'])}}" id="editForm" method="post" role="form" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">

                                        <div class = "col-sm-12 text-center">
                                            <label class = "mb-0">{{__('avatar')}}</label>
                                            <div class = "text-center">
                                                <div class = "images-upload-block single-image">
                                                    <label class = "upload-img">
                                                        <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader" >
                                                        <i class="fas fa-cloud-upload-alt"></i>
                                                    </label>
                                                    <div class = "upload-area" id="upload_area_img">
                                                        <div class="uploaded-block" data-count-order="0">
                                                            <a href="{{$provider['avatar']}}" data-fancybox="{{$provider['avatar']}}" data-caption="{{$provider['avatar']}}">
                                                                <img src="{{$provider['avatar']}}"></a>
                                                            <button class="close" type="button">×</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('admin.username')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="text" name="username" id="username" value="{{$provider['username']}}" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('name')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="text" name="name" id="name" value="{{$provider['name']}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('admin.logo')}}</label>
                                                <input type="text" name="slogan" id="slogan" value="{{$provider['slogan']}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>{{__('admin.storenameArabic')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="text" name="store_name[ar]" value="{{$provider->getTranslation('store_name','ar')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>{{__('admin.storenameEnglish')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="text" name="store_name[en]" value="{{$provider->getTranslation('store_name','en')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.Type')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="category_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category['id']}}" @if($provider['category_id'] == $category['id']) selected @endif>{{$category['title']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('admin.Groups')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <div class="groups"></div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('phone')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="number" name="phone" value="{{$provider['phone']}}" class="form-control">
                                            </div>
                                            @foreach($provider->contactDetails()->where('type',2)->get() as $contact)
                                                <div class="form-group">
                                                    <label>{{__('phone')}}</label>
                                                    <input type="number" name="phones[]" value="{{$contact['data']}}" class="form-control">
                                                </div>
                                            @endforeach
                                            <button type="button" class="btn btn-success btn-sm add_phone" style="margin-top: -20px;">اضافة هاتف أخر</button>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('email')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="email" name="email" value="{{$provider['email']}}" class="form-control">
                                            </div>
                                            @foreach($provider->contactDetails()->where('type',1)->get() as $contact)
                                                <div class="form-group">
                                                    <label>{{__('email')}}</label>
                                                    <input type="email" name="emails[]" value="{{$contact['data']}}" class="form-control">
                                                </div>
                                            @endforeach
                                            <button type="button" class="btn btn-success btn-sm add_email" style="margin-top: -20px;">اضافة بريد الكتروني أخر</button>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('password')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="password" name="password" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Password Confirmation')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="password" name="password_confirmation" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Country')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="country_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country['id']}}" @if($provider['country_id'] == $country['id']) selected @endif>{{$country->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('City')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="city_id" id="city" class="form-control">
                                                    <option value="" selected hidden>{{__('Choose')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <div class="form-group">
                                                <label>{{__('Address')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="hidden" name="lat" id="lat" value="{{$provider['lat']}}">
                                                <input type="hidden" name="lng" id="lng" value="{{$provider['lng']}}">
                                                <input type="text" name="address" class="form-control pac-target-input" value="{{$provider['address']}}" placeholder="Enter a query" autocomplete="off">
                                                <div id="map" style="height: 300px"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.CommercialNo')}}</label>
                                                <input type="number" name="commercial_num" value="{{$provider['commercial_num']}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.BankNumber')}}</label>
                                                <input type="number" name="acc_bank" value="{{$provider['acc_bank']}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.ContractExpiryDate')}}</label>
                                                <input type="date" name="end_date_contract" id="end_date_contract" value="{{$provider['end_date_contract']}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.processingTime')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="number" name="processing_time" value="{{$provider['processing_time']}}" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="submit" class="btn btn-primary">{{__('save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    {!! $validator->selector('#editForm') !!}
    <script src="{{ dashboard_url('dashboard/assets/js/map.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ settings('map_key') }}&libraries=places&callback=initMap&lang=ar"
            async defer></script>
    <script>
        function getGroups(category_id,types = '',placeholder = `{{__('Choose')}}`){
            var html = '';
            $('.groups').empty();
            if(category_id){
                $.ajax({
                    url: `{{route('admin.ajax.getGroups')}}`,
                    type: 'post',
                    dataType: 'json',
                    data:{id: category_id},
                    success: function (res) {
                        $.each(res.data,function (index,value) {
                            html += `<label class="control-label" style="padding: 10px;"><input type="checkbox" name="group_id[]" value="${value.id}" ${types.includes(value.id.toString()) == true ? 'checked' : ''}>${value.title.ar}</label>`;
                        });
                        $('.groups').append(html);
                    }
                });
            }
        }

        $('body').on('click','.add_phone',function () {
            var html = `<div class="form-group">
                            <label>{{__('phone')}}</label>
                            <input type="number" name="phones[]" class="form-control">
                        </div>`;
            $(this).before(html);
        });
        $('body').on('click','.add_email',function () {
            var html = `<div class="form-group">
                            <label>{{__('email')}}</label>
                            <input type="text" name="emails[]" class="form-control">
                        </div>`;
            $(this).before(html);
        });
        $('body').on('change','[name=country_id]',function () {
            var id = $(this).val();
            getCities(id);
        });
        $('body').on('change','[name=category_id]',function () {
            var id = $(this).val();
            getGroups(id);
        });
        @if(count($provider->groups) > 0)
            var groups = {!!$provider->groups()->pluck('groups.id')!!};
            getGroups(`{{$provider['category_id']}}`,groups);
        @endif
        @if($provider['city_id'])
            getCities(`{{$provider['country_id']}}`,`{{$provider['city_id']}}`);
        @endif
    </script>

@endpush
