@extends('admin.layout.master')
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{__('admin.categoriesProvider')}}</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <button type="button" data-toggle="modal" data-target="#addModel" class="btn btn-primary btn-wide waves-effect waves-light">
                    <i class="fas fa-plus"></i> {{__('admin.AddCategory')}}
                </button>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                    <thead>
                    <tr>
                        <th>
                            <label class="custom-control material-checkbox" style="margin: auto">
                                <input type="checkbox" class="material-control-input" id="checkedAll">
                                <span class="material-control-indicator"></span>
                            </label>
                        </th>
                        <th>{{__('name')}}</th>
                        <th>{{__('control')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sellercategories as $ob)
                        <tr>
                            <td>
                                <label class="custom-control material-checkbox" style="margin: auto">
                                    <input type="checkbox" class="material-control-input checkSingle" id="{{$ob->id}}">
                                    <span class="material-control-indicator"></span>
                                </label>
                            </td>
                            <td>{{$ob->title}}</td>
                            <td>
                                <a href="javascript:void(0)" onclick="confirmDelete('{{route('admin.providers.deletecategories',[$ob->id,'user_id'=>$provider['id']])}}')" data-toggle="modal" data-target="#delete-model">
                                    <i class="feather icon-trash"></i>
                                </a>
                                <a href="javascript:void(0)" type="button" class="subs" onclick="edit({{$ob}})" data-toggle="modal" data-target="#editModel"><i class="feather icon-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    @if(count($sellercategories) > 0)
                        <tr>
                            <td colspan="30">
                                <button class="btn btn-danger confirmDel" disabled onclick="deleteAllData('more','{{route('admin.providers.deletecategories',$ob->id)}}')" data-toggle="modal" data-target="#confirm-all-del">
                                    <i class="fas fa-trash"></i>
                                    {{__('deleteSelected')}}
                                </button>
                            </td>
                        </tr>
                    @endif
                </table>
                </div>
            </div>
        </div>
    </div>


 <!-- add model -->
 <div class="modal fade" id="addModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">{{__('admin.AddCategory')}}</h4></div>
            <form action="{{route('admin.providers.postcategories')}}" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{$provider['id']}}" name="user_id">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_ar')}}</label>
                                <input type="text" name="title_ar" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_en')}}</label>
                                <input type="text" name="title_en" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-primary">{{__('save')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end add model -->

 <!-- add model -->
 <div class="modal fade" id="editModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">{{__('admin.EditCategory')}}</h4></div>
            <form action="" method="post" role="form" enctype="multipart/form-data" id="editForm">
                @csrf
                <input type="hidden" value="{{$provider['id']}}" name="user_id">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_ar')}}</label>
                                <input type="text" name="title_ar" id="title_ar" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_en')}}</label>
                                <input type="text" name="title_en" id="title_en" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-primary">{{__('save')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@push('js')
    <script>

        function edit(ob){
            $('#editForm')      .attr("action","{{route('admin.providers.updatecategories','obId')}}".replace('obId',ob.id));
            $('#title_ar')    .val(ob.title_ar);
            $('#title_en')     .val(ob.title_en);
        }
    </script>
@endpush
