@extends('admin.layout.master')
@section('title',__('admin.EditingProviderMenu'))
@section('content')
    @push('css')
        <style>
            [v-cloak]{
                display: none;
            }
            .modal-header .close{
                margin-right: 0;
            }
            select.form-control{
                height: 36px !important;
            }
            .small{
                font-size:9px;
                margin-bottom: 0;
            }
            .d-flex{
                display: flex !important;
            }
            .d-flex .form-group{
                width: 100% !important;
            }
            .checker input[type=checkbox], .choice input[type=radio]{
                opacity: 1;
            }
            .category-section{
                padding: 30px 70px 30px 120px !important
            }
            .branch-btn{
                margin-bottom: 20px;
                margin-right: 150px;
            }
            .text-danger, .text-danger:hover, .text-danger:focus{
                font-size: 10px;
            }
            .input-group-prepend {
                position: absolute !important;
                right: 0px !important;
                left: auto !important;
                height: 40px !important;
                z-index: 9 !important;
                line-height: 36px !important;
                color: #d5d5d5 !important;
            }
            .pin_btn{
                right: 15px;
                bottom: 15px;
                background: #404146;
                border-radius: 50%;
                border: 0;
                width: 30px;
                height: 30px;
                cursor: pointer;
                color: #fff;
            }
            .product_image_btn{
                position: absolute;
                bottom: 15px;
                left: 15px;
                background: #5dd5c4;
                border: 1px solid #5dd5c4;
                color: #fff;
                border-radius: 99px;
                padding: 3px 15px 4px;
                font-size: 12px;
                cursor: pointer;
            }
            .input-group-prepend{
                position: absolute;
                right: 0px;
                height: 42px;
                z-index: 9;
                color: #d5d5d5;
            }
            /*.input-group-text{*/
            /*    background-color: transparent;*/
            /*    border: none;*/
            /*}*/
            .btn-tiffany{
                padding: 15px 30px;
                height: auto;
                background: #5dd5c4;
                color: #fff;
                cursor: pointer;
            }
            .btn-tiffany:hover{
                color: #fff;
            }
            .form-control{
                line-height: 1.6em;
            }
            select.form-control:not([size]):not([multiple]){
                height: calc(2.50rem + 2px);
            }
            .select2,.select2-selection{
                height: calc(2.50rem + 2px);
                overflow: hidden;
            }
            .select2-selection__rendered{
                display: inline-flex !important;
            }
            .icofont{
                font-size: 18px;
            }
            .down-button,.up-button{
                background: transparent;
                color: #333;
            }
            .category-section{
                background: #f3f3f3;
                padding: 30px;
                border-radius: 15px;
            }
            #product-desc-tab,#product-options-tab{
                padding: 40px 20px;
            }
            .ck-editor{
                width: 100% !important;
            }
            .input-group-addon{
                border-radius: 0;
            }
            .slim-result{
                height: 150px;
            }
            .product-photo-meta{
                padding: 0 20px;
                background: #cccccc;
            }
            .product-data-row .input-group-addon{
                /*line-height: 37px;*/
                background: #ccc;
                width: 40px;
                text-align: center;
                color: #fff;
            }
            .content-page .content{
                margin-top: 0 !important;
            }
        </style>
    @endpush
    <div class="row">
            <div class="col-md-12">
                <form action="">
                    <div class="form-group">
                        <label for="">{{__('stores')}}</label>
                        <select name="provider_id" class="form-control select2">
                            <option value="" selected hidden>{{__('Choose')}}</option>
                            @foreach($sellers as $provider)
                                <option value="{{$provider['id']}}" @if(request('provider_id') == $provider['id']) selected @endif>{{$provider['store_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">{{__('send')}}</button>
                </form>
            </div>
    </div>
    @if(request('provider_id') != null)
    <div class="page-body" id="items" v-cloak>
        <div class="row">
            <div class="col-sm-12">
                <span class="pb-5 d-block">
                    @if(!isset($id))
                        <button type="button" v-cloak @click="addItem()" class="btn btn-tiffany" style="border-radius: 25px !important;height: auto !important;margin-top: 30px">{{awtTrans('اضافة منتج جديد')}}</button>
                    @endif
                </span>
                <!-- Product edit card start -->
                <div class="row">

                    <div class="col-md-3 col-sm-6 col-xs-12 product-box" v-for="form, index in forms" v-cloak>

                        <div class="product_conent">
                            <div class="card mb-4" style="padding: 2px;border-top: none">
                                <div class="thumbnail position-relative">
                                    <a v-bind:href="form.image"  data-fancybox v-bind:data-caption="form.image" v-if="form.image" ><img class="card-img-top w-100" style="height: 300px;object-fit: cover;" v-bind:src="form.image" alt="Card image cap"></a>
                                    <img class="card-img-top w-100" v-else style="height: 300px;object-fit: cover;" src="{{dashboard_url('images/placeholder.png')}}" alt="Card image cap">
                                    <button class="pin_btn position-absolute" v-on:click="changeStatus(index)" v-if="form.status == 1" type="button" title="{{awtTrans('اظهار المنتج')}}"><span class="fa fa-bookmark"></span></button>
                                    <button class="pin_btn position-absolute" v-on:click="changeStatus(index)" style="background: #ef6c00" v-else-if="form.status == 0" type="button" title="{{awtTrans('اظهار المنتج')}}"><span class="fa fa-bookmark"></span></button>
                                    <button class="pin_btn position-absolute" style="background: #d84315" v-else-if="form.status == 3" type="button" title="{{awtTrans('اظهار المنتج')}}"><span class="fa fa-bookmark"></span></button>
                                    <button class="product_image_btn" type="button" v-on:click="addImages(index)">{{awtTrans('اضافة صوره')}}</button>
                                </div>
                                <div class="card-body px-0">
                                    <div class="container" style="width: 100%">
                                        <div class="form-group">
                                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" style="padding: 0.2rem 0.6rem !important;" data-toggle="tab" v-bind:href="langHref('ar',index)" role="tab" aria-expanded="false">{{awtTrans('عربي')}}</a>
                                                    <div class="slide"></div>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" style="padding: 0.2rem 0.6rem !important;" data-toggle="tab" v-bind:href="langHref('en',index)" role="tab" aria-expanded="false">{{awtTrans('انجليزي')}}</a>
                                                    <div class="slide"></div>
                                                </li>
                                            </ul>
                                            <label class="small">{{awtTrans('الاسم')}}</label>
                                            <div class="tab-content">
                                                <div class="tab-pane active" v-bind:id="langId('ar',index)" role="tabpanel" aria-expanded="false">
                                                    <div class="input-group mb-3" title="{{awtTrans('الاسم بالعربي')}}">
                                                        <input type="text" class="form-control"  v-model="form.title_ar" placeholder="{{awtTrans('الاسم بالعربي')}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="fa fa-heading"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" v-bind:id="langId('en',index)" role="tabpanel" aria-expanded="false">
                                                    <div class="input-group mb-3" title="{{awtTrans('الاسم بالانجليزي')}}">
                                                        <input type="text" class="form-control" v-model="form.title_en" placeholder="{{awtTrans('الاسم بالانجليزي')}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="fa fa-heading"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row p-0">
                                                {{--                                                <div class="col-md-6">--}}
                                                {{--                                                    <div class="form-group">--}}
                                                {{--                                                        <label class="small">{{awtTrans('السعر')}}</label>--}}
                                                {{--                                                        <div class="input-group mb-3" title="{{awtTrans('السعر')}}">--}}
                                                {{--                                                            <input type="number" class="form-control" v-model="form.price" style="padding: 0" placeholder="{{awtTrans('السعر')}}">--}}
                                                {{--                                                            <div class="input-group-append">--}}
                                                {{--                                                                <span class="input-group-text" style="padding: 10px"><i class="fa fa-dollar-sign"></i></span>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                </div>--}}
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="small">{{awtTrans('القسم الفرعي')}}</label>
                                                        <select class="form-control" v-model="form.subcategory_id">
                                                            <option value="" selected hidden>{{awtTrans('قسم فرعي')}}</option>
                                                            <option v-for="child in form.subcategories" v-bind:value="child.id">@{{ child.title.ar }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div>
                                                        <button v-on:click="addMainFeature(index)" type="button" class="btn btn-block" style="color:#333;margin-top: 20px;font-size: 11px;background: transparent;border: 2px solid #000;cursor:pointer;height: 36px;line-height: 8px !important;">
                                                            {{awtTrans('بيانات أخري')}}<i class="icofont icofont-rounded-left"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" @click="save(index)" class="btn btn-tiffany btn-xs save-product btn-save" style="opacity: 1;padding: 7px 30px !important;">حفظ</button>

                                        <span class="nav-item dropdown" style="float: left;">
                                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down fa-2x"></i></a>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="javascript:void(0)" v-on:click="delItem(index)">{{awtTrans('حذف')}}</a>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <template  v-cloak>
                    <div class="d-block">
                        {{--                        @if(\Illuminate\Support\Facades\Route::currentRouteName() =='admin.items.create')--}}
                        {!! $items->render("pagination::bootstrap-4") !!}
                        {{--                        @endif--}}
                    </div>
                </template>

            </div>
            <!-- Product edit card end -->
        </div>
        @include('admin.item.modal.features')
        @include('admin.item.modal.files')
    </div>
    @endif
@endsection
@push('js')
    <script src="{{dashboard_url('dashboard/assets/js/vue.js')}}"></script>
    <script src="{{dashboard_url('dashboard/assets/js/axios.min.js')}}"></script>
    <script>
        Vue.component('v-select',{
            template: `<select class="form-control select2" data-allow-clear=true multiple><slot></slot></select>`,
            props:['value'],
            mounted: function () {
                var vm = this
                var arr = [];
                $(this.$el)
                    // init select2
                    .select2()
                    .val(this.value)
                    // emit event on change.
                    .on('change', function (ev, args) {
                        if (!(args && "ignore" in args)) {
                            vm.$emit('input', $(this).val())
                        }
                    });
            },
            watch: {
                value: function (value, oldValue) {
                    // update value
                    $(this.$el)
                        .val(value)
                        .trigger('change', { ignore: false });
                },
                options: function (options) {
                    // update options
                    $(this.$el).select2({ data: options })
                }
            },
            destroyed: function () {
                $(this.$el).off().select2('destroy')
            }
        });
        Vue.component('v-textarea',{
            template: `<textarea class="form-control editor" name="content" id="editor" :value="value"
         v-on:input="updateValue($event.target.value)" cols="30" rows="2"><slot></slot></textarea>`,
            data: function () {
                return {
                    instance: null
                }
            },
            props: ['value'],
            mounted(){
                var vm = this;
                ClassicEditor
                    .create( document.querySelector('.editor'), {
                        // The language code is defined in the https://en.wikipedia.org/wiki/ISO_639-1 standard.

                        language: `{{app()->getLocale()}}`,
                    } )
                    .then( editor => {
                        this.instance = editor;
                        editor.model.document.on( 'change:data', () => {
                            vm.$emit('input',editor.getData())
                        });
                    })
                    .catch( error => {
                        console.error( error );
                    } );
            },
            watch: {
                value: function(){
                    let html = this.instance.getData();
                    if(html != this.value){
                        this.instance.setData(this.value)
                    }
                }
            }
        });

        let items = new Vue({
            el: `#items`,
            data:{
                subcategories:[],
                types:[
                        @foreach($types as $type)
                    {
                        id: `{{$type['id']}}`,
                        title_ar: `{{$type['title'] != null ? $type->getTranslations('title')['ar'] : ''}}`,
                        title_en: `{{$type['title'] != null ? $type->getTranslations('title')['en'] : ''}}`,
                    },
                    @endforeach
                ],
                details:0,
                selectedTypeId:'',
                featureDetails:0,
                childFeatureDetails:0,
                groupIndex:0,
                typeValue:true,
                forms:[
                        @if(count($items) > 0)
                        @foreach($items as $item)
                    {
                        id:`{{$item['id'] ? $item['id'] : null}}`,
                        title_ar:`{{$item->getTranslations('title')['ar'] ?? ''}}`,
                        title_en:`{{$item->getTranslations('title')['en'] ?? ''}}`,
                        description_ar:`{!! $item->getTranslations('description')['ar'] ?? '' !!}`,
                        description_en:`{!! $item->getTranslations('description')['en'] ?? '' !!}`,
                        image:`{{$item->main_image}}`,
                        count:`{{$item['count']}}`,
                        price:`{{$item['price'] ?? 0}}`,
                        type:`{{$item['type']}}`,
                        user_id:`{{$item['user_id']}}`,
                        category_id:`{{$item->category_id}}`,
                        categories:[

                                @foreach($categories as $category)
                            {
                                id: `{{$category['id']}}`,
                                title: {
                                    'ar' : `{{$category->getTranslations('title')['ar']}}`,
                                    'en' : `{{$category->getTranslations('title')['en']}}`,
                                },
                            },
                            @endforeach
                        ],
                        subcategory_id:`{{$item->subcategory_id}}`,
                        subcategories:[
                                @if($item->user)
                                @if(count($item->user->categories) > 0)
                                @foreach($item->user->categories as $category)
                            {
                                id: `{{$category['id']}}`,
                                title: {
                                    'ar' : `{{$category->getTranslations('title')['ar']}}`,
                                    'en' : `{{$category->getTranslations('title')['en']}}`,
                                },
                            },
                            @endforeach
                            @endif
                            @endif

                        ],
                        {{--sku:`{{$item['sku']}}`,--}}
                            {{--discount_price:`{{$item['discount_price']}}`,--}}
                            {{--from:`{{$item->from}}`,--}}
                            {{--to:`{{$item->to}}`,--}}
                        weight:`{{$item['weight']}}`,
                        stock:`{{$item['stock']}}`,
                        features:[
                                @if(count($item->features) > 0)
                                @foreach($item->features as $feature)
                            {
                                id: `{{$feature['id']}}`,
                                title_ar: `{{$feature->getTranslations('title')['ar']}}`,
                                title_en: `{{$feature->getTranslations('title')['en']}}`,
                                price: `{{$feature['price']}}`,
                            },
                                @endforeach
                                @else
                            {
                                id: null,
                                title_ar: '',
                                title_en: '',
                                price: 0,
                            },
                            @endif
                        ],
                        branches:[
                            @if(count($item->branches) > 0)
                            @foreach($item->branches as $key => $branch)
                            `{{$branch['id']}}`,
                            @endforeach
                            @endif
                        ],
                        sellerBranches:[
                                @if($item->user != null && count($item->user->branches) > 0)
                                @foreach($item->user->branches as $branch)
                            {
                                id:`{{$branch['id']}}`,
                                title:`{{$branch['title']}}`,
                            },
                            @endforeach
                            @endif
                        ],
                        checkedBranch:[
                            @if(count($item->branches) > 0)
                            @foreach($item->branches as $key => $branch)
                            `{{$branch['id']}}`,
                            @endforeach
                            @endif
                        ],
                        types:[
                                @if(count($item->types) > 0)
                                @foreach($item->types as $type)
                            {
                                id: `{{$type['id']}}`,
                                item_id:`{{$type['item_id']}}`,
                                type_id:`{{$type['type_id']}}`,
                                children: [
                                        @foreach($type->children as $child)
                                    {
                                        id:`{{$child['id']}}`,
                                        title_ar: `{{$child->getTranslations('title')['ar']}}`,
                                        title_en: `{{$child->getTranslations('title')['en']}}`,
                                        price:`{{$child['price']}}`,
                                    },
                                    @endforeach
                                ]
                            },
                                @endforeach
                                @else
                            {
                                id: null,
                                item_id:'',
                                type_id:'',
                                children: [

                                    {
                                        id:'',
                                        title_ar: '',
                                        title_en: '',
                                        price:0,
                                    },
                                ]
                            },
                            @endif
                        ],
                        selected:`{{$item->main() ? $item->main()['id'] : 0}}`,
                        status:`{{$item['status']}}`,
                        stock_count:`{{$item['stock_count']}}`,

                        errors: [],
                    },
                        @endforeach
                        @else
                    {
                        id:null,
                        title_ar:'',
                        title_en:'',
                        count:0,
                        price:0,
                        // sku:'',
                        // discount_price:'',
                        // from:'',
                        // to:'',
                        description_ar:'',
                        description_en:'',
                        type:'',
                        typeValue:true,
                        weight:'',
                        shipweight:'',
                        stock:'',
                        stock_count:0,
                        category_id:'',
                        subcategory_id:'',
                        branches:[],
                        sellerBranches:[],
                        checkedBranch:[],
                        subcategories:[],
                        user_id:`{{request('provider_id')}}`,
                        features:[
                            {
                                id:'',
                                title_ar:'',
                                title_en:'',
                                price:0,
                            }
                        ],
                        types:[],
                        files:[],
                        selected:0,
                        status:0,
                        image:'',
                        errors: [],
                    },
                    @endif
                ],
            },
            methods:{
                addItem(){
                    this.forms.unshift({
                        id:null,
                        title_ar:'',
                        title_en:'',
                        count:0,
                        price:0,
                        // sku:'',
                        // discount_price:'',
                        // from:'',
                        // to:'',
                        description_ar:'',
                        description_en:'',
                        type:'',
                        typeValue:true,
                        weight:'',
                        shipweight:'',
                        stock:'',
                        stock_count:0,
                        category_id:'',
                        subcategory_id:'',
                        branches:[],
                        subcategories:[],
                        sellerBranches:[],
                        checkedBranch:[],
                        user_id:`{{request('provider_id')}}`,
                        features:[
                            {
                                id:'',
                                title_ar:'',
                                title_en:'',
                                price:0,
                            }
                        ],
                        types:[
                            {
                                id:'',
                                title_ar:'',
                                title_en:'',
                                children:[],
                            }
                        ],
                        files:[],
                        selected:0,
                        status:0,
                        image:'',
                        errors: [],
                    });
                    $('.dropdown-toggle').dropdown();
                },
                addImages(index){
                    if (this.forms[index].id != null){
                        this.details = index;
                        this.forms[index].files.filter((value,fileindex)=>{
                            value.id == this.forms[index].selected;
                        });
                        $('.modal_product_files').modal('toggle')
                    }else{
                        toastr.error(`يرجي حفظ المنتج قبل الاستكمال`)
                    }
                },
                addOption(index){
                    this.forms[index].types.push({
                        id:null,
                        item_id:'',
                        type_id:'',
                        children:[
                            {
                                id:'',
                                title_ar: '',
                                title_en: '',
                                price: 0,
                            }
                        ]
                    })
                },
                addDetails(index,typeindex){
                    this.forms[index].types[typeindex].children.push({
                        id:null,
                        price:0,
                        title_ar:'',
                        title_en:'',
                    })
                },
                changeStatus(index){
                    if (this.forms[index].id != null){
                        var that = this;
                        axios.post(`/admin/items/changeStatus`,{id: this.forms[index].id}).then((res)=> {
                            that.forms[index].status = res.data;
                        });
                    }else{
                        toastr.error(`يرجي حفظ المنتج قبل الاستكمال`)
                    }
                },
                getSellerCategories(index){
                    if(this.forms[index].user_id == ''){
                        this.forms[index].subcategories = [];
                        this.forms[index].category_id = '';
                        return;
                    }
                    axios.post(`{{route('admin.items.SellerCategories')}}`,{id: this.forms[index].user_id}).then((res)=>{
                        this.forms[index].subcategories = res.data.categories;
                        this.forms[index].sellerBranches = res.data.branches;
                        this.forms[index].category_id = '';
                    });
                },
                addFileFeature(index,indexChild){
                    this.featureDetails = index;
                    this.childFeatureDetails = indexChild;
                    $('#modal_product_addFileFeature').modal('toggle');
                },
                fileMainfeature(i,details){
                    this.forms[details].features[this.featureDetails].children[this.childFeatureDetails].image_id = this.forms[details].files[i].id;
                    this.forms[details].features[this.featureDetails].children[this.childFeatureDetails].image = this.forms[details].files[i].image;
                },
                fileMainChange(i,index){
                    axios.post(`{{route('admin.items.changeMain')}}`,{id: this.forms[index].files[i].id}).then((res)=>{
                        this.forms[index].image = res.data;
                    })

                },
                delImage(i,index){
                    axios.post(`/admin/delimage`,{id: this.forms[index].files[i].id}).then((res)=>{
                        var that = this;
                        if(res.data == true){
                            if(this.forms[index].files[i].image == this.forms[index].image){
                                that.forms[index].image = '';
                            }
                            that.$delete(this.forms[index].files,i)
                            toastr.success(`تم الحذف بنجاح`);

                        }
                    })

                },
                delItem(index){
                    if (this.forms[index].id != null){
                        axios.post(`{{route('admin.items.delete')}}`,{id:this.forms[index].id}).then((res)=>{
                            if(res.data == true){
                                toastr.success(`تم الحذف بنجاح`,
                                    {
                                        onHidden: function () {
                                            this.$delete(this.forms,index)
                                        }
                                    });
                            }
                        })
                    }else{
                        this.$delete(this.forms,index)
                    }

                },
                delFeature(index,featureIndex){
                    var that = this;
                    if(that.forms[index].features[featureIndex].id == null){
                        that.$delete(that.forms[index].features,featureIndex);
                    }else {
                        axios.post(`{{route('admin.items.deleteFeature')}}`, {
                            feature_id: that.forms[index].features[featureIndex].id,
                            item_id: that.forms[index].id,
                        }).then((res) => {
                            if(res.data.value == 0){
                                toastr.error(res.data.msg);
                            }else{
                                that.$delete(that.forms[index].features,featureIndex);
                                toastr.success(`تم الحذف بنجاح`);
                            }
                        });
                    }
                },
                delDetail(index,typeindex,detailIndex){
                    var that = this;
                    if(that.forms[index].types[typeindex].children[detailIndex].id == null){
                        that.$delete(that.forms[index].types[typeindex].children,detailIndex);
                    }else {
                        axios.post(`{{route('admin.items.deleteDetail')}}`, {
                            detail_id: that.forms[index].types[typeindex].children[detailIndex].id,
                            item_id: that.forms[index].id,
                        }).then((res) => {
                            if(res.data.value == 0){
                                toastr.error(res.data.msg);
                            }else{
                                that.$delete(that.forms[index].types[typeindex].children,detailIndex);
                                toastr.success(`تم الحذف بنجاح`);
                            }
                        });
                    }
                },
                delChild(index,typeindex){
                    var that = this;
                    if(that.forms[index].types[typeindex].id == null){
                        that.$delete(that.forms[index].types,typeindex);
                    }else {
                        axios.post(`{{route('admin.items.deleteTypeOption')}}`, {
                            option_id: that.forms[index].types[typeindex].id,
                            item_id: that.forms[index].id,
                        }).then((res) => {
                            if(res.data.value == 0){
                                toastr.error(res.data.msg);
                            }else{
                                that.$delete(this.forms[index].types,typeindex);
                                toastr.success(`تم الحذف بنجاح`);
                            }
                        });
                    }
                },
                delType(index,typeindex){
                    var that = this;
                    if(that.forms[index].types[typeindex].id == null){
                        that.$delete(that.forms[index].types,typeindex);
                    }else {
                        axios.post(`{{route('admin.items.deleteType')}}`, {
                            type_id: that.forms[index].types[typeindex].id,
                            item_id: that.forms[index].id,
                        }).then((res) => {
                            if(res.data.value == 0){
                                toastr.error(res.data.msg);
                            }else{
                                that.forms[index].types = res.data.types;
                                toastr.success(`تم الحذف بنجاح`);
                            }
                        });
                    }
                },
                addFeature(){
                    $('#modal_product_addFeatures').modal('toggle')
                },
                checkType(typeindex){
                    var that = this;
                    var type = that.forms[that.details].types[typeindex];
                    var types = that.forms[that.details].types;
                    type.item_id = that.forms[that.details].id;
                    that.selectedTypeId = type.type_id;
                },
                addNewFeature(){
                    this.features.push({
                        id: null,
                        title_ar: '',
                        title_en: '',
                        price: 0,
                    });
                },
                saveFeature(index){
                    axios.post(`{{route('admin.items.saveFeatures')}}`,{features : this.forms[index].features,item_id: this.forms[index].id}).then((res)=>{
                        var that = this;
                        if(res.data.value == 0){
                            toastr.error(res.data.msg);
                        }else{
                            that.forms[index].features = res.data.features;
                            toastr.success(`تم الحفظ بنجاح`);
                        }
                    })
                },
                saveItemTypes(details){
                    try{
                        if(this.forms[details].types[0].title_ar != ""){
                            axios.post(`{{route('admin.items.saveItemTypes')}}`,{types : this.forms[details].types,item_id:this.forms[details].id}).then((res)=>{
                                if(res.data.value == 0){
                                    toastr.error(res.data.msg);
                                }else{
                                    this.forms[details].types = res.data.types;
                                    toastr.success(`تم الحفظ بنجاح`);
                                }
                            })
                        }
                    }catch(error){
                        toastr.error(`يجب استكمال حقول المنتج الخاليه`);
                    }
                },
                addMainFeature(index){
                    if (this.forms[index].id != null){
                        this.details = index;
                        $('.modal_product_feature .nav-tabs li:first-child').addClass('active').siblings().removeClass('active')
                        $('.modal_product_feature .tab-content .tab-pane:first-child').addClass('active').addClass('in').siblings().removeClass('active').removeClass('in')
                        $('.modal_product_feature').modal('toggle')
                    }else{
                        toastr.error(`يرجي أولا الحفظ قبل الاستكمال`);
                    }
                },
                langHref(lang,index){
                    return `#${lang}${index}`;
                },
                langId(lang,index){
                    return `${lang}${index}`;
                },
                modelHref(index){
                    return `.modal_product_feature${index}`
                },
                dropdownMenuHref(index){
                    return `modal_product_feature${index}`
                },
                dropdownMenuRoute(index){
                    return `.modal_product_feature${index}`
                },
                addFeatureDetails(index){
                    this.forms[index].features.push({
                        id:null,
                        title_ar:'',
                        title_en:'',
                        price:0,
                    })
                },
                save(index){
                    this.checkForm(index);
                    let check = '';
                    check = this.forms[index].price >= 0 && this.forms[index].user_id != '' && this.forms[index].title_ar != '' && this.forms[index].title_en != '';
                    if (check){
                        axios.post(`{{route('admin.items.store')}}`,this.forms[index]).then((res)=>{
                            if(this.forms[index].id == null){
                                this.forms[index].id = res.data.id;
                            }
                            this.forms[index].status = res.data.status;
                            toastr.success(`تم الحفظ بنجاح`);
                        })
                    }
                },
                saveDetails(index){
                    this.checkDetailsForm(index);
                    if (this.forms[index].description_en != '' && this.forms[index].description_ar != ''){
                        axios.put(`/admin/items/update/${this.forms[index].id}`,this.forms[index]).then((res)=>{
                            toastr.success(`تم الحفظ بنجاح`,
                                {
                                    onHidden: function () {
                                        res.data.forEach((value,featureindex)=>{
                                            this.forms[index].features[featureindex].id = value.id
                                            value.children.forEach((childvalue,childindex)=>{
                                                this.forms[index].features[featureindex].children[childindex].id = parseInt(childvalue.id);
                                            })
                                        })
                                    }
                                });
                        })
                    }
                },
                checkForm: function (index) {
                    if (!this.forms[index].title_ar) {
                        toastr.error(`الاسم بالعربي مطلوب`);
                    }
                    if (!this.forms[index].title_en) {
                        toastr.error(`الاسم بالانجليزي مطلوب`);
                    }
                    if (this.forms[index].price === null || this.forms[index].price === '') {
                        toastr.error(`السعر مطلوب`);
                    }
                    if (!this.forms[index].subcategory_id) {
                        toastr.error(`القسم الفرعي مطلوب`);
                    }
                    if (!this.forms[index].user_id) {
                        toastr.error(`البائع مطلوب`);
                    }
                },
                checkDetailsForm: function (index) {
                    if (!this.forms[index].description_ar) {
                        toastr.error(`الوصف مطلوب بالعربي`);
                    }
                    if (!this.forms[index].description_en) {
                        toastr.error(`الوصف مطلوب بالانجليزي`);
                    }
                },
                checkDetailsGroup: function (index,groupIndex) {
                    if (!this.forms[index].groups[groupIndex].count) {
                        toastr.error(`الكميه مطلوبه`);
                    }
                    if (!this.forms[index].groups[groupIndex].price) {
                        toastr.error(`السعر مطلوب`);
                    }
                    if (!this.forms[index].groups[groupIndex].image_id) {
                        toastr.error(`الصوره مطلوبه`);
                    }
                }
            },
            mounted(){

                $('.dropdown-toggle').dropdown();
                const inputElement = document.querySelector('input[class="filepond"]');
                const pond = FilePond.create( inputElement );
                var tokenElement = document.head.querySelector('meta[name="csrf-token"]');
                var token;


                FilePond.setOptions({
                    server: {
                        // process:(fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
                        process: (fieldName, file, metadata, load, error, progress, abort) => {

                            // set data
                            const formData = new FormData();
                            formData.append(fieldName, file, file.name);
                            formData.append('id',this.forms[this.details].id)

                            // related to aborting the request
                            const CancelToken = axios.CancelToken;
                            const source = CancelToken.source();

                            // the request itself
                            axios({
                                method: 'post',
                                url: '/admin/items/files',
                                data: formData,
                                cancelToken: source.token,
                                onUploadProgress: (e) => {
                                    // updating progress indicator
                                    progress(e.lengthComputable, e.loaded, e.total);
                                }
                            }).then(response => {
                                // passing the file id to FilePond
                                this.forms[this.details].files = [];
                                response.data.forEach((value,fileindex)=>{
                                    this.forms[this.details].files.push({
                                        id:value.id,
                                        image:value.image,
                                    })
                                });
                                load(response.data.id)
                            }).catch((thrown) => {
                                if (axios.isCancel(thrown)) {
                                    console.log('Request canceled', thrown.message);
                                } else {
                                    // handle error
                                }
                            });

                            // Setup abort interface
                            return {
                                abort: () => {
                                    source.cancel('Operation canceled by the user.');
                                }
                            };
                        }
                    }
                })
            }
        });
    </script>
    <style>
        .dropdown-toggle::after{
            content: none !important;
        }
    </style>
    <script>
        $('.select2').select2({
            placholder: `{{__('Choose')}}`
        });
    </script>
@endpush
