@extends('admin.layout.master')
@section('title',__('admin.AddProvider'))
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{__('providers')}}</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.providers.index') }}">{{__('providers')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.AddProvider')}} </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin.providers.store')}}" id="editForm" method="post" role="form" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">

                                        <div class = "col-sm-12 text-center">
                                            <label class = "mb-0">{{__('avatar')}}</label>
                                            <div class = "text-center">
                                                <div class = "images-upload-block single-image">
                                                    <label class = "upload-img">
                                                        <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader" >
                                                        <i class="fas fa-cloud-upload-alt"></i>
                                                    </label>
                                                    <div class = "upload-area" id="upload_area_img"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.MarketingRepresentative')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="representative_id" id="representative" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($representatives as $representative)
                                                        <option value="{{$representative['id']}}">{{$representative['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label></label>
                                                        <select class="form-control role">
                                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                                            @foreach($roles as $role)
                                                                <option value="{{$role['id']}}">{{$role['title']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>{{__('admin.accountManager')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                        <select name="account_manager_id" id="accountManager" class="form-control">
                                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label></label>
                                                        <select class="form-control role">
                                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                                            @foreach($roles as $role)
                                                                <option value="{{$role['id']}}">{{$role['title']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>{{__('admin.ActivationResponsible')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                        <select name="active_responsible_id" id="active_responsible" class="form-control">
                                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label></label>
                                                        <select class="form-control role">
                                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                                            @foreach($roles as $role)
                                                                <option value="{{$role['id']}}">{{$role['title']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>{{__('admin.accountant')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                        <select name="accountant_id" id="accountant" class="form-control">
                                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('admin.username')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="text" name="username" id="username" value="{{old('username')}}" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('name')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('admin.logo')}}</label>
                                                <input type="text" name="slogan" id="slogan" value="{{old('slogan')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>{{__('admin.storenameArabic')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="text" name="store_name[ar]" id="store_name_ar" value="{{old('store_name.ar')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>{{__('admin.storenameEnglish')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="text" name="store_name[en]" id="store_name_en" value="{{old('store_name.en')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.Type')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="category_id" id="category_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category['id']}}" @if(old('category_id') == $category['id']) selected @endif>{{$category['title']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('admin.Groups')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <div class="groups"></div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('phone')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="number" name="phone" id="phone" value="{{old('phone')}}" class="form-control">
                                            </div>
                                            <button type="button" class="btn btn-success btn-sm add_phone" style="margin-top: -20px;">اضافة هاتف أخر</button>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('email')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="email" name="email" id="email" value="{{old('email')}}" class="form-control">
                                            </div>
                                            <button type="button" class="btn btn-success btn-sm add_email" style="margin-top: -20px;">اضافة بريد الكتروني أخر</button>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('password')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="password" name="password" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Password Confirmation')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="password" name="password_confirmation" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Country')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="country_id" id="country_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country['id']}}" @if(old('country_id') == $country['id']) selected @endif>{{$country->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('City')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="city_id" id="city" class="form-control">
                                                    <option value="" selected hidden>{{__('Choose')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <div class="form-group">
                                                <label>{{__('Address')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="hidden" name="lat" id="lat" value="24.7135517">
                                                <input type="hidden" name="lng" id="lng" value="46.67529569999999">
                                                <input type="text" name="address" id="address" class="form-control pac-target-input" value="" placeholder="Enter a query" autocomplete="off">
                                                <div id="map" style="height: 300px"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.CommercialNo')}}</label>
                                                <input type="number" name="commercial_num" id="commercial_num" value="{{old('commercial_num')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.BankNumber')}}</label>
                                                <input type="number" name="acc_bank" id="acc_bank" value="{{old('acc_bank')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.ContractExpiryDate')}}</label>
                                                <input type="date" name="end_date_contract" id="end_date_contract" value="{{old('end_date_contract')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.processingTime')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="number" name="processing_time" id="processing_time" value="{{old('processing_time')}}" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="submit" class="btn btn-primary">{{__('save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Admin\Provider\Create') !!}
    <script src="{{dashboard_url('dashboard/assets/js/map.js')}}"></script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key={{settings('map_key')}}&libraries=places&callback=initMap&lang=ar"
            async defer></script>
    <script>
        function getGroups(category_id,type = '',placeholder = `{{__('Choose')}}`){
            var html = '';
            $('.groups').empty();
            if(category_id){
                $.ajax({
                    url: `{{route('admin.ajax.getGroups')}}`,
                    type: 'post',
                    dataType: 'json',
                    data:{id: category_id},
                    success: function (res) {
                        $.each(res.data,function (index,value) {
                            html += `<label class="control-label" style="padding: 10px;"><input type="checkbox" name="group_id[]" value="${value.id}">${value.title.ar}</label>`;
                        });
                        $('.groups').append(html);
                    }
                });
            }
        }
        $('body').on('change','.role',function (){
            var role = $(this).val();
            var html = '';
            var that = $(this);
            $.ajax({
                url: `{{route('admin.ajax.getAdminByRole')}}`,
                type: 'post',
                dataType: 'json',
                data: {role: role},
                success:function (res){
                    that.parent().parent().siblings('.col-sm-6').find('select').empty();
                    html += `<option value="" selected hidden>{{__('Choose')}}</option>`;
                    $.each(res,function (index,value) {
                        html += `<option value="${value.id}">${value.name}</option>`;
                    });
                    that.parent().parent().siblings('.col-sm-6').find('select').append(html);
                }
            });
        });
        $('body').on('click','.add_phone',function () {
            var html = `<div class="form-group">
                            <label>{{__('phone')}}</label>
                            <input type="number" name="phones[]" class="form-control">
                        </div>`;
            $(this).before(html);
        });
        $('body').on('click','.add_email',function () {
            var html = `<div class="form-group">
                            <label>{{__('email')}}</label>
                            <input type="number" name="emails[]" class="form-control">
                        </div>`;
            $(this).before(html);
        });
        $('body').on('change','[name=country_id]',function () {
            var id = $(this).val();
            getCities(id);
        });
        $('body').on('change','[name=category_id]',function () {
            var id = $(this).val();
            getGroups(id);
        });
    @if(old('category_id'))
        getGroups(`{{old('category_id')}}`);
    @endif
</script>
@endpush
