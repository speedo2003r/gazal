@extends('admin.layout.master')
@section('content')


    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{__('admin.imageFilesProvider')}}</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box card page-body p-5">
                <div class="container">
                    <form action="{{route('admin.providers.storeImages',$provider['id'])}}" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label class="col-lg-3 control-label">{{__('admin.logoProvider')}}</label>
                            <div class="col-lg-9">
                                <div class = "images-upload-block single-image">
                                    <label class = "upload-img">
                                        <input type = "file" name = "_logo" id = "logo" accept = "image/*" class = "image-uploader">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </label>
                                    <div class = "upload-area">
                                        <div class="uploaded-block" data-count-order="1">
                                            <a href="{{$provider->provider->logo}}" data-fancybox data-caption="{{$provider->provider->logo}}" ><img src="{{$provider->provider->logo}}"></a>
                                            <button class="close">&times;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label">{{__('admin.bannerProvider')}}</label>
                            <div class="col-lg-9">
                                <div class = "images-upload-block single-image">
                                    <label class = "upload-img">
                                        <input type = "file" name = "_banner" id = "banner" accept = "image/*" class = "image-uploader">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </label>
                                    <div class = "upload-area">
                                        <div class="uploaded-block" data-count-order="1">
                                            <a href="{{$provider->provider->banner}}" data-fancybox data-caption="{{$provider->provider->banner}}"><img src="{{$provider->provider->banner}}"></a>
                                            <button class="close">&times;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label">{{__('admin.imgIdentity')}}</label>
                            <div class="col-lg-9">
                                <div class = "images-upload-block single-image">
                                    <label class = "upload-img">
                                        <input type = "file" name = "_id_avatar" id = "id_avatar" accept = "image/*" class = "image-uploader">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </label>
                                    <div class = "upload-area">
                                        <div class="uploaded-block" data-count-order="1">
                                            <a href="{{$provider->provider->id_avatar}}" data-fancybox data-caption="{{$provider->provider->id_avatar}}"><img src="{{$provider->provider->id_avatar}}"></a>
                                            <button class="close">&times;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label">{{__('admin.commercialProvider')}}</label>
                            <div class="col-lg-9">
                                <div class = "images-upload-block single-image">
                                    <label class = "upload-img">
                                        <input type = "file" name = "_commercial_image" id = "commercial_image" accept = "image/*" class = "image-uploader">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </label>
                                    <div class = "upload-area">
                                        <div class="uploaded-block" data-count-order="1">
                                            <a href="{{$provider->provider->commercial_image}}" data-fancybox data-caption="{{$provider->provider->commercial_image}}"><img src="{{$provider->provider->commercial_image}}"></a>
                                            <button class="close">&times;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label">{{__('admin.taxCertificate')}}</label>
                            <div class="col-lg-9">
                                <div class = "images-upload-block single-image">
                                    <label class = "upload-img">
                                        <input type = "file" name = "_tax_certificate" id = "tax_certificate" accept = "image/*" class = "image-uploader">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </label>
                                    <div class = "upload-area">
                                        <div class="uploaded-block" data-count-order="1">
                                            <a href="{{$provider->provider->tax_certificate}}" data-fancybox data-caption="{{$provider->provider->tax_certificate}}"><img src="{{$provider->provider->tax_certificate}}"></a>
                                            <button class="close">&times;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label">{{__('admin.MunicipalLicense')}}</label>
                            <div class="col-lg-9">
                                <div class = "images-upload-block single-image">
                                    <label class = "upload-img">
                                        <input type = "file" name = "_municipal_license" id = "municipal_license" accept = "image/*" class = "image-uploader">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </label>
                                    <div class = "upload-area">
                                        <div class="uploaded-block" data-count-order="1">
                                            <a href="{{$provider->provider->municipal_license}}" data-fancybox data-caption="{{$provider->provider->municipal_license}}"><img src="{{$provider->provider->municipal_license}}"></a>
                                            <button class="close">&times;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">{{__('save')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
