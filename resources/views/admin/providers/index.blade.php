@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('providers')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <a href="{{route('admin.providers.create')}}" class="btn btn-primary btn-wide waves-effect waves-light add-user">
                    <i class="fas fa-plus"></i> {{__('admin.AddNewProvider')}}
                </a>
                <button class="btn btn-warning btn-wide waves-effect waves-light" data-toggle="modal" data-target="#change-schedules">
                    <i class="fas fa-clock-o"></i>{{__('admin.ChangeScheduleDate')}}
                </button>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    {!! $dataTable->table([
                     'class' => "table table-striped table-bordered dt-responsive nowrap",
                     'id' => "providerdatatable-table",
                     ],true) !!}
                </div>
            </div>
        </div>
    </div>


<!-- send-noti modal-->
<div class="modal fade" id="change-schedules"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('admin.ChangeScheduleDate')}}</h5>
            </div>
            <div class="modal-body">
                <form action="{{route('admin.providers.changeSchedule')}}" id="sendnotifyuserForm" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="">
                            {{__('admin.Schedule')}}
                        </label>
                        <select name="schedule" id="schedule" class="form-control">
                            <option value="" selected>{{__('Choose')}}</option>
                            <option value="1">{{__('admin.normalDate')}}</option>
                            <option value="2">{{__('admin.ramadanDate')}}</option>
                        </select>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-sm btn-success save" onclick="sendnotifyuser()">إرسال</button>
                        <button type="button" class="btn btn-default" id="notifyClose" data-dismiss="modal">اغلاق</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--end send-noti modal-->
@endsection
@push('js')
    {!! $dataTable->scripts() !!}
@endpush
