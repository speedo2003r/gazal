@extends('admin.layout.master')
@section('title',__('admin.providerDataShow'))
@section('content')
@push('css')
    <style>
        :root {
            --star-size: 15px;
            --star-color: #ccc;
            --star-background: #333;
        }
        .Stars {
            --percent: calc(var(--rating) / 5 * 100%);
            display: inline-block;
            font-size: var(--star-size);
            font-family: Times;
            line-height: 1;
        }
        .Stars::before {
            content: "★★★★★";
            letter-spacing: 3px;
            background: linear-gradient(90deg, var(--star-background) var(--percent), var(--star-color) var(--percent));
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
    </style>

@endpush

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{__('providers')}}</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.providers.index') }}">{{__('providers')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.providerShow')}} ({{ $provider->name }})</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="users-view-image">
                                    <img src="{{ $provider->avatar }}" class="users-avatar-shadow w-100 rounded mb-2 pr-2 ml-1" alt="avatar">
                                </div>
                                <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                    <table>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('name') }}</td>
                                            <td>{{ $provider->name }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('email') }}</td>
                                            <td>{{  $provider->email }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('phone') }}</td>
                                            <td>{{  $provider->phone }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.walletBalance') }}</td>
                                            <td>{{  $provider->wallet }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.NumberOfOrders') }}</td>
                                            <td>{{  $provider->orders()->count() }}</td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="col-12 col-md-12 col-lg-5">
                                    <table class="ml-0 ml-sm-0 ml-lg-0">
                                        <tr>
                                            <td class="font-weight-bold">{{ __('created_at') }}</td>
                                            <td>{{  date('Y-m-d h:i a',strtotime($provider->created_at)) }}</td>
                                        </tr>
                                        <?php
                                        $incomesView = \App\Entities\IncomesView::where('user_id',$provider['id'])->first();
                                        ?>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.indebtedness') }}</td>
                                            <td>{{  $incomesView ? $incomesView['balance'] : 0 }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.income') }}</td>
                                            <td>{{  $incomesView ? $incomesView['income'] : 0 }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.totalOrders') }}</td>
                                            <?php
                                            $total = 0;
                                            foreach ($provider->orders as $order){
                                                $total += $order->_price();
                                            }
                                            ?>
                                            <td>{{  $total }}</td>
                                        </tr>



                                    </table>
                                </div>
                                <div class="col-12">
{{--                                    <a href="{{ route('admin.countries.edit', $country) }}" class="btn btn-primary mr-1"><i class="feather icon-edit-1"></i> تعديل</a>--}}
                                    @if($provider->banned == 0)
                                    <a class="btn btn-outline-danger action-block mb-2" data-toggle="modal" data-target="#blockModel"><i class="fa fa-ban"></i>
                                        {{__('ban')}}</a>
                                    @else
                                        <a class="btn btn-outline-success action-block mb-2" data-toggle="modal" data-target="#unblockModel"><i class="fa fa-check"></i>
                                            {{__('unban')}}</a>
                                    @endif
                                    <a class="btn btn-outline-success action-compensation mb-2" data-toggle="modal" data-target="#walletModel"><i class="fa fa-wallet"></i>
                                        {{__('admin.addWallet')}}</a>
                                    <a href="#" class="btn btn-outline-primary single mb-2" title="{{__('admin.SendNotify')}}" onclick="sendNotify('one' , '{{ $provider['id'] }}')" data-toggle="modal" data-target="#send-noti">
                                        <i class="feather icon-send"></i>{{__('admin.SendNotify')}}
                                    </a>
                                    <a target="_blank" href="{{url('/admin/items?user_id='.$provider['id'])}}" class="btn btn-outline-success mb-2">
                                        {{__('items')}}
                                    </a>
                                    <a href="{{route('admin.providers.branches',$provider)}}" class="btn btn-outline-success mb-2">{{__('عرض الفروع')}} ({{count($provider->branches)}})</a>
                                    <a href="{{route('admin.providers.callcenter',$provider)}}" class="btn btn-outline-success mb-2">{{__('عرض الكول سنتر')}} ({{count($provider->employees)}})</a>
                                    @if($provider->category)
                                        <button type="button" class="btn btn-outline-success subs mb-2" data-perms='{{$provider->categories}}' data-categories='{{$provider->category->children}}' data-user_id='{{$provider['id']}}' data-target="#subpermsModal" data-toggle="modal">
                                            {{__('admin.ViewSections')}}  ({{count($provider->categories)}})
                                        </button>
                                    @endif
{{--                                    <a href="{{route('admin.orders.client',$provider['id'])}}" class="btn btn-outline-success">({{$provider->ordersAsUser()->count()}})طلبات</a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(count($provider->ratings) > 0)
                <!-- account end -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                        {{ __('admin.CustomerReviews') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('admin.orderNumber')}}</th>
                                                <th>{{__('admin.delegateName')}}</th>
                                                <th>{{__('admin.rate')}}</th>
                                                <th>{{__('created_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($provider->userRatings->groupBy('order_id') as $key => $ob)
                                                <tr>
                                                    <td>{{\App\Entities\Order::find($key)['order_name']}}</td>
                                                    <td>{{$ob->first()->user['name']}}</td>
                                                    <td>
                                                        @foreach($ob as $data)
                                                            {{$data->ratingList['title']}} : <div class="Stars" style="--rating: {{$data['rate']}};">
                                                        @endforeach
                                                    </td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->first()->user['created_at']))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                <div class="modal fade" id="subpermsModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="branches">
                                    <h3>{{__('categories')}}</h3>
                                    <form action="{{route('admin.providers.postcategories')}}" method="post">
                                        @csrf()
                                        <input type="hidden" name="user_id" id="user_id" value="">
                                        <div class="children-groups">

                                        </div>

                                        <div class="modal-footer justify-content-between">
                                            <button type="submit" class="btn btn-primary">{{__('save')}}</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(count($provider->blockUsers) > 0)
                <!-- account end -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                        {{ __('admin.ReasonsForBan') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('reason')}}</th>
                                                <th>{{__('created_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($provider->blockUsers as $ob)
                                                <tr>
                                                    <td>{{$ob->notes}}</td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->created_at))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </section>
        <div class="modal fade" id="blockModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('admin.banClient')}}</h4></div>
                    <form action="{{route('admin.clients.block')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$provider['id']}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">{{__('reason')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <textarea name="notes" class="form-control" rows="6"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- send-noti modal-->
        <div class="modal fade" id="send-noti" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('admin.sendNotify')}}</h5>
                    </div>
                    <div class="modal-body">
                        <form action="" id="sendnotifyuserForm" method="POST">
                            @csrf
                            <input type="hidden" name="type" value="provider">
                            <input type="hidden" name="notify_type" id="notify_type" value="client">
                            <input type="hidden" name="id" id="notify_id">

                            <div class="form-group">
                                <label>{{__('admin.titleMessageInArabic')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <input type="text" name="title_ar" id="notifyTitle_ar" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>{{__('admin.titleMessageInEnglish')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <input type="text" name="title_en" id="notifyTitle_en" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">
                                    {{__('admin.MessageInArabic')}}<span style="color:rgb(145, 4, 4)">*</span>
                                </label>
                                <textarea name="message_ar" id="notifyMessage_ar" cols="30" rows="4" class="form-control"
                                          placeholder="{{__('writeMessage')}}"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">
                                    {{__('admin.MessageInEnglish')}}<span style="color:rgb(145, 4, 4)">*</span>
                                </label>
                                <textarea name="message_en" id="notifyMessage_en" cols="30" rows="4" class="form-control"
                                          placeholder="{{__('writeMessage')}}"></textarea>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <button type="submit" class="btn btn-sm btn-success save" onclick="sendnotifyuser()">
                                    {{__('send')}}</button>
                                <button type="button" class="btn btn-default" id="notifyClose" data-dismiss="modal">
                                    {{__('close')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end send-noti modal-->
        <div class="modal fade" id="unblockModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('admin.unbanClient')}}</h4></div>
                    <form action="{{route('admin.clients.unblock')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$provider['id']}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <h3>{{__('AreSureUnblockClient')}}</h3>
                            </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="walletModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('admin.AddClientWallet')}}</h4></div>
                    <form action="{{route('admin.addToWallet')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$provider['id']}}">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="">{{__('ExpiryDate')}}</label>
                                        <input type="date" class="form-control" name="expire_date">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="">{{__('admin.balance')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <input type="number" name="amount" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('reason')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <textarea name="notes" class="form-control" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
@push('js')
    <script>
        function sendnotifyuser() {
            event.preventDefault();
            $.ajax({
                type        : 'POST',
                url         : '{{ route('admin.sendnotifyuser') }}' ,
                datatype    : 'json' ,
                async       : false,
                processData : false,
                contentType : false,
                data        : new FormData($("#sendnotifyuserForm")[0]),
                success     : function(msg){
                    if(msg.value == '0'){
                        toastr.error(msg.msg);
                    }else{
                        $('#notifyClose').trigger('click');
                        $('#notifyMessage').html('');
                        toastr.success(msg.msg);
                    }
                }
            });
        }

        $(function () {
            'use strict'
            $('body').on('click','.subs',function () {
                var user = $(this).data('user_id');
                var perms = $(this).data('perms');
                $('#subpermsModal #user_id').val(user);
                var subperms = $(this).data('categories');
                $('.children-groups').empty();
                var html = '';
                var subcat = '';
                $.each(subperms,(indexperm,value)=>{
                    $.each(perms,(index,catvalue)=>{
                        if(value.id == catvalue.id){
                            subcat = value;
                        }
                    });
                    html += `
                    <table class="table">
                        <tr>
                            <th>
                                ${value.title.ar}
                            </th>
                            <td>
                                <input type="checkbox" name="categories[]" ${subcat == value ? 'checked' : ''} value="${value.id}">
                            </td>
                        </tr>
                    </table>
                    `;
                });

                $('.children-groups').append(html);
            })
        });
    </script>
@endpush
