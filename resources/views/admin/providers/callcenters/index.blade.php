@extends('admin.layout.master')
@section('content')
  @push('css')
    <style>
      @media (min-width: 576px) {
        #dateModal .modal-dialog {
          max-width: 1000px !important;
        }
      }

    </style>
  @endpush
  <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
      <div class="row breadcrumbs-top">
        <div class="col-12">
          <h2 class="content-header-title float-left mb-0">
            {{ __('admin.callcenterProvider') }}
          </h2>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card-box table-responsive">
        <a href="{{route('admin.providers.callcenterCreate',$provider)}}"
                class="btn btn-primary btn-wide waves-effect waves-light">
          <i class="fas fa-plus"></i> {{ __('admin.AddCallCenter') }}
        </a>
        <div id="datatable_wrapper"
             class="dataTables_wrapper form-inline dt-bootstrap no-footer">
          <table id="datatable"
                 class="table table-striped table-bordered dt-responsive nowrap"
                 style="width:100%">
            <thead>
              <tr>
                <th>
                  <label class="custom-control material-checkbox" style="margin: auto">
                    <input type="checkbox" class="material-control-input" id="checkedAll">
                    <span class="material-control-indicator"></span>
                  </label>
                </th>
                <th> {{ __('name') }}</th>
                <th> {{ __('phone') }}</th>
                <th> {{ __('City') }}</th>
                <th> {{ __('Address') }}</th>
                <th>{{ __('control') }}</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($branches as $ob)
                <tr>
                  <td>
                    <label class="custom-control material-checkbox" style="margin: auto">
                      <input type="checkbox" class="material-control-input checkSingle"
                             id="{{ $ob->id }}">
                      <span class="material-control-indicator"></span>
                    </label>
                  </td>
                  <td>{{ $ob['name'] }}</td>
                  <td>{{ $ob['phone'] }}</td>
                  <td>{{ $ob->city['title'] }}</td>
                  <td>{{ $ob['address'] }}</td>
                  <td>
                    <a href="{{route('admin.providers.editCallcenter',$ob)}}" class="branchEdit"><i
                         class="feather icon-edit"></i></a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


@endsection
