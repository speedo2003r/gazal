@extends('admin.layout.master')
@section('title', __('admin.EditCallCenter'))
@section('content')
  <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
      <div class="row breadcrumbs-top">
        <div class="col-12">
            <h2 class="content-header-title float-left mb-0">{{ __('admin.callcenterProvider') }}
                ({{ $employee->parent['store_name'] }})</h2>
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a
                        href="{{ route('admin.providers.callcenter', $employee->parent) }}">{{ __('admin.callcenterProvider') }}</a>
                </li>
              <li class="breadcrumb-item active">{{ __('admin.EditCallCenter') }} </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content-body">
    <!-- page users view start -->
    <section class="page-users-view">
      <div class="row">
        <!-- account start -->
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <form action="{{ route('admin.providers.updateCallcenter', $employee) }}"
                    method="POST" id="editForm" class="saveBranch"
                    enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                  <?php $n = 0; ?>
                  <div class="row">
                    <div class="col-sm-12 text-center">
                      <label class="mb-0">{{ __('avatar') }}</label>
                      <div class="text-center">
                        <div class="images-upload-block single-image">
                          <label class="upload-img">
                            <input type="file" name="image" id="image" accept="image/*"
                                   class="image-uploader">
                            <i class="fas fa-cloud-upload-alt"></i>
                          </label>
                          <div class="upload-area" id="upload_area_img">
                            <div class="uploaded-block" data-count-order="0">
                              <a href="{{ $employee['avatar'] }}"
                                 data-fancybox="{{ $employee['avatar'] }}"
                                 data-caption="{{ $employee['avatar'] }}">
                                <img src="{{ $employee['avatar'] }}"></a>
                              <button class="close" type="button">×</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>{{ __('name') }}<span
                                style="color:rgb(145, 4, 4)">*</span></label>
                        <input type="text" name="name" id="name"
                               value="{{ $employee['name'] }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{ __('phone') }}<span
                                style="color:rgb(145, 4, 4)">*</span></label>
                        <input type="number" name="phone" id="phone"
                               value="{{ $employee['phone'] }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{ __('email') }}<span
                                style="color:rgb(145, 4, 4)">*</span></label>
                        <input type="text" name="email" id="email"
                               value="{{ $employee['email'] }}" class="form-control">
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{ __('password') }}<span
                                style="color:rgb(145, 4, 4)">*</span></label>
                        <input type="password" name="password" class="form-control"
                               autocomplete="off">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{ __('Password Confirmation') }}<span
                                style="color:rgb(145, 4, 4)">*</span></label>
                        <input type="password" name="password_confirmation"
                               class="form-control" autocomplete="off">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{ __('Country') }}<span
                                style="color:rgb(145, 4, 4)">*</span></label>
                        <select name="country_id" id="country_id" class="form-control">
                          <option value="" hidden selected>{{ __('Choose') }}
                          </option>
                          @foreach ($countries as $country)
                            <option value="{{ $country['id'] }}"
                                    @if ($employee['country_id'] == $country['id']) selected @endif>
                              {{ $country->title }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>{{ __('City') }}<span
                                style="color:rgb(145, 4, 4)">*</span></label>
                        <select name="city_id" id="city" class="form-control">
                          <option value="" selected hidden>{{ __('Choose') }}
                          </option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group col-sm-12">
                      <label>{{ __('Address') }}<span
                              style="color:rgb(145, 4, 4)">*</span></label>
                      <input type="hidden" name="lat" id="lat"
                             value="{{ $employee['lat'] }}">
                      <input type="hidden" name="lng" id="lng"
                             value="{{ $employee['lng'] }}">
                      <input type="text" name="address" id="address" class="form-control"
                             value="{{ $employee['address'] }}">
                    </div>
                    <div id="map" style="height: 300px;width: 100%"></div>
                  </div>
                  <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-sm btn-success save">{{__('send')}}</button>
                    <button type="button" class="btn btn-default" id="notifyClose"
                            data-dismiss="modal">{{__('close')}}</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  @endsection
  @push('js')
    {!! $validator->selector('#editForm') !!}
    <script src="{{ dashboard_url('dashboard/assets/js/map.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ settings('map_key') }}&libraries=places&callback=initMap&lang=ar"
            async defer></script>
    <script>
      $('body').on('change', '[name=country_id]', function() {
        var id = $(this).val();
        getCities(id);
      });
      $('body').on('change', '[name=city_id]', function() {
        var id = $(this).val();
      });
      @if ($employee['city_id'])
        getCities(`{{ $employee['country_id'] }}`,`{{ $employee['city_id'] }}`);
      @endif

    </script>

  @endpush
