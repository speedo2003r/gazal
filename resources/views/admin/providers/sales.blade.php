@extends('admin.layout.master')
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{__('admin.salesProviders')}}</h2>
                </div>
            </div>
        </div>
    </div>

    <!-- page users view start -->
    <section class="page-users-view">
        <div class="row">
            <!-- account start -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">{{__('from')}}</label>
                                    <input type="date" class="form-control" name="from">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">{{__('to')}}</label>
                                    <input type="date" class="form-control" name="to">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">{{__('providers')}}</label>
                                    <select name="provider_id" class="form-control">
                                        <option value="" selected hidden>{{__('Choose')}}</option>
                                        @foreach($providers as $provider)
                                            <option value="{{$provider['id']}}">{{$provider['store_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">{{__('admin.Branches')}}</label>
                                    <select name="branch_id" class="form-control">
                                        <option value="" selected hidden>{{__('Choose')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-outline-primary send">ارسال</button>
                        <div class="text-center w-100">
                            <div class="small-box smallBoxCustom bg-brown-400">
                                <div class="inner">
                                    <h3 class="amount">0</h3>
                                    <p> {{__('SAR')}} </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
@push('js')

    <script>
        $('body').on('change','[name=provider_id]',function (){
           var id = $(this).val();
           getBranches(id);
        });
        $(document).on('click','.send',function (){
            var from = $('[name=from]').val();
            var to = $('[name=to]').val();
            var provider_id = $('[name=provider_id]').val();
            var branch_id = $('[name=branch_id]').val();
            $.ajax({
                url: `{{ route('admin.providers.salesAmount') }}`,
                type: 'post',
                dataType: 'json',
                data: {
                    from: from,
                    to: to,
                    provider_id: provider_id,
                    branch_id: branch_id,
                },
                success: function(res) {
                    if(res.value == 0){
                        toastr.error(res.msg);
                    }else{
                        $('.amount').html(res.data);
                    }
                }
            });
        });
        function getBranches(provider_id, placeholder = 'كل الفروع') {
            var html = '';
            $('[name=branch_id]').empty();
            if (provider_id) {
                $.ajax({
                    url: `{{ route('admin.ajax.getBranches') }}`,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: provider_id
                    },
                    success: function(res) {
                        html += `<option value="">${placeholder}</option>`;
                        $.each(res.data, function(index, value) {
                            html +=
                                `<option value="${value.id}">${value.title.ar}</option>`;
                        });
                        $('[name=branch_id]').append(html);
                    }
                });
            }
        }
    </script>
@endpush
