@extends('admin.layout.master')
@section('title', __('admin.AddBranch'))
@section('content')
@push('css')
    <style>
        .page-users-view table td{
            min-width: auto !important;
        }
    </style>
@endpush

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.providers.branchesDates') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.EditBranchesDates')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{ __('admin.AddNewDate') }}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="branches">
                                <button type="button"
                                        class="btn btn-primary waves-effect waves-light addmoredate mb-2"
                                        data-user_id="" data-toggle="modal" data-target="#addDateModal">
                                    {{__('admin.AddNewDate')}}</button>
                                <table class="table table-border">
                                    <tr>
                                        <th>{{ __('admin.day') }}</th>
                                        <th colspan="2">{{ __('admin.firstShift') }}</th>
                                        <th colspan="2">{{ __('admin.secondShift') }}</th>
                                        <th colspan="2">{{ __('admin.thirdShift') }}</th>
                                        <th colspan="2">{{ __('admin.fourthShift') }}</th>
                                        <th>{{ __('control') }}</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th>{{ __('from') }}</th>
                                        <th>{{ __('to') }}</th>
                                        <th>{{ __('from') }}</th>
                                        <th>{{ __('to') }}</th>
                                        <th>{{ __('from') }}</th>
                                        <th>{{ __('to') }}</th>
                                        <th>{{ __('from') }}</th>
                                        <th>{{ __('to') }}</th>
                                        <th></th>
                                    </tr>
                                    @if(count($branchDates) > 0)
                                    @foreach($branchDates as $branchDate)
                                    <tr>
                                        <td>{{\App\Entities\BranchDate::Days($branchDate['day'])}}</td>
                                        <td>{{$branchDate['shift'] == 1 ? $branchDate->timefrom : ''}}</td>
                                        <td>{{$branchDate['shift'] == 1 ? $branchDate->timeto : ''}}</td>
                                        <td>{{$branchDate['shift'] == 2 ? $branchDate->timefrom : ''}}</td>
                                        <td>{{$branchDate['shift'] == 2 ? $branchDate->timeto : ''}}</td>
                                        <td>{{$branchDate['shift'] == 3 ? $branchDate->timefrom : ''}}</td>
                                        <td>{{$branchDate['shift'] == 3 ? $branchDate->timeto : ''}}</td>
                                        <td>{{$branchDate['shift'] == 4 ? $branchDate->timefrom : ''}}</td>
                                        <td>{{$branchDate['shift'] == 4 ? $branchDate->timeto : ''}}</td>
                                        <td>
                                            <form action="{{ route('admin.providers.deletedatebranches') }}" method="post">
                                                @csrf()
                                                <input type="hidden" value="{{$branchDate['day']}}" name="date">
                                                <input type="hidden" value="{{$branch['id']}}" name="branch_id">
                                                <button type="submit" class="btn btn-danger"><i class="feather icon-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td colspan="10">لا يوجد مواعيد خاصه بهذا الفرع</td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <div class="modal fade" id="addDateModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ __('admin.AddDate') }}
                            <span class="userName"></span>
                        </h5>
                    </div>
                    <div class="modal-body">

                        <form action="{{ route('admin.providers.storebranchdate') }}" method="POST"
                              id="saveBranchDate">
                            {{-- <form action="" method="POST" class="saveBranch"> --}}
                            {{ csrf_field() }}
                            <input type="hidden" name="branch_id" value="{{$branch['id']}}">
                            <input type="hidden" name="type" value="{{$typeValue}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="time" name="timefrom" required class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="time" name="timeto" required class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select name="shift" required style="width: 100% !important;"
                                                class="form-control">
                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                            <option value="1">{{__('admin.firstShift')}}</option>
                                            <option value="2">{{__('admin.secondShift')}}</option>
                                            <option value="3">{{__('admin.thirdShift')}}</option>
                                            <option value="4">{{__('admin.fourthShift')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select name="work_days[]" required multiple
                                                style="width: 100% !important;" class="form-control select2">
                                            @foreach (\App\Models\User::workDays() as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="submit" class="btn btn-sm btn-success save">إرسال</button>
                                <button type="button" class="btn btn-default" id="notifyClose"
                                        data-dismiss="modal">اغلاق</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endsection
