@extends('admin.layout.master')
@section('content')
    @push('css')
        <style>
            @media (min-width: 576px) {
                #dateModal .modal-dialog {
                    max-width: 1000px !important;
                }
            }

        </style>
    @endpush
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">
                        {{ __('admin.BranchesOpenClosed') }}
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">{{__('admin.Type')}}</label>
                <select name="type" class="form-control type">
                    <option value="">{{__('Choose')}}</option>
                    <option value="open">{{__('Open')}}</option>
                    <option value="close">{{__('Closed')}}</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">{{__('providers')}}</label>
                <select name="provider_id" class="form-control provider_id">
                    <option value="">{{__('Choose')}}</option>
                    @foreach($providers as $provider)
                        <option value="{{$provider['id']}}">{{$provider['store_name']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div id="datatable_wrapper"
                     class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <table
                           class="table table-striped table-bordered dt-responsive nowrap"
                           style="width:100%">
                        <thead>
                        <tr>
                            <th> {{ __('admin.branchName') }}</th>
                            <th> {{ __('phone') }}</th>
                            <th> {{ __('Address') }}</th>
                            <th> {{ __('City') }}</th>
                            <th> {{ __('control') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        var provider_id = '';
        var type = '';
        $('body').on('change','.provider_id',function (){
            provider_id = $(this).val();
            oTable.draw();
        });
        $('body').on('change','.type',function (){
            type = $(this).val();
            oTable.draw();
        });
        $('.table').dataTable().fnDestroy();
        var oTable = $('.table').DataTable({
            dom: '<"top"<"actions">Blfrtip<"clear">>rt<"bottom"iflp<"clear">>',
            bDestroy: true,
            searching: false,
            pageLength: 10,
            processing: true,
            serverSide: true,
            ajax: {
                url: `{{route('admin.providers.getBranchData')}}`,
                data: function (d) {
                    d.provider_id = provider_id;
                    d.type = type;
                }
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'phone', phone: 'name'},
                {data: 'address', name: 'address'},
                {data: 'current_city', name: 'current_city'},
                {data: 'control'},
            ],
            lengthMenu :[
                [10,25,50,100,-1],[10,25,50,100,`{{__('view_all')}}`]
            ],
            buttons: [
                {
                    extend: 'excel',
                    text: `{{__('excel_file')}}`,
                    className: "btn btn-success"

                },
                {
                    extend: 'copy',
                    text: `{{__('copy')}}`,
                    className: "btn btn-inverse"
                },
                {
                    extend: 'print',
                    text: `{{__('print')}}`,
                    className: "btn btn-success"
                },
            ],

            "language": {
                "sEmptyTable": `{{ __('NoDataInTable') }}`,
                "sLoadingRecords": `{{ __('sLoading') }}`,
                "sProcessing": `{{ __('sLoading') }}`,
                "sLengthMenu": '{{ __('showIn') }} _MENU_ {{ __('input') }}',
                "sZeroRecords": `{{ __('noDataFind') }}`,
                "sInfo": "{{ __('showIn') }} _START_ {{ __('to') }} _END_ {{ __('outOf') }} _TOTAL_ {{ __('entry') }}",
                "sInfoEmpty": `{{ __('DisplaysOutRecords') }}`,
                "sInfoFiltered": "({{ __('selectedFromTotal') }} _MAX_ {{ __('entry') }})",
                "sInfoPostFix": "",
                "sSearch": `{{ __('search') }}:`,
                "sUrl": "",
                "oPaginate": {
                    "sFirst": `{{ __('first') }}`,
                    "sPrevious": `{{ __('previous') }}`,
                    "sNext": `{{ __('next') }}`,
                    "sLast": `{{ __('last') }}`
                },
                "oAria": {
                    "sSortAscending": `{{ __(' EnableAscendingOrder') }}:`,
                    "sSortDescending": `{{ __(': EnableDescendingOrder') }}`
                }
            }
        });
    </script>

@endpush
