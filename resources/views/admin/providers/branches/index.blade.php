@extends('admin.layout.master')
@section('content')
  <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
      <div class="row breadcrumbs-top">
        <div class="col-12">
          <h2 class="content-header-title float-left mb-0">
            {{ __('admin.branchesProvider') }}
          </h2>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card-box table-responsive">
        <a href="{{route('admin.providers.branches.create',$provider)}}"
                class="btn btn-primary btn-wide waves-effect waves-light">
          <i class="fas fa-plus"></i> {{ __('admin.AddBranch') }}
        </a>
        <div id="datatable_wrapper"
             class="dataTables_wrapper form-inline dt-bootstrap no-footer">
          <table id="datatable"
                 class="table table-striped table-bordered dt-responsive nowrap"
                 style="width:100%">
            <thead>
              <tr>
                <th>
                  <label class="custom-control material-checkbox" style="margin: auto">
                    <input type="checkbox" class="material-control-input" id="checkedAll">
                    <span class="material-control-indicator"></span>
                  </label>
                </th>
                <th> {{ __('admin.branchName') }}</th>
                <th> {{ __('phone') }}</th>
                <th> {{ __('City') }}</th>
                <th> {{ __('Address') }}</th>
                <th> {{ __('admin.SchedulesWork') }}</th>
                <th>{{ __('control') }}</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($branches as $ob)
                <tr>
                  <td>
                    <label class="custom-control material-checkbox" style="margin: auto">
                      <input type="checkbox" class="material-control-input checkSingle"
                             id="{{ $ob->id }}">
                      <span class="material-control-indicator"></span>
                    </label>
                  </td>
                  <td>{{ $ob['title'] }}</td>
                  <td>{{ $ob['phone'] }}</td>
                  <td>{{ $ob->city['title'] }}</td>
                  <td>{{ $ob['address'] }}</td>
                  <td>
                    @if($ob['schedule_switch'] == \App\Entities\Branch::NORMAL)
                            {{__('admin.normalDate')}}
                      @else
                            {{__('admin.ramadanDate')}}
                    @endif
                  </td>
                  <td>
                    <a href="javascript:void(0)" class="margin-left times" data-toggle="modal"
                       data-target="#dateModal" data-branch_id="{{ $ob['id'] }}">

                      <i class="fa fa-clock"></i>
                    </a>
                      <a href="{{route('admin.providers.showbranch',$ob)}}" class="branchEdit"><i
                              class="feather icon-eye"></i></a>
                    <a href="{{route('admin.providers.editbranch',$ob)}}" class="branchEdit"><i
                         class="feather icon-edit"></i></a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>





  <div class="modal fade" id="dateModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            {{ __('admin.ChangeScheduleDateBranch') }} <span
                  class="userName"></span>
          </h5>
        </div>
        <div class="modal-body">
            <form action="{{route('admin.branches.changeSchedule')}}" id="sendnotifyuserForm" method="POST">
                @csrf
                <input type="hidden" name="branch_id">
                <div class="form-group">
                    <label for="">
                        {{__('admin.Schedule')}}
                    </label>
                    <select name="schedule" id="schedule" class="form-control">
                        <option value="" selected>{{__('Choose')}}</option>
                        <option value="1">{{__('admin.normalDate')}}</option>
                        <option value="2">{{__('admin.ramadanDate')}}</option>
                    </select>
                </div>

                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-sm btn-success save" onclick="sendnotifyuser()">إرسال</button>
                    <button type="button" class="btn btn-default" id="notifyClose" data-dismiss="modal">اغلاق</button>
                </div>
            </form>

        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="addDateModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">{{ __('admin.AddDate') }}
            <span class="userName"></span>
          </h5>
        </div>
        <div class="modal-body">

          <form action="{{ route('admin.providers.storebranchdate') }}" method="POST"
                id="saveBranchDate">
            {{-- <form action="" method="POST" class="saveBranch"> --}}
            {{ csrf_field() }}
            <input type="hidden" name="branch_id">
            <input type="hidden" value="{{ $provider['id'] }}" name="user_id"
                   class="user_id">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="time" name="timefrom" required class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="time" name="timeto" required class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <select name="shift" required style="width: 100% !important;"
                          class="form-control">
                    <option value="" hidden selected>{{__('Choose')}}</option>
                    <option value="1">{{__('admin.firstShift')}}</option>
                    <option value="2">{{__('admin.secondShift')}}</option>
                    <option value="3">{{__('admin.thirdShift')}}</option>
                    <option value="4">{{__('admin.fourthShift')}}</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <select name="work_days[]" required multiple
                          style="width: 100% !important;" class="form-control select2">
                    @foreach (\App\Models\User::workDays() as $key => $value)
                      <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-sm btn-success save">إرسال</button>
              <button type="button" class="btn btn-default" id="notifyClose"
                      data-dismiss="modal">اغلاق</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

@endsection
@push('js')
  <script src="{{ dashboard_url('dashboard/assets/js/map.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key={{ settings('map_key') }}&libraries=places&callback=initMap&lang=ar"
          async defer></script>
  <script>

    $(function() {
      'use strict'
      $('body').on('change', '[name=country_id]', function() {
        var id = $(this).val();
        getCities(id);
      });

      $('body').on('change', '[name=city_id]', function() {
        var id = $(this).val();
        getZones(id);
      });

    });

    $('.add-user').on('click', function() {
      $('#editModel .modal-title').text(`{{ __('admin.AddBranch') }}`);
      $('[name=city_id]').empty();
      $('#editForm :input:not([type=checkbox],[type=radio],[type=hidden])').val('');
      $('#upload_area_img').empty();
      $('#editForm').attr("action", "{{ route('admin.providers.postbranch') }}");
    });

    $(document).on('click', '.featureEdit', function() {
      var id = $(this).data('id');
      $('.branch_id2').val(id);
    });
    $('body').on('click','.times',function (){
        var branch_id = $(this).data('branch_id');
        $('[name=branch_id]').val(branch_id);
    });
    function edit(ob) {
      $('[name=password]').val('');
      $('#editForm').attr("action", "{{ route('admin.providers.updatebranch', 'obId') }}"
        .replace('obId', ob.id));

      $('#editModel .modal-title').text(`{{ __('admin.EditBranch') }}`);
      $('.saveBranch').children('input').empty();
      $('#branch_id').val(ob.id);
      $('#name').val(ob.name);
      $('#title_ar').val(ob.title.ar);
      $('#title_en').val(ob.title.en);
      $('#phone').val(ob.phone);
      $('#email').val(ob.email);
      $('#country_id').val(ob.country_id).change;
      $('#lat').val(ob.lat);
      $('#lng').val(ob.lng);
      $('#address').val(ob.address);
      getCities(ob.country_id, ob.city_id);
      getZones(ob.city_id, ob.zone_id);
      initMap();
      let image = $('#upload_area_img');
      image.empty();
      image.append('<div class="uploaded-block" data-count-order="1"><a href="' + ob
        .avatar + '"  data-fancybox data-caption="' + ob.avatar + '" ><img src="' + ob
        .avatar + '"></a><button class="close">&times;</button></div>');
    }

    function show(ob) {
      $('#featureModel :input:not([type=checkbox],[type=hidden])').val('');
      $('#featureModel :input([type=checkbox])').prop('checked', false);
      $.each(ob, (index, value) => {
        if (value.feature == 'ship') {
          $('.ship textarea').val(value.desc);
          $('.ship [type=checkbox]').prop('checked', value.status == 1 ? true : false);
        }
        if (value.feature == 'return') {
          $('.return textarea').val(value.desc);
          $('.return [type=checkbox]').prop('checked', value.status == 1 ? true :
            false);
        }
        if (value.feature == 'shop') {
          $('.shop textarea').val(value.desc);
          $('.shop [type=checkbox]').prop('checked', value.status == 1 ? true : false);
        }
      })
    }


    function getZones(city_id, type = '', placeholder = `{{__('Choose')}}`) {
      var html = '';
      var zone_id = '';
      $('[name=zone_id]').empty();
      if (city_id) {
        $.ajax({
          url: `{{ route('admin.ajax.getZones') }}`,
          type: 'post',
          dataType: 'json',
          data: {
            id: city_id
          },
          success: function(res) {
            if (type != '') {
              zone_id = type;
            }
            html += `<option value="" selected hidden>${placeholder}</option>`;
            $.each(res.data, function(index, value) {
              html +=
                `<option value="${value.id}" ${zone_id == value.id ? 'selected':'' }>${value.title.ar}</option>`;
            });
            $('[name=zone_id]').append(html);
          }
        });
      }
    }
  </script>

@endpush
