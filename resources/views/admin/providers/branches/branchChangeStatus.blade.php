@extends('admin.layout.master')
@section('title',__('admin.showDataBranch'))
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{__('admin.Branches')}}</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.providers.branches',$branch->user) }}">{{('admin.Branches')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.showBranch')}} ({{ $branch->name }})</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="users-view-image">
                                    <img src="{{ $branch->avatar }}" class="users-avatar-shadow w-100 rounded mb-2 pr-2 ml-1" alt="avatar">
                                </div>
                                <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                    <table>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('name') }}</td>
                                            <td>{{ $branch->name }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('email') }}</td>
                                            <td>{{  $branch->email }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('phone') }}</td>
                                            <td>{{  $branch->phone }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.walletBalance') }}</td>
                                            <td>{{  $branch->wallet }}</td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="col-12 col-md-12 col-lg-5">
                                    <table class="ml-0 ml-sm-0 ml-lg-0">
                                        <tr>
                                            <td class="font-weight-bold">{{ __('JoinDatetime') }}</td>
                                            <td>{{  date('Y-m-d h:i a',strtotime($branch->created_at)) }}</td>
                                        </tr>



                                    </table>
                                </div>
                                <div class="col-12">
                                    @if($branch->banned == 0)
                                    <a class="btn btn-outline-danger action-block" data-toggle="modal" data-target="#blockModel"><i class="fa fa-ban"></i>
                                        {{__('ban')}}</a>
                                    @else
                                        <a class="btn btn-outline-success action-block" data-toggle="modal" data-target="#unblockModel"><i class="fa fa-check"></i>
                                            {{__('unban')}}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if(count($branch->blockUsers) > 0)

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        {{ __('admin.reasonBanUnban') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('reason')}}</th>
                                                <th>{{__('admin.Type')}}</th>
                                                <th>{{__('created_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($branch->blockUsers as $ob)
                                                <tr>
                                                    <td>{{$ob->notes}}</td>
                                                    <td>{{$ob->status == 1 ? __('ban') : __('unban')}}</td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->created_at))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </section>
        <div class="modal fade" id="blockModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('ban')}}</h4></div>
                    <form action="{{route('admin.branches.block')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$branch['id']}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">{{__('reason')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <textarea name="notes" class="form-control" rows="6"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!--end send-noti modal-->
        <div class="modal fade" id="unblockModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('unban')}}</h4></div>
                    <form action="{{route('admin.branches.unblock')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$branch['id']}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">{{__('reason')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <textarea name="notes" class="form-control" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
@push('js')
    <script>
        function sendnotifyuser() {
            event.preventDefault();
            $.ajax({
                type        : 'POST',
                url         : '{{ route('admin.sendnotifyuser') }}' ,
                datatype    : 'json' ,
                async       : false,
                processData : false,
                contentType : false,
                data        : new FormData($("#sendnotifyuserForm")[0]),
                success     : function(msg){
                    if(msg.value == '0'){
                        toastr.error(msg.msg);
                    }else{
                        $('#notifyClose').trigger('click');
                        $('#notifyMessage').html('');
                        toastr.success(msg.msg);
                    }
                }
            });
        }

        $(function () {
            'use strict'
            $('body').on('click','.subs',function () {
                var user = $(this).data('user_id');
                var perms = $(this).data('perms');
                $('#subpermsModal #user_id').val(user);
                var subperms = $(this).data('categories');
                $('.children-groups').empty();
                var html = '';
                var subcat = '';
                $.each(subperms,(indexperm,value)=>{
                    $.each(perms,(index,catvalue)=>{
                        if(value.id == catvalue.id){
                            subcat = value;
                        }
                    });
                    html += `
                    <table class="table">
                        <tr>
                            <th>
                                ${value.title.ar}
                            </th>
                            <td>
                                <input type="checkbox" name="categories[]" ${subcat == value ? 'checked' : ''} value="${value.id}">
                            </td>
                        </tr>
                    </table>
                    `;
                });

                $('.children-groups').append(html);
            })
        });
    </script>
@endpush
