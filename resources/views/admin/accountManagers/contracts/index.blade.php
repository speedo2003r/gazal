@extends('admin.layout.master')
@section('content')


    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{__('admin.ExpiredContractsAndCloseToExpiration')}}</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <table id="table"  class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                        <tr>
                            <th> {{__('number')}} </th>
                            <th> {{__('nameOfProvider')}} </th>
                            <th>  {{__('email')}} </th>
                            <th>  {{__('phone')}} </th>
                            <th> {{__('control')}}   </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- edit model -->
    <div class="modal fade" id="editModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"><h4 class="modal-title">{{__('admin.ModifyTheContractExpiryDate')}}</h4></div>
                <form action=""  id="editForm" method="post" role="form" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>{{__('admin.ContractExpiryDate')}}</label>
                                    <input type="date" id="end_date_contract" name="end_date_contract" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                        <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">{{__('close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end edit model -->

<!-- end add model -->

@endsection
@push('js')
    <script>

        var oTable = $('.table').DataTable({
            dom: 'Blfrtip',
            pageLength: 10,
            processing: true,
            serverSide: true,
            ajax: {
                url: `{{route('admin.manager.getEndOfContractData')}}`,
                // data: function (d) {
                //     d.from = from;
                //     d.to = to;
                // }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'control', name: 'control'},
            ],
            lengthMenu :[
                [10,25,50,100,-1],[10,25,50,100,`{{__('view_all')}}`]
            ],
            buttons: [
                {
                    extend: 'excel',
                    text: `{{__('excel_file')}}`,
                    className: "btn btn-success"

                },
                {
                    extend: 'copy',
                    text: `{{__('copy')}}`,
                    className: "btn btn-inverse"
                },
                {
                    extend: 'print',
                    text: `{{__('print')}}`,
                    className: "btn btn-success"
                },
            ],

            "language": {
                "sEmptyTable": `{{ __('NoDataInTable') }}`,
                "sLoadingRecords": `{{ __('sLoading') }}`,
                "sProcessing": `{{ __('sLoading') }}`,
                "sLengthMenu": '{{ __('showIn') }} _MENU_ {{ __('input') }}',
                "sZeroRecords": `{{ __('noDataFind') }}`,
                "sInfo": "{{ __('showIn') }} _START_ {{ __('to') }} _END_ {{ __('outOf') }} _TOTAL_ {{ __('entry') }}",
                "sInfoEmpty": `{{ __('DisplaysOutRecords') }}`,
                "sInfoFiltered": "({{ __('selectedFromTotal') }} _MAX_ {{ __('entry') }})",
                "sInfoPostFix": "",
                "sSearch": `{{ __('search') }}:`,
                "sUrl": "",
                "oPaginate": {
                    "sFirst": `{{ __('first') }}`,
                    "sPrevious": `{{ __('previous') }}`,
                    "sNext": `{{ __('next') }}`,
                    "sLast": `{{ __('last') }}`
                },
                "oAria": {
                    "sSortAscending": `{{ __(' EnableAscendingOrder') }}:`,
                    "sSortDescending": `{{ __(': EnableDescendingOrder') }}`
                }
            }
        });

        function edit(ob){
            console.log(ob);
            $('#editForm')      .attr("action","{{route('admin.manager.endOfContractStore','obId')}}".replace('obId',ob.id));
            $('#end_date_contract')    .val(ob.end_date_contract);
        }
    </script>
@endpush
