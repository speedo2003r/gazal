@extends('admin.layout.master')
@section('title',__('admin.addPercentagesPage'))
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{__('admin.addPercentagesSales')}}</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.manager.percentagesSales') }}">{{__('admin.addPercentagesSales')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.addPercentagesPage')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin.manager.postCommission',$provider['id'])}}" method="post">
                                @csrf()
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>العموله قيمه أو نسبه</label>
                                        <div class="radio-group">
                                            <div class="row text-center">
                                                <div class="form-group col-sm-6">
                                                    <label for="seller">
                                                        <input type="radio" name="commission_status" @if($provider['commission_status'] == 1) checked @endif value="1" class="form-control" id="value">
                                                        قيمه
                                                    </label>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="item">
                                                        <input type="radio" name="commission_status" @if($provider['commission_status'] == 2) checked @endif value="2" class="form-control" id="percentage">
                                                        نسبه
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>العموله</label>
                                        <input type="number" name="commission" id="commission" value="{{$provider['commission']}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                                <div>
                                    <button type="submit" class="btn btn-outline-primary waves-effect waves-light">{{__('send')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
