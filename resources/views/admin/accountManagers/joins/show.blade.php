@extends('admin.layout.master')
@section('title',__('admin.WatchServiceProvidersJoin'))
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{__('admin.ServiceProvidersJoin')}}</h2>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.manager.joins') }}">{{__('admin.ServiceProvidersJoin')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.WatchServiceProvidersJoin')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                    <table>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('Name') }}</td>
                                            <td>{{ $join->name }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('phone') }}</td>
                                            <td>{{  $join->phone }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('category') }}</td>
                                            <td>{{  $join->category['title'] }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('created_at') }}</td>
                                            <td>{{  date('Y-m-d h:i a',strtotime($join->created_at)) }}</td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="col-12 col-md-12 col-lg-5">
                                    <table class="ml-0 ml-sm-0 ml-lg-0">
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.ActivityName') }}</td>
                                            <td>{{  $join->store_name }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('email') }}</td>
                                            <td>{{  $join->email }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('City') }}</td>
                                            <td>{{  $join->city['title'] }}</td>
                                        </tr>



                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endsection
