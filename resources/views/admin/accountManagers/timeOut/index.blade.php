@extends('admin.layout.master')
@section('content')


    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{__('admin.providersTimeOut')}}</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    {!! $dataTable->table([
                     'class' => "table table-striped table-bordered dt-responsive nowrap",
                     'id' => "percentagesdatatable-table",
                     ],true) !!}
                </div>
            </div>
        </div>
    </div>


@endsection
@push('js')
    {!! $dataTable->scripts() !!}
@endpush
