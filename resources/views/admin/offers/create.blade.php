@extends('admin.layout.master')
@section('title',__('admin.addOffer'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.offers.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.offers')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.addOffer')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin.offers.store')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class = "col-sm-12 text-center">
                                            <label class = "mb-0">{{__('avatar')}}</label>
                                            <div class = "text-center">
                                                <div class = "images-upload-block single-image">
                                                    <label class = "upload-img">
                                                        <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader">
                                                        <i class="fas fa-cloud-upload-alt"></i>
                                                    </label>
                                                    <div class = "upload-area" id="upload_area_img"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.offerType')}}</label>
                                                <select name="type" class="form-control type">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    <option value="seller">{{__('store')}}</option>
                                                    <option value="item">{{__('item')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('page')}}</label>
                                                <select name="page" class="form-control page">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    <option value="special">{{__('admin.specialOffers')}}</option>
                                                    <option value="offer">{{__('admin.offersPage')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 radio-group">
                                            <div class="row text-center">
                                                <div class="form-group col-sm-6">
                                                    <label>{{__('from')}}</label>
                                                    <input type="date" name="start_date" class="form-control" id="start_date">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label>{{__('to')}}</label>
                                                    <input type="date" name="end_date" class="form-control" id="end_date">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 radio-group">
                                            <label>{{__('Country')}}</label>
                                            <select name="country_id" id="country_id" class="form-control">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country['id']}}">{{$country['title']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-4 radio-group">
                                            <label>{{__('City')}}</label>
                                            <select name="city_id" id="city_id" class="form-control">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4 radio-group">
                                            <label>{{__('category')}}</label>
                                            <select name="category_id" id="category_id" class="form-control">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category['id']}}">{{$category['title']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('title_ar')}}</label>
                                                <input type="text" name="title[ar]" id="title_ar" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('title_en')}}</label>
                                                <input type="text" name="title[en]" id="title_en" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('desc_ar')}}</label>
                                                <input type="text" name="desc[ar]" id="desc_ar" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('desc_en')}}</label>
                                                <input type="text" name="desc[en]" id="desc_en" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>{{__('Discount')}}</label>
                                                <input type="text" name="discount" id="discount" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 sellers" style="display: none">
                                            <label>{{__('stores')}}</label>
                                            <select name="provider_id" id="user_id" class="form-control edit_stores">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-12 items" style="display: none">
                                            <label>{{__('items')}}</label>
                                            <select name="item_id" id="item_id" class="form-control edit_items">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="submit" class="btn btn-primary">{{__('save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Admin\Offer\Create') !!}
    <script>
        $(function () {
            'use strict'
            $('body').on('change','#city_id',function () {
                var category_id = $('#category_id').val();
                var city_id = $(this).val();
                if(category_id != '' && city_id != ''){
                    getSellers(city_id,category_id);
                }
            });
            $('body').on('change', '#country_id', function () {
                var country_id = $(this).val();
                getCities(country_id);
            });
            $('body').on('change','#category_id',function () {
                var category_id = $(this).val();
                var city_id = $('#city_id').val();
                if(category_id != '' && city_id != ''){
                    getSellers(city_id,category_id);
                }
            });
        });
        $(function () {
            'use strict'
            $('body').on('change','#editForm .type',function () {
                var id = $(this).val();
                console.log(id);
                if(id == 'seller'){
                    $('.sellers').css({'display':'block'});
                    $('.items').css({'display':'none'});
                }else if(id == 'item'){
                    $('.sellers').css({'display':'block'});
                    $('.items').css({'display':'block'});
                }else{
                    $('.sellers').css({'display':'none'});
                    $('.items').css({'display':'none'});
                }
            })
        });

        function getItems(id,category_id,item_id=''){
            var html = '';
            if(id > 0){
                $.ajax({
                    url:`{{route('admin.ajax.getItems')}}`,
                    type:'get',
                    postType:'json',
                    data: {id: id,category_id: category_id},
                    success:function (data) {
                        html += `<option value="" hidden selected>{{__('Choose')}}</option>`;
                        $.each(data,function (index,value) {
                            console.log(value);
                            html += `<option value="${value.id}">${value.title.ar}</option>`;
                        });
                        $('.edit_items').append(html);
                        if(item_id != ''){
                            $('.edit_items')     .val(item_id).change;
                        }
                    }
                });
            }

        }
        function getSellers(id,category_id,seller_id=null){
            var html = '';
            if(id > 0){
                $.ajax({
                    url:`{{route('admin.ajax.getSellers')}}`,
                    type:'get',
                    postType:'json',
                    data: {id: id,category_id: category_id},
                    success:function (data) {
                        $('.edit_stores').empty();
                        html += `<option value="" hidden selected>اختر</option>`;
                        $.each(data,function (index,value) {
                            html += `<option value="${value.id}">${value.store_name.ar}</option>`;
                        });
                        $('.edit_stores').append(html);
                        if(seller_id != null){
                            $('.edit_stores')     .val(seller_id).change;
                        }
                    }
                });
            }
        }
        $('.edit_stores').on('change',function () {
            var id = $(this).val();
            var category_id = $('#category_id').val();
            $('.edit_items').empty();
            getItems(id,category_id);
        });
    </script>
@endpush
