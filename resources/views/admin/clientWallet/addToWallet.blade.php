@extends('admin.layout.master')
@section('title',__('admin.clientWallet'))
@section('content')
@push('css')
    <style>
        .select2,.select2-search__field{
            width: 100% !important;
        }
    </style>
@endpush
@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.wallets.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.clientWallet')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.addClientWalletPage')}}</span></a>
    </div>
@endsection
<div class="content-body">
    <!-- page users view start -->
    <section class="page-users-view">
        <div class="row">
            <!-- account start -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                <table>
                                    <tr>
                                        <td class="font-weight-bold">{{ __('name') }}</td>
                                        <td>{{ $user->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">{{ __('email') }}</td>
                                        <td>{{  $user->email }}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">{{ __('phone') }}</td>
                                        <td>{{  $user->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">{{ __('admin.walletBalance') }}</td>
                                        <td>{{  $user->wallet }}</td>
                                    </tr>


                                </table>
                            </div>
                            <div class="col-12 col-md-12 col-lg-5">
                                <table class="ml-0 ml-sm-0 ml-lg-0">
                                    <tr>
                                        <td class="font-weight-bold">{{ __('birth_date') }}</td>
                                        <td>{{ $user->birth_date }}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">{{ __('gender') }}</td>
                                        <td>{{  __($user->gender) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">{{ __('JoinDatetime') }}</td>
                                        <td>{{  date('Y-m-d h:i a',strtotime($user->created_at)) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">{{ __('admin.giftBalance') }}</td>
                                        <td>{{  $user->gift() }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-12">
                                <a class="btn waves-effect waves-light btn-outline-success action-compensation" data-toggle="modal" data-target="#walletModel"><i class="fa fa-wallet"></i>
                                    {{__('addToWallet')}}</a>
                                <a class="btn waves-effect waves-light btn-outline-success action-compensation" data-toggle="modal" data-target="#giftModel"><i class="fa fa-box"></i>
                                    {{__('admin.addGiftAmount')}}</a>
                            </div>

                        @if(count($user->wallets()->where('type','deposit')->get()) > 0)
                            <!-- account end -->
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    {{ __('wallet') }}
                                                    <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                                        <thead>
                                                        <tr>
                                                            <th>{{__('value')}}</th>
                                                            <th>{{__('reason')}}</th>
                                                            <th>{{__('created_at')}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($user->wallets()->where('type','deposit')->get() as $ob)
                                                            <tr>
                                                                <td>{{$ob->amount}}</td>
                                                                <td>{{$ob->notes}}</td>
                                                                <td>{{date('Y-m-d h:i a' , strtotime($ob->created_at))}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @if(count($user->gifts()->get()) > 0)
                            <!-- account end -->
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    {{ __('admin.giftBalance') }}
                                                    <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                                        <thead>
                                                        <tr>
                                                            <th>{{__('ExpiryDate')}}</th>
                                                            <th>{{__('value')}}</th>
                                                            <th>{{__('reason')}}</th>
                                                            <th>{{__('providers')}}</th>
                                                            <th>{{__('created_at')}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($user->gifts()->latest()->get() as $ob)
                                                            <tr>
                                                                <td>{{$ob->expire_date}}</td>
                                                                <td>{{$ob->amount}}</td>
                                                                <td>{{$ob->notes}}</td>
                                                                <td style="white-space: pre-line">
                                                                    @if(count($ob->providers) > 0)
                                                                        @foreach($ob->providers as $provider)
                                                                        {{$provider['store_name']}} -
                                                                        @endforeach
                                                                    @else
                                                                    {{__('admin.allProviders')}}
                                                                    @endif
                                                                </td>
                                                                <td>{{date('Y-m-d h:i a' , strtotime($ob->created_at))}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div class="modal fade" id="walletModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">{{__('admin.AddClientWallet')}}</h4></div>
            <form action="{{route('admin.addToWallet')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="user_id" value="{{$user['id']}}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">{{__('admin.balance')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <input type="number" name="amount" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">{{__('reason')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                        <textarea name="notes" class="form-control" rows="6"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn waves-effect waves-light btn-primary">{{__('send')}}</button>
                    <button type="button" class="btn waves-effect waves-light btn-default" data-dismiss="modal">{{__('close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="giftModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">{{__('admin.addGiftAmount')}}</h4></div>
            <form action="{{route('admin.wallets.addGift')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="user_id" value="{{$user['id']}}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">{{__('providers')}}</label>
                                <select name="providers[]" multiple class="form-control select2">
                                    @foreach($providers as $provider)
                                        <option value="{{$provider['id']}}">{{$provider['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">{{__('ExpiryDate')}}</label>
                                <input type="date" class="form-control" name="expire_date">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">{{__('admin.balance')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <input type="number" name="amount" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">{{__('reason')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                        <textarea name="notes" class="form-control" rows="6"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn waves-effect waves-light btn-primary">{{__('send')}}</button>
                    <button type="button" class="btn waves-effect waves-light btn-default" data-dismiss="modal">{{__('close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        $(function (){
            'use strict'
           $('.select2').select2({
               placeholder: `{{__('admin.allProviders')}}`
           });
        });
    </script>
@endpush
