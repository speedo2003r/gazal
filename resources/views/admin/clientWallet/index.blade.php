@extends('admin.layout.master')
@section('title',__('admin.clientWallet'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.clientWallet')}}</span></a>
    </div>
@endsection

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <button class="btn btn-warning btn-wide waves-effect waves-light all" data-toggle="modal" data-target="#send-wallet">
                <i class="fa fa-wallet"></i>{{__('admin.sendToWallet')}}
            </button>
            <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                {!! $dataTable->table([
                 'class' => "table table-striped table-bordered dt-responsive nowrap",
                 'id' => "clientwalletsdatatable-table",
                 ],true) !!}
            </div>
        </div>
    </div>
</div>


<!-- send-noti modal-->
<div class="modal fade" id="send-wallet"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('admin.sendToWallet')}}</h5>
            </div>
            <div class="modal-body">
                <form action="{{route('admin.wallets.addToAllUser')}}" id="sendnotifyuserForm" method="POST">
                    @csrf
                    <input type="hidden" name="data_ids" id="delete_ids">

                    <div class="form-group">
                        <label>{{__('balance')}}</label>
                        <input type="number" name="amount" class="form-control">
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-sm btn-success save">{{__('send')}}</button>
                        <button type="button" class="btn btn-default" id="notifyClose" data-dismiss="modal">{{__('close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--end send-noti modal-->
@endsection
@push('js')
    {!! $dataTable->scripts() !!}
    <script>
        $(function () {
        'use strict'
        $('.table.nowrap thead tr:first th:first').html(`
        <label class="custom-control material-checkbox" style="margin: auto">
          <input type="checkbox" class="material-control-input" id="checkedAll">
        </label>`);
        });
    </script>
@endpush
