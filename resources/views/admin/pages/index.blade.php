@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.mainPages')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('name')}}</th>
                        <th>{{__('control')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pages as $ob)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$ob->title}}</td>
                            <td>
                                <a href="javascript:void(0)" onclick="edit({{$ob}})" data-toggle="modal" data-target="#editModel"><i class="feather icon-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>


    <!-- end add model -->

    <!-- edit model -->
    <div class="modal fade" id="editModel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header"><h4 class="modal-title">{{__('editPage')}}</h4></div>
                <form action=""  id="editForm" method="post" role="form" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>{{__('title_ar')}}</label>
                                    <input type="text" id="title_ar" name="title[ar]" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>{{__('title_en')}}</label>
                                    <input type="text" id="title_en" name="title[en]" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>{{__('desc_ar')}}</label>
                                    <textarea id="desc_ar" name="desc[ar]" class="form-control" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>{{__('desc_en')}}</label>
                                    <textarea id="desc_en" name="desc[en]" class="form-control" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-primary">{{__('save')}}</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end edit model -->

@endsection
@push('js')
    <script>

        function edit(ob){

            $('#editForm')      .attr("action","{{route('admin.pages.update','obId')}}".replace('obId',ob.id));
            $('#title_ar')    .val(ob.title.ar);
            $('#title_en')     .val(ob.title.en);
            $('#desc_ar')     .val(ob.desc.ar);
            $('#desc_en')     .val(ob.desc.en);
        }
    </script>
@endpush
