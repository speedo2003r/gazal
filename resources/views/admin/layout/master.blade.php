<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
<!-- BEGIN: Head-->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="{{ settings('site_description') }}">
  <meta name="keywords" content="{{ settings('site_tagged') }} ">
  <meta name="author" content="{{ settings('site_name') }} ">
  <title>{{ \Request::route()->getAction()['title'] }}</title>
  @stack('before-css')
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Almarai:wght@300&display=swap"
        rel="stylesheet">
  <link rel="shortcut icon" type="image/x-icon"
        href="{{ settings('favicon') ? dashboard_url('storage/images/settings/' . settings('favicon')) : dashboard_url('greenLogo.png') }}">

  <link href="{{ dashboard_url('dashboard2/app-assets/fonts/googleapi.css') }}"
        rel="stylesheet">

  <!-- BEGIN: Vendor CSS-->
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/vendors' . $dir . '.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/charts/apexcharts.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/forms/select/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/pickers/pickadate/pickadate.css') }}">

  <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/editors/quill/katex.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/editors/quill/monokai-sublime.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/editors/quill/quill.snow.css') }}">

  <link rel="stylesheet" type="text/css"
        href=" {{ dashboard_url('dashboard2/app-assets/vendors/css/extensions/tether-theme-arrows.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/extensions/tether.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/extensions/shepherd-theme-default.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/file-uploaders/dropzone.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard/assets/css/filepond.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/pages/app-chat.css') }}">

  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('admin/toastr/toastr.css') }}">
  <link rel="stylesheet"
        href="{{ dashboard_url('admin/datatables-responsive/css/responsive.bootstrap4.css') }}">
  <!-- END: Vendor CSS-->

  <!-- BEGIN: Theme CSS-->
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/bootstrap.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/bootstrap-extended.css') }}">
  <link rel="stylesheet" type="text/css"
        href=" {{ dashboard_url('dashboard2/app-assets/css' . $dir . '/colors.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/plugins/forms/validation/form-validation.css') }}">

  <link rel="stylesheet" type="text/css"
        href=" {{ dashboard_url('dashboard2/app-assets/css' . $dir . '/components.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/themes/dark-layout.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/themes/semi-dark-layout.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/core/menu/menu-types/vertical-menu.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/core/menu/menu-types/horizontal-menu.css') }}">

  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/core/colors/palette-gradient.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/plugins/file-uploaders/dropzone.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/pages/data-list-view.css') }}">

  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/pages/dashboard-analytics.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/pages/card-analytics.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/plugins/tour/tour.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/pages/app-user.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/pages/app-email.css') }}">
  <!-- END: Page CSS-->

  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('admin/fontawesome-free/css/all.css') }}">

  <!-- BEGIN: csrf-token-->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- END: csrf-token-->


  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css"
        href="{{ dashboard_url('dashboard2/app-assets/css' . $dir . '/custom' . $dir . '.css') }}">
  <link rel="stylesheet" type="text/css"
        href=" {{ dashboard_url('dashboard2/assets/css/style.css') }}">
  <style>
    #datatable-table_filter {
      float: right !important;
    }
    .menu-item{
        white-space: pre-line;
    }
    .dataTables_length {
      display: flex;
    }

    .dataTables_filter {
      display: flex;
      margin-top: 10px;
    }

    .dataTables_wrapper {
      width: 100% !important;
    }

    .notification-list {
      padding-bottom: 0 !important;
    }

    /*#sidebar-menu ul li .menu-arrow:before{*/
    /*    font-family: 'Font Awesome 5 Free';*/
    /*    content: "\f107";*/
    /*}*/
    .modal-header {
      background: #fff;
    }

    .filepond--drop-label.filepond--drop-label label {
      color: #333;
    }

    .dataTables_wrapper {
      margin-top: 20px;
    }

    .page-item:first-child .page-link,
    .page-item:last-child .page-link {
      border-radius: 0;
    }

    .pagination {
      justify-content: center !important;
    }

    .images-upload-block {
      display: inline-block;
      vertical-align: top;
      position: relative;
      margin: 15px 0;
    }


    /*--------------------------------*/

    .images-upload-blockS {

      width: 85px;
      height: 85px;
      border-radius: 50%;
      border: 3px solid #EEE;
      position: relative
    }


    .images-upload-blockS .upload-img,
    .images-upload-blockS .uploaded-block .close {
      position: absolute;
      width: 25px;
      height: 25px;
      border-radius: 50%;
      background: var(--main);
      bottom: -17px;
      overflow: hidden;
      display: flex;
      color: #FFFFFF;
      font-size: 12px;
      opacity: 1;
      justify-content: center;
      align-items: center;
      z-index: 3;
      left: 50%;
      transform: translatex(-50%);
    }


    .images-upload-blockS .image-uploader {

      position: absolute;
      width: 100%;
      height: 100%;
      opacity: 0
    }

    .images-upload-blockS img {
      border-radius: 50%;
      max-width: 100%;
      width: 100%;
      height: 100%;
    }


    .images-upload-blockS .uploaded-block {
      position: absolute;
      top: 0px;
      left: 0px;
      width: 100%;
      height: 100%;
      border-radius: 50%;
    }


    /*--------------------------------*/


    #itemdatatable-table_filter {
      float: right;
    }

    .upload-img {
      display: inline-block;
      width: 130px;
      height: 120px;
      border: 1px dashed #5B5B5B;
      line-height: 140px;
      text-align: center;
      position: relative;
      border-radius: 50%;
      margin: 5px;
    }

    .upload-img i {
      font-size: 50px;
      color: #5B5B5B;
    }

    .upload-img input {
      position: absolute;
      /*opacity: 0;*/
      z-index: -1;
      /*display: none*/
    }

    .uploaded-block {
      position: absolute;
      top: 0;
      width: 140px;
      height: 135px;
    }

    .uploaded-block img {
      width: 100%;
      height: 100%;
      margin: 0;
      border-radius: 50%;
    }

    .uploaded-block .close {
      position: absolute;
      top: -8px;
      color: #FFF;
      background: #03A9F4;
      font-size: 18px;
      width: 25px;
      height: 25px;
      text-align: center;
      border-radius: 50%;
      opacity: 1;
      left: 0;
      right: 0;
      margin: auto;
      padding: 0
    }

    .block-img .images-upload-block {
      display: block;
    }

    .block-img .upload-img {
      display: table;
      margin: 5px auto;
    }

    .block-img .uploaded-block {
      position: relative;
      display: inline-block;
      margin: 5px;
    }

    .noti_message {
      padding: 20px;
      border-radius: 20px;
      display: flex;
      justify-content: space-between;
      align-items: flex-start;
      box-shadow: 0px 3px 3px 0px #EEE;
      margin: 10px 0px;
    }

    .noti_message img {
      width: 75px;
      height: 75px;
      max-width: 100%;
      border-radius: 50%;
      margin-inline-end: 20px;
    }


    .noti_message>div p {
      width: 70%;
      max-height: 70px;
      overflow: hidden;
      text-overflow: ellipsis;
    }


    .noti_message>aside {

      display: flex;
      flex-direction: column;
      align-items: flex-end;
      flex-shrink: 0;
    }


    .noti_message>aside span {
      width: 20px;
      height: 20px;
      background-color: crimson;
      color: #FFFFFF;
      border-radius: 50%;
      display: flex;
      justify-content: center;
      align-items: center
    }

    .custom-select {
      padding: .45rem 1.9rem .45rem .9rem !important;
    }

  </style>
  @stack('css')
</head>

<body class="vertical-layout vertical-menu-modern {{ session()->get('them') }}-layout 2-columns  navbar-floating footer-static  "
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns"
      data-layout="dark-layout">



  @if (Route::currentRouteName() != 'admin.show.login' && auth('admin')->check())
    @include('admin.partial.navbar')
    @include('admin.partial.sidebar')
  @endif

  <!-- Main Content -->
  <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      @yield('content')

    </div>
  </div>

  <div class="sidenav-overlay"></div>
  <div class="drag-target"></div>


  <script>
    var resizefunc = [];
  </script>

  <!-- KNOB JS -->

  <!-- BEGIN: Vendor JS-->
  <script src="{{ dashboard_url('dashboard2/app-assets/vendors/js/vendors.min.js') }}">
  </script>
  <!-- BEGIN Vendor JS-->

  <!-- BEGIN: Page Vendor JS-->
  <script
          src="{{ dashboard_url('dashboard2/app-assets/vendors/js/tables/ag-grid/ag-grid-community.min.noStyle.js') }}">
  </script>
  <!-- END: Page Vendor JS-->

  <!-- BEGIN: Theme JS-->
  <script src="{{ dashboard_url('dashboard2/app-assets/js/core/app-menu.js') }}"></script>
  <script src="{{ dashboard_url('dashboard2/app-assets/js/core/app.js') }}"></script>
  <script src="{{ dashboard_url('dashboard2/app-assets/js/scripts/components.js') }}">
  </script>
  <!-- END: Theme JS-->

  <!-- SweetAlert js -->
  <script src="{{ dashboard_url('dashboard2/app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ dashboard_url('dashboard2/app-assets/js/scripts/extensions/sweet-alerts.js') }}"
          type="text/javascript"></script>


  <script
          src="{{ dashboard_url('dashboard2/app-assets/vendors/js/tables/datatable/datatables.min.js') }}">
  </script>
  <script
          src="{{ dashboard_url('dashboard2/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}">
  </script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js">
  </script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.2/pdfmake.min.js">
  </script>
  <script src="{{dashboard_url('js/jschart/chart.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/vfs_fonts.js"></script>
  <script
          src="{{ dashboard_url('admin/datatables-responsive/js/dataTables.responsive.js') }}">
  </script>
  <script
          src="{{ dashboard_url('admin/datatables-responsive/js/responsive.bootstrap4.js') }}">
  </script>
  <script
          src="{{ dashboard_url('dashboard2/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}">
  </script>
  <script
          src="{{ dashboard_url('dashboard2/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') }}">
  </script>
  <script
          src="{{ dashboard_url('dashboard2/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}">
  </script>
  <script
          src="{{ dashboard_url('dashboard2/app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}">
  </script>
  <script
          src="{{ dashboard_url('dashboard2/app-assets/js/scripts/ui/data-list-view.js') }}">
  </script>


  <!-- App js -->
  <script src="{{ dashboard_url('dashboard/assets/js/jquery.core.js') }}"></script>
  <script src="{{ dashboard_url('dashboard/assets/js/jquery.app.js') }}"></script>
  {{-- <script src="{{dashboard_url("dashboard/assets/js/app.min.js")}}"></script> --}}
  <script src=" {{ dashboard_url('dashboard/assets/libs/parsleyjs/parsley.min.js') }}">
  </script>
  <script src="{{ dashboard_url('dashboard/assets/js/pages/form-validation.init.js') }}">
  </script>
  <script src="{{ dashboard_url('admin/select2/js/select2.js') }}"></script>
  <script src="{{ dashboard_url('admin/toastr/toastr.min.js') }}"></script>
  <script src="{{ dashboard_url('dashboard/assets/js/filepond.min.js') }}"></script>
  <script src="{{ dashboard_url('admin/adminlte.js') }}"></script>
  <script src="{{ dashboard_url('admin/moment.min.js') }}"></script>
  <script
          src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js">
  </script>
  <script src="{{ dashboard_url('dashboard/assets/js/custom.js') }}"></script>
  <!-- Dashboard init -->
  <script src="{{ dashboard_url('dashboard/assets/pages/jquery.dashboard.js') }}"></script>
  <!-- Laravel Javascript Validation -->
  <script type="text/javascript"
          src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"></script>

  @include('admin.partial.alert')
  @include('admin.partial.confirm_delete')

  <script>
    $(function() {
      'use strict'
      var a = $("#datatable").DataTable({
        dom: 'Blfrtip',
        pageLength: 10,
          lengthMenu :[
              [10,25,50,100,-1],[10,25,50,100,`{{__('view_all')}}`]
          ],
          buttons: [
              {
                  extend: 'excel',
                  text: `{{__('excel_file')}}`,
                  className: "btn btn-success"

              },
              {
                  extend: 'copy',
                  text: `{{__('copy')}}`,
                  className: "btn btn-inverse"
              },
              {
                  extend: 'print',
                  text: `{{__('print')}}`,
                  className: "btn btn-success"
              },
          ],

          "language": {
              "sEmptyTable": `{{ __('NoDataInTable') }}`,
              "sLoadingRecords": `{{ __('sLoading') }}`,
              "sProcessing": `{{ __('sLoading') }}`,
              "sLengthMenu": '{{ __('showIn') }} _MENU_ {{ __('input') }}',
              "sZeroRecords": `{{ __('noDataFind') }}`,
              "sInfo": "{{ __('showIn') }} _START_ {{ __('to') }} _END_ {{ __('outOf') }} _TOTAL_ {{ __('entry') }}",
              "sInfoEmpty": `{{ __('DisplaysOutRecords') }}`,
              "sInfoFiltered": "({{ __('selectedFromTotal') }} _MAX_ {{ __('entry') }})",
              "sInfoPostFix": "",
              "sSearch": `{{ __('search') }}:`,
              "sUrl": "",
              "oPaginate": {
                  "sFirst": `{{ __('first') }}`,
                  "sPrevious": `{{ __('previous') }}`,
                  "sNext": `{{ __('next') }}`,
                  "sLast": `{{ __('last') }}`
              },
              "oAria": {
                  "sSortAscending": `{{ __(' EnableAscendingOrder') }}:`,
                  "sSortDescending": `{{ __(': EnableDescendingOrder') }}`
              }
          }
      });
      a.buttons().container().appendTo("#datatable-table_wrapper .col-md-6:eq() ")


    });

    function confirmDelete(url) {
      $('#confirm-delete-form').attr('action', url);
    }

    function deleteAllData(type, url) {
      $('#delete_type').val(type);
      $('#delete-all').attr('action', url);
    }

    function sendToWallet(id) {
      $('#user_id').val(id);
    }

    function sendNotify(type, id) {
      $('#notify_type').val(type);
      $('#notify_id').val(id);
      $("#notifyMessage").val('');
      $('#notifyMessage').html('');
      $('.location select').val('');
      if($('#country').val() != null){
          $('[name=country_id]').val($('#country').val());
      }
      if (type == 'all') {
        $('.location').show();
      } else {
        $('.location').hide();
      }
    }

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    function changeUserStatus(id,type) {
      var tokenv = "{{ csrf_token() }}";
      var status = 1;
      $.ajax({
        type: 'POST',
        url: "{{ route('admin.changeStatus') }}",
        datatype: 'json',
        data: {
          'id': id,
          'status': status,
          'type': type,
          '_token': tokenv
        },
        success: function(res) {
          //
        }
      });
    }

    function changeItemStatus(id) {
      var tokenv = "{{ csrf_token() }}";
      var status = 1;
      $.ajax({
        type: 'POST',
        url: "{{ route('admin.items.changeStatus') }}",
        datatype: 'json',
        data: {
          'id': id,
          'status': status,
          '_token': tokenv
        },
        success: function(res) {
          //
        }
      });
    }
  </script>
  <script>
    function footerBtn(url) {
      $('tfoot').empty();
      var length = $('.table thead tr th').length;
      var html = `<tr>
                <td colspan="${length}">
                    <button class="btn btn-danger confirmDel" disabled onclick="deleteAllData('more','${url}')" data-toggle="modal" data-target="#confirm-all-del">
                        <i class="fas fa-trash"></i>
                        {{ awtTrans('حذف المحدد') }}
                    </button>
                </td>
            </tr>`;
      $('tfoot').append(html);
    }
  </script>
  <script>
{{--    @if (\Illuminate\Support\Facades\Route::current()->uri != 'admin/items/create' && \Illuminate\Support\Facades\Route::currentRouteName() != 'admin.clients.show' && \Illuminate\Support\Facades\Route::currentRouteName() != 'admin.orders.client' && \Illuminate\Support\Facades\Route::currentRouteName() != 'admin.orders.index' && \Illuminate\Support\Facades\Route::currentRouteName() != 'admin.items.edit' && \Illuminate\Support\Facades\Route::currentRouteName() != 'admin.clients.index')--}}
      // $(function () {
      // 'use strict'
      // $('.table.nowrap thead tr:first th:first').html(`
      // <label class="custom-control material-checkbox" style="margin: auto">
      //   <input type="checkbox" class="material-control-input" id="checkedAll">
      // </label>`);
      // });
{{--    @endif--}}

    function getCities(country_id, type = '', placeholder = 'اختر') {
      var html = '';
      var city_id = '';
      $('[name=city_id]').empty();
      if (country_id) {
        $.ajax({
          url: `{{ route('admin.ajax.getCities') }}`,
          type: 'post',
          dataType: 'json',
          data: {
            id: country_id
          },
          success: function(res) {
            if (type != '') {
              city_id = type;
            }
            html += `<option value="" selected hidden>${placeholder}</option>`;
            $.each(res.data, function(index, value) {
              html +=
                `<option value="${value.id}" ${city_id == value.id ? 'selected':'' }>${value.title.ar}</option>`;
            });
            $('[name=city_id]').append(html);
          }
        });
      }
    }

    function changeUserAccepted(id,type) {
      var tokenv = "{{ csrf_token() }}";
      var accepted = 1;
      $.ajax({
        type: 'POST',
        url: "{{ route('ajax.changeAccepted') }}",
        datatype: 'json',
        data: {
          'id': id,
          'accepted': accepted,
          'type': type,
          '_token': tokenv
        },
        success: function(res) {
          // if ($('#customSwitch'+id).attr('checked')) {
          //     $('#customSwitch'+id).prop('checked',false)
          // }else{
          //     $('#customSwitch'+id).prop('checked',true)
          // }
        }
      });
    }
  </script>
  <script src="{{ dashboard_url('dashboard2/app-assets/js/scripts/pages/app-user.js') }}">
  </script>

  @stack('js')

{{--  <script src="{{ asset('js/app.js') }}"></script>--}}
{{--  <script>--}}
{{--    Echo.channel('channel-order-data')--}}
{{--        .listen('.order.updated', (e) => {--}}
{{--          console.log('data from pusher: ', e);--}}
{{--        });--}}

{{--    Echo.private(`driver.1`)--}}
{{--        .listen('location.updated', (e) => {--}}
{{--          console.log(e);--}}
{{--        });--}}
{{--  </script>--}}

  <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('3a8e8bb5142a9b1cc71d', {
      cluster: 'eu',
      authEndpoint: "/broadcasting/auth",
      forceTLS: true,
      auth: {
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}',
          //'Authorization': 'Bearer ' + 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvd3d3LmdyZWVwYXBwLmNvbVwvYXBpXC9zaWduLWluIiwiaWF0IjoxNjQwMDM4Nzg3LCJuYmYiOjE2NDAwMzg3ODcsImp0aSI6IldreHI3TmJoMWw0NkJ5OE4iLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.Wul0S_NhyuEEN1mDG8QNUQ9cZlVk5yM4OFQ4wgLvcV4'
          //'Authorization': 'Bearer ' + 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvd3d3LmdyZWVwYXBwLmNvbVwvYXBpXC9zaWduLWluIiwiaWF0IjoxNjQwMDk3NTA3LCJuYmYiOjE2NDAwOTc1MDcsImp0aSI6IjdaTEFCYnVwOEd2ZmttWUwiLCJzdWIiOjExLCJwcnYiOiJmZjZjMzc1Y2VkZmRhNDM0YjMxMzJlZmE0M2Q3Mjc4Nzg4NTNmYzA5In0.aMTT4qe1qgtS9fNrLEbQfpTE84yR7pCys4GMqute5EA'
          //'Authorization': 'Bearer ' + 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvd3d3LmdyZWVwYXBwLmNvbVwvYXBpXC9zaWduLWluIiwiaWF0IjoxNjQwMTE2NzQwLCJuYmYiOjE2NDAxMTY3NDAsImp0aSI6InE5YWF2ZmRTblFtT2FRTTAiLCJzdWIiOjExLCJwcnYiOiJmZjZjMzc1Y2VkZmRhNDM0YjMxMzJlZmE0M2Q3Mjc4Nzg4NTNmYzA5In0.7cQnV8aP_AwganzBdbYqUgYGMVWEtQI564JUxv_vlTY'
          'Authorization': 'Bearer ' + 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvd3d3LmdyZWVwYXBwLmNvbVwvYXBpXC9zaWduLWluIiwiaWF0IjoxNjQwMDMwMjAxLCJuYmYiOjE2NDAwMzAyMDEsImp0aSI6ImdsS1RIRnJGQlJXZmtFejMiLCJzdWIiOjExLCJwcnYiOiJmZjZjMzc1Y2VkZmRhNDM0YjMxMzJlZmE0M2Q3Mjc4Nzg4NTNmYzA5In0.wG0Zr5Vsv_MKoyITg2Kmlyl6Kg6WEvXM7FXn_okbkdI'
          //'Authorization': 'Bearer ' + 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvd3d3LmdyZWVwYXBwLmNvbVwvYXBpXC9zaWduLWluIiwiaWF0IjoxNjQwMTE3MTkwLCJuYmYiOjE2NDAxMTcxOTAsImp0aSI6Ik9UZDZpTWlhejlFQ01ZU1MiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.VxE2NLT8btHsWHuL1CJgRlEZS2vDyHIoFND5AZ3x5Xs'
        }
      }
    });

    var channel = pusher.subscribe('channel-order-data');
    channel.bind('.order.updated', function(data) {
      console.log('pusher listen: ', JSON.stringify(data));
    });

    var channel1 = pusher.subscribe('private-driver.1');
    channel1.bind('location.updated', function(data) {
      console.log('pusher private listen: ',JSON.stringify(data));
    });
  </script>
</body>

</html>
