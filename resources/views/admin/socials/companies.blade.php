@extends('admin.layout.master')
@section('title',__('admin.companiesApp'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.companiesApp')}}</span></a>
    </div>
@endsection
<div class="content-body">
    <!-- page users view start -->
    <section class="page-users-view">
        <div class="row">
            <!-- account start -->
            <div class="col-12">
                <div class="card">
                    <form action="{{route('admin.socials.update')}}" method="post" enctype="multipart/form-data">
                        <div class="card-body">

                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{__('admin.SupportServiceCompanies')}}</label>
                                        <input type="number" name="keys[company_phone]" value="{{settings('company_phone')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{__('facebook')}}</label>
                                        <input type="text" name="socials[company_facebook]" value="{{Socials('company_facebook')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{__('instagram')}}</label>
                                        <input type="text" name="socials[company_instagram]" value="{{Socials('company_instagram')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{__('twitter')}}</label>
                                        <input type="text" name="socials[company_twitter]" value="{{Socials('company_twitter')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{__('youtube')}}</label>
                                        <input type="text" name="socials[company_youtube]" value="{{Socials('company_youtube')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success save" style="width:100%">{{__('save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
