@extends('admin.layout.master')
@section('title',__('admin.delegatesApp'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.delegatesApp')}}</span></a>
    </div>
@endsection
<div class="content-body">
    <!-- page users view start -->
    <section class="page-users-view">
        <div class="row">
            <!-- account start -->
            <div class="col-12">
                <div class="card">
                    <form action="{{route('admin.socials.update')}}" method="post" enctype="multipart/form-data">
                        <div class="card-body">

                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{__('admin.SupportNumberDelegates')}}</label>
                                        <input type="number" name="keys[delegate_phone]" value="{{settings('delegate_phone')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{__('facebook')}}</label>
                                        <input type="text" name="socials[delegate_facebook]" value="{{Socials('delegate_facebook')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{__('instagram')}}</label>
                                        <input type="text" name="socials[delegate_instagram]" value="{{Socials('delegate_instagram')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{__('twitter')}}</label>
                                        <input type="text" name="socials[delegate_twitter]" value="{{Socials('delegate_twitter')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{__('youtube')}}</label>
                                        <input type="text" name="socials[delegate_youtube]" value="{{Socials('delegate_youtube')}}" class="form-control" id="exampleInputPhone1" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success save" style="width:100%">{{__('save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
