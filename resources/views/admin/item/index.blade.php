@extends('admin.layout.master')
@section('title',__('items'))
@section('content')
    @push('css')
        <style>
            .select2-container{
                width: 100% !important;
            }
            .act-button {
                padding: 10px 0;
                display: flex;
                align-items: center;
                justify-content: center;
            }

            .act-button button {
                margin: 0 15px;
                border-radius: 30px;
            }

            .changeStar {
                background: transparent;
                border: none;
                position: absolute;
                top: 10px;
                left: 0;
                color: #ffc71b;
                font-size: 20px;
            }

            .changeStar i {
                font-size: 20px;
            }

            .cart-table .product-col {
                display: flex;
                width: 300px;
                align-items: center;
                position: relative;
            }

            .cart-table .product-col img {
                width: 100px;
                height: 100px;
                margin: auto;
            }

            .cart-table .product-col .pc-title h4 {
                font-size: 16px;
                color: #29ABE2;
                font-weight: 700;
                margin-bottom: 3px;
            }

            .cart-table .product-col .pc-title {
                display: flex;
                vertical-align: middle;
                padding-right: 30px;
                flex-direction: column;
                justify-content: center;
                text-align: center;
                margin: auto;
            }

            .cart-table .product-col .pc-title p {
                margin-bottom: 0;
                font-size: 16px;
                color: #414141;
            }
        </style>
    @endpush


@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('items')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <a href="{{route('admin.items.create')}}" class="btn btn-primary btn-wide waves-effect waves-light">
                    <i class="fas fa-plus"></i> {{__('admin.addItem')}}
                </a>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                {!! $dataTable->table([
                 'class' => "table table-striped table-bordered dt-responsive nowrap",
                 'id' => "itemdatatable-table",
                 ],true) !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    {!! $dataTable->scripts() !!}
    <script>
        footerBtn(`{{url('admin/items/delete/0')}}`);
        $(function () {
        'use strict'
        $('.table.nowrap thead tr:first th:first').html(`
        <label class="custom-control material-checkbox" style="margin: auto">
          <input type="checkbox" class="material-control-input" id="checkedAll">
        </label>`);
        });
    </script>
@endpush
