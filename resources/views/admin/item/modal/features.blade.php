<div class="modal modal_product_feature" style="background: #3333338c;overflow: scroll !important;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" style="min-width:1100px">
        <div class="modal-content">
            <div class="modal-header" style="background: #5dd5c4;direction: ltr;padding-bottom: 0 !important;">
                <h6 class="modal-title text-left" style="padding: 20px;margin-top: -10px;"> {{awtTrans('تفاصيل المنتج')}}</h6>
                <button type="button" class="close" data-dismiss="modal" style="margin-top: 10px">×</button>
            </div>
            <div class="modal-body" style="direction: rtl">
                <ul class="nav nav-tabs nav-tabs-solid nav-tabs-component nav-justified" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#product-desc-tab" role="tab" aria-controls="home" aria-selected="true">تفاصيل المنتج</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#product-options-tab" role="tab" aria-controls="profile" aria-selected="false">سمات المنتج</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="join-tab" data-toggle="tab" href="#product-joinoptions-tab" role="tab" aria-controls="join" aria-selected="false">اضافات المنتج</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active in" id="product-desc-tab">
                        <div class="product-data-row product">
{{--                            <div class="row product-data-row">--}}
{{--                                <div class="col-sm-3">--}}
{{--                                    <label class="small">رمز التخزين SKU</label>--}}
{{--                                    <div class="input-group mb-3" title="رمز التخزين SKU">--}}
{{--                                        <input type="text" v-model="forms[details].sku" class="form-control" placeholder="رمز التخزين SKU">--}}
{{--                                        <div class="input-group-append">--}}
{{--                                            <span class="input-group-text"> <i class="fa fa-barcode"></i> </span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-sm-3">--}}
{{--                                    <label class="small">النسبه المخفضه</label>--}}
{{--                                    <div class="input-group mb-3" title="النسبه المخفضه">--}}
{{--                                        <input type="text" v-model="forms[details].discount_price" class="form-control product_sale_price _parseArabicNumbers" placeholder="النسبه المخفضه">--}}
{{--                                        <div class="input-group-append">--}}
{{--                                            <span class="input-group-text"><i class="fa fa-percent"></i></span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-sm-3">--}}
{{--                                    <label class="small">بداية التخفيض</label>--}}
{{--                                    <div class="input-group mb-3" title="بداية التخفيض">--}}
{{--                                        <input v-model="forms[details].from" max="2100-04-30" class="form-control" type="datetime-local">--}}
{{--                                        <div class="input-group-append">--}}
{{--                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-sm-3">--}}
{{--                                    <label class="small">نهاية التخفيض</label>--}}
{{--                                    <div class="input-group mb-3" title="نهاية التخفيض">--}}
{{--                                        <input v-model="forms[details].to" max="2100-04-30" class="form-control" type="datetime-local">--}}
{{--                                        <div class="input-group-append">--}}
{{--                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <strong v-if="forms[details].sellerBranches.length > 0" class="my-3 d-block text-center">الفروع</strong>
                            <div class="row product-data-row">
                                <div class="col-sm-3" v-for="branch in forms[details].sellerBranches">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span>@{{ branch.title }}</span>
                                            <input type="checkbox" class="form-control" v-bind:value="branch.id" v-model="forms[details].branches" name="branches">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <label>الوصف</label>
                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" v-bind:href="langHref('ar9999',details)" role="tab" aria-expanded="false">عربي</a>
                                    <div class="slide"></div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" v-bind:href="langHref('en9999',details)" role="tab" aria-expanded="false">انجليزي</a>
                                    <div class="slide"></div>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" v-bind:id="langId('ar9999',details)" role="tabpanel" aria-expanded="false">
                                    <div class="form-group mt-20">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-12 mt-20">
                                                        <div class="form-group">
                                                            <div class="input-group" title="وصف صفحة المنتج بالعربي (Page Description)" v-bind:class="{'mb-0': forms[details].errors['description_ar'],'mb-3': !forms[details].errors['description_ar']}">
                                                                <textarea cols="30" rows="10" v-model="forms[details].description_ar" class="form-control">@{{ forms[details].description_ar }}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" v-bind:id="langId('en9999',details)" role="tabpanel" aria-expanded="false">
                                    <div class="form-group mt-20">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-12 mt-20">
                                                        <div class="form-group">
                                                            <div class="input-group" title="وصف صفحة المنتج بالانجليزيه (Page Description)" v-bind:class="{'mb-0': forms[details].errors['description_en'],'mb-3': !forms[details].errors['description_en']}">
                                                                <textarea cols="30" rows="10" v-model="forms[details].description_en" class="form-control">@{{ forms[details].description_en }}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>

                        <div class="modal-footer justify-content-between">
                            <button type="button" id="save_product_features" class="btn btn-tiffany btn-save" v-on:click="saveDetails(details)">حفظ</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" style="color: #333">{{__('close')}}</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="product-options-tab" data-type="product">
                        <div class="product-tab2" id="product_options_tab2">
                            <div class="alert alert-warning no-border mb-40">
                                <div class="align-justify">
                                    <i class="sicon-warning"></i> <strong>ملاحظة هامة</strong><br>النوع الخاص بالمنتج يمكن من اضافة نوع للمنتج (كجوال a50) ويمكن من اضافة سمه خاصه كالحجم وغيرها من السمات بداخل النوع (كالمقاس واللون) ثم يمكنك بعد ذلك من اضافة التفاصيل الخاصه بهذه السمه كالمعيار والمقياس والسعر والسمه الفرعيه (صغير - متوسط - كبير)
                                </div>
                            </div>
                            <div class="option-section clone_option_group" v-for="type, typeindex in forms[details].types">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-11">
                                                <div class="form-group">
                                                    <label for="">السمه</label>
                                                    <div class="input-group mb-3">
                                                        <select v-model="type.type_id" class="form-control" v-on:change="checkType(typeindex)">
                                                            <option value="" hidden selected>اختر</option>
                                                            <option v-for="type in types" :key="type.id" v-bind:value="type.id">@{{ type.title_ar }}</option>
                                                        </select>
                                                        <div class="input-group-append">
                                                            <button type="button" v-cloak class="btn btn-danger input-group-text" @click="delChild(details,typeindex)">حذف</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <template  v-for="detail, detailIndex in type.children">
                                                <div class="col-md-2"></div>
                                                <div class="col-sm-10">
                                                <div class="row">
                                                    <div class="col-sm-11">
                                                        <div class="row">

                                                            <div class="col-sm-4">
                                                                <label for="">الاسم بالعربي</label>
                                                                <input type="text" class="form-control" v-model="detail.title_ar" placeholder="الاسم بالعربي">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">الاسم بالانجليزي</label>
                                                                <input type="text" class="form-control" v-model="detail.title_en" placeholder="الاسم بالانجليزي">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label for="">السعر</label>
                                                                <input min="0" type="number" v-model.number="detail.price" class="form-control">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-1">
                                                        <button type="button" v-cloak class="btn btn-danger input-group-text" style="margin-top: 30px" @click="delDetail(details,typeindex,detailIndex)">حذف</button>
                                                    </div>
                                                </div>
                                            </div>

                                            </template>
                                        </div>
                                        <button type="button" id="add_option" class="btn btn-tiffany mt-4" v-on:click="addDetails(details,typeindex)"> إضافة تفاصيل </button>


                                    </div>
                                </div>
                            </div>

                            <button type="button" v-on:click="addOption(details)" class="btn border border-1 border-slate text-slate-800 btn-flat mb-4 btn-full btn-block mt-5" style="margin: 0 0 15px;color: #6c757d"><i class="fa fa-plus" style="font-size: 40px"></i></button>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" id="save_product_features" class="btn btn-tiffany btn-save" v-on:click="saveItemTypes(details)">حفظ</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" style="color: #333">{{__('close')}}</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="product-joinoptions-tab" data-type="product">
                        <div class="row mt-4">
                            <div class="form-group col-md-12 features" v-for="feature, featureindex in forms[details].features">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" v-model="feature.title_ar" placeholder="الاسم بالعربي">
                                    <input type="text" class="form-control" v-model="feature.title_en" placeholder="الاسم بالانجليزي">
                                    <input type="number" min="0" class="form-control" v-model.number="feature.price" placeholder="السعر">
                                    <div class="input-group-append">
                                        <button type="button" v-cloak class="btn btn-danger input-group-text" @click="delFeature(details,featureindex)">حذف</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" id="save_product_features" class="btn btn-tiffany btn-save" v-on:click="addFeatureDetails(details)">اضافة جديده</button>

                        <div class="modal-footer justify-content-between mt-3">
                            <button type="button" id="save_product_features" class="btn btn-tiffany btn-save mt-4" v-on:click="saveFeature(details)">حفظ</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" style="color: #333">{{__('close')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
