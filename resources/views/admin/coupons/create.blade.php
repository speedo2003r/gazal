@extends('admin.layout.master')
@section('title',__('admin.AddCoupon'))
@section('content')
@push('css')
    <style>
        .select2{
            width: 100% !important;
        }
    </style>
@endpush
@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.coupons.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.coupons')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.AddCoupon')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin.coupons.store')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('name')}}</label>
                                                <input type="text" name="title" id="title" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.usedCount')}}</label>
                                                <select name="limit" id="limit" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    <option value="limited">{{__('admin.limited')}}</option>
                                                    <option value="unlimited">{{__('admin.unlimited')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Country')}}</label>
                                                <select name="country_id" id="country_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country['id']}}">{{$country['title']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('City')}}</label>
                                                <select name="city_id" id="city_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('admin.CouponCode')}}</label>
                                                <input type="text" name="code" id="code" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('admin.SelectCategory')}}</label>
                                                <select name="type" id="type" class="form-control type">
                                                    <option value="" selected hidden>{{__('Choose')}}</option>
                                                    <option value="public">{{__('admin.general')}}</option>
                                                    <option value="private">{{__('stores')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('admin.SelectType')}}</label>
                                                <select name="kind" class="form-control kind" id="kind">
                                                    <option value="" selected hidden>{{__('Choose')}}</option>
                                                    <option value="percent">{{__('admin.PurchasingValue')}}</option>
                                                    <option value="fixed">{{__('admin.invoiceValueComprehensive')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row sellers" style="display: none">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>{{__('stores')}}</label>
                                                <select name="seller_id[]" multiple class="form-control edit_stores select2 w-100">
                                                    @foreach($providers as $provider)
                                                        <option value="{{$provider['id']}}">{{$provider['store_name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row value">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>{{__('admin.MinimumReduction')}}</label>
                                                <input type="number" name="min_order_amount" id="min_order_amount" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>{{__('admin.maxPercentAmount')}}</label>
                                                <input type="number" name="max_percent_amount" id="max_percent_amount" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>{{__('admin.NumberUses')}}</label>
                                                <input type="number" name="count" id="count" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>{{__('value')}}</label>
                                                <input type="number" name="value" id="value" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.CouponStartDate')}}</label>
                                                <input type="date" name="start_date" id="start_date" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.CouponEndDate')}}</label>
                                                <input type="date" name="end_date" id="end_date" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Admin\Coupon\Create') !!}
    <script>
        $('body').on('change','#limit',function () {
            var limit = $(this).val();
            if(limit == 'unlimited'){
                $('#count').attr('readonly',true);
                $('#count').val('');
            }else{
                $('#count').removeAttr('readonly');
            }
        });
        $('body').on('change','#country_id',function () {
            var country_id = $(this).val();
            getCities(country_id);
        });
        $(function (){
            'use strict'
            $('.select2').select2();
        });
        $(function () {
            'use strict'
            $('body').on('change','.type',function () {
                var type = $(this).val();
                if(type == 'private'){
                    $('.sellers').css({'display':'flex'});
                }else{
                    $('.sellers').css({'display':'none'});
                }

            });
        });
    </script>
@endpush
