@extends('admin.layout.master')
@section('content')



@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.DelegateJoiningOrder')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    {!! $dataTable->table([
                     'class' => "table table-striped table-bordered dt-responsive nowrap",
                     'id' => "joindatatable-table",
                     ],true) !!}
                </div>
            </div>
        </div>
    </div>


<!-- end add model -->

<!-- show model -->
    <div class="modal fade show-profile" id="contact-profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="img-div">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <img src="{{dashboard_url('mail.png')}}" alt="">
                    </div>
                    <div class="user-d text-center">
                        <p class="name" id="show_name"></p>
                        <ul>
                            <li>
                                <div class="d-block">{{__('admin.activityName')}}</div>
                                 :
                                <span id="show_store_name"></span>
                            </li>
                            <li>
                                <div class="d-block">{{__('admin.activityType')}}</div>
                                 :
                                <span id="show_category"></span>
                            </li>
                            <li>
                                <div class="d-block">{{__('phone')}}</div>
                                 :
                                <span id="show_phone"></span>
                            </li>
                            <li>
                                <div class="d-block">{{__('email')}}</div>
                                 :
                                <span id="show_email"></span>
                            </li>
                            <li>
                                <div class="d-block">{{__('Country')}}</div>
                                 :
                                <span id="show_country"></span>
                            </li>
                            <li>
                                <div class="d-block">{{__('City')}}</div>
                                 :
                                <span id="show_city"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end edit model -->

@endsection
@push('js')
    {!! $dataTable->scripts() !!}
    <script>

        footerBtn(`{{route('admin.joinUs.destroy',0)}}`);
        function show(contact){
            $('#show_name').html(contact.name);
            $('#show_store_name').html(contact.store_name);
            $('#show_category').html(contact.category.title.ar);
            $('#show_phone').html(contact.phone);
            $('#show_email').html(contact.email);
            $('#show_country').html(contact.country.title.ar);
            $('#show_city').html(contact.city.title.ar);

        }
    </script>
@endpush
