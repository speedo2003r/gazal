<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>{{__('login')}}</title>
    <link rel="apple-touch-icon" href="{{dashboard_url('dashboard2/app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{settings('favicon') ? dashboard_url('storage/images/settings/' . settings('favicon') ) : dashboard_url('greenLogo.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{dashboard_url('dashboard2/app-assets/vendors/css/vendors-rtl.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{dashboard_url('dashboard2/app-assets/css-rtl/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{dashboard_url('dashboard2/app-assets/css-rtl/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href=" {{dashboard_url('dashboard2/app-assets/css-rtl/colors.css')}}">
    <link rel="stylesheet" type="text/css" href=" {{dashboard_url('dashboard2/app-assets/css-rtl/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{dashboard_url('dashboard2/app-assets/css-rtl/themes/dark-layout.css')}}">
    <link rel="stylesheet" type="text/css" href=" {{dashboard_url('dashboard2/app-assets/css-rtl/themes/semi-dark-layout.css')}}">

    <link rel="stylesheet" type="text/css" href="{{dashboard_url('admin/toastr/toastr.css')}}">
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{dashboard_url('dashboard2/app-assets/css-rtl/core/menu/menu-types/horizontal-menu.css')}}">

    <link rel="stylesheet" type="text/css" href="{{dashboard_url('dashboard2/app-assets/css-rtl/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{dashboard_url('dashboard2/app-assets/css-rtl/pages/authentication.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{dashboard_url('dashboard2/app-assets/css-rtl/custom-rtl.css')}}">
    <link rel="stylesheet" type="text/css" href=" {{dashboard_url('dashboard2/assets/css/style-rtl.css')}}">
    <!-- END: Custom CSS-->
    <link rel="stylesheet" type="text/css" href=" {{dashboard_url('dashboard2/assets/css/style.css')}}">

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="horizontal-layout horizontal-menu dark-layout 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="hover" data-menu="horizontal-menu" data-col="1-column" data-layout="dark-layout">
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="row flexbox-container">
                <div class="col-xl-8 col-11 d-flex justify-content-center">
                    <div class="card bg-authentication rounded-0 mb-0">
                        <div class="row m-0">
                            <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                                <img src="{{dashboard_url('dashboard2/app-assets/images/pages/login.png')}}" alt="branding logo">
                            </div>
                            <div class="col-lg-6 col-12 p-0">
                                <div class="card rounded-0 mb-0 px-2">
                                    <div class="card-header pb-1">
                                        <div class="card-title">
                                            <h4 class="mb-0">{{__('login')}}</h4>
                                        </div>
                                    </div>
                                    <p class="px-2">مرحبًا بك من جديد ، يرجى تسجيل الدخول إلى حسابك.</p>
                                    @include('admin.includes.alerts.success_auth')
                                    @include('admin.includes.alerts.errors')
                                    <div class="card-content">
                                        <div class="card-body pt-1">
                                            <form method="post" action="{{route('admin.login')}}">
                                                @csrf
                                                <fieldset class="form-label-group form-group position-relative has-icon-left">
                                                    <input type="email" class="form-control" name="email" placeholder="البريد الالكتروني"
                                                           data-validation-required-message="This Email field is required" autofocus value="{{ old('email') }}">
                                                    <div class="form-control-position">
                                                        <i class="feather icon-user"></i>
                                                    </div>
                                                    <label for="user-name">{{__('emailOrPhone')}}</label>

                                                </fieldset>
                                                @error('email')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                                <fieldset class="form-label-group position-relative has-icon-left">
                                                    <input type="password" class="form-control" name="password" placeholder="الرقم السري">
                                                    <div class="form-control-position">
                                                        <i class="feather icon-lock"></i>
                                                    </div>
                                                    <label for="user-password">{{__('password')}}</label>
                                                </fieldset>

                                                @error('password')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror

                                                <fieldset class="checkbox">
                                                    <div class="vs-checkbox-con vs-checkbox-primary">
                                                        <input type="checkbox" name="remember" @if(old('remember_me') == 'on') checked @endif >
                                                        <span class="vs-checkbox">
                                                                <span class="vs-checkbox--check">
                                                                    <i class="vs-icon feather icon-check"></i>

                                                                </span>
                                                            </span>
                                                        <span class="">{{__('remember')}}</span>
                                                    </div>
                                                </fieldset>

                                                <button type="submit" class="btn btn-primary float-right btn-inline waves-effect waves-light">
                                                    {{__('login')}}</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="login-footer">
                                        <div class="divider">
                                        </div>
                                        <div class="footer-btn d-inline">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
<!-- END: Content-->


<!-- BEGIN: Vendor JS-->
<script src="{{dashboard_url('dashboard2/app-assets/vendors/js/vendors.min.js')}}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{dashboard_url('dashboard2/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{dashboard_url('admin/toastr/toastr.min.js')}}"></script>
<script src="{{dashboard_url('dashboard2/app-assets/js/core/app-menu.js')}}"></script>
<script src="{{dashboard_url('dashboard2/app-assets/js/core/app.js')}}"></script>
<script src="{{dashboard_url('dashboard2/app-assets/js/scripts/components.js')}}"></script>
<!-- END: Theme JS-->
@include('admin.partial.alert')
@include('admin.partial.confirm_delete')

<!-- BEGIN: Page JS-->
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
