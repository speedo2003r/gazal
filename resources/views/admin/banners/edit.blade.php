@extends('admin.layout.master')
@section('title',__('admin.ModifyFixedBanner'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.banners.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.FixedBanner')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.ModifyFixedBanner')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin.banners.update',$banner)}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class = "col-sm-12 text-center">
                                            <label class = "mb-0">{{__('avatar')}}</label>
                                            <div class = "text-center">
                                                <div class = "images-upload-block single-image">
                                                    <label class = "upload-img">
                                                        <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader">
                                                        <i class="fas fa-cloud-upload-alt"></i>
                                                    </label>
                                                    <div class = "upload-area" id="upload_area_img">
                                                        <div class="uploaded-block" data-count-order="0">
                                                            <a href="{{$banner['image']}}" data-fancybox="{{$banner['image']}}" data-caption="{{$banner['image']}}">
                                                                <img src="{{$banner['image']}}"></a>
                                                            <button class="close" type="button">×</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12  radio-group">
                                            <div class="row text-center">
                                                <div class="form-group col-sm-6">
                                                    <label for="seller">
                                                        <input type="radio" name="type" value="seller" class="form-control type" @if($banner['type'] == 'seller') checked @endif id="seller">
                                                        {{__('store')}}
                                                    </label>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="item">
                                                        <input type="radio" name="type" value="item" class="form-control type" @if($banner['type'] == 'item') checked @endif id="item">
                                                        {{__('item')}}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12  radio-group">
                                            <div class="row text-center">
                                                <div class="form-group col-sm-6">
                                                    <label>
                                                        <input type="date" name="start_date" class="form-control" value="{{$banner['start_date']}}" id="start_date">
                                                        {{__('from')}}
                                                    </label>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label>
                                                        <input type="date" name="end_date" class="form-control" value="{{$banner['end_date']}}" id="end_date">
                                                        {{__('to')}}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 radio-group">
                                            <label>{{__('Country')}}</label>
                                            <select name="country_id" id="country_id" class="form-control">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country['id']}}" @if($banner['country_id'] == $country['id']) selected @endif>{{$country['title']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-4  radio-group">
                                            <label>{{__('City')}}</label>
                                            <select name="city_id" id="city_id" class="form-control">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4  radio-group">
                                            <label>{{__('category')}}</label>
                                            <select name="category_id" id="category_id" class="form-control">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category['id']}}" @if($banner['category_id'] == $category['id']) selected @endif>{{$category['title']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('title_ar')}}</label>
                                                <input type="text" name="title[ar]" id="title_ar" value="{{$banner->getTranslation('title','ar')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('title_en')}}</label>
                                                <input type="text" name="title[en]" id="title_en" value="{{$banner->getTranslation('title','en')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 sellers" style="display: none">
                                            <label>{{__('stores')}}</label>
                                            <select name="provider_id" id="user_id" class="form-control edit_stores">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-12 items" style="display: none">
                                            <label>{{__('items')}}</label>
                                            <select name="item_id" id="item_id" class="form-control edit_items">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Admin\Banner\Update') !!}
    <script>

        if(`{{$banner['type']}}` == 'seller'){
            $('.sellers').css({display:'block'});
            $('.items').css({display:'none'});
        }else if(`{{$banner['type']}}` == 'item'){
            $('.sellers').css({display:'block'});
            $('.items').css({display:'block'});
        }else{
            $('.sellers').css({display:'none'});
            $('.items').css({display:'none'});
        }
        var item_id = '';
        $(function () {
            'use strict'
            @if($banner['city_id'] != null)
            getCities(`{{$banner['country_id']}}`,`{{$banner['city_id']}}`);
            @endif
            @if($banner['category_id'] != null && $banner['city_id'] != null)
            getSellers(`{{$banner['city_id']}}`, `{{$banner['category_id']}}`,`{{$banner['user_id']}}`);
            @endif
            @if($banner['category_id'] != null && $banner['user_id'] != null)
                item_id = `{{$banner['item_id']}}`;
            @endif
            $('body').on('change', '#city_id', function () {
                var category_id = $('#category_id').val();
                var city_id = $(this).val();
                if (category_id != '' && city_id != '') {
                    getSellers(city_id, category_id);
                }
            });
            $('body').on('change', '#country_id', function () {
                var country_id = $(this).val();
                getCities(country_id);
            });

            $('body').on('change', '#category_id', function () {
                var category_id = $(this).val();
                var city_id = $('#city_id').val();
                $('#item_id').empty();
                if (category_id != '' && city_id != '') {
                    getSellers(city_id, category_id);
                }
            });
        });
        $('body').on('change','#user_id',function () {
            var id = $(this).val();
            var category_id = $('#category_id').val();
            $('.edit_items').empty();
            getItems(id,category_id,item_id);
        });
        $(function () {
            'use strict'
            $('body').on('change','.radio-group input[type=radio]',function () {
                var id = $(this).val();
                if(id == 'seller'){
                    $('.sellers').css({'display':'block'});
                    $('.items').css({'display':'none'});
                }else if(id == 'item'){
                    $('.sellers').css({'display':'block'});
                    $('.items').css({'display':'block'});
                }else{
                    $('.sellers').css({'display':'none'});
                    $('.items').css({'display':'none'});
                }
            })
        });
        function getItems(id,category_id,item_id=''){
            console.log(id,category_id,item_id);
            var html = '';
            if(id > 0){
                $.ajax({
                    url:`{{route('admin.ajax.getItems')}}`,
                    type:'get',
                    postType:'json',
                    data: {id: id,category_id: category_id},
                    success:function (data) {
                        html += `<option value="" hidden selected>{{__('Choose')}}</option>`;
                        $.each(data,function (index,value) {
                            html += `<option value="${value.id}">${value.title.ar}</option>`;
                        });
                        $('.edit_items').append(html);
                        if(item_id != '' && item_id != null){
                            $('.edit_items').val(item_id).change();
                        }
                    }
                });
            }

        }
        function getSellers(id,category_id,seller_id=null){
            var html = '';
            if(id > 0){
                $.ajax({
                    url:`{{route('admin.ajax.getSellers')}}`,
                    type:'get',
                    postType:'json',
                    data: {id: id,category_id: category_id},
                    success:function (data) {
                        $('.edit_stores').empty();
                        html += `<option value="" hidden selected>{{__('Choose')}}</option>`;
                        $.each(data,function (index,value) {
                            html += `<option value="${value.id}">${value.store_name.ar}</option>`;
                        });
                        $('.edit_stores').append(html);
                        if(seller_id != null){
                            $('#user_id')     .val(seller_id).change();
                        }
                    }
                });
            }
        }
    </script>
@endpush
