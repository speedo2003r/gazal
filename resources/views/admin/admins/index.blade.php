@extends('admin.layout.master')
@section('content')
    @section('breadcrumb')
        <div style="font-size:14px ; font-family:'cairo' ; color:black">
            <a href="{{url('/')}}"><span class="user-name"> <i class="feather icon-home"></i> الرئيسيه</span></a>
            <span class="user-name">/</span>
            <a href="javacsript:void(0)"><span class="user-name">{{__('admin.supervisors')}}</span></a>
        </div>
    @endsection
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <a href="{{route('admin.admins.create')}}" class="btn btn-primary btn-wide waves-effect waves-light">
                <i class="fas fa-plus"></i> {{__('admin.addAdmin')}}
            </a>
            <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                <table id="datatable" class="table table-striped table-bordered dataTable no-footer"  style="width:100%">
                    <thead>
                    <tr>
                        <th>
                            <label class="custom-control material-checkbox" style="margin: auto">
                                <input type="checkbox" class="material-control-input" id="checkedAll">
                                <span class="material-control-indicator"></span>
                            </label>
                        </th>
                        <th>{{__('name')}}</th>
                        <th>{{__('email')}}</th>
                        <th>{{__('phone')}}</th>
                        <th>{{__('status')}}</th>
                        <th>{{__('control')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($admins as $ob)
                        <tr>
                            <td>
                                @if(\App\Models\User::first()['id'] != $ob['id'])
                                    <label class="custom-control material-checkbox" style="margin: auto">
                                        <input type="checkbox" class="material-control-input checkSingle" id="{{$ob->id}}">
                                        <span class="material-control-indicator"></span>
                                    </label>
                                @endif
                            </td>
                            <td>{{$ob->name}}</td>
                            <td>{{$ob->email}}</td>
                            <td>{{$ob->phone}}</td>
                            <td>
                                @if(\App\Models\User::first()['id'] != $ob['id'])
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success" style="direction: ltr">
                                    <input type="checkbox" onchange="changeUserStatus('{{$ob->id}}')" {{ $ob->banned == 0 ? 'checked' : '' }} class="custom-control-input" id="customSwitch{{$ob->id}}">
                                    <label class="custom-control-label" id="status_label{{$ob->id}}" for="customSwitch{{$ob->id}}"></label>
                                </div>
                                @endif
                            </td>
                            <td>
                                @if(\App\Models\User::first()['id'] != $ob['id'])
                                <a href="javascript:void(0)" onclick="confirmDelete('{{route('admin.admins.delete',$ob->id)}}')" data-toggle="modal" data-target="#delete-model">
                                    <i class="feather icon-trash"></i>
                                </a>
                                @endif
                                <a href="{{route('admin.admins.edit',$ob)}}"><i class="feather icon-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    @if(count($admins) > 0)
                        <tr>
                            <td colspan="30">
                                <button class="btn btn-danger confirmDel waves-effect waves-light" disabled onclick="deleteAllData('more','{{route('admin.admins.delete',$ob->id)}}')" data-toggle="modal" data-target="#confirm-all-del">
                                    <i class="fas fa-trash"></i>
                                    {{__('deleteSelected')}}
                                </button>
                            </td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>


@endsection
