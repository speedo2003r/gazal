@extends('admin.layout.master')
@section('title',__('admin.addAdmin'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.admins.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.supervisors')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.addAdmin')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="{{route('admin.admins.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">

                                    <div class = "col-sm-12 text-center">
                                        <label class = "mb-0">{{__('avatar')}}</label>
                                        <div class = "text-center">
                                            <div class = "images-upload-block single-image">
                                                <label class = "upload-img">
                                                    <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader">
                                                    <i class="fas fa-cloud-upload-alt"></i>
                                                </label>
                                                <div class = "upload-area" id="upload_area_img"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('name')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="text" value="{{old('name')}}" name="name" id="name" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('phone')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="number" value="{{old('phone')}}" name="phone" class="form-control" id="phone">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('email')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="email" value="{{old('email')}}" name="email" class="form-control" id="email">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('password')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="password" name="password" class="form-control" autocomplete="off">
                                        </div>
                                    </div>


                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('role')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <select name="role_id" class="form-control" id="role_id">
                                                <option value="" selected hidden disabled>{{__('choose_role')}}</option>
                                                @foreach($roles as $role)
                                                    <option value="{{$role->id}}" @if(old('role_id') == $role['id']) selected @endif>{{$role->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('Address')}}</label>
                                            <input type="text" name="address" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('Country')}}</label>
                                            <select name="country_id" class="form-control" id="country_id">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country['id']}}" @if(old('country_id') == $country['id']) selected @endif>{{$country->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('City')}}</label>
                                            <select name="city_id" class="form-control" id="city_id">
                                                <option value="">{{__('Choose')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Admin\Admin\Create') !!}
    <script>
        $(document).on('change','#country_id',function (){
            var country = $(this).val();
            getCities(country);
        });
    </script>
@endpush
