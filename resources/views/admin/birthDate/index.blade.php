@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('birthDate')}}</span></a>
    </div>
@endsection
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <button type="button" data-toggle="modal" data-target="#addModel" class="btn btn-primary btn-wide waves-effect waves-light add-user">
                <i class="fas fa-plus"></i> {{__('add_city')}}
            </button>
            <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                    <thead>
                    <tr>
                        <th>{{__('id')}}</th>
                        <th>{{__('name')}}</th>
                        <th>{{__('email')}}</th>
                        <th>{{__('phone')}}</th>
                        <th>{{__('control')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clients as $ob)
                        <tr>
                            <td>{{$ob->id}}</td>
                            <td>{{$ob->name}}</td>
                            <td>{{$ob->email}}</td>
                            <td>{{$ob->phone}}</td>
                            <td>
                                <a href="{{route('admin.marketing.birthDates.notify',$ob->id)}}"><i class="feather icon-send"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection
