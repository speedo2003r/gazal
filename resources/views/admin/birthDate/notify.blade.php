@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.clientNotifications')}}</span></a>
    </div>
@endsection
<div class="content-body">
    <!-- page users view start -->
    <section class="page-users-view">
        <div class="row">
            <!-- account start -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <form action="{{route('admin.marketing.birthDates.sendNotify')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$user['id']}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('admin.titleMessageInArabic')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <input type="text" name="title_ar" id="notifyTitle_ar" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('admin.titleMessageInEnglish')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <input type="text" name="title_en" id="notifyTitle_en" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">
                                            {{__('admin.MessageInArabic')}}<span style="color:rgb(145, 4, 4)">*</span>
                                        </label>
                                        <textarea name="message_ar" id="notifyMessage_ar" cols="30" rows="4" class="form-control"
                                                  placeholder="{{__('writeMessage')}}"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">
                                            {{__('admin.MessageInArabic')}}<span style="color:rgb(145, 4, 4)">*</span>
                                        </label>
                                        <textarea name="message_en" id="notifyMessage_en" cols="30" rows="4" class="form-control"
                                                  placeholder="{{__('writeMessage')}}"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn waves-effect waves-light btn-sm btn-success save" onclick="sendnotifyuser()">
                                        {{__('send')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


@endsection
@push('js')
    <script>
        $(function (){
           'use strict'
           $('.select2').select2({
               placeholder: `{{__('admin.allClients')}}`
           })
        });
    </script>
@endpush
