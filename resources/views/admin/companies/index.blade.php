@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.OperatingCompanies')}}</span></a>
    </div>
@endsection

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <a href="{{route('admin.companies.create')}}" class="btn btn-primary btn-wide waves-effect waves-light">
                    <i class="fas fa-plus"></i> {{__('admin.addOperatingCompany')}}
                </a>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                {!! $dataTable->table([
                 'class' => "table table-striped table-bordered dt-responsive nowrap",
                 'id' => "companydatatable-table",
                 ],true) !!}
                </div>
            </div>
        </div>
    </div>



@endsection
@push('js')
    {!! $dataTable->scripts() !!}
@endpush
