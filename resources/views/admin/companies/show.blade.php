@extends('admin.layout.master')
@section('title',__('admin.showOperatingCompany'))
@section('content')
    @push('css')
        <style>
            :root {
                --star-size: 15px;
                --star-color: #ccc;
                --star-background: #333;
            }
            .Stars {
                --percent: calc(var(--rating) / 5 * 100%);
                display: inline-block;
                font-size: var(--star-size);
                font-family: Times;
                line-height: 1;
            }
            .Stars::before {
                content: "★★★★★";
                letter-spacing: 3px;
                background: linear-gradient(90deg, var(--star-background) var(--percent), var(--star-color) var(--percent));
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
            }
        </style>

    @endpush

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.companies.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.OperatingCompanies')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.showOperatingCompany')}} ({{ $company->name }})</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="users-view-image">
                                    <img src="{{ $company->avatar }}" class="users-avatar-shadow w-100 rounded mb-2 pr-2 ml-1" alt="avatar">
                                </div>
                                <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                    <table>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('name') }}</td>
                                            <td>{{ $company->name }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('email') }}</td>
                                            <td>{{  $company->email }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('phone') }}</td>
                                            <td>{{  $company->phone }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.NumberOfOrders') }}</td>
                                            <td>{{  $company->orders()->count() }}</td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="col-12 col-md-12 col-lg-5">
                                    <table class="ml-0 ml-sm-0 ml-lg-0">
                                        <tr>
                                            <td class="font-weight-bold">{{ __('JoinDatetime') }}</td>
                                            <td>{{  date('Y-m-d h:i a',strtotime($company->created_at)) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('Country') }}</td>
                                            <td>{{  $company->country->title }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('City') }}</td>
                                            <td>{{  $company->city->title }}</td>
                                        </tr>



                                    </table>
                                </div>
                                <div class="col-12">
{{--                                    <a href="{{ route('admin.countries.edit', $country) }}" class="btn waves-effect waves-light btn-primary mr-1"><i class="feather icon-edit-1"></i> تعديل</a>--}}
                                    @if($company->banned == 0)
                                    <a class="btn waves-effect waves-light btn-outline-danger action-block" data-toggle="modal" data-target="#blockModel"><i class="fa fa-ban"></i>
                                        {{__('ban')}}</a>
                                    @else
                                        <a class="btn waves-effect waves-light btn-outline-success action-block" data-toggle="modal" data-target="#unblockModel"><i class="fa fa-check"></i>
                                            {{__('unban')}}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(count($company->blockUsers) > 0)

                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                        {{ __('admin.reasonBanUnban') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('reason')}}</th>
                                                <th>{{__('admin.Type')}}</th>
                                                <th>{{__('created_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($company->blockUsers as $ob)
                                                <tr>
                                                    <td>{{$ob->notes}}</td>
                                                    <td>{{$ob->status == 1 ? 'محظور' : 'غير محظور'}}</td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->created_at))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-md-12">
                    <h3>{{__('admin.CompanyDrivers')}}</h3>
                    <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        {!! $dataTable->table([
                         'class' => "table table-striped table-bordered dt-responsive nowrap",
                         'id' => "delegatedatatable-table",
                         ],true) !!}
                    </div>
                </div>
            </div>
        </section>

        <div class="modal fade" id="blockModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('حظر عميل')}}</h4></div>
                    <form action="{{route('admin.companies.block')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$company['id']}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">{{__('reason')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <textarea name="notes" class="form-control" rows="6"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn waves-effect waves-light btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn waves-effect waves-light btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--end send-noti modal-->
        <div class="modal fade" id="unblockModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('admin.unbanClient')}}</h4></div>
                    <form action="{{route('admin.companies.unblock')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$company['id']}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">{{__('reason')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <textarea name="notes" class="form-control" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn waves-effect waves-light btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn waves-effect waves-light btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection

@push('js')
    {!! $dataTable->scripts() !!}
@endpush
