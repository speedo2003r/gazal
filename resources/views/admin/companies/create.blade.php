@extends('admin.layout.master')
@section('title',__('admin.addOperatingCompany'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.companies.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.OperatingCompanies')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.addOperatingCompany')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin.companies.store')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">

                                        <div class = "col-sm-12 text-center">
                                            <label class = "mb-0">{{__('avatar')}}</label>
                                            <div class = "text-center">
                                                <div class = "images-upload-block single-image">
                                                    <label class = "upload-img">
                                                        <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader" >
                                                        <i class="fas fa-cloud-upload-alt"></i>
                                                    </label>
                                                    <div class = "upload-area" id="upload_area_img"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>{{__('name')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="text" name="name" value="{{old('name')}}" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('phone')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="number" name="phone" value="{{old('phone')}}" class="form-control">
                                            </div>
                                            <button type="button" class="btn btn-success waves-effect waves-light btn-sm add_phone" style="margin-top: -20px;">اضافة هاتف أخر</button>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('email')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="email" name="email" value="{{old('email')}}" class="form-control">
                                            </div>
                                            <button type="button" class="btn btn-success waves-effect waves-light btn-sm add_email" style="margin-top: -20px;">اضافة بريد الكتروني أخر</button>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('password')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="password" name="password" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Password Confirmation')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="password" name="password_confirmation" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Country')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="country_id" id="country_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country['id']}}" @if(old('country_id') == $country['id']) selected @endif>{{$country->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('City')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="city_id" id="city" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <div class="form-group">
                                                <label>{{__('Address')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="hidden" name="lat" id="lat" value="{{old('lat')}}">
                                                <input type="hidden" name="lng" id="lng" value="{{old('lng')}}">
                                                <input type="text" name="address" id="address" value="{{old('address')}}" class="form-control">
                                            </div>
                                            <div id="map" style="height: 300px"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Admin\Company\Create') !!}
    <script src="{{ dashboard_url('dashboard/assets/js/map.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ settings('map_key') }}&libraries=places&callback=initMap&lang=ar"
            async defer></script>
    <script>
        $('body').on('change','[name=country_id]',function () {
            var id = $(this).val();
            getCities(id);
        });
    @if(old('city_id'))
        getCities(`{{old('country_id')}}`,`{{old('city_id')}}`)
    @endif

    $('body').on('click','.add_phone',function () {
        var html = `<div class="form-group">
                            <label>{{__('phone')}}</label>
                            <input type="number" name="phones[]" class="form-control">
                        </div>`;
        $(this).before(html);
    });
        $('body').on('click','.add_email',function () {
            var html = `<div class="form-group">
                            <label>{{__('email')}}</label>
                            <input type="number" name="emails[]" class="form-control">
                        </div>`;
            $(this).before(html);
        });
</script>
@endpush
