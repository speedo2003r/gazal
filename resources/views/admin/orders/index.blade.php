@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.orders')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">{{__('from')}}</label>
                            <input type="date" class="form-control from" name="from">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">{{__('to')}}</label>
                            <input type="date" class="form-control to" name="to">
                        </div>
                    </div>
                </div>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <table id="table"  class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                        <tr>

                            <th> {{__('number')}} </th>
                            <th>  {{__('admin.orderNumber')}} </th>
                            <th> {{__('email')}} </th>
                            <th> {{__('phone')}} </th>
                            <th> {{__('admin.nameOfProvider')}} </th>
                            <th> {{__('admin.delegateName')}} </th>
                            @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'admin.orders.index')
                            <th>{{__('admin.orderStatus')}}</th>
                            @endif
                            <th> {{__('admin.orderTime')}} </th>
                            <th> {{__('admin.shippingRate')}} </th>
                            <th> {{__('admin.total')}} </th>

                            <th> {{__('control')}}   </th>



                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>


<!-- end edit model -->

@endsection
@push('js')
    <script>
        $(function () {
            'use strict'
            footerBtn(`{{route('admin.orders.destroy',0)}}`);
        });
        var from = '';
        var to = '';
        $('body').on('change','.from',function (){
            from = $(this).val();
            oTable.draw();
        });
        $('body').on('change','.to',function (){
            to = $(this).val();
            oTable.draw();
        });
        $('.table').dataTable().fnDestroy();
        var oTable = $('.table').DataTable({
            dom: 'Blfrtip',
            pageLength: 10,
            processing: true,
            serverSide: true,
            ajax: {
                url: `{{route('admin.orders.getFilterData',['type'=>\Illuminate\Support\Facades\Route::currentRouteName()])}}`,
                data: function (d) {
                    d.from = from;
                    d.to = to;
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'order_num', name: 'order_num'},
                {data: 'email', name: 'users.email'},
                {data: 'phone', name: 'users.phone'},
                {data: 'provider_name', name: 'provider_name'},
                {data: 'delegate_name', name: 'delegate_name'},
                @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'admin.orders.index')
                {data: 'order_status', name: 'order_status'},
                @endif
                {data: 'time', name: 'time'},
                {data: 'shipping_price', name: 'shipping_price'},
                {data: 'final_total', name: 'final_total'},
                {data: 'control', name: 'control'},
            ],
            lengthMenu :[
                [10,25,50,100,-1],[10,25,50,100,`{{__('view_all')}}`]
            ],
            buttons: [
                {
                    extend: 'excel',
                    text: `{{__('excel_file')}}`,
                    className: "btn btn-success"

                },
                {
                    extend: 'copy',
                    text: `{{__('copy')}}`,
                    className: "btn btn-inverse"
                },
                {
                    extend: 'print',
                    text: `{{__('print')}}`,
                    className: "btn btn-success"
                },
            ],

            "language": {
                "sEmptyTable": `{{ __('NoDataInTable') }}`,
                "sLoadingRecords": `{{ __('sLoading') }}`,
                "sProcessing": `{{ __('sLoading') }}`,
                "sLengthMenu": '{{ __('showIn') }} _MENU_ {{ __('input') }}',
                "sZeroRecords": `{{ __('noDataFind') }}`,
                "sInfo": "{{ __('showIn') }} _START_ {{ __('to') }} _END_ {{ __('outOf') }} _TOTAL_ {{ __('entry') }}",
                "sInfoEmpty": `{{ __('DisplaysOutRecords') }}`,
                "sInfoFiltered": "({{ __('selectedFromTotal') }} _MAX_ {{ __('entry') }})",
                "sInfoPostFix": "",
                "sSearch": `{{ __('search') }}:`,
                "sUrl": "",
                "oPaginate": {
                    "sFirst": `{{ __('first') }}`,
                    "sPrevious": `{{ __('previous') }}`,
                    "sNext": `{{ __('next') }}`,
                    "sLast": `{{ __('last') }}`
                },
                "oAria": {
                    "sSortAscending": `{{ __(' EnableAscendingOrder') }}:`,
                    "sSortDescending": `{{ __(': EnableDescendingOrder') }}`
                }
            }
        });
    </script>
@endpush
