@extends('admin.layout.master')
@section('content')
    <!-- Main content -->


    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                    <table>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.orderNumber') }}</td>
                                            <td>{{$order->id}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.orderStatus') }}</td>
                                            <td>{{trans('order.'.$order->order_status)}}
                                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#changeStatus">تغيير الحاله</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.customerName') }}</td>
                                            <td>{{!is_null($order->user) ? $order->user->name : $order->user_name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.customerPhone') }}</td>
                                            <td>{{!is_null($order->user) ? $order->user->phone : $order->user_phone}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{__('admin.orderTime')}}</td>
                                            <td>{{$order->time}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{__('admin.EstimatedDeliveryTime')}}</td>
                                            <td>{{getDrivingDistance($order['lat'],$order['lng'],$order->branch['lat'],$order->branch['lng'])['time'] + $order->provider['processing_time']}} دقيقه</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-12 col-md-12 col-lg-5">
                                    <table class="ml-0 ml-sm-0 ml-lg-0">
                                        <tr>
                                            <td class="font-weight-bold">{{__('admin.orderDate')}}</td>
                                            <td>{{date('Y-m-d' , strtotime($order->date))}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{__('Address')}}</td>
                                            <td>{{$order['map_desc']}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{__('admin.delegateName')}}</td>
                                            <td>{{$order->delegate->name}}
                                                <button type="button" class="btn btn-success btn-sm checkDelegate" data-toggle="modal" data-target="#changeDelegate">تغيير الفني</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{__('admin.delegatePhone')}}</td>
                                            <td>{{$order->delegate->phone}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{__('admin.paymentMethod')}}</td>
                                            <td>@if($order->pay_type == 'bank') تحويل بنكي @elseif($order->pay_type == 'online') اون لاين @else كاش @endif</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="row">
        <!-- account start -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <strong class="d-block text-center my-3">
                        {{__('admin.items')}}
                    </strong>
                    <div class="row">
                        @foreach($order->orderProducts as $item)

                            <div class="col-md-3 text-center">
                                <div class="nav-item">
                                    <a class="nav-link active" style="width: 150px;display: block;margin: 0 auto" data-toggle="tab" href="" role="tab" aria-controls="setting" aria-selected="true">

                                        <img src="{{$item->item->main_image}}" style="width: 100%" alt="">
                                        <span>{{$item->item->title}}</span>
                                    </a>
                                </div>
                                <div>{{$item->_single_price()}} {{__('SAR')}}</div>
                                <div>{{$item->qty}} {{__('unit')}}</div>

                            </div>
                        @endforeach
                    </div>

                    <strong class="d-block text-center my-3">
                        {{__('Address')}}
                    </strong>
                    <div class="form-group">
                        <input type="hidden" name="lat" id="lat" value="{{$order['lat']}}">
                        <input type="hidden" name="lng" id="lng" value="{{$order['lng']}}">
                        <input type="text" name="address" class="form-control" readonly value="{{$order['map_desc']}}">
                    </div>
                    <div id="map" style="height: 300px"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- END timeline item -->
                <!-- END timeline item -->
    <!-- confirm-del modal-->
    <div class="modal fade" id="notes-model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('notes')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h3 class="text-center" id="notes">

                    </h3>
                </div>
            </div>
        </div>
    </div>
    <!--end confirm-del modal-->

    <div class="modal fade" id="changeDelegate"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.appointmentDelegate')}}</h5>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.orders.assignDelegate')}}" method="POST">
                        @csrf
                        <input type="hidden" name="order_id" id="order_id" value="{{$order['id']}}">
                        <div class="form-group">
                            <label for="">
                                {{__('admin.delegate')}}
                            </label>
                            <select name="delegate_id" class="form-control" id="delegate_id">
                                <option value="" selected hidden>{{__('Choose')}}</option>
                            </select>
                        </div>

                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-sm btn-success">{{__('send')}}</button>
                            <button type="button" class="btn btn-default" id="notifyClose" data-dismiss="modal">{{__('اغلاق')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="changeStatus"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.changeStatus')}}</h5>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.orders.changeStatus')}}" method="POST">
                        @csrf
                        <input type="hidden" name="order_id" id="order_id" value="{{$order['id']}}">
                        <div class="form-group">
                            <label for="">
                                {{__('الحاله')}}
                            </label>
                            <select name="status" class="form-control">
                                <option value="" selected hidden>{{__('Choose')}}</option>
                                @foreach(\App\Entities\Order::userStatus() as $key => $value)
                                    <option value="{{$key}}" @if($order['status'] == $key) selected @endif>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-sm btn-success">{{__('send')}}</button>
                            <button type="button" class="btn btn-default" id="notifyClose" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
@endsection
@push('js')
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{settings('map_key')}}&libraries=places&callback=initMap&lang=ar"
        async defer></script>
    <script>

        $(document).on('click','.checkDelegate',function (){
            var order = `{{$order['id']}}`;
            $('#delegate_id').empty();
            $('#order_id').val(order);
            getDelegates(order);
        });
        function getDelegates(order_id,type = '',placeholder = `{{__('Choose')}}`){
            var html = '';
            var delegate_id = '';
            $('[name=delegate_id]').empty();
            if(order_id){
                $.ajax({
                    url: `<?php echo e(route('admin.ajax.getDelegates')); ?>`,
                    type: 'post',
                    dataType: 'json',
                    data:{id: order_id},
                    success: function (res) {
                        if(type != ''){
                            delegate_id = type;
                        }
                        html += `<option value="" hidden selected>${placeholder}</option>`;
                        $.each(res,function (index,value) {
                            html += `<option value="${value.id}" ${delegate_id == value.id ? 'selected':'' }>${value.name}</option>`;
                        });
                        $('[name=delegate_id]').append(html);
                    }
                });
            }
        }
        function showNotes(notes) {
            if(notes == '') notes = `{{__('admin.thereNoNotes')}}`;
            $('#notes').html(notes);
        }
        <?php $i = 0; ?>


        var places = [
            [ <?= $order->map_desc; ?>,<?= $order->lat; ?>, <?= $order->lng ?>, 1],
            [ <?= $order->branch->address; ?>, <?= $order->branch->lat; ?>, <?= $order->branch->lng ?>, 2],
        ];


        function initMap() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                innerHTML = `{{__('admin.SiteOccurred')}}`;
            }

            function showPosition(position) {
                var maplat = position.coords.latitude;
                var maplng = position.coords.longitude;

                var myLatLng = {
                    lat: maplat,
                    lng: maplng
                };
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 6,
                    center: myLatLng,
                    disableDefaultUI: false,
                    // mapTypeId: google.maps.MapTypeId.TERRAIN,
                });
                setMarkers(map);
                // }
            }

            function setMarkers(map) {
                var shape = {
                    coords: [1, 1, 1, 20, 18, 20, 18, 1],
                    type: 'poly'
                };
                for (var i = 0; i < places.length; i++) {
                    var place = places[i];

                    var marker = new google.maps.Marker({
                        position: {
                            lat: place[1],
                            lng: place[2]
                        },
                        // url : place[4],
                        map: map,
                        draggable: true,
                        shape: shape,
                        title: place[0],
                        zIndex: place[3]
                    });
                    var infowindow = new google.maps.InfoWindow({
                        content: place[0]
                    });
                    // google.maps.event.addListener(marker, 'click', function() {
                    // infowindow.open(map,marker);
                    // });

                    // marker.addListener('click', function() {
                    //  window.location.href = this.url;
                    // });
                }
            }
        }
    </script>
@endpush

