@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.clientOrders')}} {{$user['name']}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                        <thead>
                        <tr>
                            <th>{{__('admin.nameOfProvider')}}</th>
                            <th>{{__('admin.orderDate')}}</th>
                            <th>{{__('admin.orderTime')}}</th>
                            <th>{{__('admin.orderNumber')}}</th>
                            <th>{{__('admin.orderStatus')}}</th>
                            <th>{{__('admin.shippingRate')}}</th>
                            <th>{{__('admin.total')}}</th>
                            <th>{{__('control')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->ordersAsUser as $ob)
                            <tr>
                                <td>{{$ob->provider['name']}}</td>
                                <td>{{date('Y-m-d',strtotime($ob->date))}}</td>
                                <td>{{date('h:i a',strtotime($ob->time))}}</td>
                                <td>{{$ob->order_num}}</td>
                                <td>{{\App\Entities\Order::userStatus($ob->status)}}</td>
                                <td>{{$ob->shipping_price}}</td>
                                <td>{{$ob->final_total}}</td>
                                <td>
                                    <a href="{{route('admin.orders.show',$ob['id'])}}" class="btn btn-primary mx-2"><i class="fas fa-eye"></i></a>
                                    <button class="btn btn-danger" onclick="confirmDelete('{{route('admin.orders.destroy',$ob['id'])}}')" data-toggle="modal" data-target="#delete-model">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if(count($user->ordersAsUser) > 0)
                            <tr>
                                <td colspan="3">
                                    <button class="btn btn-danger confirmDel" disabled onclick="deleteAllData('more','{{route('admin.orders.destroy',$ob->id)}}')" data-toggle="modal" data-target="#confirm-all-del">
                                        <i class="fas fa-trash"></i>
                                        {{__('deleteSelected')}}
                                    </button>
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>


    <!-- /.content -->
@endsection


