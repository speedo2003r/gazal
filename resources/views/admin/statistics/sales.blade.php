@extends('admin.layout.master')
@section('title',__('admin.salesServices'))
@section('content')
@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.salesServices')}}</span></a>
    </div>
@endsection
<div class="content-body">
    <!-- page users view start -->
    <section class="page-users-view">
        <div class="row">
            <!-- account start -->
            <div class="col-12">
                <div class="row align-center">

                    <a href="#" class="col">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                        <div class="avatar-content">
                                            <i class="feather icon-list text-danger font-medium-5"></i>
                                        </div>
                                    </div>
                                    <h2 class="text-bold-700">{{$acceptedOrders}}</h2>
                                    <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.acceptedOrders')}}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="col">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                        <div class="avatar-content">
                                            <i class="feather icon-list text-danger font-medium-5"></i>
                                        </div>
                                    </div>
                                    <h2 class="text-bold-700">{{$preparedOrders}}</h2>
                                    <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.preparedOrders')}}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="col">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                        <div class="avatar-content">
                                            <i class="feather icon-list text-danger font-medium-5"></i>
                                        </div>
                                    </div>
                                    <h2 class="text-bold-700">{{$onwayOrders}}</h2>
                                    <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.onwayOrders')}}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="col">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                        <div class="avatar-content">
                                            <i class="feather icon-list text-danger font-medium-5"></i>
                                        </div>
                                    </div>
                                    <h2 class="text-bold-700">{{$arrivedOrders}}</h2>
                                    <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.arrivedOrders')}}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="col">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                        <div class="avatar-content">
                                            <i class="feather icon-list text-danger font-medium-5"></i>
                                        </div>
                                    </div>
                                    <h2 class="text-bold-700">{{$refusedOrders}}</h2>
                                    <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.refusedOrders')}}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="col">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                        <div class="avatar-content">
                                            <i class="feather icon-list text-danger font-medium-5"></i>
                                        </div>
                                    </div>
                                    <h2 class="text-bold-700">{{$timeoutOrders}}</h2>
                                    <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.timeoutOrders')}}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="col">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                        <div class="avatar-content">
                                            <i class="feather icon-list text-danger font-medium-5"></i>
                                        </div>
                                    </div>
                                    <h2 class="text-bold-700">{{$canceledOrders}}</h2>
                                    <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.canceledOrders')}}</p>
                                </div>
                            </div>
                        </div>
                    </a>


                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <div class="form-group form-inline py-2 my-0">
                                        <h3 class="kt-portlet__head-title">
                                            {{__('admin.OrderStats')}}
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <canvas id="myChart" style="background: #fff"></canvas>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <a href="#" class="col">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="feather icon-users text-danger font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{$activeDelegates}}</h2>
                            <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.activeDelegates')}}</p>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" class="col">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="feather icon-users text-danger font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{$disactiveDelegates}}</h2>
                            <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.disactiveDelegates')}}</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="form-group form-inline py-2 my-0">
            <h3 class="kt-portlet__head-title">
                {{__('admin.ordersWithAllTypeDrivers')}}
            </h3>
        </div>
        <div class="row">
            @foreach($vehicles as $key => $vehicle)
            <a href="#" class="col">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="fas fa-truck-loading text-danger font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{$vehicle}}</h2>
                            <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{$key}}</p>
                        </div>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
        <div class="row">
            <a href="#" class="col">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="feather icon-list text-danger font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{$ordersPreparesCount}}</h2>
                            <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.TotalOrdersBeingPrepared')}}</p>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" class="col">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="feather icon-list text-danger font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{$income->total ?? 0}}</h2>
                            <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.TotalIncomeSales')}}</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="row">
            <a href="#" class="col">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="feather icon-list text-danger font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{$cash}}</h2>
                            <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.orderInCash')}}</p>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" class="col">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="feather icon-list text-danger font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{$online}}</h2>
                            <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.orderInOnline')}}</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </section>
</div>
@endsection
@push('js')

    <script>
        var ctx = document.getElementById('myChart').getContext('2d');

        var arr = [];
        @foreach($data['selleds'] as $selleds)
        arr.push({{$selleds}});
        @endforeach
        var ctx = document.getElementById('myChart').getContext('2d');

        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: [`{{__('January')}}`, `{{__('February')}}`, `{{__('March')}}`,`{{__('April')}}` , `{{__('May')}}`, `{{__('June')}}`, `{{__('July')}}`,`{{__('August')}}` ,`{{__('September')}}` ,`{{__('October')}}`,`{{__('November')}}` ,`{{__('December')}}`],
                datasets: [{
                    label: "إحصائيات الطلبات",
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 3,
                    hoverBackgroundColor: "rgba(1,152,117,0.2)",
                    hoverBorderColor: "rgba(182,28,28,1)",
                    scaleStepWidth: 5,
                    borderColor: 'rgba(182,28,28,1)',
                    data: arr
                }]
            },

            // Configuration options go here
            options: {}
        });
    </script>
@endpush
