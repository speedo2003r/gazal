@extends('admin.layout.master')
@section('title',__('admin.salesProviders'))
@section('content')
@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.salesProviders')}}</span></a>
    </div>
@endsection
<div class="content-body">
    <!-- page users view start -->
    <section class="page-users-view">
        <div class="row">
            <!-- account start -->
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form action="" method="get">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">{{__('Cities')}}</label>
                                            <select name="city_id" id="city_id" class="form-control">
                                                <option value="" selected>{{__('allCities')}}</option>
                                                @foreach($cities as $city)
                                                    <option value="{{$city['id']}}" @if(request('city_id') == $city['id']) selected @endif>{{$city['title']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">{{__('providers')}}</label>
                                            <select name="provider_id" class="form-control">
                                                <option value="" selected>{{__('Choose')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">{{__('from')}}</label>
                                            <input type="date" class="form-control" value="{{request('from')}}" name="from">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">{{__('to')}}</label>
                                            <input type="date" class="form-control" name="to" value="{{request('to')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-success">{{__('send')}}</button>
                                </div>
                            </form>

                            <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer nowrap">
                                {!! $dataTable->table([
                                 'class' => "table table-striped table-bordered dt-responsive nowrap",
                                 'id' => "orderdatatable-table",
                                 ],true) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row align-center">
            <a href="#" class="col">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="feather icon-list text-danger font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{$onwayOrders}}</h2>
                            <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.onwayOrders')}}</p>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" class="col">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="feather icon-list text-danger font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{$arrivedOrders}}</h2>
                            <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.arrivedOrders')}}</p>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" class="col">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="feather icon-list text-danger font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{$refusedOrders}}</h2>
                            <p class="mb-0 line-ellipsis" style="color: #6e6a6a">{{__('admin.refusedOrders')}}</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </section>
</div>
@endsection
@push('js')
    {!! $dataTable->scripts() !!}
    <script>
        @if(request('provider_id'))
            getProvider(`{{request('city_id')}}`,`{{request('provider_id')}}`)
        @endif
        $('body').on('change','#city_id',function (){
            var city_id = $(this).val();
            getProvider(city_id);
        });
        function getProvider(city_id, type = '', placeholder = 'اختر'){
            var html = '';
            var provider = '';
            $('[name=provider_id]').empty();
            if (city_id) {
                $.ajax({
                    url: `{{ route('admin.ajax.getProviders') }}`,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: city_id
                    },
                    success: function(res) {
                        if (type != '') {
                            provider = type;
                        }
                        html += `<option value="" selected hidden>${placeholder}</option>`;
                        $.each(res, function(index, value) {
                            html +=
                                `<option value="${value.id}" ${provider == value.id ? 'selected':'' }>${value.store_name.ar}</option>`;
                        });
                        $('[name=provider_id]').append(html);
                    }
                });
            }
        }
    </script>
@endpush
