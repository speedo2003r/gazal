<a href="javascript:void(0)" onclick="confirmDelete('{{route($url,$id)}}')" data-toggle="modal" data-target="#delete-model">
    <i class="feather icon-trash"></i>
</a>
<a href="{{route($show,$data)}}"><i class="feather icon-eye"></i></a>
<a href="{{route($edit,$data)}}"><i class="feather icon-edit"></i></a>
