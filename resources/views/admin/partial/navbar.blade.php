<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav">

                        <li class="nav-item mobile-menu d-xl-none mr-auto">
                            <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                                <i class="ficon feather icon-menu"></i>
                            </a>
                        </li>

                    </ul>
                    @yield('breadcrumb')
                    <ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block">
                            {{--                            <a class="nav-link bookmark-star">--}}
                            {{--                                <i class="ficon feather icon-star warning"></i>--}}
                            {{--                            </a>--}}

                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon">
                                    <i class="feather icon-search primary"></i>
                                </div>

                                <input class="form-control input" type="text" placeholder="Explore Vuexy..." tabindex="0" data-search="template-list">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>
                </div>

                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-language nav-item">
                        @if(!session()->has('them') || session()->get('them') == 'light')
                            <a class="dropdown-toggle nav-link" id="dropdown-flag" href="{{ route('admin.them', 'dark') }}" aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-moon"></i>
                                <span class="selected-language">الوضع الليلى</span>
                            </a>
                        @else
                            <a class="dropdown-toggle nav-link" id="dropdown-flag" href="{{ route('admin.them', 'light') }}" aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-sun"></i>
                                <span class="selected-language">الوضع النهارى</span>
                            </a>
                        @endif
                    </li>
                    <!-- Start Languages -->
                    <li class="dropdown dropdown-language nav-item">
                        <a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flag-icon @if(app()->getLocale() == 'ar' ) flag-icon-sa @else flag-icon-us @endif "></i>
                            <span class="selected-language">@if(app()->getLocale() == 'ar' ) العربية @else الإنجليزية @endif</span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                            <a class="dropdown-item" href="{{ route('admin.change.lang', ['lang' => (app()->getLocale() == 'ar') ? 'en' : 'ar']) }}" data-language="en">
                                <i class="flag-icon @if(app()->getLocale() == 'ar' ) flag-icon-us @else flag-icon-sa @endif "></i>
                                @if(app()->getLocale() == 'ar' ) الإنجليزية @elseالعربية@endif
                            </a>

                        </div>
                    </li>
                    <!-- End Languages -->

                    <!-- Start maximize -->
                    <li class="nav-item d-none d-lg-block">
                        <a class="nav-link nav-link-expand">
                            <i class="ficon feather icon-maximize"></i>
                        </a>
                    </li>
                    <!-- End maximize -->

                    <!-- Start notification -->
                    <li class="dropdown dropdown-notification nav-item">
                        <a class="nav-link nav-link-label" href="{{ route('admin.notifications.index') }}">
                            <i class="ficon feather icon-bell"></i>
                            <span class="badge badge-pill badge-primary badge-up @if(count($notifications) <1) hidden @endif">{{ count($notifications) }}</span>
                        </a>
                    </li>
                    <!-- End notification -->

                    <!-- Start Messages -->
                    <li class="dropdown dropdown-notification nav-item">
                        <a class="nav-link nav-link-label" href="{{ route('admin.contacts.index') }}">
                            <i class="ficon feather icon-message-square"></i>
                            <span class="badge badge-pill badge-success badge-up @if(count($messages) <1) hidden @endif">{{ count($messages) }}</span>
                        </a>
                    </li>
                    <!-- End Messages -->

                    <!-- Start user -->
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <div class="user-nav d-sm-flex d-none">
                                <span class="user-name text-bold-600">{{ auth('admin')->user()->name }}</span>
                                {{--                                <span class="user-status">Available</span>--}}
                            </div>
                            <span>
                                <img class="round" src="{{ auth('admin')->user()->avatar }}" alt="avatar" height="40" width="40">
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ route('admin.settings.index') }}">
                                <i class="feather icon-settings"></i> الاعدادات
                            </a>

                            <a class="dropdown-item" href="{{ route('admin.contacts.index') }}">
                                <i class="feather icon-mail"></i> البريد الوارد
                            </a>

                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                تسجيل الخروج
                            </a>
                            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                    <!-- End user -->
                </ul>
            </div>
        </div>
    </div>
</nav>
