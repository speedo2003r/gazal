
<a href="{{route($url,$item_id)}}" class="mx-2"><i class="fas fa-edit"></i></a>
<a href="javascript:void(0)" onclick="confirmDelete('{{route($delete_url,$item_id)}}')" data-toggle="modal" data-target="#delete-model">
    <i class="feather icon-trash"></i>
</a>
