<a href="#" class="single" title="{{awtTrans('اضافة رصيد')}}" onclick="sendToWallet('{{ $id }}')" data-toggle="modal" data-target="#send-wallet">
    <i class="fas fa-wallet"></i>
</a>
<a href="javascript:void(0)"  data-toggle="modal" data-target="#discountModel" data-id="{{$data->id}}" data-provider="{{$data}}" class="discountEdit"><i>%</i></a>
<a href="{{route('admin.providers.edit',$id)}}" title="{{awtTrans('تعديل')}}"><i class="feather icon-edit"></i></a>

