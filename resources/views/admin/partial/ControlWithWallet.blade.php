<a href="javascript:void(0)" title="{{awtTrans('حذف')}}" onclick="confirmDelete('{{route($url,$id)}}')" data-toggle="modal" data-target="#delete-model">
    <i class="feather icon-trash"></i>
</a>
<a href="#" class="single" title="{{awtTrans('اضافة رصيد')}}" onclick="sendToWallet('{{ $id }}')" data-toggle="modal" data-target="#send-wallet">
    <i class="fas fa-wallet"></i>
</a>
<a href="javascript:void(0)" title="{{awtTrans('تعديل')}}"  onclick="edit({{$data}})" data-toggle="modal" data-target="#{{$target}}"><i class="feather icon-edit"></i></a>

