@extends('admin.layout.master')
@section('title',__('admin.editMarketingRepresentative'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.employees.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.MarketingRepresentatives')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.editMarketingRepresentative')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="{{route('admin.employees.update',$employee['id'])}}" id="editForm" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">

                                    <div class = "col-sm-12 text-center">
                                        <label class = "mb-0">{{__('avatar')}}</label>
                                        <div class = "text-center">
                                            <div class = "images-upload-block single-image">
                                                <label class = "upload-img">
                                                    <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader">
                                                    <i class="fas fa-cloud-upload-alt"></i>
                                                </label>
                                                <div class = "upload-area" id="upload_area_img">
                                                    <div class="uploaded-block" data-count-order="0">
                                                        <a href="{{ $employee['avatar'] }}"
                                                           data-fancybox="{{ $employee['avatar'] }}"
                                                           data-caption="{{ $employee['avatar'] }}">
                                                            <img src="{{ $employee['avatar'] }}"></a>
                                                        <button class="close" type="button">×</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('name')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="text" name="name" value="{{$employee['name']}}" id="name" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('phone')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="number" name="phone" value="{{$employee['phone']}}" class="form-control" id="phone">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('email')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="email" name="email" value="{{$employee['email']}}" class="form-control" id="email" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('Address')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="text" name="address" value="{{$employee['address']}}" class="form-control" id="address" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    {!! $validator->selector('#editForm') !!}
@endpush
