@extends('admin.layout.master')
@section('title',__('admin.ViewCustomerData'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.clients.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.clients')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.showClient')}} {{ $client->name }}</span></a>
    </div>
@endsection
@push('css')
    <style>
        :root {
            --star-size: 15px;
            --star-color: #ccc;
            --star-background: #333;
        }
        .Stars {
            --percent: calc(var(--rating) / 5 * 100%);
            display: inline-block;
            font-size: var(--star-size);
            font-family: Times;
            line-height: 1;
        }
        .Stars::before {
            content: "★★★★★";
            letter-spacing: 3px;
            background: linear-gradient(90deg, var(--star-background) var(--percent), var(--star-color) var(--percent));
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
    </style>

@endpush

    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="users-view-image">
                                    <img src="{{ $client->avatar }}" class="users-avatar-shadow w-100 rounded mb-2 pr-2 ml-1" alt="avatar">
                                </div>
                                <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                    <table>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('name') }}</td>
                                            <td>{{ $client->name }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('email') }}</td>
                                            <td>{{  $client->email }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('phone') }}</td>
                                            <td>{{  $client->phone }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.walletBalance') }}</td>
                                            <td>{{  $client->wallet }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.NumberOfOrders') }}</td>
                                            <td>{{  $client->ordersAsUser()->count() }}</td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="col-12 col-md-12 col-lg-5">
                                    <table class="ml-0 ml-sm-0 ml-lg-0">
                                        <tr>
                                            <td class="font-weight-bold">{{ __('birth_date') }}</td>
                                            <td>{{ $client->birth_date }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('gender') }}</td>
                                            <td>{{  __($client->gender) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('shareCode') }}</td>
                                            <td>{{  $client->share_code }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('JoinDatetime') }}</td>
                                            <td>{{  date('Y-m-d h:i a',strtotime($client->created_at)) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.totalOrders') }}</td>
                                            <?php
                                            $total = 0;
                                            foreach ($client->ordersAsUser as $order){
                                                $total += $order->_price();
                                            }
                                            ?>
                                            <td>{{  $total }}</td>
                                        </tr>



                                    </table>
                                </div>
                                <div class="col-12">
{{--                                    <a href="{{ route('admin.countries.edit', $country) }}" class="btn waves-effect waves-light btn-primary mr-1"><i class="feather icon-edit-1"></i> تعديل</a>--}}
                                    @if($client->banned == 0)
                                    <a class="btn waves-effect waves-light btn-outline-danger action-block" data-toggle="modal" data-target="#blockModel"><i class="fa fa-ban"></i>
                                        {{__('ban')}}</a>
                                    @else
                                        <a class="btn waves-effect waves-light btn-outline-success action-block" data-toggle="modal" data-target="#unblockModel"><i class="fa fa-check"></i>
                                            {{__('unban')}}</a>
                                    @endif
                                    <a class="btn waves-effect waves-light btn-outline-success action-compensation" data-toggle="modal" data-target="#compensationModel"><i class="fa fa-wallet"></i>
                                        {{__('Compensation')}}</a>
                                    <a class="btn waves-effect waves-light btn-outline-success action-compensation" data-toggle="modal" data-target="#walletModel"><i class="fa fa-wallet"></i>
                                        {{__('addToWallet')}}</a>
                                    <a href="#" class="btn waves-effect waves-light btn-outline-primary single" title="{{__('إرسال إشعار')}}" onclick="sendNotify('one' , '{{ $client['id'] }}')" data-toggle="modal" data-target="#send-noti">
                                        <i class="feather icon-send"></i>{{__('admin.SendNotify')}}
                                    </a>
                                    <a href="{{route('admin.orders.client',$client['id'])}}" class="btn waves-effect waves-light btn-outline-success">({{$client->ordersAsUser()->count()}})طلبات</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(count($client->userRatings) > 0)
                <!-- account end -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                        {{ __('admin.CustomerReviews') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('admin.orderNumber')}}</th>
                                                <th>{{__('admin.delegateName')}}</th>
                                                <th>{{__('rating')}}</th>
                                                <th>{{__('created_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($client->userRatings->groupBy('order_id') as $key => $ob)
                                                <tr>
                                                    <td>{{\App\Entities\Order::find($key)['order_name']}}</td>
                                                    <td>{{$ob->first()->user['name']}}</td>
                                                    <td>
                                                        @foreach($ob as $data)
                                                            {{$data->ratingList['title']}} : <div class="Stars" style="--rating: {{$data['rate']}};">
                                                        @endforeach
                                                    </td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->first()->user['created_at']))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(count($client->suggests) > 0)
                <!-- account end -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                        {{ __('admin.ComplaintsSuggestions') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('name')}}</th>
                                                <th>{{__('admin.ComplaintOrSuggestion')}}</th>
                                                <th>{{__('created_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($client->suggests as $ob)
                                                <tr>
                                                    <td>{{$ob->name}}</td>
                                                    <td>{{$ob->notes}}</td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->created_at))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(count($client->wallets()->where('type','deposit')->get()) > 0)
                <!-- account end -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                        {{ __('wallet') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('value')}}</th>
                                                <th>{{__('reason')}}</th>
                                                <th>{{__('created_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($client->wallets()->where('type','deposit')->get() as $ob)
                                                <tr>
                                                    <td>{{$ob->amount}}</td>
                                                    <td>{{$ob->notes}}</td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->created_at))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(count($client->wallets()->where('type','compensation')->get()) > 0)
                <!-- account end -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                        {{ __('admin.compensations') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('admin.orderNumber')}}</th>
                                                <th>{{__('value')}}</th>
                                                <th>{{__('reason')}}</th>
                                                <th>{{__('created_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($client->wallets()->where('type','compensation')->get() as $ob)
                                                <tr>
                                                    <td>{{$ob->order ? $ob->order['order_num'] : '-'}}</td>
                                                    <td>{{$ob->amount}}</td>
                                                    <td>{{$ob->notes}}</td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->created_at))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(count($client->blockUsers) > 0)
                <!-- account end -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                        {{ __('admin.ReasonsForBan') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('reason')}}</th>
                                                <th>{{__('created_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($client->blockUsers as $ob)
                                                <tr>
                                                    <td>{{$ob->notes}}</td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->created_at))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(count($client->audits) > 0)
                <!-- account end -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                        {{ __('admin.PreviousEdits') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('name')}}</th>
                                                <th>{{__('email')}}</th>
                                                <th>{{__('phone')}}</th>
                                                <th>{{__('updated_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($client->audits as $ob)
                                                <tr>
                                                    <td>{{json_decode($ob['audit_data'])->name}}</td>
                                                    <td>{{json_decode($ob['audit_data'])->email}}</td>
                                                    <td>{{json_decode($ob['audit_data'])->phone}}</td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->created_at))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </section>
        <div class="modal fade" id="blockModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('admin.banClient')}}</h4></div>
                    <form action="{{route('admin.clients.block')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$client['id']}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">{{__('reason')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <textarea name="notes" class="form-control" rows="6"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn waves-effect waves-light btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn waves-effect waves-light btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- send-noti modal-->
        <div class="modal fade" id="send-noti"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('admin.sendNotify')}}</h5>
                    </div>
                    <div class="modal-body">
                        <form action="" id="sendnotifyuserForm" method="POST">
                            @csrf
                            <input type="hidden" name="type" value="client">
                            <input type="hidden" name="notify_type" id="notify_type" value="client">
                            <input type="hidden" name="id" id="notify_id">

                            <div class="form-group">
                                <label>{{__('admin.titleMessageInArabic')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <input type="text" name="title_ar" id="notifyTitle_ar" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>{{__('admin.titleMessageInEnglish')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <input type="text" name="title_en" id="notifyTitle_en" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">
                                    {{__('admin.MessageInArabic')}}<span style="color:rgb(145, 4, 4)">*</span>
                                </label>
                                <textarea name="message_ar" id="notifyMessage_ar" cols="30" rows="4" class="form-control"
                                          placeholder="{{__('writeMessage')}}"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">
                                    {{__('admin.MessageInArabic')}}<span style="color:rgb(145, 4, 4)">*</span>
                                </label>
                                <textarea name="message_en" id="notifyMessage_en" cols="30" rows="4" class="form-control"
                                          placeholder="{{__('writeMessage')}}"></textarea>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <button type="submit" class="btn waves-effect waves-light btn-sm btn-success save" onclick="sendnotifyuser()">
                                    {{__('send')}}</button>
                                <button type="button" class="btn waves-effect waves-light btn-default" id="notifyClose" data-dismiss="modal">
                                    {{__('close')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end send-noti modal-->
        <div class="modal fade" id="unblockModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('admin.unbanClient')}}</h4></div>
                    <form action="{{route('admin.clients.unblock')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$client['id']}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <h3>{{__('AreSureUnblockClient')}}</h3>
                            </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn waves-effect waves-light btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn waves-effect waves-light btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="compensationModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('admin.CustomerCompensation')}}</h4></div>
                    <form action="{{route('admin.clients.compensation')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$client['id']}}">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="">{{__('orders')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <select name="order_id" class="form-control">
                                            <option value="" selected hidden>{{__('Choose')}}</option>
                                            @foreach($client->ordersAsUser as $order)
                                                <option value="{{$order['id']}}">{{$order['order_num']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="">{{__('admin.balance')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <input type="number" name="amount" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('reason')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <textarea name="notes" class="form-control" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn waves-effect waves-light btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn waves-effect waves-light btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="walletModel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('admin.AddClientWallet')}}</h4></div>
                    <form action="{{route('admin.addToWallet')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$client['id']}}">
                        <div class="modal-body">
                            <div class="row">
{{--                                <div class="col-sm-6">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="">{{__('ExpiryDate')}}</label>--}}
{{--                                        <input type="date" class="form-control" name="expire_date">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="">{{__('admin.balance')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <input type="number" name="amount" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('reason')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <textarea name="notes" class="form-control" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn waves-effect waves-light btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn waves-effect waves-light btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
@push('js')
    <script>
        function sendnotifyuser() {
            event.preventDefault();
            $.ajax({
                type        : 'POST',
                url         : '{{ route('admin.sendnotifyuser') }}' ,
                datatype    : 'json' ,
                async       : false,
                processData : false,
                contentType : false,
                data        : new FormData($("#sendnotifyuserForm")[0]),
                success     : function(msg){
                    if(msg.value == '0'){
                        toastr.error(msg.msg);
                    }else{
                        $('#notifyClose').trigger('click');
                        $('#notifyMessage').html('');
                        toastr.success(msg.msg);
                    }
                }
            });
        }
    </script>
@endpush
