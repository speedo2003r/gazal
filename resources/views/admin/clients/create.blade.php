@extends('admin.layout.master')
@section('title',__('admin.AddClient'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.clients.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.clients')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.addClient')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="{{route('admin.clients.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">

                                    <div class = "col-sm-12 text-center">
                                        <label class = "mb-0">{{__('avatar')}}</label>
                                        <div class = "text-center">
                                            <div class = "images-upload-block single-image">
                                                <label class = "upload-img">
                                                    <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader">
                                                    <i class="fas fa-cloud-upload-alt"></i>
                                                </label>
                                                <div class = "upload-area" id="upload_area_img"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('name')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="text" name="name" id="name" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('phone')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <input type="number" name="phone" class="form-control" id="phone">
                                                </div>
                                                <div class="col-md-4">
                                                    <select name="phone_code" class="form-control">
                                                        <option value="" hidden selected>كود الدوله</option>
                                                        @foreach($allCountries as $country)
                                                            <option value="{{$country['id']}}">{{$country['phone_code']}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('email')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="email" name="email" class="form-control" id="email" autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('birth_date')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="date" max="{{\Carbon\Carbon::now()->subYears(10)->format('Y-m-d')}}" name="birth_date" id="birth_date" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('gender')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <select name="gender" id="gender" class="form-control">
                                                <option value="" selected hidden>{{__('Choose')}}</option>
                                                <option value="male">{{__('male')}}</option>
                                                <option value="female">{{__('female')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('password')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="password" name="password" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('Password Confirmation')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="password" name="password_confirmation" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('Country')}}</label>
                                            <select name="country_id" class="form-control" id="country_id">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country['id']}}" @if(old('country_id') == $country['id']) selected @endif>{{$country->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('City')}}</label>
                                            <select name="city_id" class="form-control" id="city_id">
                                                <option value="">{{__('Choose')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Admin\Client\Create') !!}
    <script>
        $(document).on('change','#country_id',function (){
            var country = $(this).val();
            getCities(country);
        });
    </script>
@endpush
