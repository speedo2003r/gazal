@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.clients')}}</span> (<span class="clientCount">{{$clientsCount}}</span>)</a>
    </div>
@endsection
    <div class="content-header">
        <div class="content-header-left mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-6">
                    <label for="">{{__('Country')}}</label>
                    <select name="country" id="country" class="form-control">
                        <option value="" hidden selected>{{__('Choose')}}</option>
                        @foreach($countries as $country)
                            <option value="{{$country['id']}}" @if(request()->query('country_id') == $country['id']) selected @endif>{{$country['title']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-6">
                    <label for="">{{__('rateSort')}}</label>
                    <select name="rate" id="rate" class="form-control">
                        <option value="" selected>{{__('Choose')}}</option>
                        <option value="1">{{__('upToDown')}}</option>
                        <option value="2">{{__('downToUp')}}</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <a href="{{route('admin.clients.create')}}" class="btn btn-primary btn-wide waves-effect waves-light add-user">
                    <i class="fas fa-plus"></i>{{__('admin.addClient')}}
                </a>
                <button class="btn btn-warning btn-wide waves-effect waves-light all" onclick="sendNotify('all' , '0')" data-toggle="modal" data-target="#send-noti">
                    <i class="fas fa-paper-plane"></i>{{__('sendNotifyForAll')}}
                </button>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer nowrap">
                    <table id="table"  class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                        <tr>
                            <th> {{__('id')}} </th>
                            <th>{{__('name')}}  </th>
                            <th>{{__('email')}}</th>
                            <th>{{__('phone')}} </th>
                            <th> {{__('OTP')}}   </th>
                            <th> {{__('orders')}}   </th>
                            <th> {{__('addresses')}}   </th>
                            <th> {{__('Show')}}   </th>
                        </tr>
                        </thead>
                    </table>


{{--                {!! $dataTable->table([--}}
{{--                 'class' => "table table-striped table-bordered dt-responsive nowrap",--}}
{{--                 'id' => "clientdatatable-table",--}}
{{--                 ],true) !!}--}}
                </div>
            </div>
        </div>
    </div>

    <!-- send-noti modal-->
    <div class="modal fade" id="send-noti"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.sendNotify')}}</h5>
                </div>
                <div class="modal-body">
                    <form action="" id="sendnotifyuserForm" method="POST">
                        @csrf
                        <input type="hidden" name="country_id">
                        <input type="hidden" name="type" value="client">
                        <input type="hidden" name="notify_type" id="notify_type" value="client">
                        <input type="hidden" name="id" id="notify_id">

                        <div class="form-group">
                            <label>{{__('admin.titleMessageInArabic')}}</label>
                            <input type="text" name="title_ar" id="notifyTitle_ar" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>{{__('admin.titleMessageInEnglish')}}</label>
                            <input type="text" name="title_en" id="notifyTitle_en" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">
                                {{__('admin.MessageInArabic')}}
                            </label>
                            <textarea name="message_ar" id="notifyMessage_ar" cols="30" rows="4" class="form-control"
                                      placeholder="{{__('writeMessage')}}"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">
                                {{__('admin.MessageInEnglish')}}
                            </label>
                            <textarea name="message_en" id="notifyMessage_en" cols="30" rows="4" class="form-control"
                                      placeholder="{{__('writeMessage')}}"></textarea>
                        </div>

                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-sm btn-success save" onclick="sendnotifyuser()">{{__('send')}}</button>
                            <button type="button" class="btn btn-default" id="notifyClose" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end send-noti modal-->

@endsection
@push('js')
    <script>
        $('body').on('click','.single,.all',function (){
            console.log(true);
            $("#notifyTitle_ar").val('');
            $("#notifyTitle_en").val('');
            $("#notifyMessage_ar").val('');
            $("#notifyMessage_en").val('');
        })

        function sendnotifyuser() {
            event.preventDefault();
            $.ajax({
                type        : 'POST',
                url         : '{{ route('admin.sendnotifyuser') }}' ,
                datatype    : 'json' ,
                async       : false,
                processData : false,
                contentType : false,
                data        : new FormData($("#sendnotifyuserForm")[0]),
                success     : function(msg){
                    if(msg.value == '0'){
                        toastr.error(msg.msg);
                    }else{
                        $('#notifyClose').trigger('click');
                        $('#notifyMessage').html('');
                        toastr.success(msg.msg);
                    }
                }
            });
        }
        var country_id = '';
        var rate = '';
        $('body').on('change','#country', function() {
            country_id = $(this).val();
            $.ajax({
                type        : 'POST',
                url         : '{{ route('admin.ajax.getClientCount') }}' ,
                datatype    : 'json' ,
                data        : {country_id:country_id},
                success     : function(data){
                    console.log(data);
                    $('.clientCount').html(data.data);
                }
            });
            oTable.draw();
        });
        $('body').on('change','#rate', function() {
            rate = $(this).val();
            oTable.draw();
        });
        $('.table').dataTable().fnDestroy();
        var oTable = $('.table').DataTable({
            dom: '<"top"<"actions">Blfrtip<"clear">>rt<"bottom"iflp<"clear">>',
            pageLength: 10,
            processing: true,
            serverSide: true,
            ajax: {
                url: `{{route('admin.clients.getFilterData')}}`,
                data: function (d) {
                    d.country_id = country_id;
                    d.rate = rate;
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'v_code', name: 'v_code'},
                {data: 'orders', name: 'orders'},
                {data: 'addresses', name: 'addresses'},
                {data: 'control', name: 'control'},
            ],
            lengthMenu :[
                [10,25,50,100,-1],[10,25,50,100,`{{__('view_all')}}`]
            ],
            buttons: [
                {
                    extend: 'excel',
                    text: `{{__('excel_file')}}`,
                    className: "btn btn-success"

                },
                {
                    extend: 'copy',
                    text: `{{__('copy')}}`,
                    className: "btn btn-inverse"
                },
                {
                    extend: 'print',
                    text: `{{__('print')}}`,
                    className: "btn btn-success"
                },
            ],

            "language": {
                "sEmptyTable": `{{ __('NoDataInTable') }}`,
                "sLoadingRecords": `{{ __('sLoading') }}`,
                "sProcessing": `{{ __('sLoading') }}`,
                "sLengthMenu": '{{ __('showIn') }} _MENU_ {{ __('input') }}',
                "sZeroRecords": `{{ __('noDataFind') }}`,
                "sInfo": "{{ __('showIn') }} _START_ {{ __('to') }} _END_ {{ __('outOf') }} _TOTAL_ {{ __('entry') }}",
                "sInfoEmpty": `{{ __('DisplaysOutRecords') }}`,
                "sInfoFiltered": "({{ __('selectedFromTotal') }} _MAX_ {{ __('entry') }})",
                "sInfoPostFix": "",
                "sSearch": `{{ __('search') }}:`,
                "sUrl": "",
                "oPaginate": {
                    "sFirst": `{{ __('first') }}`,
                    "sPrevious": `{{ __('previous') }}`,
                    "sNext": `{{ __('next') }}`,
                    "sLast": `{{ __('last') }}`
                },
                "oAria": {
                    "sSortAscending": `{{ __(' EnableAscendingOrder') }}:`,
                    "sSortDescending": `{{ __(': EnableDescendingOrder') }}`
                }
            }
        });
    </script>
@endpush
