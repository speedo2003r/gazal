@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.clients.index') }}"><span class="user-name"> <i class="feather icon-home"></i>{{__('admin.clients')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.addressesPage')}} ({{$client['name']}})</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <a href="{{route('admin.clients.addressCreate',$client['id'])}}" class="btn btn-primary btn-wide waves-effect waves-light add-user">
                    <i class="fas fa-plus"></i>{{__('admin.addAddress')}}
                </a>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer nowrap">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive"  style="width:100%">
                        <thead>
                        <tr>
                            <th>{{__('name')}}</th>
                            <th>{{__('street')}}</th>
                            <th>{{__('building')}}</th>
                            <th>{{__('floor')}}</th>
                            <th>{{__('flagNo')}}</th>
                            <th>{{__('uniqueAssign')}}</th>
                            <th>{{__('control')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($client->addresses as $ob)
                            <tr>
                                <td>{{$ob->name}}</td>
                                <td>{{$ob->street}}</td>
                                <td>{{$ob->building}}</td>
                                <td>{{$ob->floor}}</td>
                                <td>{{$ob->flat}}</td>
                                <td>{{$ob->unique_sign}}</td>
                                <td>
                                    <a href="{{route('admin.clients.addressEdit',$ob['id'])}}"><i class="feather icon-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
