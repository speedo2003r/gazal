@extends('admin.layout.master')
@section('title',__('addAddress'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.clients.addresses',$client['id']) }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.addressesPage')}} ({{$client['name']}})</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.addAddress')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="{{route('admin.clients.storeAddress')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" value="{{$client['id']}}" name="user_id">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>{{__('Country')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <select name="country_id" id="country_id" class="form-control">
                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country['id']}}" @if($country['id'] == old('country_id')) selected @endif>{{$country['title']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>{{__('City')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <select name="city_id" class="form-control">
                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('name')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="text" name="name" class="form-control" value="{{old('name')}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('street')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="text" name="street" class="form-control" value="{{old('street')}}" autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>{{__('buildingNo')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="number" name="building" class="form-control" value="{{old('building')}}">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>{{__('floorNo')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="number" name="floor" class="form-control" value="{{old('floor')}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>{{__('flagNo')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="number" name="flat" class="form-control" value="{{old('flat')}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('uniqueAssign')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="text" name="unique_sign" class="form-control" value="{{old('unique_sign')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{__('addressType')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <select name="type" class="form-control">
                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                            <option value="home" @if(old('type') == 'home') selected @endif>{{__('house')}}</option>
                                            <option value="work" @if(old('type') == 'work') selected @endif>{{__('work')}}</option>
                                            <option value="esteraha" @if(old('type') == 'esteraha') selected @endif>{{__('break')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('phone')}}</label>
                                            <input type="number" name="phone" id="phone" class="form-control" value="{{old('phone')}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 mb-3">
                                        <div class="form-group">
                                            <label>{{__('Address')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="hidden" name="lat" id="lat" value="24.7135517">
                                            <input type="hidden" name="lng" id="lng" value="46.67529569999999">
                                            <input type="text" name="map_desc" id="address" value="{{old('map_desc')}}" class="form-control pac-target-input" placeholder="Enter a query" autocomplete="off">
                                            <div id="map" style="height: 300px"></div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Admin\Addresses\Create') !!}
    <script src="{{dashboard_url('dashboard/assets/js/map.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{settings('map_key')}}&libraries=places&callback=initMap&language=ar"
        async defer></script>
    <script>
        $(function (){
           'use strict'
           $('body').on('click','#country_id',function (){
               var country_id = $(this).val();
               getCities(country_id);
           });
        });
    </script>
@endpush
