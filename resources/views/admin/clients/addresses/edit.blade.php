@extends('admin.layout.master')
@section('title',__('admin.editAddress'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.clients.addresses',$client['id']) }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.addressesPage')}} ({{$client['name']}})</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.editAddress')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="{{route('admin.clients.updateAddress',$address['id'])}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>{{__('Country')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <select name="country_id" id="country_id" class="form-control">
                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country['id']}}" @if($address['country_id'] == $country['id']) selected @endif>{{$country['title']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>{{__('City')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <select name="city_id" id="city_id" class="form-control">
                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('name')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="text" name="name" value="{{$address['name']}}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('street')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="text" name="street" value="{{$address['street']}}" class="form-control" autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>{{__('buildingNo')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="number" value="{{$address['building']}}" name="building" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>{{__('floorNo')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="number" value="{{$address['floor']}}" name="floor" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>{{__('flagNo')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="number" value="{{$address['flat']}}" name="flat" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('uniqueAssign')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="text" value="{{$address['unique_sign']}}" name="unique_sign" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{__('addressType')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <select name="type" class="form-control">
                                            <option value="" hidden selected>{{__('Choose')}}</option>
                                            <option value="home" @if($address['type'] == 'home') selected @endif>{{__('house')}}</option>
                                            <option value="work" @if($address['type'] == 'work') selected @endif>{{__('work')}}</option>
                                            <option value="esteraha" @if($address['type'] == 'esteraha') selected @endif>{{__('break')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('phone')}}</label>
                                            <input type="number" name="phone" value="{{$address['phone']}}" id="phone" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 mb-3">
                                        <div class="form-group">
                                            <label>{{__('Address')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                            <input type="hidden" name="lat" id="lat" value="{{$address['lat']}}">
                                            <input type="hidden" name="lng" id="lng" value="{{$address['lng']}}">
                                            <input type="text" name="map_desc" id="address" class="form-control pac-target-input" value="{{$address['map_desc']}}" placeholder="Enter a query" autocomplete="off">
                                            <div id="map" style="height: 300px"></div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    <script>
        getCities(`{{$address['country_id']}}`,`{{$address['city_id']}}`);
    </script>
    {!! JsValidator::formRequest('App\Http\Requests\Admin\Addresses\Update') !!}
@endpush
