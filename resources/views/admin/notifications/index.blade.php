@extends('admin.layout.master')

@section('title', __('notifications'))

@section('content')
    <!--content wrapper -->
    <!--end of content wrapper -->

    <div class="content-body">
        <section id="alert-colors">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">
                            {{__('notifications')}}
                        </h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            @if(count($notifications) > 0)
                            @foreach($notifications as $notification)
                                    <div class="alert alert-primary" role="alert">
                                        <p class="mb-0">{{ $notification->message }}</p>
                                    </div>
                            @endforeach
                            @else

                            <h2 class="text-center">{{__('admin.thereNoNotifications')}}</h2>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
@endsection
