@extends('admin.layout.master')
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">{{awtTrans('مدفوعات المندوبين')}}</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                {!! $dataTable->table([
                 'class' => "table table-striped table-bordered dt-responsive nowrap",
                 'id' => "admindelegatepaydatatable-table",
                 ],true) !!}
                </div>
            </div>
        </div>
    </div>

    <!-- send-noti modal-->
    <div class="modal fade" id="exampleModal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{awtTrans('تفاصيل')}}</h5>
                </div>
                <div class="modal-body">
                    <div class="bank d-none">
                        <div class="form-group">
                            <label for="bank_name">{{awtTrans('اسم البنك')}}</label>
                            <span id="bank_name"></span>
                        </div>
                        <div class="form-group">
                            <label for="bank_name">{{awtTrans('اسم صاحب الحساب')}}</label>
                            <span id="acc_owner_name"></span>
                        </div>
                        <div class="form-group">
                            <label for="bank_name">{{awtTrans('رقم الحساب')}}</label>
                            <span id="acc_number"></span>
                        </div>
                        <div class="form-group">
                            <label for="bank_name">{{awtTrans('المبلغ المدفوع')}}</label>
                            <span id="price"></span>
                        </div>
                        <div class="form-group" style="margin-bottom: 0">
                            <label for="bank_name">{{awtTrans('صورة الايصال')}}</label>
                            <a href="" target="_blank" class="image_link">
                                <img src="" style="width: 200px;height: 200px" alt="" id="image">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModal2">
        <div class="modal-dialog model-sm">
            <div class="modal-content">
                <form action="{{route('admin.delegateAccount.accept')}}" method="post" id="confirm-form">
                    @csrf
                    <input type="hidden" name="pay_id" id="pay_id">
                    <div class="modal-header"><h6 class="modal-title">{{awtTrans('تأكيد القبول')}}</h6></div>
                    <div class="modal-body p-5"><h5 class="text-center">{{awtTrans('هل انت متاكد من قبول الدفع')}}</h5></div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{awtTrans('الغاء')}}</button>
                        <button type="submit" class="btn btn-success">{{awtTrans('تأكيد')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal3">
        <div class="modal-dialog model-sm">
            <div class="modal-content">
                <form action="{{route('admin.delegateAccount.refuse')}}" method="post" id="confirm-form">
                    @csrf
                    <input type="hidden" name="pay_id">
                    <div class="modal-header"><h6 class="modal-title">{{awtTrans('تأكيد الرفض')}}</h6></div>
                    <div class="modal-body p-5"><h5 class="text-center">{{awtTrans('هل انت متاكد من الرفض')}}</h5></div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{awtTrans('الغاء')}}</button>
                        <button type="submit" class="btn btn-success">{{awtTrans('تأكيد')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('js')
    {!! $dataTable->scripts() !!}

    <script>

        $(function (){
            'use strict'
            $(document).on('click','.openDeletemodal',function (){
                var id = $(this).data('id');
                $('#exampleModal3 form input[name=pay_id]').val(id);
            });
            $(document).on('click','.openEditmodal',function (){
                var id = $(this).data('id');
                $('#exampleModal2 form input[name=pay_id]').val(id);
            });
        })
        $(function () {
            'use strict'
            $(document).on('click','.detail',function (){
                var data = $(this).data('data');
                if(data.type == 'bank'){
                    $('.bank').removeClass('d-none').siblings().addClass('d-none')
                    $('#bank_name').html(data.bank_name);
                    $('#acc_owner_name').html(data.acc_owner_name);
                    $('#acc_number').html(data.acc_number);
                    $('#price').html(data.price);
                    $('#image').attr('src',data.image);
                    $('.image_link').attr('href',data.image);
                }
            })
        });
    </script>
@endpush
