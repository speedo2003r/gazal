@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.QuestionsAnswers')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <button type="button" data-toggle="modal" data-target="#editModel" class="btn btn-primary btn-wide waves-effect waves-light add-user">
                    <i class="fas fa-plus"></i> {{__('admin.AddQuestion')}}
                </button>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                        <thead>
                        <tr>
                            <th>
                                <label class="custom-control material-checkbox" style="margin: auto">
                                    <input type="checkbox" class="material-control-input" id="checkedAll">
                                    <span class="material-control-indicator"></span>
                                </label>
                            </th>
                            <th>{{__('admin.Question')}}</th>
                            <th>{{__('control')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($questions as $ob)
                            <tr>
                                <td>
                                    <label class="custom-control material-checkbox" style="margin: auto">
                                        <input type="checkbox" class="material-control-input checkSingle" id="{{$ob->id}}">
                                        <span class="material-control-indicator"></span>
                                    </label>
                                </td>
                                <td>{{$ob->key}}</td>
                                <td>
                                    <a href="javascript:void(0)" onclick="confirmDelete('{{route('admin.questions.destroy',$ob->id)}}')" data-toggle="modal" data-target="#delete-model">
                                        <i class="feather icon-trash"></i>
                                    </a>
                                    <a href="javascript:void(0)" onclick="edit({{$ob}})" data-toggle="modal" data-target="#editModel"><i class="feather icon-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>



<!-- edit model -->
<div class="modal fade" id="editModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">تعديل دوله</h4></div>
            <form action=""  id="editForm" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('admin.ArQuestion')}}</label>
                                <input type="text" id="key_ar" name="key[ar]" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('admin.EnQuestion')}}</label>
                                <input type="text" id="key_en" name="key[en]" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('admin.ArAnswer')}}</label>
                                <textarea id="value_ar" name="value[ar]" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('admin.EnAnswer')}}</label>
                                <textarea id="value_en" name="value[en]" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-primary">{{__('save')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit model -->
@endsection
@push('js')
    <script>
        $('.add-user').on('click',function () {
            $('#editModel .modal-title').text(`{{__('admin.AddQuestion')}}`);
            $('#editForm :input:not([type=checkbox],[type=radio],[type=hidden])').val('');
            $('#editForm')      .attr("action","{{route('admin.questions.store')}}");
        });
        function edit(ob){
            $('#editModel .modal-title').text(`{{__('admin.EditQuestion')}}`);
            $('#editForm')      .attr("action","{{route('admin.questions.update','obId')}}".replace('obId',ob.id));
            $('#key_ar')    .val(ob.key.ar);
            $('#key_en')    .val(ob.key.en);
            $('#value_ar')    .val(ob.value.ar);
            $('#value_en')    .val(ob.value.en);
        }
    </script>
@endpush
