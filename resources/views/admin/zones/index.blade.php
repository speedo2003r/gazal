@extends('admin.layout.master')
@section('content')


@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name"> {{ __('admin.GeographicalRange') }}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <button type="button" data-toggle="modal" data-target="#addModel"
                        class="btn btn-primary btn-wide waves-effect waves-light add-user">
                    <i class="fas fa-plus"></i> {{ __('Add') }}
                </button>
                <div id="datatable_wrapper"
                     class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <table id="datatable"
                           class="table table-striped table-bordered dt-responsive nowrap"
                           style="width:100%">
                        <thead>
                        <tr>
                            <th>
                                <label class="custom-control material-checkbox" style="margin: auto">
                                    <input type="checkbox" class="material-control-input" id="checkedAll">
                                    <span class="material-control-indicator"></span>
                                </label>
                            </th>
                            <th>{{ __('name') }}</th>
                            <th>{{ __('City') }}</th>
                            <th>{{ __('control') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($zones as $ob)
                            <tr>
                                <td>
                                    <label class="custom-control material-checkbox" style="margin: auto">
                                        <input type="checkbox" class="material-control-input checkSingle"
                                               id="{{ $ob->id }}">
                                        <span class="material-control-indicator"></span>
                                    </label>
                                </td>
                                <td>{{ $ob->title }}</td>
                                <td>{{ $ob->city->title }}</td>
                                <td>
                                    <a href="javascript:void(0)"
                                       onclick="confirmDelete('{{ route('admin.zones.destroy', $ob->id) }}')"
                                       data-toggle="modal" data-target="#delete-model">
                                        <i class="feather icon-trash"></i>
                                    </a>
                                    <a title="{{ __('admin.EditGeographicalRange') }}" href="{{ route('admin.zones.edit.map', $ob->id) }}">
                                        <i class="feather icon-map"></i>
                                    </a>
                                    <a href="javascript:void(0)" onclick="edit({{ $ob }})"
                                       data-toggle="modal" data-target="#editModel"><i
                                                class="feather icon-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if (count($zones) > 0)
                            <tr>
                                <td colspan="30">
                                    <button class="btn btn-danger confirmDel" disabled
                                            onclick="deleteAllData('more','{{ route('admin.zones.destroy', $ob->id) }}')"
                                            data-toggle="modal" data-target="#confirm-all-del">
                                        <i class="fas fa-trash"></i>
                                        {{ __('deleteSelected') }}
                                    </button>
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>


    <!-- add model -->
    <div class="modal fade" id="addModel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Add') }}</h4>
                </div>
                <form action="{{ route('admin.zones.store') }}" method="post" role="form"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>{{ __('title_ar') }}</label>
                                    <input type="text" name="title[ar]" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>{{ __('title_en') }}</label>
                                    <input type="text" name="title[en]" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>{{ __('cities') }}</label>
                                    <select name="city_id" class="form-control">
                                        <option value="" selected hidden>{{ __('Choose') }}</option>
                                        @foreach ($cities as $city)
                                            <option value="{{ $city['id'] }}">{{ $city->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-primary">{{ __('save') }}</button>
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('close') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end add model -->

    <!-- edit model -->
    <div class="modal fade" id="editModel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit') }}</h4>
                </div>
                <form action="" id="editForm" method="post" role="form"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>{{ __('title_ar') }}</label>
                                    <input type="text" name="title[ar]" id="title_ar" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>{{ __('title_en') }}</label>
                                    <input type="text" name="title[en]" id="title_en" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>{{ __('cities') }}</label>
                                    <select name="city_id" id="city_id" class="form-control">
                                        <option value="" selected hidden>{{ __('Choose') }}</option>
                                        @foreach ($cities as $city)
                                            <option value="{{ $city['id'] }}">{{ $city->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-primary">{{ __('save') }}</button>
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('close') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end edit model -->

@endsection
@push('js')
    <script>
      function edit(ob) {
        $('#editForm').attr("action", "{{ route('admin.zones.update', 'obId') }}".replace(
          'obId', ob.id));
        $('#title_ar').val(ob.title.ar);
        $('#title_en').val(ob.title.en);
        $('#city_id').val(ob.city_id).change;
      }
    </script>
@endpush
