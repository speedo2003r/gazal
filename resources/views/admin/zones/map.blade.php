@extends('admin.layout.master')
@section('content')


@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.zones.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.GeographicalRange')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">
                {{ __('admin.GeographicalAreaMap') }}
                        -
                        {{ $zone->city->title }}
                        -
                        {{ $zone->title }}</span></a>
    </div>
@endsection
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12" style="max-width:100%">
                    <form action="{{ route('admin.zones.update.map') }}" id=" editMapForm"
                          method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="map" id="polygon_map" style="height: 500px">
                                        </div>
                                        <input type="hidden" name="id" value="{{ $zone->id }}">
                                        <input type="" id="geometry" class="geometry"
                                               value="{{ $zone->geometry ?? '' }}" name="geometry"
                                               style="width:100%" readonly />
                                        <div style="text-align:center">
                                            <input class="clear_shapes" onclick="clearShapes()"
                                                   value="{{ __('admin.deleteShape') }}" type="button" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-primary">{{ __('save') }}</button>
                            <a type="button" class="btn btn-default"
                               href={{ route('admin.zones.index') }}>{{ __('close') }}</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('js')
    <script type="text/javascript"
            src="https://maps.google.com/maps/api/js?key={{ settings('map_key') }}&libraries=places,drawing">
    </script>

    <script type="text/javascript">
      var Latitude = 24.774265;
      var Longitude = 46.738586;
      var Zoom = 10;
      var drawingManager;
      var selectedShape;
      let polyOptions = {
        // fillColor: '#0099FF',
        // fillOpacity: 0.7,
        // strokeColor: '#AA2143',
        // strokeWeight: 2,
        editable: true
      };

      function clearSelection() {
        if (selectedShape) {
          selectedShape.set('editable', false);
          selectedShape = null;
        }
      }

      function setSelection(shape) {
        console.log('set selection')
        clearSelection();
        selectedShape = shape;
        selectedShape.set('editable', true);
      }

      function clearShapes() {
        if (selectedShape) {
          $('#geometry').val('');
          selectedShape.setMap(null);
        }
      }

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);

        function showPosition(position) {
          Latitude = position.coords.latitude;
          Longitude = position.coords.longitude;
          console.log('current ', Latitude, Longitude);
        }
      }

      function initialize() {
        console.log('initialize map')

        let map = new google.maps.Map(document.getElementById('polygon_map'), {
          zoom: Zoom,
          center: new google.maps.LatLng(Latitude, Longitude),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true,
          zoomControl: true
        });

        let drowNewShap = function(map) {
          return new google.maps.drawing.DrawingManager({
            drawingControlOptions: {
              drawingModes: [
                google.maps.drawing.OverlayType.POLYGON
              ]
            },
            polygonOptions: polyOptions,
            map: map
          });
        }

        drawingManager = drowNewShap(map);

        let dataLayer = new google.maps.Data();

        let saveData = function(path) {
          // to use stringify need datalayer
          dataLayer.forEach(function(f) {
            dataLayer.remove(f);
          });

          dataLayer.add(new google.maps.Data.Feature({
            geometry: new google.maps.Data.Polygon([path.getArray()])
          }));

          dataLayer.toGeoJson(function(obj) {
            let geometry = obj.features[0].geometry;
            console.log('data to save ', geometry)
            let dataa = JSON.stringify(geometry);
            $('#geometry').val(dataa);
          });
        }

        let listenTo = google.maps.event.addListener;

        let listeners = function(theShap) {
          listenTo(theShap.getPath(), 'set_at', function(e) {
            console.log('set_at');
            setSelection(theShap);
            saveData(theShap.getPath());
          });
          listenTo(theShap.getPath(), 'insert_at', function(e) {
            console.log('insert_at');
            setSelection(theShap);
            saveData(theShap.getPath());
          });
        }

        listenTo(drawingManager, 'overlaycomplete', function(e) {
          console.log('overlaycomplete new shap');
          clearShapes();
          let shape = e.overlay;
          setSelection(shape);
          saveData(shape.getPath());
          listeners(selectedShape);
        });

        google.maps.Polygon.prototype.my_getBounds = function() {
          var bounds = new google.maps.LatLngBounds()
          this.getPath().forEach(function(element, index) {
            bounds.extend(element)
          })
          return bounds
        }

        let zoneOldGeometry = $('#geometry').val();
        if (zoneOldGeometry) {
          console.log('zoneOldGeometry', zoneOldGeometry)
          let paths = JSON.parse(zoneOldGeometry);
          if (paths.type == "Polygon") {
            console.log('has old shape: ', paths)
            clearShapes();

            let old_coor = paths.coordinates[0];
            let to_coor = old_coor.map(function(point) {
              return new google.maps.LatLng(point[1], point[0]);
            })

            let oldShape = new google.maps.Polygon({
              editable: true,
              paths: to_coor,
            });

            var center = oldShape.my_getBounds().getCenter();
            //console.log('old polygon center ',center);
            map.setCenter(center)
            setSelection(oldShape);
            oldShape.setMap(map);
            listeners(selectedShape);
          }
        }

        let cityOtherZonesGeometryArr = @json($cityOtherZones);
        if (cityOtherZonesGeometryArr.length > 0) {

          for (var i = 0; i < cityOtherZonesGeometryArr.length; i++) {
            let otherZoneObj = JSON.parse(cityOtherZonesGeometryArr[i]);
            console.log('otherZoneObj', otherZoneObj)
            if (otherZoneObj != null && otherZoneObj.type == "Polygon") {
              let old_coor = otherZoneObj.coordinates[0];
              let to_coor = old_coor.map(function(point) {
                return new google.maps.LatLng(point[1], point[0]);
              })

              let oldShape = new google.maps.Polygon({
                editable: false,
                paths: to_coor,
                fillColor: '#E5AA97',
                fillOpacity: 0.5,
                strokeColor: '#D15318',
                strokeWeight: 1,
                hover: 'hover',
                title: 'title',
                lable: 'lable',
                content: 'content',
              });
              oldShape.setMap(map);
            }
          }
        }

        //listenTo(map, 'click', clearSelection);
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endpush
