@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.roles.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.powers')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('add_role')}}</span></a>
    </div>
@endsection
    <section class="content-header row">

        <div class="card">
            <form action="{{route('admin.roles.store')}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_ar')}}</label>
                                <input type="text" name="name[ar]" class="form-control" placeholder="{{__('enter')}} ..." required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_en')}}</label>
                                <input type="text" name="name[en]" class="form-control" placeholder="{{__('enter')}} ..." required>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-detached sidebar-left">
                        <div class="row">
                        {{addRole()}}
                        </div>
                    </div>
                    <div class="m-5">
                        <input type="submit" value="{{__('save')}}" class="btn btn-success form-control" >
                    </div>
                </div>
            </form>
        </div>
    </section>


@endsection

@push('js')
    <script>
            $(".check-all").on("change" , function(){
            if($(this).is(':checked') )
            $(this).parents(".roll-checkk").find(".checkk").attr("checked" , true);
            else
            $(this).parents(".roll-checkk").find(".checkk").attr("checked" , false);
        })
    </script>
@endpush

