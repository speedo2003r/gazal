@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('roles')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <a href="{{route('admin.roles.create')}}" class="btn btn-primary btn-wide  waves-effect waves-light"><i class="fas fa-plus"></i> {{__('add_role')}}</a>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <table id="datatable" class="table table-striped table-bordered dataTable no-footer"  style="width:100%">
                        <thead>
                        <tr>
                            <th>
                                <label class="custom-control material-checkbox" style="margin: auto">
                                    <input type="checkbox" class="material-control-input" id="checkedAll">
                                    <span class="material-control-indicator"></span>
                                </label>
                            </th>
                            <th>{{__('name')}}</th>
                            <th>{{__('control')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $ob)
                            <tr>
                                <td>
                                    <label class="custom-control material-checkbox" style="margin: auto">
                                        <input type="checkbox" class="material-control-input checkSingle" id="{{$ob->id}}">
                                        <span class="material-control-indicator"></span>
                                    </label>
                                </td>
                                <td>{{$ob['title']}}</td>
                                <td>
                                    <a href="javascript:void(0)" onclick="confirmDelete('{{route('admin.roles.delete',$ob->id)}}')" data-toggle="modal" data-target="#delete-model">
                                        <i class="feather icon-trash"></i>
                                    </a>
                                    <a href="{{route('admin.roles.edit',$ob->id)}}"><i class="feather icon-edit"></i></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        @if(count($roles) > 0)
                            <tr>
                                <td colspan="30">
                                    <button class="btn btn-danger confirmDel" disabled onclick="deleteAllData('more','{{route('admin.roles.delete',$ob->id)}}')" data-toggle="modal" data-target="#confirm-all-del">
                                        <i class="fas fa-trash"></i>
                                        {{awtTrans('deleteSelected')}}
                                    </button>
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
