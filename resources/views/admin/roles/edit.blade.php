@extends('admin.layout.master')
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.roles.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.powers')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('edit_role')}}</span></a>
    </div>
@endsection
    <section class="content-header row">

        <div class="card">
            <form action="{{route('admin.roles.update',$role->id)}}" method="post">
                @method('put')
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_ar')}}</label>
                                <input type="text" name="name[ar]" class="form-control" value="{{$role->getTranslations()['title']['ar']}}" placeholder="{{__('enter')}} ..." >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_en')}}</label>
                                <input type="text" name="name[en]" class="form-control" value="{{$role->getTranslations()['title']['en']}}" placeholder="{{__('enter')}} ..." >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{editRole($role->id)}}
                    </div>
                    <div class="m-5">
                        <button type="submit" class="btn btn-success form-control" >{{__('save')}}</button>
                    </div>
                </div>
            </form>
        </div>
</section>


@endsection

@push('js')
    <script>
        $(".check-all").on("change" , function(){
            if($(this).is(':checked') )
                $(this).parents(".roll-checkk").find(".checkk").prop("checked", true);
            else
                $(this).parents(".roll-checkk").find(".checkk").prop("checked", false);
        })
    </script>
@endpush

