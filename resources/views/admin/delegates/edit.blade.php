@extends('admin.layout.master')
@section('title',__('admin.editDelegate'))
@section('content')


@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.delegates.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.delegates')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.editDelegate')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin.delegates.update',$delegate['id'])}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">

                                        <div class = "col-sm-12 text-center">
                                            <label class = "mb-0">{{__('avatar')}}</label>
                                            <div class = "text-center">
                                                <div class = "images-upload-block single-image">
                                                    <label class = "upload-img">
                                                        <input type = "file" name = "image" id = "image" accept = "image/*" class = "image-uploader" >
                                                        <i class="fas fa-cloud-upload-alt"></i>
                                                    </label>
                                                    <div class = "upload-area" id="upload_area_img">
                                                        <div class="uploaded-block" data-count-order="0">
                                                            <a href="{{$delegate['avatar']}}" data-fancybox="" data-caption="{{$delegate['avatar']}}">
                                                                <img src="{{$delegate['avatar']}}">
                                                            </a>
                                                            <button class="close" type="button">×</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>{{__('name')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="text" name="name" value="{{$delegate['name']}}" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('phone')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="number" name="phone" value="{{$delegate['phone']}}" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('email')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="email" name="email" value="{{$delegate['email']}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('birth_date')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="date" max="{{\Carbon\Carbon::now()->subYears(18)->format('Y-m-d')}}" name="birth_date" value="{{$delegate['birth_date']}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.carType')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="car_type_id" id="car_type_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($carTypes as $carType)
                                                        <option value="{{$carType['id']}}" @if($delegate['car_type_id'] == $carType['id']) selected @endif>{{$carType['title']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('password')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="password" name="password" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Password Confirmation')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="password" name="password_confirmation" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('admin.operatingCompany')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="company_id" id="company_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($companies as $company)
                                                        <option value="{{$company['id']}}" @if(old('company_id') == $company['id']) selected @endif>{{$company->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('Country')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="country_id" id="country_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country['id']}}" @if($delegate['country_id'] == $country['id']) selected @endif>{{$country->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('City')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="city_id" id="city" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <div class="form-group">
                                                <label>{{__('Address')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="hidden" name="lat" id="lat" value="{{$delegate['lat']}}">
                                                <input type="hidden" name="lng" id="lng" value="{{$delegate['lng']}}">
                                                <input type="text" name="address" id="address" value="{{$delegate['address']}}" class="form-control">
                                            </div>
                                            <div id="map" style="height: 300px"></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('admin.maximumDebtLimit')}}</label>
                                                <input type="number" name="max_dept" id="max_dept" value="{{old('max_dept')}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('admin.commissionValuePercentage')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <div class="radio-group">
                                                    <div class="row text-center">
                                                        <div class="form-group col-sm-6">
                                                            <label for="seller">
                                                                <input type="radio" name="commission_status" @if($delegate['commission_status'] == 1) checked @endif value="1" class="form-control" id="value">
                                                                {{__('admin.value')}}
                                                            </label>
                                                        </div>
                                                        <div class="form-group col-sm-6">
                                                            <label for="item">
                                                                <input type="radio" name="commission_status" @if($delegate['commission_status'] == 2) checked @endif value="2" class="form-control" id="percentage">
                                                                {{__('admin.percentage')}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>{{__('admin.commission')}}</label>
                                                <input type="number" name="commission" id="commission" value="{{$delegate['commission']}}" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="submit" class="btn btn-primary">{{__('save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    {!! $validator->selector('#editForm') !!}
    <script src="{{ dashboard_url('dashboard/assets/js/map.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ settings('map_key') }}&libraries=places&callback=initMap&lang=ar"
            async defer></script>
    <script>
        $(document).on('change','#country_id',function (){
            var country = $(this).val();
            getCities(country);
        });
        @if($delegate['city_id'] != null)
        getCities(`{{$delegate['country_id']}}`,`{{$delegate['city_id']}}`)
        @endif
    </script>

@endpush
