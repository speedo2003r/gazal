@extends('admin.layout.master')
@section('content')

    @push('css')
        <style>
            :root {
                --star-size: 15px;
                --star-color: #ccc;
                --star-background: #333;
            }
            .Stars {
                --percent: calc(var(--rating) / 5 * 100%);
                display: inline-block;
                font-size: var(--star-size);
                font-family: Times;
                line-height: 1;
            }
            .Stars::before {
                content: "★★★★★";
                letter-spacing: 3px;
                background: linear-gradient(90deg, var(--star-background) var(--percent), var(--star-color) var(--percent));
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
            }
        </style>

    @endpush



@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.delegatesRating')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">{{__('admin.SortByRating')}}</label>
                <select name="rate" class="form-control" id="rate">
                    <option value="" selected hidden>{{__('Choose')}}</option>
                    <option value="1">{{__('admin.topRating')}}</option>
                    <option value="2">{{__('admin.lowRating')}}</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">{{__('Country')}}</label>
                <select name="country_id" class="form-control" id="country_id">
                    <option value="" selected hidden>{{__('Choose')}}</option>
                    @foreach($countries as $country)
                        <option value="{{$country['id']}}">{{$country['title']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">{{__('City')}}</label>
                <select name="city_id" id="city_id" class="form-control">
                    <option value="" selected hidden>{{__('Choose')}}</option>
                </select>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div id="datatable_wrapper"
                     class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <table
                        class="table table-striped table-bordered dt-responsive nowrap"
                        style="width:100%">
                        <thead>
                            <tr>
                                <th>{{__('name')}}</th>
                                <th>{{__('phone')}}</th>
                                <th>{{__('rating')}}</th>
                            </tr>
                        </thead>
                </table>
                </div>
            </div>
        </div>
    </div>




@endsection
@push('js')
    <script>
        $('body').on('change','#country_id',function (){
            var id = $('#country_id').val();
            getCities(id);
        });

        var rate = '';
        var city_id = '';
        $('body').on('change','#rate',function (){
            rate = $(this).val();
            oTable.draw();
        });
        $('body').on('change','#city_id',function (){
            city_id = $(this).val();
            oTable.draw();
        });
        $('.table').dataTable().fnDestroy();
        var oTable = $('.table').DataTable({
            dom: 'Blfrtip',
            bDestroy: true,
            searching: false,
            pageLength: 10,
            processing: true,
            serverSide: true,
            ajax: {
                url: `{{route('admin.delegates.getFilterData')}}`,
                data: function (d) {
                    d.rate = rate;
                    d.city_id = city_id;
                }
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'phone', name: 'phone'},
                {data: 'rate'},
            ],
            lengthMenu :[
                [10,25,50,100,-1],[10,25,50,100,`{{__('view_all')}}`]
            ],
            buttons: [
                {
                    extend: 'excel',
                    text: `{{__('excel_file')}}`,
                    className: "btn btn-success"

                },
                {
                    extend: 'copy',
                    text: `{{__('copy')}}`,
                    className: "btn btn-inverse"
                },
                {
                    extend: 'print',
                    text: `{{__('print')}}`,
                    className: "btn btn-success"
                },
            ],

            "language": {
                "sEmptyTable": `{{ __('NoDataInTable') }}`,
                "sLoadingRecords": `{{ __('sLoading') }}`,
                "sProcessing": `{{ __('sLoading') }}`,
                "sLengthMenu": '{{ __('showIn') }} _MENU_ {{ __('input') }}',
                "sZeroRecords": `{{ __('noDataFind') }}`,
                "sInfo": "{{ __('showIn') }} _START_ {{ __('to') }} _END_ {{ __('outOf') }} _TOTAL_ {{ __('entry') }}",
                "sInfoEmpty": `{{ __('DisplaysOutRecords') }}`,
                "sInfoFiltered": "({{ __('selectedFromTotal') }} _MAX_ {{ __('entry') }})",
                "sInfoPostFix": "",
                "sSearch": `{{ __('search') }}:`,
                "sUrl": "",
                "oPaginate": {
                    "sFirst": `{{ __('first') }}`,
                    "sPrevious": `{{ __('previous') }}`,
                    "sNext": `{{ __('next') }}`,
                    "sLast": `{{ __('last') }}`
                },
                "oAria": {
                    "sSortAscending": `{{ __(' EnableAscendingOrder') }}:`,
                    "sSortDescending": `{{ __(': EnableDescendingOrder') }}`
                }
            }
        });
    </script>
@endpush
