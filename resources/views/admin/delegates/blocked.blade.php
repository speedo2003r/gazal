@extends('admin.layout.master')
@section('content')

    @push('css')
        <style>
            :root {
                --star-size: 15px;
                --star-color: #ccc;
                --star-background: #333;
            }
            .Stars {
                --percent: calc(var(--rating) / 5 * 100%);
                display: inline-block;
                font-size: var(--star-size);
                font-family: Times;
                line-height: 1;
            }
            .Stars::before {
                content: "★★★★★";
                letter-spacing: 3px;
                background: linear-gradient(90deg, var(--star-background) var(--percent), var(--star-color) var(--percent));
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
            }
        </style>

    @endpush


@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.suspendedAccounts')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    {!! $dataTable->table([
                     'class' => "table table-striped table-bordered dt-responsive nowrap",
                     'id' => "delegatedatatable-table",
                     ],true) !!}
                </div>
            </div>
        </div>
    </div>




@endsection
@push('js')
    {!! $dataTable->scripts() !!}
@endpush
