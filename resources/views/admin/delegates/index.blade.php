@extends('admin.layout.master')
@section('content')


@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.delegates')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <a href="{{route('admin.delegates.create')}}" class="btn btn-primary btn-wide waves-effect waves-light">
                    <i class="fas fa-plus"></i> {{__('admin.addDelegate')}}
                </a>
                <button class="btn btn-warning btn-wide waves-effect waves-light all" onclick="sendNotify('all' , '0')" data-toggle="modal" data-target="#send-noti">
                    <i class="fas fa-paper-plane"></i>{{__('sendNotifyForAll')}}
                </button>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                {!! $dataTable->table([
                 'class' => "table table-striped table-bordered dt-responsive nowrap",
                 'id' => "delegatedatatable-table",
                 ],true) !!}
                </div>
            </div>
        </div>
    </div>




    <!-- send-noti modal-->
    <div class="modal fade" id="send-noti"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.sendNotify')}}</h5>
                </div>
                <div class="modal-body">
                    <form action="" id="sendnotifyuserForm" method="POST">
                        @csrf
                        <input type="hidden" name="type" id="type" value="delegate">
                        <input type="hidden" name="notify_type" id="notify_type" value="delegate">
                        <input type="hidden" name="id" id="notify_id">

                        <div class="form-group">
                            <label>{{__('admin.titleMessageInArabic')}}</label>
                            <input type="text" name="title_ar" id="notifyTitle_ar" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>{{__('admin.titleMessageInEnglish')}}</label>
                            <input type="text" name="title_en" id="notifyTitle_en" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">
                                {{__('admin.MessageInArabic')}}
                            </label>
                            <textarea name="message_ar" id="notifyMessage_ar" cols="30" rows="4" class="form-control"
                                      placeholder="{{__('writeMessage')}}"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">
                                {{__('admin.MessageInEnglish')}}
                            </label>
                            <textarea name="message_en" id="notifyMessage_en" cols="30" rows="4" class="form-control"
                                      placeholder="{{__('writeMessage')}}"></textarea>
                        </div>

                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-sm btn-success save" onclick="sendnotifyuser()">إرسال</button>
                            <button type="button" class="btn btn-default" id="notifyClose" data-dismiss="modal">اغلاق</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end send-noti modal-->

@endsection
@push('js')
    {!! $dataTable->scripts() !!}

    <script src="{{dashboard_url('dashboard/js/map.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{settings('map_key')}}&libraries=places&callback=initMap&language=ar"
        async defer></script>
    <script>


        function sendnotifyuser() {
            event.preventDefault();
            $.ajax({
                type        : 'POST',
                url         : '{{ route('admin.sendnotifyuser') }}' ,
                datatype    : 'json' ,
                async       : false,
                processData : false,
                contentType : false,
                data        : new FormData($("#sendnotifyuserForm")[0]),
                success     : function(msg){
                    if(msg.value == '0'){
                        toastr.error(msg.msg);
                    }else{
                        $('#notifyClose').trigger('click');
                        $('#notifyMessage').html('');
                        toastr.success(msg.msg);
                    }
                }
            });
        }
    </script>
@endpush
