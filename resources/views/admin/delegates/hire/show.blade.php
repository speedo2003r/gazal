@extends('admin.layout.master')
@section('title',__('admin.showHireOrder'))
@section('content')
@push('css')
    <style>
        :root {
            --star-size: 15px;
            --star-color: #ccc;
            --star-background: #333;
        }
        .Stars {
            --percent: calc(var(--rating) / 5 * 100%);
            display: inline-block;
            font-size: var(--star-size);
            font-family: Times;
            line-height: 1;
        }
        .Stars::before {
            content: "★★★★★";
            letter-spacing: 3px;
            background: linear-gradient(90deg, var(--star-background) var(--percent), var(--star-color) var(--percent));
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
    </style>

@endpush

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.delegates.hire') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.hire')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.showHireOrder')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                    <table>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('provider') }}</td>
                                            <td>{{ $hire->provider['store_name'] }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.delegatesCount') }}</td>
                                            <td>{{  $hire->delegates_count }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('from') }}</td>
                                            <td>{{  $hire->from }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('created_at') }}</td>
                                            <td>{{  $hire->created_at->format('Y-m-d h:i a') }}</td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="col-12 col-md-12 col-lg-5">
                                    <table class="ml-0 ml-sm-0 ml-lg-0">
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.carType') }}</td>
                                            <td>{{ $hire->carType['title'] }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.price') }}</td>
                                            <td>{{  $hire->amount }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('to') }}</td>
                                            <td>{{  $hire->to }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @if(count($hire->delegates) > 0)
                <!-- account end -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        {{ __('admin.delegates') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('name')}}</th>
                                                <th>{{__('phone')}}</th>
                                                <th>{{__('email')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($hire->delegates as $ob)
                                                <tr>
                                                    <td>{{$ob->name}}</td>
                                                    <td>{{$ob->phone}}</td>
                                                    <td>{{$ob->email}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </section>
@endsection
