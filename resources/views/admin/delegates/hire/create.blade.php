@extends('admin.layout.master')
@section('title',__('admin.addHireOrder'))
@section('content')
@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.delegates.hire') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.hire')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.addHireOrder')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin.delegates.hire.store')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.provider')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="provider_id" id="provider_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($providers as $provider)
                                                        <option value="{{$provider['id']}}" @if(old('provider_id') == $provider['id']) selected @endif>{{$provider['store_name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.carType')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="car_type_id" id="car_type_id" class="form-control">
                                                    <option value="" hidden selected>{{__('Choose')}}</option>
                                                    @foreach($carTypes as $carType)
                                                        <option value="{{$carType['id']}}" @if(old('car_type_id') == $carType['id']) selected @endif>{{$carType['title']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.delegatesCount')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="number" class="form-control" name="delegates_count">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('admin.price')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="number" class="form-control" name="amount">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('from')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="date" class="form-control" name="from">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('to')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <input type="date" class="form-control" name="to">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>{{__('admin.delegates')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                                <select name="delegate_id[]" id="delegates" class="form-control select2" multiple>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="submit" class="btn btn-primary">{{__('save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Admin\Hire\Create') !!}

    <script>
        $(document).ready(function() {
            $('#delegates').select2({
                placeholder: `{{__('Choose')}}`
            });
        });
        $('body').on('change','#car_type_id',function (){
            var id = $(this).val();
            getDelegates(id);
        });
        function getDelegates(car_type_id, placeholder = 'اختر') {
            var html = '';
            $('#delegates').empty();
            if (car_type_id) {
                $.ajax({
                    url: `{{ route('admin.ajax.getDelegatesCarType') }}`,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        car_type_id: car_type_id
                    },
                    success: function(res) {
                        $.each(res, function(index, value) {
                            html +=
                                `<option value="${value.id}">${value.name}</option>`;
                        });
                        $('#delegates').append(html);
                    }
                });
            }
        }
    </script>
@endpush
