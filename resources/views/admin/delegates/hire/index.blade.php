@extends('admin.layout.master')
@section('title',__('admin.hire'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.hire')}}</span></a>
    </div>
@endsection
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <a href="{{route('admin.delegates.hire.create')}}" class="btn btn-primary btn-wide waves-effect waves-light add-user">
                <i class="fas fa-plus"></i>{{__('admin.addHireOrder')}}
            </a>
            <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                {!! $dataTable->table([
                 'class' => "table table-striped table-bordered dt-responsive nowrap",
                 'id' => "hiretdatatable-table",
                 ],true) !!}
            </div>
        </div>
    </div>
</div>

@endsection
@push('js')
    {!! $dataTable->scripts() !!}
@endpush
