@extends('admin.layout.master')
@section('content')


@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.imagesFilesDelegate')}}</span></a>
    </div>
@endsection

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box card page-body p-5">
                <div class="container">
                   <form action="{{route('admin.delegates.storeImages',$delegate['id'])}}" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label class="col-lg-3 control-label">{{__('admin.licenseImage')}}</label>
                    <div class="col-lg-9">
                        <div class = "images-upload-block single-image">
                            <label class = "upload-img">
                                <input type = "file" name = "_licence_image" id = "licence_image" accept = "image/*" class = "image-uploader">
                                <i class="fas fa-cloud-upload-alt"></i>
                            </label>
                            <div class = "upload-area">
                                <div class="uploaded-block" data-count-order="1">
                                    <a href="{{$delegate->delegate->licence_image}}" data-fancybox data-caption="{{$delegate->delegate->licence_image}}"><img src="{{$delegate->delegate->licence_image}}"></a>
                                    <button class="close">&times;</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label">{{__('admin.idImage')}}</label>
                    <div class="col-lg-9">
                        <div class = "images-upload-block single-image">
                            <label class = "upload-img">
                                <input type = "file" name = "_id_avatar" id = "id_avatar" accept = "image/*" class = "image-uploader">
                                <i class="fas fa-cloud-upload-alt"></i>
                            </label>
                            <div class = "upload-area">
                                <div class="uploaded-block" data-count-order="1">
                                    <a href="{{$delegate->delegate->id_avatar}}" data-fancybox data-caption="{{$delegate->delegate->id_avatar}}"><img src="{{$delegate->delegate->id_avatar}}"></a>
                                    <button class="close">&times;</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label">{{__('admin.carLicense')}}</label>
                    <div class="col-lg-9">
                        <div class = "images-upload-block single-image">
                            <label class = "upload-img">
                                <input type = "file" name = "_licence_car_image" id = "licence_car_image" accept = "image/*" class = "image-uploader">
                                <i class="fas fa-cloud-upload-alt"></i>
                            </label>
                            <div class = "upload-area">
                                <div class="uploaded-block" data-count-order="1">
                                    <a href="{{$delegate->delegate->licence_car_image}}" data-fancybox data-caption="{{$delegate->delegate->licence_car_image}}"><img src="{{$delegate->delegate->car_form_image}}"></a>
                                    <button class="close">&times;</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label">{{__('admin.frontImageVehicle')}}</label>
                    <div class="col-lg-9">
                        <div class = "images-upload-block single-image">
                            <label class = "upload-img">
                                <input type = "file" name = "_car_front_image" id = "car_front_image" accept = "image/*" class = "image-uploader">
                                <i class="fas fa-cloud-upload-alt"></i>
                            </label>
                            <div class = "upload-area">
                                <div class="uploaded-block" data-count-order="1">
                                    <a href="{{$delegate->delegate->car_front_image}}" data-fancybox data-caption="{{$delegate->delegate->car_front_image}}"><img src="{{$delegate->delegate->car_front_image}}"></a>
                                    <button class="close">&times;</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label">{{__('admin.backImageVehicle')}}</label>
                    <div class="col-lg-9">
                        <div class = "images-upload-block single-image">
                            <label class = "upload-img">
                                <input type = "file" name = "_car_back_image" id = "car_back_image" accept = "image/*" class = "image-uploader">
                                <i class="fas fa-cloud-upload-alt"></i>
                            </label>
                            <div class = "upload-area">
                                <div class="uploaded-block" data-count-order="1">
                                    <a href="{{$delegate->delegate->car_back_image}}" data-fancybox data-caption="{{$delegate->delegate->car_back_image}}"><img src="{{$delegate->delegate->car_back_image}}"></a>
                                    <button class="close">&times;</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">{{__('save')}}</button>
            </form>
                </div>
            </div>
        </div>
    </div>



@endsection
@push('js')
    <script>

    </script>
@endpush
