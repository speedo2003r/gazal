@extends('admin.layout.master')
@section('title',__('admin.showDelegate'))
@section('content')
@push('css')
    <style>
        :root {
            --star-size: 15px;
            --star-color: #ccc;
            --star-background: #333;
        }
        .Stars {
            --percent: calc(var(--rating) / 5 * 100%);
            display: inline-block;
            font-size: var(--star-size);
            font-family: Times;
            line-height: 1;
        }
        .Stars::before {
            content: "★★★★★";
            letter-spacing: 3px;
            background: linear-gradient(90deg, var(--star-background) var(--percent), var(--star-color) var(--percent));
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
    </style>

@endpush

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.delegates.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.delegates')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.showDelegate')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="users-view-image">
                                    <img src="{{ $delegate->avatar }}" class="users-avatar-shadow w-100 rounded mb-2 pr-2 ml-1" alt="avatar">
                                </div>
                                <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                    <table>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('name') }}</td>
                                            <td>{{ $delegate->name }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('email') }}</td>
                                            <td>{{  $delegate->email }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('phone') }}</td>
                                            <td>{{  $delegate->phone }}</td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="col-12 col-md-12 col-lg-5">
                                    <table class="ml-0 ml-sm-0 ml-lg-0">
                                        <tr>
                                            <td class="font-weight-bold">{{ __('birth_date') }}</td>
                                            <td>{{ $delegate->birth_date }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('JoinDatetime') }}</td>
                                            <td>{{  date('Y-m-d h:i a',strtotime($delegate->created_at)) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.walletBalance') }}</td>
                                            <td>{{  $delegate->wallet }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">{{ __('admin.indebtedness') }}</td>
                                            <td>{{  $delegate->balance }}</td>
                                        </tr>



                                    </table>
                                </div>
                                <div class="col-12">
{{--                                    <a href="{{ route('admin.countries.edit', $country) }}" class="btn btn-primary mr-1"><i class="feather icon-edit-1"></i> تعديل</a>--}}
                                    @if($delegate->banned == 0)
                                    <a class="btn btn-outline-danger action-block" data-toggle="modal" data-target="#blockModel"><i class="fa fa-ban"></i>
                                        {{__('ban')}}</a>
                                    @else
                                        <a class="btn btn-outline-success action-block" data-toggle="modal" data-target="#unblockModel"><i class="fa fa-check"></i>
                                            {{__('unban')}}</a>
                                    @endif
                                    <a class="btn btn-outline-success action-compensation" data-toggle="modal" data-target="#walletModel"><i class="fa fa-wallet"></i>
                                        {{__('admin.addWallet')}}</a>
                                    <a href="#" class="btn btn-outline-primary single" title="{{__('إرسال إشعار')}}" onclick="sendNotify('one' , '{{ $delegate['id'] }}')" data-toggle="modal" data-target="#send-noti">
                                        <i class="feather icon-send"></i>{{__('admin.SendNotify')}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(count($delegate->ratings) > 0)
                <!-- account end -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                        {{ __('admin.reviewsDelegate') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('admin.orderNumber')}}</th>
                                                <th>{{__('admin.delegateName')}}</th>
                                                <th>{{__('rating')}}</th>
                                                <th>{{__('created_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($delegate->ratings->groupBy('order_id') as $key => $ob)
                                                <tr>
                                                    <td>{{\App\Entities\Order::find($key)['order_name']}}</td>
                                                    <td>{{$ob->first()->user['name']}}</td>
                                                    <td>
                                                        @foreach($ob as $data)
                                                            {{$data->ratingList['title']}} : <div class="Stars" style="--rating: {{$data['rate']}};">
                                                        @endforeach
                                                    </td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->first()->user['created_at']))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(count($delegate->blockUsers) > 0)
                <!-- account end -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                        {{ __('admin.ReasonsForBan') }}
                                        <table class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('reason')}}</th>
                                                <th>{{__('created_at')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($delegate->blockUsers as $ob)
                                                <tr>
                                                    <td>{{$ob->notes}}</td>
                                                    <td>{{date('Y-m-d h:i a' , strtotime($ob->created_at))}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </section>
        <div class="modal fade" id="blockModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('admin.banClient')}}</h4></div>
                    <form action="{{route('admin.delegates.block')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$delegate['id']}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">السبب<span style="color:rgb(145, 4, 4)">*</span></label>
                                <textarea name="notes" class="form-control" rows="6"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- send-noti modal-->
        <div class="modal fade" id="send-noti"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('admin.sendNotify')}}</h5>
                    </div>
                    <div class="modal-body">
                        <form action="" id="sendnotifyuserForm" method="POST">
                            @csrf
                            <input type="hidden" name="type" value="delegate">
                            <input type="hidden" name="notify_type" id="notify_type" value="delegate">
                            <input type="hidden" name="id" id="notify_id">

                            <div class="form-group">
                                <label>{{__('admin.titleMessageInArabic')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <input type="text" name="title_ar" id="notifyTitle_ar" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>{{__('admin.titleMessageInEnglish')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                <input type="text" name="title_en" id="notifyTitle_en" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">
                                    {{__('admin.MessageInArabic')}}<span style="color:rgb(145, 4, 4)">*</span>
                                </label>
                                <textarea name="message_ar" id="notifyMessage_ar" cols="30" rows="4" class="form-control"
                                          placeholder="{{__('writeMessage')}}"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">
                                    {{__('admin.MessageInEnglish')}}<span style="color:rgb(145, 4, 4)">*</span>
                                </label>
                                <textarea name="message_en" id="notifyMessage_en" cols="30" rows="4" class="form-control"
                                          placeholder="{{__('writeMessage')}}"></textarea>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <button type="submit" class="btn btn-sm btn-success save" onclick="sendnotifyuser()">إرسال</button>
                                <button type="button" class="btn btn-default" id="notifyClose" data-dismiss="modal">اغلاق</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end send-noti modal-->
        <div class="modal fade" id="unblockModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('admin.unbanClient')}}</h4></div>
                    <form action="{{route('admin.delegates.unblock')}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$delegate['id']}}">
                        <div class="modal-body">
                            <div class="form-group">
                                <h3>{{__('admin.AreSureUnblockClient')}}</h3>
                            </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="walletModel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">{{__('admin.AddClientWallet')}}</h4></div>
                    <form action="{{route('admin.delegates.addToWallet')}}" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$delegate['id']}}">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="">{{__('admin.balance')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <input type="number" name="amount" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="submit" class="btn btn-primary">{{__('send')}}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('close')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
@push('js')
    <script>
        function sendnotifyuser() {
            event.preventDefault();
            $.ajax({
                type        : 'POST',
                url         : '{{ route('admin.sendnotifyuser') }}' ,
                datatype    : 'json' ,
                async       : false,
                processData : false,
                contentType : false,
                data        : new FormData($("#sendnotifyuserForm")[0]),
                success     : function(msg){
                    if(msg.value == '0'){
                        toastr.error(msg.msg);
                    }else{
                        $('#notifyClose').trigger('click');
                        $('#notifyMessage').html('');
                        toastr.success(msg.msg);
                    }
                }
            });
        }
    </script>
@endpush
