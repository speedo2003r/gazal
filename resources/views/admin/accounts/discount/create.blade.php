@extends('admin.layout.master')
@section('title',__('admin.addDiscountAmount'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.accounts.discountAmount',$provider['id']) }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.discountAmount')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.addDiscountAmount')}}</span></a>
    </div>
@endsection
<div class="content-body">
    <!-- page users view start -->
    <section class="page-users-view">
        <div class="row">
            <!-- account start -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <form action="{{route('admin.accounts.discountAmountStore',$provider['id'])}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">


                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>{{__('Amount')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <input type="text" name="amount" id="amount" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>{{__('admin.dateAddAmount')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <input type="date" name="created_date" id="created_date" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>{{__('admin.payMethods')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <select name="pay_type" class="form-control">
                                            <option value="" selected hidden>{{__('Choose')}}</option>
                                            @foreach(\App\Entities\DiscountAmount::methods() as $key => $value)
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>{{__('reason')}}</label>
                                        <select name="discount_reason_id" class="form-control">
                                            <option value="" selected hidden>{{__('Choose')}}</option>
                                            @foreach($discountReasons as $discountReason)
                                                <option value="{{$discountReason['id']}}">{{$discountReason['reason']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <h3>{{__('Payments')}}</h3>
                            <div class="payment">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="">{{__('Amount')}}</label>
                                            <input type="number" name="payment[amount][]" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="">{{__('Date')}}</label>
                                            <input type="date" name="payment[date][]" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-danger delPayment" style="margin-top: 20px;">{{__('Delete')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <button type="button" class="btn btn-success btn-sm waves-effect waves-light add_payment">{{__('addPayment')}}</button>
                            </div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('js')
    <script>
        $('body').on('click','.add_payment',function (){
            var html = `<div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="">{{__('Amount')}}</label>
                                <input type="number" name="payment[amount][]" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="">{{__('Date')}}</label>
                                <input type="date" name="payment[date][]" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <button type="button" class="btn btn-danger delPayment" style="margin-top: 20px;">{{__('Delete')}}</button>
                            </div>
                        </div>
                    </div>`;
                $('.payment').append(html);
        });
        $('body').on('click','.delPayment',function (){
            $(this).parent().parent().parent().remove();
        });
    </script>

@endpush
