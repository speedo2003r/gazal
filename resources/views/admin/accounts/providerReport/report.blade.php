@extends('admin.layout.master')
@section('title',__('admin.providerReport'))
@section('content')
@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.providerReport')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">{{__('From')}}</label>
                <input type="date" class="form-control" name="from">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">{{__('To')}}</label>
                <input type="date" class="form-control" name="to">
            </div>
        </div>
    </div>


@endsection
