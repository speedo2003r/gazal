@extends('admin.layout.master')
@section('title',__('admin.addDiscountAmount'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.addTax')}}</span></a>
    </div>
@endsection
<div class="content-body">
    <!-- page users view start -->
    <section class="page-users-view">
        <div class="row">
            <!-- account start -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <form action="{{route('admin.settings.update')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>{{__('tax')}}<span style="color:rgb(145, 4, 4)">*</span></label>
                                        <input type="text" name="keys[tax]" value="{{settings('tax')}}" class="form-control">
                                    </div>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
