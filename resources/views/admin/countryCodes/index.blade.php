@extends('admin.layout.master')
@section('title',__('admin.countriesCodes'))
@section('content')


@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.countriesCodes')}}</span></a>
    </div>
@endsection

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                    <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>{{__('name')}}</th>
                        <th>{{__('admin.code')}}</th>
                        <th>{{__('admin.motherLang')}}</th>
                        <th>{{__('admin.example')}}</th>
                        <th>{{__('control')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($countries as $ob)
                        <tr>
                            <td>
                                {{$ob->id}}
                            </td>
                            <td>{{$ob->name}}</td>
                            <td>{{$ob->phone_code}}</td>
                            <td>{{$ob->lang_name}}</td>
                            <td>{{$ob->example}}</td>
                            <td>
                                <a href="javascript:void(0)" onclick="edit({{json_encode($ob, true)}})" data-toggle="modal" data-target="#editModel"><i class="feather icon-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>


<!-- edit model -->
<div class="modal fade" id="editModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">{{__('admin.EditCountry')}}</h4></div>
            <form action=""  id="editForm" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('admin.motherLang')}}</label>
                                <input type="text" id="lang_name" name="lang_name" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('admin.example')}}</label>
                                <input type="number" id="example" name="example" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                    <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">{{__('close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit model -->
@endsection
@push('js')
    <script>
        function edit(ob){
            $('#editForm')      .attr("action","{{route('admin.countryCodes.update','obId')}}".replace('obId',ob.id));
            $('#lang_name')    .val(ob.lang_name);
            $('#example')     .val(ob.example);
        }
    </script>
@endpush
