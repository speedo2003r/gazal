@extends('admin.layout.master')
@section('title',__('admin.ModifyFixedBanner'))
@section('content')

@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ route('admin.banners.index') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('admin.FixedBanner')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('admin.ModifyFixedBanner')}}</span></a>
    </div>
@endsection
    <div class="content-body">
        <!-- page users view start -->
        <section class="page-users-view">
            <div class="row">
                <!-- account start -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin.topLists.update',$topList['id'])}}"  id="editForm" method="post" role="form" enctype="multipart/form-data">
                                @csrf<div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>{{__('providers')}}</label>
                                            <select name="provider_id" id="provider_id" class="form-control js-example-basic-single">
                                                <option value="" hidden selected>{{__('Choose')}}</option>
                                                @foreach($providers as $provider)
                                                    <option value="{{$provider['id']}}" @if($topList['provider_id'] == $provider['id']) selected @endif>{{$provider['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>{{__('sort')}}</label>
                                            <input type="number" name="sort" class="form-control" value="{{$topList['sort']}}">
                                        </div>
                                        <div class="col-sm-12  radio-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>{{__('from')}}</label>
                                                        <input type="date" name="from" class="form-control" id="start_date" value="{{$topList['from']}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>{{__('to')}}</label>
                                                        <input type="date" name="to" class="form-control" id="end_date" value="{{$topList['to']}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{__('save')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('js')
    {!! JsValidator::formRequest('App\Http\Requests\Admin\TopList\Update') !!}

@endpush
