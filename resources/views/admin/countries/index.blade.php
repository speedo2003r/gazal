@extends('admin.layout.master')
@section('title',__('countries'))
@section('content')


@section('breadcrumb')
    <div style="font-size:14px ; font-family:'cairo' ; color:black">
        <a href="{{ url('/') }}"><span class="user-name"> <i class="feather icon-home"></i> {{__('home')}}</span></a>
        <span class="user-name">/</span>
        <a href="javacsript:void(0)"><span class="user-name">{{__('countries')}}</span></a>
    </div>
@endsection
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <button type="button" data-toggle="modal" data-target="#addModel" class="btn btn-primary btn-wide waves-effect waves-light add-user">
                    <i class="fas fa-plus"></i>{{__('admin.AddCountry')}}
                </button>
                <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"  style="width:100%">
                    <thead>
                    <tr>
                        <th>
                            <label class="custom-control material-checkbox" style="margin: auto">
                                <input type="checkbox" class="material-control-input" id="checkedAll">
                                <span class="material-control-indicator"></span>
                            </label>
                        </th>
                        <th>{{__('name')}}</th>
                        <th>{{__('control')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($countries as $ob)
                        <tr>
                            <td>
                                <label class="custom-control material-checkbox" style="margin: auto">
                                    <input type="checkbox" class="material-control-input checkSingle" id="{{$ob->id}}">
                                    <span class="material-control-indicator"></span>
                                </label>
                            </td>
                            <td>{{$ob->title}}</td>
                            <td>
                                <a href="javascript:void(0)" onclick="confirmDelete('{{route('admin.countries.destroy',$ob->id)}}')" data-toggle="modal" data-target="#delete-model">
                                    <i class="feather icon-trash"></i>
                                </a>
                                <a href="javascript:void(0)" onclick="edit({{$ob}})" data-toggle="modal" data-target="#editModel"><i class="feather icon-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    @if(count($countries) > 0)
                    <tr>
                        <td colspan="3">
                            <button class="btn btn-danger confirmDel waves-effect waves-light" disabled onclick="deleteAllData('more','{{route('admin.countries.destroy',$ob->id)}}')" data-toggle="modal" data-target="#confirm-all-del">
                                <i class="fas fa-trash"></i>
                                {{__('deleteSelected')}}
                            </button>
                        </td>
                    </tr>
                    @endif
                </table>
                </div>
            </div>
        </div>
    </div>


 <!-- add model -->
 <div class="modal fade" id="addModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">{{__('اضافة دوله')}}</h4></div>
            <form action="{{route('admin.countries.store')}}" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="uuid" value="uuid">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_ar')}}</label>
                                <input type="text" name="title[ar]" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_en')}}</label>
                                <input type="text" name="title[en]" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn waves-effect waves-light btn-primary">{{__('save')}}</button>
                    <button type="button" class="btn waves-effect waves-light btn-default" data-dismiss="modal">{{__('close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end add model -->

<!-- edit model -->
<div class="modal fade" id="editModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title">{{__('admin.EditCountry')}}</h4></div>
            <form action=""  id="editForm" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_ar')}}</label>
                                <input type="text" id="title[ar]" name="title_ar" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>{{__('title_en')}}</label>
                                <input type="text" id="title[en]" name="title_en" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="submit" class="btn waves-effect waves-light btn-primary">{{__('save')}}</button>
                    <button type="button" class="btn waves-effect waves-light btn-default" data-dismiss="modal">{{__('close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit model -->
@endsection
@push('js')
    <script>
        function edit(ob){
            console.log(ob);
            $('#editForm')      .attr("action","{{route('admin.countries.update','obId')}}".replace('obId',ob.id));
            $('#title_ar')    .val(ob.title.ar);
            $('#title_en')     .val(ob.title.en);
        }
    </script>
@endpush
