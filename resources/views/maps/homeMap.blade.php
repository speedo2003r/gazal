@include('maps.mapKey')
<script type="text/javascript">
  var LatEle = document.getElementById("lat");
  var LngEle = document.getElementById("lng");
  var MapDescEle = document.getElementById("map_desc");
  var MapDescEle2 = document.getElementById("map_desc2");
  var MapSearchEle = document.getElementById("map_search");

  var setLatlng = function(d_lat, d_lng) {
    if (LatEle != null && LngEle != null) {
      document.getElementById("lat").value = d_lat
      document.getElementById("lng").value = d_lng
    }

    return {
      lat: d_lat,
      lng: d_lng
    };
  }

  var Latitude = LatEle != null ? Number(LatEle.value) : '';
  var Longitude = LngEle != null ? Number(LngEle.value) : '';

  var Zoom = 10;
  var latlng;

  if (Latitude == '' || Longitude == '') {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);

      function showPosition(position) {
        Latitude = position.coords.latitude;
        Longitude = position.coords.longitude;
        latlng = setLatlng(Latitude, Longitude);
        console.log('current location ', Latitude, Longitude);
      }
    } else {
      Latitude = 24.774265;
      Longitude = 46.738586;
      latlng = setLatlng(Latitude, Longitude);
      console.log('default location ', Latitude, Longitude);
    }
  } else {
    latlng = setLatlng(Latitude, Longitude);
    console.log('old data location ', Latitude, Longitude);
  }

  var newMap = function(d_id, d_latlng) {
    return new google.maps.Map(document.getElementById(d_id), {
      zoom: Zoom,
      center: d_latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
      zoomControl: true
    });
  }

  var newMarker = function(d_latlng, d_map) {
    return new google.maps.Marker({
      position: d_latlng,
      map: d_map,
      draggable: true,
      animation: google.maps.Animation.BOUNCE,
    });
  }

  // show address
  var geocoder = new google.maps.Geocoder();
  var infowindow = new google.maps.InfoWindow();
  var getGeocoder = function(d_latlng, d_map, d_marker) {
    return geocoder.geocode({
      'latLng': d_latlng
    }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          let cityName = results[0].address_components[1].short_name;
          let formatedAdd = results[0].formatted_address;

          if (MapDescEle != null) MapDescEle.value = cityName;
          if (MapDescEle2 != null) MapDescEle2.value = cityName;
          infowindow.setContent(cityName);

          infowindow.open(d_map, d_marker);
        }
      }
    });
  }

  var markerDragend = function(d_marker) {
    return google.maps.event.addListener(d_marker, 'dragend', function() {
      Latitude = this.getPosition().lat();
      Longitude = this.getPosition().lng();
      latlng = setLatlng(Latitude, Longitude);
      getGeocoder(latlng, map, d_marker);
      console.log('after drag', Latitude, Longitude);
    });
  }

  var mapClick = function(d_map, d_marker) {
    return google.maps.event.addListener(d_map, 'click', function(e) {
      Latitude = e.latLng.lat()
      Longitude = e.latLng.lng()
      latlng = setLatlng(Latitude, Longitude);
      d_marker.setPosition(e.latLng)
      getGeocoder(latlng, d_map, d_marker);
      console.log('after click', Latitude, Longitude);
    });
  }

  var mapSearch = function(search_input, d_map, d_marker) {
    var searchBox = new google.maps.places.SearchBox(search_input);

    return google.maps.event.addListener(searchBox, "places_changed", function() {
      var places = searchBox.getPlaces();
      var bounds = new google.maps.LatLngBounds();
      var i, place;
      for (i = 0;
        (place = places[i]); i++) {
        bounds.extend(place.geometry.location);
        d_marker.setPosition(place.geometry.location);
      }
      d_map.fitBounds(bounds);
      d_map.setZoom(12);
    });
  }

  var markerPositionChanged = function(d_marker) {
    return google.maps.event.addListener(d_marker, 'position_changed', function() {
      Latitude = this.getPosition().lat();
      Longitude = this.getPosition().lng();
      latlng = setLatlng(Latitude, Longitude);
      getGeocoder(latlng, map, d_marker);
      console.log('after position changed', Latitude, Longitude);
    });
  }

  function initialize() {
    console.log('initialize map')
    var map = newMap('map', latlng)
    var marker = newMarker(latlng, map);

    getGeocoder(latlng, map, marker);
    markerDragend(marker);
    mapClick(map, marker);

    closeEnterForSearch();
    mapSearch(MapSearchEle, map, marker);
    markerPositionChanged(marker);
  }

  function closeEnterForSearch() {
    // close enter action and make it for search
    $('html').bind('keypress', function(e) {
      if (e.keyCode == 13) {
        return false;
      }
    });
  }

  google.maps.event.addDomListener(window, 'load', initialize);
</script>
