<script
        src="https://maps.googleapis.com/maps/api/js?key={{ settings('map_key') }}&libraries=places&language={{ lang() }}">
</script>
