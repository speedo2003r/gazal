@extends('front.client.layouts.master')
@section('content')
  <section class="all-forms">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="my-form foot-form">
            <div class="top-logo">
              <img src="{{ client_path() }}/images/icons/surface1.png">
              <h4>{{ awtTrans('كن شريكاً') }}</h4>
            </div>
            <form role="form" method="post" action="{{ route('client.storeBePartner') }}">
              <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('الاسم') }}" name="name">
              </div>
              <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('إسم النشاط') }}" name="store_name">
              </div>
              <div class="form-group">
                <div class="custom-select">
                  <select name="category_id">
                    <option value="">{{ awtTrans('اختر نوع النشاط') }}</option>
                    @foreach ($mainCategories as $category)
                      <option value="{{ $category->id }}">
                        {{ $category->title }}
                      </option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('رقم التواصل') }}" name="phone">
              </div>
              <div class="form-group">
                <input type="email" class="form-control"
                       placeholder="{{ awtTrans('البريد الالكتروني') }}" name="email">
              </div>
              <div class="form-group">
                <div class="custom-select">
                  <select name="city_id">
                    <option value="">{{ awtTrans('اختر المدينة') }}</option>
                    @foreach ($cities as $city)
                      <option value="{{ $city->id }}">{{ $city->title }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="center-align">
                <button class="stand-link btnSubmitForm">
                  <span>{{ awtTrans('ارسال الطلب') }}</span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!--end be-partner-->

@endsection
