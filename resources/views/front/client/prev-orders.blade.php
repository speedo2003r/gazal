@extends('front.client.layouts.master')
@section('content')
  @include('front.client.layouts.parts.section-main-header')

  <section class="prev-orders">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="bradcrumb">
            <ul class="list-unstyled">
              <li><a href="{{ route('client.categories') }}">{{ awtTrans('الرئيسية') }}</a></li>
              <li><i class="fas fa-angle-left"></i></li>
              <li><a href="prev-orders.html">{{ awtTrans('الطلبات السابقة') }}</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="prev-ord-card">
            <div class="prev-ord">
              <div class="media">

                <a href="#" data-toggle="modal" data-target="#order-modal"><img
                       src="{{ client_path() }}/images/prev-orders/Mask Group 5.png"></a>
                <div class="media-body">
                  <h4>بيتزا هت</h4>
                  <p>
                    {{ awtTrans('الطلب') }}
                    :
                    <span>12546546</span>
                  </p>
                  <p>
                    {{ awtTrans('الوقت') }}
                    :
                    <span>2021 / 02 / 25 - 23:11</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="prev-actions">
              <button data-toggle="modal" data-target="#rate-modal" class="btn rate-btn">
                <i class="far fa-grin"></i>
                {{ awtTrans('تقيم') }}
              </button>
              <button data-toggle="modal" data-target="#order-modal" class="btn repeat-btn">
                <i class="fas fa-redo"></i>
                {{ awtTrans('اعادة الطلب') }}
              </button>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="prev-ord-card">
            <div class="prev-ord">
              <div class="media">

                <a href="#" data-toggle="modal" data-target="#order-modal"><img
                       src="{{ client_path() }}/images/prev-orders/Mask Group 5.png"></a>
                <div class="media-body">
                  <h4>بيتزا هت</h4>
                  <p>
                    {{ awtTrans('الطلب') }}
                    :
                    <span>12546546</span>
                  </p>
                  <p>
                    {{ awtTrans('الوقت') }}
                    :
                    <span>2021 / 02 / 25 - 23:11</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="prev-actions">
              <button data-toggle="modal" data-target="#rate-modal" class="btn rate-btn">
                <i class="far fa-grin"></i>
                {{ awtTrans('تقيم') }}
              </button>
              <button data-toggle="modal" data-target="#order-modal" class="btn repeat-btn">
                <i class="fas fa-redo"></i>
                {{ awtTrans('اعادة الطلب') }}
              </button>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="prev-ord-card">
            <div class="prev-ord">
              <div class="media">

                <a href="#" data-toggle="modal" data-target="#order-modal"><img
                       src="{{ client_path() }}/images/prev-orders/Mask Group 5.png"></a>
                <div class="media-body">
                  <h4>بيتزا هت</h4>
                  <p>
                    {{ awtTrans('الطلب') }}
                    :
                    <span>12546546</span>
                  </p>
                  <p>
                    {{ awtTrans('الوقت') }}
                    :
                    <span>2021 / 02 / 25 - 23:11</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="prev-actions">
              <button data-toggle="modal" data-target="#rate-modal" class="btn rate-btn">
                <i class="far fa-grin"></i>
                {{ awtTrans('تقيم') }}
              </button>
              <button data-toggle="modal" data-target="#order-modal" class="btn repeat-btn">
                <i class="fas fa-redo"></i>
                {{ awtTrans('اعادة الطلب') }}
              </button>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="prev-ord-card">
            <div class="prev-ord">
              <div class="media">

                <a href="#" data-toggle="modal" data-target="#order-modal"><img
                       src="{{ client_path() }}/images/prev-orders/Mask Group 5.png"></a>
                <div class="media-body">
                  <h4>بيتزا هت</h4>
                  <p>
                    {{ awtTrans('الطلب') }}
                    :
                    <span>12546546</span>
                  </p>
                  <p>
                    {{ awtTrans('الوقت') }}
                    :
                    <span>2021 / 02 / 25 - 23:11</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="prev-actions">
              <button data-toggle="modal" data-target="#rate-modal" class="btn rate-btn">
                <i class="far fa-grin"></i>
                {{ awtTrans('تقيم') }}
              </button>
              <button data-toggle="modal" data-target="#order-modal" class="btn repeat-btn">
                <i class="fas fa-redo"></i>
                {{ awtTrans('اعادة الطلب') }}
              </button>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="prev-ord-card">
            <div class="prev-ord">
              <div class="media">

                <a href="#" data-toggle="modal" data-target="#order-modal"><img
                       src="{{ client_path() }}/images/prev-orders/Mask Group 5.png"></a>
                <div class="media-body">
                  <h4>بيتزا هت</h4>
                  <p>
                    {{ awtTrans('الطلب') }}
                    :
                    <span>12546546</span>
                  </p>
                  <p>
                    {{ awtTrans('الوقت') }}
                    :
                    <span>2021 / 02 / 25 - 23:11</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="prev-actions">
              <button data-toggle="modal" data-target="#rate-modal" class="btn rate-btn">
                <i class="far fa-grin"></i>
                {{ awtTrans('تقيم') }}
              </button>
              <button data-toggle="modal" data-target="#order-modal" class="btn repeat-btn">
                <i class="fas fa-redo"></i>
                {{ awtTrans('اعادة الطلب') }}
              </button>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="prev-ord-card">
            <div class="prev-ord">
              <div class="media">

                <a href="#" data-toggle="modal" data-target="#order-modal"><img
                       src="{{ client_path() }}/images/prev-orders/Mask Group 5.png"></a>
                <div class="media-body">
                  <h4>بيتزا هت</h4>
                  <p>
                    {{ awtTrans('الطلب') }}
                    :
                    <span>12546546</span>
                  </p>
                  <p>
                    {{ awtTrans('الوقت') }}
                    :
                    <span>2021 / 02 / 25 - 23:11</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="prev-actions">
              <button data-toggle="modal" data-target="#rate-modal" class="btn rate-btn">
                <i class="far fa-grin"></i>
                {{ awtTrans('تقيم') }}
              </button>
              <button data-toggle="modal" data-target="#order-modal" class="btn repeat-btn">
                <i class="fas fa-redo"></i>
                {{ awtTrans('اعادة الطلب') }}
              </button>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="prev-ord-card">
            <div class="prev-ord">
              <div class="media">

                <a href="#" data-toggle="modal" data-target="#order-modal"><img
                       src="{{ client_path() }}/images/prev-orders/Mask Group 5.png"></a>
                <div class="media-body">
                  <h4>بيتزا هت</h4>
                  <p>
                    {{ awtTrans('الطلب') }}
                    :
                    <span>12546546</span>
                  </p>
                  <p>
                    {{ awtTrans('الوقت') }}
                    :
                    <span>2021 / 02 / 25 - 23:11</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="prev-actions">
              <button data-toggle="modal" data-target="#rate-modal" class="btn rate-btn">
                <i class="far fa-grin"></i>
                {{ awtTrans('تقيم') }}
              </button>
              <button data-toggle="modal" data-target="#order-modal" class="btn repeat-btn">
                <i class="fas fa-redo"></i>
                {{ awtTrans('اعادة الطلب') }}
              </button>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="prev-ord-card">
            <div class="prev-ord">
              <div class="media">

                <a href="#" data-toggle="modal" data-target="#order-modal"><img
                       src="{{ client_path() }}/images/prev-orders/Mask Group 5.png"></a>
                <div class="media-body">
                  <h4>بيتزا هت</h4>
                  <p>
                    {{ awtTrans('الطلب') }}
                    :
                    <span>12546546</span>
                  </p>
                  <p>
                    {{ awtTrans('الوقت') }}
                    :
                    <span>2021 / 02 / 25 - 23:11</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="prev-actions">
              <button data-toggle="modal" data-target="#rate-modal" class="btn rate-btn">
                <i class="far fa-grin"></i>
                {{ awtTrans('تقيم') }}
              </button>
              <button data-toggle="modal" data-target="#order-modal" class="btn repeat-btn">
                <i class="fas fa-redo"></i>
                {{ awtTrans('اعادة الطلب') }}
              </button>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="prev-ord-card">
            <div class="prev-ord">
              <div class="media">

                <a href="#" data-toggle="modal" data-target="#order-modal"><img
                       src="{{ client_path() }}/images/prev-orders/Mask Group 5.png"></a>
                <div class="media-body">
                  <h4>بيتزا هت</h4>
                  <p>
                    {{ awtTrans('الطلب') }}
                    :
                    <span>12546546</span>
                  </p>
                  <p>
                    {{ awtTrans('الوقت') }}
                    :
                    <span>2021 / 02 / 25 - 23:11</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="prev-actions">
              <button data-toggle="modal" data-target="#rate-modal" class="btn rate-btn">
                <i class="far fa-grin"></i>
                {{ awtTrans('تقيم') }}
              </button>
              <button data-toggle="modal" data-target="#order-modal" class="btn repeat-btn">
                <i class="fas fa-redo"></i>
                {{ awtTrans('اعادة الطلب') }}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!--order-details modal -->
  <div class="modal order-details fade" id="order-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="d-status">
            <h5 class="modal-title" id="orderModalLongTitle">
              {{ awtTrans('تفاصيل الطلب') }}</h5>
          </div>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="detail-order">
            <div class="row">
              <div class="col-lg-4">
                <div class="right-details">
                  <ul class="list-unstyled or-list">
                    <li>
                      <span class="tit">
                        {{ awtTrans('رقم الطلب') }}
                        :
                      </span>
                      <span class="val">123456789</span>
                    </li>
                    <li>
                      <span class="tit">
                        {{ awtTrans('مقدم الخدمة') }}
                        :
                      </span>
                      <span class="val">بيتزا هت</span>
                    </li>
                    <li>
                      <span class="tit">
                        {{ awtTrans('الوقت') }}
                        :
                      </span>
                      <span class="val">2021 / 02 / 25 - 23:11</span>
                    </li>
                    <li>
                      <span class="tit">
                        {{ awtTrans('طريقة الدفع') }}
                        :
                      </span>
                      <span class="val">كاش</span>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-7 offset-lg-1">
                <div class="left-details">
                  <ul class="list-unstyled or-list">
                    <li>
                      <span class="tit">
                        {{ awtTrans('العنوان') }}
                        :
                      </span>
                      <span class="val">الرياض ، الحسينية ، بلوك 15 ، شقة
                        2</span>
                    </li>
                    <li>
                      <span class="tit">
                        {{ awtTrans('رقم الجوال') }}
                        :
                      </span>
                      <span class="val">966 434837438</span>
                    </li>
                    <li>
                      <span class="tit">
                        {{ awtTrans('حالة الطلب') }}
                        :
                      </span>
                      <span class="val">منتهى</span>
                    </li>
                  </ul>
                  <div class="prev-actions">
                    <button data-target="#rate-modal" data-toggle="modal"
                            class="btn rate-btn">
                      <i class="far fa-grin"></i>
                      {{ awtTrans('تقيم') }}
                    </button>
                    <button class="btn repeat-btn">
                      <i class="fas fa-redo"></i>
                      {{ awtTrans('اعادة الطلب') }}
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <hr class="line">
            <div class="bottom-details">
              <div class="row">
                <div class="col-lg-8">
                  <ul class="list-unstyled right-menu">
                    <li>
                      <p><span>2x</span> بيتزا خضروات</p>
                      <h5>25 ر.س</h5>
                    </li>
                    <li>
                      <p><span>2x</span> بيتزا خضروات</p>
                      <h5>25 ر.س</h5>
                    </li>
                    <li>
                      <p><span>2x</span> بيتزا خضروات</p>
                      <h5>25 ر.س</h5>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-4">
                  <h4 class="total">{{ awtTrans('مجموع الطلب') }}</h4>
                  <ul class="list-unstyled left-menu">
                    <li>
                      <span>{{ awtTrans('مبلغ الطلب') }}</span>
                      <span>25 ر.س</span>
                    </li>
                    <li>
                      <span>{{ awtTrans('رسوم التوصيل') }}</span>
                      <span>25 ر.س</span>
                    </li>
                    <li>
                      <h5>{{ awtTrans('المبلغ المدفوع') }}</h5>
                      <h5>25 ر.س</h5>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--rate-modal-->
  <div class="modal rate-details fade" id="rate-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="d-status">
            <h5 class="modal-title" id="orderModalLongTitle">
              {{ awtTrans('تقيم الطلب') }}</h5>
          </div>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row justify-content-center">
            <div class="col-lg-8">
              <div class="rate-box">
                <form>
                  <div class="rate-det">
                    <div class="rate-text">
                      <h5>{{ awtTrans('التغليف') }}</h5>
                    </div>
                    <div class="rating">
                      <input name="stars" id="e5" type="radio">
                      <label for="e5">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e4" type="radio">
                      <label for="e4">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e3" type="radio">
                      <label for="e3">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e2" type="radio">
                      <label for="e2">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e1" type="radio">
                      <label for="e1">
                        <i class="fas fa-star"></i>
                      </label>
                    </div>
                  </div>
                  <div class="rate-det">
                    <div class="rate-text">
                      <h5>{{ awtTrans('جودة المنتجات') }}</h5>
                    </div>
                    <div class="rating">
                      <input name="stars" id="e5" type="radio">
                      <label for="e5">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e4" type="radio">
                      <label for="e4">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e3" type="radio">
                      <label for="e3">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e2" type="radio">
                      <label for="e2">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e1" type="radio">
                      <label for="e1">
                        <i class="fas fa-star"></i>
                      </label>
                    </div>
                  </div>
                  <div class="rate-det">
                    <div class="rate-text">
                      <h5>{{ awtTrans('المنتجات كما طلبتها') }}</h5>
                    </div>
                    <div class="rating">
                      <input name="stars" id="e5" type="radio">
                      <label for="e5">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e4" type="radio">
                      <label for="e4">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e3" type="radio">
                      <label for="e3">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e2" type="radio">
                      <label for="e2">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars" id="e1" type="radio">
                      <label for="e1">
                        <i class="fas fa-star"></i>
                      </label>
                    </div>
                  </div>
                  <button class="stand-link" data-target="#rate2-modal"
                          data-toggle="modal" type="button">
                    <span>{{ awtTrans('تقيم') }}</span>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--rate-captin-modal-->
  <div class="modal rate2-details fade" id="rate2-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="d-status">
            <h5 class="modal-title" id="orderModalLongTitle">
              {{ awtTrans('تقيم الكابتن') }}</h5>
          </div>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row justify-content-center">
            <div class="col-lg-8">
              <div class="rate-box">
                <form>
                  <div class="rate-det">
                    <div class="rate-text">
                      <h5>{{ awtTrans('مظهر السائق') }}</h5>
                    </div>
                    <div class="rating">
                      <input name="stars2" id="e5" type="radio">
                      <label for="e5">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e4" type="radio">
                      <label for="e4">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e3" type="radio">
                      <label for="e3">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e2" type="radio">
                      <label for="e2">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e1" type="radio">
                      <label for="e1">
                        <i class="fas fa-star"></i>
                      </label>
                    </div>
                  </div>
                  <div class="rate-det">
                    <div class="rate-text">
                      <h5>{{ awtTrans('الإجراءات الوقائية') }}</h5>
                    </div>
                    <div class="rating">
                      <input name="stastars2rs" id="e5" type="radio">
                      <label for="e5">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e4" type="radio">
                      <label for="e4">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e3" type="radio">
                      <label for="e3">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e2" type="radio">
                      <label for="e2">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e1" type="radio">
                      <label for="e1">
                        <i class="fas fa-star"></i>
                      </label>
                    </div>
                  </div>
                  <div class="rate-det">
                    <div class="rate-text">
                      <h5>{{ awtTrans('اسلوب السائق') }}</h5>
                    </div>
                    <div class="rating">
                      <input name="stars2" id="e5" type="radio">
                      <label for="e5">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e4" type="radio">
                      <label for="e4">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e3" type="radio">
                      <label for="e3">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e2" type="radio">
                      <label for="e2">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e1" type="radio">
                      <label for="e1">
                        <i class="fas fa-star"></i>
                      </label>
                    </div>
                  </div>
                  <div class="rate-det">
                    <div class="rate-text">
                      <h5>{{ awtTrans('سرعة التوصيل') }}</h5>
                    </div>
                    <div class="rating">
                      <input name="stars2" id="e5" type="radio">
                      <label for="e5">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e4" type="radio">
                      <label for="e4">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e3" type="radio">
                      <label for="e3">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e2" type="radio">
                      <label for="e2">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e1" type="radio">
                      <label for="e1">
                        <i class="fas fa-star"></i>
                      </label>
                    </div>
                  </div>
                  <div class="rate-det">
                    <div class="rate-text">
                      <h5>{{ awtTrans('التجاوب') }}</h5>
                    </div>
                    <div class="rating">
                      <input name="stars2" id="e5" type="radio">
                      <label for="e5">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e4" type="radio">
                      <label for="e4">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e3" type="radio">
                      <label for="e3">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e2" type="radio">
                      <label for="e2">
                        <i class="fas fa-star"></i>
                      </label>
                      <input name="stars2" id="e1" type="radio">
                      <label for="e1">
                        <i class="fas fa-star"></i>
                      </label>
                    </div>
                  </div>
                  <div class="tips">
                    <h5>{{ awtTrans('إكرامية') }}</h5>
                    <div class="tips-data">
                      <input type="text" class="form-control" placeholder="{{ awtTrans('اكتب المبلغ') }}">
                      <button class="btn">{{ awtTrans('دفع') }}</button>
                    </div>
                  </div>
                  <button class="stand-link">
                    <span>{{ awtTrans('تقيم') }}</span>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('front.client.layouts.parts.section-footer-app')
@endsection
