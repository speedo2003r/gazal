@extends('front.client.layouts.master')

@push('style')
  <style>
    .food-filter .filter-right ul li span.icon img {
      width: 35%;
      height: 40px;
    }
  </style>
@endpush
@section('content')
  @include('front.client.layouts.parts.section-main-header')

  <!--start prev-orders-->
  <section class="food-filter">
    <div class="container">
      @livewire('category-branches-filter',[
      'categoryBranches' => $categoryBranches,
      'providersCategories' => $providersCategories,
      'mainCategory' => $mainCategory,
      'requestLatLngCity' => collect(request()->query())
      ])
    </div>
  </section>

  <!--end prev-orders-->

  @include('front.client.layouts.parts.section-footer-app')
@endsection
