@extends('front.client.layouts.master')
@section('content')
  @include('front.client.layouts.parts.section-main-header')

  <section class="all-forms">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="my-form foot-form">
            <div class="top-logo">
              <img src="{{ client_path() }}/images/icons/surface1.png">
              <h4>{{ awtTrans('إضافة بطاقة') }}</h4>
              <p>
                {{ awtTrans('سيتم تعيين هذه البطاقة كطريقة الدفع الافتراضية لطلباتك المستقبلية') }}
              </p>
            </div>
            <form role="form">
              <div class="form-group have-icon">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('الاسم') }}">
                <span class="icon fas fa-user"></span>
              </div>
              <div class="form-group have-icon">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('رقم البطاقة') }}">
                <span class="icon fas fa-credit-card"></span>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group have-icon">
                    <input class="form-control" id="datepicker"
                           placeholder="{{ awtTrans('تاريخ الانتهاء') }}" />
                    <span class="icon fas fa-calendar-week"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group have-icon">
                    <input type="text" class="form-control" placeholder="CVV" />
                    <span class="icon fas fa-lock"></span>
                  </div>
                </div>
              </div>
              <div class="center-align">
                <button class="stand-link btn-block">
                  <span>{{ awtTrans('تأكيد') }}</span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('front.client.layouts.parts.section-footer-app')
@endsection
