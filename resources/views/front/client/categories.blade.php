@extends('front.client.layouts.master')

@section('closeYouHeader')
  <!--close you-->
  <div class="midle-icons">
    <div class="row justify-content-center">
      <div class="col-sm-12">
        <p class="top-del">
          {{ awtTrans('توصيل الى') }}
          {{ request('map_desc') ?? '' }}
          <span class="icon">
            <i class="fas fa-angle-down"></i>
          </span>
        </p>
      </div>
      <div class="col-md-5">
        @livewire('main-categories',['mainCategories' => $mainCategories])
      </div>
    </div>
  </div>
  <!--close you-->
@endsection

@section('content')
  @include('front.client.layouts.parts.section-main-header')

  <!--start food-near-->
  <section class="food-near">
    @livewire('category-branches',[
        'category_id' => $mainCategories->first()->id,
        'category_title' => $mainCategories->first()->title,
        'requestLatLngCity' => collect(request()->query())
        ])
  </section>
  <!--end food-near-->

  @include('front.client.layouts.parts.section-footer-app')
@endsection
