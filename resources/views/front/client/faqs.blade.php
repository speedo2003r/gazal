@extends('front.client.layouts.master')
@section('content')
  @include('front.client.layouts.parts.section-main-header')

  <!--start faqs-->

  <section class="all-static">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="bradcrumb">
            <ul class="list-unstyled">
              <li><a
                   href="{{ route('client.categories') }}">{{ awtTrans('الرئيسية') }}</a>
              </li>
              <li><i class="fas fa-angle-left"></i></li>
              <li><a href="faqs.html">{{ awtTrans('الاسئلة الشائعة') }}</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="all-text">
        <div class="row">
          <div class="col-md-8">
            <div id="accordion">
              @foreach ($faqs as $faq)
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse"
                              data-target="#collapseOne" aria-expanded="true"
                              aria-controls="collapseOne">
                        {{ $faq->key }}
                      </button>
                    </h5>
                  </div>

                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                       data-parent="#accordion">
                    <div class="card-body">
                      {{ $faq->value }}
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
          <div class="col-md-4">
            <div class="img">
              <img src="{{ client_path() }}/images/static-pages/FAQs-rafiki.png">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!--end faqs-->

  @include('front.client.layouts.parts.section-footer-app')
@endsection
