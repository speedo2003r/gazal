<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="ie=edge" />
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>greep</title>

<link rel="stylesheet" href="{{ client_path() }}/css/all.min.css" />
<link rel="stylesheet" href="{{ client_path() }}/css/bootstrap.min.css" />

<!--keep this in arabic and remove in english-->
@if(lang() == 'ar')
<link rel="stylesheet" href="{{ client_path() }}/css/bootstrap-rtl.css" />
@endif
<!--keep this in arabic and remove in english-->

<link rel="stylesheet" href="{{ client_path() }}/css/owl.carousel.min.css" />
<link rel="stylesheet" href="{{ client_path() }}/css/owl.theme.default.min.css" />

<!--keep this in arabic english-->
<link rel="stylesheet" href="{{ client_path() }}/css/style.css" />
<!--keep this in arabic english-->

<link rel="stylesheet" href="{{ client_path() }}/css/media.css" />
<link rel="stylesheet" href="{{ client_path() }}/css/animate.css" />
<link rel="stylesheet" href="{{ client_path() }}/js/wow.min.js" />
<link rel="stylesheet" href="{{ client_path() }}/css/hover-min.css" />
<link rel="stylesheet" href="{{ client_path() }}/css/toastr.min.css" />

<link rel="shortcut icon" href="{{ client_path() }}/images/Group 725.png"
      type="image/x-icon" />

@stack('style')
@livewireStyles
