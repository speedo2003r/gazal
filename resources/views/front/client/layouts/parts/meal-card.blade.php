<div class="meal-card">
  <div class="top-meal">
    <di class="media">
      <img src="{{ $item->main_image }}" alt="item">
      <div class="media-body">
        <span>{{ $item->title ?? '' }}</span>
        <p>{{ $item->description ?? '' }}</p>
      </div>
    </di>
    <button class="love {{ $item->is_fav ? 'fas' : 'far' }} fa-heart"
            wire:click="toggleFav({{ $item->id }})"></button>
  </div>
  <div class="bottom-meal">
    <h5 class="price">
      {{ $item->price() }}
      {{ awtTrans('ر.س') }}
    </h5>
    @if ($showPlus)
      <button class="btn"
              wire:click="$emit('add-item', {{ $item->id }}, {{ $branch->id }})">
        <i class="fas fa-plus"></i>
      </button>
    @endif
  </div>
</div>
