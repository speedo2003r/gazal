<div id="scroll-top">
  <i class="fas fa-angle-up"></i>
</div>

<script src="{{ client_path() }}/js/jquery.min.js"></script>
<script src="{{ client_path() }}/js/popper.min.js"></script>
<script src="{{ client_path() }}/js/bootstrap.min.js"></script>
<script src="{{ client_path() }}/js/owl.carousel.min.js"></script>
<script src="{{ client_path() }}/js/map.js"></script>
<script src="{{ client_path() }}/js/main.js"></script>
<script src="{{ client_path() }}/js/wow.min.js"></script>

<script src="{{ client_path() }}/js/toastr.min.js"></script>
@include('front.client.layouts.parts.alert')

<script>
  new WOW().init();

  $('#change-language').on('change', function() {
    let Lang = $(this).val();
    $.get('/change-language/' + Lang, function(res) {
      location.reload();
    });
  });

  // create uuid in browser localstorage for guest use
  $(document).ready(function() {
    let UUID = localStorage.getItem('UUID');
    if (UUID == null) {
      let randomId = Math.random().toString(36).substring(2, 15);
      localStorage.setItem('UUID', randomId);
      UUID = localStorage.getItem('UUID');
    }

    $('#uuid').val(UUID);
    $.get('/session-uuid/' + UUID, function(res) {
      console.log(res.msg);
    });
  })

  // display toastr from livewire events
  window.addEventListener('showToastr', event => {
    let res_key = event.detail.key;
    let res_msg = event.detail.msg;
    if ('success' == res_key) {
      toastr.success(res_msg);
    } else {
      toastr.success(res_msg);
    }
  })
</script>

<!-- connect and listen to comming messages from socket -->
@if (Auth::check())
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js">
  </script>
  <script>
    $(document).ready(function() {
      var socket = io("{{ env('NODE_HOST') . ':' . env('NODE_PORT') }}", {
        query: "id= " + "{{ Auth::id() }}"
      });

      socket.on("connect", function(data) {
        console.log('connected to socket from site master')

        socket.emit('update-driver-location', {
          location: {
              lat: '111',
              lng: '222'
          },
          toUserId: "{{ Auth::id() }}"
        })

        socket.emit('get-order-data', {
          orderId: 1
        })
      });

      socket.on('connect_error', function(err) {
        console.log('socket err', err)
        console.log('يرجى التواصل مع الدعم لتنشيط الاتصال');
      });

      socket.on('update-driver-location-res', function(res) {
        console.log('update-driver-location-res', res)
      });

      socket.on('order-data-res', function(res) {
        console.log('order-data-res', res)
      });

      socket.on("addMessageResponse", function(message) {
        // playSound(window.origin + '/img/inflicted.mp3');
        // console.log('message', message)
        // toastr.options = { onclick: function () { window.location.replace('/get-site-chat/' + message.room_id); } }
        // toastr.info('لديك رسالة جديدة')
      });
    });

    function playSound(sound) {
      if (sound) {
        var audio = new Audio(sound);
        audio.play();
      }
    }
  </script> --}}
@endif

<script src="{{ asset('js/app.js') }}"></script>
<script>
  Echo.channel('channel-order-data')
    .listen('.order.updated', (e) => {
      console.log('data from pusher: ', e);
    });

  Echo.private(`driver.1`)
    .listen('location.updated', (e) => {
      console.log(e);
    });
</script>

@stack('script')
@livewireScripts
