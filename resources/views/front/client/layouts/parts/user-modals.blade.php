  <!-- your-data -->
  <div class="modal fade" id="data-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ awtTrans('بياناتك') }}</h5>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="my-form">
            <form role="form" method="post" action="{{ route('client.profile') }}">
              <div class="form-group have-icon">
                <input type="text" class="form-control" name="name" placeholder="{{ auth()->user()->name }}">
                <i class="fas fa-user"></i>
              </div>
              <div class="form-group have-icon">
                <input type="email" class="form-control" name="email" placeholder="{{ auth()->user()->email }}">
                <i class="fas fa-envelope"></i>
              </div>
              <div class="center-align">
                <button type="submit" class="btn btn-block btnSubmitForm">{{ awtTrans('تعديل') }}</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--active phone-->
  <div class="modal fade" id="phone-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ awtTrans('تعديل رقم الجوال') }}</h5>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="my-form">
            <form role="form" method="post" action="{{ route('client.profile') }}">
              <div class="form-group have-icon">
                <input type="text" class="form-control" name="phone" placeholder="{{ auth()->user()->phone }}">
                <i class="fas fa-mobile-alt"></i>
              </div>
              <div class="center-align">
                <button type="submit" class="btn btn-block btnSubmitForm">{{ awtTrans('تعديل') }}</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--password edit-->
  <div class="modal fade" id="password-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{ awtTrans('تعديل كلمة المرور') }}</h5>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="my-form">
            <form role="form" method="post" action="{{ route('client.profile') }}">
              <div class="form-group have-icon">
                <input type="password" class="form-control"
                       placeholder="{{ awtTrans('كلمة المرور القديمة') }}" name="old_password">
                <i class="fas fa-lock"></i>
              </div>
              <div class="form-group have-icon">
                <input type="password" class="form-control"
                       placeholder="{{ awtTrans('كلمة المرور الحديثة') }}">
                <i class="fas fa-lock"></i>
              </div>
              <div class="form-group have-icon">
                <input type="password" class="form-control"
                       placeholder="{{ awtTrans('تأكيد كلمة المرور الحديثة') }}">
                <i class="fas fa-lock"></i>
              </div>
              <div class="center-align">
                <button type="submit" class="btn btn-block btnSubmitForm">{{ awtTrans('تعديل') }}</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--order-details-->
  <div class="modal order-details fade" id="order-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="d-status">
            <h5 class="modal-title" id="orderModalLongTitle">تفاصيل الطلب</h5>
          </div>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="detail-order">
            <div class="row">
              <div class="col-lg-4">
                <div class="right-details">
                  <ul class="list-unstyled or-list">
                    <li>
                      <span class="tit">رقم الطلب : </span>
                      <span class="val">123456789</span>
                    </li>
                    <li>
                      <span class="tit">مقدم الخدمة : </span>
                      <span class="val">بيتزا هت</span>
                    </li>
                    <li>
                      <span class="tit">الوقت : </span>
                      <span class="val">2021 / 02 / 25 - 23:11</span>
                    </li>
                    <li>
                      <span class="tit">طريقة الدفع : </span>
                      <span class="val">كاش</span>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-7 offset-lg-1">
                <div class="left-details">
                  <ul class="list-unstyled or-list">
                    <li>
                      <span class="tit">العنوان : </span>
                      <span class="val">الرياض ، الحسينية ، بلوك 15 ، شقة
                        2</span>
                    </li>
                    <li>
                      <span class="tit">رقم الجوال : </span>
                      <span class="val">966 434837438</span>
                    </li>
                    <li>
                      <span class="tit">حالة الطلب : </span>
                      <span class="val">منتهى</span>
                    </li>
                  </ul>
                  <div class="prev-actions">
                    <button data-target="#rate-modal" data-toggle="modal"
                            class="btn rate-btn"><i class="far fa-grin"></i>
                      تقيم</button>
                    <button class="btn repeat-btn"><i class="fas fa-redo"></i> اعادة
                      الطلب</button>
                  </div>
                </div>
              </div>
            </div>
            <hr class="line">
            <div class="bottom-details">
              <div class="row">
                <div class="col-lg-8">
                  <ul class="list-unstyled right-menu">
                    <li>
                      <p><span>2x</span> بيتزا خضروات</p>
                      <h5>25 ر.س</h5>
                    </li>
                    <li>
                      <p><span>2x</span> بيتزا خضروات</p>
                      <h5>25 ر.س</h5>
                    </li>
                    <li>
                      <p><span>2x</span> بيتزا خضروات</p>
                      <h5>25 ر.س</h5>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-4">
                  <h4 class="total">مجموع الطلب</h4>
                  <ul class="list-unstyled left-menu">
                    <li>
                      <span>مبلغ الطلب</span>
                      <span>25 ر.س</span>
                    </li>
                    <li>
                      <span>رسوم التوصيل</span>
                      <span>25 ر.س</span>
                    </li>
                    <li>
                      <h5>المبلغ المدفوع</h5>
                      <h5>25 ر.س</h5>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
