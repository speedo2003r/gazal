<!--srart footer-app-->
<section class="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <div class="foot">
          <h4>{{ awtTrans('دعونا نفعل ذلك معا') }}</h4>
          <ul class="list-unstyled">
            <li>
              <a href="{{ route('client.getJoinGreep') }}" target="_blank">
                {{ awtTrans('انضم لفريق قريب') }} </a>
            </li>
            <li>
              <a href="{{ route('client.getBeGreep') }}" target="_blank">
                {{ awtTrans('كن قريب') }} </a>
            </li>
            <li>
              <a href="{{ route('client.getBePartner') }}" target="_blank">
                {{ awtTrans('كن شريك') }} </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="foot">
          <h4>{{ awtTrans('روابط تهمك') }}</h4>
          <ul class="list-unstyled">
            <li>
              <a href="{{ route('client.aboutUs') }}"
                 target="_blank">{{ awtTrans('عن قريب') }}</a>
            </li>
            <li>
              <a href="{{ route('client.suggestions') }}"
                 target="_blank">{{ awtTrans('الاقتراحات') }}</a>
            </li>
            <li>
              <a href="{{ route('client.contactUs') }}"
                 target="_blank">{{ awtTrans('تواصل معنا') }}</a>
            </li>
            <li>
              <a href="{{ route('client.faqs') }}"
                 target="_blank">{{ awtTrans('الاسئلة الشائعة') }}</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="foot">
          <h4>{{ awtTrans('تابعنا عبر') }}</h4>
          <ul class="list-unstyled">
            <li><a href="{{ socials('facebook') }}"
                 target="_blank">{{ awtTrans('فيسبوك') }}</a></li>
            <li><a href="{{ socials('twitter') }}"
                 target="_blank">{{ awtTrans('تويتر') }}</a></li>
            <li><a href="{{ socials('instagram') }}"
                 target="_blank">{{ awtTrans('انستقرام') }}</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="foot">
          <ul class="list-unstyled last">
            <li>
              <a href="https://play.google.com/store/apps/details?id=com.greep.customer" target="_blank">
                <img src="{{ client_path() }}/images/download-app/google-paly.png">
              </a>
            </li>
            <li>
              <a href="" target="_blank">
                <img src="{{ client_path() }}/images/download-app/app-store.png">
              </a>
            </li>
            <li>
              <a href="" target="_blank">
                <img src="{{ client_path() }}/images/download-app/AppGallery.png">
              </a>
            </li>
            <li>
              <a href="{{ route('client.usagePolicy') }}"
                 target="_blank">{{ awtTrans('سياسة الاستخدام') }}</a>
            </li>
            <li>
              <a href="{{ route('client.privacyPolicy') }}"
                 target="_blank">{{ awtTrans('سياسة الخصوصية') }}</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="row">
          <div class="col-md-3">
            <div class="custom-select">
              <select name="lang" id="change-language">
                <option value="ar" {{ lang() == 'ar' ? 'selected' : '' }}>
                  {{ awtTrans('العربية') }}
                </option>
                <option value="en" {{ lang() == 'en' ? 'selected' : '' }}>
                  {{ awtTrans('English') }}
                </option>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--end footer-app-->
@auth
  @include('front.client.layouts.parts.user-modals')
@endauth
