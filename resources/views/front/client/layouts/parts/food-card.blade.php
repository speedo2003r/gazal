<div class="food-card">
  <div class="img">
    <img src="{{ client_path() }}/images/near-food/Mask Group 5.png">
  </div>
  <div class="exp">
    <div class="exp-info">
      <h4>{{ $branch->user->provider->store_name }}</h4>

      @foreach ($branch->user->categories as $category)
        <span>{{ $category->title }}</span>
        @if (!$loop->last)
          <span>-</span>
        @endif
      @endforeach

    </div>
    <div class="exp-del">
      <span>
        {{ $branch->user->provider->processing_time }}
        {{ awtTrans('دقيقة للتوصيل') }}
      </span>
    </div>
  </div>
  <div class="discount">
    15
    %
    {{ awtTrans('خصم') }}
  </div>
</div>
