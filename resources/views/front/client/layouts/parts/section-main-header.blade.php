<!--start main-header-->
<section
         class="main-header {{ \Route::currentRouteName() != 'client.providerDetails' ? 'other-header' : 'other-header2' }}">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="top-header">
          <!--logo-->
          <div class="logo">
            <a href="{{ route('client.home') }}">
              @if (\Route::currentRouteName() != 'client.providerDetails')
                <img src="{{ client_path() }}/images/Group 725.png">
              @else
                <img src="{{ client_path() }}/images/icons/logo-white.png">
              @endif
            </a>
            </a>
          </div>

          <!--search-->
          @if (\Route::currentRouteName() != 'client.providerDetails')
            <div class="search">
              <input type="text" class="form-control"
                     placeholder="{{ awtTrans('ابحث') }}">
              <i class="fas fa-search"></i>
            </div>
          @endif


          <!-- profile and current order -->
          @auth
            <div class="icons-head">
              <ul class="list-unstyled fl-hd">
                <!--user-->
                <li class="icon-item">
                  <a href="#" class="" role=" button" id="dropdownMenuLink"
                     data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                       class="far fa-user-circle"></i></a>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <li>
                      <div class="drop-flex">
                        <p>
                          {{ awtTrans('اهلا') }}
                          {{ auth()->user()->name }}
                          !
                        </p>
                      </div>
                    </li>
                    <li>
                      <div class="drop-flex">
                        <div class="exp">
                          <span>{{ awtTrans('الاسم') }}</span>
                          <p>{{ auth()->user()->name }}</p>
                          <span>{{ awtTrans('البريد الالكتروني') }}</span>
                          <p>{{ auth()->user()->email }}</p>
                        </div>
                        <a href="#" data-toggle="modal" data-target="#data-modal"
                           class="edit">{{ awtTrans('تعديل') }}</a>
                      </div>
                    </li>
                    <li>
                      <div class="drop-flex">
                        <div class="exp">
                          <span>{{ awtTrans('رقم الجوال') }}</span>
                          <p>{{ auth()->user()->phone }}</p>
                        </div>
                        <a href="#" data-toggle="modal" data-target="#phone-modal"
                           class="edit">{{ awtTrans('تعديل') }}</a>
                      </div>
                    </li>
                    <li>
                      <div class="drop-flex">
                        <div class="exp">
                          <span>{{ awtTrans('كلمة المرور') }}</span>
                          <p>**********</p>
                        </div>
                        <a data-toggle="modal" data-target="#password-modal" href="#"
                           class="edit">{{ awtTrans('تعديل') }}</a>
                      </div>
                    </li>
                    <li>
                      <div class="drop-flex">
                        <div class="exp">
                          <span>{{ awtTrans('المفضلة') }}</span>
                          <p>
                            {{ auth()->user()->Favs_items_count }}
                            {{ awtTrans('منتج') }}
                          </p>
                          <p>
                            {{ auth()->user()->Favs_branches_count }}
                            {{ awtTrans('فرع') }}
                          </p>
                        </div>
                        <a href="{{ route('client.favourite') }}"
                           class="edit">{{ awtTrans('عرض') }}</a>
                      </div>
                    </li>
                    <li>
                      <div class="drop-flex">
                        <div class="exp">
                          <span>{{ awtTrans('الرصيد والدفع') }}</span>
                          <p>
                            {{ awtTrans('الرصيد') }}
                            :
                            {{ auth()->user()->wallet }}
                            {{ awtTrans('ر.س') }}
                          </p>
                        </div>
                        <a href="{{ route('client.wallet') }}"
                           class="edit">{{ awtTrans('عرض') }}</a>
                      </div>
                    </li>
                    <li>
                      <div class="drop-flex">
                        <div class="exp">
                          <span>{{ awtTrans('كود المشاركة') }}</span>
                          <p>
                            {{ auth()->user()->share_code ?? '' }}
                          </p>
                        </div>
                        <a href="#" class="edit">{{ awtTrans('مشاركة') }}</a>
                      </div>
                    </li>
                    <li>
                      <div class="drop-flex">
                        <a href="{{ route('client.logout') }}"
                           class="edit">{{ awtTrans('تسجيل الخروج') }}</a>
                      </div>
                    </li>
                  </ul>
                </li>
                <!--orders-->
                <li class="icon-item">
                  <a href="#" class="" role=" button" id="dropdownMenuLink2"
                     data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                       class="fas fa-bars"></i></a>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                    <li>
                      <div class="drop-flex">
                        <p>
                          {{ awtTrans('اهلا') }}
                          محمود
                          !
                        </p>
                      </div>
                    </li>
                    <li>
                      <div class="drop-flex">
                        <div class="exp">
                          <div class="ord-data">
                            <span
                                  class="det-key">{{ awtTrans('الطلب الحالي') }}</span>
                            <span class="det-value">بيتزا هت</span>
                          </div>
                          <div class="ord-data">
                            <span
                                  class="det-key">{{ awtTrans('رقم الطلب') }}</span>
                            <span class="det-value">1458996</span>
                          </div>
                          <div class="ord-data">
                            <span class="det-key">{{ awtTrans('الوقت') }}</span>
                            <span class="det-value">11:55 21/2/2021</span>
                          </div>
                        </div>
                        <a href="" data-toggle="modal" data-target="#order-modal"
                           class="edit">{{ awtTrans('عرض التفاصيل') }}</a>
                      </div>
                    </li>
                    <li>
                      <div class="drop-flex fol-ord">
                        <h5>{{ awtTrans('تتبع الطلب') }}</h5>
                        <h5>
                          <i class="fas fa-clock"></i>
                          25
                          {{ awtTrans('دقيقة للتوصيل') }}
                        </h5>
                      </div>
                      <div id="map" class="map-menu"></div>
                    </li>
                    <li>
                      <div class="drop-flex">
                        <ul class="list-unstyled status-ord">
                          <!-- get from order translate status -->
                          <li><span>تم استلام الطلب</span></li>
                          <li><span>جاري تحضير الطلب</span></li>
                          <li><span>الغزال وصل لاستلام طلبك</span></li>
                          <li><span>جاري التوصيل</span></li>
                          <li><span>تم التوصيل</span></li>
                        </ul>
                      </div>
                    </li>
                    <li>
                      <div class="drop-flex2">
                        <a href="{{ route('client.prevOrders') }}"
                           class="edit diff">
                          {{ awtTrans('الطلبات السابقة') }}
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          @endauth
        </div>
      </div>
    </div>
    @yield('closeYouHeader')
  </div>
</section>
<!--end main-header-->
