<!DOCTYPE html>
<html lang="en">

<head>
  @include('front.client.layouts.parts.header')
</head>

<body>
  @include('front.client.layouts.parts.loader')

  @yield('content')

  @include('front.client.layouts.parts.footer')
</body>

</html>
