@extends('front.client.layouts.master')
@section('content')
  @include('front.client.layouts.parts.section-main-header')

  <!--start usage-policy-->

  <section class="all-static">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="bradcrumb">
            <ul class="list-unstyled">
              <li><a href="{{ route('client.categories') }}">{{ awtTrans('الرئيسية') }}</a></li>
              <li><i class="fas fa-angle-left"></i></li>
              <li><a href="usage-policy.html">{{ awtTrans('سياسة الاستخدام') }}</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="all-text">
        <div class="row">
          <div class="col-md-8">
            <div class="text">
              <h4>{{ $usagePolicy->title }}</h4>
              <p>{{ $usagePolicy->desc }}</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="img">
              <img src="{{ client_path() }}/images/static-pages/Responsive-rafiki.png">
            </div>
          </div>
          {{-- <div class="col-sm-12">
            <div class="text">
              <h4>عنوان النص</h4>
              <p>هذا النص هو مثال لنص يمكن أن يستخدم في نفس المساحة هذا النص هو مثال لنص
                يمكن أن يستخدم في نفس المساحة هذا النص هو مثال لنص يمكن أن يستخدم في نفس
                المساحة هذا النص هو مثال لنص يمكن أن يستخدم في نفس المساحة هذا النص هو مثال
                لنص
                يمكن أن يستخدم في نفس المساحة هذا النص هو مثال لنص يمكن أن يستخدم في نفس
                المساحة هذا النص هو مثال لنص يمكن أن يستخدم في نفس المساحة هذا النص هو مثال
                لنص يمكن أن يستخدم في نفس المساحة هذا النص هو مثال لنص يمكن أن يستخدم
                في نفس المساحة هذا النص هو مثال لنص يمكن أن يستخدم في نفس المساحة هذا النص
                هو مثال لنص يمكن أن يستخدم في نفس المساحة هذا النص هو مثال لنص يمكن أن
                يستخدم في نفس المساحة هذا النص هو مثال لنص يمكن أن يستخدم في نفس المساحة
                هذا النص هو مثال لنص يمكن أن يستخدم في نفس المساحة هذا النص هو مثال لنص يمكن
                أن يستخدم في نفس المساحة هذا النص هو مثال لنص يمكن أن يستخدم في نفس المساحة
                هذا النص هو مثال لنص يمكن أن يستخدم في نفس المساحة هذا النص هو مثال
                لنص يمكن أن يستخدم في نفس المساحة
              </p>
            </div>
          </div> --}}
        </div>
      </div>
    </div>
  </section>

  <!--end usage-policy-->

  @include('front.client.layouts.parts.section-footer-app')
@endsection
