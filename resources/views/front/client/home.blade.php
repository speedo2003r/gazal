@extends('front.client.layouts.master')

@push('style')
  <style>
    .flage img {
      width: 65px;
      height: 28px;
      margin-bottom: 3px;
    }

    .flage {}

    .drop-down {
      overflow: auto;
      max-height: 50px;
    }

    html {
      overflow: scroll;
      overflow-x: hidden;
    }

    ::-webkit-scrollbar {
      width: 0;
      /* Remove scrollbar space */
      background: transparent;
      /* Optional: just make scrollbar invisible */
    }

    /* Optional: show position indicator in red */
    ::-webkit-scrollbar-thumb {
      background: #FF0000;
    }

  </style>
@endpush
@section('content')
  <!--start main-header-->
  <section class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="top-header">
            <div class="logo">
              <img src="{{ client_path() }}/images/Group 725.png">
            </div>
            <div class="start-now">
              <button class="btn head" data-toggle="modal"
                      data-target="#login-modal">{{ awtTrans('ابدأ') }}</button>
            </div>
          </div>
        </div>
      </div>
      <!--close you-->
      <div class="close-you">
        <div class="row">
          <div class="col-md-4 wow fadeInRight" data-wow-duration="1s" data-wow-delay="1s">
            <div class="food-img">
              <img src="{{ client_path() }}/images/main-header/food.png">
            </div>
          </div>
          <div class="col-md-7 offset-md-1 wow fadeInLeft" data-wow-duration="1s"
               data-wow-delay="1s">
            <div class="exp">
              <h1> {{ awtTrans('قربناك من كل شئ') }}</h1>
              <p>{{ awtTrans('مطاعم , ماركت , صيدليات , ورد وأي شئ') }}</p>
              <div class="input-group">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-secondary" type="button"
                          data-toggle="modal" data-target="#address-modal"
                          onclick="initialize()">
                    <i class="fas fa-flag"></i>
                  </button>
                </div>
                <input type="text" class="form-control" id="map_desc"
                       placeholder="{{ awtTrans('ما هو عنوانك') }}" aria-label=""
                       aria-describedby="basic-addon1">
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--close you-->
    </div>
  </section>
  <!--end main-header-->

  <!--start best resturant-->
  <section class="best-resturant">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="main-title">
            <h3>
              {{ awtTrans('أفضل المطاعم وأكثر في قريب') }}
            </h3>
          </div>
        </div>
      </div>
      <div class="row">
        @foreach ($resturants as $branch)
          <div class="col-lg-3 col-md-6">
            <div class="best-rest">
              <img src="{{ $branch->user->provider->banner ?? '' }}">
              <a href="{{ route('client.providerDetails', $branch->branch_id) }}">{{ $branch->user->provider->store_name ?? '' }}
              </a>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  <!--end best resturant-->

  <!--start all you need-->
  <section class="all-need">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="main-title">
            <h3>
              {{ awtTrans('كل اللى تحتاجه في مكان واحد') }}
            </h3>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 wow fadeInLeft" data-wow-delay="1s" data-wow-duration="1s">
          <div class="need-div">
            <img src="{{ client_path() }}/images/all-need/Take Away-bro.png">
            <h5>{{ awtTrans('أفضل المطاعم في مدينتك') }}}</h5>
            <p>
              {{ awtTrans('مع مجموعة كبيرة ومتنوعة من المطاعم ، يمكنك طلب طعامك المفضل أو استكشاف مطاعم
              جديدة قريبة') }}
            </p>
          </div>
        </div>
        <div class="col-md-4 wow fadeInDown" data-wow-delay="1s" data-wow-duration="1s">
          <div class="need-div">
            <img src="{{ client_path() }}/images/all-need/Take Away-amico.png">
            <h5>{{ awtTrans('توصيل سريع') }}</h5>
            <p>
              {{ awtTrans('نحن نفخر بأنفسنا على السرعة. اطلب أو أرسل أي شيء في مدينتك وسنقوم باستلامه
              وتسليمه في دقائق.') }}
            </p>
          </div>
        </div>
        <div class="col-md-4 wow fadeInRight" data-wow-delay="1s" data-wow-duration="1s">
          <div class="need-div">
            <img src="{{ client_path() }}/images/all-need/Group 4681.png">
            <h5>{{ awtTrans('توصيل البقالة والمزيد') }}</h5>
            <p>
              {{ awtTrans('ابحث عن أي شيء تحتاجه! من محلات السوبر ماركت إلى المحلات التجارية والصيدليات
              إلى بائعي الزهور - إذا كان في مدينتك يمكنك الاعتماد علينا في الحصول علي كل ما
              تحتاجة .') }}
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--end all you need-->

  <!--start cities we are there-->
  <section class="home-cities">
    <img class="shape1" src="{{ client_path() }}/images/cities/bg-shape.png">
    <img class="shape2" src="{{ client_path() }}/images/cities/bg-shape2.png">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="main-title">
            <img src="{{ client_path() }}/images/cities/World Bicycle Day-bro.png">
            <h4>{{ awtTrans('المدن التى نصل اليها') }}</h4>
          </div>
        </div>
      </div>
      <div class="cities-list">
        <div class="row">
          @foreach ($cities as $city)
            <div class="col-lg-2 col-md-4">
              <div class="cit-list">
                <span>{{ $city->title }}</span>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>
  <!--start cities we are there-->

  <!--start download-app-->
  <section class="download-app">
    <div class="container">
      <div class="row">
        <div class="col-md-4 wow fadeInUp" data-wow-delay="1s" data-wow-duration="1s">
          <div class="down-text">
            <img src="{{ client_path() }}/images/download-app/Group 4690.png">
            <h4>{{ awtTrans('حمل التطبيق الان') }}</h4>
            <p>{{ awtTrans('اطلب أي شيء وتتبعه في الوقت الفعلي باستخدام تطبيق قريب') }}
            </p>
            <ul class="down-btns list-unstyled">
              <li>
                <a href="https://play.google.com/store/apps/details?id=com.greep.customer"
                   target="_blank">
                  <img src="{{ client_path() }}/images/download-app/google-paly.png">
                </a>
              </li>
              <li>
                <a href="">
                  <img src="{{ client_path() }}/images/download-app/app-store.png">
                </a>
              </li>
              <li>
                <a href="">
                  <img src="{{ client_path() }}/images/download-app/AppGallery.png">
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-md-8 wow fadeInDown" data-wow-delay="1s" data-wow-duration="1s">
          <div class="down-screens">
            <img src="{{ client_path() }}/images/download-app/down-screens.png">
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--end download-app-->

  <!-- login -->
  <div class="modal fade" id="login-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">
            {{ awtTrans('تسجيل دخول في قريب') }}
          </h5>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="my-form">
            <form role="form" method="post" action="{{ route('client.login') }}">

              <div class="form-group have-icon">
                <input type="text" class="form-control" name="phone"
                       placeholder="{{ awtTrans('رقم الجوال') }}">
                <i class="fas fa-mobile-alt"></i>
              </div>

              <div class="form-group have-icon">
                <input type="password" class="form-control" name="password"
                       placeholder="{{ awtTrans('كلمة المرور') }}">
                <i class="fas fa-lock"></i>
              </div>

              <div class="forget-pass">
                <a href="#" data-toggle="modal" data-target="#forget-modal">
                  {{ awtTrans('نسيت كلمة المرور؟') }}
                </a>
              </div>

              <div class="center-align">
                <button type="submit" class="btn btn-block btnSubmitForm">
                  {{ awtTrans('تسجيل الدخول') }}
                </button>
              </div>

              <div class="form-footer center-align">
                <p>{{ awtTrans('ليس هناك حساب ؟') }}
                  <a href="" data-toggle="modal" data-target="#register-modal">
                    {{ awtTrans('انشاء حساب') }}
                  </a>
                </p>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- register -->
  <div class="modal fade" id="register-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">
            {{ awtTrans('تسجيل دخول في قريب') }}
          </h5>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="my-form">
            <form role="form" method="post" enctype="multipart/form-data"
                  action="{{ route('client.register') }}">
              <div class="form-group have-icon">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('الاسم') }}" name="name">
                <i class="fas fa-user"></i>
              </div>
              <div class="form-group have-icon">
                {{-- <div class="col-1">
                  <i class="fas fa-mobile-alt"></i>
                </div>
                <div class="col-2 drop-down">
                  @foreach ($allCountries as $country)
                    <div class="flage">
                      <img src="{{ $country['flage'] }}" alt="" width="5" height="5">
                    </div>
                  @endforeach
                </div>
                <div class="col-9">
                  <input type="text" class="form-control"
                         placeholder="{{ awtTrans('رقم الجوال') }}" name="phone">
                </div> --}}

                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('رقم الجوال') }}" name="phone">
                <i class="fas fa-mobile-alt"></i>


              </div>
              <div class="form-group have-icon">
                <input type="email" class="form-control"
                       placeholder="{{ awtTrans('البريد الالكتروني') }}" name="email">
                <i class="fas fa-envelope"></i>
              </div>
              <div class="form-group have-icon">
                <input type="date" class="form-control"
                       placeholder="{{ awtTrans('تاريخ الميلاد') }}" name="birth_date">
                <i class="fas fa-calendar-day"></i>
              </div>
              <div class="form-group have-icon">
                <select name="gender" class="form-control">
                  <option value="">{{ awtTrans('اختر النوع') }}</option>
                  <option value="male">{{ awtTrans('ذكر') }}</option>
                  <option value="female">{{ awtTrans('انثى') }}</option>
                </select>
                <i class="fas fa-venus-mars"></i>
              </div>
              <div class="form-group have-icon">
                <input type="password" class="form-control"
                       placeholder="{{ awtTrans('كلمة المرور') }}" name="password">
                <i class="fas fa-lock"></i>
              </div>
              <div class="form-group have-icon">
                <input type="password" class="form-control"
                       placeholder="{{ awtTrans('تأكيد كلمة المرور') }}"
                       name="password_confirmation">
                <i class="fas fa-lock"></i>
              </div>
              <div class="forget-pass">
                <a href="" data-toggle="modal" data-target="#forget-modal">
                  {{ awtTrans('نسيت كلمة المرور؟') }}
                </a>
              </div>
              <div class="center-align">
                <button type="submit" class="btn btn-block btnSubmitForm">
                  {{ awtTrans('انشاء حساب') }}
                </button>
              </div>
              <div class="form-footer center-align">
                <p>
                  {{ awtTrans('لديك هناك حساب ؟') }}
                  <a href="" data-toggle="modal" data-target="#login-modal">
                    {{ awtTrans('تسجيل دخول') }}
                  </a>
                </p>
                <p>
                  {{ awtTrans('بالتسجيل ، فإنك توافق على شروط الخدمة و سياسة الخصوصية الخاصة بنا') }}
                </p>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- activate -->
  <div class="modal fade" id="activate-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="d-status">
            <h5 class="modal-title">
              {{ awtTrans('تفعيل رقم الجوال') }}
            </h5>
            <p>{{ awtTrans('ادخل كود التحقق المرسل علي جوالك') }}</p>
          </div>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="my-form">
            <form role="form" method="post" action="{{ route('client.activation') }}">
              <div class="form-group flex-inputs">
                <input type="text" class="form-control" maxlength="1" placeholder=""
                       name="code[]">
                <input type="text" class="form-control" maxlength="1" placeholder=""
                       name="code[]">
                <input type="text" class="form-control" maxlength="1" placeholder=""
                       name="code[]">
                <input type="text" class="form-control" maxlength="1" placeholder=""
                       name="code[]">
                <input type="text" class="form-control" maxlength="1" placeholder=""
                       name="code[]">
                <input type="text" class="form-control" maxlength="1" placeholder=""
                       name="code[]">
              </div>
              <div class="center-align">
                <button type="button" data-toggle="modal" data-target="#forget-modal"
                        class="btn btn-block btnSubmitForm">
                  {{ awtTrans('تحقق') }}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- forget -->
  <div class="modal fade" id="forget-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="d-status">
            <h5 class="modal-title">
              {{ awtTrans('ارسال كود التحقق') }}
            </h5>
            <p>{{ awtTrans('ادخل رقم الجوال لارسال كود التحقق') }}</p>
          </div>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="my-form">
            <form role="form" method="post"
                  action="{{ route('client.forgetPassword') }}">
              <div class="form-group flex-inputs">
                <input type="text" class="form-control" placeholder="" name="phone">
              </div>
              <div class="center-align">
                <button type="button" class="btn btn-block btnSubmitForm">
                  {{ awtTrans('ارسال') }}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- forget update -->
  <div class="modal fade" id="forget-update-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">
            {{ awtTrans('تغيير كلمة المرور') }}
          </h5>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="my-form">
            <form role="form" method="post"
                  action="{{ route('client.forgetPasswordUpdate') }}">

              <div class="form-group have-icon">
                <input type="text" class="form-control" name="code"
                       placeholder="{{ awtTrans('كود التحقق') }}">
              </div>

              <div class="form-group have-icon">
                <input type="password" class="form-control"
                       placeholder="{{ awtTrans('كلمة المرور') }}" name="password">
                <i class="fas fa-lock"></i>
              </div>
              <div class="form-group have-icon">
                <input type="password" class="form-control"
                       placeholder="{{ awtTrans('تأكيد كلمة المرور') }}"
                       name="password_confirmation">
                <i class="fas fa-lock"></i>
              </div>

              <div class="center-align">
                <button type="button" class="btn btn-block btnSubmitForm">
                  {{ awtTrans('ارسال') }}
                </button>
              </div>

              <div class="forget-pass">
                <a href="#" data-toggle="modal" data-target="#forget-modal">
                  {{ awtTrans('اعادة ارسال الكود') }}
                </a>
              </div>

              <div class="form-footer center-align">
                <p>{{ awtTrans('ليس هناك حساب ؟') }}
                  <a href="" data-toggle="modal" data-target="#register-modal">
                    {{ awtTrans('انشاء حساب') }}
                  </a>
                </p>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- address -->
  <div class="modal fade" id="address-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="d-status">
            <h5 class="modal-title">
              {{ awtTrans('اختيار عنوان') }}
            </h5>
          </div>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="my-form">
            <form role="form" method="get" action="{{ route('client.categories') }}">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group have-icon"
                       title="{{ awtTrans('اكتب عنوانك ثم اضغط ENTER للذهاب') }}">
                    <input type="text" class="form-control" id="map_search"
                           placeholder="{{ awtTrans('ما هو عنوانك - ENTER') }}">
                    <i class="fas fa-flag"></i>
                  </div>
                  <div class="detect-address">
                    <p>{{ awtTrans('او حدد موقعك على الخريطة') }}</p>
                  </div>
                  <input type="hidden" name="lat" id="lat">
                  <input type="hidden" name="lng" id="lng">
                  <input type="hidden" name="map_desc" id="map_desc2">
                </div>
                <div class="col-lg-6">
                  <div id="map" class="map-section"></div>
                </div>
              </div>
              <div class="center-align">
                <button type="submit" class="btn btn-block">
                  {{ awtTrans('تأكيد العنوان') }}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('maps.homeMap')
  @include('front.client.layouts.parts.section-footer-app')
@endsection
