@extends('front.client.layouts.master')
@section('content')

  <section class="all-forms">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="my-form foot-form">
            <div class="top-logo">
              <img src="{{ client_path() }}/images/icons/surface1.png">
              <h4>{{ awtTrans('إنضم لفريق قريب') }}</h4>
            </div>
            <form role="form" method="post" action="{{ route('client.storeJoinGreep') }}">
              <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('الاسم') }}" name="name">
              </div>
              <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('رقم الجوال') }}" name="phone">
              </div>
              <div class="form-group">
                <div class="custom-select">
                  <select name="city_id">
                    <option value="">{{ awtTrans('اختر المدينة') }}</option>
                    @foreach ($cities as $city)
                      <option value="{{ $city->id }}">{{ $city->title }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('الوظيفة') }}" name="job_description">
              </div>
              <div class="form-group">
                <input type="email" class="form-control"
                       placeholder="{{ awtTrans('البريد الالكتروني') }}" name="email">
              </div>
              <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('وصف') }}" name="desc">
              </div>
              <div class="form-group">
                <div class="file-upload">
                  <div class="file-select">
                    <div class="file-select-name noFile">{{ awtTrans('ارفاق') }} cv</div>
                    <input type="file" name="cv" class="chooseFile"
                           accept="application/pdf">
                    <i class="fas fa-upload"></i>
                  </div>
                </div>
              </div>
              <div class="center-align">
                <button class="stand-link btnSubmitForm">
                  <span>{{ awtTrans('ارسال الطلب') }}</span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection
