@extends('front.client.layouts.master')
@section('content')
  @include('front.client.layouts.parts.section-main-header')

  <!--start order-summery-->
  <section class="order-summery">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-8">
          <div class="tit-sum">
            <i class="fas fa-long-arrow-alt-right"></i>
            <span>{{ awtTrans('ملخص الطلب') }}</span>
          </div>
          <div class="res-tit">
            <h4> {{ $provider->store_name }} </h4>
          </div>
          <div class="ord-num">
            <h5>
              <span>
                {{ $count }}
                {{ awtTrans('منتج من') }}
              </span>
              {{ $branch->name }}
            </h5>
          </div>
          <div class="sum-elements">
            @foreach ($orderProducts as $orderProduct)
              <div class="order-element">
                <div class="order-flex">
                  <span>
                    {{ $orderProduct->qty }}
                    x
                    {{ $orderProduct->item->title }}
                  </span>
                  <h5>
                    {{ round($orderProduct->_price(), 2) }}
                    {{ awtTrans('ر.س') }}
                  </h5>
                </div>
                <div class="order-flex">
                  <button class="btn"><i class="fas fa-plus"></i></button>
                  <button class="btn"><i class="fas fa-minus"></i></button>
                </div>
              </div>
            @endforeach
          </div>

          <!-- map -->
          <div class="order-address">
            <div id="map"></div>
          </div>

          <!-- complete data -->
          <div class="bottom-items">
            <div class="media">
              <i class="fas fa-map-marker-alt"></i>
              <div class="media-body">
                <div class="body-text">
                  <p>الرياض ، الحسينية</p>
                  <span>الحسينية ، شارع الملك فهد</span>
                </div>
                <button data-toggle="modal" data-target="#address-modal"
                        class="btn"><i class="fas fa-angle-left"></i></button>
              </div>
            </div>
          </div>
          <div class="bottom-items">
            <div class="media">
              <i class="fas fa-calendar-week"></i>
              <div class="media-body">
                <div class="body-text">
                  <p>الإربعاء 25 أغسطس</p>
                  <span>18:00 - 19:00</span>
                </div>
                <button data-toggle="modal" data-target="#time-modal"
                        class="btn"><i class="fas fa-angle-left"></i></button>
              </div>
            </div>
          </div>
          <div class="bottom-items">
            <div class="media">
              <i class="fas fa-mobile-alt"></i>
              <div class="media-body">
                <div class="body-text">
                  <p>{{ awtTrans('أضف رقم الجوال') }}</p>
                </div>
                <button data-toggle="modal" data-target="#phone-modal"
                        class="btn"><i class="fas fa-angle-left"></i></button>
              </div>
            </div>
          </div>
          <div class="way-pay">
            <h4>{{ awtTrans('طريقة الدفع') }}</h4>
            <div class="bottom-items">
              <div class="media">
                <i class="fas fa-credit-card"></i>
                <div class="media-body">
                  <div class="body-text">
                    <p>{{ awtTrans('اختر طريقة الدفع') }}</p>
                  </div>
                  <form action="{{ route('client.createPaymentCard') }}">
                    <button type="submit" class="btn"><i
                         class="fas fa-angle-left"></i></button>
                  </form>
                </div>
              </div>
            </div>
            <div class="bottom-items">
              <div class="media">
                <i class="fas fa-tag"></i>
                <div class="media-body">
                  <div class="body-text">
                    <p>{{ awtTrans('احصل على كود خصم') }}</p>
                  </div>
                  <button class="btn"><i class="fas fa-angle-left"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- summary -->
        <div class="col-lg-3 col-md-4">
          <div class="the-summery">
            <div class="top-head">
              <h5>{{ awtTrans('الملخص') }}</h5>
              <img src="{{ client_path() }}/images/icons/food.png">
            </div>
            <div class="flex-total">
              <span>{{ awtTrans('المنتجات') }}</span>
              <span>
                {{ $cartPrice }}
                {{ awtTrans('ر.س') }}
              </span>
            </div>
            <div class="flex-total">
              <span>{{ awtTrans('التوصيل') }}</span>
              <span>
                {{ $cart->shipping_price }}
                {{ awtTrans('ر.س') }}
              </span>
            </div>
            <div class="flex-total">
              <span>{{ awtTrans('ضريبة القيمة المضافة') }}</span>
              <span>
                {{ $cart->vat_amount }}
                {{ awtTrans('ر.س') }}
              </span>
            </div>
            <div class="flex-total">
              <h5>{{ awtTrans('الاجمالي') }}</h5>
              <h5>
                {{ $cart->_price() }}
                {{ awtTrans('ر.س') }}
              </h5>
            </div>
            <button data-target="#send-modal" data-toggle="modal"
                    class="stand-link btn-block">
              <span>{{ awtTrans('تأكيد الطلب') }}</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--end order-summery-->

  <!--address-->
  <div class="modal fade" id="address-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="d-status">
            <h5 class="modal-title" id="exampleModalLongTitle">
              {{ awtTrans('اضافة عنوان توصيل') }}}</h5>
          </div>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="my-form">
            <form role="form">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group have-icon">
                    <input type="text" class="form-control"
                           placeholder="{{ awtTrans('ما هو عنوانك') }}}">
                    <i class="fas fa-flag"></i>
                  </div>
                  <div class="detect-address">
                    <p>{{ awtTrans('او حدد موقعك على الخريطة') }}</p>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div id="map2" class="map-section"></div>
                </div>
              </div>
              <div class="center-align">
                <button type="button"
                        class="btn btn-block">{{ awtTrans('تأكيد العنوان') }}</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--order-time-->
  <div class="modal fade order-time" id="time-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="d-status">
            <h5 class="modal-title" id="exampleModalLongTitle">
              {{ awtTrans('وقت الطلب') }}</h5>
            <p>{{ awtTrans('اختر التاريخ والوقت الذي تريد منا توصيل طلبك فيه') }}</p>
          </div>
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="my-form">
            <form role="form">
              <div class="row">
                <div class="col-lg-6">
                  <div class="ord-box">
                    <h5><i class="fas fa-calendar-week"></i> {{ awtTrans('التاريخ') }}
                    </h5>
                    <div class="box-data">
                      <ul class="list-unstyled">
                        <li>
                          <input type="radio" name="d1" id="rad1">
                          <label for="rad1">{{ awtTrans('في اقرب وقت') }}</label>
                        </li>
                        <li>
                          <input type="radio" name="d1" id="rad2">
                          <label for="rad2">{{ awtTrans('غدا') }}</label>
                        </li>
                        <li>
                          <input type="radio" name="d1" id="rad3">
                          <label for="rad3">24 اغسطس2021</label>
                        </li>
                        <li>
                          <input type="radio" name="d1" id="rad4">
                          <label for="rad4">25 اغسطس2021</label>
                        </li>
                        <li>
                          <input type="radio" name="d1" id="rad5">
                          <label for="rad5">26 اغسطس2021</label>
                        </li>
                        <li>
                          <input type="radio" name="d1" id="rad6">
                          <label for="rad6">27 اغسطس2021</label>
                        </li>
                        <li>
                          <input type="radio" name="d1" id="rad7">
                          <label for="rad7">28 اغسطس2021</label>
                        </li>
                        <li>
                          <input type="radio" name="d1" id="rad8">
                          <label for="rad8">29 اغسطس2021</label>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="ord-box">
                    <h5><i class="far fa-clock"></i> {{ awtTrans('الوقت') }}</h5>
                    <div class="box-data">
                      <ul class="list-unstyled">
                        <li>
                          <input type="radio" name="d2" id="tim1">
                          <label for="tim1">12.00 - 13.00</label>
                        </li>
                        <li>
                          <input type="radio" name="d2" id="2">
                          <label for="tim2">12.00 - 13.00</label>
                        </li>
                        <li>
                          <input type="radio" name="d2" id="tim3">
                          <label for="tim3">12.00 - 13.00</label>
                        </li>
                        <li>
                          <input type="radio" name="d2" id="tim4">
                          <label for="tim4">12.00 - 13.00</label>
                        </li>
                        <li>
                          <input type="radio" name="d2" id="tim5">
                          <label for="tim5">12.00 - 13.00</label>
                        </li>
                        <li>
                          <input type="radio" name="d2" id="tim6">
                          <label for="tim6">12.00 - 13.00</label>
                        </li>
                        <li>
                          <input type="radio" name="d2" id="tim7">
                          <label for="tim7">12.00 - 13.00</label>
                        </li>
                        <li>
                          <input type="radio" name="d2" id="tim8">
                          <label for="tim8">12.00 - 13.00</label>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="center-align">
                <button type="button" class="btn btn-block">{{ awtTrans('تأكيد') }}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--order-send-->
  <div class="modal fade" id="send-modal" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="order-send center-align">
            <div class="image">
              <img src="images/icons/Delivery-bro.png">
            </div>
            <h4>{{ awtTrans('تم ارسال طلبك بنجاح') }}</h4>
            <a href="{{ route('client.home') }}" target="_blank"
               class="stand-link">{{ awtTrans('اذهب للرئيسية') }}</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('front.client.layouts.parts.section-footer-app')
@endsection
