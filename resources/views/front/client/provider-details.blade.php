@extends('front.client.layouts.master')
@push('style')
  @if ($provider->banner)
    <style>
      .main-header.other-header2 {
        background: url({{ $provider->banner }});
      }

    </style>
  @endif
@endpush
@section('content')
  @include('front.client.layouts.parts.section-main-header')

  <!--start resturant-details-->
  <section class="resturant-details">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <!--breadcrumb-->
          <div class="bradcrumb">
            <ul class="list-unstyled">
              <li><a href="#">{{ $provider->store_name ?? '' }}</a></li>
              <li><i class="fas fa-angle-left"></i></li>
              <li><a href="">{{ $branch->name }}</a></li>
              {{-- <li><i class="fas fa-angle-left"></i></li>
              <li><a href="">المطعم</a></li> --}}
              {{-- <li><i class="fas fa-angle-left"></i></li>
              <li><a href="">بيتزا هت</a></li> --}}
            </ul>
          </div>
          <!--resturant details-->
          <div class="rest-details">
            <div class="rest-card">
              <div class="rest-exp">
                <h4>{{ $branch->name }}</h4>
                @foreach ($providerCategories as $category)
                  <span>{{ $category->title }}</span>
                  @if (!$loop->last)
                    <span>-</span>
                  @endif
                @endforeach

                {{-- @if ($provider->processing_time) --}}
                <p>
                  <img
                       src="{{ client_path() }}/images/resturant-details/food-delivery.png">
                  {{ $provider->processing_time }}
                  {{ awtTrans('دقيقة للتوصيل') }}
                </p>
                {{-- @endif --}}
              </div>
              @livewire('branch-toggle-fav', ['branch' => $branch])
            </div>
          </div>

          <!--filter-->
          @livewire('branch-category-items', [
          'providerCategories' => $providerCategories,
          'branch' => $branch,
          'user' => $user,
          'provider' => $provider,
          ])
        </div>
        <div class="col-md-3">
          <!--order-->
          @livewire('cart-details')
        </div>
      </div>
    </div>
  </section>
  <!--end prev-orders-->

  @livewire('add-order-modal')
  @include('front.client.layouts.parts.section-footer-app')
@endsection
@push('script')
  <script>
    window.addEventListener('openAddOrderModal', event => {
      $("#add-order-modal").modal('show');
    })

    window.addEventListener('closeAddOrderModal', event => {
      $("#add-order-modal").modal('hide');
    })
  </script>
@endpush
