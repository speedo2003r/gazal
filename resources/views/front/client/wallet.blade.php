@extends('front.client.layouts.master')
@section('content')
  @include('front.client.layouts.parts.section-main-header')

  <!--start wallet-->

  <section class="wallet">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="bradcrumb">
            <ul class="list-unstyled">
              <li><a href="{{ route('client.categories') }}">{{ awtTrans('الرئيسية') }}</a></li>
              <li><i class="fas fa-angle-left"></i></li>
              <li><a href="faqs.html">{{ awtTrans('الرصيد والدفع') }}</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-md-4">
          <div class="wallet-card">
            <div class="wal-img">
              <img src="{{ client_path() }}/images/wallet/Wallet-bro.png">
            </div>
            <div class="wal-exp">
              <h4>55 ر.س</h4>
              <p>{{ awtTrans('رصيد في المحفظة') }}</p>
            </div>
          </div>
          <div class="money">
            <input type="text" class="form-control" placeholder="المبلغ">
            <button class="btn"><i class="fas fa-plus"></i>اضافة</button>
          </div>
        </div>
      </div>
      <div class="credit-sec">
        <div class="row">
          <div class="col-sm-12">
            <div class="cre-tit">
              <span>{{ awtTrans('بطاقات الإئتمان') }}</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="credit-card">
              <h4>{{ awtTrans('البطاقة الأساسية') }}</h4>
              <div class="cre-flex">
                <div class="name">
                  <i class="fas fa-star"></i>
                  <span>بطاقة ماستر كارد</span>
                </div>
                <img src="{{ client_path() }}/images/wallet/Group 4493.png">
              </div>
              <div class="cre-flex">
                <div class="name">
                  <span>1234****</span>
                </div>
                <div class="date">
                  {{ awtTrans('تاريخ الإنتهاء') }}
                  :
                  25
                  /
                  01
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12">
            <button class="add-button btn"><i
                 class="fas fa-plus"></i>{{ awtTrans('اضافة') }}</button>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!--end wallet-->

  @include('front.client.layouts.parts.section-footer-app')
@endsection
