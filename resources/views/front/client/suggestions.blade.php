@extends('front.client.layouts.master')
@section('content')
  @include('front.client.layouts.parts.section-main-header')

  <section class="all-static">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="bradcrumb">
            <ul class="list-unstyled">
              <li><a href="{{ route('client.home') }}">{{ awtTrans('الرئيسية') }}</a>
              </li>
              <li><i class="fas fa-angle-left"></i></li>
              <li><a href="#">{{ awtTrans('الاقتراحات') }}</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="all-text">
        <form action="{{ route('client.storeSuggest') }}" method="post">
          <div class="row">
            <div class="col-md-8">
              <div class="our-form">
                <div class="form-group">
                  <h4>{{ awtTrans('الاسم') }}</h4>
                  <input type="text" class="form-control" name="name"
                         placeholder="{{ awtTrans('الاسم') }}">
                </div>
                <div class="form-group">
                  <h4>{{ awtTrans('تفاصيل الاقتراح') }}</h4>
                  <textarea class="form-control" name="notes"
                            placeholder="{{ awtTrans('اكتب تفاصيل الاقتراح') }}">
                              </textarea>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="img">
                <img src="{{ client_path() }}/images/static-pages/Responsive-rafiki.png">
              </div>
            </div>
            <div class="col-sm-12">
              <div class="center-align">
                <button class="stand-link btnSubmitForm">
                  <span>{{ awtTrans('ارسال') }}</span>
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>

  @include('front.client.layouts.parts.section-footer-app')
@endsection
