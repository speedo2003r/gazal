@extends('front.client.layouts.master')
@section('content')
  @include('front.client.layouts.parts.section-main-header')

  <section class="favourite">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="bradcrumb">
            <ul class="list-unstyled">
              {{-- <li><a href="#">{{ awtTrans('الحسينية') }}</a></li> --}}
              {{-- <li><i class="fas fa-angle-left"></i></li> --}}
              <li><a href="">{{ awtTrans('المفضلة') }}</a></li>
            </ul>
          </div>
          <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="pills-home-tab" data-toggle="pill"
                 href="#branches" role="tab" aria-controls="pills-home"
                 aria-selected="true">{{ awtTrans('مقدمي الخدمات') }}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                 href="#items" role="tab" aria-controls="pills-profile"
                 aria-selected="false">{{ awtTrans('الوجبات') }}</a>
            </li>
          </ul>

          <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="branches" role="tabpanel"
                 aria-labelledby="pills-home-tab">
              <div class="row">
                @foreach ($branches as $branch)
                  <div class="col-lg-4 col-md-6">
                    <a href="{{ route('client.providerDetails', $branch) }}">
                      @include('front.client.layouts.parts.food-card',['branch' => $branch])
                    </a>
                  </div>
                @endforeach

              </div>
            </div>
            <div class="tab-pane fade" id="items" role="tabpanel"
                 aria-labelledby="pills-profile-tab">
              <div class="row">
                @foreach ($items as $item)
                  <div class="col-md-4">
                    @livewire('meal-card',['item' => $item])
                  </div>
                @endforeach
              </div>
            </div>
          </div>

          <!-- pagination links -->
          {{-- <div class="col-sm-12">
            <div class="center-align">
              <ul class="list-unstyled pagination">
                <li class="prev">
                  <a href=""><i class="fas fa-long-arrow-alt-right"></i></a>
                </li>
                <li><span>1-7</span></li>
                <li class="next"><a href=""><i
                       class="fas fa-long-arrow-alt-left"></i></a></li>
              </ul>
            </div>
          </div> --}}
        </div>
      </div>
    </div>
  </section>

  @include('front.client.layouts.parts.section-footer-app')
@endsection
