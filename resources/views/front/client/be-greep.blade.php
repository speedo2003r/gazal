@extends('front.client.layouts.master')
@section('content')
  <section class="all-forms">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="my-form foot-form">
            <div class="top-logo">
              <img src="{{ client_path() }}/images/icons/surface1.png">
              <h4>{{ awtTrans('كن قريب') }}</h4>
            </div>
            <form role="form" method="post" action="{{ route('client.storeBeGreep') }}">
              <!-- begreep-->
              <div class="form-group">
                <div class="upload-area">
                  <div class="file-upload-wrapper">
                    <input type="file" name="avatar" class="file-upload-native"
                           accept="image/*">
                    <input type="text" disabled="" placeholder="upload image"
                           class="file-upload-text">
                    <span class="file-upload-text icon"><i
                         class="fa fa-camera"></i></span>
                  </div>
                  <div class="preview img-wrapper"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('الاسم') }}" name="name">
              </div>
              <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('رقم الجوال') }}" name="phone">
              </div>
              <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="{{ awtTrans('رقم جوال للطوارئ') }}" name="phone2">
              </div>
              <div class="form-group">
                <input type="email" class="form-control"
                       placeholder="{{ awtTrans('البريد الالكتروني') }}" name="email">
              </div>
              <div class="form-group" title="{{ awtTrans('تاريخ الميلاد') }}">
                <input class="form-control" id="datepicker"
                       placeholder="{{ awtTrans('تاريخ الميلاد') }}" name="birth_date"
                       type="date" />
                {{-- <i class="fas fa-calendar-week"></i> --}}
              </div>

              {{-- <div class="form-group">
                <div class="custom-select">
                  <select name="category_id">
                    <option value="">{{ awtTrans('اختر نوع النشاط') }}</option>
                    @foreach ($mainCategories as $category)
                      <option value="{{ $category->id }}">
                        {{ $category->title }}
                      </option>
                    @endforeach
                  </select>
                </div>
              </div> --}}

              <!-- begreep2-->
              <div class="form-group">
                <div class="custom-select">
                  <select name="city_id">
                    <option value="">{{ awtTrans('اختر المدينة') }}</option>
                    @foreach ($cities as $city)
                      <option value="{{ $city->id }}">{{ $city->title }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="custom-select">
                  <select name="car_type_id">
                    <option value="">نوع مركبة التوصيل</option>
                    @foreach ($carTypes as $carType)
                      <option value="{{ $carType->id }}">{{ $carType->title }}
                      </option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="file-upload">
                  <div class="file-select">
                    <div class="file-select-name noFile">{{ awtTrans('رخصة المركبة') }}
                    </div>
                    <input type="file" name="licence_car_image" class="chooseFile"
                           accept="image/*">
                    <i class="fas fa-upload"></i>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="file-upload">
                  <div class="file-select">
                    <div class="file-select-name noFile">{{ awtTrans('رخصة القيادة') }}
                    </div>
                    <input type="file" name="licence_image" class="chooseFile"
                           accept="image/*">
                    <i class="fas fa-upload"></i>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="file-upload">
                  <div class="file-select">
                    <div class="file-select-name noFile">
                      {{ awtTrans('صورة المركبة من الخلف') }}</div>
                    <input type="file" name="car_back_image" class="chooseFile"
                           accept="image/*">
                    <i class="fas fa-upload"></i>
                  </div>
                </div>
              </div>

              <!-- password-->
              {{-- <div class="form-group">
                <input type="password" class="form-control"
                       placeholder="{{ awtTrans('كلمة المرور') }}" name="password">
              </div>
              <div class="form-group">
                <input type="password" class="form-control"
                       placeholder="{{ awtTrans('تأكيد كلمة المرور') }}" name="password_confirmation">
              </div> --}}
              <div class="center-align">
                <button class="stand-link btnSubmitForm">
                  <span>{{ awtTrans('ارسال الطلب') }}</span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
