<?php

return [

  # order status
  '0'  => 'WAITING',
  '1'  => 'ACCEPTED',
  '2'  => 'PREPARED',
  '3'  => 'DELEGATE ACCEPT',
  '4'  => 'ARRIVED TO PROVIDER',
  '5'  => 'GIVE TO DELEGATE',
  '6'  => 'ON WAY',
  '7'  => 'ARRIVED TO CLIENT',
  '8'  => 'FINISH',
  '9'  => 'REFUSED',
  '10' => 'CANCELED',

  #
];
