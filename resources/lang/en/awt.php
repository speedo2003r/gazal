<?php

return
[
	'أنواع السيارات' => 'Types of cars',
	'اضافة نوع سياره' => 'Add a car type',
	'حذف المحدد' => 'Delete specified',
	'الاسم بالعربي' => 'Name in Arabic',
	'الاسم بالانجليزي' => 'Name of English',
	'تعديل نوع سياره' => 'Modify the car type',
	'ملف Excel' => 'Excel file',
	'نسخ' => 'Copied',
	'طباعه' => 'Print',
	'ليست هناك بيانات متاحة في الجدول' => 'There are no data available in the table',
	'جارٍ التحميل...' => 'loading...',
	'أظهر _MENU_ مدخلات' => 'Show _menu_ input',
	'لم يعثر على أية سجلات' => 'No records were found',
	'إظهار' => 'show',
	'إلى' => 'to me',
	'من أصل' => 'Out of',
	'مدخل' => 'entrance',
	'يعرض 0 إلى 0 من أصل 0 سجل' => 'Displays 0 to 0 out of 0 record',
	'منتقاة من مجموع' => 'Selected from total',
	'مُدخل' => 'input',
	'ابحث:' => 'Search:',
	'الأول' => 'the first',
	'السابق' => 'the previous',
	'التالي' => 'the following',
	'الأخير' => 'the last one',
	': تفعيل لترتيب العمود تصاعدياً' => ': Activation to arrange the column',
	': تفعيل لترتيب العمود تنازلياً' => ': Activation to arrange column descending',
	'أظهر' => 'show up',
	'مدخلات' => 'Input',
	'العملاء' => 'Customer',
	' اضافة عميل' => 'Add a client',
	'ارسال اشعارات للجميع' => 'Send notices for everyone',
	'إرسال للمحفظه' => 'Send to the purse',
	'القيمه' => 'the value',
	'إرسال' => 'send',
	'إرسال أشعار' => 'Send notice',
	'المتجر' => 'the shop',
	'عنوان الرساله عربي' => 'The title of the message is an Arab',
	'عنوان الرساله انجليزي' => 'The title of the publication of English',
	'الرسالة بلعربي' => 'Letter',
	'اكتب رسالتك ...' => 'Type your message ...',
	'الرسالة بالانجليزي' => 'Letter in English',
	'تعديل عميل' => 'Adjust a client',
	'تاريخ الميلاد' => 'date of birth',
	'الجنس' => 'Sex',
	'ذكر' => 'Male',
	'أنثي' => 'feminine',
	'اعادة كلمة المرور' => 'Re-password',
	'اضافة عميل' => 'Add a client',
	'اضافة منتج جديد' => 'Add a new product',
	'اظهار المنتج' => 'Product Show',
	'اضافة صوره' => 'add a picture',
	'عربي' => 'Arabic',
	'انجليزي' => 'Einglish',
	'الاسم' => 'The name',
	'السعر' => 'the price',
	'القسم الفرعي' => 'Sub-section',
	'قسم فرعي' => 'Subscription',
	'بيانات أخري' => 'Other data',
	'حذف' => 'Delete',
	'مرحبا' => 'welcome',
	'تسجيل الخروج' => 'sign out',
	'لوحة الاداره' => 'Management panel',
	'تأكيد الحذف' => 'Confirm deletion',
	'هل انت متاكد من عملية الحذف' => 'are sure of the deleting process',
	'الغاء' => 'Cancellation',
	'الأعضاء' => 'Members',
	'المندوبين' => 'Delegates',
	'مقدمي الخدمه' => 'Service providers',
	'الفروع' => 'Branches',
	'المديرين' => 'Managers',
	'الأقسام' => 'sections',
	'الدول' => 'Countries',
	'المدن' => 'the cities',
	'تسجيل الدخول' => 'sign in',
	'الرئيسيه' => 'Main',
	'قائمة الصلاحيات' => 'Power List',
	'اضافة صلاحيه' => 'Add your validity',
	'تمكين اضافة صلاحيه' => 'Enable additional',
	'تعديل صلاحيه' => 'Adjustable access',
	'تمكين تعديل صلاحيه' => 'Enable an adjustment',
	'حذف صلاحيه' => 'Deletion',
	'الاعدادات' => 'Settings',
	'تحديث الاعدادات' => 'Update settings',
	'اضافه وسائل التواصل' => 'Add media communication',
	'تحديث وسائل التواصل' => 'Update media communication',
	'الاعدادت الرئيسيه' => 'Main preparation',
	'حذف نوع سياره' => 'Delete a car type',
	'السمات الأساسيه للمنتج' => 'Basic features of the product',
	'اضافة سمه للمنتجات' => 'Add a name for products',
	'تعديل سمه للمنتجات' => 'Modify a name for products',
	'حذف سمه للمنتجات' => 'Delete a name for products',
	'قائمة التقييمات الاساسيه' => 'List of basic assessments',
	'اضافة مسمي تقييم' => 'Add a names evaluation',
	'تعديل مسمي تقييم' => 'Edit evaluation',
	'حذف مسمي تقييم' => 'Delete evaluation',
	'ادارة الاحصائيات' => 'Management Statistics',
	'الزيارات' => 'Visits',
	'الأكثر طلبا' => 'the most wanted',
	'السائقين' => 'Drivers',
	'جلب البيانات' => 'Bring data',
	'المستخدمين' => 'Users',
	' المشرفين' => 'Supervisors',
	'اضافة مشرف' => 'Add administrator',
	' تعديل مشرف' => 'Modify Musharraf',
	'حذف مشرف' => 'Remove overlooking',
	' مقدمي الخدمه' => 'Service providers',
	'اضافة مقدم خدمه' => 'Add a service provider',
	'تعديل مقدم خدمه' => 'Adjustable service',
	'حذف مقدم خدمه' => 'Delete service provider',
	'معرض الصور للمقدم' => 'Photo Gallery',
	'حفظ معرض الصور للمقدم' => 'Save photo gallery',
	'الأقسام للبائع' => 'Departments Seller',
	'حفظ الأقسام للبائع' => 'Save sections',
	'فروع البائع' => 'Seller branches',
	'حفظ فروع البائع' => 'Save the seller branches',
	'تعديل فروع البائع' => 'Adjust the seller branches',
	'حذف فروع البائع' => 'Delete the seller branches',
	'حفظ مواعيد الفرع' => 'Save section dates',
	'حذف مواعيد الفرع' => 'Delete section dates',
	' العملاء' => 'Customer',
	'حذف عميل' => 'Delete a client',
	'اضافه الي المحفظه' => 'Add to the wallet',
	' المندوبين' => 'Delegates',
	'اضافة مندوب' => 'Add a delegate',
	'تعديل مندوب' => 'Adjust a delegate',
	'حذف مندوب' => 'Delete a delegate',
	'معرض الصور للمندوب' => 'Photo Gallery of Delegate',
	'حفظ معرض الصور للمندوب' => 'Save the photo gallery for the delegate',
	'ارسال اشعارات' => 'Send notices',
	'ارسال اشعارات للعملاء' => 'Send notices for customers',
	'تغيير الحاله' => 'Change the situation',
	'المنتجات' => 'Products',
	'اضافة المنتج' => 'Add the product',
	'تعديل المنتج' => 'Product modification',
	'تحديث المنتج' => 'Product Update',
	'تغيير حالة المنتج' => 'Change the product status',
	'حذف الصور' => 'Delete images',
	'حذف المنتج' => 'Delete the product',
	'حذف المنتج من الجدول' => 'Delete the product from the table',
	'حفظ اضافه' => 'Save add',
	'حذف اضافه' => 'Remove additional',
	'حذف السمه' => 'Delete the name',
	'حذف تفاصيل السمه' => 'Delete the details of the name',
	'حفظ السمات' => 'Save features',
	'حذف السمه الفرعيه' => 'Delete the submerged',
	'الصوره الرئيسيه' => 'Main picture',
	'اضافة صور' => 'Add photos',
	'حفظ المنتج' => 'Save',
	'أقسام المتجر' => 'Store sections',
	' الطلبات' => 'Orders',
	'مشاهدة طلب' => 'View request',
	'حذف طلب' => 'Deleted',
	'الحسابات العامه' => 'General accounts',
	'حسابات مقدمي الخدمه' => 'Accounts of service providers',
	'قبول سداد المقدم' => 'Accept payment',
	'رفض سداد المقدم' => 'Rejection of payment',
	'حذف حسابات مقدم الخدمه' => 'Delete service provider',
	'حسابات المندوبين' => 'Accounts of delegates',
	'قبول سداد المندوب' => 'Acceptance of delegate',
	'رفض سداد المندوب' => 'Refuse to pay the delegate',
	'حذف حسابات المندوبين' => 'Delete delegates accounts',
	' الأقسام' => 'sections',
	'عرض شجري للاقسام' => 'Reburray cleanser',
	'اضافة قسم' => 'Add Category',
	'تعديل قسم' => 'Edit section',
	'حذف قسم' => 'Delete section',
	' البنرات' => 'Banners',
	'البنرات المتحركه' => 'Mobile animation',
	'اضافة بنر' => 'Add a banner',
	'تعديل بنر' => 'Edit Banner',
	'حذف بنر' => 'Remove banner',
	'تنشيط' => 'Energizing',
	'البنرات الثابته' => 'Fixed Banners',
	' العروض' => 'Presentation',
	'اضافة عرض' => 'Add a display',
	'تعديل عرض' => 'Edit display',
	'حذف عرض' => 'Delete display',
	'الأقسام الفرعيه' => 'Penthouse sections',
	'اضافة قسم فرعي' => 'Add a subcategory',
	'تعديل قسم فرعي' => 'Edit subcategory',
	'حذف قسم فرعي' => 'Delete subcategory',
	'الفئات' => 'Categories',
	'اضافة فئه' => 'Add a category',
	'تعديل فئه' => 'Adjustable class',
	'حذف فئه' => 'Delete a class',
	' الكوبونات' => 'Coupons',
	'اضافة الكوبون' => 'Add Coupon',
	'تعديل الكوبون' => 'Modify Coupon',
	'حذف الكوبون' => 'Delete coupon',
	' ادارة الدول والمدن' => 'Countries and Cities',
	'اضافة دوله' => 'Add its country',
	'تعديل دوله' => 'Modify its country',
	'حذف دوله' => 'Delete its state',
	' المدن' => 'Cities',
	'اضافة مدينه' => 'Add the city',
	'تعديل مدينه' => 'Modification of the city',
	'حذف مدينه' => 'Delete the city',
	' الصفحات الرئيسيه' => 'Main page',
	'تعديل صفحه' => 'Modify page',
	' اقتراحات' => 'Suggestions',
	'حذف اقتراح' => 'Delete suggestion',
	' طلب انضمام مقدم' => 'Request for accession',
	'حذف انضمام' => 'Delete joining',
	' تواصل معنا' => 'Connect with us',
	'تعديل تواصل معنا' => 'Edit contact with us',
	'حذف تواصل معنا' => 'Delete contact with us',
	' تقارير لوحة التحكم' => 'Control panel reports',
	'حذف تقرير' => 'Delete Report',
	' الترجمات' => 'Translations',
	'احضار ترجمه' => 'Bring translated',
	'حفظ ترجمه' => 'Save translation',
	'بنرات متحركه' => 'Animated Banners',
	'تعديل البنر' => 'Adjust the banner',
	'الصوره' => 'Picture',
	'متجر' => 'a store',
	'منتج' => 'Producer',
	'المدينه' => 'City',
	'اختر مدينه' => 'Choose a city',
	'القسم' => 'Section',
	'اختر القسم' => 'Select Section',
	'المسمي بالعربي' => 'Named in arabic',
	'المسمي بالانجليزي' => 'Named in english',
	'المتاجر' => 'Shops',
	'بنرات ثابته' => 'Fixed Banners',
	'من' => 'from',
	'الي' => 'to me',
	'Add your validity' => 'Add your validity',
	'Enable additional' => 'Enable additional',
	'Adjustable access' => 'Adjustable access',
	'Enable an adjustment' => 'Enable an adjustment',
	'Deletion' => 'Deletion',
	'Settings' => 'Settings',
	'Update settings' => 'Update settings',
	'Add media communication' => 'Add media communication',
	'Update media communication' => 'Update media communication',
	'Types of cars' => 'Types of cars',
	'Add a car type' => 'Add a car type',
	'Modify the car type' => 'Modify the car type',
	'Delete a car type' => 'Delete a car type',
	'Basic features of the product' => 'Basic features of the product',
	'Add a name for products' => 'Add a name for products',
	'Modify a name for products' => 'Modify a name for products',
	'Delete a name for products' => 'Delete a name for products',
	'List of basic assessments' => 'List of basic assessments',
	'Add a names evaluation' => 'Add a names evaluation',
	'Edit evaluation' => 'Edit evaluation',
	'Delete evaluation' => 'Delete evaluation',
	'Customer' => 'Customer',
	'Visits' => 'Visits',
	'the most wanted' => 'the most wanted',
	'Drivers' => 'Drivers',
	'Bring data' => 'Bring data',
	'Supervisors' => 'Supervisors',
	'Add administrator' => 'Add administrator',
	'Modify Musharraf' => 'Modify Musharraf',
	'Remove overlooking' => 'Remove overlooking',
	'Add a client' => 'Add a client',
	'Adjust a client' => 'Adjust a client',
	'Delete a client' => 'Delete a client',
	'Add to the wallet' => 'Add to the wallet',
	'Delegates' => 'Delegates',
	'Add a delegate' => 'Add a delegate',
	'Adjust a delegate' => 'Adjust a delegate',
	'Delete a delegate' => 'Delete a delegate',
	'Photo Gallery of Delegate' => 'Photo Gallery of Delegate',
	'Save the photo gallery for the delegate' => 'Save the photo gallery for the delegate',
	'Service providers' => 'Service providers',
	'Add a service provider' => 'Add a service provider',
	'Adjustable service' => 'Adjustable service',
	'Delete service provider' => 'Delete service provider',
	'Seller branches' => 'Seller branches',
	'Save the seller branches' => 'Save the seller branches',
	'Adjust the seller branches' => 'Adjust the seller branches',
	'Delete the seller branches' => 'Delete the seller branches',
	'Save section dates' => 'Save section dates',
	'Delete section dates' => 'Delete section dates',
	'Departments Seller' => 'Departments Seller',
	'Save sections' => 'Save sections',
	'Photo Gallery' => 'Photo Gallery',
	'Save photo gallery' => 'Save photo gallery',
	'Send notices' => 'Send notices',
	'Send notices for customers' => 'Send notices for customers',
	'Change the situation' => 'Change the situation',
	'Save' => 'Save',
	'Product Update' => 'Product Update',
	'Add the product' => 'Add the product',
	'Product modification' => 'Product modification',
	'Delete the product' => 'Delete the product',
	'Delete the product from the table' => 'Delete the product from the table',
	'Delete images' => 'Delete images',
	'Add photos' => 'Add photos',
	'Save add' => 'Save add',
	'Remove additional' => 'Remove additional',
	'Main picture' => 'Main picture',
	'Delete the details of the name' => 'Delete the details of the name',
	'Delete the name' => 'Delete the name',
	'Save features' => 'Save features',
	'Store sections' => 'Store sections',
	'Change the product status' => 'Change the product status',
	'View request' => 'View request',
	'Deleted' => 'Deleted',
	'Accounts of service providers' => 'Accounts of service providers',
	'Accept payment' => 'Accept payment',
	'Rejection of payment' => 'Rejection of payment',
	'Accounts of delegates' => 'Accounts of delegates',
	'Delete delegates accounts' => 'Delete delegates accounts',
	'Acceptance of delegate' => 'Acceptance of delegate',
	'Refuse to pay the delegate' => 'Refuse to pay the delegate',
	'sections' => 'sections',
	'Reburray cleanser' => 'Reburray cleanser',
	'Addition' => 'Addition',
	'Edit section' => 'Edit section',
	'Delete section' => 'Delete section',
	'Penthouse sections' => 'Penthouse sections',
	'Add a subcategory' => 'Add a subcategory',
	'Edit subcategory' => 'Edit subcategory',
	'Delete subcategory' => 'Delete subcategory',
	'Categories' => 'Categories',
	'Add a category' => 'Add a category',
	'Adjustable class' => 'Adjustable class',
	'Delete a class' => 'Delete a class',
	'Mobile animation' => 'Mobile animation',
	'Add a banner' => 'Add a banner',
	'Edit Banner' => 'Edit Banner',
	'Remove banner' => 'Remove banner',
	'Energizing' => 'Energizing',
	'Fixed Banners' => 'Fixed Banners',
	'Presentation' => 'Presentation',
	'Add a display' => 'Add a display',
	'Edit display' => 'Edit display',
	'Delete display' => 'Delete display',
	'Coupons' => 'Coupons',
	'Add Coupon' => 'Add Coupon',
	'Modify Coupon' => 'Modify Coupon',
	'Delete coupon' => 'Delete coupon',
	'Countries' => 'Countries',
	'Add its country' => 'Add its country',
	'Modify its country' => 'Modify its country',
	'Delete its state' => 'Delete its state',
	'Cities' => 'Cities',
	'Add the city' => 'Add the city',
	'Modification of the city' => 'Modification of the city',
	'Delete the city' => 'Delete the city',
	'Main page' => 'Main page',
	'Modify page' => 'Modify page',
	'Suggestions' => 'Suggestions',
	'Delete suggestion' => 'Delete suggestion',
	'Request for accession' => 'Request for accession',
	'Delete joining' => 'Delete joining',
	'Connect with us' => 'Connect with us',
	'Edit contact with us' => 'Edit contact with us',
	'Delete contact with us' => 'Delete contact with us',
	'Control panel reports' => 'Control panel reports',
	'Delete Report' => 'Delete Report',
	'Bring translated' => 'Bring translated',
	'Save translation' => 'Save translation',
	'اعدادت التطبيق' => 'Prepare the application',
	'مواقع التواصل' => 'communication Web-sites',
	'اعدادات الخريطة' => 'Map settings',
	'اعدادات البحث' => 'Search settings',
	'اعدادات الدعم' => 'Support settings',
	'اعدادات الدفع' => 'Payment settings',
	'اسم التطبيق' => 'Application name',
	'البريد الإلكتروني' => 'E-mail',
	'واتساب' => 'whatsapp',
	'الجوال' => 'cell phone',
	'رقم الدعم للمندوبين' => 'Support number for delegates',
	'رقم الدعم لمقدمي الخدمه' => 'Support number for service providers',
	'الضريبه' => 'Tax',
	'سعر الشحن لكل كيلومتر' => 'Shipping price per kilometer',
	'سعر الشحن المبدئي' => 'Initial shipping price',
	'مفتاح الخريطة' => 'map key',
	'لوجو' => 'Leo',
	'حفظ' => 'Maintained',
	'اكتب موقعك' => 'Write your site',
	'الوصف' => 'the description',
	'الكلمات المفتاحية' => 'key words',
	'النوع' => 'Type',
	'اسم المستخدم' => 'user name',
	'الرقم السري' => 'password',
	'الايميل المرسل' => 'Email Sender',
	'الاسم المرسل' => 'Sender name',
	'البورت' => 'Port',
	'الهوست' => 'Ior',
	'التشفير' => 'Encryption',
	'اضافة وسيله تواصل' => 'Add and communicate',
	'مسمي الوسيله' => 'Media',
	'حدث خطا أتناء تحديد الموقع' => 'Site intake occurred',
	'اضافة سمه' => 'Add a name',
	'تعديل سمه' => 'Modify a name',
	'قائمة مسميات التقييمات' => 'List of evaluations',
	'نوع المستخدم' => 'User type',
	'اختر' => 'Choose',
	'مقدم خدمه' => 'Serving',
	'مندوب' => 'representative',
	'عميل' => 'Client',
	'عدد الطلبات' => 'Number of applications',
	'البريد الالكتروني' => 'E-mail',
	'رقم الجوال' => 'Mobile number',
	'الحاله' => 'the condition',
	'تاريخ التسجيل' => 'date of registration',
	'العملاء الأكثر دفعا الكترونيا' => 'The most paid customer electronically',
	'ملف' => 'a file',
	'ملف طباعه' => 'File printing',
	'اختيار' => 'Selection',
	'العملاء الأكثر طلبا' => 'Most customer customers',
	'الزيارات من خلال' => 'Visits through',
	'الزيارات من خلال الدول' => 'Visits through countries',
	'الزيارات من خلال المدن' => 'Visits through cities',
	'الأيام' => 'the days',
	'العدد' => 'the number',
	'عدد الزيارات من تطبيق' => 'Number of visits from an application',
	'الأيام الأكثر طلبا' => 'The most requested days',
	'طلبات' => 'orders',
	'الساعات الأكثر طلبا' => 'The most requested watches',
	'نشط' => 'active',
	'عدد المندوبين' => 'Number of delegates',
	'عدد المندوبين النشطين' => 'Number of active delegates',
	'عدد المندوبين الغير نشطين' => 'Number of non-active delegates',
	'عدد المندوبين الأكثر قبولا للطلبات' => 'Number of most acceptable delegates for orders',
	'الدوله' => 'Country',
	'إرسال إشعار' => 'Send notice',
	'اضافة رصيد' => 'Add credit',
	'الهاتف' => 'the phone',
	'المحفظه' => 'Portfolio',
	'التحكم' => 'control',
	'ابحث' => 'Search',
	' تفعيل لترتيب العمود تصاعدياً' => 'Activate to arrange the column',
	'معرض الصور والمستندات' => 'Photo Gallery and documents',
	'المديونيه' => 'Medion',
	'المناديب' => 'Call',
	'الرسالة' => 'the message',
	'نوع السياره' => 'Vehicle Type',
	'العنوان' => 'Address',
	'العموله قيمه أو نسبه' => 'Employment or accounts',
	'قيمه' => 'value',
	'نسبه' => 'Percentage of',
	'العموله' => 'Common',
	'الموافقه' => 'Approval',
	'الاقسام' => 'sections',
	'الصور والمستندات' => 'Images and documents',
	'اضافة مقدم خدمه جديد' => 'Add new service provider',
	'اسم المتجر بالعربي' => 'The name of the store in Arabic',
	'اسم المتجر بالانجليزي' => 'The name of the shop in English',
	'الفئه' => 'Class',
	'رقم السجل التجاري' => 'Commercial Registration No',
	'رقم الحساب البنكي' => 'Bank account number',
	'اغلاق' => 'Close',
	'عرض الأقسام' => 'View sections',
	'عرض المستندات' => 'View documents',
	'عرض الفروع' => 'View branches',
	'رقم الطلب' => 'order number',
	'سعر الشحن' => 'Shipping Rate',
	'الاجمالي' => 'Total',
	'الطلبات' => 'orders',
	'تفاصيل الطلب' => 'Demand details',
	'حالة الطلب' => 'Order status',
	'أسم العميل' => 'customer name',
	'جوال العميل' => 'Customer mobile',
	'طريقة الدفع' => 'Payment method',
	'ريال سعودي' => 'SR',
	'وحده' => 'lonliness',
	'ملاحظات' => 'Notes',
	'لا يوجد ملاحظات' => 'There is no notes',
	'نشط / غير نشط' => 'Active / inactive',
	'تاريخ الاضافه' => 'Date Added',
	'صوره' => 'picture',
	'تعديل القسم' => 'Modify the section',
	'الأقسام الرئيسيه' => 'Main sections',
	' اضافة فئه' => 'Add a category',
	'اسم العميل' => 'customer name',
	'اسم المتجر' => 'The name of the store',
	'تاريخ الطلب' => 'The date of application',
	'عمولة الموقع' => 'Commission site',
	'وسيلة الدفع' => 'Payment',
	'تفاصيل' => 'details',
	'مدفوعات مقدمي الخدمه' => 'Payments of service providers',
	'اسم البنك' => 'Bank name',
	'اسم صاحب الحساب' => 'Account Holder is Name',
	'رقم الحساب' => 'account number',
	'المبلغ المدفوع' => 'The amount paid',
	'صورة الايصال' => 'Image of the receipt',
	'تأكيد القبول' => 'Confirm acceptance',
	'هل انت متاكد من قبول الدفع' => 'Are you sure to accept payment',
	'تأكيد' => 'Confirm',
	'تأكيد الرفض' => 'Confirm rejection',
	'هل انت متاكد من الرفض' => 'Are you sure of rejection?',
	'كود الخصم' => 'Discount code',
	'قيمة الخصم' => 'discount value',
	'عدد الاستخدامات' => 'Number of uses',
	'كوبونات الخصم' => 'Discount coupons',
	' اضافة كوبون خصم' => 'Add coupon discount',
	'تعديل كوبون' => 'Coupon adjustment',
	'رمز الكوبون' => 'Coupon code',
	'تحديد الفئه' => 'Select the category',
	'عام' => 'general',
	'تحديد النوع' => 'Select type',
	'نسبه من قيمة المشتريات' => 'Purchasing value',
	'قيمه ثابته من قيمة الفاتورة شامل قيمة التوصيل' => 'The value of the invoice value is comprehensive',
	'الحد الأدني للتخفيض' => 'Minimum reduction',
	'تاريخ بداية الكوبون' => 'Coupon start date',
	'تاريخ نهاية الكوبون' => 'Coupon end date',
	' اضافة دوله' => 'Add its country',
	'تعديل المدينه' => 'Modification of the city',
	'الصفحات الاساسيه' => 'Basic pages',
	'تعديل الصفحه' => 'edit page',
	'التفاصيل بالعربي' => 'Details in Arabic',
	'التفاصيل بالانجليزي' => 'Details in English',
	'وقت التسجيل' => 'Registration time',
	'الاقتراحات' => 'Suggestions',
	'اسم النشاط' => 'Name of activity',
	'هاتف' => 'phone',
	'بريد الكتروني' => 'e-mail',
	'طلب انضمام مقدم خدمه' => 'Request for joining service',
	'نوع النشاط' => 'Type of activity',
	'حالة الرسالة' => 'Message status',
	'تواصل معنا' => 'Connect with us',
	'تقارير لوحة التحكم' => 'Control panel reports',
    'اكسيل'=>'Excel',
    "الارشيف"=>"History",
    "إنشاء فرع"=>"Create branch",
];
