<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAoDP9tOY:APA91bGmMgvlmSDo8n9PZkHESKUNKsTc4G4oykvsvm-_IVPnwozImrj1MMUMuJf_63EuIV20Ya19KTD0Dsp5anGu0tkVLLOoQn4g-wHo5ATDdHjBO99p7Z7QSO-24vIWSGEEftVSBaU_'),
        'sender_id' => env('FCM_SENDER_ID', '688067032294'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
