<?php

use Illuminate\Support\Facades\Route;

Route::redirect('/', 'client/home');
Route::get('change-language/{lang}', 'Controller@changeLanguage')->name('changeLanguage');
Route::get('session-uuid/{uuid}', 'Controller@storeUuidInSession')->name('storeUuidInSession');


Route::group(['middleware' => ['web-lang', 'check-user-type:client'], 'prefix' => 'client', 'namespace' => 'Client', 'as' => 'client.'], function () {

  Route::get('about-us', 'PageController@aboutUs')->name('aboutUs');
  Route::get('contact-us', 'PageController@contactUs')->name('contactUs');
  Route::get('privacy-policy', 'PageController@privacyPolicy')->name('privacyPolicy');
  Route::get('usage-policy', 'PageController@usagePolicy')->name('usagePolicy');
  Route::get('faqs', 'PageController@faqs')->name('faqs');

  Route::get('home', 'HomeController@home')->name('home');
  Route::get('categories', 'HomeController@categories')->name('categories');
  Route::get('filter/{category_id}', 'HomeController@filter')->name('filter');
  Route::get('provider-details/{branch_id}', 'HomeController@providerDetails')->name('providerDetails');

  Route::get('join-greep', 'HomeController@getJoinGreep')->name('getJoinGreep');
  Route::post('join-greep', 'HomeController@storeJoinGreep')->name('storeJoinGreep');

  Route::get('be-greep', 'HomeController@getBeGreep')->name('getBeGreep');
  Route::post('be-greep', 'HomeController@storeBeGreep')->name('storeBeGreep');

  Route::get('be-partner', 'HomeController@getBePartner')->name('getBePartner');
  Route::post('be-partner', 'HomeController@storeBePartner')->name('storeBePartner');

  Route::get('suggestions', 'HomeController@suggestions')->name('suggestions');
  Route::post('suggestions', 'HomeController@storeSuggest')->name('storeSuggest');

  Route::post('sign-up', 'AuthController@register')->name('register');
  Route::post('active-code', 'AuthController@activation')->name('activation');
  Route::post('sign-in', 'AuthController@login')->name('login');
  Route::get('sign-out', 'AuthController@logout')->name('logout');
  Route::post('forget-password', 'AuthController@forgetPassword')->name('forgetPassword');
  Route::post('forget-password-update', 'AuthController@forgetPasswordUpdate')->name('forgetPasswordUpdate');

  // todo: move under middleware
  Route::get('prev-orders', 'UserController@prevOrders')->name('prevOrders');
  Route::get('favourite', 'UserController@favourite')->name('favourite');
  Route::get('wallet', 'UserController@wallet')->name('wallet');
  Route::get('create-payment-card', 'UserController@createPaymentCard')->name('createPaymentCard');
  Route::group(['middleware' => 'auth'], function () {
    Route::get('order-summary', 'UserController@orderSummary')->name('orderSummary');
    Route::post('profile', 'AuthController@profile')->name('profile');
  });

});

Route::group(['prefix' => 'adminLangPage', 'namespace' => 'FrontAdmin', 'as' => 'adminFront.'], function () {

  Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@index')->name('index');
    Route::resource('users', 'UserController');
    Route::resource('contents', 'contentController');
    Route::resource('sliders', 'sliderController');
    Route::resource('reviews', 'reviewController');
    Route::resource('statements', 'statementController');
    Route::resource('fqs', 'fqsController');
    Route::resource('blogs', 'blogController');
    Route::resource('contactDetails', 'contactDetailsController');
    Route::resource('aboutUs', 'aboutUsController');
    Route::resource('aboutStatements', 'aboutStatementController');
  });

  Route::post('/logout', 'DashboardController@logout')->name('logout');
  Route::get('/login', 'DashboardController@showLoginForm')->name('login');
  Route::post('/login', 'DashboardController@login')->name('post.login');
  Route::get('/home', 'HomeController@index')->name('home');
});
