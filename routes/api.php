<?php

use Illuminate\Support\Facades\Route;

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

Route::any('test-fcm/{user_id}', 'Api\HomeController@testFcm');

Route::get('update-order-event/{order_id}', 'Api\OrderController@updateOrderEvent');
Route::get('update-location-event', 'Api\OrderController@updateLocationEvent');

Route::group(['middleware' => ['auth-check', 'api-lang'], 'namespace' => 'Api'], function () {
  Route::any('home-add-address', 'HomeController@homeAddAddress');
  Route::any('categories', 'SettingController@Categories');
  Route::any('all_countries', 'SettingController@allCountriesWithFlags');
  Route::any('cities', 'SettingController@getCities');
  Route::any('offers', 'SettingController@offers');
  Route::any('search', 'HomeController@search');
  Route::any('filter', 'HomeController@filter');
  Route::any('home', 'HomeController@home');
  Route::any('provider-contact', 'ProviderController@contact');

  Route::any('site-data', 'SettingController@SiteData');

  Route::any('pray-time', 'PrayTimeController@prayTime');

  Route::any('weather', 'WeatherController@weather');

  Route::any('join-us', 'SettingController@joinUs');

  Route::any('about', 'PageController@about');

  Route::any('single-provider', 'ProviderController@singleProvider');
  Route::any('select-category-provider', 'ProviderController@selectCategoryProvider');
  Route::any('single-item', 'ItemController@SingleItem');

  Route::any('check-point-map', 'HomeController@checkPointMap');
  Route::any('check-zone-name', 'HomeController@checkZoneName');

  Route::any('zone-branches', 'HomeController@getZoneBranches');
  Route::any('check-branch-zone-point', 'HomeController@checkBranchZonePoint');

  /***************************** CartController start *****************************/
  Route::any('add-to-cart', 'CartController@AddToCart');
  Route::any('cart', 'CartController@Cart');
  Route::any('decrease', 'CartController@decreaseCount');
  Route::any('increase', 'CartController@increaseCount');
  Route::any('write-note', 'CartController@writeNote');
  Route::any('add-coupon', 'CartController@addCoupon');
  Route::any('change-order-type', 'CartController@changeOrderType');
  Route::any('change-order-address', 'CartController@changeOrderAddress');
  Route::any('postponed-order', 'CartController@postponedOrder');

  Route::any('rating-list', 'RatingController@ratingList');

  Route::any('get-cities', 'SettingController@getCities');

  Route::any('get-cars', 'SettingController@getCars');

  Route::any('suggest', 'SettingController@suggest'); // with token or without
  /***************************** CartController End *****************************/

  /***************************** AuthController Start *****************************/
  Route::post('user/sign-up', 'AuthController@UserRegister')->middleware('fix-phone');
  Route::any('delegate/sign-up', 'AuthController@DelegateRegister')->middleware('fix-phone');

  # Code For Active User
  Route::any('reset-password', 'AuthController@resetPassword')->middleware('fix-phone');
  Route::any('send-active-code', 'AuthController@sendActiveCode')->middleware('fix-phone');
  Route::any('active-code', 'AuthController@Activation')->middleware('fix-phone');
  Route::any('resend-code', 'AuthController@ResendActiveCode')->middleware('fix-phone');

  Route::any('provider-sign-in', 'AuthController@ProviderLogin')->middleware('fix-phone');
  Route::any('sign-in', 'AuthController@Login')->middleware('fix-phone');

  Route::any('logout', 'AuthController@Logout');

  Route::any('get-shipPrice-destination-between', 'SettingController@getShipPriceDestinationBetween');

  #favorites
  Route::any('favorites', 'FavoriteController@favorites');
  Route::any('toggle-favorites', 'FavoriteController@toggleFavorites');
  Route::any('delete-favorites', 'FavoriteController@deleteFavorites');

  Route::group(['middleware' => ['jwt.verify', 'phone-activated']], function () {

    Route::any('complaint', 'SettingController@complaint');

    Route::any('change-password', 'AuthController@UpdatePassword');

    Route::any('edit-lang', 'AuthController@EditLang');

    Route::any('share-code', 'AuthController@shareCode');

    # switch notification status for receive fcm or not
    Route::any('edit-notification-status', 'AuthController@NotificationStatus');

    # User profile
    Route::any('profile', 'AuthController@ShowProfile');
    Route::any('profile/update', 'AuthController@UpdateProfile');

    # notifications
    Route::any('notifications', 'AuthController@Notifications');
    Route::any('unread-notifications-count', 'AuthController@UnreadNotifications');
    Route::delete('delete-notification', 'AuthController@deleteNotification');
    Route::any('fake-notifications/{id}', 'AuthController@Fakenotifications');

    # credits
    Route::any('get-credits', 'PaymentController@getCredits');
    Route::any('add-credit', 'PaymentController@addCredit');
    Route::any('delete-credit', 'PaymentController@deleteCredit');

    # user addresses
    Route::any('addresses', 'AuthController@Addresses');
    Route::any('add-address', 'AuthController@AddAddress');
    Route::any('edit-address', 'AuthController@EditAddress');
    Route::delete('delete-address', 'AuthController@DeleteAddress');

    # order
    Route::any('place-order', 'OrderController@placeOrder');
    Route::any('get-any-order', 'OrderController@orderAny');
    Route::any('cancel-order', 'OrderController@cancelOrder');
    Route::any('old-orders', 'OrderController@oldOrders');
    Route::any('re-order', 'CartController@reorder');
    Route::any('order-details', 'OrderController@OrderDetails');
    Route::any('get-order-status', 'OrderController@getOrderStatus');

    # rating
    Route::any('post-rate-provider', 'RatingController@postRateProvider');
    Route::any('post-rate-delegate', 'RatingController@postRateDelegate');

    # wallet
    Route::any('wallet', 'AuthController@Wallet');

    Route::group(['middleware' => ['CheckBranch']], function () {
      Route::any('home-provider', 'ProviderController@HomeProvider');
      Route::any('home-provider-sell-count', 'ProviderController@HomeProviderSellCount');
      Route::any('home-accepted-provider', 'ProviderController@HomeAcceptedProvider');
      Route::any('appear-switch', 'ProviderController@appearSwitch');
      Route::any('provider-new-orders', 'OrderController@providerNewOrders');
      Route::any('provider-single-order', 'OrderController@providerSingleOrder');
      Route::any('provider-cancel-orders', 'OrderController@providerCancelOrders');
      Route::any('provider-refused-orders', 'OrderController@providerRefusedOrders');
      Route::any('provider-postponed-orders', 'OrderController@providerPostponedOrders');
      Route::any('provider-late-orders', 'OrderController@providerLateOrders');
      Route::any('order-accept', 'OrderController@orderAccept');
      Route::any('order-refused', 'OrderController@orderRefused');
      Route::any('order-status', 'OrderController@orderStatus');
      Route::any('order-prepared', 'OrderController@orderPrepared');
      Route::any('order-provider-give-to-delegate', 'OrderController@orderProviderGiveToDelegate');

      Route::any('provider-get-delegate', 'OrderController@providerGetDelegate');

      Route::any('orders-rating', 'OrderController@ordersRating');
      Route::any('order-single-rating', 'OrderController@orderSingleRating');

      Route::any('provider-categories', 'ProviderCategoriesController@providerCategories');

      Route::any('provider-notifications', 'AuthController@providerNotifications');
    });

    Route::group(['middleware' => ['CheckDelegate']], function () {
      Route::any('delegate-home-page', 'DelegateController@delegateHomePage');
      Route::any('delegate-new-orders', 'DelegateOrderController@delegateNewOrders');
      Route::any('delegate-accept-order', 'DelegateOrderController@delegateAcceptOrder');
      Route::any('delegate-order-refuse', 'DelegateOrderController@delegateOrderRefuse');
      Route::any('delegate-order-detail', 'DelegateOrderController@delegateOrderDetails');
      Route::any('delegate-order-received-from-provider', 'DelegateOrderController@delegateOrderReceivedFromProvider');
      Route::any('delegate-order-arrived-to-client', 'DelegateOrderController@delegateOrderArrivedToClient');
      Route::any('delegate-order-delivered', 'DelegateOrderController@delegateOrderDelivered');
      Route::any('delegate-order-client-address', 'DelegateOrderController@delegateOrderClientAddress');
      Route::any('delegate-order-status', 'DelegateOrderController@delegateOrderStatus');

      Route::any('post-rate-client', 'RatingController@postRateClient');
      Route::any('delegate-clients-rate', 'RatingController@delegateClientsRate');

      Route::any('delegate-complete-orders', 'DelegateOrderController@delegateCompleteOrders');
      Route::any('delegate-current-orders', 'DelegateOrderController@delegateCurrentOrders');
      Route::any('delegate-cancel-user-order', 'DelegateOrderController@delegateCancelUserOrders');
      Route::any('delegate-refused-order', 'DelegateOrderController@delegateRefusedOrders');

      # credits
      Route::any('delegate-get-credits', 'PaymentController@DelegateGetCredits');
      Route::any('delegate-add-credit', 'PaymentController@DelegateAddCredit');
      Route::any('delegate-delete-credit', 'PaymentController@DelegateDeleteCredit');

      Route::any('delegate-wallet', 'AuthController@DelegateWallet');
    });
  });
});
