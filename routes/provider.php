<?php

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'provider', 'namespace' => 'Provider', 'as' => 'provider.'], function () {

    Route::get('login', [
        'uses'  => 'AuthController@showLoginForm',
        'title' => 'تسجيل الدخول',
    ])->name('show.login');

    Route::post('login', 'AuthController@login')->name('login');

    Route::get('register','AuthController@register')->name('register');
    Route::get('register2','AuthController@register2')->name('register2');
    Route::post('register','AuthController@postRegister')->name('postRegister');
    Route::get('active','AuthController@active')->name('active');
    Route::post('activation','AuthController@Activation')->name('activation');
    Route::get('activeCode','AuthController@activeCode')->name('activeCode');
    Route::post('sendActiveCode','AuthController@sendActiveCode')->name('sendActiveCode');
    Route::get('reset','AuthController@reset')->name('reset');
    Route::post('resetPassword','AuthController@resetPassword')->name('resetPassword');
    Route::get('forget','AuthController@forget')->name('forget');
    Route::post('checkUser','AuthController@checkUser')->name('checkUser');

    Route::get('change-language/{lang}', 'HomeController@changeLanguage')->name('change.language');

    Route::group(['middleware' => ['SellerAuth', 'admin-lang']], function () {

        /********************************* HomeController start *********************************/
        Route::get('logout','AuthController@logout')->name('logout');
        Route::get('/','HomeController@dashboard')->name('home');

        Route::get('notification','NotificationController@index')->name('notification');

        Route::get('setting','SettingController@index')->name('setting');
        Route::post('settingUpdate','SettingController@update')->name('setting.update');

//        account
        Route::get('accounts','AccountController@index')->name('accounts');
        Route::post('pay','AccountController@pay')->name('pay');

//        products
        Route::get('products','ProductController@index')->name('product');
        Route::get('products/{id}/show','ProductController@show')->name('product.show');
        Route::post('products','ProductController@store')->name('product.store');
        Route::get('products/create/index','ProductController@create')->name('product.create');
        Route::get('products/{id}','ProductController@edit')->name('product.edit');
        Route::post('products/deleteFeature', 'ProductController@deleteFeature')->name('product.deleteFeature');
        Route::post('products/files/change','ProductController@changeMain')->name('product.changeMain');
        Route::post('products/files', 'ProductController@addFile')->name('product.addFile');
        Route::post('products/delimage','ProductController@delimage')->name('product.delimage');
        Route::put('products/{id}','ProductController@update')->name('product.update');
        Route::delete('products','ProductController@destroy')->name('product.destroy');
        Route::post('products/saveFeatures', 'ProductController@saveFeatures')->name('product.saveFeatures');
        Route::post('products/saveItemTypes', 'ProductController@saveItemTypes')->name('product.saveItemTypes');
        Route::post('products/deleteType', 'ProductController@deleteType')->name('product.deleteType');
        Route::post('products/getSellerCategories', 'ProductController@getSellerCategories')->name('product.SellerCategories');
        Route::post('products/items/changeStatus', 'ProductController@changeStatus')->name('product.changeStatus');
        Route::post('products/deleteDetail', 'ProductController@deleteDetail')->name('product.deleteDetail');
        Route::post('products/deleteTypeOption', 'ProductController@deleteTypeOption')->name('product.deleteTypeOption');
//        Route::get('offers','OfferController@index')->name('offer');
//        Route::post('postOffers','OfferController@postOffers')->name('postOffers');
//        coupons
        Route::get('coupons','CouponController@index')->name('coupon');
        Route::post('coupons','CouponController@store')->name('coupon.store');
        Route::post('coupons/{id}','CouponController@update')->name('coupon.update');
        Route::delete('coupons/{id}','CouponController@destroy')->name('coupon.destroy');


        Route::get('setting','SettingController@index')->name('setting');
        Route::get('changePassword','SettingController@changePassword')->name('changePassword');
        Route::post('changePassword','SettingController@postChangePassword')->name('postChangePassword');
        Route::post('settingUpdate','SettingController@update')->name('setting.update');

//        orders

        Route::get('orders/show/{id}','OrderController@show')->name('showOrders');
        Route::post('changeCarType','OrderController@changeCarType')->name('changeCarType');
        Route::post('orderRefuse','OrderController@orderRefuse')->name('orderRefuse');
        Route::post('orderAccept','OrderController@orderAccept')->name('orderAccept');
        Route::post('orderOnWay','OrderController@orderOnWay')->name('orderOnWay');
        Route::get('orders','OrderController@index')->name('orders.index');
        Route::get('orders-history','OrderController@index')->name('orders.history');
        //Route::get('new-orders','OrderController@index')->name('newOrders');
        //Route::get('pickup-orders','OrderController@index')->name('pickupOrders');
        //Route::get('finish-orders','OrderController@index')->name('finishOrders');
        //Route::get('pending-orders','OrderController@index')->name('pendingOrders');
        //Route::get('refuse-orders','OrderController@index')->name('refuseOrders');
        Route::get('return-orders','OrderController@return')->name('returnOrders');
        Route::post('post-return-orders','OrderController@postReturnOrder')->name('postReturnOrder');
        Route::get('return-details-orders/{order}','OrderController@returnDetails')->name('returnDetails');


        //  branches
        Route::post('branch/appear/{branch}','BranchController@toggleAppear');

        Route::resources([
            'branch' => 'BranchController',
            'branch.times' => 'BranchTimesController',
            'categories' => 'CategoryController',
            'subcategories' => 'SubCategoryController',
            'employees' => 'EmployeeController',
        ]);
        Route::post('employees/{id}/status', ['as' => 'employees.status', 'uses' => 'EmployeeController@changeStatus']);

        // Reports
        Route::group(['prefix' => 'reports', 'as' => 'reports.'], function () {
            Route::get('today', ['as' => 'today', 'uses' => 'ReportController@today']);
        });

        /********************************* HomeController end *********************************/
    });
});
