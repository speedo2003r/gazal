<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AuthController;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('qr-code-g', function () {
//    \SimpleSoftwareIO\QrCode\Facades\QrCode::size(500)
//        ->format('png')
//        ->generate('ItSolutionStuff.com', public_path('images/qrcode.png');
//
//    return view('qrCode');
//
//});
Route::get('clear', function () {
  return \Illuminate\Support\Facades\Artisan::call('migrate');
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {
  Route::get('lang/{lang}', function ($lang) {
    Session::put('applocale', $lang);
    return redirect()->back();
  })->name('change.lang');
  //    Route::get('theme/{theme}', function ($theme){
  //        Session::put('theme', $theme);
  //        return redirect()->back();
  //    })->name('change.theme');

  Route::get('them/{them}', 'HomeController@them')->name('them');
  Route::get('login', [
    'uses'  => 'AuthController@showLoginForm',
    'title' => 'تسجيل الدخول',
  ])->name('show.login');
  Route::post('login', 'AuthController@login')->name('login');
  Route::post('logout', 'AuthController@logout')->name('logout');
  Route::get('change-language/{lang}', 'HomeController@changeLanguage')->name('change.language');

  Route::group(['middleware' => ['admin', 'check-role', 'admin-lang']], function () {

    /********************************* HomeController start *********************************/
    Route::get('dashboard', [
      'uses'  => 'HomeController@dashboard',
      'as'    => 'dashboard',
      'icon'  => '<i class="feather icon-home"></i>',
      'title' => 'الرئيسيه',
      'type'  => 'parent'
    ]);
    /********************************* HomeController end *********************************/
    /********************************* StatisticsController start *********************************/

      #statistics routes
    Route::get('statistics', [
      'as'        => 'admin.statistics',
      'title'     => 'الاحصائيات',
      'icon'      => '<i class="feather icon-pie-chart"></i>',
      'type'      => 'parent',
      'sub_route' => true,
      'child'     => ['statistics.sales','statistics.providers']
    ]);

    Route::get('statistics/sales', ['uses' => 'StatisticsController@sales', 'as' => 'statistics.sales', 'title' => 'المبيعات والخدمات', 'icon' => '<i class="nav-icon fa fa-list"></i>']);
    Route::get('statistics/providers', ['uses' => 'StatisticsController@providers', 'as' => 'statistics.providers', 'title' => 'مبيعات مقدمي الخدمه', 'icon' => '<i class="nav-icon fa fa-list"></i>']);
//    Route::get('statistics/clients', ['uses' => 'StatisticsController@clients', 'as' => 'statistics.clients', 'title' => 'المستخدمين', 'icon' => '<i class="nav-icon fa fa-list"></i>']);
////    Route::get('statistics/visits', ['uses' => 'StatisticsController@visits', 'as' => 'statistics.visits', 'title' => 'الزيارات', 'icon' => '<i class="nav-icon fa fa-list"></i>']);
//    Route::get('statistics/almostorder', ['uses' => 'StatisticsController@almostOrder', 'as' => 'statistics.almostOrder', 'title' => 'الأكثر طلبا', 'icon' => '<i class="nav-icon fa fa-list"></i>']);
//    Route::get('statistics/delegates', ['uses' => 'StatisticsController@delegates', 'as' => 'statistics.delegates', 'title' => 'السائقين', 'icon' => '<i class="nav-icon fa fa-list"></i>']);
//    Route::post('statistics/getdata', ['uses' => 'StatisticsController@getdata', 'as' => 'statistics.getdata', 'title' => 'جلب البيانات']);
    /********************************* RoleController start *********************************/
    Route::get('roles', [
      'uses'      => 'RoleController@index',
      'as'        => 'roles.index',
      'title'     => 'قائمة الصلاحيات',
      'icon'      => '<i class="feather icon-user-check"></i>',
      'type'      => 'parent',
      'sub_route' => false,
      'child'     => ['roles.create', 'roles.store', 'roles.edit', 'roles.update', 'roles.delete']
    ]);

    Route::get('roles/create', ['uses' => 'RoleController@create', 'as' => 'roles.create', 'title' => 'اضافة صلاحيه']);
    Route::post('roles/store', ['uses' => 'RoleController@store', 'as' => 'roles.store', 'title' => 'تمكين اضافة صلاحيه']);
    Route::get('roles/{id}/edit', ['uses' => 'RoleController@edit', 'as' => 'roles.edit', 'title' => 'تعديل صلاحيه']);
    Route::put('roles/{id}', ['uses' => 'RoleController@update', 'as' => 'roles.update', 'title' => 'تمكين تعديل صلاحيه']);
    Route::delete('roles/{id}', ['uses' => 'RoleController@destroy', 'as' => 'roles.delete', 'title' => 'حذف صلاحيه']);
    /********************************* RoleController end *********************************/

    /********************************* SettingController , SocialController  start *********************************/


    Route::get('settings', ['uses' => 'SettingController@index', 'as' => 'settings.index', 'title' => 'الاعدادات', 'icon' => '<i class="nav-icon fa fa-cog"></i>']);
    Route::post('settings', ['uses' => 'SettingController@update', 'as' => 'settings.update', 'title' => 'تحديث الاعدادات']);
    Route::post('socials/store', ['uses' => 'SocialController@store', 'as' => 'socials.store', 'title' => 'اضافه وسائل التواصل']);
    Route::post('socials', ['uses' => 'SocialController@update', 'as' => 'socials.update', 'title' => 'تحديث وسائل التواصل']);
    /********************************* SettingController , SocialController end *********************************/

    /********************************* CarTypeController start *********************************/
    Route::get('general', [
      'as'        => 'general',
      'icon'      => '<i class="fa fa-globe"></i>',
      'title'     => 'الاعدادت الرئيسيه',
      'type'      => 'parent',
      'sub_route' => true,
      'child'     => [
        'settings.index', 'settings.update', 'socials.store', 'socials.update',
        'carTypes.index', 'carTypes.store', 'carTypes.update', 'carTypes.destroy',
        'discountAmount.index', 'discountAmount.store', 'discountAmount.update', 'discountAmount.destroy',
        'types.index', 'types.store', 'types.update', 'types.destroy',
        'ratingList.index', 'ratingList.store', 'ratingList.update', 'ratingList.destroy',
        'countryCodes.index', 'countryCodes.update',
      ]
    ]);
    Route::get('car_types', ['uses' => 'CarTypeController@index', 'as' => 'carTypes.index', 'title' => 'أنواع السيارات', 'icon' => '<i class="nav-icon fa fa-globe"></i>']);
    Route::post('carTypes/store', ['uses' => 'CarTypeController@store', 'as' => 'carTypes.store', 'title' => 'اضافة نوع سياره']);
    Route::post('carTypes/{id}', ['uses' => 'CarTypeController@update', 'as' => 'carTypes.update', 'title' => 'تعديل نوع سياره']);
    Route::delete('carTypes/{id}', ['uses' => 'CarTypeController@destroy', 'as' => 'carTypes.destroy', 'title' => 'حذف نوع سياره']);


    Route::get('discountAmount', ['uses' => 'DiscountAmountController@index', 'as' => 'discountAmount.index', 'title' => 'أسباب الخصم', 'icon' => '<i class="nav-icon fa fa-globe"></i>']);
    Route::post('discountAmount/store', ['uses' => 'DiscountAmountController@store', 'as' => 'discountAmount.store', 'title' => 'اضافة سبب خصم']);
    Route::post('discountAmount/{id}', ['uses' => 'DiscountAmountController@update', 'as' => 'discountAmount.update', 'title' => 'تعديل سبب خصم']);
    Route::delete('discountAmount/{id}', ['uses' => 'DiscountAmountController@destroy', 'as' => 'discountAmount.destroy', 'title' => 'حذف سبب خصم']);


    Route::get('types', ['uses' => 'TypeController@index', 'as' => 'types.index', 'title' => 'السمات الأساسيه للمنتج', 'icon' => '<i class="nav-icon fa fa-tag"></i>']);
    Route::post('types/store', ['uses' => 'TypeController@store', 'as' => 'types.store', 'title' => 'اضافة سمه للمنتجات']);
    Route::post('types/{id}', ['uses' => 'TypeController@update', 'as' => 'types.update', 'title' => 'تعديل سمه للمنتجات']);
    Route::delete('types/{id}', ['uses' => 'TypeController@destroy', 'as' => 'types.destroy', 'title' => 'حذف سمه للمنتجات']);
    /********************************* CarTypeController end *********************************/

    Route::get('ratingList', ['uses' => 'RatingListController@index', 'as' => 'ratingList.index', 'title' => 'قائمة التقييمات الاساسيه', 'icon' => '<i class="nav-icon fa fa-tag"></i>']);
    Route::post('ratingList/store', ['uses' => 'RatingListController@store', 'as' => 'ratingList.store', 'title' => 'اضافة مسمي تقييم']);
    Route::post('ratingList/{id}', ['uses' => 'RatingListController@update', 'as' => 'ratingList.update', 'title' => 'تعديل مسمي تقييم']);
    Route::delete('ratingList/{id}', ['uses' => 'RatingListController@destroy', 'as' => 'ratingList.destroy', 'title' => 'حذف مسمي تقييم']);
    /********************************* RatingListController end *********************************/

    Route::get('countryCodes', ['uses' => 'CountryCodesController@index', 'as' => 'countryCodes.index', 'title' => 'أكواد الدول', 'icon' => '<i class="nav-icon fa fa-flag"></i>']);
    Route::post('countryCodes/{id}', ['uses' => 'CountryCodesController@update', 'as' => 'countryCodes.update', 'title' => 'تعديل كود دوله']);
    /********************************* RatingListController end *********************************/
    /********************************* AdminController start *********************************/

      Route::get('admins', [
          'uses'      => 'AdminController@index',
          'as'        => 'admins.index',
          'title'     => 'المشرفين',
          'icon'      => '<i class="feather icon-user"></i>',
          'type'      => 'parent',
          'sub_route' => false,
          'child'     => [
              'admins.index', 'admins.create', 'admins.store', 'admins.edit',  'admins.update', 'admins.delete',
          ]
      ]);

      # AdminController
      Route::get('admins/create', ['uses' => 'AdminController@create', 'as' => 'admins.create', 'title' => 'اضافة مشرف']);
      Route::post('admins/store', ['uses' => 'AdminController@store', 'as' => 'admins.store', 'title' => 'حفظ بيانات مشرف']);
      Route::get('admins/edit/{admin}', ['uses' => 'AdminController@edit', 'as' => 'admins.edit', 'title' => 'تعديل مشرف']);
      Route::post('admins/{id}', ['uses' => 'AdminController@update', 'as' => 'admins.update', 'title' => ' تحديث بيانات مشرف']);
      Route::delete('admins/{id}', ['uses' => 'AdminController@destroy', 'as' => 'admins.delete', 'title' => 'حذف مشرف']);

      /********************************* AdminController end *********************************/

    /********************************* OrderController start *********************************/
    Route::get('allOrders', [
      'as'        => 'orders',
      'title'     => 'الطلبات',
      'icon'      => '<i class="feather icon-shopping-cart"></i>',
      'type'      => 'parent',
      'sub_route' => true,
      'child'     => ['orders.index','orders.accepted','orders.prepared','orders.refused','orders.onway','orders.arrived','orders.timeout','orders.postponed','orders.assignDelegate','orders.changeStatus','orders.show','orders.getFilterData', 'orders.client', 'orders.destroy']
    ]);

    Route::get('orders', ['uses' => 'OrderController@index', 'as' => 'orders.index', 'title' => 'جميع الطلبات','icon' => '<i class="feather icon-shopping-cart"></i>']);
    Route::get('orders/accepted', ['uses' => 'OrderController@index', 'as' => 'orders.accepted', 'title' => 'طلبات مقبوله','icon' => '<i class="feather icon-shopping-cart"></i>']);
    Route::get('orders/prepared', ['uses' => 'OrderController@index', 'as' => 'orders.prepared', 'title' => 'طلبات جاري تحضيرها','icon' => '<i class="feather icon-shopping-cart"></i>']);
    Route::get('orders/onway', ['uses' => 'OrderController@index', 'as' => 'orders.onway', 'title' => 'طلبات جاري توصيلها','icon' => '<i class="feather icon-shopping-cart"></i>']);
    Route::get('orders/arrived', ['uses' => 'OrderController@index', 'as' => 'orders.arrived', 'title' => 'طلبات تم توصيلها','icon' => '<i class="feather icon-shopping-cart"></i>']);
    Route::get('orders/refused', ['uses' => 'OrderController@index', 'as' => 'orders.refused', 'title' => 'طلبات مرفوضه','icon' => '<i class="feather icon-shopping-cart"></i>']);
    Route::get('orders/timeout', ['uses' => 'OrderController@index', 'as' => 'orders.timeout', 'title' => 'طلبات time out','icon' => '<i class="feather icon-shopping-cart"></i>']);
    Route::get('orders/postponed', ['uses' => 'OrderController@index', 'as' => 'orders.postponed', 'title' => 'طلبات مؤجله','icon' => '<i class="feather icon-shopping-cart"></i>']);
    Route::get('orders/show/{id}', ['uses' => 'OrderController@show', 'as' => 'orders.show', 'title' => 'مشاهدة طلب']);
    Route::get('orders/filter/getData', ['uses' => 'OrderController@getFilterData', 'as' => 'orders.getFilterData', 'title' => 'جلب بيانات الطلبات']);
    Route::get('orders/client/{id}', ['uses' => 'OrderController@orderClient', 'as' => 'orders.client', 'title' => 'مشاهدة طلبات العميل']);
    Route::post('orders/changeStatus', ['uses' => 'OrderController@changeStatus', 'as' => 'orders.changeStatus', 'title' => 'تغيير حالة الطلب']);
    Route::post('orders/assignDelegate', ['uses' => 'OrderController@assignDelegate', 'as' => 'orders.assignDelegate', 'title' => 'تعيين مندوب']);
    Route::delete('orders/{id}', ['uses' => 'OrderController@destroy', 'as' => 'orders.destroy', 'title' => 'حذف طلب']);
    /********************************* OrderController end *********************************/
    Route::get('clients', [
      'uses'      => 'ClientController@index',
      'as'        => 'clients.index',
      'title'     => 'المستخدمين',
      'icon'      => ' <i class="feather icon-user"></i>',
      'type'      => 'parent',
      'sub_route' => false,
      'child'     => ['clients.index', 'clients.getFilterData', 'clients.create', 'clients.show', 'clients.addresses', 'clients.addressCreate', 'clients.addressEdit', 'clients.storeAddress', 'clients.updateAddress', 'clients.store', 'clients.update', 'clients.delete', 'clients.compensation', 'clients.block', 'clients.unblock', 'addToWallet', 'sendnotifyuser', 'changeStatus']
    ]);

    # ClientController
    Route::get('clients/show/{id}', ['uses' => 'ClientController@show', 'as' => 'clients.show', 'title' => 'مشاهدة بيانات العميل']);
    Route::get('clients/create', ['uses' => 'ClientController@create', 'as' => 'clients.create', 'title' => 'صفحة اضافة عميل']);
    Route::get('clients/getFilterData/all', ['uses' => 'ClientController@getFilterData', 'as' => 'clients.getFilterData', 'title' => 'بيانات المستخدمين']);
    Route::get('clients/addresses/{id}', ['uses' => 'ClientController@addresses', 'as' => 'clients.addresses', 'title' => 'صفحة عناوين العميل']);
    Route::get('clients/addresses/{id}/create', ['uses' => 'ClientController@addressCreate', 'as' => 'clients.addressCreate', 'title' => 'صفحة اضافة عناوين العميل']);
    Route::get('clients/addresses/{id}/edit', ['uses' => 'ClientController@addressEdit', 'as' => 'clients.addressEdit', 'title' => 'صفحة تعديل عنوان العميل']);
    Route::post('clients/addresses', ['uses' => 'ClientController@storeAddress', 'as' => 'clients.storeAddress', 'title' => 'حفظ عنوان للعميل']);
    Route::post('clients/addresses/update/{id}', ['uses' => 'ClientController@updateAddress', 'as' => 'clients.updateAddress', 'title' => 'تحديث عنوان للعميل']);
    Route::post('clients/store', ['uses' => 'ClientController@store', 'as' => 'clients.store', 'title' => 'اضافة عميل']);
    Route::post('clients/{id}', ['uses' => 'ClientController@update', 'as' => 'clients.update', 'title' => 'تعديل عميل']);
    Route::post('clients/compensation/single', ['uses' => 'ClientController@compensation', 'as' => 'clients.compensation', 'title' => 'تعويض عميل']);
    Route::post('clients/block/single', ['uses' => 'ClientController@block', 'as' => 'clients.block', 'title' => 'حظر عميل']);
    Route::post('clients/unblock/single', ['uses' => 'ClientController@unblock', 'as' => 'clients.unblock', 'title' => 'الغاء حظر عميل']);
    Route::delete('clients/{id}', ['uses' => 'ClientController@destroy', 'as' => 'clients.delete', 'title' => 'حذف عميل']);
    Route::post('addToWallet', ['uses' => 'ClientController@addToWallet', 'as' => 'addToWallet', 'title' => 'اضافه الي المحفظه']);

    Route::post('send-notify-user', ['uses' => 'ClientController@sendnotifyuser', 'as' => 'sendnotifyuser', 'title' => 'ارسال اشعارات']);
    Route::post('change-status', ['uses' => 'ClientController@changeStatus', 'as' => 'changeStatus', 'title' => 'تغيير الحاله']);


    Route::get('allproviders', [
      'as'        => 'providers',
      'title'     => ' المتاجر',
      'icon' => ' <i class="feather icon-users"></i>',
      'type'      => 'parent',
      'sub_route' => true,
      'child'     => [
        'providers.index', 'providers.create', 'providers.store', 'providers.edit', 'providers.show', 'providers.update', 'providers.discount','providers.changeSchedule',
        'categories.index', 'categories.store', 'categories.update', 'categories.destroy',
        'subcategories.index', 'subcategories.store', 'subcategories.update', 'subcategories.destroy',
        'groups.index', 'groups.store', 'groups.update', 'groups.destroy',
        'providers.callcenter','providers.callcenterCreate','providers.editCallcenter','providers.callcenterStore','providers.updateCallcenter',
        'providers.branchesCreate','providers.branchesDates','providers.branchesTypeDate','providers.branches','providers.getBranchData', 'providers.postbranch', 'providers.editbranch', 'providers.updatebranch','providers.showbranch', 'providers.deletebranches', 'providers.branches.create', 'providers.storebranchdate', 'providers.deletedatebranches',
        'providers.categories', 'providers.postcategories', 'providers.images', 'providers.storeImages','providers.sales','providers.salesAmount',
        'providers.items',
        'branches.branchOpenClosed','branches.branchChangeStatus','branches.block','branches.unblock','branches.changeSchedule'
      ]
    ]);

    # ProviderController
    Route::get('providers', ['uses' => 'ProviderController@index', 'as' => 'providers.index', 'title' => 'مقدمين الخدمه','icon' => ' <i class="feather icon-users"></i>']);
    Route::get('providers/create', ['uses' => 'ProviderController@create', 'as' => 'providers.create', 'title' => 'اضافة مقدم خدمه','icon' => ' <i class="fa fa-plus"></i>']);
    Route::get('providers/edit/{provider}', ['uses' => 'ProviderController@edit', 'as' => 'providers.edit', 'title' => 'صفحة تعديل مقدم خدمه']);
    Route::get('providers/show/{provider}', ['uses' => 'ProviderController@show', 'as' => 'providers.show', 'title' => 'مشاهدة بيانات مقدم خدمه']);
    Route::post('providers/store', ['uses' => 'ProviderController@store', 'as' => 'providers.store', 'title' => 'اضافة مقدم خدمه']);
    Route::post('providers/changeSchedule', ['uses' => 'ProviderController@changeSchedule', 'as' => 'providers.changeSchedule', 'title' => 'تغيير مواعيد العمل لمقدمي الخدمه']);
    Route::post('providers/{id}', ['uses' => 'ProviderController@update', 'as' => 'providers.update', 'title' => 'تعديل مقدم خدمه']);

      Route::get('providers/callcenter/{provider}', ['uses' => 'CallCenterController@sellercallcenter', 'as' => 'providers.callcenter', 'title' => 'كول سنتر مقدم الخدمه']);
      Route::get('providers/callcenter/create/{provider}', ['uses' => 'CallCenterController@create', 'as' => 'providers.callcenterCreate', 'title' => 'اضافة كول سنتر مقدم الخدمه']);
      Route::post('providers/callcenter/store', ['uses' => 'CallCenterController@store', 'as' => 'providers.callcenterStore', 'title' => 'حفظ كول سنتر مقدم الخدمه']);
      Route::get('providers/callcenter/edit/{provider}', ['uses' => 'CallCenterController@edit', 'as' => 'providers.editCallcenter', 'title' => 'تعديل كول سنتر مقدم الخدمه']);
      Route::post('providers/callcenter/update/{id}', ['uses' => 'CallCenterController@update', 'as' => 'providers.updateCallcenter', 'title' => 'تحديث كول سنتر مقدم الخدمه']);
      /********************************* CategoryController start *********************************/
      Route::get('categories', ['uses' => 'CategoryController@index', 'as' => 'categories.index', 'title' => 'الأقسام','icon' => '<i class="feather icon-layers"></i>']);
      Route::post('categories/store', ['uses' => 'CategoryController@store', 'as' => 'categories.store', 'title' => 'اضافة قسم']);
      Route::post('categories/{id}', ['uses' => 'CategoryController@update', 'as' => 'categories.update', 'title' => 'تعديل قسم']);
      Route::delete('categories/{id}', ['uses' => 'CategoryController@destroy', 'as' => 'categories.destroy', 'title' => 'حذف قسم']);
      /********************************* CategoryController end *********************************/

      /********************************* SubCategoryController start *********************************/
      Route::get('subcategories/{id}', ['uses' => 'SubCategoryController@index', 'as' => 'subcategories.index', 'title' => 'الأقسام الفرعيه']);
      Route::post('subcategories/store/{id}', ['uses' => 'SubCategoryController@store', 'as' => 'subcategories.store', 'title' => 'اضافة قسم فرعي']);
      Route::post('subcategories/{id}', ['uses' => 'SubCategoryController@update', 'as' => 'subcategories.update', 'title' => 'تعديل قسم فرعي']);
      Route::delete('subcategories/{id}', ['uses' => 'SubCategoryController@destroy', 'as' => 'subcategories.destroy', 'title' => 'حذف قسم فرعي']);
      /********************************* SubCategoryController end *********************************/

      /********************************* GroupController start *********************************/
      Route::get('groups/{id}', ['uses' => 'GroupController@index', 'as' => 'groups.index', 'title' => 'التصنيفات']);
      Route::post('groups/store/{id}', ['uses' => 'GroupController@store', 'as' => 'groups.store', 'title' => 'اضافة تصنيف']);
      Route::post('groups/{id}', ['uses' => 'GroupController@update', 'as' => 'groups.update', 'title' => 'تعديل تصنيف']);
      Route::delete('groups/{id}', ['uses' => 'GroupController@destroy', 'as' => 'groups.destroy', 'title' => 'حذف تصنيف']);
      /********************************* GroupController end *********************************/


      Route::post('providers/branches/discount', ['uses' => 'ProviderController@discount', 'as' => 'providers.discount', 'title' => 'خصم البائع']);
    Route::get('providers/images/{id}', ['uses' => 'ProviderController@images', 'as' => 'providers.images', 'title' => 'معرض الصور للمقدم']);
    Route::post('providers/images/{id}', ['uses' => 'ProviderController@storeImages', 'as' => 'providers.storeImages', 'title' => 'حفظ معرض الصور للمقدم']);
    Route::get('providers/categories/{$provider}', ['uses' => 'ProviderController@categories', 'as' => 'providers.categories', 'title' => 'الأقسام للبائع',]);
    Route::post('providers/categories/store', ['uses' => 'ProviderController@postcategories', 'as' => 'providers.postcategories', 'title' => 'حفظ الأقسام للبائع']);
      Route::post('providers/add/Wallet', ['uses' => 'ProviderController@addToWallet', 'as' => 'providers.addToWallet', 'title' => 'اضافة رصيد محفظه لمقدم خدمه']);

    Route::get('providers/branches/getBranchData', ['uses' => 'BranchController@getBranchData', 'as' => 'providers.getBranchData', 'title' => 'جلب بيانات الفرع للجدول']);
    Route::get('providers/branches/create', ['uses' => 'BranchController@branchesCreate', 'as' => 'providers.branchesCreate', 'title' => 'اضافة فروع','icon' => '<i class="fa fa-plus"></i>']);
    Route::get('providers/branches/dates', ['uses' => 'BranchController@branchesDates', 'as' => 'providers.branchesDates', 'title' => 'تعديل أوقات عمل الفروع','icon' => '<i class="fa fa-clock-o"></i>']);
    Route::get('providers/branches/dates/{type}/{branch}', ['uses' => 'BranchController@branchesTypeDate', 'as' => 'providers.branchesTypeDate', 'title' => 'صفحة أوقات العمل']);

    Route::get('providers/sales', ['uses' => 'ProviderController@sales', 'as' => 'providers.sales', 'title' => 'مبيعات مقدمي الخدمه','icon' => ' <i class="fa fa-dollar"></i>']);
    Route::post('providers/sales/amount', ['uses' => 'ProviderController@salesAmount', 'as' => 'providers.salesAmount', 'title' => 'قيمة المبيعات']);

    Route::get('providers/items', ['uses' => 'ProviderController@items', 'as' => 'providers.items', 'title' => 'تعديل منيو مقدمي الخدمه','icon' => ' <i class="feather icon-package"></i>']);

    Route::get('providers/branches/openClosed', ['uses' => 'BranchController@branchOpenClosed', 'as' => 'branches.branchOpenClosed', 'title' => 'الفروع المفتوحه والمغلقه','icon' => ' <i class="fas fa-arrows-alt"></i>']);
    Route::get('providers/branches/branchChangeStatus/{branch}', ['uses' => 'BranchController@branchChangeStatus', 'as' => 'branches.branchChangeStatus', 'title' => 'تغيير حالة الفرع']);
    Route::post('providers/branches/block', ['uses' => 'BranchController@block', 'as' => 'branches.block', 'title' => 'حظر فرع']);
    Route::post('providers/branches/unblock', ['uses' => 'BranchController@unblock', 'as' => 'branches.unblock', 'title' => 'الغاء حظر فرع']);


    Route::get('providers/branches/create/{provider}', ['uses' => 'BranchController@sellerbrancheCreate', 'as' => 'providers.branches.create', 'title' => 'صفحة اضافة فرع للبائع']);
    Route::get('providers/branches/{provider}', ['uses' => 'BranchController@sellerbranches', 'as' => 'providers.branches', 'title' => 'فروع البائع']);
    Route::post('providers/branches/store', ['uses' => 'BranchController@postbranch', 'as' => 'providers.postbranch', 'title' => 'حفظ فروع البائع']);
    Route::get('providers/branches/edit/{branch}', ['uses' => 'BranchController@editbranch', 'as' => 'providers.editbranch', 'title' => 'صفحة تعديل فروع البائع']);
    Route::get('providers/branches/show/{branch}', ['uses' => 'BranchController@showbranch', 'as' => 'providers.showbranch', 'title' => 'مشاهدة ملف فرع البائع']);
    Route::post('providers/branches/update/{id}', ['uses' => 'BranchController@updatebranch', 'as' => 'providers.updatebranch', 'title' => 'تعديل فروع البائع']);
    Route::delete('providers/branches/delete/{id}', ['uses' => 'BranchController@deletebranches', 'as' => 'providers.deletebranches', 'title' => 'حذف فروع البائع']);
    Route::post('providers/branches/storeDate', ['uses' => 'BranchController@storebranchdate', 'as' => 'providers.storebranchdate', 'title' => 'حفظ مواعيد الفرع']);
    Route::post('providers/branches/datedelete', ['uses' => 'BranchController@deletedatebranches', 'as' => 'providers.deletedatebranches', 'title' => 'حذف مواعيد الفرع']);
    Route::post('providers/branches/changeSchedule', ['uses' => 'BranchController@changeSchedule', 'as' => 'branches.changeSchedule', 'title' => 'تغيير مواعيد الفرع']);


      /*------------ start Of account manger Controller ----------*/


    #manager routes
    Route::get('manager', [
      'uses'      => 'ManagerController@statistics',
      'as'        => 'manager',
      'title'     => 'ادارة الحسابات',
      'icon'      => '<i class="feather icon-pie-chart"></i>',
      'type'      => 'parent',
      'sub_route' => true,
      'child'     => ['manager.joins','manager.showJoin','manager.joinsDelete','manager.endOfContract','manager.endOfContractStore','manager.getEndOfContractData','manager.closedProviders','manager.providersTimeOut','manager.percentagesSales','manager.addPercentagesPage','manager.postCommission']
    ]);

      Route::get('manager/joins', ['uses' => 'ManagerController@joins', 'as' => 'manager.joins', 'title' => 'انضمام مقدمي الخدمه', 'icon' => '<i class="nav-icon fa fa-list"></i>']);
      Route::get('manager/endOfContract', ['uses' => 'ManagerController@endOfContract', 'as' => 'manager.endOfContract', 'title' => 'العقود المنتهيه', 'icon' => '<i class="nav-icon fa fa-list"></i>']);
      Route::get('manager/closedProviders', ['uses' => 'ManagerController@closedProviders', 'as' => 'manager.closedProviders', 'title' => 'مقدمين الخدمه المغلقين', 'icon' => '<i class="nav-icon fa fa-list"></i>']);
      Route::post('manager/endOfContract/{id}', ['uses' => 'ManagerController@endOfContractStore', 'as' => 'manager.endOfContractStore', 'title' => 'تعديل ميعاد انتهاء التعاقد']);
      Route::get('manager/getEndOfContractData', ['uses' => 'ManagerController@getEndOfContractData', 'as' => 'manager.getEndOfContractData', 'title' => 'بيانات العقود المنتهيه']);
      Route::get('manager/providersTimeOut', ['uses' => 'ManagerController@providersTimeOut', 'as' => 'manager.providersTimeOut', 'title' => 'مقدمين خدمه لا يستجيب للطلبات', 'icon' => '<i class="nav-icon fa fa-list"></i>']);

      Route::get('manager/percentagesSales', ['uses' => 'ManagerController@percentagesSales', 'as' => 'manager.percentagesSales', 'title' => 'اضافة النسب والعموله', 'icon' => '<i class="nav-icon fa fa-list"></i>']);
      Route::get('manager/addPercentagesPage/{provider}', ['uses' => 'ManagerController@addPercentagesPage', 'as' => 'manager.addPercentagesPage', 'title' => 'صفحة اضافة النسب والعموله']);
      Route::post('manager/postCommission/{provider}', ['uses' => 'ManagerController@postCommission', 'as' => 'manager.postCommission', 'title' => 'حفظ النسبه أو العموله']);

      Route::get('manager/joins/{id}', ['uses' => 'ManagerController@showJoin', 'as' => 'manager.showJoin', 'title' => 'مشاهدة انضمام مقدمي الخدمه']);
      Route::delete('manager/joins/{id}', ['uses' => 'ManagerController@joinsDelete', 'as' => 'manager.joinsDelete', 'title' => 'حذف انضمام مقدمي الخدمه']);


      #accounts routes
      Route::get('account', [
          'as'        => 'accounts',
          'title'     => 'الحسابات',
          'icon'      => '<i class="feather icon-pie-chart"></i>',
          'type'      => 'parent',
          'sub_route' => true,
          'child'     => ['accounts.index','accounts.discountAmount','accounts.discountAmountCreate','accounts.discountAmountStore'
              ,'accounts.providerReport','accounts.filterProviderReport','accounts.providerReportPdf'
              ,'accounts.addTax'
          ]
      ]);
      Route::get('accounts', ['uses' => 'AccountController@index', 'as' => 'accounts.index', 'title' => 'مبالغ الخصم','icon' => '<i class="fa fa-plus"></i>']);
      Route::get('accounts/discountAmount/{id}', ['uses' => 'AccountController@discountAmount', 'as' => 'accounts.discountAmount', 'title' => 'اضافة مبالغ الخصم']);
      Route::get('accounts/discountAmount/create/{id}', ['uses' => 'AccountController@discountAmountCreate', 'as' => 'accounts.discountAmountCreate', 'title' => 'صفحة اضافة مبالغ الخصم']);
      Route::post('accounts/discountAmount/store/{id}', ['uses' => 'AccountController@discountAmountStore', 'as' => 'accounts.discountAmountStore', 'title' => 'حفظ مبالغ الخصم']);

      Route::get('providerReport', ['uses' => 'AccountController@providerReport', 'as' => 'accounts.providerReport', 'title' => 'تقرير مقدم الخدمه وكشف الحساب','icon' => '<i class="fas fa-flag-checkered"></i>']);
      Route::get('providerReport/{provider}', ['uses' => 'AccountController@filterProviderReport', 'as' => 'accounts.filterProviderReport', 'title' => 'صفحة تقرير مقدم الخدمه']);
      Route::post('providerReport/{provider}', ['uses' => 'AccountController@providerReportPdf', 'as' => 'accounts.providerReportPdf', 'title' => 'تقرير مقدم الخدمه']);

      Route::get('addTax', ['uses' => 'AccountController@addTax', 'as' => 'accounts.addTax', 'title' => 'اضافة الضريبه','icon' => '<i class="fa fa-dollar"></i>']);

      /********************************* all fleet controllers start *********************************/
    Route::get('fleet', [
      'as'        => 'fleets',
      'icon'      => '<i class="feather icon-users"></i>',
      'title'     => 'ادارة الأسطول',
      'type'      => 'parent',
      'sub_route' => true,
      'child'     => [
         'delegates.create','companies.index','companies.create','companies.store','companies.edit','companies.update','companies.show','companies.block','companies.unblock','delegates.getFilterData', 'delegates.index', 'delegates.edit', 'delegates.show', 'delegates.store', 'delegates.update', 'delegates.images',
          'delegates.storeImages','delegates.block','delegates.unblock','delegates.addToWallet','delegates.rating','delegates.blocked',
          'delegates.hire','delegates.hire.create','delegates.hire.show','delegates.hire.store','delegates.hire.edit','delegates.hire.update','delegates.hire.destroy',
      ]
    ]);

    # delegateController
      Route::get('delegates/create', ['uses' => 'DelegateController@create', 'as' => 'delegates.create', 'title' => 'اضافة سائق','icon' => '<i class="fa fa-plus"></i>']);
      Route::get('companies', ['uses' => 'OperatingCompanyController@index', 'as' => 'companies.index', 'title' => 'الشركات المشغله','icon' => '<i class="fa fa-users"></i>']);
      Route::post('companies', ['uses' => 'OperatingCompanyController@store', 'as' => 'companies.store', 'title' => 'حفظ بيانات الشركه المشغله']);
      Route::get('companies/create', ['uses' => 'OperatingCompanyController@create', 'as' => 'companies.create', 'title' => 'اضافة شركه مشغله','icon' => '<i class="fa fa-plus"></i>']);
      Route::get('companies/show/{company}', ['uses' => 'OperatingCompanyController@show', 'as' => 'companies.show', 'title' => 'مشاهدة الملف الشخصي للشركه المشغله']);
      Route::get('companies/edit/{company}', ['uses' => 'OperatingCompanyController@edit', 'as' => 'companies.edit', 'title' => 'صفحة تعديل الشركه المشغله']);
      Route::post('companies/update/{id}', ['uses' => 'OperatingCompanyController@update', 'as' => 'companies.update', 'title' => 'تحديث بيانات الشركه المشغله']);
      Route::post('companies/block/single', ['uses' => 'OperatingCompanyController@block', 'as' => 'companies.block', 'title' => 'حظر الشركه المشغله']);
      Route::post('companies/unblock/single', ['uses' => 'OperatingCompanyController@unblock', 'as' => 'companies.unblock', 'title' => 'الغاء حظر الشركه المشغله']);

      Route::get('delegates/getFilterData', ['uses' => 'DelegateController@getFilterData', 'as' => 'delegates.getFilterData', 'title' => 'جلب بيانات السائقين']);
      Route::get('delegates', ['uses' => 'DelegateController@index', 'as' => 'delegates.index', 'title' => 'استعراض السائقين','icon' => '<i class="feather icon-users"></i>']);
      Route::get('delegates/edit/{delegate}', ['uses' => 'DelegateController@edit', 'as' => 'delegates.edit', 'title' => 'صفحة تعديل سائق']);
      Route::post('delegates/store', ['uses' => 'DelegateController@store', 'as' => 'delegates.store', 'title' => 'حفظ بيانات سائق']);
      Route::post('delegates/{id}', ['uses' => 'DelegateController@update', 'as' => 'delegates.update', 'title' => 'تحديث بيانات سائق']);
//    Route::delete('delegates/{id}', ['uses' => 'DelegateController@destroy', 'as' => 'delegates.delete', 'title' => 'حذف سائق']);
    Route::get('delegates/show/{delegate}', ['uses' => 'DelegateController@show', 'as' => 'delegates.show', 'title' => 'مشاهدة الملف الشخصي للسائق']);
    Route::get('delegates/images/{delegate}', ['uses' => 'DelegateController@images', 'as' => 'delegates.images', 'title' => 'معرض الصور للسائق']);
    Route::post('delegates/images/{delegate}', ['uses' => 'DelegateController@storeImages', 'as' => 'delegates.storeImages', 'title' => 'حفظ معرض الصور للسائق']);

    Route::post('delegates/add/Wallet', ['uses' => 'DelegateController@addToWallet', 'as' => 'delegates.addToWallet', 'title' => 'اضافة رصيد محفظه للسائق']);
    Route::post('delegates/block/single', ['uses' => 'DelegateController@block', 'as' => 'delegates.block', 'title' => 'حظر سائق']);
    Route::post('delegates/unblock/single', ['uses' => 'DelegateController@unblock', 'as' => 'delegates.unblock', 'title' => 'الغاء حظر سائق']);

    Route::get('delegates/rating', ['uses' => 'DelegateController@rating', 'as' => 'delegates.rating', 'title' => 'تقييم السائقين','icon' => '<i class="fa fa-star"></i>']);

    Route::get('delegates/blocked', ['uses' => 'DelegateController@blocked', 'as' => 'delegates.blocked', 'title' => 'الحسابات الموقوفه','icon' => '<i class="fa fa-ban"></i>']);

    Route::get('delegates/hire', ['uses' => 'HireController@index', 'as' => 'delegates.hire', 'title' => 'التأجير','icon' => '<i class="fa fa-star"></i>']);
    Route::get('delegates/hire/show/{hire}', ['uses' => 'HireController@show', 'as' => 'delegates.hire.show', 'title' => 'مشاهدة طلب التأجير']);
    Route::get('delegates/hire/create', ['uses' => 'HireController@create', 'as' => 'delegates.hire.create', 'title' => 'اضافة طلب التأجير']);
    Route::post('delegates/hire/store', ['uses' => 'HireController@store', 'as' => 'delegates.hire.store', 'title' => 'حفظ طلب التأجير']);
    Route::get('delegates/hire/{hire}', ['uses' => 'HireController@edit', 'as' => 'delegates.hire.edit', 'title' => 'تعديل طلب التأجير']);
    Route::post('delegates/hire/{hire}', ['uses' => 'HireController@update', 'as' => 'delegates.hire.update', 'title' => 'تحديث طلب التأجير']);
    Route::post('delegates/hire/destroy/{id}', ['uses' => 'HireController@destroy', 'as' => 'delegates.hire.destroy', 'title' => 'حذف طلب تأجير']);
    /********************************* all users controllers end *********************************/

      /*------------ start Of marketing Controller ----------*/


      #index
      Route::get('marketing', [
          'as'        => 'marketing',
          'title'     => 'ادارة التسويق',
          'icon'      => '<i class="fas fa-search-dollar"></i>',
          'type'      => 'parent',
          'sub_route' => true,
          'child'     => ['coupons.index', 'coupons.create', 'coupons.store', 'coupons.edit','coupons.update', 'coupons.destroy',
              'wallets.index','wallets.addToWallet','wallets.addGift','wallets.addToAllUser',
              'marketing.notifications','marketing.notifications.send',
              'marketing.birthDates','marketing.birthDates.notify','marketing.birthDates.sendNotify',
              'topLists.index', 'topLists.create','topLists.store', 'topLists.edit', 'topLists.update', 'topLists.destroy',
              'offers.index', 'offers.create','offers.store', 'offers.edit', 'offers.update', 'offers.destroy',
              'sliders.index', 'sliders.create','sliders.store','sliders.edit', 'sliders.update', 'sliders.destroy', 'sliders.changeActive',
              'banners.index', 'banners.create', 'banners.store', 'banners.edit',  'banners.update', 'banners.destroy', 'banners.changeActive',
              'employees.index','employees.create','employees.store','employees.edit','employees.update',
              ]
      ]);

      Route::get('coupons', ['uses' => 'CouponController@index', 'as' => 'coupons.index', 'title' => 'الأكواد','icon' => '<i class="feather icon-minus"></i>']);
      Route::get('coupons/create', ['uses' => 'CouponController@create', 'as' => 'coupons.create', 'title' => 'صفحة اضافة الكود']);
      Route::post('coupons/store', ['uses' => 'CouponController@store', 'as' => 'coupons.store', 'title' => 'اضافة الكود']);
      Route::get('coupons/edit/{coupon}', ['uses' => 'CouponController@edit', 'as' => 'coupons.edit', 'title' => 'صفحة تعديل الكود']);
      Route::post('coupons/{id}', ['uses' => 'CouponController@update', 'as' => 'coupons.update', 'title' => 'تعديل الكود']);
      Route::delete('coupons/{id}', ['uses' => 'CouponController@destroy', 'as' => 'coupons.destroy', 'title' => 'حذف الكود']);

      Route::get('wallets', ['uses' => 'WalletController@index', 'as' => 'wallets.index', 'title' => 'محفظة العملاء','icon' => '<i class="fa fa-wallet"></i>']);
      Route::post('wallets/addToAllUser', ['uses' => 'WalletController@addToAllUser', 'as' => 'wallets.addToAllUser', 'title' => 'اضافة الي محفظة كل العملاء']);
      Route::get('wallets/addTo/{user}', ['uses' => 'WalletController@addToWallet', 'as' => 'wallets.addToWallet', 'title' => 'صفحة اضافه الي المحفظه']);
      Route::post('wallets/addGift', ['uses' => 'WalletController@addGift', 'as' => 'wallets.addGift', 'title' => 'اضافة رصيد هديه']);
//      notifications
      Route::get('clientNotifications', ['uses' => 'ClientNotificationController@index', 'as' => 'marketing.notifications', 'title' => 'الاشعارات','icon' => '<i class="feather icon-bell"></i>']);
      Route::post('clientNotifications/send', ['uses' => 'ClientNotificationController@send', 'as' => 'marketing.notifications.send', 'title' => 'ارسال اشعار']);

//      birth dates
      Route::get('birthDates', ['uses' => 'BirthDateController@index', 'as' => 'marketing.birthDates', 'title' => 'عيد الميلاد','icon' => '<i class="feather icon-bell"></i>']);
      Route::get('birthDates/notify/{user}', ['uses' => 'BirthDateController@notify', 'as' => 'marketing.birthDates.notify', 'title' => 'صفحة ارسال اشعار لصاحب عيد الميلاد']);
      Route::post('birthDates/notify/send', ['uses' => 'BirthDateController@sendNotify', 'as' => 'marketing.birthDates.sendNotify', 'title' => 'ارسال اشعار لصاحب عيد الميلاد']);



      Route::get('topLists', ['uses' => 'TopListController@index', 'as' => 'topLists.index', 'title' => 'التوب ليست','icon'=>'<i class="feather icon-grid"></i>']);
      Route::get('topLists/create', ['uses' => 'TopListController@create', 'as' => 'topLists.create', 'title' => 'صفحة اضافة التوب ليست']);
      Route::get('topLists/edit/{topList}', ['uses' => 'TopListController@edit', 'as' => 'topLists.edit', 'title' => 'صفحة تعديل توب ليست']);
      Route::post('topLists/store', ['uses' => 'TopListController@store', 'as' => 'topLists.store', 'title' => 'اضافة توب ليست']);
      Route::post('topLists/{id}', ['uses' => 'TopListController@update', 'as' => 'topLists.update', 'title' => 'تعديل توب ليست']);
      Route::delete('topLists/{id}', ['uses' => 'TopListController@destroy', 'as' => 'topLists.destroy', 'title' => 'حذف توب ليست']);


      Route::get('offers', ['uses' => 'OfferController@index', 'as' => 'offers.index', 'title' => 'عروض مقدمي الخدمه','icon'=>'<i class="feather icon-grid"></i>']);
      Route::get('offers/create', ['uses' => 'OfferController@create', 'as' => 'offers.create', 'title' => 'صفحة اضافة عرض']);
      Route::get('offers/edit/{offer}', ['uses' => 'OfferController@edit', 'as' => 'offers.edit', 'title' => 'صفحة تعديل عرض']);
      Route::post('offers/store', ['uses' => 'OfferController@store', 'as' => 'offers.store', 'title' => 'اضافة عرض']);
      Route::post('offers/{id}', ['uses' => 'OfferController@update', 'as' => 'offers.update', 'title' => 'تعديل عرض']);
      Route::delete('offers/{id}', ['uses' => 'OfferController@destroy', 'as' => 'offers.destroy', 'title' => 'حذف عرض']);
      Route::post('offers/active/change', ['uses' => 'OfferController@changeActive', 'as' => 'offers.changeActive', 'title' => 'تنشيط']);

//      sliders

      Route::get('sliders', ['uses' => 'SliderController@index', 'as' => 'sliders.index', 'title' => 'الاعلانات المتحركه', 'icon' => '<i class="nav-icon fa fa-database"></i>']);
      Route::get('sliders/create', ['uses' => 'SliderController@create', 'as' => 'sliders.create', 'title' => 'صفحة اضافة اعلان متحرك']);
      Route::get('sliders/edit/{slider}', ['uses' => 'SliderController@edit', 'as' => 'sliders.edit', 'title' => 'صفحة تعديل اعلان متحرك']);
      Route::post('sliders/store', ['uses' => 'SliderController@store', 'as' => 'sliders.store', 'title' => 'اضافة اعلان متحرك']);
      Route::post('sliders/{id}', ['uses' => 'SliderController@update', 'as' => 'sliders.update', 'title' => 'تعديل اعلان متحرك']);
      Route::delete('sliders/{id}', ['uses' => 'SliderController@destroy', 'as' => 'sliders.destroy', 'title' => 'حذف اعلان متحرك']);
      Route::post('sliders/active/change', ['uses' => 'SliderController@changeActive', 'as' => 'sliders.changeActive', 'title' => 'تنشيط اعلان متحرك']);
      /********************************* SliderController end *********************************/


      /********************************* BannerController start *********************************/

      Route::get('banners', ['uses' => 'BannerController@index', 'as' => 'banners.index', 'title' => 'البنرات الثابته', 'icon' => '<i class="feather icon-map"></i>']);
      Route::get('banners/create', ['uses' => 'BannerController@create', 'as' => 'banners.create', 'title' => 'صفحة اضافة بنر ثابت']);
      Route::get('banners/edit/{banner}', ['uses' => 'BannerController@edit', 'as' => 'banners.edit', 'title' => 'صفحة تعديل بنر ثابت']);
      Route::post('banners/store', ['uses' => 'BannerController@store', 'as' => 'banners.store', 'title' => 'اضافة بنر']);
      Route::post('banners/{id}', ['uses' => 'BannerController@update', 'as' => 'banners.update', 'title' => 'تعديل بنر']);
      Route::delete('banners/{id}', ['uses' => 'BannerController@destroy', 'as' => 'banners.destroy', 'title' => 'حذف بنر']);
      Route::post('banners/active/change', ['uses' => 'BannerController@changeActive', 'as' => 'banners.changeActive', 'title' => 'تنشيط']);
      /********************************* BannerController end *********************************/

//      employee

      Route::get('employees', ['uses' => 'EmployeeController@index', 'as' => 'employees.index', 'title' => 'مندوبين التسويق', 'icon' => '<i class="nav-icon fa fa-users"></i>']);
      Route::get('employees/create', ['uses' => 'EmployeeController@create', 'as' => 'employees.create', 'title' => 'صفحة اضافة مندوب تسويق']);
      Route::get('employees/edit/{employee}', ['uses' => 'EmployeeController@edit', 'as' => 'employees.edit', 'title' => 'صفحة تعديل مندوب تسويق']);
      Route::post('employees/store', ['uses' => 'EmployeeController@store', 'as' => 'employees.store', 'title' => 'اضافة مندوب تسويق']);
      Route::post('employees/{id}', ['uses' => 'EmployeeController@update', 'as' => 'employees.update', 'title' => 'تعديل مندوب تسويق']);
      /********************************* marketing end *********************************/
      /********************************* socials start *********************************/

      #index
      Route::get('media', [
          'as'        => 'media',
          'title'     => 'السوشيال ميديا',
          'icon'      => '<i class="fas fa-share-alt"></i>',
          'type'      => 'parent',
          'sub_route' => true,
          'child'     => ['socials.clients','socials.providers','socials.delegates','socials.companies','socials.providersPortal']
      ]);
      Route::get('socials/clients', ['uses' => 'SocialController@clients', 'as' => 'socials.clients', 'title' => 'تطبيق العميل', 'icon' => '<i class="nav-icon fas fa-mobile-alt"></i>']);
      Route::get('socials/providers', ['uses' => 'SocialController@providers', 'as' => 'socials.providers', 'title' => 'تطبيق مقدم خدمه', 'icon' => '<i class="nav-icon fas fa-mobile-alt"></i>']);
      Route::get('socials/delegates', ['uses' => 'SocialController@delegates', 'as' => 'socials.delegates', 'title' => 'تطبيق مندوب', 'icon' => '<i class="nav-icon fas fa-mobile-alt"></i>']);
      Route::get('socials/companies', ['uses' => 'SocialController@companies', 'as' => 'socials.companies', 'title' => 'بورتال الشركات المشغله', 'icon' => '<i class="nav-icon fas fa-mobile-alt"></i>']);
      Route::get('socials/providersPortal', ['uses' => 'SocialController@providersPortal', 'as' => 'socials.providersPortal', 'title' => 'بورتال مقدم الخدمه', 'icon' => '<i class="nav-icon fas fa-mobile-alt"></i>']);

      /*------------ start Of items Controller ----------*/
      #season page
    Route::get('items', [
      'uses'      => 'ItemController@index',
      'as'        => 'items.index',
      'title'     => 'المنتجات',
      'icon'      => '<i class="feather icon-package"></i>',
      'type'      => 'parent',
      'sub_route' => false,
      'child'     => [
        'items.store',
        'items.update',
        'items.create',
        'items.edit',
        'items.delete',
        'items.destroy',
        'items.delimage',
        'items.addFile',
        'items.saveFeatures',
        'items.deleteFeature',
        'items.changeMain',
        'items.deleteDetail',
        'items.deleteTypeOption',
        'items.deleteType',
        'items.saveItemTypes',
        'items.SellerCategories',
        'items.changeStatus',
      ]
    ]);
    Route::get('items/create', [
      'uses'  => 'ItemController@create',
      'as'    => 'items.create',
      'title' => 'اضافة المنتج',
    ]);
    Route::get('items/edit/{id}', [
      'uses'  => 'ItemController@edit',
      'as'    => 'items.edit',
      'title' => 'تعديل المنتج',
    ]);
    Route::put('items/update/{id}', [
      'uses'  => 'ItemController@update',
      'as'    => 'items.update',
      'title' => 'تحديث المنتج',
    ]);
    Route::post('items/changeStatus', [
      'uses'  => 'ItemController@changeStatus',
      'as'    => 'items.changeStatus',
      'title' => 'تغيير حالة المنتج',
    ]);
    Route::post('delimage', [
      'uses'  => 'ItemController@delimage',
      'as'    => 'items.delimage',
      'title' => 'حذف الصور',
    ]);
    Route::post('items/delete', [
      'uses'  => 'ItemController@delete',
      'as'    => 'items.delete',
      'title' => 'حذف المنتج',
    ]);
    Route::delete('items/delete/{id}', [
      'uses'  => 'ItemController@destroy',
      'as'    => 'items.destroy',
      'title' => 'حذف المنتج من الجدول',
    ]);
    Route::post('saveFeatures', [
      'uses'  => 'ItemController@saveFeatures',
      'as'    => 'items.saveFeatures',
      'title' => 'حفظ اضافه',
    ]);
    Route::post('deleteFeature', [
      'uses'  => 'ItemController@deleteFeature',
      'as'    => 'items.deleteFeature',
      'title' => 'حذف اضافه',
    ]);
    Route::post('deleteTypeOption', [
      'uses'  => 'ItemController@deleteTypeOption',
      'as'    => 'items.deleteTypeOption',
      'title' => 'حذف السمه',
    ]);
    Route::post('deleteType', [
      'uses'  => 'ItemController@deleteType',
      'as'    => 'items.deleteType',
      'title' => 'حذف السمه',
    ]);
    Route::post('deleteDetail', [
      'uses'  => 'ItemController@deleteDetail',
      'as'    => 'items.deleteDetail',
      'title' => 'حذف تفاصيل السمه',
    ]);
    Route::post('saveItemTypes', [
      'uses'  => 'ItemController@saveItemTypes',
      'as'    => 'items.saveItemTypes',
      'title' => 'حفظ السمات',
    ]);
    Route::post('delTypeDetails', [
      'uses'  => 'ItemController@delTypeDetails',
      'as'    => 'items.delTypeDetails',
      'title' => 'حذف السمه الفرعيه',
    ]);
    Route::post('files/change', [
      'uses'  => 'ItemController@changeMain',
      'as'    => 'items.changeMain',
      'title' => 'الصوره الرئيسيه',
    ]);
    Route::post('items/files', [
      'uses'  => 'ItemController@addFile',
      'as'    => 'items.addFile',
      'title' => 'اضافة صور',
    ]);
    Route::post('items/store', [
      'uses'  => 'ItemController@store',
      'as'    => 'items.store',
      'title' => 'حفظ المنتج',
    ]);
    Route::post('items/getSellerCategories', [
      'uses'  => 'ItemController@getSellerCategories',
      'as'    => 'items.SellerCategories',
      'title' => 'أقسام المتجر',
    ]);
    /********************************* AccountController start *********************************/
//    Route::get('accounting', [
//      'as'        => 'accounting',
//      'icon'      => '<i class="feather icon-file-text"></i>',
//      'title'     => 'الحسابات العامه',
//      'type'      => 'parent',
//      'sub_route' => true,
//      'child'     => [
//        'sellerAccount.index', 'sellerAccount.destroy', 'sellerAccount.accept', 'sellerAccount.refuse',
//        'delegateAccount.index', 'delegateAccount.destroy', 'delegateAccount.accept', 'delegateAccount.refuse',
//      ]
//    ]);
//
//    Route::get('sellerAccount', ['uses' => 'AccountController@sellerAccountIndex', 'as' => 'sellerAccount.index', 'title' => 'حسابات مقدمي الخدمه', 'icon' => '<i class="fa fa-globe"></i>']);
//    Route::post('sellerAccount/accept', ['uses' => 'AccountController@sellerAccountAccept', 'as' => 'sellerAccount.accept', 'title' => 'قبول سداد المقدم']);
//    Route::post('sellerAccount/refuse', ['uses' => 'AccountController@sellerAccountRefuse', 'as' => 'sellerAccount.refuse', 'title' => 'رفض سداد المقدم']);
//    Route::delete('sellerAccount/{id}', ['uses' => 'AccountController@sellerAccountDestroy', 'as' => 'sellerAccount.destroy', 'title' => 'حذف حسابات مقدم الخدمه']);
//
//    Route::get('delegateAccount', ['uses' => 'AccountController@delegateAccountIndex', 'as' => 'delegateAccount.index', 'title' => 'حسابات المندوبين', 'icon' => '<i class="fa fa-globe"></i>']);
//    Route::post('delegateAccount/accept', ['uses' => 'AccountController@delegateAccountAccept', 'as' => 'delegateAccount.accept', 'title' => 'قبول سداد المندوب']);
//    Route::post('delegateAccount/refuse', ['uses' => 'AccountController@delegateAccountRefuse', 'as' => 'delegateAccount.refuse', 'title' => 'رفض سداد المندوب']);
//    Route::delete('delegateAccount/{id}', ['uses' => 'AccountController@delegateAccountDestroy', 'as' => 'delegateAccount.destroy', 'title' => 'حذف حسابات المندوبين']);
    /********************************* AccountController end *********************************/

    /********************************* QuestionController start *********************************/
    Route::get('questions', [
      'uses'      => 'QuestionController@index',
      'as'        => 'questions.index',
      'title'     => ' الأسئله والأجوبه',
      'icon'      => '<i class="feather icon-check-square"></i>',
      'type'      => 'parent',
      'sub_route' => false,
      'child'     => ['questions.index', 'questions.store', 'questions.update', 'questions.destroy']
    ]);

    Route::post('questions/store', ['uses' => 'QuestionController@store', 'as' => 'questions.store', 'title' => 'اضافة سؤال']);
    Route::post('questions/{id}', ['uses' => 'QuestionController@update', 'as' => 'questions.update', 'title' => 'تعديل سؤال']);
    Route::delete('questions/{id}', ['uses' => 'QuestionController@destroy', 'as' => 'questions.destroy', 'title' => 'حذف سؤال']);
    /********************************* QuestionController end *********************************/

    /********************************* CountriesController start *********************************/

    Route::get('country', [
      'as'        => 'country',
      'icon'      => '<i class="nav-icon fa fa-globe"></i>',
      'title'     => ' ادارة الدول والمدن',
      'type'      => 'parent',
      'sub_route' => true,
      'child'     => [
        'countries.index', 'countries.store', 'countries.update', 'countries.destroy',
        'cities.index', 'cities.store', 'cities.update', 'cities.destroy',
        'zones.index', 'zones.store', 'zones.edit.map', 'zones.update.map', 'zones.update', 'zones.destroy'
      ]
    ]);

    Route::get('countries', ['uses' => 'CountriesController@index', 'as' => 'countries.index', 'title' => 'الدول', 'icon' => '<i class="nav-icon fa fa-flag"></i>']);
    Route::post('countries/store', ['uses' => 'CountriesController@store', 'as' => 'countries.store', 'title' => 'اضافة دوله']);
    Route::post('countries/{id}', ['uses' => 'CountriesController@update', 'as' => 'countries.update', 'title' => 'تعديل دوله']);
    Route::delete('countries/{id}', ['uses' => 'CountriesController@destroy', 'as' => 'countries.destroy', 'title' => 'حذف دوله']);
    /********************************* CountriesController end *********************************/

    /********************************* CityController start *********************************/
    Route::get('cities', [
      'uses'  => 'CityController@index',
      'as'    => 'cities.index',
      'title' => ' المدن',
      'icon'  => '<i class="nav-icon fa fa-flag"></i>',
    ]);
    Route::post('cities/store', ['uses' => 'CityController@store', 'as' => 'cities.store', 'title' => 'اضافة مدينه']);
    Route::post('cities/{id}', ['uses' => 'CityController@update', 'as' => 'cities.update', 'title' => 'تعديل مدينه']);
    Route::delete('cities/{id}', ['uses' => 'CityController@destroy', 'as' => 'cities.destroy', 'title' => 'حذف مدينه']);
    /********************************* CityController end *********************************/

    /********************************* ZoneController start *********************************/
    Route::get('zones', [
      'uses'  => 'ZoneController@index',
      'as'    => 'zones.index',
      'title' => 'النطاق الجغرافي',
      'icon'  => '<i class="nav-icon fa fa-flag"></i>',
    ]);
    Route::post('zones/store', ['uses' => 'ZoneController@store', 'as' => 'zones.store', 'title' => 'اضافة نطاق جغرافي']);
    Route::get('zones/{id}/map', ['uses' => 'ZoneController@getZoneMap', 'as' => 'zones.edit.map', 'title' => 'عرض خريطة نطاق جغرافي']);
    Route::post('zones/update-map', ['uses' => 'ZoneController@updateZoneMap', 'as' => 'zones.update.map', 'title' => 'تحديث خريطة نطاق جغرافي']);
    Route::post('zones/{id}', ['uses' => 'ZoneController@update', 'as' => 'zones.update', 'title' => 'تعديل نطاق جغرافي']);
    Route::delete('zones/{id}', ['uses' => 'ZoneController@destroy', 'as' => 'zones.destroy', 'title' => 'حذف نطاق جغرافي']);
    /********************************* ZoneController end *********************************/

    /********************************* PageController start *********************************/
    Route::get('pages', [
      'uses'      => 'PageController@index',
      'as'        => 'pages.index',
      'title'     => ' الصفحات الرئيسيه',
      'icon'      => '<i class="feather icon-layout"></i>',
      'type'      => 'parent',
      'sub_route' => false,
      'child'     => ['pages.index', 'pages.update']
    ]);
    Route::post('pages/{id}', ['uses' => 'PageController@update', 'as' => 'pages.update', 'title' => 'تعديل صفحه']);
    /********************************* PageController end *********************************/

    /********************************* SuggestController start *********************************/
    Route::get('suggestsComplaints', [
      'as'        => 'suggests',
      'title'     => 'اقتراحات وشكاوي',
      'icon'      => '<i class="feather icon-life-buoy"></i>',
      'type'      => 'parent',
      'sub_route' => true,
      'child'     => ['suggests.index', 'suggests.destroy','complaints.index', 'complaints.destroy']
    ]);

    Route::get('suggests', ['uses' => 'SuggestController@index', 'as' => 'suggests.index', 'title' => 'اقتراحات','icon' => '<i class="feather icon-life-buoy"></i>']);
    Route::delete('suggests/{id}', ['uses' => 'SuggestController@destroy', 'as' => 'suggests.destroy', 'title' => 'حذف اقتراح']);
    Route::get('complaints', ['uses' => 'ComplaintController@index', 'as' => 'complaints.index', 'title' => 'شكاوي','icon' => '<i class="feather icon-life-buoy"></i>']);
    Route::delete('complaints/{id}', ['uses' => 'ComplaintController@destroy', 'as' => 'complaints.destroy', 'title' => 'حذف شكوي']);

      /********************************* SuggestController end *********************************/

    /********************************* joinUsController start *********************************/
    Route::get('join', [
      'as'        => 'joinUs',
      'title'     => ' طلبات الانضمام',
      'icon'      => '<i class="nav-icon fa fa-envelope"></i>',
      'type'      => 'parent',
      'sub_route' => true,
      'child'     => ['joinUs.delegeteJoinsIndex', 'joinUs.delegeteJoinsDestroy', 'joinUs.employeeJoinsIndex', 'joinUs.employeeJoinsDestroy']
    ]);
    Route::get('delegeteJoins', ['uses' => 'joinUsController@delegeteJoinsIndex', 'as' => 'joinUs.delegeteJoinsIndex', 'title' => 'طلب انضمام مندوب', 'icon' => '<i class="nav-icon fa fa-envelope"></i>']);
    Route::delete('delegeteJoins/{id}', ['uses' => 'joinUsController@delegeteJoinsDestroy', 'as' => 'joinUs.delegeteJoinsDestroy', 'title' => 'حذف انضمام مندوب']);

    Route::get('employeeJoin', ['uses' => 'joinUsController@employeeJoinsIndex', 'as' => 'joinUs.employeeJoinsIndex', 'title' => 'طلب انضمام موظف', 'icon' => '<i class="nav-icon fa fa-envelope"></i>']);
    Route::delete('employeeJoin/{id}', ['uses' => 'joinUsController@employeeJoinsDestroy', 'as' => 'joinUs.employeeJoinsDestroy', 'title' => 'حذف انضمام موظف']);

    /********************************* ContactController end *********************************/

    /********************************* ContactController start *********************************/
    Route::get('contacts', [
      'uses'      => 'ContactController@index',
      'as'        => 'contacts.index',
      'title'     => ' تواصل معنا',
      'icon'      => '<i class="feather icon-mail"></i>',
      'type'      => 'parent',
      'sub_route' => false,
      'child'     => ['contacts.index', 'contacts.update', 'contacts.destroy']
    ]);
    Route::post('contacts/{id}', ['uses' => 'ContactController@update', 'as' => 'contacts.update', 'title' => 'تعديل تواصل معنا']);
    Route::delete('contacts/{id}', ['uses' => 'ContactController@destroy', 'as' => 'contacts.destroy', 'title' => 'حذف تواصل معنا']);
    /********************************* ContactController end *********************************/

    /********************************* ReportController start *********************************/
    Route::get('reports', [
      'uses'      => 'ReportController@index',
      'as'        => 'reports.index',
      'title'     => ' تقارير لوحة التحكم',
      'icon'      => '<i class="feather icon-pie-chart"></i>',
      'type'      => 'parent',
      'sub_route' => false,
      'child'     => ['reports.index', 'reports.delete']
    ]);
    Route::delete('reports/{id}', ['uses' => 'ReportController@destroy', 'as' => 'reports.delete', 'title' => 'حذف تقرير']);
    /********************************* ReportController end *********************************/
    Route::get('/notifications', [
      'uses'      => 'NotificationController@index',
      'as'        => 'notifications.index',
      'title'     => 'الاشعارات',
      'icon'      => '<i class="feather icon-bell"></i>',
      'type'      => 'parent',
      'sub_route' => false,
      'child'     => [
        'notification.create',
        'notification.store_sms',
        'notification.store_email',
        'notification.store_notification',
      ]
    ]);
    Route::get('/notifications/create', ['uses' => 'NotificationController@create', 'as' => 'notification.create', 'title' => 'اضافة اشعار']);
    Route::post('/notifications/store-sms', ['uses' => 'NotificationController@sendSmsMessage', 'as' => 'notification.store_sms', 'title' => 'اشعار بالرساله sms']);
    Route::post('/notifications/store-email', ['uses' => 'NotificationController@sendEmail', 'as' => 'notification.store_email', 'title' => 'اشعار بالبريد الالكتروني']);
    Route::post('/notifications/notification/store', ['uses' => 'NotificationController@adminSendNotification', 'as' => 'notification.store_notification', 'title' => 'حفظ الاشعار']);
    /********************************* TransController start *********************************/
    Route::get('trans', [
      'uses'      => 'TransController@index',
      'as'        => 'trans.index',
      'title'     => ' الترجمات',
      'icon'      => '<i class="feather icon-flag"></i>',
      'type'      => 'parent',
      'sub_route' => false,
      'child'     => ['trans.getLangDetails', 'trans.transInput']
    ]);
    Route::post('getLangDetails', ['uses' => 'TransController@getLangDetails', 'as' => 'trans.getLangDetails', 'title' => 'احضار ترجمه']);
    Route::post('transInput', ['uses' => 'TransController@transInput', 'as' => 'trans.transInput', 'title' => 'حفظ ترجمه']);
    /********************************* TransController end *********************************/
  });
});


//    ajax helper
Route::any('/admin/getGroups', 'AjaxController@getGroups')->name('admin.ajax.getGroups');
Route::any('/admin/getCategories', 'AjaxController@getCategories')->name('admin.ajax.getCategories');
Route::any('/admin/getItems', 'AjaxController@getItems')->name('admin.ajax.getItems');
Route::get('/admin/getSellers', 'AjaxController@getSellers')->name('admin.ajax.getSellers');
Route::post('/admin/getProviders', 'AjaxController@getProviders')->name('admin.ajax.getProviders');
Route::post('/admin/cities', 'AjaxController@getCities')->name('admin.ajax.getCities');
Route::post('/admin/cityZones', 'AjaxController@getZones')->name('admin.ajax.getZones');
Route::any('/admin/changeAccepted', 'AjaxController@changeAccepted')->name('ajax.changeAccepted');
Route::any('/admin/getClientCount', 'AjaxController@getClientCount')->name('admin.ajax.getClientCount');
Route::any('/admin/getDelegates', 'AjaxController@getDelegates')->name('admin.ajax.getDelegates');
Route::any('/admin/getDelegatesCarType', 'AjaxController@getDelegatesCarType')->name('admin.ajax.getDelegatesCarType');
Route::any('/admin/getBranches', 'AjaxController@getBranches')->name('admin.ajax.getBranches');
Route::any('/admin/getAdminByRole', 'AjaxController@getAdminByRole')->name('admin.ajax.getAdminByRole');
Route::any('/admin/getUserByZone', 'AjaxController@getUserByZone')->name('admin.ajax.getUserByZone');


//    ajax helper
Route::post('/admin/cities', 'AjaxController@getCities')->name('admin.ajax.getCities');
