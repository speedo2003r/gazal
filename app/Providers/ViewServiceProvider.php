<?php

namespace App\Providers;

use App\Exceptions\ApplicationIsCompletedException;
use App\Http\View\Composers\NotificationComposer;
use App\Http\ViewComposers\ApplicationDirectionViewComposer;
use App\Models\Contact;
use App\Models\Setting;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            View::composer('admin.partial.navbar', NotificationComposer::class);
            View::composer('*', ApplicationDirectionViewComposer::class);

        } catch (\Exception $exception) {
            // new ApplicationIsCompletedException($exception->getMessage());
        }
    }
}
