<?php

namespace App\DataTables;

use App\Entities\Item;
use App\Entities\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class OrderSellerDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('order_num', '<a href="{{route("provider.showOrders",$id)}}">{{$order_num}}</a>')
            ->editColumn('order_status', '{{ __("order.".$order_status) }}')
            ->addColumn('price',function ($query){
                return ($query->_price());
            })
            ->rawColumns(['price','order_num','id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Entities\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Request $request, Order $model)
    {
        $model = $model->newQuery()->latest()->with(['branch'])->where([['provider_id', currentUser()->id], ['live', 1]])->when(!is_null($request->order_status), function ($q) use ($request) {
            $q->where('order_status', $request->order_status);
        })->when(!is_null($request->branch), function ($q) use ($request) {
            $q->where('branch_id', $request->branch);
        });

        if ($request->routeIs('provider.orders.index')) {
            return $model
                ->whereNotIn('order_status',[Order::STATUS_REFUSED, Order::STATUS_CANCELED, Order::STATUS_FINISH]);
        } else {
            return $model
                ->whereIn('order_status',[Order::STATUS_REFUSED, Order::STATUS_CANCELED, Order::STATUS_FINISH]);
        }

        /*if(Route::currentRouteName() == 'provider.newOrders'){
            return $model
                ->where('order_status',Order::STATUS_WAITING)
                ->where('provider_status',null);
        }elseif (Route::currentRouteName() == 'provider.pickupOrders'){
            return $model
                ->where('order_status','onWay')
                ->where('provider_status','shipped');
        }elseif (Route::currentRouteName() == 'provider.finishOrders'){
            return $model
                ->where('order_status','done')
                ->where('provider_status','delivered');
        }elseif (Route::currentRouteName() == 'provider.pendingOrders'){
            return $model
                ->where('order_status','');
        }elseif (Route::currentRouteName() == 'provider.refuseOrders'){
            return $model
                ->where('order_status',Order::STATUS_REFUSED)
                ->where('provider_status','refused');
        }*/

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                   // ->setTableId('orderdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'paging'   => true,
                        'bLengthChange'   => true,
                        'bInfo'   => true,
                        'responsive'   => true,
                        'dom' => 'Blfrtip',
                        //'lengthMenu' => [
                        //    [10,25,50,100],[10,25,50,100]
                        //],
                        'buttons' => [
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                            ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                            ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                        ],
                        "language" =>  datatableTrans(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('order_num')->title(__('admin.orderNumber')),
            Column::make('date')->title(__('date')),
            Column::make('time')->title(__('time')),
            Column::make('branch.name')->title(__('Branch')),
            Column::make('price')->title(__('admin.total')),
            Column::make('order_status')->title(__('status')),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
