<?php

namespace App\DataTables;

use App\Entities\Item;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ItemSellerDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)

            ->editColumn('created_at',function ($query){
                return $query['created_at']->toDateString();
            })
            ->addColumn('afterDiscount',function ($query){
                return $query->price();
            })
            ->addColumn('discount',function ($query){
                return ((int) $query->discount());
            })
            ->addColumn('sellingCount',function ($query){
                if($query['sellingCount'] == null){
                    return 0;
                }
                return $query['sellingCount'];
            })
            ->addColumn('sellingPrice',function ($query){
                if($query['sellingPrice'] == null){
                    return 0;
                }
                return $query['sellingPrice'];
            })
            ->addColumn('status',function ($query){
                if($query->status == 0){
                    return '<span class="badge badge-danger">'.__('admin.underVision').'</span>';
                }elseif($query->status == 1){
                    return '<span class="badge badge-success">'.__('admin.online').'</span>';
                }elseif($query->status == 3){
                    return '<span class="badge badge-warning">'.__('admin.broken').'</span>';
                }
            })
            ->addColumn('item',function ($query){
                return '<img style="width:100px" src="'.(isset($query->main_image) ? $query->main_image : dashboard_url('images/placeholder.png')).'">
                <span>'.$query['title'].'</span>';
            })
            ->addColumn('actions', 'provider.item.buttons.actions')
            ->rawColumns(['afterDiscount','discount','status','sellingCount', 'sellingPrice', 'item','actions']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Entities\Item $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Item $model)
    {
        return $model->newQuery()->where('user_id', currentUser()->id)
                                 ->withCount('orders')
                                 ->withSum('orders', 'price')
                                 ->latest();

        if(request()->query('type') == null){
            return $model->query()->select('items.*','selling_view.item_id',DB::raw('SUM(selling_view.count) as sellingCount'),DB::raw('SUM(selling_view.price * selling_view.count) as sellingPrice'),'providers.store_name as store_name')
                ->leftJoin('selling_view','selling_view.item_id','=','items.id')
                ->leftJoin('providers','providers.user_id','=','items.user_id')
                ->groupBy('items.id','items.title','items.description', 'items.price', 'items.discount_price', 'items.from', 'items.to', 'items.user_id', 'items.exist', 'items.views', 'items.status', 'items.category_id', 'items.subcategory_id', 'items.created_at', 'items.updated_at', 'items.deleted_at', 'providers.store_name')
                ->where('items.user_id',auth('provider')->id())
                ->latest();
        }elseif(request()->query('type') == 'online'){
            return $model->query()->select('items.*','selling_view.item_id',DB::raw('SUM(selling_view.count) as sellingCount'),DB::raw('SUM(selling_view.price * selling_view.count) as sellingPrice'),'providers.store_name as store_name')
                ->leftJoin('selling_view','selling_view.item_id','=','items.id')
                ->leftJoin('providers','providers.user_id','=','items.user_id')
                ->groupBy('items.id','items.title','items.description', 'items.price', 'items.discount_price', 'items.from', 'items.to', 'items.user_id', 'items.exist', 'items.views', 'items.status', 'items.category_id', 'items.subcategory_id', 'items.created_at', 'items.updated_at', 'items.deleted_at', 'providers.store_name')
                ->where('items.user_id',auth('provider')->id())
                ->where('items.status',1)->latest();
        }elseif(request()->query('type') == 'notActive'){
            return $model->query()->select('items.*','selling_view.item_id',DB::raw('SUM(selling_view.count) as sellingCount'),DB::raw('SUM(selling_view.price * selling_view.count) as sellingPrice'),'providers.store_name as store_name')
                ->leftJoin('selling_view','selling_view.item_id','=','items.id')
                ->leftJoin('providers','providers.user_id','=','items.user_id')
                ->groupBy('items.id','items.title','items.description', 'items.price', 'items.discount_price', 'items.from', 'items.to', 'items.user_id', 'items.exist', 'items.views', 'items.status', 'items.category_id', 'items.subcategory_id', 'items.created_at', 'items.updated_at', 'items.deleted_at', 'providers.store_name')
                ->where('items.user_id',auth('provider')->id())
                ->where('items.status',0)->latest();
        }elseif(request()->query('type') == 'banned'){
            return $model->query()->select('items.*','selling_view.item_id',DB::raw('SUM(selling_view.count) as sellingCount'),DB::raw('SUM(selling_view.price * selling_view.count) as sellingPrice'),'providers.store_name as store_name')
                ->leftJoin('selling_view','selling_view.item_id','=','items.id')
                ->leftJoin('providers','providers.user_id','=','items.user_id')
                ->groupBy('items.id','items.title','items.description', 'items.price', 'items.discount_price', 'items.from', 'items.to', 'items.user_id', 'items.exist', 'items.views', 'items.status', 'items.category_id', 'items.subcategory_id', 'items.created_at', 'items.updated_at', 'items.deleted_at', 'providers.store_name')
                ->where('items.user_id',auth()->id('provider'))
                ->where('items.status',1)->where('items.exist',0)->latest();
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('itemdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->parameters([
                        'lengthMenu' => [
                            [10,25,50,100],[10,25,50,100]
                        ],
                        'buttons' => [
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                            ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                            ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                        ],
                        "language" =>  datatableTrans(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            [
                'name' => 'id',
                'data' => 'id',
                'title' => 'ID',
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => false,
                'width'          => '10px',
                'aaSorting'      => 'none'
            ],

            [
                'name'=>'item',
                'data'=>'item',
                'title'=> __('item'),
            ],

            [
                'name'=>'price',
                'data'=>'price',
                'title'=> __('Price'),
            ],

            [
                'name'=>'discount',
                'data'=>'discount',
                'title'=>trans('Discount'),
            ],

            [
                'name'=>'afterDiscount',
                'data'=>'afterDiscount',
                'title'=>trans('Price after discount'),
            ],

            [
                'name'=>'orders_count',
                'data'=>'orders_count',
                'title'=>trans('admin.countSellingParts'),
            ],

            [
                'name'=>'orders_sum_price',
                'data'=>'orders_sum_price',
                'title'=>trans('admin.totalSelling'),
            ],

            [
                'name' => 'created_at',
                'data' => 'created_at',
                'title' => trans('Created At'),
                'exportable' => false,
                'printable'  => false,
                'searchable' => false,
                'orderable'  => false,
            ],
            [
                'name' => 'actions',
                'data' => 'actions',
                'title' => __('Actions'),
                'exportable' => false,
                'printable'  => false,
                'searchable' => false,
                'orderable'  => false,
            ],
        ];

    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
