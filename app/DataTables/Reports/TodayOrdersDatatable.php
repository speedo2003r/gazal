<?php

namespace App\DataTables\Reports;

use App\Services\ReportService;use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TodayOrdersDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('id',function ($query){
                return '<label class="custom-control material-checkbox" style="margin: auto">
                            <input type="checkbox" class="material-control-input checkSingle" id="'.$query->id.'">
                            <span class="material-control-indicator"></span>
                        </label>';
            })
            ->addColumn('url',function ($query){
                return 'admin.orders.destroy';
            })
            // Get total in footer
            //->with('total', function() use ($query) {
            //    return $query->sum('amount');
            //})

            ->rawColumns(['status','id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Services\ReportService $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ReportService $model)
    {
        return $model->todayOrders(['provider_id' => currentUser()->id]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('orderdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->parameters([
                        'lengthMenu' => [
                            [10,25,50,100],[10,25,50,100]
                        ],
                        'buttons' => [
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('Excel')],
                            ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('Print')],
                            ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('Copy')],
                        ],
                        "language" =>  datatableTrans(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('')->orderable(false),
            Column::make('order_num')->title(__('Order no.')),
            Column::make('branch.name')->title(__('Branch')),
            Column::make('shipping_price')->title(__('Shipping cost')),
            Column::make('final_total')->title(__('Total')),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Reports_TodayOrders_' . date('YmdHis');
    }
}
