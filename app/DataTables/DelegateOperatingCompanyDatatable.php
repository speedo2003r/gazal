<?php

namespace App\DataTables;

use App\Entities\Company;
use App\Entities\Delegate;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class DelegateOperatingCompanyDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('rate',function ($query){
                return '<div class="Stars" style="--rating: '.round($query->ratings->sum('rate') / $query->ratings->count(),2).';">';
            })
            ->rawColumns(['rate']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClientDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Delegate $model)
    {
        return $model->query()->where('company_id',$this->company_id)
            ->select('delegates.*','car_types.title->ar as title_ar')
            ->leftJoin('review_rates','review_rates.rateable_id','=','delegates.id')
            ->leftJoin('car_types','car_types.id','=','delegates.car_type_id')
            ->where('review_rates.rateable_type',Delegate::class)
            ->groupBy('delegates.id')
            ->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('delegatedatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('<"top"<"actions">Blfrtip<"clear">>rt<"bottom"iflp<"clear">>')
                    ->parameters([
                        'buttons' => [
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                            ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                            ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                        ],
                        "language" =>  datatableTrans(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')->title(__('name')),
            Column::make('email')->title(__('email')),
            Column::make('phone')->title(__('phone')),
            Column::make('title_ar')->name('title_ar')->title(__('admin.carType')),
            Column::make('rate')->title(__('rating')),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
