<?php

namespace App\DataTables;

use App\Entities\EmployeeJoinUs;
use App\Entities\JoinUs;
use App\Models\User;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class EmployeeJoinUsDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('id',function ($query){
                return '<label class="custom-control material-checkbox" style="margin: auto">
                            <input type="checkbox" class="material-control-input checkSingle" id="'.$query->id.'">
                            <span class="material-control-indicator"></span>
                        </label>';
            })
            ->addColumn('url',function ($query){
                return 'admin.employeeJoins.destroy';
            })
            ->addColumn('target',function ($query){
                return 'addModel';
            })
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('control','admin.partial.ControlContact')
            ->rawColumns(['control','id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClientDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(EmployeeJoinUs $model)
    {
        return $model->query()->select('name','phone','email','job_description','desc','cv','cities.title->ar as city_title','employee_join_us.created_at')
            ->leftJoin('cities','cities.id','employee_join_us.city_id')
            ->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('joindatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Blfrtip')
            ->parameters([
                'lengthMenu' => [
                    [10,25,50,100],[10,25,50,100]
                ],
                'buttons' => [
                    ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                    ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                    ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                ],
                "language" =>  datatableTrans(),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('')->orderable(false),
            Column::make('name')->title(__('name')),
            Column::make('phone')->title(__('phone')),
            Column::make('email')->title(__('email')),
            Column::make('job_description')->title(__('admin.job')),
            Column::make('desc')->title(__('admin.description')),
            Column::make('cv')->title(__('cv')),
            Column::make('control')->title(__('control'))->orderable(false)->searchable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
