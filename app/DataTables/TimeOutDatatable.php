<?php

namespace App\DataTables;

use App\Entities\JoinUs;
use App\Entities\Order;
use App\Entities\Provider;
use App\Models\User;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TimeOutDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('showUrl',function ($query){
                return 'admin.providers.show';
            })
            ->addColumn('editUrl',function ($query){
                return 'admin.providers.edit';
            })
            ->addColumn('control','admin.partial.Control')
            ->rawColumns(['banned','control']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClientDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Provider $model)
    {
        return $model->query()->whereHas('orders',function ($order){
            $order->where('order_status',Order::STATUS_WAITING);
            $order->where(function ($q){
                $q->whereTime('progress_end','<',Carbon::now()->format('H:i:s'))->whereDate('created_date','=',Carbon::now()->format('Y-m-d'));
                $q->OrWhereDate('created_date','<',Carbon::now()->format('Y-m-d'));
            });
            $order->where('accepted_date',null);
        })->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('closeddatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Blfrtip')
            ->parameters([
                'lengthMenu' => [
                    [10,25,50,100],[10,25,50,100]
                ],
                'buttons' => [
                    ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                    ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                    ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                ],
                "language" =>  datatableTrans(),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('')->orderable(false),
            Column::make('name')->title(__('name')),
            Column::make('email')->title(__('email')),
            Column::make('phone')->title(__('phone')),
            Column::make('control')->title(__('control'))->orderable(false)->searchable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
