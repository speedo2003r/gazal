<?php

namespace App\DataTables;

use App\Entities\Banner;
use App\Entities\Offer;
use App\Entities\Slider;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class OfferDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('id',function ($query){
                return '<label class="custom-control material-checkbox" style="margin: auto">
                            <input type="checkbox" class="material-control-input checkSingle" id="'.$query->id.'">
                            <span class="material-control-indicator"></span>
                        </label>';
            })
            ->editColumn('active',function ($query){
                return '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success" style="direction: ltr">
                            <input type="checkbox" onchange="changeActive('.$query->id.')" '.($query->active == 1 ? 'checked' : '') .' class="custom-control-input" id="customSwitch'.$query->id.'">
                            <label class="custom-control-label" id="status_label'.$query->id.'" for="customSwitch'.$query->id.'"></label>
                        </div>';
            })
            ->editColumn('title',function ($query){
                $lang = app()->getLocale();
                return $query->getTranslation('title', $lang);
            })
            ->editColumn('image',function ($query){
                return '<div style="width: 140px;height: 140px"><a href="'.$query['image'].'"  data-fancybox data-caption="'.$query['image'].'" ><img src="'.$query['image'].'" style="border-radius: 50%;width: 100%;height: 100%"></a></div>';
            })
            ->addColumn('url',function ($query){
                return 'admin.offers.destroy';
            })
            ->addColumn('data',function ($query){
                return $query;
            })
            ->editColumn('created_at',function ($query){
                return date('Y-m-d H:i a',strtotime($query['created_at']));
            })
            ->editColumn('start_date',function ($query){
                return date('Y-m-d',strtotime($query['start_date']));
            })
            ->editColumn('end_date',function ($query){
                return date('Y-m-d',strtotime($query['end_date']));
            })
            ->addColumn('page',function ($query){
                return $query['page'] == 'special' ? awtTrans('العروض المميزه') : awtTrans('صفحة العروض');
            })
            ->addColumn('edit',function ($query){
                return 'admin.offers.edit';
            })
            ->addColumn('control','admin.partial.ControlInEditDel')
            ->rawColumns(['edit','page','image','active','control','id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\SliderDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Offer $model)
    {
        return $model->query()->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('offerdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->parameters([
                        'lengthMenu' => [
                            [10,25,50,100],[10,25,50,100]
                        ],
                        'buttons' => [
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                            ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                            ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                        ],
                        "language" =>  datatableTrans(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('')->orderable(false),
            Column::make('title')->title(__('name')),
            Column::make('page')->title(__('page')),
            Column::make('image')->title(__('avatar')),
            Column::make('start_date')->title(__('from')),
            Column::make('end_date')->title(__('to')),
            Column::make('active')->title(__('Active / inactive')),
            Column::make('created_at')->title(__('created_at')),
            Column::make('control')->title(__('control'))->orderable(false)->searchable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ContactUs_' . date('YmdHis');
    }
}
