<?php

namespace App\DataTables;

use App\Entities\Delegate;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class DelegateDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('id',function ($query){
                return '<label class="custom-control material-checkbox" style="margin: auto">
                            <input type="checkbox" class="material-control-input checkSingle" id="'.$query->id.'">
                            <span class="material-control-indicator"></span>
                        </label>';
            })
            ->addColumn('accept',function ($query){
                return '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success" style="direction: ltr">
                            <input type="checkbox" onchange="changeUserAccepted('.$query->id.',`delegate`)" '.($query->accepted == 1 ? 'checked' : '') .' class="custom-control-input" id="customSwitch2'.$query->id.'">
                            <label class="custom-control-label" id="status_label2'.$query->id.'" for="customSwitch2'.$query->id.'"></label>
                        </div>';
            })
            ->addColumn('url',function ($query){
                return 'admin.delegates.delete';
            })
            ->addColumn('editUrl',function ($query){
                return 'admin.delegates.edit';
            })
            ->addColumn('showUrl',function ($query){
                return 'admin.delegates.show';
            })
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('images',function ($query){
                return '<a href="'.route('admin.delegates.images',$query['id']).'" class="btn btn-success">عرض المستندات</a>';
            })
            ->addColumn('control','admin.partial.Control')
            ->rawColumns(['accept','images','control','id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClientDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Delegate $model)
    {
        return $model->query()->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('delegatedatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('<"top"<"actions">Blfrtip<"clear">>rt<"bottom"iflp<"clear">>')
                    ->parameters([
                        'lengthMenu' => [
                            [10,25,50,100],[10,25,50,100]
                        ],
                        'buttons' => [
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                            ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                            ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                        ],
                        "language" =>  datatableTrans(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('')->orderable(false),
            Column::make('name')->title(__('name')),
            Column::make('accept')->title(__('admin.activate'))->searchable(false),
            Column::make('images')->title(__('admin.Photo Gallery and documents')),
            Column::make('email')->title(__('email')),
            Column::make('wallet')->title(__('wallet')),
            Column::make('phone')->title(__('phone')),
            Column::make('control')->title(__('control'))->orderable(false)->searchable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
