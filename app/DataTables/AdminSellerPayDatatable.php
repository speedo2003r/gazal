<?php

namespace App\DataTables;

use App\Entities\Item;
use App\Entities\Order;
use App\Entities\SellerPay;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AdminSellerPayDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at',function ($query){
                return date('Y-m-d',strtotime($query['created_at']));
            })
            ->addColumn('store_name',function ($query){
                $user = User::find($query['user_id']);
                return $user->provider['store_name'];
            })
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('control','admin.partial.StatusSellerPay')
            ->rawColumns(['store_name','control']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClientDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(SellerPay $model)
    {
        return $model->query()->select('seller_pays.parent_id','seller_pays.acc_owner_name','seller_pays.acc_number','seller_pays.bank_name','seller_pays.price','seller_pays.image','seller_pays.type as type','incomes.status','incomes.id as id','incomes.debtor as debtor','seller_pays.user_id','seller_pays.created_at as created_at','seller_pays.price as amount','users.name','providers.store_name','orders.id as order_id','orders.order_num as order_num','orders.final_total as final_total','orders.created_date as created_date')
            ->where('seller_pays.pay_status','!=',null)
            ->where('seller_pays.parent_id','=',null)
            ->leftJoin('incomes','seller_pays.income_id','=','incomes.id')
            ->leftJoin('orders','orders.id','=','seller_pays.order_id')
            ->leftJoin('users','users.id','=','seller_pays.user_id')
            ->leftJoin('providers','providers.user_id','=','users.id')
            ->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('adminsellerpaydatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->parameters([
                        'lengthMenu' => [
                            [10,25,50,100,-1],[10,25,50,100,'عرض الكل']
                        ],
                        'buttons' => [
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => awtTrans('ملف Excel')],
                            ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => awtTrans('طباعه')],
                            ['extend' => 'copy','className' => 'btn btn-success' , 'text' => awtTrans('نسخ')],
                        ],
                        "language" =>  datatableTrans(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->name('orders.order_num')->title(awtTrans('رقم الطلب')),
            Column::make('name')->name('users.name')->title(awtTrans('اسم العميل')),
            Column::make('store_name')->title(awtTrans('اسم المتجر')),
            Column::make('created_at')->name('orders.created_date')->title(awtTrans('تاريخ الطلب')),
            Column::make('debtor')->name('incomes.debtor')->title(awtTrans('عمولة الموقع')),
            Column::make('type')->name('seller_pays.type')->title(awtTrans('وسيلة الدفع')),
            Column::make('control')->title(awtTrans('التحكم')),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
