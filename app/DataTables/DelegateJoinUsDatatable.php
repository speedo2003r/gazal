<?php

namespace App\DataTables;

use App\Entities\DelegateJoinUs;
use App\Entities\JoinUs;
use App\Models\User;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class DelegateJoinUsDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('id',function ($query){
                return '<label class="custom-control material-checkbox" style="margin: auto">
                            <input type="checkbox" class="material-control-input checkSingle" id="'.$query->id.'">
                            <span class="material-control-indicator"></span>
                        </label>';
            })
            ->addColumn('url',function ($query){
                return 'admin.delegeteJoins.destroy';
            })
            ->addColumn('target',function ($query){
                return 'addModel';
            })
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('control','admin.partial.ControlContact')
            ->rawColumns(['control','id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClientDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(DelegateJoinUs $model)
    {
        return $model->query()
            ->select('name','phone','phone2','email','birth_date','car_types.title->ar as car_title','cities.title->ar as city_title','delegate_join_us.created_at')
            ->leftJoin('cities','cities.id','delegate_join_us.city_id')
            ->leftJoin('car_types','car_types.id','delegate_join_us.car_type_id')
            ->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('joindatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Blfrtip')
            ->parameters([
                'lengthMenu' => [
                    [10,25,50,100],[10,25,50,100]
                ],
                'buttons' => [
                    ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                    ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                    ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                ],
                "language" =>  datatableTrans(),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('')->orderable(false),
            Column::make('name')->title(__('name')),
            Column::make('phone')->title(__('phone')),
            Column::make('phone2')->title(__('emergencyPhone')),
            Column::make('email')->title(__('email')),
            Column::make('birth_date')->title(__('birth_date')),
            Column::make('car_title')->title(__('vehicle')),
            Column::make('city_title')->title(__('City')),
            Column::make('control')->title(__('control'))->orderable(false)->searchable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
