<?php

namespace App\DataTables;

use App\Entities\Item;
use App\Entities\Order;
use App\Entities\SellerPay;
use App\Entities\TopList;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TopListDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('edit',function ($query){
                return 'admin.topLists.edit';
            })
            ->addColumn('url',function ($query){
                return 'admin.topLists.destroy';
            })
            ->addColumn('control','admin.partial.ControlInEditDel')
            ->rawColumns(['control']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClientDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TopList $model)
    {
        return $model->query()
            ->select('top_lists.*','providers.store_name->ar as name')
            ->leftJoin('providers','top_lists.provider_id','=','providers.id')
            ->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('toplistdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->parameters([
                        'lengthMenu' => [
                            [10,25,50,100],[10,25,50,100]
                        ],
                        'buttons' => [
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                            ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                            ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                        ],
                        "language" =>  datatableTrans(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')->name('providers.store_name')->title(__('nameOfProvider')),
            Column::make('sort')->title(__('sort')),
            Column::make('from')->title(__('from')),
            Column::make('to')->title(__('to')),
            Column::make('control')->title(__('control')),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
