<?php

namespace App\DataTables;

use App\Entities\JoinUs;
use App\Entities\Provider;
use App\Models\User;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ClosedDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('showUrl',function ($query){
                return 'admin.providers.show';
            })
            ->addColumn('editUrl',function ($query){
                return 'admin.providers.edit';
            })
            ->addColumn('banned',function ($query){
                return '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success" style="direction: ltr">
                            <input type="checkbox" onchange="changeUserStatus('.$query->id.',`provider`)" '.($query->banned == 1 ? 'checked' : '') .' class="custom-control-input" id="customSwitch2'.$query->id.'">
                            <label class="custom-control-label" id="status_label2'.$query->id.'" for="customSwitch2'.$query->id.'"></label>
                        </div>';
            })
            ->addColumn('control','admin.partial.Control')
            ->rawColumns(['banned','control']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClientDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Provider $model)
    {
        return $model->query()->where('banned',1)->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('closeddatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Blfrtip')
            ->parameters([
                'lengthMenu' => [
                    [10,25,50,100],[10,25,50,100]
                ],
                'buttons' => [
                    ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                    ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                    ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                ],
                "language" =>  datatableTrans(),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('')->orderable(false),
            Column::make('name')->title(__('name')),
            Column::make('email')->title(__('email')),
            Column::make('phone')->title(__('phone')),
            Column::make('banned')->title(__('ban_unban')),
            Column::make('control')->title(__('control'))->orderable(false)->searchable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
