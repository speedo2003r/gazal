<?php

namespace App\DataTables;

use App\Entities\Branch;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class BranchDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('appear', function ($model) {
                if ($model->appear) {
                    return '<button class="btn btn-success branch-appear" data-id="'.$model->id.'">'.__('Yes').'</button>';
                }
                return '<button class="btn btn-danger branch-appear" data-id="'.$model->id.'">'.__('No').'</button>';
            })
            ->addColumn('status', '{{ $current_status }}')
            ->addColumn('reviews', function ($model) {
                return $model->reviews()->get()->map(function ($item) {
                   return ['id' => $item->id, 'rate' => $item->rate, 'user_name' => $item->modelable->name ?? ''];
                });
            })
            ->addColumn('actions', 'provider.branch.buttons.actions')
            ->rawColumns(['actions', 'appear']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Entities\Branch $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Branch $model)
    {
        $status = $this->request->status;
        if (!auth('provider')->user()->isProvider()) {
            return $model->newQuery()->with(['branchDates', 'reviews'])
                         ->whereIn('id', auth('provider')->user()->employee_branches()->pluck('branches.id'))
                         ->when(!is_null($status), function ($q) use ($status) {
                             $q->works($status);

                         })
                         ->latest();
        }
        return $model->newQuery()->with(['branchDates', 'reviews'])
                                 ->where('provider_id', auth('provider')->id())
                                 ->when(!is_null($status), function ($q) use ($status) {
                                    $q->works($status);

                                 })
                                 ->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('branchdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->parameters([
                        'lengthMenu' => [
                            [10,25,50,100],[10,25,50,100]
                        ],
                        'buttons' => [
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                            ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                            ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                        ],
                        "language" =>  datatableTrans(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            [
                'name' => 'id',
                'data' => 'id',
                'title' => '#',
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => false,
                'width'          => '10px',
                'aaSorting'      => 'none'
            ],

            [
                'name'=>'title',
                'data'=>'title.ar',
                'title'=> awtTrans('admin.branchName'),
            ],

            [
                'name'=>'address',
                'data'=>'address',
                'title'=> awtTrans('Address'),
            ],

            [
                'name'=>'appear',
                'data'=>'appear',
                'title'=>__('Visible'),
            ],

            [
                'name'=>'status',
                'data'=>'status',
                'title'=>__('Status'),
            ],

            [
                'name' => 'created_at',
                'data' => 'created_at',
                'title' => __('Created At'),
                'exportable' => false,
                'printable'  => false,
                'searchable' => false,
                'orderable'  => false,
            ],
            [
                'name' => 'actions',
                'data' => 'actions',
                'title' => __('Actions'),
                'exportable' => false,
                'printable'  => false,
                'searchable' => false,
                'orderable'  => false,
            ],
        ];

    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Branch_' . date('YmdHis');
    }
}
