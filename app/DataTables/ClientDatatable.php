<?php

namespace App\DataTables;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ClientDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('status',function ($query){
                return '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success" style="direction: ltr">
                            <input type="checkbox" onchange="changeUserStatus('.$query->id.',`client`)" '.($query->banned == 0 ? 'checked' : '') .' class="custom-control-input" id="customSwitch'.$query->id.'">
                            <label class="custom-control-label" id="status_label'.$query->id.'" for="customSwitch'.$query->id.'"></label>
                        </div>';
            })
            ->addColumn('orders',function ($query){
                return '<a href="'.route('admin.orders.client',$query['id']).'" class="btn btn-outline-success">('.$query->ordersAsUser()->count().')'.__('orders').'</a>';
            })
            ->addColumn('addresses',function ($query){
                return '<a href="'.route('admin.clients.addresses',$query['id']).'" class="btn btn-outline-success">'.__('addresses').'</a>';
            })
            ->addColumn('url',function ($query){
                return 'admin.clients.delete';
            })
            ->addColumn('viewRoute',function ($query){
                return 'admin.clients.show';
            })
            ->addColumn('target',function ($query){
                return 'editModel';
            })
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('control','admin.partial.ControlWithOutDel')
            ->rawColumns(['addresses','orders','status','control']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClientDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->query()->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        if(auth()->user()['role_id'] == 1){
            return $this->builder()
                ->setTableId('clientdatatable-table')
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->dom('<"top"<"actions">Blfrtip<"clear">>rt<"bottom"iflp<"clear">>')
                ->parameters([
                    'lengthMenu' => [
                        [10,25,50,100],[10,25,50,100]
                    ],
                    'buttons' => [
                        ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                        ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                        ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                    ],
                    "language" =>  datatableTrans(),
                ]);
        }else{
            return $this->builder()
                ->setTableId('clientdatatable-table')
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->parameters([
                    'lengthMenu' => [
                        [10,25,50,100],[10,25,50,100]
                    ],
                    "language" =>  datatableTrans(),
                ]);
        }
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title(__('id'))->orderable(false),
            Column::make('name')->title(__('name')),
            Column::make('email')->title(__('email')),
            Column::make('phone')->title(__('phone')),
            Column::make('v_code')->title(__('OTP')),
            Column::make('orders')->title(__('orders'))->searchable(false),
            Column::make('addresses')->title(__('addresses'))->searchable(false),
            Column::make('control')->title(__('control'))->orderable(false)->searchable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
