<?php

namespace App\DataTables;

use App\Entities\Banner;
use App\Entities\Hire;
use App\Entities\Slider;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class HireDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('id',function ($query){
                return '<label class="custom-control material-checkbox" style="margin: auto">
                            <input type="checkbox" class="material-control-input checkSingle" id="'.$query->id.'">
                            <span class="material-control-indicator"></span>
                        </label>';
            })
            ->addColumn('url',function ($query){
                return 'admin.delegates.hire.destroy';
            })
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('edit',function ($query){
                return 'admin.delegates.hire.edit';
            })
            ->addColumn('show',function ($query){
                return 'admin.delegates.hire.show';
            })
            ->editColumn('created_at',function ($query){
                return date('Y-m-d H:i a',strtotime($query['created_at']));
            })
            ->addColumn('control','admin.partial.ControlInShowEditDel')
            ->rawColumns(['edit','control','id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\SliderDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Hire $model)
    {
        return $model->query()
            ->select('hire.id','hire.amount','hire.delegates_count','hire.from','hire.to','providers.store_name->ar as store_name','hire.created_at')
            ->leftJoin('providers','providers.id','=','hire.provider_id')
            ->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('hiretdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->parameters([
                        'lengthMenu' => [
                            [10,25,50,100],[10,25,50,100]
                        ],
                        'buttons' => [
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                            ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                            ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                        ],
                        "language" =>  datatableTrans(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('')->orderable(false),
            Column::make('store_name')->title(__('store_name')),
            Column::make('amount')->title(__('Price')),
            Column::make('delegates_count')->title(__('admin.NumberDelegatesRequired')),
            Column::make('created_at')->title(__('created_at')),
            Column::make('control')->title(__('control'))->orderable(false)->searchable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ContactUs_' . date('YmdHis');
    }
}
