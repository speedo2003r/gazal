<?php

namespace App\DataTables;

use App\Entities\Delegate;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class DelegateBlockedDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('editUrl',function ($query){
                return 'admin.delegates.edit';
            })
            ->addColumn('showUrl',function ($query){
                return 'admin.delegates.show';
            })
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('notes',function ($query){
                $notes = $query->blockUsers()->latest()->first() ? $query->blockUsers()->latest()->first()['notes'] : '';
                return $notes;
            })
            ->addColumn('rate',function ($query){
                return '<div class="Stars" style="--rating: '.($query->ratings->sum('rate') > 0 ? (round($query->ratings->sum('rate') / $query->ratings->count(),2)) : '0').';">';
            })
            ->addColumn('date',function ($query){
                $date = $query->blockUsers()->latest()->first() ? $query->blockUsers()->latest()->first()['created_at'] : '';
                return $date;
            })
            ->addColumn('control','admin.partial.Control')
            ->rawColumns(['rate','notes','accept','images','control']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClientDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Delegate $model)
    {
        return $model->query()->where('banned',1)->with('ratings')->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('delegatedatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('<"top"<"actions">Blfrtip<"clear">>rt<"bottom"iflp<"clear">>')
                    ->parameters([
                        'lengthMenu' => [
                            [10,25,50,100],[10,25,50,100]
                        ],
                        'buttons' => [
                            ['extend' => 'excel','className' => 'btn btn-success' , 'text' => __('excel_file')],
                            ['extend' => 'print','className' => 'btn btn-inverse' , 'text' => __('print')],
                            ['extend' => 'copy','className' => 'btn btn-success' , 'text' => __('copy')],
                        ],
                        "language" =>  datatableTrans(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('')->orderable(false),
            Column::make('name')->title(__('name')),
            Column::make('phone')->title(__('phone')),
            Column::make('date')->title(__('suspensionDate')),
            Column::make('notes')->title(__('ReasonSuspension')),
            Column::make('rate')->title(__('rating')),
            Column::make('control')->title(__('control'))->orderable(false)->searchable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Client_' . date('YmdHis');
    }
}
