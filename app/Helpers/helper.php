<?php

use App\Models\User;
use App\Models\Permission;
use Illuminate\Support\Facades\Route;

function provider_dashboard_path()
{
    return url('/provider_assets');
}

function LocaleLang($ar, $en)
{
    return app()->getLocale() == 'ar' ? $ar : $en;
}

function RedirectPage($type, $recourse)
{
    if ($type == 'added') {
        session()->flash('success', 'تم الاضافه');
        return redirect()->route('adminFront.' . $recourse . '.index');
    } elseif ($type == 'updated') {
        session()->flash('success', 'تم التعديل');
        return redirect()->route('adminFront.' . $recourse . '.index');
    } elseif ($type == 'deleted') {
        session()->flash('success', 'تم الحذف');
        return redirect()->route('adminFront.' . $recourse . '.index');
    } elseif ($type == 'refused') {
        session()->flash('success', trans('site.refused_successfully'));
        return redirect()->route('adminFront.' . $recourse . '.index');
    } elseif ($type == 'blocked') {
        session()->flash('success', trans('site.blocked_successfully'));
        return redirect()->route('adminFront.' . $recourse . '.index');
    } elseif ($type == 'unblocked') {
        session()->flash('success', trans('site.unblocked_successfully'));
        return redirect()->route('adminFront.' . $recourse . '.index');
    } elseif ($type == 'activated') {
        session()->flash('success', trans('site.activated_successfully'));
        return redirect()->route('adminFront.' . $recourse . '.index');
    } else {
        return redirect()->back();
    }
}

function dashboard_path()
{
    return url('/front/dashboard_assets');
}


function landPage_path()
{
    return url('/front/landPage_assets');
}

function client_path()
{
    if(env('ADD_PUBLIC')){
        return url('/public/front/client_assets');
    }else{
        return url('/front/client_assets');
    }
}


function pages_path()
{
    return url('/front/pages_assets');
}


function Home()
{
    $colors = [
        '#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#7FB3D5', '#e67e22', '#229954', '#f39c12', '#F6CD61',
        '#FE8A71', '#199EC7', '#C39BD3', '#5b239c', '#73603e'
    ];
    $home   = [
        [
            'name'  => 'المشرفين',
            'count' => User::where('role_id', '!=', 0)->count(),
            'icon'  => '<i class="fa fa-users"></i>',
            'color' => $colors[array_rand($colors)]
        ]
    ];

    return $blocks[] = $home;
}

function getCityBylatlng($lat = '', $long = '', $lang = 'ar')
{
    $google_key = settings('map_key');
    $geocode = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=false&key=$google_key&language=$lang";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $geocode);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $output = json_decode($response);
    $dataarray = get_object_vars($output);
    $short_name = '';
    if ($dataarray['status'] != 'ZERO_RESULTS' && $dataarray['status'] != 'INVALID_REQUEST') {
        if (isset($dataarray['results'][0]->address_components)) {

            if (isset($dataarray['results'][0]->address_components[1])) {
                if ($dataarray['results'][0]->address_components[1]->types[0] == 'locality' || $dataarray['results'][0]->address_components[1]->types[0] == 'administrative_area_level_1') {
                    return $dataarray['results'][0]->address_components[1]->short_name;
                }
            }

            if (isset($dataarray['results'][0]->address_components[2])) {
                if ($dataarray['results'][0]->address_components[2]->types[0] == 'locality' || $dataarray['results'][0]->address_components[2]->types[0] == 'administrative_area_level_1') {
                    return $dataarray['results'][0]->address_components[2]->short_name;
                }
            }
            if (isset($dataarray['results'][0]->address_components[3])) {
                if ($dataarray['results'][0]->address_components[3]->types[0] == 'locality' || $dataarray['results'][0]->address_components[3]->types[0] == 'administrative_area_level_1') {
                    return $dataarray['results'][0]->address_components[3]->short_name;
                }
            }
            if (isset($dataarray['results'][0]->address_components[4])) {
                if ($dataarray['results'][0]->address_components[4]->types[0] == 'locality' || $dataarray['results'][0]->address_components[4]->types[0] == 'administrative_area_level_1') {
                    return $dataarray['results'][0]->address_components[4]->short_name;
                }
            }
            if (isset($dataarray['results'][0]->address_components[5])) {
                if ($dataarray['results'][0]->address_components[5]->types[0] == 'locality' || $dataarray['results'][0]->address_components[5]->types[0] == 'administrative_area_level_1') {
                    return $dataarray['results'][0]->address_components[5]->short_name;
                }
            }
        } else {
            $short_name = '';
        }
    } else {
        $short_name = '';
    }
    return $short_name;
}
function distance($lat1, $lng1, $lat2, $lng2, $unit = 'K')
{
    if (($lat1 == $lat2) && ($lng1 == $lng2)) {
        return 0;
    } else {
        $theta = $lng1 - $lng2;
        $dist  = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist  = acos($dist);
        $dist  = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit  = strtoupper($unit);

        if ($unit == "K") {
            return (string) round($miles * 1.609344, 2);
        } else if ($unit == "N") {
            return (string) round($miles * 0.8684, 2);
        } else if ($unit == "M") {
            return (string) round($miles * 1609.344, 2);
        } else {
            return (string) round($miles, 2);
        }
    }
}
function array_equal($a, $b)
{
    return (is_array($a)
        && is_array($b)
        && count($a) == count($b)
        && array_diff($a, $b) === array_diff($b, $a));
}
function has_dupes($array)
{
    $dupe_array = array();
    foreach ($array as $val) {
        if (++$dupe_array[$val] > 1) {
            return true;
        }
    }
    return false;
}
function checkEmail($email)
{
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true;
    } else {
        return false;
    }
}
if (!function_exists('load_dep')) {
    function load_dep($select = null, $dep_hide = null)
    {
        $departments = \App\Models\Category::select('categories.title_ar as text', 'categories.id as id', 'categories.parent_id as parent', 'categories.parent_id as category_id')
            ->orderBy('categories.parent_id', 'asc')
            ->get(['text', 'parent', 'id', 'category_id']);
        $dep_arr = [];
        foreach ($departments as $key => $department) {
            $list_arr = [];
            $list_arr['icon'] = '';
            $list_arr['li_attr'] = '';
            $list_arr['a_attr'] = '';
            $list_arr['children'] = [];
            if ($select !== null and $select == $department->id) {
                $list_arr['state'] = [
                    'opened' => true,
                    'selected' => true,
                    'disabled' => false
                ];
            }
            if ($dep_hide !== null and $dep_hide == $department->id) {
                $list_arr['state'] = [
                    'opened' => false,
                    'selected' => false,
                    'disabled' => true
                ];
            }
            $code = $department->id;
            $list_arr['id'] = $department->id;
            $list_arr['parent'] = $department->parent != null ? $department->parent : '#';
            $list_arr['text'] = $department->text . ($department->parent_id != null ? ' (' . $department->category['title_ar'] . ') ' : '');
            array_push($dep_arr, $list_arr);
        }
        return json_encode($dep_arr, JSON_UNESCAPED_UNICODE);
    }
}
function datatableTrans()
{
    $langJson = [
        "sEmptyTable" => awtTrans("ليست هناك بيانات متاحة في الجدول"),
        "sLoadingRecords" => awtTrans("جارٍ التحميل..."),
        "sProcessing" => awtTrans("جارٍ التحميل..."),
        "sLengthMenu" => "".awtTrans('أظهر')." _MENU_".awtTrans('مدخلات')."",
        "sZeroRecords" => awtTrans("لم يعثر على أية سجلات"),
        "sInfo" => "".awtTrans('إظهار')." _START_ ".awtTrans('إلى')." _END_ ".awtTrans('من أصل')." _TOTAL_ ".awtTrans('مدخل')."",
        "sInfoEmpty" => awtTrans("يعرض 0 إلى 0 من أصل 0 سجل"),
        "sInfoFiltered" => "(".awtTrans('منتقاة من مجموع')." _MAX_ ".awtTrans('مُدخل').")",
        "sInfoPostFix" => "",
        "sSearch" => awtTrans("ابحث"),
        "sUrl" => "",
        "oPaginate" => [
            "sFirst" => awtTrans("الأول"),
            "sPrevious" => awtTrans("السابق"),
            "sNext" => awtTrans("التالي"),
            "sLast" => awtTrans("الأخير")
        ],
        "oAria" => [
            "sSortAscending" => awtTrans(" تفعيل لترتيب العمود تصاعدياً"),
            "sSortDescending" => awtTrans(": تفعيل لترتيب العمود تنازلياً")
        ]
    ];
    return $langJson;
}
#convert arabic number to english format - user model
if (!function_exists('convert_to_english')) {
    function convert_to_english($string)
    {
        $newNumbers = range(0, 9);
        $arabic     = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        $string     = str_replace($arabic, $newNumbers, $string);
        return $string;
    }
}

if (!function_exists('generateCode')) {
    function generateCode()
    {
        return '123456';
        $token = rand(111111, 999999);
        return $token;
    }
}
#get value from settings DB
function settings($key)
{
    $setting = \App\Entities\Setting::firstOrCreate(['key' => $key]);
    return $setting['value'];
}
#get value from socials DB
function socials($key)
{
    $setting = \App\Entities\Social::firstOrCreate(['key' => $key]);
    return $setting['value'];
}
function updateRole($id)
{
    //get all routes
    $routes      = Route::getRoutes();
    $permissions = Permission::where('role_id', $id)->pluck('permission')->toArray();

    $m = null;
    foreach ($routes as $value) {
        if ($value->getName() !== null) {

            //display main routes
            if (
                isset($value->getAction()['type']) && $value->getAction()['type'] == 'parent' &&
                isset($value->getAction()['icon']) && $value->getAction()['icon'] != null
            ) {

                echo '<div class = "col-xs-3">';
                echo '<div class = "per-box">';


                // main route
                echo ' <label>';
                echo '<input type = "checkbox" name = "permissions[]"';

                if (in_array($value->getName(), $permissions))
                    echo ' checked';

                echo '  value="' . $value->getName()
                    . '">';
                echo ' <span class = "checkmark"></span>';
                echo '<span class = "name">' . $value->getAction()["title"] . '</span>';
                echo '</label>';
                //sub routes
                if (isset($value->getAction()["child"])) {

                    $childs = $value->getAction()["child"];
                    $r2     = Route::getRoutes();

                    foreach ($r2 as $r) {
                        if ($r->getName() !== null && in_array($r->getName(), $childs)) {

                            echo ' <label>';
                            echo '<input type = "checkbox" name = "permissions[]"';

                            if (in_array($r->getName(), $permissions))
                                echo ' checked ';

                            echo ' value="' . $r->getName() . '">';
                            echo ' <span class = "checkmark"></span>';
                            echo '<span class = "name">' . $r->getAction()["title"] . '</span>';
                            echo '</label>';
                        }
                    }
                }
                echo ' </div>';
                echo '</div>';
            }
        }
    }
}

function Translate($text, $lang)
{
    $api  = 'trnsl.1.1.20201101T180227Z.875886c0b7db970c.7c2bd36a60d8c03bbaa408f56c5a058f73059da2';
    $url  = file_get_contents('https://translate.yandex.net/api/v1.5/tr.json/translate?key=' . $api
        . '&lang=ar' . '-' . $lang . '&text=' . urlencode($text));
    $json = json_decode($url);
    return $json->text[0];
}

function lang()
{
    return App()->getLocale();
}

if (!function_exists('dashboard_url')) {
    function dashboard_url($url)
    {
        if (env('APP_PUBLIC'))
            return url('/public/' . $url);
        return url('/' . $url);
    }
}

function arabicDate($time)
{
    if(app()->getLocale() == 'ar'){
        $en_data = [
            'January', 'Jan', 'Feburary', 'Feb', 'March', 'Mar',
            'April', 'Apr', 'May', 'June', 'Jun',
            'July', 'Jul', 'August', 'Aug', 'September', 'Sep',
            'October', 'Oct', 'November', 'Nov', 'December', 'Dec',
            'Satureday', 'Sat', 'Sunday', 'Sun', 'Monday', 'Mon',
            'Tuesday', 'Tue', 'Wednesday', 'Wed', 'Thursday', 'Thu', 'Friday', 'Fri',
            'AM', 'am', 'PM', 'pm'
        ];

        $ar_data = [
            'يناير', 'يناير', 'فبراير', 'فبراير', 'مارس', 'مارس',
            'أبريل', 'أبريل', 'مايو', 'مايو', 'يونيو', 'يونيو',
            'يوليو', 'يوليو', 'أغسطس', 'أغسطس', 'سبتمبر', 'سبتمبر',
            'أكتوبر', 'أكتوبر', 'نوفمبر', 'نوفمبر', 'ديسمبر', 'ديسمبر',
            'السبت', 'السبت', 'الأحد', 'الأحد', 'الإثنين', 'الإثنين',
            'الثلاثاء', 'الثلاثاء', 'الأربعاء', 'الأربعاء', 'الخميس', 'الخميس', 'الجمعة', 'الجمعة',
            'صباحاً', 'صباحاً', 'مساءً', 'مساءً'
        ];

        return str_replace($en_data, $ar_data, $time);
    }else{
        return $time;
    }
}

function getDrivingDistance($lat1='', $long1='',$lat2='', $long2='' ,$lang ='ar'){
    $google_key = settings('map_key');
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=".$lang."&key=".$google_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $result = curl_exec($ch);
    curl_close($ch);
    $response = json_decode($result, true);
    $time_text = '';
    if($response['rows']){
        if($response['rows'][0]['elements'][0]['status'] == 'ZERO_RESULTS' || ($response['rows'][0]['elements'][0]['status'] == 'NOT_FOUND') ){
            $distance = distance($lat1, $long1, $lat2, $long2);
            $time     = ceil($distance * 2).' mins' ;
            $time_text = $time;
            $distance = $distance * 1000; // in meter
        }else{
            $distance = $response['rows'][0]['elements'][0]['distance']['value'];  // in Meter
            $time_text     = $response['rows'][0]['elements'][0]['duration']['text'];  //in seconds
            $time          = intval(intval($response['rows'][0]['elements'][0]['duration']['value']) / 60) ;  //in seconds
            $time          = ($time <= 0)? 1 : $time;
        }
    }else{
        $distance = distance($lat1, $long1, $lat2, $long2);
        $time     = ceil($distance * 2) ;
        $time_text = $time;
        $distance = $distance * 1000;  // in Meter
    }
    //in text format
    // $distance = $response['rows'][0]['elements'][0]['distance']['text'];
    // $time     = $response['rows'][0]['elements'][0]['duration']['text'];
    $in_kms = ($distance / 1000); //in kms
    $in_kms = round($in_kms, 2);

    return ['distance' => $in_kms , 'time' => $time , 'time_text'=>$time_text];
}

function GetPathAndDirections($lat1='', $long1='',$lat2='', $long2='' ,$path='',$lang ='ar'){
    // $path = '31.0345612,31.3489804|31.0328805,31.36542648';
    //https://maps.googleapis.com/maps/api/directions/json?origin=31.0345612,31.3489804&destination=31.0034004,31.3730575&waypoints=31.0328805,31.36542648&mode=driving&language=ar&key=AIzaSyDYjCVA8YFhqN2pGiW4I8BCwhlxThs1Lc0
    $google_key = settings('map_key');
    $url = "https://maps.googleapis.com/maps/api/directions/json?origin=".$lat1.",".$long1."&destination=".$lat2.",".$long2."&waypoints=".$path."&mode=driving&language=".$lang."&key=".$google_key;
    $ch  = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $result = curl_exec($ch);
    curl_close($ch);
    $response = json_decode($result, true);
    // routes->0->legs->
    //                [0,1,..]->
    //                          distance&duration&end_location->lat&lng
    $distance = 0;
    $time     = 0;
    if($response['routes']){
        foreach($response['routes'][0]['legs'] as $road){
            $distance += $road['distance']['value'];
            // $time     += $road['duration']['value'];
        }
    }else{
        $distance = distance($lat1, $long1, $lat2, $long2);
        $distance = $distance * 1000;  // in Meter
        // $time     = intval($distance * 1.2).' '.trans('order.minute') ;
    }
    $in_kms = ($distance / 1000); //in kms
    $in_kms = round($in_kms, 2);

    return $in_kms;
}

if(!function_exists('languages')){
    function languages(){
        return ['ar','en'];
    }
}

if(!function_exists('getBrowserUuid')){
    function getBrowserUuid(){
        # get uuid saved to browser in local storage
        return session()->get('uuid');
    }
}

if(!function_exists('activeTab')){
    function activeTab($pattern){
        if (request()->is($pattern)) {
            return 'subdrop';
        }
        return '';
    }
}

if(!function_exists('dayIndex')){
    function dayIndex($index){
        return [
            "Saturday" => 1,
            "Sunday" => 2,
            "Monday" => 3,
            "Tuesday" => 4,
            "Wednesday" => 5,
            "Thursday" => 6,
            "Friday" => 7,
        ][$index];
    }
}


if(!function_exists('currentUser')){
    function currentUser(){
        if (auth('provider')->user()->isProvider()) {
            return auth('provider')->user();
        }
        return auth('provider')->user()->provider;
    }
}
