<?php


use App\Models\Permission;
use Illuminate\Support\Facades\Route;


// display routes
function sidebar()
{
    $routes         = Route::getRoutes();
    $routes_data    = [];
    $my_routes      = Permission::where('role_id', auth('admin')->user()->role_id)->pluck('permission')->toArray();
    foreach ($routes as $route) {
        if ($route->getName())
            $routes_data['"'.$route->getName().'"'] = [
                'title'     => isset($route->getAction()['title']) ? $route->getAction()['title'] : null,
                'subTitle'  => isset($route->getAction()['subTitle']) ? $route->getAction()['subTitle'] : null,
                'icon'      => isset($route->getAction()['icon']) ? $route->getAction()['icon'] : null,
                'subIcon'   => isset($route->getAction()['subIcon']) ? $route->getAction()['subIcon'] : null,
                'name'      => $route->getName() ?? null,
            ];
    }


    foreach ($routes as $value) {
        if ($value->getName() !== null) {

            //display only parent routes
            if (isset($value->getAction()['title']) && isset($value->getAction()['icon']) && isset($value->getAction()['type']) && $value->getAction()['type'] == 'parent') {


                //display route with sub directory
                if (isset($value->getAction()['sub_route']) && $value->getAction()['sub_route'] == true && isset($value->getAction()['child']) && count($value->getAction()['child'])) {

                    // check user auth to access this route
                    if (in_array($value->getName(), $my_routes)) {


                        //check if this is the current opened
                        $active     = '';
                        $opend      = '';
                        $style  = 'display:none';
                        $child_name = substr(Route::currentRouteName(), 6);
                        if(in_array($child_name, $value->getAction()['child'])){
                            $active = 'active';
                            $opend  = 'open';
                            $style  = 'display:block';
                        }

                        echo '<li class="nav-item ' . $opend . '" >
                                <a href="#">' . $value->getAction()['icon'] . '<span> ' . $value->getAction()['title'] . '</span><span class="menu-arrow"></span></a>
                                <ul class="menu-content" style="'.$style.'">';


                        // display child sub directories
                        foreach ($value->getAction()['child'] as $child){
                            $active = ('admin.'.$child) == Route::currentRouteName() ? 'active' : '';

                            if (isset($routes_data['"admin.' . $child . '"']) && $routes_data['"admin.' . $child . '"']['title'] && $routes_data['"admin.' . $child . '"']['icon'])
                                echo '<li class="' .$active.'"><a href="' . route('admin.'.$child) . '">'. $routes_data['"admin.' . $child . '"']['icon'].' <span class="menu-item" data-i18n="List">'. awtTrans($routes_data['"admin.' . $child . '"']['title']) . '</span> </a></li>';

                        }

                        echo '</ul></li>';
                    }
                } else {

                    if (in_array($value->getName(), $my_routes)) {
                        $active = $value->getName() == Route::currentRouteName() ? 'active' : '';

                        echo '<li class="nav-item '.$active.'"><a href="' . route($value->getName()) . '">' . $value->getAction()['icon'] . ' <span class="menu-title" data-i18n="Dashboard"> ' . awtTrans($value->getAction()['title']) . ' </span><span class="badge badge badge-warning badge-pill float-right mr-2"></span> </a></li>';
                    }
                }
            }
        }
    }
}

function addRole()
{

    $routes = Route::getRoutes();
    $routes_data = [];
    $id = 0;
    foreach ($routes as $route)
        if ($route->getName())
            $routes_data['"' . $route->getName() . '"'] = ['title' => isset($route->getAction()['title']) ? $route->getAction()['title'] : null];

    foreach ($routes as $value) {

        if (isset($value->getAction()['title']) && isset($value->getAction()['type']) && $value->getAction()['type'] == 'parent') {


            $parent_class = 'gtx_' . $id++;
            echo '
             <div class="col col-md-4 col-sm-6 md-3 roll-checkk">
                <div class="card">
                    <div class="card-body">
                        <div class="brands">
                            <a class="filter-title mb-0 select-all-permissions">' . awtTrans($value->getAction()["title"]) . '</a>
                            <div class="brand-list" id="brands">
                                <ul class="list-unstyled">
                                    <li class="d-flex justify-content-between align-items-center py-25">
                                        <span style="border-bottom: 1px solid #ddd;padding-bottom: 10px;" class="vs-checkbox-con vs-checkbox-primary">
                                            <input class="checkbox-input check-all" name="permissions[]" type="checkbox">
                                            <span class="vs-checkbox">
                                                <span class="vs-checkbox--check">
                                                    <i class="vs-icon feather icon-check"></i>
                                                </span>
                                            </span>
                                            <span class="">تحديد الكل</span>
                                        </span>
                                    </li>';


            if (isset($value->getAction()['child']) && count($value->getAction()['child'])) {


                foreach ($value->getAction()['child'] as $key => $child) {


                    echo '

                            <li class="d-flex justify-content-between align-items-center py-25">
                            <span class="vs-checkbox-con vs-checkbox-primary">
                                <input name="permissions[]" value="admin.' .$child.'" class="checkbox-input checkk" type="checkbox">
                                <span class="vs-checkbox">
                                    <span class="vs-checkbox--check">
                                        <i class="vs-icon feather icon-check"></i>
                                    </span>
                                </span>
                                <span class=""> ' . awtTrans($routes_data['"admin.' . $child . '"']['title']) . '</span>
                            </span>
                            </li>';
                }
            }

            echo '</ul></div></div></div></div></div>';
        }
    }
}

function editRole($id)
{

    $routes         = Route::getRoutes();
    $routes_data    = [];
    $my_routes      = Permission::where('role_id', $id)->pluck('permission')->toArray();
    $id = 0;
    foreach ($routes as $route)
        if ($route->getName())
            $routes_data['"' . $route->getName() . '"'] = ['title' => isset($route->getAction()['title']) ? awtTrans($route->getAction()['title']) : null];

    foreach ($routes as $value) {

        if (isset($value->getAction()['title']) && isset($value->getAction()['type']) && $value->getAction()['type'] == 'parent') {

            $select = in_array($value->getName(), $my_routes)  ? 'checked' : '';
            $parent_class = 'gtx_' . $id++;
            echo '
            <div class="col col-md-4 col-sm-6 md-3 roll-checkk">
                <div class="card">
                    <div class="card-body">
                        <div class="brands">
                            <a class="filter-title mb-0 select-all-permissions">' . awtTrans($value->getAction()["title"]) . '</a>
                            <div class="brand-list" id="brands">
                                <ul class="list-unstyled">
                                    <li class="d-flex justify-content-between align-items-center py-25">
                                        <span style="border-bottom: 1px solid #ddd;padding-bottom: 10px;" class="vs-checkbox-con vs-checkbox-primary">
                                            <input class="checkbox-input check-all" name="permissions[]" value="' . $value->getName() . '" type="checkbox" ' . $select . '>
                                            <span class="vs-checkbox">
                                                <span class="vs-checkbox--check">
                                                    <i class="vs-icon feather icon-check"></i>
                                                </span>
                                            </span>
                                            <span class="">تحديد الكل</span>
                                        </span>
                                    </li>';



            if (isset($value->getAction()['child']) && count($value->getAction()['child'])) {


                foreach ($value->getAction()['child'] as $key => $child) {
                    $select = in_array('admin.' . $child, $my_routes)  ? 'checked' : '';
                    echo '
                            <li class="d-flex justify-content-between align-items-center py-25">
                            <span class="vs-checkbox-con vs-checkbox-primary">
                                <input name="permissions[]" value="admin.' .$child.'" class="checkbox-input checkk" type="checkbox"  ' . $select . '>
                                <span class="vs-checkbox">
                                    <span class="vs-checkbox--check">
                                        <i class="vs-icon feather icon-check"></i>
                                    </span>
                                </span>
                                <span class=""> ' . awtTrans($routes_data['"admin.' . $child . '"']['title']) . '</span>
                            </span>
                            </li>';
                }
            }

            echo '</ul></div></div></div></div></div>';
        }
    }
}
