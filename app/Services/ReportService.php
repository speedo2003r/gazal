<?php

namespace App\Services;

use App\Entities\Order;
use App\Repositories\Interfaces\IItem;
use App\Repositories\Interfaces\IOrder;
use Carbon\Carbon;

class ReportService
{
    protected $orderRepo, $productRepo;

    public function __construct(IOrder $orderRepo, IItem $item)
    {
        $this->orderRepo = $orderRepo;
        $this->productRepo = $item;
    }

    public function todayOrders($data = [])
    {
        return $this->orderRepo->with(['branch'])
                               ->whereDate('date', Carbon::today())
                               ->where('order_status', Order::STATUS_FINISH)
                               ->when(isset($data['provider_id']), function ($q) use ($data) {
                                   $q->whereProviderId($data['provider_id']);
                                })
                               ->latest();
    }

    public function allOrders($data = [])
    {
        return $this->orderRepo->with(['branch'])
                               ->whereDate('date', Carbon::today())
                               ->where('order_status', Order::STATUS_FINISH)
                               ->when(isset($data['provider_id']), function ($q) use ($data) {
                                   $q->whereProviderId($data['provider_id']);
                               })
                               ->when((isset($data['from'])&&isset($data['to'])), function ($q) use ($data) {
                                   $q->whereProviderId($data['provider_id']);
                               })
                               ->latest();
    }


}