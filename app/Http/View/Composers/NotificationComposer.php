<?php

namespace App\Http\View\Composers;

use App\Entities\ContactUs;
use Illuminate\View\View;

class NotificationComposer
{
    private $notifications = [];
    private $messages = [];

    public function __construct()
    {
        $contacts = ContactUs::where('seen', 0)->get();
        $notifications = auth('admin')->check() ? auth('admin')->user()->notifications : [];
        foreach ($contacts as $contact) {
            $this->messages[] = [
                'type' => 'contact',
                'data' => $contact,
            ];
        }


        $this->notifications = $notifications;
    }

    public function compose(View $view)
    {
        $view->with([
            'messages' => $this->messages,
            'notifications' => $this->notifications,
        ]);
    }
}
