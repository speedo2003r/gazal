<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PrayTimeController extends Controller
{

    use ResponseTrait;
    use NotifyTrait;
    use UploadTrait;

    public function __construct()
    {
       //
    }

    public function prayTime(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'lat' => 'required',
            'lng' => 'required',
        ]);
        if($validator->fails()){
            return $this->ApiResponse('fail',$validator->errors()->first());
        }

        //31.041455
        //31.4178592
        $lat = $request->lat;
        $lng = $request->lng;
        $praytimes = [];
        $msg = '';
        $url = "http://api.aladhan.com/v1/calendar?latitude=" . $lat . "&longitude=" . $lng . "&method=4&month=" . date("m") . "&year=" . date("Y");
        $jsonresult = @file_get_contents($url, true);
        if ($jsonresult === FALSE) {
            return '';
        } else {
            if ($results = json_decode($jsonresult)) {
                if (isset($results->data)) {
                    $currentday = intval(date('d')) - 1;
                    $praytimes['Fajr'] = $results->data[$currentday]->timings->Fajr;
                    $praytimes['Dhuhr'] = $results->data[$currentday]->timings->Dhuhr;
                    $praytimes['Asr'] = $results->data[$currentday]->timings->Asr;
                    $praytimes['Maghrib'] = $results->data[$currentday]->timings->Maghrib;
                    $praytimes['Isha'] = $results->data[$currentday]->timings->Isha;
                    foreach ($praytimes as $key => $value) {
                        $praytime = substr($value, 0, 5);
                        $to_time = strtotime(date("Y-m-d") . " " . $praytime);
                        $from_time = strtotime(date('Y-m-d H:i'));
                        $minutes = intval(round(($to_time - $from_time) / 60, 2));
                    }
                }
            }
        }
        return $this->successResponse($praytimes);
    }
    
}
