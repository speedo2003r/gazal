<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Http\Resources\Items\ItemSingleResource;
use App\Repositories\Interfaces\IItem;
use App\Repositories\ItemRepository;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller {
  use ResponseTrait;
  public $itemRepo;
  public function __construct(IItem $item) {
    $this->itemRepo = $item;
  }
  public function SingleItem(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'item_id'   => ['required', 'exists:items,id'],
      'branch_id' => ['required', 'exists:branches,id'],
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $item = $this->itemRepo->find($request['item_id']);

    $this->itemRepo->update([
      'views' => $item['views'] + 1,
    ], $item['id']);

    $item['branch_id'] = $request['branch_id'];

    return $this->successResponse([
      'item' => new ItemSingleResource($item),
    ]);
  }
}
