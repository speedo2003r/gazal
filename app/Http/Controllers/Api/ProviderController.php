<?php

namespace App\Http\Controllers\Api;

use App\Entities\Item;
use App\Entities\Order;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Http\Resources\Branches\OrderBranchCollection;
use App\Http\Resources\Items\ItemsCollection;
use App\Http\Resources\Items\ItemsResource;
use App\Http\Resources\Users\ProviderResource;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\IGroup;
use App\Repositories\Interfaces\IOffer;
use App\Repositories\Interfaces\IProvider;
use App\Repositories\Interfaces\ISlider;
use App\Repositories\Interfaces\IUser;
use App\Repositories\Interfaces\IUserAddress;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/** import */

class ProviderController extends Controller {
  use NotifyTrait;
  use ResponseTrait;
  public $providerRepo,$userRepo, $addressRepo, $categoryRepo, $sliderRepo, $offerRepo, $groupRepo, $branch;
  public function __construct(
    IBranch $branch,
    IGroup $groupRepo,
    IOffer $offerRepo,
    ISlider $sliderRepo,
    ICategory $categoryRepo,
    IUser $user,
    IProvider $provider,
    IUserAddress $address
  ) {
    $this->branch       = $branch;
    $this->userRepo     = $user;
    $this->providerRepo     = $provider;
    $this->addressRepo  = $address;
    $this->categoryRepo = $categoryRepo;
    $this->sliderRepo   = $sliderRepo;
    $this->groupRepo    = $groupRepo;
    $this->offerRepo    = $offerRepo;
  }

  public function HomeProvider(LangRequest $request) {
    $user       = auth('branchApi')->user();
    $count      = 0;
    $totalSell  = 0;
    $countView  = DB::table('orders_count_view')->where('branch_id', $user['id'])->first();
    $perDayView = DB::table('orders_per_day_view')->where('branch_id', $user['id'])->first();
    if ($countView) {
      $count = $countView->count;
    }
    if ($perDayView) {
      $totalSell = $perDayView->total;
    }
    $orders = $user->orders()->where('live', 1)->latest()->paginate(10);
    return $this->successResponse([
      'count'     => $count,
      'totalSell' => $totalSell,
      'orders'    => OrderBranchCollection::make($orders),
      'appear'    => $user['appear'],
    ]);
  }
  public function HomeProviderSellCount(LangRequest $request) {
    $user       = auth('branchApi')->user();
    $count      = 0;
    $totalSell  = 0;
    $countView  = DB::table('orders_count_view')->where('branch_id', $user['id'])->first();
    $perDayView = DB::table('orders_per_day_view')->where('branch_id', $user['id'])->first();
    if ($countView) {
      $count = $countView->count;
    }
    if ($perDayView) {
      $totalSell = $perDayView->total;
    }
    return $this->successResponse([
      'count'     => $count,
      'totalSell' => round($totalSell, 0),
    ]);
  }
  public function providerNewOrders(LangRequest $request) {
    $user   = auth('branchApi')->user();
    $orders = $user->orders()->where('order_status', Order::STATUS_WAITING)->where('live', 1)->latest()->paginate(10);
    return $this->successResponse([
      'orders' => OrderBranchCollection::make($orders),
    ]);
  }
  public function HomeAcceptedProvider(LangRequest $request) {
    $user   = auth('branchApi')->user();
    $orders = $user->orders()->whereNotIn('orders.order_status',[Order::STATUS_REFUSED,Order::STATUS_CANCELED])->where('live', 1)->latest()->paginate(10);
    return $this->successResponse(OrderBranchCollection::make($orders));
  }
  public function appearSwitch(LangRequest $request) {
    $user         = auth('branchApi')->user();
    $user->appear = !$user->appear;
    $user->save();
    return $this->successResponse($user['appear']);
  }
  public function singleProvider(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'provider_id' => 'required|exists:users,id,deleted_at,NULL',
      'branch_id'   => 'required|exists:branches,id,deleted_at,NULL',
      'uuid'        => 'required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $user   = $this->providerRepo->find($request['provider_id']);
    $branch = $this->branch->find($request['branch_id']);
    if ($branch['provider_id'] != $user['id']) {
      $msg = 'هذا الفرع غير تابع لهذا المستخدم';
      return $this->ApiResponse('fail', $msg);
    }

    $user['branch_id'] = $branch['id'];
    $categories        = $user->categories;
      $items = Item::where('user_id', $request['provider_id'])->whereHas('branches', function ($branch) use ($request) {
          $branch->where('branches.id', $request['branch_id']);
      })->whereHas('types')->whereHas('selling_views')->get();
    if(count($items) > 0){
        $data[] = [
            'id'       => 0,
            'title'    => 'الأكثر مبيعا',
            'items' => ItemsResource::collection($items)
        ];
    }
    $data = [];
    foreach ($categories as $category) {
        $items = $user->items()->where('items.subcategory_id', $category['id'])->whereHas('branches', function ($branch) use ($request) {
            $branch->where('branches.id', $request['branch_id']);
        })->whereHas('types')->get();
        if(count($items) > 0){
            $data[] = [
                'id'       => $category['id'],
                'title'    => $category['title'],
                'items' => ItemsResource::collection($items)
            ];
        }
    }

    if (auth()->check()) {
      $order = auth()->user()->ordersAsUser()->where('orders.live', 0)->where('orders.branch_id', $branch['id'])->where('orders.user_id', $user['id'])->first();
    } else {
      $order = Order::where('orders.uuid', $request['uuid'])->where('orders.branch_id', $branch['id'])->where('orders.live', 0)->where('orders.user_id', null)->where('orders.user_id', $user['id'])->first();
    }

    $cart = [
      'cart_id' => $order ? (string) $order['id'] : '',
      'total'   => null != $order ? (string) $order->_price() . ' ' . trans('api.SAR') . ' ' : 0,
    ];


    return $this->successResponse([
      'provider'      => ProviderResource::make($user),
      'subCategories' => $data,
      'cart'          => $cart,
    ]);
  }

  public function selectCategoryProvider(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'provider_id' => 'required|exists:users,id',
      'uuid'        => 'required',
      'category_id' => 'required',
      'branch_id'   => 'required|exists:branches,id',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }
    $user   = $this->userRepo->find($request['provider_id']);
    $branch = $this->branch->find($request['branch_id']);
    if ($branch['provider_id'] != $user['id']) {
      $msg = 'هذا الفرع غير تابع لهذا المستخدم';
      return $this->ApiResponse('fail', $msg);
    }
    $categories = $user->categories;
    $data[]     = [
      'id'       => 0,
      'title'    => 'الأكثر مبيعا',
      'selected' => 0 == $request['category_id'] ? true : false,
    ];
    foreach ($categories as $category) {
      $data[] = [
        'id'       => $category['id'],
        'title'    => $category['title'],
        'selected' => $request['category_id'] == $category['id'] ? true : false,
      ];
    }
    if (auth()->check()) {
      $order = auth()->user()->ordersAsUser()->where('orders.live', 0)->where('orders.branch_id', $branch['id'])->where('orders.user_id', $user['id'])->first();
    } else {
      $order = Order::where('orders.uuid', $request['uuid'])->where('orders.live', 0)->where('orders.branch_id', $branch['id'])->where('orders.user_id', null)->where('orders.user_id', $user['id'])->first();
    }
    $cart = [
      'cart_id' => $order ? (string) $order['id'] : '',
      'total'   => null != $order ? (string) $order->_price() . ' ' . trans('api.SAR') . ' ' : 0,
    ];
    if (0 == $request['category_id']) {
      $items = App\Entities\Item::where('user_id', $request['provider_id'])->whereHas('branches', function ($branch) use ($request) {
        $branch->where('branches.id', $request['branch_id']);
      })->whereHas('types')->whereHas('selling_views')->paginate(10);
    } else {
      $items = $user->items()->whereHas('branches', function ($branch) use ($request) {
        $branch->where('branches.id', $request['branch_id']);
      })->whereHas('types')->paginate(10);
    }
    return $this->successResponse([
      'subCategories' => $data,
      'items'         => ItemsCollection::make($items),
    ]);
  }

  public function contact(Request $request) {
    $socials = App\Entities\Social::all();
    return $this->successResponse([
      'provider_phone' => settings('provider_phone'),
      'email'          => settings('email'),
      'socials'        => App\Http\Resources\Settings\SocialResource::collection($socials),
    ]);
  }
}
