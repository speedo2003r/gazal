<?php

namespace App\Http\Controllers\Api;

use App\Entities\Branch;
use App\Entities\Favourite;
use App\Entities\Group;
use App\Entities\Item;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Http\Resources\Items\FavoriteGroupResource;
use App\Http\Resources\Items\FavoriteItemResource;
use App\Http\Resources\Items\FavoriteProviderResource;
use App\Http\Resources\Items\FavoriteResource;
use App\Http\Resources\Items\GroupResource;
use App\Http\Resources\Items\ItemsResource;
use App\Models\User;
use App\Repositories\Interfaces\IFavourite;
use App\Traits\ResponseTrait;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/** import */

class FavoriteController extends Controller
{
    use ResponseTrait;
    protected $favorite;
    public function __construct(IFavourite $favorite)
    {
        $this->favorite = $favorite;
    }
    public function favorites(LangRequest $request){
        $validator = Validator::make($request->all(),[
            'type' => 'required|in:branch,item',
        ]);
        if($validator->fails()){
            return $this->ApiResponse('success',$validator->errors()->first());
        }

        $favorite_type = $request['type'] == 'item' ? Item::class : Branch::class;
        $items = $this->favorite->getFavorites($favorite_type);
        if($favorite_type == Item::class){
            return $this->successResponse(FavoriteItemResource::collection($items));
        }elseif($favorite_type == Branch::class){
            return $this->successResponse(FavoriteProviderResource::collection($items));
        }
    }

    public function toggleFavorites(LangRequest $request)
    {
        $validator = Validator::make($request->all(),[
            'group_id' => 'required',
            'type' => 'required|in:branch,item',
        ]);
        if($validator->fails()){
            return $this->ApiResponse('fail',$validator->errors()->first());
        }
        if($request['type'] == 'item'){
            $item = Item::find($request['group_id']);
            if(!$item){
                $msg = app()->getLocale() == 'ar' ? 'المنتج غير موجود' : 'item is not exist';
                return $this->ApiResponse('fail',$msg);
            }
        }else{
            $branch = Branch::find($request['group_id']);
            if(!$branch){
                $msg = app()->getLocale() == 'ar' ? 'الفرع غير موجود' : 'branch is not exist';
                return $this->ApiResponse('fail',$msg);
            }
        }
        $favorite_type = $request['type'] == 'item' ? Item::class : Branch::class;
        $item = $this->favorite->toggleFav($favorite_type,$request['group_id']);
        if($favorite_type == Item::class){
            return $this->successResponse($item ? FavoriteItemResource::make($item) : (object) []);
        }elseif($favorite_type == Branch::class){
            return $this->successResponse($item ? FavoriteProviderResource::make($item) : (object) []);
        }
    }

    public function deleteFavorites(LangRequest $request)
    {
        $validator = Validator::make($request->all(),[
            'favorite_id' => 'required|exists:favourites,id',
        ]);
        if($validator->fails()){
            return $this->ApiResponse('fail',$validator->errors()->first());
        }
        $this->favorite->delete($request['favorite_id']);
        return $this->successResponse();
    }
}
