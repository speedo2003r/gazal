<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Http\Resources\Delegates\DelegateReviewsCollection;
use App\Http\Resources\Rating\RatingResource;
use App\Repositories\Interfaces\IRatingList;
use App\Repositories\Interfaces\IReviewRate;
use App\Repositories\Interfaces\IUser;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

/** import */

class RatingController extends Controller {
  use NotifyTrait;
  use ResponseTrait;
  public $userRepo, $ratingList, $review;
  public function __construct(
    IUser $user,
    IRatingList $ratingList,
    IReviewRate $review
  ) {
    $this->userRepo   = $user;
    $this->ratingList = $ratingList;
    $this->review     = $review;
  }

  public function ratingList(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'type' => 'required|in:provider,delegate,client',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $ratingLists = $this->ratingList->where('type', $request['type'])->get();
    return $this->successResponse(RatingResource::collection($ratingLists));
  }
  public function postRateProvider(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'branch_id' => 'required|exists:branches,id,deleted_at,NULL',
      'order_id'  => 'required|exists:orders,id,deleted_at,NULL',
      'rates'     => 'required|array',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $user        = auth()->user();
    $ratingLists = $this->ratingList->where('type', 'provider')->get();
    foreach ($request['rates'] as $key => $rate) {
      $this->review->create([
        'model_id'       => $user['id'],
        'model_type'     => get_class($user),
        'order_id'       => $request['order_id'],
        'rating_list_id' => $ratingLists[$key]['id'],
        'rateable_type'  => App\Entities\Branch::class,
        'rateable_id'    => $request['branch_id'],
        'rate'           => $rate,
      ]);
    }
    return $this->successResponse();
  }
  public function postRateClient(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'user_id'  => 'required|exists:users,id,deleted_at,NULL',
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
      'rates'    => 'required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $user        = auth('delegateApi')->user();
    $ratingLists = $this->ratingList->where('type', 'client')->get();
    foreach ($request['rates'] as $key => $rate) {
      Log::debug('Showing request: ' . $rate);
      $this->review->create([
        'model_id'       => $user['id'],
        'model_type'     => get_class($user),
        'order_id'       => $request['order_id'],
        'rating_list_id' => $ratingLists[$key]['id'],
        'rateable_type'  => App\Models\User::class,
        'rateable_id'    => $request['user_id'],
        'rate'           => $rate,
      ]);
    }
    return $this->successResponse();
  }
  public function postRateDelegate(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'delegate_id' => 'required|exists:users,id,deleted_at,NULL',
      'order_id'    => 'required|exists:orders,id,deleted_at,NULL',
      'rates'       => 'required|array',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $user = auth()->user();
    foreach ($request['rates'] as $key => $rate) {
      $ratList = $this->ratingList->find($key);
      $this->review->create([
        'model_id'       => $user['id'],
        'model_type'     => get_class($user),
        'order_id'       => $request['order_id'],
        'rating_list_id' => $ratList['id'],
        'rateable_type'  => App\Entities\Delegate::class,
        'rateable_id'    => $request['delegate_id'],
        'rate'           => $rate,
      ]);
    }
    return $this->successResponse();
  }

  public function delegateClientsRate(LangRequest $request) {
    $delegate = auth()->user();
    $orders   = $delegate->orders()->whereHas('ratings', function ($query) use ($delegate) {
      $query->where('user_id', $delegate['id']);
    })->orderBy('orders.id', 'desc')->latest()->paginate(10);
    return $this->successResponse(DelegateReviewsCollection::make($orders));
  }
}
