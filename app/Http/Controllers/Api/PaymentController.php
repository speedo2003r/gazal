<?php

namespace App\Http\Controllers\Api;
use App\Entities\Delegate;
use App\Http\Controllers\Controller;
use App\Http\Resources\Credits\CreditResource;
use App\Models\User;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\IContactUs;
use App\Repositories\Interfaces\ICredit;
use App\Repositories\Interfaces\IItem;
use App\Repositories\Interfaces\IOrder;
use App\Repositories\Interfaces\IPage;
use App\Repositories\Interfaces\ISetting;
use App\Repositories\Interfaces\ISlider;
use App\Repositories\Interfaces\ISocial;
use App\Repositories\Interfaces\IUser;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PaymentController extends Controller {

  use ResponseTrait;
  public $pageRepo, $itemRepo, $credit, $orderRepo, $sliderRepo, $userRepo, $settingRepo, $socialRepo, $contactRepo, $categoryRepo;
  public function __construct(
    ICredit $credit,
    ISlider $slider,
    IItem $item,
    IOrder $order,
    IPage $page,
    IUser $user,
    ISetting $setting,
    ISocial $social,
    IContactUs $contact,
    ICategory $category
  ) {
    $this->credit       = $credit;
    $this->userRepo     = $user;
    $this->pageRepo     = $page;
    $this->itemRepo     = $item;
    $this->orderRepo    = $order;
    $this->sliderRepo   = $slider;
    $this->settingRepo  = $setting;
    $this->socialRepo   = $social;
    $this->contactRepo  = $contact;
    $this->categoryRepo = $category;
  }

  public function getCredits(Request $request) {
    $user = auth('api')->user();
    if (!$user) {
      return $this->ApiResponse('fail', 'user is undefined');
    }
    $credits = $user->credits;
    return $this->successResponse(CreditResource::collection($credits));
  }
  public function addCredit(Request $request) {
    $validator = Validator::make($request->all(), [
      'holder_name' => ['required'],
      'card_number' => ['required', 'digits:16', 'numeric', Rule::unique('credits', 'card_number')],
      //            'pass'   => ['required','digits:3','numeric'],
      'date'        => ['required'],
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $user = auth('api')->user();
    if (!$user) {
      return $this->ApiResponse('fail', 'user is undefined');
    }
    $expiry = explode("/", $request->input('date'));
    $year   = str_replace(' ', '', $expiry[1]);
    $month  = str_replace(' ', '', $expiry[0]);
    $dt     = \DateTime::createFromFormat('y', $year);
    $year   = $dt->format('Y');
    $date   = $year . '-' . $month . '-' . '01';
    $request->request->add(['model_id' => $user['id'], 'model_type' => User::class, 'expire_card' => $date]);
    $this->credit->create(array_filter($request->except('date')));
    return $this->successResponse();
  }

  public function deleteCredit(Request $request) {
    $validator = Validator::make($request->all(), [
      'credit_id' => ['required', Rule::exists('credits', 'id')],
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $user = auth()->user();
    if (!$user) {
      return $this->ApiResponse('fail', 'user is undefined');
    }
    $credit = $this->credit->findWhere(['id' => $request['credit_id'], 'model_id' => $user['id'], 'model_type' => User::class])->first();
    if (!$credit) {
      return $this->ApiResponse('fail', 'credit is undefined');
    }
    $credit->delete();
    return $this->ApiResponse('success', trans('api.delete'));
  }
  public function DelegateGetCredits(Request $request) {
    $user = auth('delegateApi')->user();
    if (!$user) {
      return $this->ApiResponse('fail', 'user is undefined');
    }
    $credits = $user->credits;
    return $this->successResponse(CreditResource::collection($credits));
  }
  public function DelegateAddCredit(Request $request) {
    $validator = Validator::make($request->all(), [
      'holder_name' => ['required'],
      'card_number' => ['required', 'digits:16', 'numeric', Rule::unique('credits', 'card_number')],
      //            'pass'   => ['required','digits:3','numeric'],
      'date'        => ['required'],
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $user = auth('delegateApi')->user();
    if (!$user) {
      return $this->ApiResponse('fail', 'user is undefined');
    }
    $expiry = explode("/", $request->input('date'));
    $year   = str_replace(' ', '', $expiry[1]);
    $month  = str_replace(' ', '', $expiry[0]);
    $dt     = \DateTime::createFromFormat('y', $year);
    $year   = $dt->format('Y');
    $date   = $year . '-' . $month . '-' . '01';
    $request->request->add(['model_id' => $user['id'], 'model_type' => Delegate::class, 'expire_card' => $date]);
    $this->credit->create(array_filter($request->except('date')));
    return $this->successResponse();
  }

  public function DelegateDeleteCredit(Request $request) {
    $validator = Validator::make($request->all(), [
      'credit_id' => ['required', Rule::exists('credits', 'id')],
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $user = auth('delegateApi')->user();
    if (!$user) {
      return $this->ApiResponse('fail', 'user is undefined');
    }
    $credit = $this->credit->findWhere(['id' => $request['credit_id'], 'model_id' => $user['id'], 'model_type' => Delegate::class])->first();
    if (!$credit) {
      return $this->ApiResponse('fail', 'credit is undefined');
    }
    $credit->delete();
    return $this->ApiResponse('success', trans('api.delete'));
  }
}
