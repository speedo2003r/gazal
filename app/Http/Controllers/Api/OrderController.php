<?php

namespace App\Http\Controllers\Api;

use App\Entities\Branch;
use App\Entities\Delegate;
use App\Entities\Order;
use App\Entities\UserAddress;
use App\Events\UpdateDriverLocation;
use App\Events\UpdateOrderEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Http\Resources\Branches\OrderBranchCollection;
use App\Http\Resources\Branches\OrderBranchRefuseCollection;
use App\Http\Resources\Branches\OrderBranchResource;
use App\Http\Resources\Branches\SingleOrderBranchResource;
use App\Http\Resources\Branches\SingleOrderRateBranchResource;
use App\Http\Resources\Orders\OrderCollection;
use App\Http\Resources\Orders\OrderDetailResource;
use App\Jobs\OrderToDelegates;
use App\Models\User;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\IFeature;
use App\Repositories\Interfaces\IGroup;
use App\Repositories\Interfaces\IItem;
use App\Repositories\Interfaces\IOrder;
use App\Repositories\Interfaces\IOrderProduct;
use App\Repositories\Interfaces\IUser;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OrderController extends Controller {
  use ResponseTrait;
  use NotifyTrait;
  use UploadTrait;
  public $itemRepo, $group, $featureRepo, $orderRepo, $userRepo, $categoryRepo, $orderProductRepo;
  public function __construct(
    IOrderProduct $orderProductRepo,
    IGroup $group,
    IFeature $feature,
    IItem $item,
    IOrder $order,
    IUser $user,
    ICategory $category
  ) {
    $this->userRepo         = $user;
    $this->featureRepo      = $feature;
    $this->group            = $group;
    $this->itemRepo         = $item;
    $this->orderProductRepo = $orderProductRepo;
    $this->orderRepo        = $order;
    $this->categoryRepo     = $category;
  }

  public function placeOrder(Request $request) {
    $validator = Validator::make($request->all(), [
      'order_id'   => ['required', Rule::exists('orders', 'id')],
      'pay_type'   => 'required',
      'address_id' => 'nullable|exists:user_addresses,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $user  = auth()->user();
    $order = $this->orderRepo->findWhere(['id' => $request['order_id'], 'user_id' => $user['id'], 'live' => 0])->first();

    if (!$order) {
      return $this->ApiResponse('fail', trans('api.emptyCart'));
    }

    if (!$request['address_id']) {
      $address = $order->address;
      if (!$address) {
        return $this->ApiResponse('fail', trans('api.needAddress'));
      }
    } else {
      $address = UserAddress::find($request['address_id']);
    }

    if (!$order['order_type']) {
      return $this->ApiResponse('fail', trans('api.needOrderType'));
    }

    $now  = Carbon::now();
    $date = 'postponed' != $order['order_type'] ? $now->format('Y-m-d') : $order['date'];
    $time = 'postponed' != $order['order_type'] ? $now->format('H:i') : $order['time'];

    $order->update([
      'pay_type'       => $request['pay_type'],
      'date'           => $date,
      'time'           => $time,
      'live'           => 1,
      'lat'            => $address['lat'],
      'lng'            => $address['lng'],
      'created_date'   => Carbon::now()->format('Y-m-d H:i'),
      'order_status'   => Order::STATUS_WAITING,
      'progress_start' => Carbon::now()->format('H:i:s'),
      'progress_end'   => Carbon::now()->addMinutes(7)->format('H:i:s'),
    ]);

    // TODO: update notification make msg by type trans
    // $this->send_notify($order['provider_id'], ' يوجد لديك طلب جديد رقم ' . $order['order_num'] . ' في انتظار موافقتك ', 'You have a new order, is waiting for your accept', $order['id'], 'new_order', $order['branch_id']);
    return $this->successResponse();
  }

  public function orderAccept(Request $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $order = $this->orderRepo->find($request['order_id']);
    $user  = auth('branchApi')->user();

    if ($order['branch_id'] != $user['id'] || $order['order_status'] != Order::STATUS_WAITING) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }

    $order->update([
      'order_status' => Order::STATUS_ACCEPTED,
      'accepted_date' => Carbon::now()->format('Y-m-d H:i:s'),
    ]);

    // emit to pusher
//    event(new UpdateOrderEvent($request['order_id']));
    // todo: update notification
    $this->send_notify($order->user,'تم الموافقه علي طلبك','your order has been accepted', ' تم الموافقه على الطلب رقم ' . $order['order_num'], 'Your order number '.$order['order_num'].' was accepted',  'branchApi');

    $job = new OrderToDelegates($order);
    $this->dispatch($job);


    return $this->ApiResponse('success', trans('api.accepted'));
  }

  public function orderPrepared(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $order = $this->orderRepo->find($request['order_id']);
    $user  = auth('branchApi')->user();

    if ($order['branch_id'] != $user['id'] || Order::STATUS_ACCEPTED != $order['order_status']) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }

    $order->update([
      'provider_status' => 'pending', // todo: remove this
      'order_status' => Order::STATUS_PREPARED,
    ]);

    // emit to pusher
    event(new UpdateOrderEvent($request['order_id']));

    //todo: remove this
    $list = [
      'accepted'  => 'طلب جديد',
      'pending'   => 'تم تحضير الطلب',
      'shipped'   => 'تسليم الطلب',
      'delivered' => 'تم التوصيل',
    ];

    return $this->successResponse([
      'order'            => OrderBranchResource::make($order),
      'orderStatus'      => $order->provider_status,
      'order_status'     => $order->order_status,
      'order_status_msg' => $order->order_status_msg,
      // 'allStatus'         => $list,
    ]);
  }

  public function orderProviderGiveToDelegate(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $order = $this->orderRepo->find($request['order_id']);
    $user  = auth('branchApi')->user();

    if ($order['branch_id'] != $user['id']
      || Order::STATUS_PREPARED != $order['order_status']
      || !$order['delegate_id']) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }

    $order->update([
      'order_status'    => Order::STATUS_GIVE_TO_DELEGATE,
      'provider_status' => 'shipped', //todo: remove this
    ]);

    // emit to pusher
    event(new UpdateOrderEvent($request['order_id']));

    //todo: remove this
    $list = [
      'accepted'  => 'طلب جديد',
      'pending'   => 'تم تحضير الطلب',
      'shipped'   => 'تم تسليم الطلب',
      'delivered' => 'تم التوصيل',
    ];

    return $this->successResponse([
      'order'            => OrderBranchResource::make($order),
      'orderStatus'      => $order->provider_status,
      'order_status'     => $order->order_status,
      'order_status_msg' => $order->order_status_msg,
      // 'allStatus'         => $list,
    ]);
  }

  public function getOrderStatus(Request $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $order = $this->orderRepo->find($request['order_id']);

    $data = [
      'processing_time' => $order->provider['processing_time'] ?? 0,
      'client_lat'      => $order['lat'],
      'client_lng'      => $order['lng'],
      'provider_lat'    => $order->branch['lat'],
      'provider_lng'    => $order->branch['lng'],
      'status'          => $order['status'],
      'status_lists'    => [
        'new'       => 'تم استلام الطلب',
        'Prepare'   => 'جاري تحضير الطلب',
        'received'  => 'الغزال وصل لاستلام طلبك',
        'onWay'     => 'جاري التوصيل',
        'delivered' => 'تم التوصيل',
      ],
    ];

    return $this->successResponse($data);
  }

  public function orderAny(Request $request) {
    $validator = Validator::make($request->all(), [
      'notes'            => 'required',
      'receive_lat'      => 'required',
      'receive_lng'      => 'required',
      'receive_address'  => 'required',
      'delivery_lat'     => 'required',
      'delivery_lng'     => 'required',
      'delivery_address' => 'required',
      'image'            => 'nullable|mimes:jpg,png,jpeg',
      'order_value'      => 'required', // 1 || 2 || 3
      'date' => 'nullable',
      'time'             => 'nullable',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $user      = auth()->user();
    $distance  = GetPathAndDirections($request['receive_lat'], $request['receive_lng'], $request['delivery_lat'], $request['delivery_lng']);
    $shipPrice = ($distance > settings('minkm_shippingPrice')) ? ($distance * settings('shippingValue')) + settings('shippingPrice') : (double) settings('shippingPrice');
    $tax       = settings('tax') ?? 0;
    $orderTax  = $shipPrice * $tax / 100;
    $time      = $request['time'] ? date('H:i:s', strtotime($request['time'])) : null;
    $date      = $request['date'] ? date('Y-m-d', strtotime($request['date'])) : null;

    $order = $this->orderRepo->create([
      'status'             => 'new',
      'order_status'       => Order::STATUS_WAITING,
      'live'               => 1,
      'user_id'            => $user['id'],
      'final_total'        => $shipPrice,
      'vat_amount'         => round($orderTax, 2),
      'vat_per'            => $tax,
      'shipping_price'     => $shipPrice,
      'map_desc'           => $request['delivery_address'],
      'lat'                => $request['delivery_lat'],
      'lng'                => $request['delivery_lng'],
      'receiving_map_desc' => $request['receive_address'],
      'receiving_lat'      => $request['receive_lat'],
      'receiving_lng'      => $request['receive_lng'],
      'time'               => $time,
      'date'               => $date,
      'order_type'         => 'free',
      'order_value'        => $request['order_value'],
      'created_date'       => Carbon::now()->format('Y-m-d H:i'),
    ]);

    if (isset($request['image'])) {
      $order->images()->create([
        'image' => $this->uploadFile($request['image'], 'orders'),
      ]);
    }
    return $this->successResponse();
  }

  public function cancelOrder(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $order = $this->orderRepo->find($request['order_id']);

    //todo: remove this and leave order_status check
    if ('user_cancel' == $order['status']) {
      return $this->ApiResponse('fail', trans('api.canceledBefore'));
    }

    if (Order::STATUS_CANCELED == $order['order_status']) {
      return $this->ApiResponse('fail', trans('api.canceledBefore'));
    }

    $order->update([
      //todo: remove status
      'status'       => 'user_cancel',
      'order_status' => Order::STATUS_CANCELED,
    ]);

    return $this->successResponse();
  }

  public function oldOrders(LangRequest $request) {
    $user   = auth()->user();
    $orders = $user->ordersAsUser()->exists()->orderBy('orders.created_at', 'desc')->paginate(10);
    return $this->successResponse(OrderCollection::make($orders));
  }

  public function OrderDetails(Request $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => ['required', Rule::exists('orders', 'id')],
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $user  = auth()->user();
    $order = $this->orderRepo->find($request['order_id']);

    if ($order['user_id'] != $user['id']) {
      return $this->ApiResponse('fail', trans('api.notAuthorized'));
    }

    $allStatus = Order::userStatus();

    return $this->successResponse([
      // todo: remove nxt 3 lines
      'allStatus'        => $allStatus,
      'status_name'      => Order::userStatus($order['order_status']),

      'order_status'     => $order['order_status'],

      'details'          => new OrderDetailResource($order),
    ]);
  }

  public function providerSingleOrder(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $order = $this->orderRepo->find($request['order_id']);
    return $this->successResponse(SingleOrderBranchResource::make($order));
  }

  public function providerCancelOrders(LangRequest $request) {
    $user   = auth('branchApi')->user();
    $orders = $user->orders()
    // ->where('orders.status', 'user_cancel') // todo: remove this
      ->where('orders.order_status', Order::STATUS_CANCELED)
      ->paginate(10);

    return $this->successResponse(OrderBranchCollection::make($orders));
  }

  public function providerRefusedOrders(LangRequest $request) {
    $user   = auth('branchApi')->user();
    $orders = $user->orders()
    // ->where('orders.status', 'refused') //todo:remove this
      ->where('orders.order_status', Order::STATUS_REFUSED)
      ->paginate(10);

    return $this->successResponse(OrderBranchRefuseCollection::make($orders));
  }

  public function providerLateOrders(LangRequest $request) {
    $user   = auth('branchApi')->user();
    $orders = $user->orders()
      ->whereDate('orders.date', '<', Carbon::now()->addDay()->format('Y-m-d'))
      ->paginate(10);

    // todo: union order resources
    return $this->successResponse(OrderBranchRefuseCollection::make($orders));
  }

  public function providerPostponedOrders(LangRequest $request) {
    $user   = auth('branchApi')->user();
    $orders = $user->orders()
      ->where('orders.order_type', 'postponed')
      ->paginate(10);

    return $this->successResponse(OrderBranchCollection::make($orders));
  }

  public function orderRefused(Request $request) {
    $validator = Validator::make($request->all(), [
      'order_id'      => 'required|exists:orders,id,deleted_at,NULL',
      'refused_notes' => 'required|string',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $order = $this->orderRepo->find($request['order_id']);
    $user  = auth('branchApi')->user();

    //todo: update provider status to order_status check
    if ($order['branch_id'] != $user['id'] || $order['order_status'] == Order::STATUS_PREPARED) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }

    $order->update([
      'order_status'    => Order::STATUS_REFUSED,
      'refused_notes'   => $request['refused_notes'],
    ]);

    // todo: update notification
    $this->send_notify($order->user, 'الطلب قد رفض', 'reques has been rejected', ' تم رفض الطلب رقم ' . $order['order_num'], 'Your order number ' . $order['order_num'] . ' was refused', 'branchApi', $order['id'], $order['status']);

    return $this->ApiResponse('success', trans('api.refused'));
  }

  public function orderStatus(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $order = $this->orderRepo->find($request['order_id']);
    $user  = auth('branchApi')->user();

    if ($order['branch_id'] != $user['id'] || $order['order_status'] == Order::STATUS_REFUSED) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }

    $list = [
      'accepted'  => 'طلب جديد',
      'pending'   => 'accepted' == $order['provider_status'] ? 'جاري تحضير الطلب' : 'تم تحضير الطلب',
      'shipped'   => 'pending' == $order['provider_status'] ? 'تسليم الطلب' : 'تم تسليم الطلب',
      'delivered' => 'تم التوصيل',
    ];

    return $this->successResponse([
      'order'       => OrderBranchResource::make($order),
      'order_status' => $order['order_status'],
      'allStatus'   => $list,
    ]);
  }

  public function ordersRating(LangRequest $request) {
    $user   = auth('branchApi')->user();
    $orders = $user->orders()
      ->whereHas('ratings', function ($rate) use ($user) {
        return $rate->where('rateable_type', Branch::class)
          ->where('rateable_id', $user['id']);
      })
      ->paginate(10);

    return $this->successResponse([
      'orders' => OrderBranchCollection::make($orders),
    ]);
  }

  public function orderSingleRating(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $order = $this->orderRepo->find($request['order_id']);
    $user  = auth('branchApi')->user();

    if ($order['branch_id'] != $user['id'] || $order['order_status'] == Order::STATUS_REFUSED) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }

    return $this->successResponse(SingleOrderRateBranchResource::make($order));
  }

  public function providerNewOrders(LangRequest $request) {
    $user   = auth('branchApi')->user();
    $orders = $user->orders()->where('orders.order_status', Order::STATUS_WAITING)->where('orders.live', 1)->orderBy('created_date', 'desc')->paginate(10);
    return $this->successResponse(OrderBranchCollection::make($orders));
  }

  //? what is this doing?
  public function providerGetDelegate(Request $request) {
    $validator = Validator::make($request->all(), [
      'notes'            => 'required',
      'receive_lat'      => 'required',
      'receive_lng'      => 'required',
      'receive_address'  => 'required',
      'delivery_lat'     => 'required',
      'delivery_lng'     => 'required',
      'delivery_address' => 'required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $user      = auth()->user();
    $distance  = GetPathAndDirections($request['receive_lat'], $request['receive_lng'], $request['delivery_lat'], $request['delivery_lng']);
    $shipPrice = ($distance > settings('minkm_shippingPrice')) ? ($distance * settings('shippingValue')) + settings('shippingPrice') : (double) settings('shippingPrice');
    $tax       = settings('tax') ?? 0;
    $orderTax  = $shipPrice * $tax / 100;

    $this->orderRepo->create([
      'status'             => 'new',
      'live'               => 1,
      'user_id'            => $user['id'],
      'final_total'        => $shipPrice,
      'vat_amount'         => round($orderTax, 2),
      'vat_per'            => $tax,
      'shipping_price'     => $shipPrice,
      'map_desc'           => $request['delivery_address'],
      'lat'                => $request['delivery_lat'],
      'lng'                => $request['delivery_lng'],
      'receiving_map_desc' => $request['receive_address'],
      'receiving_lat'      => $request['receive_lat'],
      'receiving_lng'      => $request['receive_lng'],
      'order_type'         => 'free',
      'created_date'       => Carbon::now()->format('Y-m-d H:i'),
    ]);

    return $this->successResponse();
  }

  public function updateOrderEvent($order_id) {
    //! gust for test
    // $order = Order::findOrFail($order_id);
    event(new UpdateOrderEvent($order_id));
    return $this->successResponse();
  }

  public function updateLocationEvent(Request $request) {
    //    dd(auth('api')->user());
    //! gust for test
    $lat = $request->lat;
    $lng = $request->lng;
    $userId = 1;
    //    event(new UpdateDriverLocation($userId, $lat, $lng));
    broadcast(new UpdateDriverLocation($userId, $lat, $lng));
    return $this->successResponse();
  }
}
