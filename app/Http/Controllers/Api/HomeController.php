<?php

namespace App\Http\Controllers\Api;

use App;
use App\Entities\Branch;
use App\Entities\Offer;
use App\Entities\Slider;
use App\Entities\Visit;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Http\Resources\Groups\GroupResource;
use App\Http\Resources\Offers\OfferResource;
use App\Http\Resources\Settings\CategoryResource;
use App\Http\Resources\Settings\SliderResource;
use App\Http\Resources\Users\ProviderResource;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\IGroup;
use App\Repositories\Interfaces\IOffer;
use App\Repositories\Interfaces\ISlider;
use App\Repositories\Interfaces\IUser;
use App\Repositories\Interfaces\IUserAddress;
use App\Repositories\Interfaces\IZone;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/** import */

class HomeController extends Controller {
  use NotifyTrait;
  use ResponseTrait;
  public $userRepo,
  $addressRepo,
  $categoryRepo,
  $sliderRepo,
  $offerRepo,
  $groupRepo,
  $branchRepo,
    $zoneRepo;

  public function __construct(
    IGroup $groupRepo,
    IOffer $offerRepo,
    ISlider $sliderRepo,
    ICategory $categoryRepo,
    IUser $user,
    IUserAddress $address,
    IBranch $branch,
    IZone $zone
  ) {
    $this->userRepo     = $user;
    $this->addressRepo  = $address;
    $this->categoryRepo = $categoryRepo;
    $this->sliderRepo   = $sliderRepo;
    $this->groupRepo    = $groupRepo;
    $this->offerRepo    = $offerRepo;
    $this->zoneRepo     = $zone;
    $this->branchRepo   = $branch;
  }

  public function homeAddAddress(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'uuid'        => 'required',
      'lat'         => 'required',
      'lng'         => 'required',
      'country_id'  => 'required|exists:countries,id',
      'city_id'     => 'required|exists:cities,id',
      'name'        => 'required',
      'street'      => 'required',
      'building'    => 'required',
      'floor'       => 'required',
      'flat'        => 'required',
      'unique_sign' => 'required',
      'phone'       => 'required|numeric|digits_between:9,13',
    ], [
      'name.required' => app()->getLocale() == 'ar' ? 'اسم المنطقه مطلوبه' : 'state name is required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }
    $data            = array_filter($request->all());
    $user            = auth()->check() ? auth()->user() : null;
    $data['user_id'] = $user ? $user['id'] : null;
    $data['is_main'] = 1;
    $data['type']    = 'home';
    $address         = $this->addressRepo->create($data);
    $msg             = app()->getLocale() == 'ar' ? 'تم اضافة العنوان بنجاح' : 'success add address';
    return $this->ApiResponse('success', $msg, ['address_id' => $address['id']]);
  }

  public function home(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'uuid'        => 'required',
      'lat'         => 'required',
      'lng'         => 'required',
      'category_id' => 'required|exists:categories,id',
    ]);

    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    Visit::create(['device_type' => 'android']);

    $groups           = $this->groupRepo->findWhere(['category_id' => $request['category_id']]);
    $zoneBranchesIds  = $this->zoneRepo->getZoneBranches()->pluck('id')->toArray();
    $categories       = $this->categoryRepo->getByBranchesIds($zoneBranchesIds);
    $sliders          = $this->sliderRepo->getByNearestBranchesIds($request,$zoneBranchesIds);
    $offers           = $this->offerRepo->getByNearestBranchesIds($request,$zoneBranchesIds);
    $providers        = [];

    if (count($groups) > 0) {
      // $providers = $this->branchRepo->getByGroupsBranchesIds($request,$zoneBranchesIds);
      $providers = $this->branchRepo->getCategoryBranchesInZone($request['category_id'],$zoneBranchesIds);
    }
    # save current location and zone to user
    if(auth('api')->check()){
        $zone_id        = $this->zoneRepo->getZoneId();
        $user           = auth('api')->user();
        $user->lat      = $request['lat'];
        $user->lng      = $request['lng'];
        $user->zone_id  = $zone_id;
        $user->save();
    }

    return $this->successResponse([
      //'categories' => CategoryResource::collection($categories),
      'sliders'    => SliderResource::collection($sliders),
      'offers'     => OfferResource::collection($offers),
      'groups'     => GroupResource::collection($groups),
      'providers'  => ProviderResource::collection($providers),
    ]);
  }

  public function search(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'key'         => 'required',
      'category_id' => 'required|exists:categories,id',
      'city_id'     => 'required|exists:cities,id',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }
    $users = Branch::distance($request['lat'], $request['lng'], $request['city_id'])->whereHas('user', function ($q) use ($request) {
      $q->active();
      $q->where('category_id', $request['category_id']);
      $q->whereHas('items', function ($item) use ($request) {
        $item->where('status', 1);
        $item->whereHas('branches', function ($branch) {
          $branch->whereColumn('branches.id', '=', 'branch_id');
        });
        $item->where('title->ar', 'LIKE', '%' . $request['key'] . '%');
        $item->orWhere('title->en', 'LIKE', '%' . $request['key'] . '%');
      });
    })->get();
    $providers = $users->unique('user_id');
    return $this->successResponse([
      'providers' => ProviderResource::collection($providers),
    ]);
  }

  public function filter(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'categories' => 'required|array',
      'city_id'    => 'required|exists:cities,id',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }
    $users = Branch::distance($request['lat'], $request['lng'], $request['city_id'])->whereHas('user', function ($q) use ($request) {
      $q->active();
      $q->whereHas('categories', function ($category) use ($request) {
        $category->whereIn('user_categories.subcategory_id', $request['categories']);
      });
    })->get();
    $providers = $users->unique('user_id');
    return $this->successResponse([
      'providers' => ProviderResource::collection($providers),
    ]);
  }

  public function checkPointMap(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'lat' => 'required',
      'lng' => 'required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }
    $exists = $this->zoneRepo->checkZonePoint();
    if (!$exists) {
      $msg = app()->getLocale() == 'ar' ? 'خارج نطاق التغطيه' : 'Out of coverage';
      $name = '';
      return $this->ApiResponse('success', $msg, [
          'type' => 0,
          'zoneName' => $name,
      ]);
    }
    $msg = app()->getLocale() == 'ar' ? 'داخل نطاق التغطيه' : 'In of coverage';
    $name = $this->zoneRepo->getZoneName();
    return $this->ApiResponse('success', $msg, [
        'type' => 1,
        'zoneName' => $name,
    ]);
  }

  public function checkZoneName(Request $request)
  {
      $validator = Validator::make($request->all(), [
          'lat' => 'required',
          'lng' => 'required',
      ]);
      if ($validator->fails()) {
          return $this->ApiResponse('fail', $validator->errors()->first());
      }
      $name = $this->zoneRepo->getZoneName();
      return $this->successResponse($name ?? '');
  }
  public function testFcm($user_id) {
    $user = $this->userRepo->find($user_id);

    if ($user) {
      $data['title'] = app()->getLocale() == 'ar' ? 'test title ar' : 'test title en';
      $data['body']  = app()->getLocale() == 'ar' ? 'test body ar' : 'test body en';
      if ($user->Devices) {
        foreach ($user->Devices as $device) {
          if ($device->device_id) {
            $this->send_fcm($device->device_id, $data, $device->device_type);
          }
        }
      }

      return 'sent';
    }
    return 'not sent id err';
  }

  public function getZoneBranches() {
    $branches = $this->zoneRepo->getZoneBranches();

    return $this->successResponse([
      'branches' => $branches,
    ]);
  }

  public function checkBranchZonePoint(Request $request) {
    $validator = Validator::make($request->all(), [
      'lat'       => 'required',
      'lng'       => 'required',
      'branch_id' => 'required|exists:branches,id',
    ]);

    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $branch = $this->branchRepo->find($request->branch_id);
    $check  = $this->zoneRepo->checkBranchZonePoint($request, $branch);
    $msg    = $check ? awtTrans('داخل نطاق التغطية') : awtTrans('خارج نطاق التغطية');
    return $this->ApiResponse('success', $msg, $check);
  }

}
