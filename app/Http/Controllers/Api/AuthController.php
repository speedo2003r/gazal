<?php

namespace App\Http\Controllers\Api;

use App;
use App\Entities\Notification;
use App\Entities\OrderProduct;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Delegate\DelegateRegisterRequest;
use App\Http\Requests\Api\LangRequest;
use App\Http\Requests\Api\login\LoginRequest;
use App\Http\Requests\Api\login\ProviderLoginRequest;
use App\Http\Requests\Api\profile\UpdateProfileRequest;
use App\Http\Requests\Api\register\ActivationRequest;
use App\Http\Requests\Api\register\UserRegisterRequest;
use App\Http\Resources\Branches\BranchResource;
use App\Http\Resources\Notifications\NotificationCollection;
use App\Http\Resources\Users\AddressResource;
use App\Http\Resources\Users\DelegateResource;
use App\Http\Resources\Users\UserResource;
use App\Models\User;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\IDelegate;
use App\Repositories\Interfaces\IDevice;
use App\Repositories\Interfaces\INotification;
use App\Repositories\Interfaces\IOrder;
use App\Repositories\Interfaces\IUser;
use App\Repositories\Interfaces\IUserAddress;
use App\Rules\watchOldPassword;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\SmsTrait;
use App\Traits\UploadTrait;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {

  use NotifyTrait;
  use SmsTrait;
  use ResponseTrait;
  use UploadTrait;
  public $userRepo, $deviceRepo, $branch, $addressRepo, $delegateRepo, $notifyRepo, $order;

  public function __construct(
    IBranch $branch,
    IDelegate $delegate,
    IOrder $order,
    IUser $user,
    IUserAddress $address,
    IDevice $device,
    INotification $notify
  ) {
    $this->branch       = $branch;
    $this->order        = $order;
    $this->userRepo     = $user;
    $this->deviceRepo   = $device;
    $this->addressRepo  = $address;
    $this->notifyRepo   = $notify;
    $this->delegateRepo = $delegate;
  }

  /************************* Start Register ************************/
  public function UserRegister(UserRegisterRequest $request) {
    $user = $this->userRepo->create($request->validated());
    $this->sendActiveCodeToUserSms($user);

    return $this->successResponse([
      'cartCount'    => 0,
      'phone'        => $user->phone,
      'country_code' => $user->country_code,
      'full_phone'   => $user->full_phone,
    ]);
  }

  public function DelegateRegister(DelegateRegisterRequest $request) {
      $data  = array_filter($request->except('avatar','id_avatar', 'licence_image', 'car_front_image', 'car_back_image'));
      $data['accepted'] = 0;
      if (isset($request['avatar'])) {
          $data['avatar'] = $this->uploadFile($request['avatar'], 'users');
      }
      if (isset($request['licence_image'])) {
          $licence  = $this->uploadFile($request['licence_image'], 'delegates');
          $data['licence_image'] = $licence;
      }
      if (isset($request['id_avatar'])) {
          $avatar   = $this->uploadFile($request['id_avatar'], 'delegates');
          $data['id_avatar'] = $avatar;
      }
      if (isset($request['car_front_image'])) {
          $car_front  = $this->uploadFile($request['car_front_image'], 'delegates');
          $data['car_front_image'] = $car_front;
      }
      if (isset($request['car_back_image'])) {
          $car_back  = $this->uploadFile($request['car_back_image'], 'delegates');
          $data['car_back_image'] = $car_back;
      }
      $user = $this->delegateRepo->create($data);
    //        $this->sendSMs($user['phone'],$user['v_code']);
    return $this->successResponse([
      'phone'        => $user['phone'],
      'country_code' => $user['country_code'],
      'full_phone'   => $user['full_phone'],
    ]);
  }

  public function shareCode(Request $request) {
    $user       = auth()->user();
    $share_code = $user['share_code'];
    return $this->successResponse($share_code);
  }

  # active account after register - if ok logged in
  public function Activation(ActivationRequest $request) {
      if($request['user_type'] == 'client'){
          $model = $this->userRepo;
      }elseif($request['user_type'] == 'delegate'){
          $model = $this->delegateRepo;
      }
    if (isset($request['forget'])) {
      $user = $model->findWhere([
        'phone'        => $request->input('phone'),
        'country_code' => $request->input('country_code'),
      ])->first();
      if ($user['v_code'] != $request['code']) {
        $msg = app()->getLocale() == 'ar' ? 'الكود الذي أدخلته غير صحيح' : 'The code you entered is incorrect';
        return $this->ApiResponse('fail', $msg);
      } else {
        $msg = app()->getLocale() == 'ar' ? 'الكود صحيح' : 'The code you entered is correct';
        return $this->ApiResponse('success', $msg);
      }
    }
    // todo: update with country_code effect
    if (isset($request['replaced'])) {
      $user = $model->findWhere(['replace_phone' => $request->input('phone')])->first();

      if ($request->input('code') === $user->v_code) {
        $replacePhone        = $user['replace_phone'];
        $user->phone         = $replacePhone;
        $user->replace_phone = null;
        $user->save();
        return $this->ApiResponse('success', 'تم تغيير الهاتف بنجاح');
      }
      return $this->ApiResponse('fail', 'الكود غير صحيح');
    } else {
      # activation after register
      $user = $model->findWhere([
        'phone'        => $request->input('phone'),
        'country_code' => $request->input('country_code'),
      ])->first();
      if (!$user) {
        return $this->ApiResponse('fail', awtTrans('الهاتف غير موجود'));
      }
      if ($request->input('code') === $user->v_code) {
        $user->update(['active' => 1, 'online' => 1]);
        $cartCount = 0;
        $orders    = $this->order->findWhere(['uuid' => $request['uuid'], 'user_id' => null, 'live' => 0]);
        if (count($orders) > 0) {
          $arrOrders = [];
          foreach ($orders as $value) {
            $value->user_id = $user['id'];
            $value->save();
            $arrOrders[] = $value['id'];
          }
          $cartCount = OrderProduct::whereIn('order_products.order_id', $arrOrders)->count();
        }
          $this->deviceRepo->makeNewDevice($user);

        // if ((auth()->check() && 0 == $user->accepted) || 0 == $user->accepted) {
        if ($user->accepted == 0) {
          return response()->json([
            'key'   => 'not_accepted',
            'value' => 4,
            'msg'   => 'تم تفعيل الهاتف برجاء الرجوع للاداره لتفعيل حسابك',
          ]);
        }

        return $this->ApiResponse('success', 'تم تفعيل الهاتف بنجاح', [
          'cartCount' => $cartCount,
          'user'      => userResource::make($user),
          'token'     => get_class($user) == User::class ? auth('api')->login($user) : auth('delegateApi')->login($user),
        ]);
      }

      return $this->ApiResponse('fail', 'الكود غير صحيح');
    }
  }

  public function sendActiveCode(Request $request) {
    $data = $this->userRepo->getCurrentModel();
    $table = $data['table'];
    $model = $data['model'];
    $validate = Validator::make($request->all(), [
      'user_type'        => 'required|in:client,delegate',
      'phone'        => 'required|exists:'.$table.',phone,deleted_at,NULL',
      'country_code' => 'required|digits_between:2,7',
    ]);
    if ($validate->fails()) {
      return $this->ApiResponse('fail', $validate->errors()->first());
    }

    $user = $model->where([
      'phone'        => $request->input('phone'),
      'country_code' => $request->input('country_code'),
    ])->first();
    $user->update(['v_code' => generateCode()]);
    // sendSMS($user['phone'],$user['v_code']);
    return $this->successResponse(['v_code' => $user->refresh()->v_code]);
  }

  public function resetPassword(Request $request) {
    $data = $this->userRepo->getCurrentModel();
    $table = $data['table'];
    $model = $data['model'];
    $validator = Validator::make($request->all(), [
      'user_type'        => 'required|in:client,delegate',
      'phone'        => 'required|exists:'.$table.',phone,deleted_at,NULL',
      'country_code' => 'required|digits_between:2,7',
      'password'     => ['required', 'string', 'max:100'],
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }
    $user = $model->where([
      'phone'        => $request->input('phone'),
      'country_code' => $request->input('country_code'),
    ])->first();
    $user->update(['password' => $request->password]);
    return $this->ApiResponse('success', trans('api.resetPassword'));
  }

  public function ResendActiveCode(Request $request) {
    $data = $this->userRepo->getCurrentModel();
    $table = $data['table'];
    $model = $data['model'];
    $validate = Validator::make($request->all(), [
      'user_type'        => 'required|in:client,delegate',
      'phone'        => 'required|exists:'.$table.',phone,deleted_at,NULL',
      'country_code' => 'required|digits_between:2,7',
    ]);

    if ($validate->fails()) {
      return $this->ApiResponse('fail', $validate->errors()->first());
    }

    $user = $model->where([
      'phone'        => $request->input('phone'),
      'country_code' => $request->input('country_code'),
    ])->first();

    $user->update(['v_code' => generateCode()]);

    // $this->sendSms($user['phone'],$user['v_code']);

    return $this->successResponse(['v_code' => $user->refresh()->v_code]);

  }
  /************************* End Register ************************/

  /************************   start basics *******************************/
  public function Login(LoginRequest $request) {
    # users and delegates
    $data = $this->userRepo->getCurrentModel();
    $guard = $data['guard'];
    $model = $data['model'];
    $user = $model->where([
      'phone'        => $request->input('phone'),
      'country_code' => $request->input('country_code'),
    ])->first();
    if (!$user) {
      return $this->ApiResponse('fail', awtTrans('برجاء التأكد من بيانات المستخدم'));
    }

    if ($user->banned) {
      return $this->ApiResponse('is_banned', awtTrans('لقد تم حظرك من قبل الاداره'));
    }

    if (!$user->active) {
      return $this->ApiResponse('not_active', awtTrans('يرجي تفعيل الهاتف قبل الاستكمال'));
    }

    if (!$user->accepted) {
      return $this->ApiResponse('not_accepted', awtTrans('برجاء الرجوع للاداره لتفعيل حسابك'));
    }

    $isSamePassword = $this->userRepo->checkPassword($user, $request->input('password'));
    if (!$isSamePassword) {
      return $this->ApiResponse('fail', trans('api.wrongLogin'));
    }

    $token = auth($guard)->attempt([
      'phone'        => $user->phone,
      'country_code' => $user->country_code,
      'password'     => $request->input('password'),
    ]);
    $this->deviceRepo->makeNewDevice($user);
    $user->update([
      'online' => 1,
    ]);

    $cartCount = $this->order->getCountOrdersByUuid($user);
    $this->addressRepo->saveAddressesToUser($user);

    return $this->successResponse([
      'cartCount' => $cartCount,
      'user'      => 'delegate' == $user['user_type'] ? DelegateResource::make($user) : userResource::make($user),
      'token'     => $token,
    ]);
  }

  public function ProviderLogin(ProviderLoginRequest $request) {
    $user = $this->branch->findWhere(['email' => $request['email']])->first();
    if (!$user) {
      return $this->ApiResponse('fail', awtTrans('برجاء التأكد من بيانات المستخدم'));
    }
    if (!$user->appear) {
      return $this->ApiResponse('not_accepted', awtTrans('برجاء الرجوع للاداره لتفعيل حسابك'));
    }
    $isSamePassword = $this->userRepo->checkPassword($user, convert_to_english($request->input('password')));
    if (!$isSamePassword) {
      return $this->ApiResponse('fail', trans('api.wrongLogin'));
    }
    $token  = auth('branchApi')->login($user);

    $this->deviceRepo->makeNewDevice($user);

    return $this->successResponse([
      'user'  => BranchResource::make($user),
      'token' => $token,
    ]);
  }

  public function Logout(Request $request) {
    $validator = Validator::make($request->all(), [
      'device_id' => 'required|exists:devices,device_id',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }
    $device = $this->deviceRepo->where(['device_id' => $request['device_id']])->first();
    $user = $device->user;
    if($user){
        $user->update([
            'online' => 0
        ]);
    }
    if ($device) {
      $this->deviceRepo->delete($device['id']);
    }
    return $this->successResponse();
  }

  public function UpdatePassword(LangRequest $request) {

    $data = $this->userRepo->getCurrentModel();
    $guard = $data['guard'];
    $user = auth($guard)->user();
    $validate = Validator::make($request->all(), [
      'user_type' => 'required|in:delegate,client',
      'old_password' => ['required', 'min:6', 'max:255', new watchOldPassword($guard)],
      'new_password' => 'required|min:6|max:255',
    ]);
    if (convert_to_english($request['new_password']) == $request['old_password']) {
      $msg = app()->getLocale() == 'ar' ? 'كلمة المرور الجديده هي نفس كلمة المرور القديمه' : 'new password is equal old password';
      return $this->ApiResponse('fail', $msg);
    }
    if ($validate->fails()) {
      return $this->ApiResponse('fail', $validate->errors()->first());
    }
    # update Password
      if(!$user){
          return  $this->ApiResponse('fail', 'user is undefined');
      }
    $user->update([
      'password' => convert_to_english($request['new_password']),
    ]);
    return $this->ApiResponse('success', trans('api.passwordUpdated'));

  }

  public function EditLang(Request $request) {
    $validate = Validator::make($request->all(), [
      'user_type' => 'required|in:delegate,client',
      'lang' => 'required|in:ar,en',
    ]);
    if ($validate->fails()) {
      return $this->ApiResponse('fail', $validate->errors()->first());
    }
    $data = $this->userRepo->getCurrentModel();
    $guard = $data['guard'];
    $user = auth($guard)->user();
      if(!$user){
          return  $this->ApiResponse('fail', 'user is undefined');
      }
      # update user
    $user->update(['lang' => $request['lang']]);
    $msg = trans('api.Updated');
    return $this->ApiResponse('success', $msg);
  }

  # switch notification status for receive fcm or not
  public function NotificationStatus(Request $request) {
      $validate = Validator::make($request->all(), [
          'user_type' => 'required|in:delegate,client',
      ]);
      if ($validate->fails()) {
          return $this->ApiResponse('fail', $validate->errors()->first());
      }
    $data = $this->userRepo->getCurrentModel();
    $guard = $data['guard'];
    $user = auth($guard)->user();
    if(!$user){
        return  $this->ApiResponse('fail', 'user is undefined');
    }
    # update user
      $user->update(['notify' => !$user->notify]);
    return $this->successResponse(['notify' => $user->refresh()->notify]);
  }
  /************************   End basics *******************************/

  /************************   Start profile *******************************/
  public function ShowProfile(Request $request) {
    $validate = Validator::make($request->all(), [
      'user_type' => 'required|in:delegate,client',
      'lang' => 'required|in:ar,en',
    ]);
    if ($validate->fails()) {
      return $this->ApiResponse('fail', $validate->errors()->first());
    }
    $data = $this->userRepo->getCurrentModel();
    $guard = $data['guard'];
    $user = auth($guard)->user();
      if(!$user){
          return  $this->ApiResponse('fail', 'user is undefined');
      }
    return $this->successResponse(new UserResource($user));
  }

  public function UpdateProfile(UpdateProfileRequest $request) {
    $data = $this->userRepo->getCurrentModel();
    $guard = $data['guard'];
    $user = auth($guard)->user();
    if ($user['phone'] != $request['phone']) {
      $user['replace_phone'] = $request['phone'];
      $user['v_code']        = generateCode();
      $user->save();
    } elseif ($user['phone'] == $request['phone']) {
      $user['replace_phone'] = null;
      $user->save();
    }
    # update user data
    if ($request->has('_avatar')) {
      $logo3 = $this->uploadFile($request['_avatar'], 'users');
      $request->request->add(['avatar' => $logo3]);
    }
    $user->update(array_filter($request->except('phone')));
    $userData  = $this->userRepo->find($user['id']);
    $replace_phone = null != $user['replace_phone'] ? true : false;
    return $this->successResponse(['user' => userResource::make($userData), 'replace_phone' => $user['replace_phone'] ?? '', 'replaced' => $replace_phone]);
  }

  /************************   End profile *******************************/

  /*********************** Start notifications ***********************/
  public function Notifications(Request $request) {
      $validate = Validator::make($request->all(), [
          'user_type' => 'required|in:delegate,client',
      ]);
      if ($validate->fails()) {
          return $this->ApiResponse('fail', $validate->errors()->first());
      }
    $data = $this->userRepo->getCurrentModel();
    $guard = $data['guard'];
    $user = auth($guard)->user();
    # make all notifications seen = 1
    $UnreadNotifications = $user->notifications->where('seen', 0);
    foreach ($UnreadNotifications as $UnreadNotification) {
      $this->notifyRepo->update(['seen' => 1], $UnreadNotification['id']);
    }
    $data = new NotificationCollection($user->Notifications()->orderBy('notifications.id', 'desc')->paginate(10));
    return $this->successResponse($data);
  }
  public function providerNotifications(Request $request) {
    $user = auth('branchApi')->user();
    # make all notifications seen = 1
    $UnreadNotifications = $user->notifications->where('seen', 0);
    foreach ($UnreadNotifications as $UnreadNotification) {
      $this->notifyRepo->update(['seen' => 1], $UnreadNotification['id']);
    }
    $data = new NotificationCollection($user->notifications()->orderBy('notifications.id', 'desc')->paginate(10));
    return $this->successResponse($data);
  }
  public function UnreadNotifications(Request $request) {
      $validate = Validator::make($request->all(), [
          'user_type' => 'required|in:delegate,client',
      ]);
      if ($validate->fails()) {
          return $this->ApiResponse('fail', $validate->errors()->first());
      }
    $data = $this->userRepo->getCurrentModel();
    $guard = $data['guard'];
    $user = auth($guard)->user();
    $num  = $this->notifyRepo->findWhere(['tomodel_id' => $user['id'],'tomodel_type' => get_class($user), 'seen' => 0])->count();
    return $this->successResponse(['count' => $num]);
  }
  public function deleteNotification(LangRequest $request) {
    $notify = $this->notifyRepo->find($request['id']);
    if (auth()->id() != $notify['to_id']) {
      return $this->ApiResponse('fail', 'notification is undefined');
    }
    $this->notifyRepo->delete($request['id']);
    return $this->ApiResponse('success', trans('api.delete'));
  }
  public function Fakenotifications($id) {
    for ($i = 1; $i <= 100; $i++) {
      $data[] = [
        'to_id'      => $id,
        'from_id'    => $id,
        'message_ar' => 'notification message ar',
        'message_en' => 'notification message en',
        'type'       => 'test',
      ];
    }
    Notification::insert($data);
    return 'fake notification added to user no ' . $id;
  }
  /*********************** End notifications ***********************/

  /*********************** Start user addresses / branches  ***********************/
  public function Addresses(Request $request) {
    $user = auth('api')->user();
    $data = AddressResource::collection($user->Addresses);
    return $this->successResponse($data);
  }
  public function AddAddress(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'lat'         => 'required',
      'lng'         => 'required',
      //            'country_id' => 'required|exists:countries,id',
      //            'city_id' => 'required|exists:cities,id',
      'type'        => 'required',
      'name'        => 'required',
      'street'      => 'required',
      'building'    => 'required',
      'floor'       => 'required',
      'flat'        => 'required',
      'unique_sign' => 'required',
      'phone'       => 'required|numeric|digits_between:9,13',
    ], [
      'type.required' => app()->getLocale() == 'ar' ? 'نوع العنوان منزل أم عمل مطلوبه' : 'address type is required',
      'name.required' => app()->getLocale() == 'ar' ? 'اسم المنطقه مطلوبه' : 'state name is required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $data            = array_filter($request->all());
    $user            = auth()->check() ? auth()->user() : null;
    $data['user_id'] = $user ? $user['id'] : null;
    $data['is_main'] = 0;
    $data['type']    = 'home';
    $address         = $this->addressRepo->create($data);
    return $this->successResponse(new AddressResource($address));
  }

  public function EditAddress(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'address_id'  => 'required|exists:user_addresses,id',
      'lat'         => 'required',
      'lng'         => 'required',
      'type'        => 'required',
      'country_id'  => 'required|exists:countries,id',
      'city_id'     => 'required|exists:cities,id',
      'map_desc'    => 'required',
      'name'        => 'required',
      'street'      => 'required',
      'building'    => 'required',
      'floor'       => 'required',
      'flat'        => 'required',
      'unique_sign' => 'required',
      'phone'       => 'required|numeric|digits_between:9,13',
    ], [
      'type.required' => app()->getLocale() == 'ar' ? 'نوع العنوان منزل أم عمل مطلوبه' : 'address type is required',
      'name.required' => app()->getLocale() == 'ar' ? 'اسم المنطقه مطلوبه' : 'state name is required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $address = $this->addressRepo->find($request->address_id);
    if (!$address) {
      return $this->ApiResponse('fail', 'this address is undefined');
    }
    $this->addressRepo->update(array_filter($request->all()), $address['id']);
    return $this->ApiResponse('success', trans('api.addressEdit'));
  }

  public function DeleteAddress(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'address_id' => 'required|exists:user_addresses,id',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $address = $this->addressRepo->find($request->address_id);
    # delete permanently
    $user = auth()->user();
    if (!$address && ($address->user['id'] != $user['id'])) {
      return $this->ApiResponse('fail', 'this address is undefined');
    }
    $address->delete();
    return $this->ApiResponse('success', trans('api.addressDeleted'));
  }
  /*********************** End user addresses / branches  ***********************/

  /*********************** Start wallet ***********************/
  public function Wallet(Request $request) {
    $user = auth()->user();
    return $this->successResponse([
      'wallet'   => $user['wallet'],
      'currency' => trans('api.SAR'),
    ]);
  }

  public function DelegateWallet(Request $request) {
    $user = auth('delegateApi')->user();
    return $this->successResponse($user['wallet']);
  }
  /*********************** End wallet ***********************/

}
