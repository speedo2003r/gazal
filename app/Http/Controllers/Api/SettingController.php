<?php

namespace App\Http\Controllers\Api;

use App\Entities\Category;
use App\Entities\Setting;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Http\Resources\Offers\OfferCollection;
use App\Http\Resources\Settings\CarResource;
use App\Http\Resources\Settings\CategoryResource;
use App\Http\Resources\Settings\CitiesResource;
use App\Http\Resources\Settings\SocialResource;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\ICarType;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\IComplaint;
use App\Repositories\Interfaces\IContactUs;
use App\Repositories\Interfaces\IJoinUs;
use App\Repositories\Interfaces\IOffer;
use App\Repositories\Interfaces\IPage;
use App\Repositories\Interfaces\ISetting;
use App\Repositories\Interfaces\ISlider;
use App\Repositories\Interfaces\ISocial;
use App\Repositories\Interfaces\ISuggest;
use App\Repositories\Interfaces\IUser;
use App\Repositories\Interfaces\IZone;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SettingController extends Controller {
  use ResponseTrait;
  public $pageRepo,$zoneRepo,$branchRepo, $join, $city, $carType, $suggest, $complaint, $sliderRepo, $userRepo, $settingRepo, $socialRepo, $contactRepo, $categoryRepo, $offer;
  public function __construct(
    ICarType $carType,
    IComplaint $complaint,
    ICity $city,
    IJoinUs $join,
    ISuggest $suggest,
    IOffer $offer,
    ISlider $slider,
    IPage $page,
    IUser $user,
    ISetting $setting,
    ISocial $social,
    IContactUs $contact,
    IBranch $branch,
    ICategory $category,
    IZone $zone
  ) {
    $this->userRepo     = $user;
    $this->pageRepo     = $page;
    $this->sliderRepo   = $slider;
    $this->settingRepo  = $setting;
    $this->socialRepo   = $social;
    $this->contactRepo  = $contact;
    $this->categoryRepo = $category;
    $this->suggest      = $suggest;
    $this->complaint    = $complaint;
    $this->offer        = $offer;
    $this->join         = $join;
    $this->city         = $city;
    $this->carType      = $carType;
    $this->branchRepo      = $branch;
    $this->zoneRepo      = $zone;
  }

  public function Categories(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'lat'     => 'sometimes',
      'lng'     => 'sometimes',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    if($request['lat'] && $request['lng']){
        $branchesIds = $this->zoneRepo->getZoneBranches()->pluck('id')->toArray();
        $categories = $this->categoryRepo->getByBranchesIds($branchesIds);
    }else{
        $categories = Category::where(['parent_id' => null])->take(1)->get();
    }
    return $this->successResponse(CategoryResource::collection($categories));
  }

  public function getCities(LangRequest $request) {
    $cities = $this->city->all();
    return $this->successResponse(CitiesResource::collection($cities));
  }
  public function getCars(LangRequest $request) {
    $cars = $this->carType->all();
    return $this->successResponse(CarResource::collection($cars));
  }
  public function offers(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'city_id' => 'required|exists:cities,id',
      'lat'     => 'required',
      'lng'     => 'required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $offers = $this->offer->whereHas('user', function ($query) {
      $query->active();
      $query->whereHas('branches', function ($q) {
        $q->active();
      });
    })->distance($request['lat'], $request['lng'], $request['city_id'])->offerActive()->paginate(10);
    return $this->successResponse(OfferCollection::make($offers));
  }

  public function suggest(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'name'  => 'required|string',
      'notes' => 'required|string',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $user_id         = auth()->check() ? auth()->id() : null;
    $data            = array_filter($request->all());
    $data['user_id'] = $user_id;
    $this->suggest->create($data);
    return $this->successResponse();
  }

  public function complaint(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'name'  => 'required|string',
      'notes' => 'required|string',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $user_id         = auth()->check() ? auth()->id() : null;
    $data            = array_filter($request->all());
    $data['user_id'] = $user_id;
    $this->complaint->create($data);
    return $this->successResponse();
  }

  public function joinUs(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'name'        => 'required|string|max:191',
      'store_name'  => 'required|string|max:191',
      'category_id' => 'required|exists:categories,id',
      'phone'       => 'required|numeric|digits_between:9,13',
      'email'       => 'required|email|max:191',
      'city_id'     => 'required|exists:cities,id',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $data               = array_filter($request->all());
    $city               = $this->city->find($request['city_id']);
    $data['country_id'] = $city->country_id;
    $this->join->create($data);
    return $this->successResponse();
  }
  public function SiteData(Request $request, Setting $setting) {
    $socialData = SocialResource::collection($this->socialRepo->all());
    $siteData   = [
      'socialData'             => $socialData,
      'phone'                  => settings('phone') ?? '',
      'email'                  => settings('email') ?? '',
      'whatsapp'               => settings('whatsapp') ?? '',
      'isProduction'           => false,
      'tableAnimationIOS'      => true,
      'collectionAnimationIOS' => true,
      'mapZoom'                => 14,
      'fontScale'              => 1.12,
    ];
    return $this->successResponse($siteData);
  }

  public function getShipPriceDestinationBetween(Request $request) {
    $validator = Validator::make($request->all(), [
      'lat1' => 'required',
      'lng1' => 'required',
      'lat2' => 'required',
      'lng2' => 'required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('success', $validator->errors()->first());
    }
    $distance  = GetPathAndDirections($request['lat1'], $request['lng1'], $request['lat2'], $request['lng2']);
    $shipPrice = ($distance > settings('minkm_shippingPrice')) ? ($distance * settings('shippingValue')) + settings('shippingPrice') : (double) settings('shippingPrice');
    return $this->successResponse([
      'distance'  => $distance,
      'shipPrice' => $shipPrice,
    ]);
  }

  public function allCountriesWithFlags() {
    $allCountries = DB::table('all_countries')->orderByDesc('sort_number')->get();
    $allCountries = $allCountries->map(function ($country) {
      $imageName = Str::lower($country->iso);
      return [
        'id'         => $country->id,
        'iso'        => $country->iso,
        'nice_name'  => $country->nice_name,
        'lang_name'  => $country->lang_name ?? 'test',
        'example'    => $country->example ?? '509878214',
        'phone_code' => $country->phone_code,
        'flage'      => asset("/images/flags/$imageName.png"),
      ];
    });

    return $this->successResponse([
      'all_countries' => $allCountries,
    ]);
  }
}
