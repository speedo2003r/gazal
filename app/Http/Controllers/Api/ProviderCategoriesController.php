<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\IGroup;
use App\Repositories\Interfaces\IOffer;
use App\Repositories\Interfaces\ISlider;
use App\Repositories\Interfaces\IUser;
use App\Repositories\Interfaces\IUserAddress;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;

/** import */

class ProviderCategoriesController extends Controller {
  use NotifyTrait;
  use ResponseTrait;
  public $userRepo, $addressRepo, $categoryRepo, $sliderRepo, $offerRepo, $groupRepo, $branch;
  public function __construct(
    IBranch $branch,
    IGroup $groupRepo,
    IOffer $offerRepo,
    ISlider $sliderRepo,
    ICategory $categoryRepo,
    IUser $user,
    IUserAddress $address
  ) {
    $this->branch       = $branch;
    $this->userRepo     = $user;
    $this->addressRepo  = $address;
    $this->categoryRepo = $categoryRepo;
    $this->sliderRepo   = $sliderRepo;
    $this->groupRepo    = $groupRepo;
    $this->offerRepo    = $offerRepo;
  }

  public function providerCategories(LangRequest $request) {
    $user       = auth('branchApi')->user();
    $provider   = $user->user;
    $categories = $provider->categories;
    $categories = $categories->map(function ($category) use ($user) {
      return [
        'id'    => $category['id'],
        'title' => $category['title'],
        'items' => $category->subitems->map(function ($item) use ($user) {
          return [
            'id'    => $item['id'],
            'title' => $item['title'],
            'exist' => $user->items()->where('items.id', $item['id'])->exists(),
          ];
        }),
      ];
    });
    return $this->successResponse($categories);
  }
}
