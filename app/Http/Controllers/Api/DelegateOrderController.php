<?php

namespace App\Http\Controllers\Api;

use App\Entities\Income;
use App\Entities\Order;
use App\Events\UpdateOrderEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Http\Resources\Delegates\DelegateOrderDetailResource;
use App\Http\Resources\Delegates\DelegateOrdersCollection;
use App\Http\Resources\Delegates\delegatorHomePageCollection;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\IItem;
use App\Repositories\Interfaces\IOrder;
use App\Repositories\Interfaces\IOrderProduct;
use App\Repositories\Interfaces\ISellerPay;
use App\Repositories\Interfaces\IUser;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class DelegateOrderController extends Controller {

  use ResponseTrait;
  use NotifyTrait;
  use UploadTrait;
  public $pageRepo, $itemRepo, $sellerPayRepo, $featureRepo, $orderProductRepo, $orderRepo, $sliderRepo, $userRepo, $settingRepo, $socialRepo, $contactRepo, $categoryRepo;
  public function __construct(
    ISellerPay $sellerPay,
    IOrderProduct $orderProduct,
    IItem $item,
    IOrder $order,
    IUser $user,
    ICategory $category
  ) {
    $this->sellerPayRepo    = $sellerPay;
    $this->userRepo         = $user;
    $this->itemRepo         = $item;
    $this->orderRepo        = $order;
    $this->orderProductRepo = $orderProduct;
    $this->categoryRepo     = $category;
  }

  public function delegateAcceptOrder(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $delegate = auth('delegateApi')->user();
    $order    = $this->orderRepo->find($request['order_id']);

    if ($order['delegate_id']) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }
    $order->update([
      'delegate_id' => $delegate['id'],
    ]);
    return $this->successResponse();
  }

  public function delegateOrderReceivedFromProvider(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $delegate = auth('delegateApi')->user();
    $order    = $this->orderRepo->find($request['order_id']);

    if (!$order['delegate_id']
      || $order['delegate_id'] != $delegate['id']
      || Order::STATUS_GIVE_TO_DELEGATE != $order['order_status']
    ) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }

    $order->update([
      'order_status' => Order::STATUS_ON_WAY,
      'pickup_date'  => Carbon::now()->format('Y-m-d h:i'),
    ]);

    // emit to socket
    event(new UpdateOrderEvent($request['order_id']));

    return $this->successResponse();
  }

  public function delegateOrderArrivedToClient(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $delegate = auth('delegateApi')->user();
    $order    = $this->orderRepo->find($request['order_id']);

    if (!$order['delegate_id']
      || $order['delegate_id'] != $delegate['id']
      || Order::STATUS_ON_WAY != $order['order_status']
    ) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }

    $order->update(['order_status' => Order::STATUS_ARRIVED_TO_CLIENT]);

    // emit to socket
    event(new UpdateOrderEvent($request['order_id']));

    return $this->successResponse();
  }

  public function delegateOrderDelivered(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $delegate = auth('delegateApi')->user();
    $order    = $this->orderRepo->find($request['order_id']);

    if (!$order['delegate_id']
      || $order['delegate_id'] != $delegate['id']
      || Order::STATUS_ARRIVED_TO_CLIENT != $order['order_status']
    ) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }

    # count provider commission
    $provider = $order->provider;
    if ($provider) {
      $commission = $provider['commission'] > 0 ? $provider['commission'] : settings('provider_commission') ?? 0;
      $total      = $order['final_total'];
      if($provider['commission_status'] == 1){
          $balance    = ($total - $commission);
      }else{
          $balance    = ($total * $commission) / 100;
      }
      $income     = $total - $balance;

      Income::create([
        'user_id'  => $provider['id'],
        'order_id' => $order['id'],
        'debtor'   => $balance,
        'creditor' => 0,
        'income'   => $income,
      ]);
    }

    $order->update([
      'order_status'   => Order::STATUS_FINISH,
      'delivered_date' => Carbon::now()->format('Y-m-d h:i'),
    ]);

    // emit to socket
    event(new UpdateOrderEvent($request['order_id']));

    return $this->successResponse();
  }

  public function delegateNewOrders(LangRequest $request) {
    $delegate                 = auth('delegateApi')->user();
    $delegateRefusedOrdersIds = \DB::table('refuse_orders')->where('delegate_id', $delegate->id)->pluck('order_id')->toArray();
    $orders                   = $this->orderRepo
      ->whereIn('order_status', [Order::STATUS_ACCEPTED,Order::STATUS_PREPARED])
      ->where('accepted_date', '!=', null)
      ->where('live', 1)
      ->where('delegate_id', null)
      ->whereNotIn('id', $delegateRefusedOrdersIds)
      ->orderBy('created_date', 'desc')
      ->paginate(10);

    return $this->successResponse(delegatorHomePageCollection::make($orders));
  }

  public function delegateOrderRefuse(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $delegate = auth('delegateApi')->user();
    $order    = $this->orderRepo->find($request['order_id']);

    if ($order['delegate_id']) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }

    $isRefusedBefore = \DB::table('refuse_orders')->where('delegate_id', $delegate->id)->where('order_id', $request->order_id)->exists();

    if ($isRefusedBefore) {
      return $this->ApiResponse('fail', trans('api.refusedBefore'));
    }

    $order->refuseOrders()->attach($delegate['id']);

    return $this->successResponse();
  }

  public function delegateCurrentOrders(LangRequest $request) {
    $delegate = auth('delegateApi')->user();
    $orders   = $this->orderRepo
      ->whereIn('order_status', [
        Order::STATUS_ACCEPTED,
        Order::STATUS_PREPARED,
        Order::STATUS_GIVE_TO_DELEGATE,
        Order::STATUS_ON_WAY,
        Order::STATUS_ARRIVED_TO_CLIENT,
      ])
      ->where('delegate_id', $delegate['id'])
      ->orderBy('created_date', 'desc')
      ->paginate(10);

    return $this->successResponse(DelegateOrdersCollection::make($orders));
  }

  public function delegateOrderDetails(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $delegate = auth('delegateApi')->user();
    $order    = $this->orderRepo->find($request['order_id']);

    if ($order['delegate_id'] && $order['delegate_id'] != $delegate['id']) {
      return $this->ApiResponse('fail', trans('api.notAuthorized'));
    }

    return $this->successResponse([
      'details' => new DelegateOrderDetailResource($order),
    ]);
  }

  public function delegateCompleteOrders(LangRequest $request) {
    $delegate = auth('delegateApi')->user();
    $orders   = $this->orderRepo
      ->where(['order_status' => Order::STATUS_FINISH])
      ->where('delegate_id', $delegate['id'])
      ->orderBy('created_date', 'desc')
      ->paginate(10);

    return $this->successResponse(DelegateOrdersCollection::make($orders));
  }

  public function delegateOrderClientAddress(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $delegate = auth('delegateApi')->user();
    $order    = $this->orderRepo->find($request['order_id']);

    if (!$order['delegate_id'] || $order['delegate_id'] != $delegate['id']) {
      return $this->ApiResponse('fail', trans('api.notAuthorized'));
    }

    $data = [
      'id'      => $order->user['id'],
      'avatar'  => $order->user['avatar'],
      'name'    => $order->user['name'],
      'phone'   => $order->user['phone'],
      'lat'     => $order->lat != null ? $order->lat : $order->address['lat'],
      'lng'     => $order->lng != null ? $order->lng : $order->address['lat'],
      'address' => $order->address->mapDesc(),
    ];

    return $this->successResponse($data);
  }

  public function delegateOrderStatus(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $delegate = auth('delegateApi')->user();
    $order    = $this->orderRepo->find($request['order_id']);

    if (!$order['delegate_id'] || $order['delegate_id'] != $delegate['id']) {
      return $this->ApiResponse('fail', trans('api.notAuthorized'));
    }

    $data = [
      'id'          => $order->id,
      'created_at'  => \Carbon\Carbon::parse($order->created_date)->diffforhumans(),
      'order_num'   => $order->order_num,
      'branch_name' => $order->branch ? $order->branch['title'] : '',
      'user_id'     => $order->user['id'] ?? 0,
      'avatar'      => $order->user['avatar'] ?? '',
      'name'        => $order->user['name'] ?? '',
      'phone'       => $order->user['phone'] ?? '',
      'lat'         => null != $order->lat ? $order->lat : $order->address['lat'],
      'lng'         => null != $order->lng ? $order->lng : $order->address['lat'],
      'address'     => $order->address ? $order->address->mapDesc() : '',
    ];

    $list = [
      'arrived'         => app()->getLocale() == 'ar' ? 'تم الوصول' : 'was arrived',
      'received'        => app()->getLocale() == 'ar' ? 'تم الاستلام' : 'was received',
      'arrivedToClient' => app()->getLocale() == 'ar' ? 'تم التوصيل' : 'arrived with order',
      'delivered'       => app()->getLocale() == 'ar' ? 'تم التسليم' : 'was delivered',
    ];

    return $this->successResponse([
      'order'       => $data,
      'orderStatus' => $order['delegate_status'], //todo: remove this
      'order_status' => $order['order_status'],
      'allStatus'   => $list,
    ]);
  }

  public function delegateCancelUserOrders(LangRequest $request) {
    $delegate = auth('delegateApi')->user();
    $orders   = $this->orderRepo
      ->where(['order_status' => Order::STATUS_CANCELED])
      ->where('delegate_id', $delegate['id'])
      ->orderBy('created_date', 'desc')
      ->paginate(10);
    return $this->successResponse(DelegateOrdersCollection::make($orders));
  }

  public function delegateRefusedOrders(LangRequest $request) {
    $delegate = auth('delegateApi')->user();
    $orders   = $delegate
      ->refuseOrders() //todo: use order_status here
      ->orderBy('created_date', 'desc')
      ->paginate(10);

    return $this->successResponse(DelegateOrdersCollection::make($orders));
  }

}
