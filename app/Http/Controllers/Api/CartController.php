<?php

namespace App\Http\Controllers\Api;
use App\Entities\Coupon;
use App\Entities\Feature;
use App\Entities\ItemTypeDetail;
use App\Entities\Order;
use App\Entities\OrderProduct;
use App\Entities\UserAddress;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Http\Resources\Orders\CartOrderResource;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\IContactUs;
use App\Repositories\Interfaces\IFeature;
use App\Repositories\Interfaces\IItem;
use App\Repositories\Interfaces\IOrder;
use App\Repositories\Interfaces\IOrderProduct;
use App\Repositories\Interfaces\IOrderProductFeature;
use App\Repositories\Interfaces\IOrderProductType;
use App\Repositories\Interfaces\IPage;
use App\Repositories\Interfaces\ISetting;
use App\Repositories\Interfaces\ISlider;
use App\Repositories\Interfaces\ISocial;
use App\Repositories\Interfaces\IUser;
use App\Repositories\Interfaces\IZone;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CartController extends Controller {
  use ResponseTrait;
  public $pageRepo,$zone, $branch, $itemRepo, $orderOptionRepo, $featureRepo, $orderFeatureRepo, $orderProductRepo, $orderRepo, $sliderRepo, $userRepo, $settingRepo, $socialRepo, $contactRepo, $categoryRepo;
  public function __construct(
    IBranch $branch,
    IOrderProductType $orderOption,
    ISlider $slider,
    IOrder $orderRepo,
    IFeature $feature,
    IOrderProductFeature $orderFeature,
    IOrderProduct $orderProduct,
    IItem $item,
    IOrder $order,
    IPage $page,
    IUser $user,
    ISetting $setting,
    ISocial $social,
    IContactUs $contact,
    ICategory $category,
    IZone $zone
  ) {
    $this->zone           = $zone;
    $this->branch           = $branch;
    $this->userRepo         = $user;
    $this->featureRepo      = $feature;
    $this->orderFeatureRepo = $orderFeature;
    $this->pageRepo         = $page;
    $this->itemRepo         = $item;
    $this->orderOptionRepo  = $orderOption;
    $this->orderRepo        = $orderRepo;
    $this->orderProductRepo = $orderProduct;
    $this->sliderRepo       = $slider;
    $this->settingRepo      = $setting;
    $this->socialRepo       = $social;
    $this->contactRepo      = $contact;
    $this->categoryRepo     = $category;
  }

  public function AddToCart(Request $request) {
    $validator = Validator::make($request->all(), [
      'uuid'        => 'required',
      'seller_id'   => 'required|exists:users,id,deleted_at,NULL',
      'branch_id'   => 'required|exists:branches,id,deleted_at,NULL',
      'item_id'     => 'required|exists:items,id,deleted_at,NULL',
      'device_type' => 'required',
      'count'       => 'required',
      'options'     => 'required',
      'notes'       => 'nullable',
      'features'    => 'sometimes',
    ]);

    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $user   = auth()->check() ? auth()->user() : null;
    $seller = $this->userRepo->find($request['seller_id']);

    $branch = $this->branch->find($request['branch_id']);
    if (!$branch || $branch->provider_id != $seller->id) {
      return $this->ApiResponse('fail', trans('api.cannotUpdate'));
    }

    # clear cart from other branches - cascaded with options and features
    $this->orderRepo
      ->where('live', 0)
      ->where('branch_id', '!=', $branch['id'])
      ->where('user_id', $user->id)
      ->forceDelete();

    $address = null;
    if ($user) {
      $order = $this->orderRepo
        ->where('uuid', $request['uuid'])
        ->where('provider_id', $request['seller_id'])
        ->where('branch_id', $branch['id'])
        ->where('user_id', $user->id)
        ->where('live', 0)
        ->first();

      $address = $user->addresses()->where('is_main', 1)->first();
    } else {
      $order = $this->orderRepo
        ->where('uuid', $request['uuid'])
        ->where('provider_id', $request['seller_id'])
        ->where('branch_id', $branch['id'])
        ->where('user_id', null)
        ->where('live', 0)->first();

      $address = UserAddress::where('user_id', null)->where('is_main', 1)->first();
    }

    try {

      if (!$order) {
        $order = $this->storeOrder(array_filter($request->all()), $seller, $branch, $user);
      } else {
        $this->updateOrder($order, array_filter($request->all()));
      }

      $count  = OrderProduct::where('order_products.order_id', $order['id'])->sum('qty');
      $sumAll = 0;
      if ($order) {
        foreach ($order->orderProducts as $orderProduct) {
          $sumAll += $orderProduct->_price();
        }
      }

      $tax       = settings('tax') ?? 0;
      $orderTax  = $sumAll * $tax / 100;
      $shipPrice = 0;

      if ($address) {
        $distance  = GetPathAndDirections($address['lat'], $address['lng'], $branch['lat'], $branch['lng']);
        $shipPrice = ($distance > settings('minkm_shippingPrice')) ? ($distance * settings('shippingValue')) + settings('shippingPrice') : (double) settings('shippingPrice');
      }

      $order->update([
        'final_total'    => $sumAll,
        'vat_amount'     => round($orderTax, 2),
        'vat_per'        => $tax,
        'shipping_price' => $shipPrice,
        'address_id'     => null != $address ? $address['id'] : null,
        'map_desc'       => null != $address ? $address['street'] . ',' . $address['building'] . ',' . $address['floor'] . ',' . $address['flat'] . ',' . $address['unique_sign'] : null,
        'lat'            => null != $address ? $address['lat'] : null,
        'lng'            => null != $address ? $address['lng'] : null,
      ]);

      return $this->successResponse([
        'itemPrice' => $order->price(),
        'count'     => $count,
      ]);

    } catch (\Exception$e) {
      Log::debug($e);
      return $this->ApiResponse('fail', $e->getMessage());
    }

  }

  public function Cart(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'uuid' => ['required'],
    ]);

    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $user    = auth()->check() ? auth()->user() : null;
    $user_id = $user ? $user['id'] : null;

    if ($user) {
      $order = $this->orderRepo->where('uuid', $request['uuid'])->where('user_id', $user_id)->where('live', 0)->first();
    } elseif (!$user) {
      $order = $this->orderRepo->where('uuid', $request['uuid'])->where('user_id', null)->where('live', 0)->first();
    }
    if ($order) {
      return $this->successResponse(new CartOrderResource($order));
    }
    return $this->ApiResponse('fail', trans('api.emptyCart'));
  }

  public function decreaseCount(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_product_id' => 'required|exists:order_products,id,deleted_at,NULL',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }
    $orderProduct = $this->orderProductRepo->find($request['order_product_id']);
    $order        = $this->orderRepo->find($orderProduct->order['id']);

    if (1 == $orderProduct['qty']) {
      $this->orderProductRepo->delete($orderProduct['id']);
      if (count($order->orderProducts) == 0) {
        $order->delete();
      }
    } else {
      $this->orderProductRepo->update([
        'qty' => $orderProduct['qty'] - 1,
      ], $orderProduct['id']);
      $this->orderRepo->update([
        'total_items' => $order['total_items'] - 1,
      ], $order['id']);
    }

    return $this->successResponse($orderProduct['qty'] - 1);
  }

  public function increaseCount(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_product_id' => 'required|exists:order_products,id,deleted_at,NULL',
    ]);

    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }
    $orderProduct = $this->orderProductRepo->find($request['order_product_id']);
    $order        = $this->orderRepo->find($orderProduct->order['id']);
    $this->orderProductRepo->update([
      'qty' => $orderProduct['qty'] + 1,
    ], $orderProduct['id']);
    $this->orderRepo->update([
      'total_items' => $order['total_items'] + 1,
    ], $order['id']);
    return $this->successResponse($orderProduct['qty'] + 1);
  }

  public function writeNote(Request $request) {
    $validator = Validator::make($request->all(), [
      'uuid'  => 'required',
      'notes' => 'required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }
    $user    = auth()->check() ? auth()->user() : null;
    $user_id = $user ? $user['id'] : null;

    if ($user) {
      $order = $this->orderRepo->where('uuid', $request['uuid'])->where('user_id', $user_id)->where('live', 0)->first();
    } else {
      $order = $this->orderRepo->where('uuid', $request['uuid'])->where('user_id', null)->where('live', 0)->first();
    }
    $order->notes = $request['notes'];
    $order->save();
    return $this->ApiResponse('success', trans('api.send'));
  }

  public function addCoupon(Request $request) {
    $user = auth()->user();
    if ($user['friend_share_code'] != null && $request['coupon'] == $user['friend_share_code']) {
      $validator = Validator::make($request->all(), [
        'order_id' => 'required|exists:orders,id,deleted_at,NULL',
      ]);
      if ($validator->fails()) {
        return $this->ApiResponse('fail', $validator->errors()->first());
      }
      if($user['is_friend_share_code_used'] == 1){
          $msg = app()->getLocale() == 'ar' ? 'لقد تم بالفعل استخدام كود المشاركه من قبل' : 'The share code has already been used';
          return $this->ApiResponse('fail', $msg);
      }
      $order       = $this->orderRepo->find($request['order_id']);
      $couponValue = settings('share_code_commission') ?? 0;
      $total       = (string) round($order->_price(), 2);
      if ((integer) $couponValue >= (integer) $total) {
        return $this->ApiResponse('fail', app()->getLocale() == 'ar' ? 'السعر أقل من قيمة الخصم لايمكن اتمام الخصم' : 'The price is less than the discount value, the discount cannot be completed');
      }
      $this->orderRepo->update([
        'coupon_num'    => $user['friend_share_code'],
        'coupon_amount' => $couponValue,
      ], $order['id']);
      $user['is_friend_share_code_used'] = 1;
      $user->save();
      $order = $this->orderRepo->find($request['order_id']);
      $total = (string) round($order->_price(), 2);

      return $this->successResponse(['total' => $total, 'value' => $couponValue]);
    } else {
      $validator = Validator::make($request->all(), [
        'coupon'   => 'required|exists:coupons,code,deleted_at,NULL',
        'order_id' => 'required|exists:orders,id,deleted_at,NULL',
        'lat' => 'required',
        'lng' => 'required',
      ]);

      if ($validator->fails()) {
        return $this->ApiResponse('fail', $validator->errors()->first());
      }

      $order       = $this->orderRepo->find($request['order_id']);

      $coupon      = Coupon::where('code', $request['coupon'])->first();
      if ($coupon['count'] == 0 && $coupon['limit'] == 'limited' && $coupon['end_date'] >= Carbon::now()->format('Y-m-d') && $coupon['start_date'] <= Carbon::now()->format('Y-m-d')) {
          $msg = app()->getLocale() == 'ar' ? 'هذا الكوبون غير صالح للاستخدام' : 'This coupon is not valid for use';
          return $this->ApiResponse('fail',$msg);
      }
      if($coupon['end_date'] < Carbon::now()->format('Y-m-d') || $coupon['start_date'] > Carbon::now()->format('Y-m-d')){
          $msg = app()->getLocale() == 'ar' ? 'هذا الكوبون غير صالح للاستخدام' : 'This coupon is not valid for use';
          return $this->ApiResponse('fail',$msg);
      }
      $zone = $this->zone->getZone();
      $total = (string) round($order->_price(), 2);
      if(($zone == null && $coupon['city_id'] != null) || ($zone != null && $zone['city_id'] != $coupon['city_id'])){
        $msg = app()->getLocale() == 'ar' ? 'هذا الكوبون لا يغطي النطاق الذي تتواجد به' : 'This coupon does not cover the range you are in';
        return $this->ApiResponse('fail',$msg);
      }
      if($order['final_total'] < $coupon['min_order_amount']){
        $msg = app()->getLocale() == 'ar' ? 'قيمة الطلب أقل من قيمة الحد الأدني للتحفيض' : 'The value of the order is less than the value of the minimum discount';
        return $this->ApiResponse('fail',$msg);
      }
      if ((integer) $coupon['value'] >= (integer) $total) {
        return $this->ApiResponse('fail', app()->getLocale() == 'ar' ? 'السعر أقل من قيمة الخصم لايمكن اتمام الخصم' : 'The price is less than the discount value, the discount cannot be completed');
      }
      $couponValue = $coupon->couponValue($order['final_total'], $order['provider_id']);
      if($couponValue > $coupon['max_percent_amount']){
        $msg = app()->getLocale() == 'ar' ? 'قيمة الخصم أكبر من الحد الأقصي للخصم' : 'The value of the discount is greater than the maximum discount';
        return $this->ApiResponse('fail',$msg);
      }
      $this->orderRepo->update([
        'coupon_id'     => $coupon['id'],
        'coupon_num'    => $coupon['code'],
        'coupon_type'   => $coupon['type'],
        'coupon_amount' => $couponValue,
      ], $order['id']);

      $coupon->count = $coupon->count - 1;
      $coupon->save();

      $order = $this->orderRepo->find($request['order_id']);
      $total = (string) round($order->_price(), 2);

      return $this->successResponse(['total' => $total, 'value' => $couponValue]);
    }
  }

  public function changeOrderType(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
      'type'     => 'required|in:deliver,branch,postponed',
    ]);

    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $order = $this->orderRepo->find($request['order_id']);

    $branch  = $this->branch->find($order['branch_id']);
    $address = UserAddress::find($order['address_id']);
    if (0 == $order['shipping_price'] && $address) {
      $distance  = GetPathAndDirections($address['lat'], $address['lng'], $branch['lat'], $branch['lng']);
      $shipPrice = ($distance * settings('shippingValue')) + settings('shippingPrice');
    } else {
      $shipPrice = $order['shipping_price'];
    }
    $this->orderRepo->update([
      'order_type'     => $request['type'],
      'shipping_price' => 'branch' != $request['type'] ? $shipPrice : 0,
    ], $request['order_id']);

    $order->refresh();

    return $this->successResponse([
      'timeExist'      => 'postponed' == $request['type'] ? true : false,
      'itemPrice'      => $order->price(),
      'totalTaxShip'   => $order->_price(),
      'shipping_price' => $order['shipping_price'],
    ]);
  }

  public function changeOrderAddress(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id'   => 'required|exists:orders,id,deleted_at,NULL',
      'address_id' => 'required|exists:user_addresses,id,deleted_at,NULL',
    ]);

    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $this->orderRepo->update([
      'address_id' => $request['address_id'],
    ], $request['order_id']);

    $address = UserAddress::where('id', $request['address_id'])->first();

    return $this->successResponse([
      'address' => $address->mapDesc(),
      'lat'     => $address['lat'],
      'lng'     => $address['lng'],
    ]);
  }

  public function postponedOrder(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => ['required', Rule::exists('orders', 'id')],
      'time'     => 'required',
      'date'     => 'required|date',
    ]);

    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $order = $this->orderRepo->find($request['order_id']);
    $time  = date('H:i:s', strtotime($request['time']));
    $date  = date('Y-m-d', strtotime($request['date']));

    $this->orderRepo->update([
      'time' => $time,
      'date' => $date,
    ], $order['id']);

    return $this->successResponse([
      'time'           => $time,
      'date'           => $date,
      'itemPrice'      => $order->price(),
      'totalTaxShip'   => $order->_price(),
      'shipping_price' => $order['shipping_price'],
    ]);
  }

  public function reorder(LangRequest $request) {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
      'uuid'     => 'required',
    ]);

    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $oldOrder = $this->orderRepo->find($request['order_id']);

    $user    = auth()->user();
    $user_id = $user['id'];
    $seller  = $oldOrder->provider;
    $branch  = $oldOrder->branch;
    $address = null;

    if (null != $user) {
      $orders = $this->orderRepo->where('user_id', $user_id)->where('live', 0)->get();
      foreach ($orders as $data) {
        $this->orderRepo->delete($data['id']);
      }
      $address = $user->addresses()->where('is_main', 1)->first();
    }

    $order = $this->orderRepo->findWhere(['uuid' => $request['uuid'], 'user_id' => $user_id, 'live' => 0])->first();
    if ($order) {
      $order->delete();
    }

    $order = $this->orderRepo->create([
      'status'      => 'new',
      'live'        => 0,
      'uuid'        => $request['uuid'],
      'total_items' => $oldOrder['total_items'],
      'provider_id' => $oldOrder['provider_id'],
      'branch_id'   => $oldOrder['branch_id'],
      'user_id'     => $user_id,
    ]);

    $orderProducts = $oldOrder->orderProducts;
    if (count($orderProducts) > 0) {
      foreach ($orderProducts as $product) {

        $orderProduct = $this->orderProductRepo->create([
          'order_id' => $order['id'],
          'item_id'  => $product['item_id'],
          'qty'      => $product['qty'],
          'notes'    => $product['notes'],
        ]);

        if (isset($product->features)) {
          foreach ($product->features as $feature) {
            $this->orderFeatureRepo->create([
              'order_product_id' => $orderProduct['id'],
              'feature_id'       => $feature['feature_id'],
              'price'            => $feature['price'],
            ]);
          }
        }

        // add to product option details
        if (isset($product->options)) {
          foreach ($product->options as $option) {
            $this->orderOptionRepo->create([
              'order_product_id'     => $orderProduct['id'],
              'item_types_detail_id' => $option['item_types_detail_id'],
              'price'                => $option->ItemTypeDetail->price(),
            ]);
          }
        }

        $this->orderProductRepo->update([
          'price' => $orderProduct->_single_price(),
        ], $orderProduct['id']);
      }
    }

    $sumAll = 0;
    foreach ($order->orderProducts as $orderProduct) {
      $sumAll += $orderProduct->_price();
    }

    $tax       = settings('tax') ?? 0;
    $orderTax  = $sumAll * $tax / 100;
    $shipPrice = 0;
    if (null != $address) {
      $distance  = GetPathAndDirections($address['lat'], $address['lng'], $branch['lat'], $branch['lng']);
      $shipPrice = ($distance * settings('shippingValue')) + settings('shippingPrice');
    }

    $this->orderRepo->update([
      'final_total'    => $sumAll,
      'vat_amount'     => round($orderTax, 2),
      'vat_per'        => $tax,
      'shipping_price' => $shipPrice,
      'address_id'     => null != $address ? $address['id'] : null,
      'map_desc'       => null != $address ? $address['street'] . ',' . $address['building'] . ',' . $address['floor'] . ',' . $address['flat'] . ',' . $address['unique_sign'] : null,
      'lat'            => null != $address ? $address['lat'] : null,
      'lng'            => null != $address ? $address['lng'] : null,
    ], $order['id']);

    return $this->successResponse();
  }

  private function storeOrder($request, $seller, $branch, $user) {

    $order = $this->orderRepo->create([
      'uuid'        => $request['uuid'] ?? null,
      'live'        => 0,
      'total_items' => $request['count'],
      'device_type' => $request['device_type'],
      'provider_id' => $seller['id'],
      'branch_id'   => $branch['id'],
      'user_id'     => $user ? $user->id : null,
    ]);

    $this->orderProductStore($order, $request);

    return $order;
  }

  private function updateOrder($order, $request) {
    $item = $this->itemRepo->find($request['item_id']);

    $order->update(['total_items' => $order['total_items'] + $request['count']]);

    // add to order product
    $orderProduct = OrderProduct::where('order_id', $order['id'])
      ->where('item_id', $item['id'])
      ->first();

    if (!$orderProduct) {
      $this->orderProductStore($order, $request);

    } else {
      $optionsCount = isset($request['options']) ? count(json_decode($request['options'])) : 0;
      $featureCount = isset($request['features']) ? count(json_decode($request['features'])) : 0;

      if ($optionsCount > 0 && $featureCount > 0) {
        $count = OrderProduct::where('order_id', $order['id'])
          ->where('item_id', $item['id'])
          ->whereHas('options', function ($option) use ($request) {
            $option->whereIn('item_types_detail_id', json_decode($request['options']));
          })
          ->whereHas('features', function ($feature) use ($request) {
            $feature->whereIn('feature_id', json_decode($request['features']));
          })
          ->count();

        if ($count > 1) {
          $orderProducts = OrderProduct::where('order_id', $order['id'])
            ->where('item_id', $item['id'])
            ->whereHas('options', function ($option) use ($request) {
              $option->whereIn('item_types_detail_id', json_decode($request['options']));
            })
            ->whereHas('features', function ($feature) use ($request) {
              $feature->whereIn('feature_id', json_decode($request['features']));
            })
            ->get();

          $itemArrs = [];
          foreach ($orderProducts as $orderProduct) {
            $arr1 = $orderProduct->features()->pluck('order_product_features.feature_id')->toArray();
            $arr2 = json_decode($request['features']);
            // dump(array_equal($arr1,$arr2),$arr1,$arr2);
            if (array_equal($arr1, $arr2)) {
              $itemArrs[] = $orderProduct['id'];
              $this->orderProductRepo->update([
                'qty' => $orderProduct['qty'] + $request['count'],
              ], $orderProduct['id']);
              break;
            }
          }

          if (count($itemArrs) == 0) {
            $this->orderProductStore($order, $request);
          }

        } else {

          $orderProduct = OrderProduct::where('order_id', $order['id'])
            ->where('item_id', $item['id'])
            ->whereHas('options', function ($option) use ($request) {
              $option->whereIn('item_types_detail_id', json_decode($request['options']));
            })
            ->whereHas('features', function ($feature) use ($request) {
              $feature->whereIn('feature_id', json_decode($request['features']));
            })
            ->first();

          $arr1 = $orderProduct ? (count($orderProduct->features) > 0 ? $orderProduct->features()->pluck('order_product_features.feature_id')->toArray() : []) : [];
          $arr2 = json_decode($request['features']);
          if ($orderProduct && array_equal($arr1, $arr2) == true) {
            $orderProduct->update(['qty' => $orderProduct['qty'] + $request['count']]);

          } else {
            $this->orderProductStore($order, $request);
          }
        }

      } elseif ($optionsCount > 0 && 0 == $featureCount) {

        $orderProduct = OrderProduct::where('order_id', $order['id'])
          ->where('item_id', $item['id'])
          ->whereHas('options', function ($option) use ($request) {
            $option->whereIn('item_types_detail_id', json_decode($request['options']));
          })
          ->whereDoesntHave('features')
          ->first();

        if ($orderProduct) {
          $orderProduct->update(['qty' => $orderProduct['qty'] + $request['count']]);
        } else {
          $this->orderProductStore($order, $request);
        }

      } else {
        $this->orderProductStore($order, $request);
      }
    }
  }

  public function orderProductStore($data, $request) {
    $item         = $this->itemRepo->find($request['item_id']);
    $orderProduct = $this->orderProductRepo->create([
      'order_id' => $data['id'],
      'item_id'  => $item['id'],
      'qty'      => $request['count'],
      'notes'    => isset($request['notes']) ? $request['notes'] : null,
    ]);

    if (isset($request['features'])) {
      $features = Feature::whereIn('id', json_decode($request['features']))->get();
      foreach ($features as $feature) {
        $this->orderFeatureRepo->create([
          'order_product_id' => $orderProduct['id'],
          'feature_id'       => $feature['id'],
          'price'            => $feature['price'],
        ]);
      }
    }

    // add to product option details
    if (isset($request['options'])) {
      $options = ItemTypeDetail::whereIn('id', json_decode($request['options']))->get();
      foreach ($options as $option) {
        $this->orderOptionRepo->create([
          'order_product_id'     => $orderProduct['id'],
          'item_types_detail_id' => $option['id'],
          'price'                => $option->price(),
        ]);
      }
    }

    $orderProduct->update(['price' => $orderProduct->_single_price()]);
  }

}
