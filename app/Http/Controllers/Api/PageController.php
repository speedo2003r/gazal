<?php

namespace App\Http\Controllers\Api;

use App;
use App\Entities\OrderProduct;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Http\Resources\Settings\pageResource;
use App\Repositories\Interfaces\IPage;
use App\Repositories\Interfaces\IUser;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\SmsTrait;
use App\Traits\UploadTrait;
use Auth;

/** import */

class PageController extends Controller
{
    use ResponseTrait;
    public $userRepo,$pageRepo;
    public function __construct(IPage $pageRepo,IUser $user)
    {
        $this->userRepo     = $user;
        $this->pageRepo     = $pageRepo;
    }

    public function about(LangRequest $request)
    {
        $page = $this->pageRepo->getAbout();
        return $this->successResponse(pageResource::make($page));
    }
}
