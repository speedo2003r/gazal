<?php

namespace App\Http\Controllers\Api;

use App\Entities\Order;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LangRequest;
use App\Http\Resources\Delegates\delegatorHomePageCollection;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\IDelegatePay;
use App\Repositories\Interfaces\IItem;
use App\Repositories\Interfaces\IOrder;
use App\Repositories\Interfaces\IOrderProduct;
use App\Repositories\Interfaces\ISellerPay;
use App\Repositories\Interfaces\IUser;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DelegateController extends Controller {

  use ResponseTrait;
  use NotifyTrait;
  use UploadTrait;
  public $pageRepo, $itemRepo, $delegatePay, $sellerPayRepo, $featureRepo, $orderProductRepo, $orderRepo, $sliderRepo, $userRepo, $settingRepo, $socialRepo, $contactRepo, $categoryRepo;
  public function __construct(
    ISellerPay $sellerPay,
    IDelegatePay $delegatePay,
    IOrderProduct $orderProduct,
    IItem $item,
    IOrder $order,
    IUser $user,
    ICategory $category
  ) {
    $this->sellerPayRepo    = $sellerPay;
    $this->delegatePay      = $delegatePay;
    $this->userRepo         = $user;
    $this->itemRepo         = $item;
    $this->orderRepo        = $order;
    $this->orderProductRepo = $orderProduct;
    $this->categoryRepo     = $category;
  }

  // todo: remove this moved to delegate new orders
  public function delegateHomePage(LangRequest $request) {
    $delegate = auth('delegateApi')->user();
    $orders   = $this->orderRepo->where('order_status',Order::STATUS_PREPARED)
      ->where('accepted_date', '!=', null)
      ->where('live', 1)
      ->where('delegate_id', null)
      ->whereDoesntHave('refuseOrders')
      ->orderBy('created_date', 'desc')
      ->paginate(10);
    return $this->successResponse(delegatorHomePageCollection::make($orders));
  }

  public function prayTime(Request $request) {
    $validator = Validator::make($request->all(), [
      'lat' => 'required',
      'lng' => 'required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    //31.041455
    //31.4178592
    $lat        = $request->lat;
    $lng        = $request->lng;
    $praytimes  = [];
    $msg        = '';
    $url        = "http://api.aladhan.com/v1/calendar?latitude=" . $lat . "&longitude=" . $lng . "&method=4&month=" . date("m") . "&year=" . date("Y");
    $jsonresult = @file_get_contents($url, true);
    if (FALSE === $jsonresult) {
      return '';
    } else {
      if ($results = json_decode($jsonresult)) {
        if (isset($results->data)) {
          $currentday           = intval(date('d')) - 1;
          $praytimes['Fajr']    = $results->data[$currentday]->timings->Fajr;
          $praytimes['Dhuhr']   = $results->data[$currentday]->timings->Dhuhr;
          $praytimes['Asr']     = $results->data[$currentday]->timings->Asr;
          $praytimes['Maghrib'] = $results->data[$currentday]->timings->Maghrib;
          $praytimes['Isha']    = $results->data[$currentday]->timings->Isha;
          foreach ($praytimes as $key => $value) {
            $praytime  = substr($value, 0, 5);
            $to_time   = strtotime(date("Y-m-d") . " " . $praytime);
            $from_time = strtotime(date('Y-m-d H:i'));
            $minutes   = intval(round(($to_time - $from_time) / 60, 2));
          }
        }
      }
    }
    return $this->successResponse($praytimes);
  }

  public function weather(Request $request) {
    $validator = Validator::make($request->all(), [
      'lat' => 'required',
      'lng' => 'required',
    ]);
    if ($validator->fails()) {
      return $this->ApiResponse('fail', $validator->errors()->first());
    }

    $lat       = 31.041455;
    $lon       = 31.4178592;
    $city_name = 'cairo';

    $appid = 'd1b8c096d60c49012c0e74ba4d464fc5';

    $api_url      = 'https://api.openweathermap.org/data/2.5/weather?lat=' . $lat . '&lon=' . $lon . '&appid=' . $appid;
    $weather_data = \json_decode(file_get_contents($api_url));

    $data['temperature'] = \round($weather_data->main->temp - 273.15);
    $data['description'] = $weather_data->weather[0]->description;
    $data['wind_speed']  = $weather_data->wind->speed;
    return $this->successResponse($data);

  }

}
