<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class WeatherController extends Controller
{

    use ResponseTrait;
    use NotifyTrait;
    use UploadTrait;

    public function __construct()
    {
        //
    }
    
    public function weather(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'lat' => 'required',
            'lng' => 'required',
        ]);
        if($validator->fails()){
            return $this->ApiResponse('fail',$validator->errors()->first());
        }

        $lat = 31.041455;
        $lon =  31.4178592;
        $city_name =  'cairo';

        $appid  = 'd1b8c096d60c49012c0e74ba4d464fc5';

        $api_url =  'https://api.openweathermap.org/data/2.5/weather?lat=' . $lat . '&lon=' . $lon . '&appid=' . $appid;
        $weather_data = \json_decode(file_get_contents($api_url));

        $data['temperature'] = \round($weather_data->main->temp - 273.15);
        $data['description'] = $weather_data->weather[0]->description;
        $data['wind_speed'] = $weather_data->wind->speed;
        return $this->successResponse($data);

    }

}
