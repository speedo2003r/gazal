<?php

namespace App\Http\Controllers\Provider;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\IProvider;
use App\Repositories\Interfaces\IUser;
use App\Rules\watchOldPassword;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    use UploadTrait;
    protected $user,$provider;
    public function __construct(IProvider $provider, IUser $user)
    {
        $this->provider = $provider;
        $this->user = $user;
    }

    public function index()
    {
        $user = auth()->user();
        return view('provider.setting.index',compact('user'));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'phone' => 'required|numeric|digits_between:9,13|unique:users,phone,'.auth()->id().',id,deleted_at,NULL',
            'email' => 'required|email|unique:users,email,'.auth()->id().',id,deleted_at,NULL',
            'address' => 'required|required',
            'lat' => 'required',
            'lng' => 'required',
        ]);
        $data = array_filter($request->except('image','logo','banner'));
        if($request->has('image')){
            $data['avatar'] = $this->uploadFile($request['image'],'users');
        }
        $user = auth()->user();
        $this->user->update($data,$user['id']);
        $providerData = $request->except($user->getFillable());
        $providerData['user_id'] = $user['id'];
        $provider = $user->provider;

        if($request->has('logo')){
            $this->deleteFile($user['logo'],'providers');
            $logo = $this->uploadFile($request['logo'],'providers');
            $providerData['logo'] = $logo;
        }
        if($request->has('banner')){
            $this->deleteFile($user['banner'],'providers');
            $logo = $this->uploadFile($request['banner'],'providers');
            $providerData['banner'] = $logo;
        }
        $this->provider->update($providerData,$provider['id']);
        return back()->with('success','تم التحديث بنجاح');
    }

    public function changePassword()
    {
        return view('provider.setting.changePassword');
    }
    public function postChangePassword(Request $request)
    {
        $this->validate($request,[
            'old_password' => ['required','min:6','max:255',new watchOldPassword()],
            'password' => 'required|min:6|max:255',
        ]);
        if($request['old_password'] == $request['password']){
            $msg = app()->getLocale() == 'ar' ? 'كلمة المرور الجديده هي نفس كلمة المرور القديمه' : 'new password is equal old password';
            return back()->with('danger', $msg);
        }
        $user = auth()->user();
        # update Password
        $this->user->update([
            'password' => $request['password']
        ],$user['id']);
        return back()->with('success', trans('api.passwordUpdated'));
    }
}
