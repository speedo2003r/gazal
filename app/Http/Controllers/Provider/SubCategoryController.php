<?php

namespace App\Http\Controllers\Provider;

use App\Entities\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items =  currentUser()->subcategories()->get();
        return view('provider.subcategories.index', compact('items'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['subcategories' => 'required|array']);
        currentUser()->subcategories()->attach($request->subcategories, ['is_main' => 0]);
        return redirect()->back()->with('success', __('Added Successfully'));
    }

    public function show($id)
    {
        $subcategories = Category::whereNotNull('parent_id')
                                 ->whereParentId($id)
                                 ->whereNotIn('id', acurrentUser()->subcategories()->pluck('categories.id'))
                                 ->get();
        return response()->json($subcategories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        currentUser()->subcategories()->updateExistingPivot($id, ['is_active' => $request->is_active]);
        return response()->json(__('Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        currentUser()->subcategories()->detach($id);
        return redirect()->back()->with('success', __('Deleted Successfully'));
    }
}
