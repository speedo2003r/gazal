<?php

namespace App\Http\Controllers\Provider;

use App\DataTables\ItemSellerDatatable;
use App\Entities\Item;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Repositories\Interfaces\ICarType;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IFeature;
use App\Repositories\Interfaces\IGroup;
use App\Repositories\Interfaces\IItem;
use App\Repositories\Interfaces\IUser;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class OfferController extends Controller
{

    use ResponseTrait;
    use UploadTrait;
    protected $userRepo,$groupRepo,$feature, $roleRepo,$country,$itemRepo,$category,$carType,$group;

    public function __construct(IGroup $groupRepo, IFeature $feature, IGroup $group, ICarType $carType, ICategory $category, IItem $item, IUser $user, ICountry $country,Role $role)
    {
        $this->userRepo = $user;
        $this->roleRepo = $role;
        $this->country = $country;
        $this->itemRepo = $item;
        $this->category = $category;
        $this->carType = $carType;
        $this->group = $group;
        $this->feature = $feature;
        $this->groupRepo = $groupRepo;
    }

    public function index(ItemSellerDatatable $itemSellerDatatable)
    {
        return $itemSellerDatatable->render('provider.offer.index');
    }
    public function postOffers(Request $request)
    {
        $this->validate($request,[
            'item_id' => 'required|exists:items,id',
            'discount_price' => 'required|numeric|digits_between:1,99',
            'from' => 'required|before:to',
            'to' => 'required',
        ]);
        $item = Item::where('id',$request['item_id'])->where('user_id',auth()->id())->first();
        if($item == null){
            return back();
        }
        dd($item);
        $item->discount_price = $request['discount_price'];
        $item->group->discount_price = $request['discount_price'];
        $item->from = $request['from'];
        $item->group->from = $request['from'];
        $item->to = $request['to'];
        $item->group->to = $request['to'];
        $item->save();
        $item->group->save();
        return back()->with('success',awtTrans('تم اضافة الخصم بنجاح'));
    }


}
