<?php

namespace App\Http\Controllers\Provider;

use App\DataTables\Reports\AllOrdersDatatable;
use App\DataTables\Reports\TodayOrdersDatatable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function today(TodayOrdersDatatable $datatable)
    {
        return $datatable->render('provider.reports.today');
    }


    public function all(AllOrdersDatatable $datatable)
    {
        return $datatable->render('provider.reports.all');
    }


}
