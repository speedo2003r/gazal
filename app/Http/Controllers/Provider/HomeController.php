<?php

namespace App\Http\Controllers\Provider;

use App\Entities\Order;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\IOrder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    protected $orderRepo, $branchRepo;

    public function __construct(IOrder $orderRepo, IBranch $branchRepo)
    {
        $this->orderRepo = $orderRepo;
        $this->branchRepo = $branchRepo;
    }

    /***************** dashboard *****************/
    public function dashboard()
    {
        $data = [
            'orders_today' => $this->orderRepo->findWhere([
                ['user_id', '=', currentUser()->id],
                ['created_at', 'DATE', Carbon::today()]
            ])->count(),
            'completed_orders' => $this->orderRepo->findWhere([
                ['user_id', '=', currentUser()->id],
                ['created_at', 'DATE', Carbon::today()],
                ['order_status', '=',Order::STATUS_FINISH]
            ])->count(),
            'cancelled_orders' => $this->orderRepo->findWhere([
                ['user_id', '=', currentUser()->id],
                ['created_at', 'DATE', Carbon::today()],
                ['order_status', '=',Order::STATUS_CANCELED]
            ])->count(),
            'all_branches' => $this->branchRepo->findWhere(['provider_id' => currentUser()->id])->count(),
            'offline_branches' => $this->branchRepo->findWhere(['provider_id' => currentUser()->id, 'appear' => 0])->count(),
        ];
        return view('provider.index', $data);
    }

    public function changeLanguage($lang)
    {
        Session::put('applocale', $lang);
        return redirect()->back();
    }
}
