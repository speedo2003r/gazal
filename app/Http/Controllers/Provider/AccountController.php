<?php

namespace App\Http\Controllers\Provider;

use App\DataTables\SellerPayDatatable;
use App\Entities\SellerPay;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\IUser;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{
    use UploadTrait;
    protected $user;
    public function __construct(IUser $user)
    {
        $this->user = $user;
    }

    public function index(SellerPayDatatable $datatable)
    {
        $allselling = DB::table('selling_view')->select('price','order_id')->leftJoin('orders','orders.id','=','selling_view.order_id')->where('orders.provider_id',auth()->id())->sum('price') * DB::table('selling_view')->select('count','order_id')->leftJoin('orders','orders.id','=','selling_view.order_id')->where('orders.provider_id',auth()->id())->sum('count');
        $sellerPayPrice = SellerPay::where('user_id',auth()->id())->where('status',0)->sum('price');
        return $datatable->render('provider.account.index',compact('allselling','sellerPayPrice'));
    }

    public function pay(Request $request)
    {
        $totalSellerPays = SellerPay::where('user_id',auth()->id())->where('status',0)->sum('price');
        if($totalSellerPays <= 0){
            return back()->with('success','لا يوجد أي مديونيه عليك');
        }
        $user = auth()->user();
        if($request['payment'] == null){
            return back()->with('success','يرجي اختيار طريقة الدفع');
        }
        if($request['payment'] == 'online'){
            return back()->with('success','تحت الانشاء قريبا');
        }
        if($request['pay_id'] != null){
            $SellerPay = SellerPay::where('id',$request['pay_id'])->where('user_id',auth()->id())->first();
            if($request['payment'] == 'bank') {
                $this->validate($request, [
                    'bank_name' => 'required',
                    'acc_owner_name' => 'required',
                    'acc_number' => 'required',
                    'image' => 'required',
                ]);
                DB::table('seller_pays')->where('id', $request['pay_id'])->where('user_id', auth()->id())->where('status', 0)->where('type', null)->update([
                    'type' => $request['payment'],
                    'acc_owner_name' => $request['acc_owner_name'],
                    'bank_name' => $request['bank_name'],
                    'acc_number' => $request['acc_number'],
                    'image' => $this->uploadFile($request['image'],'banks'),
                    'status' => 0,
                    'pay_status' => 0,
                ]);
            }
            if($request['payment'] == 'wallet' && $user->wallet < $user->balance){
                $msg = 'لا يوجد رصيد كافي في المحفظة.';
                return back()->with('success',$msg);
            }elseif ($request['payment'] == 'wallet' && $user->wallet > $user->balance){
                $SellerPay->type = $request['payment'];
                $SellerPay->status = 1;
                $SellerPay->pay_status = 1;
                $SellerPay->save();
                $user->balance -= $SellerPay['price'];
                $user->wallet -= $SellerPay['price'];
                $user->save();
            }


            return back()->with('success','تم التسويه بنجاح');
        }else{
            if($request['payment'] == 'bank'){
                $this->validate($request,[
                    'bank_name'=>'required',
                    'acc_owner_name'=>'required',
                    'acc_number'=>'required',
                    'image'=>'required',
                ]);
                $firstpay = SellerPay::where('user_id',auth()->id())->where('status',0)->first();
                if($firstpay == null){
                    return back()->with('danger','تم تسوية الطلبات السابقه');
                }
                if($firstpay){
                    $firstpay->type = $request['payment'];
                    $firstpay->acc_owner_name = $request['acc_owner_name'];
                    $firstpay->bank_name = $request['bank_name'];
                    $firstpay->acc_number = $request['acc_number'];
                    $firstpay->status = 0;
                    $firstpay->pay_status = 0;
                    $firstpay->image = $this->uploadFile($request['image'],'banks');
                    $firstpay->save();
                    DB::table('seller_pays')->where('user_id',auth()->id())->where('id','!=',$firstpay['id'])->where('status',0)->where('type',null)->update([
                        'type' => $request['payment'],
                        'acc_owner_name' => $request['acc_owner_name'],
                        'bank_name' => $request['bank_name'],
                        'acc_number' => $request['acc_number'],
                        'parent_id' => $firstpay['id'],
                        'status' => 0,
                        'pay_status' => 0,
                    ]);
                }

            }
            if($request['payment'] == 'wallet' && $user->wallet < $user->balance){
                $msg = 'لا يوجد رصيد كافي في المحفظة.';
                return back()->with('danger',$msg);
            }elseif ($request['payment'] == 'wallet' && $user->wallet > $user->balance){
                $totalSellerPays = SellerPay::where('user_id',auth()->id())->where('type',null)->where('status',0)->sum('price');
                $SellerPays = DB::table('seller_pays')->where('user_id',auth()->id())->where('type',null)->where('status',0)->update([
                    'type' => $request['payment'],
                    'status' => 1,
                    'pay_status' => 1,
                ]);
                $user->wallet -= $totalSellerPays;
                $user->balance -= $totalSellerPays;
                $user->save();
            }

            return back()->with('success','تم التسويه بنجاح للكل');
        }
    }
}
