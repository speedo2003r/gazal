<?php

namespace App\Http\Controllers\Provider;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IDevice;
use App\Repositories\Interfaces\IProvider;
use App\Repositories\Interfaces\IUser;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use UploadTrait;
    protected $user,$device,$category,$country,$provider;
    public function __construct(ICategory $category, IProvider $provider, ICountry $country, IUser $user,IDevice $device)
    {
        if(app()->getLocale() != 'ar'){
            app()->setLocale('ar');
        }
        $this->provider = $provider;
        $this->user = $user;
        $this->device = $device;
        $this->country = $country;
        $this->category = $category;
    }
    /***************** show login form *****************/
    public function showLoginForm()
    {
        return view('provider.auth.login');
    }

    /**************** show login form *****************/
    public function login(Request $request)
    {
        $request->validate([
            'userName'     => 'required|max:191',
            'password'  => 'required|max:191',
        ]);
        $credentials = ['password' => $request->password];
        if (checkEmail($request->userName)) $credentials['email'] = $request->userName;
        else $credentials['phone'] = $request->userName;

        if (auth('provider')->attempt($credentials, $request->filled('remember')))
            return redirect()->route('provider.home');
        else
            return redirect()->route('provider.show.login')->withErrors('تحقق من صحة البيانات المدخلة');

    }

    public function register()
    {
        $countries = $this->country->all();
        $categories = $this->category->where('parent_id',null)->get();
        return view('provider.auth.register',compact('countries','categories'));
    }
    public function register2()
    {
        return view('provider.auth.register2');
    }
    public function checkUser(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'store_name' => 'required',
            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
            'phone' => 'required|digits_between:9,13|unique:users,phone,NULL,id,deleted_at,NULL',
            'category_id' => 'required|exists:categories,id,deleted_at,NULL',
            'country_id' => 'required|exists:countries,id',
            'city_id' => 'required|exists:cities,id',
            'password' => 'required|confirmed',
        ]);
        if($validator->fails()){
            return response()->json(['value' => 0,'msg'=>$validator->errors()->first()]);
        }
        $data = array_filter($request->except('_token'));
        if(session()->exists('data')){
            session()->forget('data');
        }
        session()->put('data',$data);
        return response()->json(['value' => 1, 'url' => route('provider.register2')]);
    }
    public function postRegister(Request $request)
    {
        $data = \session()->get('data');
        $validator = Validator::make($request->all(),[
            'id_avatar' => 'required|image|mimes:jpg,jpeg,png',
            'acc_bank' => 'required|image|mimes:jpg,jpeg,png',
            'commercial_image' => 'required|image|mimes:jpg,jpeg,png',
            'tax_certificate' => 'required|image|mimes:jpg,jpeg,png',
            'municipal_license' => 'required|image|mimes:jpg,jpeg,png',
            'banner' => 'required|image|mimes:jpg,jpeg,png',
        ]);
        if($validator->fails()){
            return response()->json(['value' => 0,'msg'=>$validator->errors()->first()]);
        }
        $code = generateCode();
        $data['user_type']  = 'provider';
        $data['v_code']  = $code;
        $data['accepted']  = 0;
        $user = $this->user->create($data);
        $providerData = $request->all();
        $providerData['user_id'] = $user['id'];
        $providerData['store_name'] = [
            'ar' => $data['store_name'],
            'en' => $data['store_name'],
        ];
        if($request->has('tax_certificate')){
            $logo = $this->uploadFile($request['tax_certificate'],'providers');
            $providerData['tax_certificate'] = $logo;
        }
        if($request->has('commercial_image')){
            $logo = $this->uploadFile($request['commercial_image'],'providers');
            $providerData['commercial_image'] = $logo;
        }
        if($request->has('municipal_license')){
            $logo = $this->uploadFile($request['municipal_license'],'providers');
            $providerData['municipal_license'] = $logo;
        }
        if($request->has('acc_bank')){
            $logo = $this->uploadFile($request['acc_bank'],'providers');
            $providerData['acc_bank'] = $logo;
        }
        if($request->has('id_avatar')){
            $logo = $this->uploadFile($request['id_avatar'],'providers');
            $providerData['id_avatar'] = $logo;
        }
        if($request->has('banner')){
            $logo = $this->uploadFile($request['banner'],'providers');
            $providerData['banner'] = $logo;
        }
        $this->provider->create($providerData);
        if (Auth::attempt(['phone' => $data['phone'], 'password' => $data['password']],true)) {
            #Success response
            \session()->forget('data');
            return response()->json(['value' => 1, 'url' => route('provider.active')]);
        }
    }
    public function forget()
    {
        return view('provider.auth.forget',);
    }
    public function reset()
    {
        return view('provider.auth.reset');
    }
    public function sendActiveCode(Request $request)
    {
        $this->validate($request,[
            'phone'     => 'required|exists:users,phone',
        ]);
        $user = $this->user->findWhere(['phone'=>$request->input('phone')])->first();
        $this->user->update(['v_code' => generateCode()],$user->id);
//        sendSMS($user->phone,$user->v_code);
        session()->put('phone',$request['phone']);
        return redirect()->route('provider.reset');
    }
    public function resetPassword(Request $request)
    {
        $this->validate($request,[
            'code'   => ['required'],
            'password'   => ['required','confirmed'],
        ]);
        $user = $this->user->findWhere(['phone'=>session()->get('phone')])->first();
        if ($user->v_code === $request->input('code')) {
            $this->user->update([
                'password'=>$request['password'],
            ],$user['id']);
            Session::forget('phone');
            return redirect()->route('provider.login')->with('success','تم تغيير كلمة المرور بنجاح');
        }
        return back()->with('danger','الكود غير صحيح');
    }
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('provider.show.login');
    }

    public function active()
    {
        if(!auth()->check()){
            return redirect()->route('provider.register');
        }
        return view('provider.auth.active');
    }
    public function Activation(Request $request)
    {
        $user = auth()->user();
        if ($user->v_code === $request->input('code')) {
            $this->user->update([
                'active'=>1,
                'online'=>1,
            ],$user['id']);
            return redirect()->route('provider.home')->with('success','تم تفعيل الهاتف بنجاح');
        }
        return back()->with('danger', 'الكود غير صحيح');
    }

    public function activeCode(Request $request)
    {
        $user = auth()->user();
        $this->user->update(['v_code' => generateCode()],$user['id']);
//        sendSMS($user->phone,$user->v_code);
        return back()->with('success', 'تم الارسال بنجاح');
    }
}
