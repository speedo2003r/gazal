<?php

namespace App\Http\Controllers\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Coupon\Create;
use App\Http\Requests\Admin\Coupon\Update;
use App\Repositories\Interfaces\ICoupon;
use App\Repositories\Interfaces\IUser;
use Illuminate\Http\Request;

class CouponController extends Controller
{

    protected $user,$package,$coupon;
    public function __construct(ICoupon $coupon, IUser $user)
    {
        $this->user = $user;
        $this->coupon = $coupon;
    }

    public function index()
    {
        $coupons = $this->coupon->findWhere(['seller_id'=>currentUser()->id]);
        return view('provider.coupon.index',compact('coupons'));
    }
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        $data['type'] = 'public';
        $data['seller_id'] = currentUser()->id;
        if($data['kind'] == 'percent' && $data['value'] > 100){
            return redirect()->back()->with('danger', awtTrans('لا يمكن اضافة كوبون أكبر من 100 %'));
        }
        $this->coupon->create($data);
        return redirect()->back()->with('success', awtTrans('تم الاضافه بنجاح'));

    }
    /***************************  update provider  **************************/
    public function update(Update $request, $id)
    {
        $data = array_filter($request->all());
        $coupon = $this->coupon->find($id);
        if($data['kind'] == 'percent' && $data['value'] > 100){
            return redirect()->back()->with('danger', awtTrans('لا يمكن اضافة كوبون أكبر من 100 %'));
        }
        $this->coupon->update($data,$coupon['id']);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }
    /***************************  delete item  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->coupon->delete($d);
                }
            }
        }else {
            $role = $this->coupon->find($id);
            $this->coupon->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }
}
