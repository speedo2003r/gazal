<?php

namespace App\Http\Controllers\Provider;

use App\DataTables\OrderSellerDatatable;
use App\Entities\Order;
use App\Entities\UserAddress;
use App\Http\Controllers\Controller;
use App\Jobs\OrderToDelegates;
use App\Models\User;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IItem;
use App\Repositories\Interfaces\IOrder;
use App\Repositories\Interfaces\IUser;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    use ResponseTrait;
    use UploadTrait;
    use NotifyTrait;
    protected $userRepo,$order,$country,$itemRepo,$category,$detail,$branch;

    public function __construct(IOrder $order,ICategory $category, IItem $item, IUser $user, ICountry $country, IBranch $branch)
    {
        $this->userRepo = $user;
        $this->order = $order;
        $this->country = $country;
        $this->itemRepo = $item;
        $this->category = $category;
        $this->branch = $branch;
    }

    public function index(OrderSellerDatatable $orderSellerDatatable)
    {
        $branches = $this->branch->findWhere(['provider_id' => currentUser()->id]);
       // dd(is_null(\request('order_status')));
        if (\request()->routeIs('provider.orders.history')) {
            $render = 'provider.order.history';
        } else {
            $render = 'provider.order.index';
        }
        return $orderSellerDatatable->render($render, compact('branches'));
    }
    public function return()
    {
        $orders = $this->order->whereHas('reclaims',function ($reclaim){
            return $reclaim;
        })->get();
        return view('provider.order.return',compact('orders'));
    }
    public function returnDetails(Request $request,Order $order)
    {
        if($order['provider_id'] != currentUser()->id){
            return back();
        }
        return view('provider.order.detail',compact('order'));
    }

    public function postReturnOrder(Request $request)
    {
        $order = Order::find($request['order_id']);
        if($order['provider_id'] != currentUser()->id){
            return back();
        }
        $reclaims = $order->reclaims;
        if($request['type'] == 1){
            foreach ($reclaims as $reclaim){
                $reclaim->update([
                    'approve'=>1,
                    'accepted_date'=>Carbon::now()->format('Y-m-d H:i'),
                ]);
            }
            $this->send_notify($order['user_id'], ' لقد تم الموافقه علي اعادة طلبك رقم ' . $order['order_num'] , 'your order to return your order number '.$order['order_num'].' is accepted', $order['id'], 'accept_return_order');
//            $sum = $order->reclaims()->sum('count');
//            $orderReclaim = $this->order->create([
//                'live' => 1,
//                'total_items' => $sum,
//                'provider_id' => $order['provider_id'],
//                'branch_id' => $order['branch_id'],
//                'user_id' => $order['user_id'],
//                'status' => 'return',
//                'reclaim_id' => $order['id'],
//            ]);
            return back()->with('success',awtTrans('تم الموافقه علي طلب الاعاده'));
        }elseif ($request['type'] == 2){
            foreach ($reclaims as $reclaim){
                $reclaim->update([
                    'approve'=>2
                ]);
            }
            $this->send_notify($order['user_id'], ' لقد تم رفض اعادة طلبك رقم ' . $order['order_num'] , 'your order to return your order number '.$order['order_num'].' is refused', $order['id'], 'refuse_return_order');
            return back()->with('success',awtTrans('تم رفض طلب الاعاده'));
        }else{
            return back();
        }
    }

    public function show($id)
    {
        //$cars = $this->car->all();
        $order = $this->order->find($id);
        return view('provider.order.show',compact('order'));
    }

    public function changeCarType(Request $request)
    {
        $this->validate($request,[
            'car_type_id' => 'required|exists:car_types,id',
            'order_id' => 'required|exists:orders,id',
        ]);
        $order = $this->order->find($request['order_id']);
        if($order['provider_id'] != currentUser()->id){
            return back();
        }
        $lat = $order['lat'];
        $lng = $order['lng'];
        $carType_id = $request['car_type_id'];
        $city_name = getCityBylatlng($lat,$lng,'en');
        if($city_name == $order->branch->city['city_name']){
            $distance = $order->branch->distance($lat,$lng);
            $shipCost = ($carType_id != null && $order->branch['free_ship'] == 0) ? $order->branch->city->shippingPrice((int) $carType_id,1,$distance) : 0;
        }else{
            $distance = $order->branch->distance($lat,$lng);
            $shipCost = ($carType_id != null && $order->branch['free_ship_external'] == 0) ? $order->branch->city->shippingPrice((int) $carType_id,2,$distance) : 0;
        }
        $this->order->update([
            'car_type_id' => $request['car_type_id'],
            'shipping_price' => round($shipCost,2),
        ],$order['id']);
        return back()->with('success',awtTrans('تم تغيير المركبه'));
    }

    public function orderRefuse(Request $request)
    {
        $this->validate($request,[
            'order_id' => 'required|exists:orders,id',
            'notes' => 'required',
        ]);
        $order = $this->order->find($request['order_id']);
        if($order['provider_id'] != currentUser()->id && $order['order_status'] != Order::STATUS_WAITING){
            return back();
        }
        $this->order->update([
            'status' => 'refused',
            'order_status' => Order::STATUS_REFUSED,
            'provider_status' => 'refused',
            'notes' => $request['notes'],
        ],$order['id']);
        if($order['pay_type'] == 'online'){
            $user = $order->user;
            $user->wallet = $order->_price();
            $user->save();
        }
//        $ids = [];
//        foreach($query as $q)
//        {
//            if($q->distance <= $ship_radius){
//                array_push($ids, $q->id);
//            }
//        }
//        $dels = User::whereIn( 'id', $ids)->where('user_type','delegate')->active()->get();

            $content_ar = 'تم رفض طلبك رقم '.$order['id'];
            $content_en = 'your order number '.$order['id'].' was refused';
            $this->send_notify($order->user['id'], $content_ar, $content_en,$order['id'],$order['status'],'refused_order');

//        foreach ($order->orderProducts as $orderProduct){
//            if($orderProduct->group){
//                $orderProduct->group->update([
//                    'count' => $orderProduct->group->count + $orderProduct['qty']
//                ]);
//            }
//
//        }
        if ($request->wantsJson()) {
            return response()->json('success');
        }
        return redirect()->back()->with('success',awtTrans('تم رفض الطلب'));
    }
    public function orderAccept(Request $request)
    {
        $this->validate($request,[
            'order_id' => 'required|exists:orders,id',
        ]);
        $order = $this->order->find($request['order_id']);
        if($order['provider_id'] != currentUser()->id && $order['status'] != 'new'){
            return back();
        }
        $this->order->update([
            'order_status' =>Order::STATUS_ACCEPTED,
        ],$order['id']);

        $this->send_notify($order->user,'تم الموافقه علي طلبك','your order has been accepted', ' تم الموافقه على الطلب رقم ' . $order['order_num'], 'Your order number '.$order['order_num'].' was accepted',  'provider');
//        $ship_radius = settings('ship_radius_delegate');

        $job = new OrderToDelegates($order);
        $this->dispatch($job);
        if ($request->wantsJson()) {
            return response()->json('success');
        }
        return redirect()->back()->with('success',awtTrans('تم قبول الطلب'));
    }
    public function orderOnWay(Request $request)
    {
        $this->validate($request,[
            'order_id' => 'required|exists:orders,id',
        ]);
        $order = $this->order->find($request['order_id']);
        if($order['provider_id'] != currentUser()->id && $order['status'] != 'pending'){
            return back();
        }
        if($order['delegate_id'] == null){
            return back()->with('danger',awtTrans('لا يوجد مندوب للطلب'));
        }
        $this->order->update([
            'status' => 'onWay',
            'provider_status' => 'shipped',
        ],$order['id']);

        if($order['delegate_id'] != null && $order->delegate['notify'] == 1){
            $this->send_notify($order['delegate_id'], ' تم تأكيد تسليم الطلب رقم ' . $order['order_num']. 'الي المندوب ', 'Your order number '.$order['order_num']. ' was delivered to delegate', $order['id'], $order['status'], 'order_prepared');
        }
        $this->send_notify($order['user_id'], ' تم تأكيد تسليم الطلب رقم ' . $order['order_num']. 'الي المندوب ', 'Your order number '.$order['order_num']. ' was delivered to delegate', $order['id'], $order['status'], 'order_prepared');
        return redirect()->route('seller.newOrders')->with('success',awtTrans('تم تأكيد التسليم للمندوب'));
    }

}
