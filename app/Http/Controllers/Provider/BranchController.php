<?php

namespace App\Http\Controllers\Provider;

use App\DataTables\BranchDatatable;
use App\Entities\Branch;
use App\Entities\BranchDate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Provider\BranchRequest;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IUser;
use App\Traits\UploadTrait;

class BranchController extends Controller
{
    use UploadTrait;
    protected $user,$branch,$country,$city, $path;

    public function __construct(ICountry $country,ICity $city,IBranch $branch,IUser $user)
    {
        $this->user = $user;
        $this->city = $city;
        $this->country = $country;
        $this->branch = $branch;
        $this->path = 'provider.branch';
        $this->middleware('my-branches');
    }

    public function index(BranchDatatable $branchDatatable)
    {
        $status = null;
        //dd( auth('provider')->user()
        //                    ->employee_branches()->with(['branchDates', 'reviews'])
        //                    ->when(!is_null($status), function ($q) use ($status) {
        //                        $q->works($status);
        //
        //                    })
        //                    ->latest()->query());
        return $branchDatatable->render("{$this->path}.index");
    }

    public function create()
    {
        $cities = auth('provider')->user()->country->cities;
        $countries = $this->country->all();
        return view("{$this->path}.create", compact('cities', 'countries'));
    }


    public function store(BranchRequest $request)
    {
        $data = $request->all();

        $data['provider_id'] = auth('provider')->id();
        $data['appear'] = 1;

        // Check if there are another branch in same city
        $exists = $this->branch->where('provider_id',auth('provider')->id())->where('city_id',$request['city_id'])->exists();
        if($exists){
            return back()->with('danger','يوجد بالفعل فرع في هذه المدينه');
        }

        $data['avatar'] = $this->uploadFile($request->image, 'branches');
        $this->branch->create($data);


        if (!is_null($request->redirect_back)) {
            return redirect()->back()->with('success', 'تم الاضافه بنجاح');
        }
        return redirect()->route("{$this->path}.index")->with('success', 'تم الاضافه بنجاح');
    }

    public function show($id)
    {
        $item = $this->branch->find($id);
        $item->load(['country', 'city']);
        return view("{$this->path}.show", compact('item'));
    }

    public function edit($id)
    {
        $countries = $this->country->all();
        return view("{$this->path}.edit", ['item' => $this->branch->find($id), 'countries' => $countries]);
    }


    public function update(BranchRequest $request,$id)
    {
        $data = array_filter($request->all());
        $branch = $this->branch->find($id);
        $exists = Branch::where('provider_id',auth('provider')->id())->where('city_id',$request['city_id'])->exists();
        if($exists && $branch['city_id'] != $request['city_id']){
            return back()->with('danger','يوجد بالفعل فرع في هذه المدينه');
        }
        if(isset($data['image'])){
            if($branch['avatar'] != null){
                $this->deleteFile($branch['avatar'],'branches');
            }
            $data['avatar'] = $this->uploadFile($request['image'],'branches');
        }


        $this->branch->update($data,$branch['id']);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

    public function delete($id)
    {
        $this->branch->delete($id);
        return response()->json('success');
    }

    public function toggleAppear(Branch $branch)
    {
        $this->branch->update(['appear' => !$branch->appear], $branch->id);
        return response()->json('success');
    }

}
