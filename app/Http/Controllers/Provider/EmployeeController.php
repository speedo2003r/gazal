<?php

namespace App\Http\Controllers\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Provider\EmployeeRequest;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IProvider;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    use UploadTrait;

    protected $repo, $path, $countryRepo;

    public function __construct(IProvider $repo, ICountry $countryRepo)
    {
        $this->repo = $repo;
        $this->countryRepo = $countryRepo;
        $this->path = 'provider.employees';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = $this->repo->where(['parent_id' => auth('provider')->id()])->latest()->paginate(15);
        return view("{$this->path}.index", compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = $this->countryRepo->all();
        return view("{$this->path}.create", compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EmployeeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $request->merge([
            'active' => 1,
            'user_type' => 2,
            'parent_id' => auth('provider')->id(),
            'avatar' => $this->uploadFile($request->avatar, 'providers')
        ]);
        $employee = $this->repo->create($request->all());
        $employee->employee_branches()->attach($request->branches);
        if ($request->has('redirect_back')) {
            return redirect()->back()->with('success', __('Added Successfully'));
        }
        return redirect()->route("{$this->path}.index")->with('success', __('Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view("{$this->path}.show", ['item' => $this->repo->find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view("{$this->path}.edit", [
            'item' => $this->repo->find($id),
            'countries' => $countries = $this->countryRepo->all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EmployeeRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, $id)
    {
        $data = $request->all();
        if ($request->hasFile('avatar')) {
            $data['avatar'] = $this->uploadFile($request->avatar, 'providers');
        }
        $item = $this->repo->update($data, $id);
        $item->employee_branches()->sync($request->branches);
        return redirect()->route("{$this->path}.index")->with('success', __('Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);
        return redirect()->back()->with('success', __('Deleted Successfully'));
    }

    public function changeStatus($id)
    {
        $item = $this->repo->find($id);
        $item->update(['active' => !$item->active]);
        return response()->json(__('Success'));
    }

}
