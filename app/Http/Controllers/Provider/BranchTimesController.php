<?php

namespace App\Http\Controllers\Provider;

use App\Entities\BranchDate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BranchTimesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'timefrom' => 'required|before:timeto',
            'timeto'   => 'required|after:timefrom',
        ]);
        foreach ($request['work_days'] as $day) {
            $date = BranchDate::where('day', $day)->where('branch_id', $id)->where('shift', $request['shift'])->first();
            if ($date) {
                $date->update([
                    'timefrom' => $request['timefrom'],
                    'timeto'   => $request['timeto'],
                ]);
            } else {
                BranchDate::create([
                    'timefrom'  => $request['timefrom'],
                    'timeto'    => $request['timeto'],
                    'day'       => $day,
                    'shift'     => $request['shift'],
                    'branch_id' => $id,
                    'type' => $request['type'],
                ]);
            }
        }
        return redirect()->back()->with('success', 'تم الحفظ بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $branch, $time)
    {
        $dates = BranchDate::where('day', $request['date'])->get();
        foreach ($dates as $date) {
            $date->delete();
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }
}
