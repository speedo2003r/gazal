<?php

namespace App\Http\Controllers\Provider;

use App\Entities\Notification;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\INotification;
use App\Repositories\Interfaces\IUser;

class NotificationController extends Controller
{

    protected $user,$notification;
    public function __construct(INotification $notification,IUser $user)
    {
        $this->user = $user;
        $this->notification = $notification;
    }

    public function index()
    {
        $data = Notification::where(['to_id'=>auth()->id(),'seen'=>0])->latest()->get();
        foreach ($data as $d){
            $d->seen = 1;
            $d->save();
        }
        $notifications = Notification::where(['to_id'=>auth()->id()])->latest()->paginate(10);
        return view('provider.notification.index',compact('notifications'));
    }
}
