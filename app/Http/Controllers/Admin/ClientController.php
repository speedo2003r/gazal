<?php

namespace App\Http\Controllers\Admin;

use App\Entities\BlockUser;
use App\Entities\Branch;
use App\Entities\Delegate;
use App\Entities\Provider;
use App\Entities\UserAddress;
use App\Entities\Wallet;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Client\Create;
use App\Http\Requests\Admin\Client\Update;
use App\Jobs\Notify;
use App\Models\User;
use App\Repositories\CityRepository;
use App\Repositories\CountryRepository;
use App\Repositories\DelegateRepository;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IDelegate;
use App\Repositories\Interfaces\IProvider;
use App\Repositories\Interfaces\IUser;
use App\Repositories\ProviderRepository;
use App\Repositories\UserRepository;
use App\Traits\CountryCodeTrait;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class ClientController extends Controller
{
    use NotifyTrait;
    use ResponseTrait;
    use UploadTrait;
    use CountryCodeTrait;
    protected $user,$provider,$delegate, $country,$city;

    public function __construct(IProvider $provider,IDelegate $delegate,IUser $user,ICountry $country,ICity $city)
    {
        $this->delegate = $delegate;
        $this->provider = $provider;
        $this->user = $user;
        $this->country = $country;
        $this->city = $city;
    }

    /***************************  get all users  **************************/
    public function index()
    {
        $countries = $this->country->all();
        $clientsCount = $this->user->count();
        return view('admin.clients.index',compact('countries','clientsCount'));
    }

    /***************************  create user  **************************/
    public function create()
    {
        $allCountries = $this->getCountries();
        $countries = $this->country->all();
        return view('admin.clients.create',compact('allCountries','countries'));
    }
    /***************************  get all providers  **************************/
    public function show($id)
    {
        $client = $this->user->find($id);
        return view('admin.clients.show',compact('client'));
    }
    /***************************  get all addresses  **************************/
    public function addresses($id)
    {
        $client = $this->user->find($id);
        $countries = $this->country->all();
        return view('admin.clients.addresses.addresses',compact('client','countries'));
    }
    /***************************  save addresses  **************************/
    public function addressCreate($id)
    {
        $countries = $this->country->all();
        $client = $this->user->find($id);
        return view('admin.clients.addresses.create',compact('countries','client'));
    }
    public function addressEdit($id)
    {
        $countries = $this->country->all();
        $address = UserAddress::find($id);
        $client = $address->user;
        return view('admin.clients.addresses.edit',compact('address','countries','client'));
    }
    /***************************  save addresses  **************************/
    public function storeAddress(Request $request)
    {
        $user = User::find($request['user_id']);
        UserAddress::create($request->all());
        return redirect()->route('admin.clients.addresses',$user['id'])->with('success',awtTrans('تم الحفظ بنجاح'));
    }
    /***************************  save addresses  **************************/
    public function updateAddress(Request $request,$id)
    {
        $address = UserAddress::find($id);
        $address->update($request->all());
        $user = $address->user;
        return redirect()->route('admin.clients.addresses',$user['id'])->with('success',awtTrans('تم التحديث بنجاح'));
    }


    /***************************  store provider **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->except('image'));
        if($request->has('image')){
            $data['avatar'] = $this->uploadFile($request['image'],'users');
        }
        $this->user->create($data);
        return redirect()->route('admin.clients.index')->with('success', 'تم الاضافه بنجاح');
    }


    /***************************  update provider  **************************/
    public function update(Update $request, $id)
    {
        $client = $this->user->find($id);
        if($request->has('image')){
            if($client['avatar'] != null || $client['avatar'] != '/default.png'){
                $this->deleteFile($client['avatar'],'users');
            }
            $request->request->add(['avatar'=>$this->uploadFile($request['image'],'users')]);
        }
        $this->user->update(array_filter($request->except('image')),$client['id']);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

    public function sendnotifyuser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title_ar' => 'required|max:255',
            'title_en' => 'required|max:255',
            'message_ar' => 'required|max:255',
            'message_en' => 'required|max:255',
        ]);
        #error response
        if ($validator->fails())
            return response()->json(['value' => 0, 'msg' => $validator->errors()->first()]);
        if ($request->type == 'client'){
            $model = new User();
        }elseif($request->type == 'provider'){
            $model = new Provider();
        }elseif($request->type == 'delegate'){
            $model = new Delegate();
        }else{
            $model = new Branch();
        }
        if ($request->notify_type == 'all'){
            if($request['country_id']){
                $users = $model->where('country_id',$request['country_id'])->get();
            }else{
                $users = $model->all();
            }
        }else{
            $users = $model->whereId($request->id)->get();
        }
        foreach ($users as $user) {
            $title_ar = $request->title_ar;
            $title_en = $request->title_en;
            $message_ar = $request->message_ar;
            $message_en = $request->message_en;
            #send FCM
            $job = (new Notify(auth('admin')->user(),$user,$message_ar,$message_en));
            if($user){
                $data['title'] = app()->getLocale() == 'ar' ? $title_ar: $title_en;
                $data['body'] = app()->getLocale() == 'ar' ? $message_ar: $message_en;
                if($user->Devices){
                    foreach ($user->Devices as $device) {
                        if($device->device_id){
                            $this->send_fcm($device->device_id, $data, $device->device_type);
                        }
                    }
                }
            }
            dispatch($job);
        }
        return $this->ApiResponse('success', 'تم الارسال بنجاح');
    }

    public function changeStatus(Request $request)
    {
        if($request->ajax()){
            if($request['type'] == 'delegate'){
                $user = $this->delegate->find($request['id']);
            }elseif($request['type'] == 'provider'){
                $user = $this->provider->find($request['id']);
            }else{
                $user = $this->user->find($request['id']);
            }
            $user['banned'] = !$user['banned'];
            $user->save();
            if($user['banned'] == 1){
//                $this->send_notify($user['id'],'لقد تم حظرك من قبل الاداره' , 'you have been blocked from manager',null,null,'block');
            }
            return $this->ApiResponse('success','',$user['banned']);
        }
    }
    public function addToWallet(Request $request)
    {
        $this->validate($request,[
            'amount' => 'required|min:1|max:20',
            'notes' => 'required|string|max:2000',
        ],[],[
            'amount' => awtTrans('الرصيد'),
            'notes' => awtTrans('السبب'),
        ]);
        $user = User::find($request['user_id']);
        $user->wallet += $request['amount'];
        $user->save();
        Wallet::create([
            'amount' => $request['amount'],
            'type' => 'deposit',
            'notes' => $request['notes'],
            'user_id' => $request['user_id'],
            'expire_date' => $request['expire_date'],
        ]);
        return back()->with('success','تم الاضافه بنجاح');
    }
    public function compensation(Request $request)
    {
        $this->validate($request,[
            'amount' => 'required|min:1|max:20',
            'notes' => 'required|string|max:2000',
            'order_id' => 'required|exists:orders,id,deleted_at,NULL',
        ],[],[
            'amount' => awtTrans('الرصيد'),
            'notes' => awtTrans('السبب'),
            'order_id' => awtTrans('الطلب'),
        ]);
        $user = User::find($request['user_id']);
        $user->wallet += $request['amount'];
        $user->save();
        Wallet::create([
            'amount' => $request['amount'],
            'type' => 'compensation',
            'notes' => $request['notes'],
            'user_id' => $request['user_id'],
            'order_id' => $request['order_id'],
        ]);
        return back()->with('success','تم الاضافه بنجاح');
    }
    public function block(Request $request)
    {
        $this->validate($request,[
            'notes' => 'required|string|max:2000',
        ],[],[
            'notes' => awtTrans('السبب')
        ]);
        BlockUser::create([
            'notes' => $request['notes'],
            'modal_id' => $request['user_id'],
            'modal_type' => User::class,
        ]);
        $user = User::find($request['user_id']);
        $user->banned = 1;
        $user->save();
        return back()->with('success','تم الحظر بنجاح');
    }
    public function unblock(Request $request)
    {
        $user = User::find($request['user_id']);
        $user->banned = 0;
        $user->save();
        return back()->with('success','تم الغاء الحظر بنجاح');
    }

    public function getFilterData(Request $request,User $user)
    {
        $items = $user->query()
            ->select('users.*',DB::raw('SUM(review_rates.rate) as sumrate'))
            ->leftJoin('review_rates',function ($q){
                $q->on('review_rates.rateable_id','=','users.id');
                $q->where('review_rates.rateable_type',User::class);
            })
            ->groupBy('users.id')
            ->latest();
        return DataTables::of($items)
            ->addColumn('status',function ($query){
                return '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success" style="direction: ltr">
                            <input type="checkbox" onchange="changeUserStatus('.$query->id.')" '.($query->banned == 0 ? 'checked' : '') .' class="custom-control-input" id="customSwitch'.$query->id.'">
                            <label class="custom-control-label" id="status_label'.$query->id.'" for="customSwitch'.$query->id.'"></label>
                        </div>';
            })
            ->addColumn('orders',function ($query){
                return '<a href="'.route('admin.orders.client',$query['id']).'" class="btn btn-outline-success">('.$query->ordersAsUser()->count().')طلبات</a>';
            })
            ->addColumn('addresses',function ($query){
                return '<a href="'.route('admin.clients.addresses',$query['id']).'" class="btn btn-outline-success">العناوين</a>';
            })
            ->addColumn('url',function ($query){
                return 'admin.clients.delete';
            })
            ->addColumn('showUrl',function ($query){
                return 'admin.clients.show';
            })
            ->addColumn('target',function ($query){
                return 'editModel';
            })
            ->addColumn('data',function ($query){
                return $query;
            })
            ->filter(function ($query) use ($request) {
                if($request->search['value'] != null) {
                    $query->where('users.phone','like','%'.$request->search['value'].'%')
                        ->OrWhere('users.name','like','%'.$request->search['value'].'%')
                        ->OrWhere('users.created_at','like','%'.$request->search['value'].'%');
                }
                if($request->country_id != null) {
                    $query->where('users.country_id',$request->country_id);
                }
                if($request->rate != null) {
                    if($request->rate == 1){
                        $query->orderBy('sumrate','desc');
                    }else{
                        $query->orderBy('sumrate','asc');
                    }
                }
                return $query;
            })
            ->addColumn('control','admin.partial.ControlShow')
            ->rawColumns(['addresses','orders','status','control'])->make(true);
    }
}
