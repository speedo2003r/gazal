<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Branch;
use App\Entities\Category;
use App\Entities\City;
use App\Entities\Country;
use App\Entities\Delegate;
use App\Entities\Order;
use App\Http\Controllers\Controller;
use App\Jobs\OrderToDelegates;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /***************** dashboard *****************/
    public function dashboard()
    {
//        $order = Order::find(1);
//        $delegate = Delegate::first();
//        Artisan::call('delegate:notify', [
//            'delegate' => $delegate, 'order' => $order
//        ]);
//        $job = new OrderToDelegates($order);
//        $this->dispatch($job);
        $delegateUsers        = DB::table('delegates')->count();
        $providerUsers        = DB::table('providers')->count();
        $countBranches        = Branch::count();
        $countAdmins     = DB::table('admins')->count();
        $countCountries  = Country::count();
        $countCities  = City::count();
        $countClients    = DB::table('users')->count();
        $countCategories = Category::count();
        return view('admin.dashboard.index',get_defined_vars());
    }

    public function changeLanguage($lang)
    {
        Session::put('applocale', $lang);
        return redirect()->back();
    }

    public function them($them)
    {
        Session()->has('them') ? Session()->forget('them') : '';
        $them == 'dark' ? Session()->put('them', 'dark') : Session()->put('them', 'light');
        return back();
    }
}
