<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Role\Create;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    protected $roleRepo;

    public function __construct(Role $role)
    {
        $this->roleRepo = $role;
    }

    /***************************  get all roles  **************************/
    public function index()
    {
        $roles = $this->roleRepo->get();
        return view('admin.roles.index', compact('roles'));
    }

    /***************************  get all roles  **************************/
    public function create()
    {
        return view('admin.roles.create');
    }

    /***************************  get all roles  **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        $role = $this->roleRepo->create($data);

        $permissions = [];
        foreach ($request->permissions ?? [] as $permission)
            $permissions[]['permission'] = $permission;

        $role->permissions()->createMany($permissions);

        return redirect(route('admin.roles.index'))->with('success', 'تم الاضافه بنجاح');
    }

    /***************************  get all roles  **************************/
    public function edit($id)
    {
        $role = $this->roleRepo->find($id);
        return view('admin.roles.edit', compact('role'));
    }

    /***************************  get all roles  **************************/
    public function update(Request $request, $id)
    {
        $role = $this->roleRepo->find($id);
        $data = array_filter($request->all());
        $role->update($data);
        $role->permissions()->delete();
        $permissions = [];
        foreach ($request->permissions ?? [] as $permission)
            $permissions[]['permission'] = $permission;

        $role->permissions()->createMany($permissions);

        return redirect(route('admin.roles.index'))->with('success', 'تم التحديث بنجاح');
    }

    /***************************  destroy  **************************/
    public function destroy($id)
    {
         $role = $this->roleRepo->find($id);
        $role->delete();
        return redirect(route('admin.roles.index'))->with('success', 'تم الحذف بنجاح');
    }
}
