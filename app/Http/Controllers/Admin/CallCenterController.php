<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Branch;
use App\Entities\Provider;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IProvider;
use App\Http\Requests\Admin\CallCenter\Create;
use App\Http\Requests\Admin\CallCenter\Update;
use App\Traits\NotifyTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use JsValidator;

class CallCenterController extends Controller {
  use UploadTrait;
  use NotifyTrait;
  protected $city, $country, $role, $provider, $branchRepo;

  public function __construct(IBranch $branchRepo, IProvider $provider, ICountry $country, ICity $city) {
    $this->provider   = $provider;
    $this->country    = $country;
    $this->branchRepo = $branchRepo;
    $this->city       = $city;
  }

    public function sellercallcenter(Provider $provider) {
        $branches  = $provider->employees;
        $countries = $this->country->all();
        $cities    = $this->city->findWhere(['country_id' => $provider['country_id']]);
        return view('admin.providers.callcenters.index', compact('countries', 'cities', 'provider', 'branches'));
    }

    public function create(Provider $provider)
    {
        $countries = $this->country->all();
        $cities    = $this->city->findWhere(['country_id' => $provider['country_id']]);
        return view('admin.providers.callcenters.create', compact('countries','provider','cities'));
    }
    public function edit($id)
    {
        $validator = JsValidator::make([
            'lat'        => 'required',
            'lng'        => 'required',
            'phone'      => 'required|numeric|digits_between:9,13|unique:branches,phone,' . $id . ',id,deleted_at,NULL',
            'email'      => 'required|email|max:191|unique:branches,email,' . $id . ',id,deleted_at,NULL',
            'password'   => 'nullable|confirmed|min:3,max:191',
            'country_id' => 'required|exists:countries,id,deleted_at,NULL',
            'city_id'    => 'required|exists:cities,id,deleted_at,NULL',
            'address'    => 'required|max:191',
        ]);
        $countries = $this->country->all();
        $employee = $this->provider->find($id);
        $cities    = $this->city->findWhere(['country_id' => $employee['country_id']]);
        return view('admin.providers.callcenters.edit', compact('validator','countries','employee','cities'));
    }
    public function store(Create $request)
    {
        $data   = $request->all();
        if ($request['image']) {
            $data['avatar'] = $this->uploadFile($request['image'], 'providers');
        }
        $data['active'] = 1;
        $data['user_type'] = Provider::EMPLOYEE;
        $employee = $this->provider->create($data);
        return redirect()->route('admin.providers.callcenter',$employee['parent_id']);
    }
    public function update(Update $request,$id)
    {
        $data   = array_filter($request->all());
        $employee = $this->provider->find($id);
        if (isset($data['image'])) {
            if (null != $employee['avatar']) {
                $this->deleteFile($employee['avatar'], 'providers');
            }
            $data['avatar'] = $this->uploadFile($request['image'], 'providers');
        }
        $employee->update($data);
        return redirect()->route('admin.providers.callcenter',$employee['parent_id']);
    }
}
