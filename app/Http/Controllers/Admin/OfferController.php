<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\OfferDatatable;
use App\Entities\Offer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Offer\Create;
use App\Http\Requests\Admin\Offer\Update;
use App\Repositories\CategoryRepository;
use App\Repositories\CityRepository;
use App\Repositories\CountryRepository;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IOffer;
use App\Repositories\Interfaces\IUser;
use App\Repositories\OfferRepository;
use App\Repositories\UserRepository;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    use ResponseTrait;
    use UploadTrait;
    protected $offerRepo,$countryRepo,$user,$city,$category;

    public function __construct(ICategory $category,ICity $city,IUser $user,IOffer $offerRepo,ICountry $country)
    {
        $this->offerRepo = $offerRepo;
        $this->countryRepo = $country;
        $this->city = $city;
        $this->category = $category;
        $this->user = $user;
    }

    /***************************  get all offers  **************************/
    public function index(OfferDatatable $offerDatatable)
    {
        $cities = $this->city->all();
        $categories = $this->category->findWhere(['parent_id'=>null]);
        return $offerDatatable->render('admin.offers.index',compact('cities','categories'));
    }
    /***************************  get all offers  **************************/
    public function create()
    {
        $countries = $this->countryRepo->all();
        $categories = $this->category->findWhere(['parent_id'=>null]);
        return view('admin.offers.create',compact('countries','categories'));
    }


    /***************************  store offer **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        if($data['type'] == 'item'){
            if($data['item_id'] == null){
                return redirect()->back()->with('danger', 'حقل المنتجات مطلوب');
            }
        }
        if($request->has('image')){
            $data['image'] = $this->uploadFile($request['image'],'offers');
        }
        $this->offerRepo->create($data);
        return redirect()->route('admin.offers.index')->with('success', 'تم الاضافه بنجاح');
    }


    public function edit(Offer $offer)
    {
        $countries = $this->countryRepo->all();
        $categories = $this->category->findWhere(['parent_id'=>null]);
        return view('admin.offers.edit',compact('offer','countries','categories'));
    }
    /***************************  update offer  **************************/
    public function update(Update $request, $id)
    {
        $offer = $this->offerRepo->find($id);
        $data = array_filter($request->all());
        if($data['type'] == 'item'){
            if($data['item_id'] == null){
                return redirect()->back()->with('danger', 'حقل المنتجات مطلوب');
            }
        }
        if($request->has('image')){
            if($offer['image'] != null){
                $this->deleteFile($offer['image'],'offers');
                $data['image'] = $this->uploadFile($request['image'],'offers');
            }
        }
        $this->offerRepo->update(array_filter($data),$offer['id']);
        return redirect()->route('admin.offers.index')->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete offer  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->offerRepo->delete($d);
                }
            }
        }else {
            $role = $this->offerRepo->find($id);
            $this->offerRepo->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

    public function changeActive(Request $request)
    {
        if($request->ajax()){
            $offer = $this->offerRepo->find($request['id']);
            $offer['active'] = !$offer['active'];
            $offer->save();
            return $this->successResponse($offer['active']);
        }
    }
}
