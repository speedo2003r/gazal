<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\HireDatatable;
use App\Entities\Hire;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Hire\Create;
use App\Http\Requests\Admin\Hire\Update;
use App\Repositories\Interfaces\ICarType;
use App\Repositories\Interfaces\IHire;
use App\Repositories\Interfaces\IProvider;
use Illuminate\Http\Request;

class HireController extends Controller
{
    protected $hire,$provider,$carTypes;

    public function __construct(IHire $hire,IProvider $provider,ICarType $carTypes)
    {
        $this->hire = $hire;
        $this->provider = $provider;
        $this->carTypes = $carTypes;
    }

    /***************************  get all hiring delegates  **************************/
    public function index(HireDatatable $datatable)
    {
        return $datatable->render('admin.delegates.hire.index');
    }


    public function create()
    {
        $providers = $this->provider->providers();
        $carTypes = $this->carTypes->all();
        return view('admin.delegates.hire.create',compact('providers','carTypes'));
    }


    /***************************  store provider **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        $hire = $this->hire->create($data);
        $provider = $this->provider->find($request['provider_id']);
        $provider->delegates()->attach($request['delegate_id'],['hire_id'=>$hire['id']]);
        return redirect()->route('admin.delegates.hire')->with('success', 'تم الاضافه بنجاح');
    }



    public function show(Hire $hire)
    {
        return view('admin.delegates.hire.show',compact('hire'));
    }


    public function edit(Hire $hire)
    {
        $providers = $this->provider->providers();
        $carTypes = $this->carTypes->all();
        return view('admin.delegates.hire.edit',compact('hire','providers','carTypes'));
    }

    /***************************  update provider  **************************/
    public function update(Update $request, $id)
    {
        $hire = $this->hire->find($id);
        $data = array_filter($request->all());
        $hire->update($data);
        $provider = $this->provider->find($request['provider_id']);
        $provider->delegates()->sync($request['delegate_id'],['hire_id'=>$hire['id']]);
        return redirect()->route('admin.delegates.hire')->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete provider  **************************/
    public function destroy(Request $request,$id)
    {

        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->hire->delete($d);
                }
            }
        }else {
            $role = $this->hire->find($id);
            $this->hire->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
