<?php
namespace App\Http\Controllers\Admin;
use App\DataTables\OrderProvidersSalesDatatable;
use App\Entities\Branch;
use App\Entities\CarType;
use App\Entities\Category;
use App\Entities\Item;
use App\Entities\Order;
use App\Entities\OrderProduct;
use App\Entities\Visit;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\CountryRepository;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use const Grpc\STATUS_CANCELLED;

class StatisticsController extends Controller{
    protected $countryRepo,$cityRepo;
    public function __construct(ICity $city,ICountry $country)
    {
        $this->countryRepo = $country;
        $this->cityRepo = $city;
    }

    public function sales()
    {
        $acceptedOrders = DB::table('orders')->where('live',1)->where('order_status',Order::STATUS_ACCEPTED)->count();
        $preparedOrders = DB::table('orders')->where('live',1)->where('order_status',Order::STATUS_PREPARED)->count();
        $onwayOrders = DB::table('orders')->where('live',1)->where('order_status',Order::STATUS_ON_WAY)->count();
        $arrivedOrders = DB::table('orders')->where('live',1)->where('order_status',Order::STATUS_ARRIVED_TO_CLIENT)->count();
        $refusedOrders = DB::table('orders')->where('live',1)->where('order_status',Order::STATUS_REFUSED)->count();
        $timeoutOrders = DB::table('orders')->where('live',1)->whereTime('progress_end','<',Carbon::now()->format('H:i:s'))->whereDate('date','<=',Carbon::now()->format('Y-m-d'))->where('order_status',Order::STATUS_CANCELED)->count();
        $canceledOrders = DB::table('orders')->where('live',1)->where('order_status',Order::STATUS_CANCELED)->count();
        $firstOfYear = Carbon::now()->startOfYear();
        $selleds = [
            '1'  => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
            '2'  => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
            '3'  => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
            '4'  => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
            '5'  => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
            '6'  => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
            '7'  => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
            '8'  => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
            '9'  => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
            '10' => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
            '11' => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
            '12' => Order::whereDate('created_date' , '>=' , Carbon::parse($firstOfYear)->format('Y-m-d'))->whereDate('created_date' , '<' , Carbon::parse($firstOfYear->addMonths(1))->format('Y-m-d'))->where('live',1)->count(),
        ];
        $data = [
            'selleds'   => $selleds,
        ];
        $activeDelegates = DB::table('delegates')->where('banned',0)->count();
        $disactiveDelegates = DB::table('delegates')->where('banned',1)->count();
        $dels = DB::table('orders')
            ->leftJoin('delegates','delegates.id','=','orders.delegate_id')
            ->where('delegates.banned',0)
            ->where('orders.live',1);
        $carTypes = CarType::all();
        $vehicles = [];
        foreach ($carTypes as $carType){
            $vehicles[$carType['title']] = $dels->where('delegates.car_type_id',$carType['id'])->count();
        }
        $ordersPreparesCount = DB::table('orders')->where('live',1)->whereNotIn('order_status',[Order::STATUS_REFUSED,Order::STATUS_CANCELED,Order::STATUS_WAITING])->count();
        $income = DB::table('orders')->select(DB::raw('SUM(orders.final_total - orders.coupon_amount) as total'))->whereNotIn('order_status',[Order::STATUS_REFUSED,Order::STATUS_CANCELED,Order::STATUS_WAITING])->where('live',1)->first();
        $cash = DB::table('orders')->where('live',1)->where('pay_type','cash')->whereNotIn('order_status',[Order::STATUS_REFUSED,Order::STATUS_CANCELED,Order::STATUS_WAITING])->count();
        $online = DB::table('orders')->where('live',1)->where('pay_type','online')->whereNotIn('order_status',[Order::STATUS_REFUSED,Order::STATUS_CANCELED,Order::STATUS_WAITING])->count();
        return view('admin.statistics.sales',get_defined_vars());
    }

    public function providers(OrderProvidersSalesDatatable $datatable)
    {
        $cities = $this->cityRepo->all();
        $onwayOrders = DB::table('orders')->where('live',1)
            ->when(request('from') && request('to'),function ($res){
                $res->whereDate('created_date','<=',request('from'));
                $res->whereDate('created_date','>=',request('to'));
            })
            ->when(request('from') && request('to') && request('provider_id'),function ($res){
                $res->whereDate('created_date','<=',request('from'));
                $res->whereDate('created_date','>=',request('to'));
                $res->where('provider_id',request('provider_id'));
            })
            ->when(request('provider_id'),function ($res){
                $res->where('provider_id',request('provider_id'));
            })
            ->where('order_status',Order::STATUS_ON_WAY)->count();
        $arrivedOrders = DB::table('orders')->where('live',1)
            ->when(request('from') && request('to'),function ($res){
                $res->whereDate('created_date','<=',request('from'));
                $res->whereDate('created_date','>=',request('to'));
            })
            ->when(request('from') && request('to') && request('provider_id'),function ($res){
                $res->whereDate('created_date','<=',request('from'));
                $res->whereDate('created_date','>=',request('to'));
                $res->where('provider_id',request('provider_id'));
            })
            ->when(request('provider_id'),function ($res){
                $res->where('provider_id',request('provider_id'));
            })
            ->where('order_status',Order::STATUS_ARRIVED_TO_CLIENT)->count();
        $refusedOrders = DB::table('orders')->where('live',1)
            ->when(request('from') && request('to'),function ($res){
                $res->whereDate('created_date','<=',request('from'));
                $res->whereDate('created_date','>=',request('to'));
            })
            ->when(request('from') && request('to') && request('provider_id'),function ($res){
                $res->whereDate('created_date','<=',request('from'));
                $res->whereDate('created_date','>=',request('to'));
                $res->where('provider_id',request('provider_id'));
            })
            ->when(request('provider_id'),function ($res){
                $res->where('provider_id',request('provider_id'));
            })
            ->where('order_status',Order::STATUS_REFUSED)->count();
        return $datatable->render('admin.statistics.providers',get_defined_vars());
    }
    public function visits(Request $request)
    {
        $days = [];
        $times = [];
        $ios = [];
        $android = [];
        $from = 1;
        $fromdate = $request['from'] ? $request['from'] : Carbon::now()->format('Y-m-d');
        $arr = [];
        $now = Carbon::createFromFormat('Y-m-d', date('Y-m-d',strtotime($fromdate)));
        $monthStartDate = $now->startOfMonth()->format('Y-m-d');
        $monthEndDate = $now->startOfMonth()->format('Y-m-d');
        $period = CarbonPeriod::create($monthStartDate, $monthEndDate);
        $start = new \Carbon\Carbon($monthStartDate);
        $end = new \Carbon\Carbon($monthEndDate);
        $arr['start'] = date('d-m-Y',strtotime($start->subMonth(1)));
        $arr['end'] = date('d-m-Y',strtotime($end->addMonth(1)));

        $to = $start->daysInMonth;
        $format = $start;
        $start = Carbon::parse($format->format('Y-m'))->startOfMonth();
        for($i = $from;$i <= $to;$i++){
            $days[] = $i;
            $ios[] = Visit::where('device_type','ios')->whereMonth('created_at',$now->format('m'))->whereDay('created_at',$i)->count();
            $start->addDay();
        }
        for($i = $from;$i <= $to;$i++){
            $days[] = $i;
            $android[] = Visit::where('device_type','android')->whereMonth('created_at',$now->format('m'))->whereDay('created_at',$i)->count();
            $start->addDay();
        }

        $countriesArrs = Visit::distinct('country_id')->pluck('country_id')->toArray();
        $citiesArrs = Visit::distinct('city_id')->pluck('city_id')->toArray();
        return view('admin.statistics.visits',compact('to','from','citiesArrs','countriesArrs','now','format','arr','times','ios','android','days','start'));
    }

}
