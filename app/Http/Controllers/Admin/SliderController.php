<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\SliderDatatable;
use App\Entities\Slider;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Slider\Create;
use App\Http\Requests\Admin\Slider\Update;
use App\Repositories\CategoryRepository;
use App\Repositories\CityRepository;
use App\Repositories\CountryRepository;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\ISlider;
use App\Repositories\Interfaces\IUser;
use App\Repositories\SliderRepository;
use App\Repositories\UserRepository;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    use ResponseTrait;
    use UploadTrait;
    protected $sliderRepo,$countryRepo,$user,$city,$category;

    public function __construct(ICategory $category,ICity $city,IUser $user,ISlider $slider,ICountry $country)
    {
        $this->sliderRepo = $slider;
        $this->countryRepo = $country;
        $this->city = $city;
        $this->category = $category;
        $this->user = $user;
    }

    /***************************  get all providers  **************************/
    public function index(SliderDatatable $sliderDatatable)
    {
        return $sliderDatatable->render('admin.sliders.index');
    }

    public function create()
    {
        $countries = $this->countryRepo->all();
        $categories = $this->category->findWhere(['parent_id'=>null]);
        return view('admin.sliders.create',compact('countries','categories'));
    }

    /***************************  store provider **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        if($data['type'] == 'item'){
            if($data['item_id'] == null){
                return redirect()->back()->with('danger', 'حقل المنتجات مطلوب');
            }
        }
        if($request->has('image')){
            $data['image'] = $this->uploadFile($request['image'],'sliders');
        }
        $this->sliderRepo->create($data);
        return redirect()->route('admin.sliders.index')->with('success', 'تم الاضافه بنجاح');
    }


    public function edit(Slider $slider)
    {
        $countries = $this->countryRepo->all();
        $categories = $this->category->findWhere(['parent_id'=>null]);
        return view('admin.sliders.edit',compact('slider','countries','categories'));
    }
    /***************************  update provider  **************************/
    public function update(Update $request, $id)
    {
        $slider = $this->sliderRepo->find($id);
        $data = array_filter($request->all());
        if($data['type'] == 'item'){
            if($data['item_id'] == null){
                return redirect()->back()->with('danger', 'حقل المنتجات مطلوب');
            }
        }
        if($request->has('image')){
            if($slider['image'] != null){
                $this->deleteFile($slider['image'],'sliders');
                $data['image'] = $this->uploadFile($request['image'],'sliders');
            }
        }
        $this->sliderRepo->update(array_filter($data),$slider['id']);
        return redirect()->route('admin.sliders.index')->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete provider  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->sliderRepo->delete($d);
                }
            }
        }else {
            $role = $this->sliderRepo->find($id);
            $this->sliderRepo->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

    public function changeActive(Request $request)
    {
        if($request->ajax()){
            $slider = $this->sliderRepo->find($request['id']);
            $slider['active'] = !$slider['active'];
            $slider->save();
            return $this->successResponse($slider['active']);
        }
    }
}
