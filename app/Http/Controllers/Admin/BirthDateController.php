<?php

namespace App\Http\Controllers\Admin;

use App\Entities\BlockUser;
use App\Entities\Branch;
use App\Entities\Delegate;
use App\Entities\Provider;
use App\Entities\UserAddress;
use App\Entities\Wallet;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Client\Create;
use App\Http\Requests\Admin\Client\Update;
use App\Models\User;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IUser;
use App\Traits\CountryCodeTrait;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class BirthDateController extends Controller
{
    use NotifyTrait;
    use ResponseTrait;
    use UploadTrait;
    use CountryCodeTrait;
    protected $user,$provider,$delegate, $country,$city;

    public function __construct(IUser $user,ICountry $country,ICity $city)
    {
        $this->user = $user;
        $this->country = $country;
        $this->city = $city;
    }

    /***************************  get all users  **************************/
    public function index()
    {
        $clients = DB::table('users')->whereMonth('birth_date',Carbon::now()->format('m'))->whereDay('birth_date',Carbon::now()->format('d'))->get();
        return view('admin.birthDate.index',get_defined_vars());
    }

    /***************************  get all users  **************************/
    public function notify(User $user)
    {
        return view('admin.birthDate.notify',compact('user'));
    }

    public function sendNotify(Request $request)
    {
        $this->validate($request,[
            'title_ar' => 'required|max:255',
            'title_en' => 'required|max:255',
            'message_ar' => 'required|max:2000',
            'message_en' => 'required|max:2000',
        ]);
        $user = $this->user->find($request['user_id']);
        $title_ar = $request->title_ar;
        $title_en = $request->title_en;
        $message_ar = $request->message_ar;
        $message_en = $request->message_en;
        #send FCM
        $this->send_notify($user,$title_ar,$title_en,$message_ar, $message_en,'admin');
        return back()->with('success','تم الارسال بنجاح');
    }

}
