<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ComplaintDatatable;
use App\Http\Controllers\Controller;
use App\Repositories\ComplaintRepository;
use App\Repositories\Interfaces\IComplaint;
use Illuminate\Http\Request;

class ComplaintController extends Controller
{
    protected $complaint;

    public function __construct(IComplaint $complaint)
    {
        $this->complaint = $complaint;
    }

    /***************************  get all contacts  **************************/
    public function index(ComplaintDatatable $suggestDatatable)
    {
        $complaints = $this->complaint->all();
        return $suggestDatatable->render('admin.complaints.index', compact('complaints'));
    }

    /***************************  delete contact  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->complaint->delete($d);
                }
            }
        }else {
            $role = $this->complaint->find($id);
            $this->complaint->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
