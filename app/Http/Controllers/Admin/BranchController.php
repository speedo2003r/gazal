<?php

namespace App\Http\Controllers\Admin;

use App\Entities\BlockUser;
use App\Entities\Branch;
use App\Entities\BranchDate;
use App\Entities\Provider;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Branch\CreateBranch;
use App\Http\Requests\Admin\Branch\updateBranch;
use App\Models\Role;
use App\Repositories\BranchRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CityRepository;
use App\Repositories\CountryRepository;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IProvider;
use App\Repositories\ProviderRepository;
use App\Traits\NotifyTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use JsValidator;
use Yajra\DataTables\DataTables;

class BranchController extends Controller {
  use UploadTrait, NotifyTrait;
  protected $city, $country, $role, $provider, $category, $branchRepo;

  public function __construct(IBranch $branchRepo, ICategory $category, IProvider $provider, ICountry $country, ICity $city, Role $role) {
    $this->provider   = $provider;
    $this->category   = $category;
    $this->country    = $country;
    $this->branchRepo = $branchRepo;
    $this->role       = $role;
    $this->city       = $city;
  }

  public function sellerbranches(Provider $provider) {
    $branches  = $provider->branches;
    $countries = $this->country->all();
    $cities    = $this->city->findWhere(['country_id' => $provider['country_id']]);
    return view('admin.providers.branches.index', compact('countries', 'cities', 'provider', 'branches'));
  }

  public function branchesCreate() {
    $countries = $this->country->all();
    $providers = $this->provider->providers();
    return view('admin.providers.branches.mainCreate', compact('countries','providers'));
  }
  // branches dates page
  public function branchesDates() {
    $providers = $this->provider->providers();
    return view('admin.providers.branches.dates',compact('providers'));
  }
//  get data for datatable
  public function getBranchData(Request $request,Branch $branch)
    {
        $items = $branch->query()->latest();
        return DataTables::of($items)
            ->addColumn('currentCity','{{ $current_city }}')
            ->addColumn('normalDate',function ($query){
                return '<a href="'.route('admin.providers.branchesTypeDate',['normal',$query]).'" class="btn btn-outline-success">المواعيد العاديه</a>';
            })
            ->addColumn('ramadanDate',function ($query){
                return '<a href="'.route('admin.providers.branchesTypeDate',['ramadan',$query]).'" class="btn btn-outline-success">مواعيد رمضان</a>';
            })
            ->addColumn('control',function ($query){
                return '<a href="'.route('admin.branches.branchChangeStatus',$query).'" class="btn btn-outline-success">تغيير الحاله</a>';
            })
            ->filter(function ($query) use ($request) {
                if($request->type != null) {
                    $type = $request['type'] == 'open' ? 0 : 1;
                    $query->where('banned',$type);
                }
                if($request->provider_id != null) {
                    $query->where('provider_id',$request->provider_id);
                }
                return $query;
            })
            ->rawColumns(['control','currentCity','normalDate','ramadanDate'])->make(true);
    }

    public function branchesTypeDate($type,Branch $branch)
    {
        if($type == 'normal'){
            $typeValue = BranchDate::NORMAL;
            $branchDates = $this->branchRepo->getBranchesDates($branch['id'],$typeValue);
        }else{
            $typeValue = BranchDate::RAMADAN;
            $branchDates = $this->branchRepo->getBranchesDates($branch['id'],$typeValue);
        }
        return view('admin.providers.branches.datePage',compact('branch','branchDates','typeValue'));
    }



  public function sellerbrancheCreate(Provider $provider) {
    $countries = $this->country->all();
    $cities    = $this->city->findWhere(['country_id' => $provider['country_id']]);
    return view('admin.providers.branches.create', compact('countries', 'cities', 'provider'));
  }

  public function editbranch(Branch $branch) {
    $validator = JsValidator::make([
      'lat'        => 'required',
      'lng'        => 'required',
      'phone'      => 'required|numeric|digits_between:9,13|unique:branches,phone,' . $branch->id . ',id,deleted_at,NULL',
      'email'      => 'required|email|max:191|unique:branches,email,' . $branch->id . ',id,deleted_at,NULL',
      'password'   => 'nullable|confirmed|min:3,max:191',
      'title_ar'   => 'required|max:191',
      'title_en'   => 'required|max:191',
      'country_id' => 'required|exists:countries,id,deleted_at,NULL',
      'city_id'    => 'required|exists:cities,id,deleted_at,NULL',
      'address'    => 'required|max:191',
    ]);
    $countries = $this->country->all();
    $cities    = $this->city->findWhere(['country_id' => $branch['country_id']]);
    return view('admin.providers.branches.edit', compact('validator', 'countries', 'cities', 'branch'));
  }

  public function postbranch(CreateBranch $request) {
    $data   = $request->all();
    $branch = $this->branchRepo->find($request['branch_id']);

    $exists = Branch::where('provider_id', $request['provider_id'])->where('user_type', Branch::BRANCH)->where('city_id', $request['city_id'])->exists();
    if ($exists) {
      return back()->with('danger', 'يوجد بالفعل فرع في هذه المدينه');
    }

    if ($request['image']) {
      $data['avatar'] = $this->uploadFile($request['image'], 'branches');
    }

    $data['appear'] = 1;

    if ($branch) {
      $branch->update($data);
    } else {
      $branch = $this->branchRepo->create($data);
    }

    $branch->zones()->attach($request->zones_ids);

    return redirect()->route('admin.providers.branches', $branch->user)->with('success', 'تم الاضافه بنجاح');
  }

  public function showbranch(Branch $branch) {
    return view('admin.providers.branches.show', compact('branch'));
  }

  public function updatebranch(updateBranch $request, $id) {
    $data   = array_filter($request->all());
    $branch = $this->branchRepo->find($id);

    $exists = Branch::where('provider_id', $request['provider_id'])->where('user_type', Branch::BRANCH)->where('city_id', $request['city_id'])->exists();
    if ($exists && $branch['city_id'] != $request['city_id']) {
      return back()->with('danger', 'يوجد بالفعل فرع في هذه المدينه');
    }

    if (isset($data['image'])) {
      if (null != $branch['avatar']) {
        $this->deleteFile($branch['avatar'], 'branches');
      }
      $data['avatar'] = $this->uploadFile($request['image'], 'branches');
    }

    $branch->update($data);
    $branch->zones()->sync($request->zones_ids);
    return redirect()->route('admin.providers.branches', $branch->user)->with('success', 'تم التحديث بنجاح');
  }

  public function storebranchdate(Request $request) {
    $this->validate($request, [
      'timefrom' => 'required|before:timeto',
      'timeto'   => 'required|after:timefrom',
    ]);
    foreach ($request['work_days'] as $day) {
      $date = BranchDate::where('day', $day)->where('branch_id', $request['branch_id'])->where('shift', $request['shift'])->first();
      if ($date) {
        $date->update([
          'timefrom' => $request['timefrom'],
          'timeto'   => $request['timeto'],
        ]);
      } else {
        BranchDate::create([
          'timefrom'  => $request['timefrom'],
          'timeto'    => $request['timeto'],
          'day'       => $day,
          'type'       => $request['type'],
          'shift'     => $request['shift'],
          'branch_id' => $request['branch_id'],
        ]);
      }
    }
    return redirect()->back()->with('success', 'تم الحفظ بنجاح');
  }

  public function deletedatebranches(Request $request) {
    $dates = BranchDate::where('day', $request['date'])->get();
    foreach ($dates as $date) {
      $date->delete();
    }
    return redirect()->back()->with('success', 'تم الحذف بنجاح');
  }

  public function deletebranches(Request $request, $id) {
    $role = $this->branchRepo->find($id);
    $this->branchRepo->delete($role['id']);
    return redirect()->back()->with('success', 'تم الحذف بنجاح');
  }

  public function branchOpenClosed()
  {
      $providers = $this->provider->providers();
      return view('admin.providers.branches.openClosed',compact('providers'));
  }

  public function branchChangeStatus(Branch $branch)
  {
      return view('admin.providers.branches.branchChangeStatus',compact('branch'));
  }

    public function block(Request $request)
    {
        $this->validate($request,[
            'notes' => 'required|string|max:2000',
        ],[],[
            'notes' => awtTrans('السبب')
        ]);
        BlockUser::create([
            'notes' => $request['notes'],
            'modal_id' => $request['user_id'],
            'modal_type' => Branch::class,
        ]);
        $user = Branch::find($request['user_id']);
        $user->banned = 1;
        $user->save();
        return back()->with('success','تم الحظر بنجاح');
    }
    public function unblock(Request $request)
    {
        $this->validate($request,[
            'notes' => 'required|string|max:2000',
        ],[],[
            'notes' => awtTrans('السبب')
        ]);
        BlockUser::create([
            'notes' => $request['notes'],
            'modal_id' => $request['user_id'],
            'modal_type' => Branch::class,
            'status' => 2
        ]);
        $user = Branch::find($request['user_id']);
        $user->banned = 0;
        $user->save();
        return back()->with('success','تم الغاء الحظر بنجاح');
    }

    public function changeSchedule(Request $request)
    {
        $this->validate($request,[
            'schedule' => 'required'
        ],[],[
            'schedule' => __('admin.Schedule')
        ]);
        $branch = $this->branchRepo->find($request['branch_id']);
        $branch->schedule_switch = $request['schedule'];
        $branch->save();
        return back()->with('success','تم الارسال بنجاح');
    }
}
