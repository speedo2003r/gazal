<?php

namespace App\Http\Controllers\Admin;

use App\Entities\BlockUser;
use App\Entities\Branch;
use App\Entities\Delegate;
use App\Entities\Provider;
use App\Entities\UserAddress;
use App\Entities\Wallet;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Client\Create;
use App\Http\Requests\Admin\Client\Update;
use App\Jobs\NotifyFcm;
use App\Models\User;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IUser;
use App\Repositories\Interfaces\IZone;
use App\Traits\CountryCodeTrait;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class ClientNotificationController extends Controller
{
    use NotifyTrait;
    use ResponseTrait;
    use UploadTrait;
    use CountryCodeTrait;
    protected $user,$provider,$delegate, $country,$city,$zone;

    public function __construct(IZone $zone,IUser $user,ICountry $country,ICity $city)
    {
        $this->user = $user;
        $this->zone = $zone;
        $this->country = $country;
        $this->city = $city;
    }

    /***************************  get all users  **************************/
    public function index()
    {
        $clients = DB::table('users')->latest()->get();
        $zones = $this->zone->all();
        return view('admin.clientNotifications.index',compact('clients','zones'));
    }

    public function send(Request $request)
    {
        $this->validate($request,[
            'time' => 'required',
            'zone_id' => 'nullable|exists:zones,id',
            'title_ar' => 'required|max:255',
            'title_en' => 'required|max:255',
            'message_ar' => 'required|max:2000',
            'message_en' => 'required|max:2000',
        ]);
        if(isset($request['clients']) &&  count($request['clients']) > 0){
            $users = $this->user->findWhereIn('id',$request['clients']);
        }else{
            if(isset($request['zone_id']) && $request['zone_id'] != null){
                $zone = $this->zone->find($request['zone_id']);
                $users = $zone->users;
            }else{
                $users = $this->user->all();
            }
        }
        $time = Carbon::createFromTimeString(date('H:i:s',strtotime($request['time'])));
        $start_of_day = Carbon::createFromTimeString(date('H:i:s',strtotime($request['time'])))->startOfDay();
        $total_minutes = $time->diffInMinutes($start_of_day);
        if(date('H:i:s',strtotime($request['time'])) < Carbon::now()->format('H:i:s')){
            $total_minutes += 1440;
        }
        foreach ($users as $user) {
            $title_ar = $request->title_ar;
            $title_en = $request->title_en;
            $message_ar = $request->message_ar;
            $message_en = $request->message_en;
            #send FCM
            $job = (new NotifyFcm(auth('admin')->user(),$user,$message_ar,$message_en));
//            if($user){
//                $data['title'] = app()->getLocale() == 'ar' ? $title_ar: $title_en;
//                $data['body'] = app()->getLocale() == 'ar' ? $message_ar: $message_en;
//                if($user->Devices){
//                    foreach ($user->Devices as $device) {
//                        if($device->device_id){
//                            $this->send_fcm($device->device_id, $data, $device->device_type);
//                        }
//                    }
//                }
//            }
            dispatch($job)->delay(now()->addMinutes($total_minutes));
        }
        return back()->with('success','تم الارسال بنجاح');
    }

}
