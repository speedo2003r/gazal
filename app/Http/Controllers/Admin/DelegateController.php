<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\DelegateBlockedDatatable;
use App\DataTables\DelegateDatatable;
use App\Entities\BlockUser;
use App\Entities\Delegate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Delegate\Create;
use App\Http\Requests\Admin\Delegate\Update;
use App\Models\User;
use App\Repositories\CarTypeRepository;
use App\Repositories\CityRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\CountryRepository;
use App\Repositories\DelegateRepository;
use App\Repositories\Interfaces\ICarType;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICompany;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IDelegate;
use App\Repositories\UserRepository;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use JsValidator;
use Yajra\DataTables\DataTables;

class DelegateController extends Controller
{
    use NotifyTrait;
    use ResponseTrait;
    use UploadTrait;
    protected $company,$delegate, $country,$city,$carType;

    public function __construct(ICompany $company,ICarType $carType,IDelegate $delegate,ICountry $country ,ICity $city)
    {
        $this->delegate = $delegate;
        $this->country = $country;
        $this->city = $city;
        $this->company = $company;
        $this->carType = $carType;
    }

    /***************************  get all providers  **************************/
    public function index(DelegateDatatable $clientDatatable)
    {
        return $clientDatatable->render('admin.delegates.index');
    }
    public function create()
    {
        $countries = $this->country->all();
        $carTypes = $this->carType->all();
        $companies = $this->company->all();
        return view('admin.delegates.create',compact('companies','countries','carTypes'));
    }
    public function edit(Delegate $delegate)
    {
        $validator = JsValidator::make([
            'name' => 'required|max:191',
            'phone'      => 'required|numeric|digits_between:9,13|unique:delegates,phone,'.$delegate['id'].',id,deleted_at,NULL',
            'email'      => 'required|email|max:191|unique:delegates,email,'.$delegate['id'].',id,deleted_at,NULL',
            'password'   => 'nullable|max:191|confirmed',
            'birth_date'   => 'required',
            'image'      => 'nullable|mimes:jpeg,jpg,png',
            'car_type_id'    => 'required|exists:car_types,id,deleted_at,NULL',
            'country_id'    => 'required|exists:countries,id,deleted_at,NULL',
            'city_id'    => 'required|exists:cities,id,deleted_at,NULL',
            'company_id'    => 'required|exists:companies,id,deleted_at,NULL',
            'lat'    => 'required',
            'lng'    => 'required',
            'address'    => 'required',
            'commission' => 'required',
            'commission_status' => 'required',
            'max_dept' => 'required',
        ]);
        $countries = $this->country->all();
        $companies = $this->company->all();
        $carTypes = $this->carType->all();
        return view('admin.delegates.edit', compact('companies','validator','delegate','countries','carTypes'));
    }

    /***************************  show delegate **************************/
    public function show(Delegate $delegate)
    {
        return view('admin.delegates.show',compact('delegate'));
    }
    /***************************  store delegate **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->except('image'));
        if($request->has('image')){
            $data['avatar'] = $this->uploadFile($request['image'],'delegates');
        }
        $this->delegate->create($data);
        return redirect()->route('admin.delegates.index')->with('success', 'تم الاضافه بنجاح');
    }


    /***************************  update provider  **************************/
    public function update(Update $request,$id)
    {
        $delegate = $this->delegate->find($id);
        if($request->has('image')){
            if($delegate['avatar'] != null || $delegate['avatar'] != '/default.png'){
                $this->deleteFile($delegate['avatar'],'delegates');
            }
            $request->request->add(['avatar'=>$this->uploadFile($request['image'],'delegates')]);
        }
        $this->delegate->update(array_filter($request->except('image')),$delegate['id']);
        return redirect()->route('admin.delegates.index')->with('success', 'تم التحديث بنجاح');
    }


    public function sendnotifyuser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required|max:255',
        ]);
        #error response
        if ($validator->fails())
            return response()->json(['value' => 0, 'msg' => $validator->errors()->first()]);

        if ($request->type == 'all') $users = User::where('user_type', $request['notify_type'])->get();
        else $users = User::whereId($request->id)->get();

        foreach ($users as $user) {
            $message = $request->message;
            #send FCM
            $this->send_notify($user->id, $message, $message);
        }
        return $this->ApiResponse('success', 'تم الارسال بنجاح');
    }
    public function images(Delegate $delegate)
    {
        return view('admin.delegates.images.index', compact('delegate'));
    }
    public function storeImages(Request $request,Delegate $delegate)
    {
        if($request->has('_licence_image')){
            $this->deleteFile($delegate['licence_image'],'delegates');
            $logo = $this->uploadFile($request['_licence_image'],'delegates');
            $request->request->add(['licence_image'=>$logo]);
        }
        if($request->has('_id_avatar')){
            $this->deleteFile($delegate['id_avatar'],'delegates');
            $logo = $this->uploadFile($request['_id_avatar'],'delegates');
            $request->request->add(['id_avatar'=>$logo]);
        }
        if($request->has('_car_form_image')){
            $this->deleteFile($delegate['car_form_image'],'delegates');
            $logo = $this->uploadFile($request['_car_form_image'],'delegates');
            $request->request->add(['car_form_image'=>$logo]);
        }
        if($request->has('_car_front_image')){
            $this->deleteFile($delegate['car_front_image'],'delegates');
            $logo = $this->uploadFile($request['_car_front_image'],'delegates');
            $request->request->add(['car_front_image'=>$logo]);
        }
        if($request->has('_car_back_image')){
            $this->deleteFile($delegate['car_back_image'],'delegates');
            $logo = $this->uploadFile($request['_car_back_image'],'delegates');
            $request->request->add(['car_back_image'=>$logo]);
        }
        $delegateRequests = $request->all();
        $this->delegate->update($delegateRequests,$delegate['id']);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

    public function block(Request $request)
    {
        $this->validate($request,[
            'notes' => 'required|string|max:2000',
        ],[],[
            'notes' => awtTrans('السبب')
        ]);
        BlockUser::create([
            'notes' => $request['notes'],
            'modal_id' => $request['user_id'],
            'modal_type' => Delegate::class,
        ]);
        $user = Delegate::find($request['user_id']);
        $user->banned = 1;
        $user->save();
        return back()->with('success','تم الحظر بنجاح');
    }
    public function unblock(Request $request)
    {
        $user = Delegate::find($request['user_id']);
        $user->banned = 0;
        $user->save();
        return back()->with('success','تم الغاء الحظر بنجاح');
    }

    public function addToWallet(Request $request)
    {
        $this->validate($request,[
            'amount' => 'required'
        ],[],[
            'amount' => awtTrans('رصيد')
        ]);
        $user = $this->delegate->find($request['user_id']);
        $user->wallet = $request['amount'];
        $user->save();
        return back()->with('success','تم الاضافة الي المحفظه بنجاح');
    }

    public function rating()
    {
        $countries = $this->country->all();
        return view('admin.delegates.rating',compact('countries'));
    }
    public function blocked(DelegateBlockedDatatable $datatable)
    {
        return $datatable->render('admin.delegates.blocked');
    }
    public function getFilterData(Request $request,Delegate $delegate)
    {
        $items = $delegate->query()
            ->select('delegates.*',DB::raw('SUM(review_rates.rate) as sumrate'),DB::raw('COUNT(review_rates.id) as countrate'))
            ->leftJoin('review_rates','review_rates.rateable_id','=','delegates.id')
            ->where('review_rates.rateable_type',Delegate::class)
            ->groupBy('delegates.id')
            ->latest();
        return DataTables::of($items)
            ->addColumn('rate',function ($query){
                return '<div class="Stars" style="--rating: '.($query->sumrate > 0 ? (round($query->sumrate / $query->countrate,2)) : '0').';">';
            })
            ->filter(function ($query) use ($request) {
                if($request['rate']){
                    if($request['rate'] == 1){
                        $query->orderBy('sumrate','desc');
                    }else{
                        $query->orderBy('sumrate','asc');
                    }
                }
                if($request['city_id']){
                    $query->where('city_id',$request['city_id']);
                }
                return $query;
            })
            ->rawColumns(['rate'])->make(true);
    }
}
