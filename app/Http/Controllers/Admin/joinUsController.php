<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\DelegateJoinUsDatatable;
use App\DataTables\EmployeeJoinUsDatatable;
use App\DataTables\JoinUsDatatable;
use App\Http\Controllers\Controller;
use App\Repositories\DelegateJoinUsRepository;
use App\Repositories\EmployeeJoinUsRepository;
use App\Repositories\Interfaces\IDelegateJoinUs;
use App\Repositories\Interfaces\IEmployeeJoinUs;
use App\Repositories\JoinUsRepository;
use Illuminate\Http\Request;

class joinUsController extends Controller
{
    protected $join,$delegateJoin,$porviderJoin,$employeeJoin;

    public function __construct(IDelegateJoinUs $delegateJoin,IEmployeeJoinUs $employeeJoin)
    {
        $this->delegateJoin = $delegateJoin;
        $this->employeeJoin = $employeeJoin;
    }

    /***************************  get all contacts  **************************/
    public function delegeteJoinsIndex(DelegateJoinUsDatatable $joinDatatable)
    {
        $joins = $this->delegateJoin->all();
        return $joinDatatable->render('admin.delegeteJoins.index', compact('joins'));
    }

    /***************************  delete contact  **************************/
    public function delegeteJoinsDestroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->delegateJoin->delete($d);
                }
            }
        }else {
            $role = $this->delegateJoin->find($id);
            $this->delegateJoin->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }
    /***************************  get all contacts  **************************/
    public function employeeJoinsIndex(EmployeeJoinUsDatatable $joinDatatable)
    {
        $joins = $this->employeeJoin->all();
        return $joinDatatable->render('admin.EmployeeJoins.index', compact('joins'));
    }

    /***************************  delete contact  **************************/
    public function employeeJoinsDestroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->employeeJoin->delete($d);
                }
            }
        }else {
            $role = $this->employeeJoin->find($id);
            $this->employeeJoin->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
