<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Type\Create;
use App\Http\Requests\Admin\Type\Update;
use App\Repositories\Interfaces\IRatingList;
use App\Repositories\RatingListRepository;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class RatingListController extends Controller
{
    use UploadTrait;
    protected $ratingList;

    public function __construct(IRatingList $ratingList)
    {
        $this->ratingList = $ratingList;
    }

    /***************************  get all providers  **************************/
    public function index()
    {
        $ratingLists = $this->ratingList->all();
        return view('admin.ratingList.index', compact('ratingLists'));
    }


    /***************************  store provider **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        $this->ratingList->create($data);
        return redirect()->back()->with('success', 'تم الاضافه بنجاح');
    }


    /***************************  update provider  **************************/
    public function update(Update $request, $id)
    {
        $type = $this->ratingList->find($id);
        $data = array_filter($request->all());
        $this->ratingList->update($data,$type['id']);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete provider  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->ratingList->delete($d);
                }
            }
        }else {
            $role = $this->ratingList->find($id);
            $this->ratingList->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
