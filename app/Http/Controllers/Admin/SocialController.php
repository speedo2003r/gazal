<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\ISetting;
use App\Repositories\Interfaces\ISocial;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class SocialController extends Controller
{
    use UploadTrait;
    protected $socialRepo,$settingRepo;

    public function __construct(ISocial $social,ISetting $setting)
    {
        $this->socialRepo = $social;
        $this->settingRepo = $setting;
    }

    public function clients()
    {
        return view('admin.socials.clients');
    }
    public function providers()
    {
        return view('admin.socials.providers');
    }
    public function delegates()
    {
        return view('admin.socials.delegates');
    }
    public function companies()
    {
        return view('admin.socials.companies');
    }
    public function providersPortal()
    {
        return view('admin.socials.providersPortal');
    }
    /***************************  update social  **************************/
    public function update(Request $request)
    {
        $generalData = $request['keys'];
        $this->settingRepo->updateall($generalData);
        $data = $request['socials'];
        $this->socialRepo->updateall($data);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }
    /***************************  update social  **************************/
//    public function store(Request $request)
//    {
//        $this->socialRepo->store($request->all());
//        return redirect()->back()->with('success', 'تم التحديث بنجاح');
//    }
}
