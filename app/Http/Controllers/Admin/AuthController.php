<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    /***************** show login form *****************/
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    /**************** show login form *****************/
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required|email|exists:admins,email,deleted_at,NULL',
            'password'  => 'required|string',
        ]);
        $remember = $request->remember ? true : false;
        if (auth()->guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $remember)) {
            return redirect()->route('admin.dashboard');
        }else{
            return redirect()->route('admin.show.login')->withErrors('تحقق من صحة البيانات المدخلة');
        }

    }

    /**************** logout *****************/
    public function logout()
    {
        auth()->guard()->logout();
        session()->invalidate();
        session()->regenerateToken();
        return redirect(route('admin.login'));
    }



}
