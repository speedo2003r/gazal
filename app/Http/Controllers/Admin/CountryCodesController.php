<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CountryCodesController extends Controller
{

    /***************************  get all providers  **************************/
    public function index()
    {
        $countries = DB::table('all_countries')->get();
        return view('admin.countryCodes.index', compact('countries'));
    }



    /***************************  update provider  **************************/
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'lang_name' => 'required',
            'example' => 'required',
        ]);
        DB::table('all_countries')->where('id',$id)->update([
            'lang_name' => $request['lang_name'],
            'example' => $request['example'],
        ]);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

}
