<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\EmployeeDatatable;
use App\Entities\Employee;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Employee\Create;
use App\Http\Requests\Admin\Employee\Update;
use App\Repositories\Interfaces\IEmployee;
use App\Traits\CountryCodeTrait;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use JsValidator;

class EmployeeController extends Controller
{
    use NotifyTrait;
    use ResponseTrait;
    use UploadTrait;
    use CountryCodeTrait;
    protected $employee;

    public function __construct(IEmployee $employee)
    {
        $this->employee = $employee;
    }

    /***************************  get all users  **************************/
    public function index(EmployeeDatatable $datatable)
    {
        return $datatable->render('admin.employees.index');
    }
    public function create()
    {
        return view('admin.employees.create');
    }
    public function store(Create $request)
    {
        $data = array_filter($request->except('image'));
        if($request->has('image')){
            $data['avatar'] = $this->uploadFile($request['image'],'employees');
        }
        $this->employee->create($data);
        return redirect()->route('admin.employees.index')->with('success', 'تم الاضافه بنجاح');
    }
    public function edit(Employee $employee)
    {
        $validator = JsValidator::make([
            'avatar' => 'nullable|mimes:jpg,png,jpeg',
            'name' => 'required|max:191',
            'phone'      => "required|numeric|digits_between:9,13|unique:employees,phone,{$employee['id']},id",
            'email'      => "required|email|max:191|unique:employees,email,{$employee['id']},id",
            'address'      => 'required',
        ]);
        return view('admin.employees.edit',compact('employee','validator'));
    }
    public function update(Update $request, $id)
    {
        $client = $this->employee->find($id);
        if($request->has('image')){
            if($client['avatar'] != null || $client['avatar'] != '/default.png'){
                $this->deleteFile($client['avatar'],'employees');
            }
            $request->request->add(['avatar'=>$this->uploadFile($request['image'],'employees')]);
        }
        $this->employee->update(array_filter($request->except('image')),$client['id']);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

}
