<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ClientWalletDatatable;
use App\Entities\Gift;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Interfaces\IProvider;
use App\Repositories\Interfaces\IUser;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    use ResponseTrait;
    use UploadTrait;
    protected $user,$provider;

    public function __construct(IUser $user,IProvider $provider)
    {
        $this->user = $user;
        $this->provider = $provider;
    }

    /***************************  get all providers  **************************/
    public function index(ClientWalletDatatable $datatable)
    {
        return $datatable->render('admin.clientWallet.index');
    }
    public function addToWallet(User $user)
    {
        $providers = $this->provider->providers();
        return view('admin.clientWallet.addToWallet',compact('user','providers'));
    }
    public function addGift(Request $request)
    {
        $this->validate($request,[
            'amount' => 'required|min:1|max:20',
            'notes' => 'required|string|max:2000',
            'expire_date' => 'required|after:'.Carbon::now()->format('Y-m-d'),
        ],[],[
            'amount' => __('balance'),
            'notes' => __('reason'),
            'expire_date' => __('ExpiryDate'),
        ]);
        $user = User::find($request['user_id']);
        $user->wallet += $request['amount'];
        $user->save();
        $gift = Gift::create([
            'amount' => $request['amount'],
            'notes' => $request['notes'],
            'user_id' => $request['user_id'],
            'expire_date' => $request['expire_date'],
        ]);
        if(isset($request['providers']) && count($request['providers']) > 0){
            $gift->providers()->attach($request['providers']);
        }
        return back()->with('success','تم الاضافه بنجاح');
    }

    public function addToAllUser(Request $request)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $user = $this->user->find($d);
                    $user->wallet += $request['amount'];
                    $user->save();
                }
            }
            return back()->with('success','تم الارسال بنجاح');
        }
    }
}
