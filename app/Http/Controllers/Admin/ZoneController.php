<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Zone\Create;
use App\Http\Requests\Admin\Zone\Update;
use App\Repositories\CityRepository;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\IZone;
use App\Repositories\ZoneRepository;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ZoneController extends Controller {
  protected $cityRepo, $zoneRepo;

  public function __construct(IZone $zone, ICity $city) {
    $this->zoneRepo = $zone;
    $this->cityRepo = $city;
  }

  public function index() {
    $cities = $this->cityRepo->all();
    $zones  = $this->zoneRepo->all();
    return view('admin.zones.index', compact('zones', 'cities'));
  }

  public function getZoneMap($id) {
    $zone           = $this->zoneRepo->find($id);
    $cityOtherZones = $this->zoneRepo->cityOtherZones($zone);
    return view('admin.zones.map', compact('zone', 'cityOtherZones'));
  }

  public function updateZoneMap(Request $request) {
    $validate = Validator::make($request->all(), [
      'id'       => 'required',
      'geometry' => 'required',
    ]);
    if ($validate->fails()) {
      return redirect()->back()->with('danger', $validate->errors()->first());
    }

    # convert request geometry to polygon
    $p_geometry = Polygon::fromJson($request->geometry);

    $zone = $this->zoneRepo->find($request->id);
    $zone->update([
      'geometry'    => $request->geometry,
      'p_geometry'  => $p_geometry,
    ]);

    return redirect()->back()->with('success', 'تم الاضافه بنجاح');
  }

  public function store(Create $request) {
    $data = array_filter($request->all());
    $this->zoneRepo->create($data);
    return redirect()->back()->with('success', 'تم الاضافه بنجاح');
  }

  public function update(Update $request, $id) {
    $data = array_filter($request->all());
    $this->zoneRepo->update($data, $id);
    return redirect()->back()->with('success', 'تم التحديث بنجاح');
  }

  public function destroy(Request $request, $id) {

    if (isset($request['data_ids'])) {
      $data = explode(',', $request['data_ids']);
      foreach ($data as $d) {
        if ("" != $d) {
          $this->zoneRepo->delete($d);
        }
      }
    } else {
      $this->zoneRepo->delete($id);
    }

    return redirect()->back()->with('success', 'تم الحذف بنجاح');
  }

}
