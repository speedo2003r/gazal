<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CouponDatatable;
use App\Entities\Coupon;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Coupon\Create;
use App\Http\Requests\Admin\Coupon\Update;
use App\Repositories\CategoryRepository;
use App\Repositories\CityRepository;
use App\Repositories\CountryRepository;
use App\Repositories\CouponRepository;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\ICoupon;
use App\Repositories\Interfaces\IProvider;
use App\Repositories\Interfaces\IUser;
use App\Repositories\ProviderRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    protected $provider,$couponRepo,$userRepo,$city,$category,$country;

    public function __construct(ICountry $country,IProvider $provider,ICategory $category,ICity $city,ICoupon $coupon,IUser $user)
    {
        $this->country = $country;
        $this->couponRepo = $coupon;
        $this->userRepo = $user;
        $this->provider = $provider;
        $this->city = $city;
        $this->category = $category;
    }

    /***************************  get all providers  **************************/
    public function index(CouponDatatable $couponDatatable)
    {
        $coupons = $this->couponRepo->all();
        $categories = $this->category->findWhere(['parent_id'=>null]);
        return $couponDatatable->render('admin.coupons.index', compact('coupons','categories'));
    }

    public function create()
    {
        $providers = $this->provider->all();
        $countries = $this->country->all();
        return view('admin.coupons.create', compact('countries','providers'));
    }


    /***************************  store provider **************************/
    public function store(Create $request)
    {
        $data = $request->all();
        $coupon = $this->couponRepo->create($data);
        if(isset($request['seller_id']) && count($request['seller_id']) > 0){
            $coupon->providers()->attach($request['seller_id']);
        }
        return redirect()->route('admin.coupons.index')->with('success', 'تم الاضافه بنجاح');
    }

    public function edit(Coupon $coupon)
    {
        $providers = $this->provider->all();
        $countries = $this->country->all();
        return view('admin.coupons.edit', compact('countries','coupon','providers'));
    }
    /***************************  update provider  **************************/
    public function update(Update $request, $id)
    {
        $coupon = $this->couponRepo->find($id);
        $data = $request->all();
        $this->couponRepo->update($data,$coupon['id']);
        return redirect()->route('admin.coupons.index')->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete provider  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $role = $this->couponRepo->find($d);
                    $this->couponRepo->delete($role['id']);
                }
            }
        }else {
            $role = $this->couponRepo->find($id);
            $this->couponRepo->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
