<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Country\Create;
use App\Http\Requests\Admin\Country\Update;
use App\Repositories\CarTypeRepository;
use App\Repositories\Interfaces\ICarType;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class CarTypeController extends Controller
{
    use UploadTrait;
    protected $cartype;

    public function __construct(ICarType $cartype)
    {
        $this->cartype = $cartype;
    }

    /***************************  get all providers  **************************/
    public function index()
    {
        $cartypes = $this->cartype->all();
        return view('admin.carTypes.index', compact('cartypes'));
    }


    /***************************  store provider **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        if($request->has('image')){
            $data['image'] = $this->uploadImg($request['image'],'carTypes');
        }
        $data['title'] = [
            'ar' => $request['title_ar'],
            'en' => $request['title_en']
        ];
        $this->cartype->create($data);
        return redirect()->back()->with('success', 'تم الاضافه بنجاح');
    }


    /***************************  update provider  **************************/
    public function update(Update $request, $id)
    {
        $cartype = $this->cartype->find($id);
        $data = array_filter($request->all());
        if($request->has('image')){
            $data['image'] = $this->uploadImg($request['image'],'carTypes');
        }
        $data['title'] = [
            'ar' => $request['title_ar'],
            'en' => $request['title_en']
        ];
        $this->cartype->update($data,$cartype['id']);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete provider  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->cartype->delete($d);
                }
            }
        }else {
            $role = $this->cartype->find($id);
            $this->cartype->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
