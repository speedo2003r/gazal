<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ItemDatatable;
use App\Entities\Category;
use App\Entities\Item;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\IFeature;
use App\Repositories\Interfaces\IImage;
use App\Repositories\Interfaces\IItem;
use App\Repositories\Interfaces\IItemType;
use App\Repositories\Interfaces\IItemTypeDetail;
use App\Repositories\Interfaces\IProvider;
use App\Repositories\Interfaces\IType;
use App\Repositories\Interfaces\IUser;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ItemController extends Controller {
  use UploadTrait;
  protected $providerRepo, $featureRepo, $itemRepo, $typeRepo, $itemType, $categoryRepo, $userRepo, $fileRepo, $langs, $ItemTypeDetailRepo;

  public function __construct(
    IProvider $providerRepo,
    IFeature $featureRepo,
    IItemTypeDetail $ItemTypeDetailRepo,
    IItem $item,
    IType $type,
    IItemType $itemType,
    ICategory $category,
    IUser $user,
    IImage $file
  ) {
    $this->itemRepo           = $item;
    $this->categoryRepo       = $category;
    $this->userRepo           = $user;
    $this->providerRepo       = $providerRepo;
    $this->typeRepo           = $type;
    $this->itemType           = $itemType;
    $this->fileRepo           = $file;
    $this->featureRepo        = $featureRepo;
    $this->ItemTypeDetailRepo = $ItemTypeDetailRepo;
    $this->langs              = [
      'ar' => 'arabic',
      'en' => 'english',
    ];
  }
  public function index(ItemDatatable $itemDatatable) {
    return $itemDatatable->render('admin.item.index');
  }

  public function create() {
    $langs      = $this->langs;
    $categories = $this->categoryRepo->findWhere(['parent_id' => null]);
    $items      = $this->itemRepo->orderBy('created_at', 'desc')->paginate(8);
    $sellers    = $this->providerRepo->providers();
    $types      = $this->typeRepo->all();
//        foreach ($items as $item){
//            foreach($item->user->branches as $branch){
//                dump($branch);
//            }
//        }
//        dd(true);
    return view('admin.item.addItem', compact('types', 'items', 'categories', 'langs', 'sellers'));
  }
  public function edit($id) {
    $langs      = $this->langs;
    $categories = $this->categoryRepo->findWhere(['parent_id' => null]);
    $items      = Item::whereId($id)->with('files')->with('types')->orderBy('id', 'desc')->get();
    $sellers    = $items->first()->user()->get();
    $types      = $this->typeRepo->all();
    return view('admin.item.addItem', compact('types', 'id', 'items', 'categories', 'langs', 'sellers'));
  }
  public function delete(Request $request) {
    $item = $this->itemRepo->find($request->id);
    $this->itemRepo->delete($item['id']);
    return response()->json(true);
  }
  public function destroy(Request $request) {
    if (isset($request['data_ids'])) {
      $data = explode(',', $request['data_ids']);
      foreach ($data as $d) {
        if ("" != $d) {
          $item = Item::find($d);
          if (null != $item) {
            $item->delete();
          }
        }
      }
    } else {
      $item = $this->itemRepo->find($request->id);
      $this->itemRepo->delete($item['id']);
    }
    return redirect()->back()->with('success', 'تم الحذف بنجاح');
  }
  public function addFile(Request $request) {
    $item               = $this->itemRepo->find($request->id);
    $data               = array_filter($request->except('filepond'));
    $data['image_type'] = Item::class;
    $data['image_id']   = $item['id'];
    if ($request->has('filepond')) {
      $data['image'] = $this->uploadFile($request['filepond'], 'items');
    } else {
      $file['image'] = $this->uploadFile($request['image'], 'items');
    }
    $file = $this->fileRepo->create($data);
    $file->save();
    $images = $this->fileRepo->findWhere(['image_type' => Item::class, 'image_id' => $item['id']]);
    $images = $images->map(function ($image) {
      return [
        'id'    => $image['id'],
        'image' => dashboard_url('storage/images/items/' . $image['image']),
      ];
    });
    return response()->json($images);
  }
  public function changeMain(Request $request) {
    $file = $this->fileRepo->find($request->id);
    $item = $file->item;
    $item->files->each(function ($value, $index) use ($request) {
      if ($request['id'] != $value['id']) {
        $value->main = 0;
        $value->save();
      } else {
        $value->main = 1;
        $value->save();
      }
    });
    return response()->json(dashboard_url('storage/images/items/' . $file['image']));
  }

  public function store(Request $request) {
    if (null == $request->id) {
      $data          = array_filter($request->all());
      $data['title'] = [
        'ar' => $request['title_ar'],
        'en' => $request['title_en'],
      ];
      $category            = Category::find($request['subcategory_id']);
      $data['category_id'] = $category->parent['id'];
      $item                = $this->itemRepo->create($data);
      $item                = $this->itemRepo->find($item['id']);
      $request->request->add(['item_id' => $item['id']]);
    } else {
      $item = $this->itemRepo->find($request->id);
      $data = array_filter($request->all());
      if ($item['type'] != $request->type) {
        $this->itemRepo->update(['status' => 0], $item['id']);
      }
      $data['title'] = [
        'ar' => $request['title_ar'],
        'en' => $request['title_en'],
      ];
      $category            = Category::find($request['subcategory_id']);
      $data['category_id'] = $category->parent['id'];
      $this->itemRepo->update($data, $item['id']);
    }
    $array = [];
    foreach ($request['branches'] as $branch) {
      $array[$branch] = ['category_id' => $request['subcategory_id']];
    }
    $item->branches()->sync($array);
    return response()->json($item);
  }

  public function delimage(Request $request) {
    $id         = $request->id;
    $image      = $this->fileRepo->find($id);
    $image_path = $image['image']; // Value is not URL but directory file path;
    if (File::exists($image_path)) {
      File::delete($image_path);
    }
    $this->fileRepo->delete($image['id']);
    return response()->json(true);
  }

  public function update(Request $request, $id) {
    $item                = $this->itemRepo->find($id);
    $data                = array_filter($request->all());
    $data['description'] = [
      'ar' => $request['description_ar'],
      'en' => $request['description_en'],
    ];
    $this->itemRepo->update($data, $item['id']);
    $array = [];
    foreach ($request['branches'] as $branch) {
      $array[$branch] = ['category_id' => $request['subcategory_id']];
    }
    $item->branches()->sync($array);
    return response()->json($item);
  }

  public function deleteFeature(Request $request) {
    $feature = $this->featureRepo->find($request['feature_id']);
    $this->featureRepo->delete($feature['id']);
    $item     = Item::find($request['item_id']);
    $features = $item->features->map(function ($feature) {
      return [
        'id'       => $feature['id'],
        'title_ar' => $feature->getTranslations('title')['ar'],
        'title_en' => $feature->getTranslations('title')['en'],
        'price'    => $feature['price'],
      ];
    });
    return response()->json(['value' => 1, 'features' => $features, 'msg' => 'تم الحذف بنجاح']);
  }
  public function saveFeatures(Request $request) {
    DB::beginTransaction();
    try {
      if (count($request->features) > 0) {
        $item = Item::find($request->item_id);
        foreach ($request->features as $key => $res) {
          if ('' == $res['title_ar'] && '' == $res['title_en']) {
            return response()->json(['msg' => 'يوجد بعض الحقول خاليه لا يمكن اكمال عملية الحفظ']);
          }
          $res['item_id'] = $item['id'];

          if (null == $res['price'] || '' == $res['price']) {
            $msg = 'يرجي كتابة السعر';
            return response()->json(['value' => 0, 'msg' => $msg]);
          }
          if (null == $res['title_ar'] || '' == $res['title_ar']) {
            $msg = 'يرجي كتابة الاسم بالعربي';
            return response()->json(['value' => 0, 'msg' => $msg]);
          }
          if (null == $res['title_en'] || '' == $res['title_en']) {
            $msg = 'يرجي كتابة الاسم بالانجليزي';
            return response()->json(['value' => 0, 'msg' => $msg]);
          }
          if (null != $res['id']) {
            $feature      = $this->featureRepo->find($res['id']);
            $res['title'] = [
              'ar' => $res['title_ar'],
              'en' => $res['title_en'],
            ];
            $this->featureRepo->update($res, $feature['id']);
          }
          if (null == $res['id']) {
            $res['title'] = [
              'ar' => $res['title_ar'],
              'en' => $res['title_en'],
            ];
            $this->featureRepo->create($res);
          }
        }
        $features = $item->features->map(function ($feature) {
          return [
            'id'       => $feature['id'],
            'title_ar' => $feature->getTranslations('title')['ar'],
            'title_en' => $feature->getTranslations('title')['en'],
            'price'    => $feature['price'],
          ];
        });
      }
      DB::commit();
    } catch (\Exception$e) {
      $e->getMessage();
      DB::rollBack();
    }
    return response()->json(['features' => $features]);
  }
  public function deleteDetail(Request $request) {
    $detail = $this->ItemTypeDetailRepo->find($request['detail_id']);
    $this->ItemTypeDetailRepo->delete($detail['id']);
    $item  = Item::find($request['item_id']);
    $types = $item->types;
    $types = $types->map(function ($type) use ($item) {
      return [
        'id'       => $type['id'],
        'item_id'  => $type['item_id'],
        'type_id'  => $type['type_id'],
        'children' => $type->children->map(function ($child) {
          return [
            'id'             => $child['id'],
            'title_ar'       => $child->getTranslations('title')['ar'],
            'title_en'       => $child->getTranslations('title')['en'],
            'price'          => $child['price'],
            'type_option_id' => $child['type_option_id'],
          ];
        }),
      ];
    });
    return response()->json(['value' => 1, 'types' => $types, 'msg' => 'تم الحذف بنجاح']);
  }
  public function deleteTypeOption(Request $request) {
    $option = $this->itemType->find($request['option_id']);
    $this->itemType->delete($option['id']);
    $item  = Item::find($request['item_id']);
    $types = $item->types;
    $types = $types->map(function ($type) use ($item) {
      return [
        'id'       => $type['id'],
        'item_id'  => $type['item_id'],
        'type_id'  => $type['type_id'],
        'children' => $type->children->map(function ($child) {
          return [
            'id'             => $child['id'],
            'title_ar'       => $child->getTranslations('title')['ar'],
            'title_en'       => $child->getTranslations('title')['en'],
            'price'          => $child['price'],
            'type_option_id' => $child['type_option_id'],
          ];
        }),
      ];
    });
    return response()->json(['value' => 1, 'types' => $types, 'msg' => 'تم الحذف بنجاح']);
  }
  public function deleteType(Request $request) {
    $type = $this->typeRepo->find($request['type_id']);
    if (count($type->children) > 0) {
      return response()->json(['value' => 0, 'msg' => 'لا يمكن اتمام الحذف']);
    }
    $this->typeRepo->delete($type['id']);
    $item  = Item::find($request['item_id']);
    $types = $item->types;
    $types = $types->map(function ($type) use ($item) {
      return [
        'id'       => $type['id'],
        'item_id'  => $type['item_id'],
        'type_id'  => $type['type_id'],
        'children' => $type->children->map(function ($child) {
          return [
            'id'             => $child['id'],
            'title_ar'       => $child->getTranslations('title')['ar'],
            'title_en'       => $child->getTranslations('title')['en'],
            'price'          => $child['price'],
            'type_option_id' => $child['type_option_id'],
          ];
        }),
      ];
    });
    return response()->json(['value' => 1, 'types' => $types, 'msg' => 'تم الحذف بنجاح']);
  }

  public function changeStatus(Request $request) {
    $item = $this->itemRepo->find($request->id);
    if (1 == $item->status) {
      $item->status = 0;
    } else {
      $item->status = 1;
    }
    $item->save();
    return response()->json($item->status);
  }
  public function saveItemTypes(Request $request) {
    $item = $this->itemRepo->find($request->item_id);
    DB::beginTransaction();
    try {
      if (count($request->types) > 0) {
        foreach ($request->types as $key => $res) {
          $type = null;
          if (null == $res['id']) {
            $type = $this->itemType->create([
              'item_id' => $item['id'],
              'type_id' => $res['type_id'],
            ]);
          }
          if (null != $res['id']) {
            $type = $this->itemType->find($res['id']);
            $this->itemType->update([
              'item_id' => $item['id'],
              'type_id' => $res['type_id'],
            ], $type['id']);
          }
          if (count($res['children']) > 0) {
            foreach ($res['children'] as $childkey => $detail) {
              if (null == $res['type_id'] || '' == $res['type_id']) {
                $msg = 'يرجي اختيار السمه الفرعيه';
                return response()->json(['value' => 0, 'msg' => $msg]);
              }
              if ('' == $detail['title_ar'] || '' == $detail['title_en'] || '' == $detail['price']) {
                $msg = 'يرجي ادخال تفاصيل للسمه';
                return response()->json(['value' => 0, 'msg' => $msg]);
              }
              if (0 == $detail['price']) {
                $msg = 'يرجي ادخال السعر في تفاصيل للسمه';
                return response()->json(['value' => 0, 'msg' => $msg]);
              }
              $itemTypeDetailRepo = null;
              if (null == $detail['id']) {
                $itemTypeDetailRepo = $this->ItemTypeDetailRepo->create([
                  'title'        => [
                    'ar' => $detail['title_ar'],
                    'en' => $detail['title_en'],
                  ],
                  'price'        => $detail['price'],
                  'item_type_id' => $type['id'],
                ]);
              }
              if (null != $detail['id']) {
                $itemTypeDetailRepo = $this->ItemTypeDetailRepo->find($detail['id']);
                $this->ItemTypeDetailRepo->update([
                  'title' => [
                    'ar' => $detail['title_ar'],
                    'en' => $detail['title_en'],
                  ],
                  'price' => $detail['price'],
                ], $itemTypeDetailRepo['id']);
              }
            }
          } else {
            $msg = 'يرجي ادخال تفاصيل للسمه';
            return response()->json(['value' => 0, 'msg' => $msg]);
          }
        }
        $types = $item->types;
        $types = $types->map(function ($type) use ($item) {
          return [
            'id'       => $type['id'],
            'item_id'  => $type['item_id'],
            'type_id'  => $type['type_id'],
            'children' => $type->children->map(function ($child) {
              return [
                'id'             => $child['id'],
                'title_ar'       => $child->getTranslations('title')['ar'],
                'title_en'       => $child->getTranslations('title')['en'],
                'price'          => $child['price'],
                'type_option_id' => $child['type_option_id'],
              ];
            }),
          ];
        });

      }
      DB::commit();
    } catch (\Exception$e) {
      $e->getMessage();
      DB::rollBack();
    }
    return response()->json(['value' => 1, 'types' => $types]);
  }
  public function getSellerCategories(Request $request) {
    $seller        = $this->providerRepo->find($request['id']);
    $allcategories = $seller->categories;
    $branches      = $seller->branches->map(function ($data) {
      return [
        'id'    => $data['id'],
        'title' => $data['title'],
      ];
    });
    return response()->json(['categories' => $allcategories, 'branches' => $branches]);
  }
}
