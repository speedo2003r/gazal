<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\IDiscountReason;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class DiscountAmountController extends Controller
{
    use UploadTrait;
    protected $discount;

    public function __construct(IDiscountReason $discount)
    {
        $this->discount = $discount;
    }

    /***************************  get all providers  **************************/
    public function index()
    {
        $discounts = $this->discount->all();
        return view('admin.discountReasons.index', compact('discounts'));
    }


    /***************************  store provider **************************/
    public function store(Request $request)
    {
        $this->validate($request,[
            'reason.ar' => 'required',
            'reason.en' => 'required',
        ]);
        $data = array_filter($request->all());
        $this->discount->create($data);
        return redirect()->back()->with('success', 'تم الاضافه بنجاح');
    }


    /***************************  update provider  **************************/
    public function update(Update $request, $id)
    {
        $this->validate($request,[
            'reason.ar' => 'required',
            'reason.en' => 'required',
        ]);
        $discount = $this->discount->find($id);
        $data = array_filter($request->all());
        $this->discount->update($data,$discount['id']);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete provider  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->discount->delete($d);
                }
            }
        }else {
            $role = $this->discount->find($id);
            $this->discount->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
