<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProviderDatatable;
use App\Entities\Branch;
use App\Entities\BranchDate;
use App\Entities\Category;
use App\Entities\Item;
use App\Entities\Order;
use App\Entities\Provider;
use App\Entities\Type;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Branch\CreateBranch;
use App\Http\Requests\Admin\Branch\updateBranch;
use App\Http\Requests\Admin\Provider\Create;
use App\Http\Requests\Admin\Provider\Update;
use App\Jobs\SwitchScheduleBranch;
use App\Models\Role;
use App\Repositories\BranchRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CityRepository;
use App\Repositories\CountryRepository;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IEmployee;
use App\Repositories\Interfaces\IItem;
use App\Repositories\Interfaces\IProvider;
use App\Repositories\Interfaces\IType;
use App\Repositories\ProviderRepository;
use App\Repositories\UserRepository;
use App\Traits\NotifyTrait;
use App\Traits\UploadTrait;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class ProviderController extends Controller {
  use UploadTrait;
  use NotifyTrait;
  protected $city, $item, $type, $country, $role, $employee, $provider, $category, $branchRepo,$langs;

  public function __construct(IType $type,IItem $item,IEmployee $employee,IBranch $branchRepo, ICategory $category, IProvider $provider, ICountry $country, ICity $city, Role $role) {
    $this->provider   = $provider;
    $this->category   = $category;
    $this->country    = $country;
    $this->branchRepo = $branchRepo;
    $this->role       = $role;
    $this->city       = $city;
    $this->employee       = $employee;
    $this->item       = $item;
    $this->type       = $type;
      $this->langs              = [
          'ar' => 'arabic',
          'en' => 'english',
      ];
  }

  /***************************  get all providers  **************************/
  public function index(ProviderDatatable $providerDatatable) {
    return $providerDatatable->render('admin.providers.index');
  }

  /***************************  get all providers  **************************/
  public function create() {
    $countries  = $this->country->all();
    $categories = $this->category->findWhere(['parent_id' => null]);
    $representatives = $this->employee->all();
    $roles = $this->role->all();
    return view('admin.providers.create', compact('roles','representatives','categories', 'countries'));
  }
  public function edit(Provider $provider) {
    $validator = JsValidator::make([
      'username'        => 'required|max:191',
      'slogan'          => 'sometimes|max:191',
      'name'            => 'required|max:191',
      'phones[]'        => 'sometimes|numeric|digits_between:9,13',
      'phone'           => 'required|numeric|digits_between:9,13|unique:providers,phone,' . $provider['id'] . ',id,deleted_at,NULL',
      'email'           => 'required|email|max:191|unique:providers,email,' . $provider['id'] . ',id,deleted_at,NULL',
      'emails[]'        => 'sometimes|email|max:191',
      'password'        => 'nullable|confirmed|max:191',
      'store_name.ar'   => 'required|max:191',
      'store_name.en'   => 'required|max:191',
      'category_id'     => 'required|exists:categories,id,deleted_at,NULL',
      'group_id[]'        => 'required|exists:groups,id,deleted_at,NULL',
      'image'           => 'nullable|file|image|mimes:jpeg,jpg,png',
      'country_id'      => 'required|exists:countries,id,deleted_at,NULL',
      'city_id'         => 'required|exists:cities,id,deleted_at,NULL',
      'address'         => 'required',
      'lat'             => 'required',
      'lng'             => 'required',
      'processing_time' => 'required',
      'commercial_num'  => 'nullable|digits_between:7,10',
      'acc_bank'        => 'nullable|digits_between:20,24',
      'commission'      => 'nullable',
    ]);
    $countries  = $this->country->all();
    $categories = $this->category->allMainCategories();
    return view('admin.providers.edit', compact('validator', 'provider', 'categories', 'countries'));
  }

  /***************************  store provider **************************/
  public function store(Create $request) {

  DB::beginTransaction();
  try{
    $data = array_filter($request->except('image'));
    if ($request->has('image')) {
      $data['avatar'] = $this->uploadFile($request['image'], 'providers');
    }
    $provider = $this->provider->create($data);
    $data['title']       = $request['store_name'];
    $data['status']      = 1;
    $data['provider_id'] = $provider['id'];
    $data['name']        = $provider['name'];
    $this->branchRepo->create($data);
      if(isset($request->phones) && count($request->phones) > 0){
          foreach ($request->phones as $phone){
              $provider->contactDetails()->create([
                  'type'=>2,
                  'data'=>$phone,
              ]);
          }
      }
      if(isset($request->emails) && count($request->emails) > 0){
          foreach ($request->emails as $email){
              $provider->contactDetails()->create([
                  'type'=>1,
                  'data'=>$email,
              ]);
          }
      }
      DB::commit();
  }catch(\Exception $e){
      DB::rollBack();
  }
    return redirect()->route('admin.providers.index')->with('success', 'تم الاضافه بنجاح');
  }

  /***************************  update provider  **************************/
  public function update(Update $request,$id) {
    $provider = $this->provider->find($id);
    if ($request->has('image')) {
      if (null != $provider['avatar'] || '/default.png' != $provider['avatar']) {
        $this->deleteFile($provider['avatar'], 'users');
      }
      $request->request->add(['avatar' => $this->uploadFile($request['image'], 'providers')]);
    }
    $this->provider->update(array_filter($request->except('image')), $provider['id']);
    $provider->groups()->sync($request['group_id']);
    if(isset($request->phones) && count($request->phones) > 0){
        $provider->contactDetails()->where('type',2)->delete();
        foreach ($request->phones as $phone){
            $provider->contactDetails()->create([
                'type'=>2,
                'data'=>$phone,
            ]);
        }
    }
    if(isset($request->emails) && count($request->emails) > 0){
        $provider->contactDetails()->where('type',1)->delete();
        foreach ($request->emails as $email){
            $provider->contactDetails()->create([
                'type'=>1,
                'data'=>$email,
            ]);
        }
    }
    return redirect()->route('admin.providers.index')->with('success', 'تم التحديث بنجاح');
  }
  public function categories(Provider $provider) {
    $categories       = $provider->category->children;
    $sellercategories = $provider->categories;
    return view('admin.providers.categories.index', compact('provider', 'categories', 'sellercategories'));
  }

    /***************************  show delegate **************************/
    public function show(Provider $provider)
    {
        return view('admin.providers.show',compact('provider'));
    }
  public function postcategories(Request $request) {
    $provider = $this->provider->find($request['user_id']);
    $provider->categories()->sync($request['categories']);
    return redirect()->back()->with('success', 'تم الاضافه بنجاح');
  }

  public function images($id) {
    $provider = $this->provider->find($id);
    return view('admin.providers.images.index', compact('provider'));
  }
  public function storeImages(Request $request, $id) {
    $user = $this->provider->find($id);
    if ($request->has('_logo')) {
      $this->deleteFile($user['logo'], 'providers');
      $logo = $this->uploadFile($request['_logo'], 'providers');
      $request->request->add(['logo' => $logo]);
    }
    if ($request->has('_banner')) {
      $this->deleteFile($user['banner'], 'providers');
      $logo = $this->uploadFile($request['_banner'], 'providers');
      $request->request->add(['banner' => $logo]);
    }
    if ($request->has('_tax_certificate')) {
      $this->deleteFile($user['tax_certificate'], 'providers');
      $logo = $this->uploadFile($request['_tax_certificate'], 'providers');
      $request->request->add(['tax_certificate' => $logo]);
    }
    if ($request->has('_id_avatar')) {
      $this->deleteFile($user['id_avatar'], 'providers');
      $logo = $this->uploadFile($request['_id_avatar'], 'providers');
      $request->request->add(['id_avatar' => $logo]);
    }
    if ($request->has('_commercial_image')) {
      $this->deleteFile($user['commercial_image'], 'providers');
      $logo = $this->uploadFile($request['_commercial_image'], 'providers');
      $request->request->add(['commercial_image' => $logo]);
    }
    if ($request->has('_municipal_license')) {
      $this->deleteFile($user['municipal_license'], 'providers');
      $logo = $this->uploadFile($request['_municipal_license'], 'providers');
      $request->request->add(['municipal_license' => $logo]);
    }
    $sellerRequests = $request->all();
    $this->provider->update($sellerRequests, $user['id']);
    return redirect()->back()->with('success', 'تم التحديث بنجاح');
  }


  public function sellerbranches(Provider $provider) {
    $branches  = $provider->branches;
    $countries = $this->country->all();
    $cities    = $this->city->findWhere(['country_id' => $provider['country_id']]);
    return view('admin.providers.branches.index', compact('countries',  'cities', 'provider', 'branches'));
  }

  public function discount(Request $request) {
    $this->validate($request, [
      'provider_id' => 'required|exists:providers,id,deleted_at,NULL',
      'discount'    => 'min:0|max:99|size:2',
      'from'        => 'date|before:to',
      'to'          => 'date|after:from',
    ]);
    $user = $this->provider->find($request['provider_id']);
    $this->provider->update($request->all(), $user['id']);
    return back()->with('success', awtTrans('تم تعديل الخصم بنجاح'));
  }
  public function addToWallet(Request $request)
  {
      $this->validate($request,[
          'amount' => 'required'
      ],[],[
          'amount' => awtTrans('رصيد')
      ]);
      $user = $this->provider->find($request['user_id']);
      $user->wallet = $request['amount'];
      $user->save();
      return back()->with('success','تم الاضافة الي المحفظه بنجاح');
  }

  public function sales()
  {
      $providers = $this->provider->all();
      return view('admin.providers.sales',compact('providers'));
  }

  public function salesAmount(Request $request)
  {
      $validator = Validator::make($request->all(),[
          'from' => 'nullable|before:to',
          'to' => 'nullable|after:from',
          'provider_id' => 'required|exists:providers,id,deleted_at,NULL',
      ],[],[
          'from' => __('from'),
          'to' => __('to'),
          'provider_id' => __('provider'),
      ]);
      if($validator->fails()){
          return response()->json([
              'key' => 'fail',
              'value' => 0,
              'msg' => $validator->errors()->first(),
          ]);
      }
      if($request['provider_id']){
          $data = Order::query()->select(DB::raw('SUM((orders.final_total + orders.vat_amount) - orders.coupon_amount) as total'))
              ->where('provider_id',$request['provider_id']);
      }
      if($request['branch_id']){
          $data->where('branch_id',$request['branch_id']);
      }
      if($request['from'] && $request['to']){
          $data->whereDate('created_date','>=',$request['from'])->whereDate('created_date','<=',$request['from']);
      }
      $data = $data->first();
      $total = $data['total'] ?? 0;
      return response()->json([
          'key' => 'success',
          'value' => 1,
          'data' => $total,
      ]);
  }

  public function items()
  {
      $sellers = $this->provider->all();
      $categories = $this->category->where(['parent_id' => null]);
      $items      = $this->item->where('user_id',\request('provider_id'))->orderBy('created_at', 'desc')->paginate(8);
      $types      = $this->type->all();
      return view('admin.providers.items.index',get_defined_vars());
  }

  public function changeSchedule(Request $request)
  {
      $this->validate($request,[
          'schedule' => 'required'
      ],[],[
          'schedule' => __('admin.Schedule')
      ]);
      $job = new SwitchScheduleBranch($request['schedule']);
      dispatch($job);
      return back()->with('success','تم الارسال بنجاح');
  }
}
