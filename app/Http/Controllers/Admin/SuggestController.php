<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\SuggestDatatable;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\ISuggest;
use App\Repositories\SuggestRepository;
use Illuminate\Http\Request;

class SuggestController extends Controller
{
    protected $suggest;

    public function __construct(ISuggest $suggest)
    {
        $this->suggest = $suggest;
    }

    /***************************  get all contacts  **************************/
    public function index(SuggestDatatable $suggestDatatable)
    {
        $suggests = $this->suggest->all();
        return $suggestDatatable->render('admin.suggests.index', compact('suggests'));
    }

    /***************************  delete contact  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->suggest->delete($d);
                }
            }
        }else {
            $role = $this->suggest->find($id);
            $this->suggest->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
