<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ClosedDatatable;
use App\DataTables\JoinUsDatatable;
use App\DataTables\PercentagesSalesDatatable;
use App\DataTables\TimeOutDatatable;
use App\Entities\JoinUs;
use App\Entities\Provider;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\IJoinUs;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JsValidator;
use Yajra\DataTables\DataTables;

class ManagerController extends Controller
{
    use UploadTrait;
    protected $join;

    public function __construct(IJoinUs $join)
    {
        $this->join = $join;
    }


    /***************************  get all joins of providers  **************************/
    public function joins(JoinUsDatatable $joinDatatable)
    {
        $joins = $this->join->all();
        return $joinDatatable->render('admin.accountManagers.joins.index', compact('joins'));
    }

    /***************************  get all end of contract  **************************/
    public function endOfContract()
    {
        return view('admin.accountManagers.contracts.index');
    }
    /***************************  get all closed providers  **************************/
    public function closedProviders(ClosedDatatable $datatable)
    {
        return $datatable->render('admin.accountManagers.closed.index');
    }
    public function percentagesSales(PercentagesSalesDatatable $datatable)
    {
        return $datatable->render('admin.accountManagers.sales.index');
    }
    public function addPercentagesPage(Provider $provider)
    {
        return view('admin.accountManagers.sales.addPercentagesPage',compact('provider'));
    }
    public function postCommission(Provider $provider,Request $request)
    {
        $this->validate($request,[
            'commission_status' =>'required',
            'commission' =>'required',
        ]);
        if($request['commission_status'] == 2 && $request['commission'] > 100){
            return back()->with('danger','يجب أن يكون قيمة العموله أصغر من 100');
        }
        $data = array_filter($request->all());
        $provider->update($data);
        return redirect()->route('admin.manager.percentagesSales');
    }
    /***************************  delete join  **************************/
    public function joinsDelete(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->join->delete($d);
                }
            }
        }else {
            $role = $this->join->find($id);
            $this->join->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }
    /***************************  show join  **************************/
    public function showJoin($id)
    {
        $join = JoinUs::find($id);
        return view('admin.accountManagers.joins.show',get_defined_vars());
    }
    public function endOfContractStore(Request $request,$id)
    {
        $this->validate($request,[
            'end_date_contract' => 'required|after:'.Carbon::now()->format('Y-m-d')
        ],[],[
            'end_date_contract' => 'تاريخ انتهاء التعاقد'
        ]);
        $provider = Provider::find($id);
        $provider->update([
            'end_date_contract'=>$request['end_date_contract']
        ]);
        return back()->with('success','تم تغيير التاريخ بنجاح');
    }

    public function providersTimeOut(TimeOutDatatable $datatable)
    {
        return $datatable->render('admin.accountManagers.timeOut.index');
    }
    public function getEndOfContractData(Request $request,Provider $provider)
    {
        $items = $provider->query()->where(function($query){
            $query->whereDate('end_date_contract','<=',Carbon::now()->addDays(30)->format('Y-m-d'));
            $query->orWhereDate('end_date_contract','<=',Carbon::now()->format('Y-m-d'));
        })->latest();
        return DataTables::of($items)
            ->editColumn('id',function ($query){
                return '<label class="custom-control material-checkbox" style="margin: auto">
                            <input type="checkbox" class="material-control-input checkSingle" id="'.$query->id.'">
                            <span class="material-control-indicator"></span>
                        </label>';
            })
            ->addColumn('view',function ($query){
                return 'admin.providers.show';
            })
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('control','admin.partial.ControlEndContract')
//            ->filter(function ($query) use ($request) {
//                if($request->search != null) {
//                    $query->where('phone','like','%'.$request->search['value'].'%')
//                        ->OrWhere('email','like','%'.$request->search['value'].'%')
//                        ->OrWhere('id','like','%'.$request->search['value'].'%');
//                }
//            })
            ->rawColumns(['status','control','id'])->make(true);
    }
}
