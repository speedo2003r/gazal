<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\OrderDatatable;
use App\Entities\Order;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Interfaces\IOrder;
use App\Repositories\OrderRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class OrderController extends Controller
{
    protected $orderRepo;

    public function __construct(IOrder $order)
    {
        $this->orderRepo = $order;
    }

    /***************************  get all providers  **************************/
    public function index()
    {
        return view('admin.orders.index');
    }



    /***************************  update provider  **************************/
    public function show($id)
    {
        $order = $this->orderRepo->find($id);
        return view('admin.orders.show', compact('order'));
    }
    /***************************  update provider  **************************/
    public function orderClient($id)
    {
        $user = User::find($id);
        return view('admin.orders.client', compact('user'));
    }

    /***************************  delete provider  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->orderRepo->delete($d);
                }
            }
        }else {
            $role = $this->orderRepo->find($id);
            $this->orderRepo->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }


    public function getFilterData(Request $request,Order $order)
    {
            $items = $order->query()->where('live',1)
                ->when($request['type'] == 'admin.orders.accepted',function ($query){
                    $query->where('order_status',Order::STATUS_ACCEPTED);
                })
                ->when($request['type'] == 'admin.orders.prepared',function ($query){
                    $query->where('order_status',Order::STATUS_PREPARED);
                })
                ->when($request['type'] == 'admin.orders.onway',function ($query){
                    $query->where('order_status',Order::STATUS_ON_WAY);
                })
                ->when($request['type'] == 'admin.orders.arrived',function ($query){
                    $query->where('order_status',Order::STATUS_ARRIVED_TO_CLIENT);
                })
                ->when($request['type'] == 'admin.orders.refused',function ($query){
                    $query->where('order_status',Order::STATUS_REFUSED);
                })
                ->when($request['type'] == 'admin.orders.timeout',function ($query){
                    $query->whereTime('progress_end','<',Carbon::now()->format('H:i:s'))->whereDate('date','<=',Carbon::now()->format('Y-m-d'))->where('order_status',Order::STATUS_CANCELED);
                })
                ->when($request['type'] == 'admin.orders.postponed',function ($query){
                    $query->where('order_status',Order::STATUS_PREPARED)->where('delegate_id',null);
                })
                ->select('orders.*','users.email','users.phone','providers.name as provider_name','delegates.name as delegate_name')
                ->leftJoin('users','users.id','=','orders.user_id')
                ->leftJoin('providers','providers.id','=','orders.provider_id')
                ->leftJoin('delegates','delegates.id','=','orders.delegate_id')
                ->latest();
        return DataTables::of($items)
            ->editColumn('id',function ($query){
                return '<label class="custom-control material-checkbox" style="margin: auto">
                            <input type="checkbox" class="material-control-input checkSingle" id="'.$query->id.'">
                            <span class="material-control-indicator"></span>
                        </label>';
            })
            ->editColumn('order_status',function ($query){
                return trans('order.'.$query['order_status']);
            })
            ->addColumn('url',function ($query){
                return 'admin.orders.destroy';
            })
            ->addColumn('show',function ($query){
                return 'admin.orders.show';
            })
            ->addColumn('data',function ($query){
                return $query;
            })
            ->addColumn('control','admin.partial.ControlOrder')
            ->filter(function ($query) use ($request) {
                if($request->search != null) {
                    $query->where('users.phone','like','%'.$request->search['value'].'%')
                        ->OrWhere('users.email','like','%'.$request->search['value'].'%')
                        ->OrWhere('orders.order_num','like','%'.$request->search['value'].'%');
                }
                if($request->from != null && $request->to != null) {
                    return $query->whereDate('created_date','>=',$request->from)->whereDate('created_date','<=',$request->to);
                }
            })
            ->rawColumns(['status','control','id'])->make(true);
    }

    /***************************  update status  **************************/
    public function changeStatus(Request $request)
    {
        $data = array_filter($request->all());
        $order = $this->orderRepo->find($data['order_id']);
        if($order['delegate_id'] == null){
            return back()->with('danger','قم باختيار مندوب أولا للمهمه');
        }
        $this->orderRepo->update([
            'status' => $request['status'],
        ],$order['id']);
        return back()->with('success','تم تغيير حالة الطلب');
    }
    public function assignDelegate(Request $request)
    {
        $data = array_filter($request->all());
        $order = $this->orderRepo->find($data['order_id']);
        if($order['delegate_id'] == null){
            $this->orderRepo->update([
                'delegate_id' => $request['delegate_id'],
            ],$order['id']);
        }else{
            $this->orderRepo->update([
                'delegate_id' => $request['delegate_id'],
            ],$order['id']);
        }

        return back()->with('success','تم تعيين مندوب للطلب');
    }
}
