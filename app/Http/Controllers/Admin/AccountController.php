<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\AccountProviderDatatable;
use App\DataTables\DiscountAmountDatatable;
use App\DataTables\ProviderReportDatatable;
use App\Entities\Provider;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\IDiscountReason;
use App\Repositories\Interfaces\IProvider;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    use UploadTrait;
    protected $provider,$discountReason;
    public function __construct(IProvider $provider,IDiscountReason $reason)
    {
        $this->provider = $provider;
        $this->discountReason = $reason;
    }

    public function index(AccountProviderDatatable $datatable)
    {
        return $datatable->render('admin.accounts.discount.index');
    }

    public function discountAmount(DiscountAmountDatatable $datatable,$id)
    {
        $provider = $this->provider->find($id);
        return $datatable->with(['provider_id'=>$provider['id']])->render('admin.accounts.discount.discountAmount',get_defined_vars());
    }
    public function discountAmountCreate($id)
    {
        $provider = $this->provider->find($id);
        $discountReasons = $this->discountReason->all();
        return view('admin.accounts.discount.create',compact('provider','discountReasons'));
    }
    public function discountAmountStore(Request $request)
    {
        dd($request->all());
    }

    public function providerReport(ProviderReportDatatable $datatable)
    {
        return $datatable->render('admin.accounts.providerReport.index');
    }
    public function filterProviderReport(Provider $provider)
    {
        return view('admin.accounts.providerReport.report');
    }
    public function addTax()
    {
        return view('admin.accounts.tax.create');
    }
}
