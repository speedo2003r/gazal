<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class NotificationController extends Controller
{

    public function index()
    {
        $notifications = auth('admin')->user()->notifications;

        foreach ($notifications as $notification)
            if($notification->seen == 0){
                $notification->seen = 1;
                $notification->save();
            }
        return view('admin.notifications.index', compact('notifications'));
    }

    public function create()
    {
        return view('admin.notifications.create');
    }


}
