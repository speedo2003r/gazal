<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\DelegateOperatingCompanyDatatable;
use App\DataTables\OperatingCompanyDatatable;
use App\Entities\BlockUser;
use App\Entities\Company;
use App\Entities\Delegate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Company\Create;
use App\Http\Requests\Admin\Company\Update;
use App\Repositories\CityRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\CountryRepository;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICompany;
use App\Repositories\Interfaces\ICountry;
use App\Traits\NotifyTrait;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JsValidator;

class OperatingCompanyController extends Controller
{
    use NotifyTrait;
    use ResponseTrait;
    use UploadTrait;
    protected $country,$city,$company;
    public function __construct(ICompany $company,ICountry $country,ICity $city)
    {
        $this->country = $country;
        $this->city = $city;
        $this->company = $company;
    }

    /***************************  get all companies  **************************/
    public function index(OperatingCompanyDatatable $datatable)
    {
        return $datatable->render('admin.companies.index');
    }
    /***************************  create company  **************************/
    public function create()
    {
        $countries = $this->country->all();
        $cities = $this->city->all();
        return view('admin.companies.create',get_defined_vars());
    }
    /***************************  store company  **************************/
    public function store(Create $request)
    {
        DB::beginTransaction();
        try{
            $data = array_filter($request->except('image'));
            if($request->has('image')){
                $data['avatar'] = $this->uploadFile($request['image'],'companies');
            }
            $company = $this->company->create($data);
            if(isset($request->phones) && count($request->phones) > 0){
                foreach ($request->phones as $phone){
                    $company->contactDetails()->create([
                        'type'=>2,
                        'data'=>$phone,
                    ]);
                }
            }
            if(isset($request->emails) && count($request->emails) > 0){
                foreach ($request->emails as $email){
                    $company->contactDetails()->create([
                        'type'=>1,
                        'data'=>$email,
                    ]);
                }
            }
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
        }

        return redirect()->route('admin.companies.index')->with('success', 'تم الاضافه بنجاح');
    }
    /***************************  update company  **************************/
    public function edit(Company $company)
    {
        $validator = JsValidator::make([
            'name' => 'required|max:191',
            'phone'      => "required|numeric|digits_between:9,13|unique:companies,phone,{$this->id},id,deleted_at,NULL",
            'email'      => "required|email|max:191|unique:companies,email,{$this->id},id,deleted_at,NULL",
            'password'   => 'nullable|max:191|confirmed',
            'image'      => 'nullable|mimes:jpeg,jpg,png',
            'country_id'    => 'required|exists:countries,id,deleted_at,NULL',
            'city_id'    => 'required|exists:cities,id,deleted_at,NULL',
            'lat'    => 'required',
            'lng'    => 'required',
            'address'    => 'required',
        ]);
        $countries = $this->country->all();
        $cities = $this->city->all();
        return view('admin.companies.edit',compact('validator','company','countries','cities'));
    }
    public function update(Update $request,$id)
    {
        $company = $this->company->find($id);
        if($request->has('image')){
            if($company['avatar'] != null || $company['avatar'] != '/default.png'){
                $this->deleteFile($company['avatar'],'companies');
            }
            $request->request->add(['avatar'=>$this->uploadFile($request['image'],'companies')]);
        }
        $this->company->update(array_filter($request->except('image')),$company['id']);
        if(count($request->phones) > 0){
            $company->contactDetails()->where('type',2)->delete();
            foreach ($request->phones as $phone){
                $company->contactDetails()->create([
                    'type'=>2,
                    'data'=>$phone,
                ]);
            }
        }
        if(count($request->emails) > 0){
            $company->contactDetails()->where('type',1)->delete();
            foreach ($request->emails as $email){
                $company->contactDetails()->create([
                    'type'=>1,
                    'data'=>$email,
                ]);
            }
        }
        return redirect()->route('admin.companies.index')->with('success', 'تم التحديث بنجاح');
    }
    /***************************  show company  **************************/
    public function show(DelegateOperatingCompanyDatatable $datatable,Company $company)
    {
        return $datatable->with(['company_id'=>$company['id']])->render('admin.companies.show',compact('company'));
    }


    public function block(Request $request)
    {
        $this->validate($request,[
            'notes' => 'required|string|max:2000',
        ],[],[
            'notes' => awtTrans('السبب')
        ]);
        BlockUser::create([
            'notes' => $request['notes'],
            'modal_id' => $request['user_id'],
            'modal_type' => Company::class,
        ]);
        $user = Company::find($request['user_id']);
        $user->banned = 1;
        $user->save();
        return back()->with('success','تم الحظر بنجاح');
    }
    public function unblock(Request $request)
    {
        $this->validate($request,[
            'notes' => 'required|string|max:2000',
        ],[],[
            'notes' => awtTrans('السبب')
        ]);
        BlockUser::create([
            'notes' => $request['notes'],
            'modal_id' => $request['user_id'],
            'modal_type' => Company::class,
            'status' => 2
        ]);
        $user = Company::find($request['user_id']);
        $user->banned = 0;
        $user->save();
        return back()->with('success','تم الغاء الحظر بنجاح');
    }


}
