<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\TopListDatatable;
use App\Entities\TopList;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TopList\Create;
use App\Http\Requests\Admin\TopList\Update;
use App\Repositories\Interfaces\IProvider;
use App\Repositories\Interfaces\ITopList;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class TopListController extends Controller
{
    use ResponseTrait;
    use UploadTrait;
    protected $provider,$topList;

    public function __construct(IProvider $provider,ITopList $topList)
    {
        $this->topList = $topList;
        $this->provider = $provider;
    }

    /***************************  get all topLists  **************************/
    public function index(TopListDatatable $datatable)
    {
        return $datatable->render('admin.topList.index');
    }

    /***************************  get all topLists  **************************/
    public function create()
    {
        $providers = $this->provider->providers();
        return view('admin.topList.create',compact('providers'));
    }
    public function edit(TopList $topList)
    {
        $providers = $this->provider->providers();
        return view('admin.topList.edit',compact('providers','topList'));
    }
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        $this->topList->create($data);
        return redirect()->route('admin.topLists.index')->with('success','تم الحفظ بنجاح');
    }
    public function update(Update $request,$id)
    {
        $topList = $this->topList->find($id);
        $data = array_filter($request->all());
        $this->topList->update($data,$topList['id']);
        return redirect()->route('admin.topLists.index')->with('success','تم التحديث بنجاح');
    }

    /***************************  delete topList  **************************/
    public function destroy(Request $request,$id)
    {
        $role = $this->topList->find($id);
        $this->topList->delete($role['id']);
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }
}
