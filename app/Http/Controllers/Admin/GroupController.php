<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\Create;
use App\Http\Requests\Admin\Category\Update;
use App\Repositories\CategoryRepository;
use App\Repositories\GroupRepository;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\IGroup;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    protected $groupRepo,$categoryRepo;
    use UploadTrait;

    public function __construct(ICategory $category,IGroup $group)
    {
        $this->groupRepo = $group;
        $this->categoryRepo = $category;
    }

    /***************************  get all subCategories  **************************/
    public function index($id)
    {
        $check = $this->categoryRepo->find($id);
        if(!$check)
            return redirect()->back()->with('danger', 'هذا القسم غير موجود');

        $groups = $this->groupRepo->where('category_id',$id)->whereHas('category',function ($query){
            $query->where('parent_id',null);
        })->get();
        return view('admin.groups.index', compact('groups'));
    }


    /***************************  store category **************************/
    public function store(Create $request,$id)
    {
        $check = $this->categoryRepo->find($id);
        if(!$check)
            return redirect()->back()->with('danger', 'هذا القسم غير موجود');

        $data = array_filter($request->all());
        $data['title'] = [
            'ar' =>$request['title_ar'],
            'en' =>$request['title_en'],
        ];
        $data['category_id'] = $check['id'];
        $this->groupRepo->create($data);
        return redirect()->back()->with('success', 'تم الاضافه بنجاح');
    }


    /***************************  update category  **************************/
    public function update(Update $request, $id)
    {
        $data = array_filter($request->all());
        $subCategory = $this->groupRepo->find($id);
        $data['title'] = [
            'ar' =>$request['title_ar'],
            'en' =>$request['title_en'],
        ];
        $this->groupRepo->update($data,$subCategory['id']);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete category  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $role = $this->groupRepo->find($d);
                    $this->groupRepo->delete($role['id']);
                }
            }
        }else {
            $role = $this->groupRepo->find($id);
            $this->groupRepo->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
