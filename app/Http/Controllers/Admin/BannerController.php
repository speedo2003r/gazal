<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\BannerDatatable;
use App\Entities\Banner;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Banner\Create;
use App\Http\Requests\Admin\Banner\Update;
use App\Repositories\BannerRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CityRepository;
use App\Repositories\CountryRepository;
use App\Repositories\Interfaces\IBanner;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\Interfaces\IUser;
use App\Repositories\UserRepository;
use App\Traits\ResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    use ResponseTrait;
    use UploadTrait;
    protected $bannerRepo,$countryRepo,$user,$city,$category;

    public function __construct(ICategory $category,ICity $city,IUser $user,IBanner $bannerRepo,ICountry $country)
    {
        $this->bannerRepo = $bannerRepo;
        $this->countryRepo = $country;
        $this->city = $city;
        $this->category = $category;
        $this->user = $user;
    }

    /***************************  get all providers  **************************/
    public function index(BannerDatatable $bannerDatatable)
    {
        $cities = $this->city->all();
        $categories = $this->category->findWhere(['parent_id'=>null]);
        return $bannerDatatable->render('admin.banners.index',compact('cities','categories'));
    }

    /***************************  get all providers  **************************/
    public function create()
    {
        $countries = $this->countryRepo->all();
        $categories = $this->category->findWhere(['parent_id'=>null]);
        return view('admin.banners.create',compact('countries','categories'));
    }


    /***************************  store provider **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        if($data['type'] == 'item'){
            if($data['item_id'] == null){
                return redirect()->back()->with('danger', 'حقل المنتجات مطلوب');
            }
        }
        if($request->has('image')){
            $data['image'] = $this->uploadFile($request['image'],'banners');
        }
        $this->bannerRepo->create($data);
        return redirect()->route('admin.banners.index')->with('success', 'تم الاضافه بنجاح');
    }

    /***************************  get all providers  **************************/
    public function edit(Banner $banner)
    {
        $countries = $this->countryRepo->all();
        $categories = $this->category->findWhere(['parent_id'=>null]);
        return view('admin.banners.edit',compact('banner','countries','categories'));
    }

    /***************************  update provider  **************************/
    public function update(Update $request, $id)
    {
        $banner = $this->bannerRepo->find($id);
        $data = array_filter($request->all());
        if($data['type'] == 'item'){
            if($data['item_id'] == null){
                return redirect()->back()->with('danger', 'حقل المنتجات مطلوب');
            }
        }
        if($request->has('image')){
            if($banner['image'] != null){
                $this->deleteFile($banner['image'],'banners');
                $data['image'] = $this->uploadFile($request['image'],'banners');
            }
        }
        $this->bannerRepo->update(array_filter($data),$banner['id']);
        return redirect()->route('admin.banners.index')->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete provider  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->bannerRepo->delete($d);
                }
            }
        }else {
            $role = $this->bannerRepo->find($id);
            $this->bannerRepo->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

    public function changeActive(Request $request)
    {
        if($request->ajax()){
            $banner = $this->bannerRepo->find($request['id']);
            $banner['active'] = !$banner['active'];
            $banner->save();
            return $this->successResponse($banner['active']);
        }
    }
}
