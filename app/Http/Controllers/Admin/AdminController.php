<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Admin\Create;
use App\Http\Requests\Admin\Admin\Update;
use App\Models\Role;
use App\Repositories\AdminRepository;
use App\Repositories\CountryRepository;
use App\Repositories\Interfaces\IAdmin;
use App\Repositories\Interfaces\ICountry;
use App\Repositories\UserRepository;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use JsValidator;

class AdminController extends Controller
{
    use UploadTrait;
    protected $adminRepo, $roleRepo,$country;

    public function __construct(IAdmin $adminRepo,ICountry $country,Role $role)
    {
        $this->adminRepo = $adminRepo;
        $this->roleRepo = $role;
        $this->country = $country;
    }

    /***************************  get all admins  **************************/
    public function index()
    {
        $admins = $this->adminRepo->orderBy('created_at','desc')->get();
        return view('admin.admins.index', compact('admins'));
    }


    /***************************  create page admin **************************/
    public function create()
    {
        $roles  = $this->roleRepo->all();
        $countries  = $this->country->all();
        return view('admin.admins.create', compact('countries','roles'));
    }
    /***************************  store admin **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->except('image'));
        if($request->has('image')){
            $data['avatar'] = $this->uploadFile($request['image'],'users');
        }
        $this->adminRepo->store($data);
        return redirect()->route('admin.admins.index')->with('success', 'تم الاضافه بنجاح');
    }

    /***************************  edit admin  **************************/
    public function edit(Admin $admin)
    {
        $validator = JsValidator::make([
            'name'            => 'required|max:191',
            'phone'           => 'required|numeric|digits_between:9,13|unique:admins,phone,' . $admin['id'] . ',id,deleted_at,NULL',
            'email'           => 'required|email|max:191|unique:admins,email,' . $admin['id'] . ',id,deleted_at,NULL',
            'password'        => 'nullable|confirmed|max:191',
            'image'           => 'nullable|file|image|mimes:jpeg,jpg,png',
            'country_id'      => 'required',
            'city_id'         => 'required',
            'address'         => 'required',
        ]);
        $roles  = $this->roleRepo->all();
        $countries  = $this->country->all();
        return view('admin.admins.edit', compact('validator','countries','roles','admin'));
    }

    /***************************  update admin  **************************/
    public function update(Update $request, $id)
    {
        $admin = $this->adminRepo->find($id);
        if($request->has('image')){
            $this->deleteFile($admin['avatar'],'users');
            $request->request->add(['avatar'=>$this->uploadFile($request['image'],'users')]);
        }
        $this->adminRepo->update(array_filter($request->except('image')),$admin['id']);
        return redirect()->route('admin.admins.index')->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete admin  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->adminRepo->delete($d);
                }
            }
        }else {
            $role = $this->adminRepo->find($id);
            $this->adminRepo->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

    /***************************  update profile  **************************/
//    public function updateProfile(UpdateProfile $request,$id)
//    {
//        $admin = $this->userRepo->findOrFail($id);
//        if($request->has('image')) {
//            $this->deleteFile($admin['avatar'], 'users');
//            $request->request->add(['avatar' => $this->uploadFile($request['image'], 'users')]);
//        }
//        $this->userRepo->update(array_filter($request->except('image')),$admin);
//        return redirect()->back()->with('success', 'updated successfully');
//    }
}
