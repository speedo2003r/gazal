<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\City\Create;
use App\Http\Requests\Admin\City\Update;
use App\Repositories\CityRepository;
use App\Repositories\CountryRepository;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use Illuminate\Http\Request;

class CityController extends Controller
{
    protected $country,$city;

    public function __construct(ICity $city,ICountry $country)
    {
        $this->city = $city;
        $this->country = $country;
    }

    /***************************  get all providers  **************************/
    public function index()
    {
        $countries = $this->country->all();
        $cities = $this->city->all();
        return view('admin.cities.index', compact('countries','cities'));
    }


    /***************************  store provider **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        $this->city->create($data);
        return redirect()->back()->with('success', 'تم الاضافه بنجاح');
    }


    /***************************  update provider  **************************/
    public function update(Update $request, $id)
    {
        $data = array_filter($request->all());
        $this->city->update($data,$id);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete provider  **************************/
    public function destroy(Request $request,$id)
    {

        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->city->delete($d);
                }
            }
        }else {
            $role = $this->city->find($id);
            $this->city->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
