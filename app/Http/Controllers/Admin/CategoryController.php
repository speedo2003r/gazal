<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\Create;
use App\Http\Requests\Admin\Category\Update;
use App\Repositories\CategoryRepository;
use App\Repositories\Interfaces\ICategory;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use UploadTrait;
    protected $categoryRepo,$specification;

    public function __construct(ICategory $category)
    {
        $this->categoryRepo = $category;
    }

    /***************************  get all categories  **************************/
    public function index()
    {
        $categories = $this->categoryRepo->findWhere(['parent_id'=>null]);
        return view('admin.categories.index', compact('categories'));
    }
    /***************************  get view tree categories  **************************/
    public function view()
    {
        $categories = $this->categoryRepo->findWhere(['parent_id'=>null]);
        return view('admin.categories.tree', compact('categories'));
    }

    public function specifications(Request $request)
    {
        $category = $this->categoryRepo->find($request['category_id']);
        $category->specifications()->sync($request['tags']);
        return redirect()->back()->with('success', 'تم اضافه سمات بنجاح');
    }
    /***************************  store category **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        if($request->has('image')){
            $data['icon'] = $this->uploadFile($request['image'],'categories');
        }
        $data['title'] = [
            'ar' =>$request['title_ar'],
            'en' =>$request['title_en'],
        ];
        $this->categoryRepo->create($data);
        return redirect()->back()->with('success', 'تم الاضافه بنجاح');
    }


    /***************************  update category  **************************/
    public function update(Update $request, $id)
    {
        $category = $this->categoryRepo->find($id);
        if($request->has('image')){
            $data['icon'] = $this->uploadFile($request['image'],'categories');
        }
        $data['title'] = [
            'ar' =>$request['title_ar'],
            'en' =>$request['title_en'],
        ];
        $this->categoryRepo->update($data,$category['id']);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete category  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $role = $this->categoryRepo->find($d);
                    $this->categoryRepo->delete($role['id']);
                }
            }
        }else {
            $role = $this->categoryRepo->find($id);
            $this->categoryRepo->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
