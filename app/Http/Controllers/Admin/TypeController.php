<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Type\Create;
use App\Http\Requests\Admin\Type\Update;
use App\Repositories\Interfaces\IType;
use App\Repositories\TypeRepository;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    use UploadTrait;
    protected $type;

    public function __construct(IType $type)
    {
        $this->type = $type;
    }

    /***************************  get all providers  **************************/
    public function index()
    {
        $types = $this->type->all();
        return view('admin.types.index', compact('types'));
    }


    /***************************  store provider **************************/
    public function store(Create $request)
    {
        $data = array_filter($request->all());
        $this->type->create($data);
        return redirect()->back()->with('success', 'تم الاضافه بنجاح');
    }


    /***************************  update provider  **************************/
    public function update(Update $request, $id)
    {
        $type = $this->type->find($id);
        $data = array_filter($request->all());
        $this->type->update($data,$type['id']);
        return redirect()->back()->with('success', 'تم التحديث بنجاح');
    }

    /***************************  delete provider  **************************/
    public function destroy(Request $request,$id)
    {
        if(isset($request['data_ids'])){
            $data = explode(',', $request['data_ids']);
            foreach ($data as $d){
                if($d != ""){
                    $this->type->delete($d);
                }
            }
        }else {
            $role = $this->type->find($id);
            $this->type->delete($role['id']);
        }
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

}
