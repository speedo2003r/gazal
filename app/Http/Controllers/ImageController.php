<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ImageController extends Controller
{
    public static function upload_single($file, $path)
    {
        $name = (time() * rand(1, 99)) . '.' . $file->getClientOriginalExtension();
        $destination = $path . '/';
        $file->storeAs($destination, $name, 'public');
        return $name;
    }
    public static function upload_multiple($request_file, $path)
    {
        foreach ($request_file as $file) {
            $name = (time() * rand(1, 99)) . '.' . $file->getClientOriginalExtension();
            $destination = $path . '/';
            $file->storeAs($destination, $name, 'public');
            $images[] = $name;
        }
        return $images;
    }

    public static function delete_image($image_name, $path)
    {
        if ($image_name && asset('storage/' . $path . '/' . $image_name)) {
            Storage::delete('public/' . $path . '/' . $image_name);
        }
    }
}
