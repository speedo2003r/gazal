<?php

namespace App\Http\Controllers;

use App\Traits\ResponseTrait;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController {
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ResponseTrait;

  public function authUser() {
    try {
      $user = auth()->userOrFail();
    } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
      return response()->json(['error' => $e->getMessage()]);
    }
    return $user;
  }

  public function changeLanguage($lang) {
    Session::put('applocale', $lang);
    return $this->ApiResponse('success', 'languages changed to: ' . $lang);
  }

  public function storeUuidInSession($uuid){
    Session::put('uuid', $uuid);
    return $this->ApiResponse('success', 'uuid saved to session');
  }
}
