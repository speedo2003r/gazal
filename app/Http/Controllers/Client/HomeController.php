<?php

namespace App\Http\Controllers\Client;

use App\Entities\Order;
use App\Http\Controllers\Controller;
use App\Http\Requests\Client\BeGreepRequest;
use App\Http\Requests\Client\BePartnerRequest;
use App\Http\Requests\Client\JoinRequest;
use App\Http\Requests\Client\SuggestRequest;
use App\Repositories\Interfaces\IBranch;
use App\Repositories\Interfaces\ICarType;
use App\Repositories\Interfaces\ICategory;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\IDelegateJoinUs;
use App\Repositories\Interfaces\IEmployeeJoinUs;
use App\Repositories\Interfaces\IGroup;
use App\Repositories\Interfaces\IJoinUs;
use App\Repositories\Interfaces\ISuggest;
use Illuminate\Http\Request;

class HomeController extends Controller {
  // use CountryCodeTrait;

  public $cityRepo,
  $groupRepo,
  $branchRepo,
  $suggestRepo,
  $delegateJoinRepo,
  $joinUsRepo,
  $carTypeRepo,
    $employJoinRepo;

  public function __construct(
    ICity $city,
    IGroup $group,
    ISuggest $suggest,
    IDelegateJoinUs $delegateJoin,
    IEmployeeJoinUs $employJoin,
    IJoinUs $joinUs,
    ICarType $carType,
    ICategory $category,
    IBranch $branch
  ) {
    $this->cityRepo         = $city;
    $this->groupRepo        = $group;
    $this->branchRepo       = $branch;
    $this->suggestRepo      = $suggest;
    $this->delegateJoinRepo = $delegateJoin;
    $this->employJoinRepo   = $employJoin;
    $this->joinUsRepo       = $joinUs;
    $this->carTypeRepo      = $carType;
    $this->categoryRepo     = $category;
  }

  public function home(Request $request) {
    if (auth()->check()) {
      return $this->categories();
    }

    $cities = $this->cityRepo->getCitiesWithProviders();

    //$allCountries = $this->getCountries();
    $allCountries = [];

    /**** get resturants like home in api after move to branch repo ****/
    // todo: protect password from return with data in model
    $group      = $this->groupRepo->findWhere(['category_id' => 1])->first() ?? [];
    $resturants = $group ? $this->branchRepo->getGroupProviders($request, $group['id']) : [];
    return view('front.client.home', compact('cities', 'resturants', 'allCountries'));
  }

  public function categories() {
    $mainCategories = $this->categoryRepo->mainCategories();
    return view('front.client.categories', compact('mainCategories'));
  }

  public function filter(Request $request, $category_id) {
    // ! get lat lng from prev request, pass to route

    // TODO: paginate data with livewire filter effect
    $group        = $this->groupRepo->findWhere(['category_id' => $category_id])->first();
    $mainCategory = $group->category;

    $categoryBranches = $this->branchRepo->getGroupProviders($request, $group['id']);
    $categoryBranches->load(['user', 'user.categories']);

    $providersCategories = clone $categoryBranches->pluck('user.categories')->unique()[0];
    return view('front.client.filter', compact('categoryBranches', 'providersCategories', 'mainCategory'));
  }

  public function providerDetails($branch_id) {
    $branch             = $this->branchRepo->find($branch_id);
    $user               = $branch->user;
    $provider           = $user->provider;
    $providerCategories = $user->categories;
    $openOrder          = $this->getOpenOrder($provider, $branch);

    return view('front.client.provider-details', compact(
      'branch',
      'user',
      'provider',
      'providerCategories',
      'openOrder',
    ));
  }

  private function getOpenOrder($provider, $branch) {
    $uuid = session()->get('uuid');

    if (auth()->check()) {
      $order = auth()->user()
        ->ordersAsUser()
        ->where('orders.live', 0)
        ->where('orders.branch_id', $branch->id)
        ->where('orders.user_id', $provider->id)
        ->first();
    } else {
      $order = Order::where('orders.uuid', $uuid)
        ->where('orders.live', 0)
        ->where('orders.branch_id', $branch->id)
        ->where('orders.user_id', null)
        ->where('orders.user_id', $provider->id)
        ->first();
    }
    return $order;
  }

  public function getJoinGreep() {
    $cities = $this->cityRepo->cities();
    return view('front.client.join-greep', compact('cities'));
  }

  public function storeJoinGreep(JoinRequest $request) {
    $this->employJoinRepo->create($request->validated());
    return $this->successResponse();
  }

  public function getBeGreep() {
    $cities         = $this->cityRepo->cities();
    $carTypes       = $this->carTypeRepo->cartypes();
    $mainCategories = $this->categoryRepo->mainCategories();
    return view('front.client.be-greep', compact('cities', 'carTypes', 'mainCategories'));
  }

  public function storeBeGreep(BeGreepRequest $request) {
    $this->delegateJoinRepo->create($request->validated());
    return $this->successResponse();
  }

  public function getBePartner() {
    $cities         = $this->cityRepo->cities();
    $mainCategories = $this->categoryRepo->mainCategories();
    return view('front.client.be-partner', compact('cities', 'mainCategories'));
  }

  public function storeBePartner(BePartnerRequest $request) {
    # for providers
    $this->joinUsRepo->create($request->validated());
    return $this->successResponse();
  }

  public function suggestions() {
    return view('front.client.suggestions');
  }

  public function storeSuggest(SuggestRequest $request) {
    // todo: update service using same way
    $this->suggestRepo->create($request->validated());
    return $this->successResponse();
  }
}
