<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Client\User\ActivationRequest;
use App\Http\Requests\Client\User\ForgetPasswordUpdateRequest;
use App\Http\Requests\Client\User\ForgetRequest;
use App\Http\Requests\Client\User\LoginRequest;
use App\Http\Requests\Client\User\RegisterRequest;
use App\Http\Requests\Client\User\UpdateProfileRequest;
use App\Repositories\Interfaces\IUser;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller {

  public function __construct(
    IUser $user
  ) {
    $this->userRepo = $user;
  }

  public function register(RegisterRequest $request) {
    $user = $this->userRepo->create($request->validated());
    $request->session()->put('user_phone', $user->phone);
    return $this->successResponse([], ['modal_id' => 'activate-modal']);
  }

  public function activation(ActivationRequest $request) {
    $data = $this->userRepo->activeUserPhone($request->validated());
    return $this->ApiResponse($data['key'], $data['msg'], [], ['redirect' => '/client/categories']);
  }

  public function login(LoginRequest $request) {
    $res = $this->userRepo->login($request->validated());

    switch ($res['key']) {
    case 'not_active':
      $data       = ['modal_id' => 'activate-modal'];
      $res['key'] = 'success';
      break;

    case 'success':
      $user = $res['user'];
      $data = ['redirect' => "/client/categories?lat=$user->lat&lng=$user->lng"];
      $request->session()->flash('success', $res['msg']);
      break;

    default:
      $data = [];
      break;
    }

    return $this->ApiResponse($res['key'], $res['msg'], [], $data);
  }

  public function logout() {
    // todo: check if othre devices logged in and update online
    Auth::logout();
    request()->session()->flash('success', awtTrans('تم تسجيل الخروج بنجاح'));
    return redirect()->route('client.home');
  }

  public function forgetPassword(ForgetRequest $request) {
    $res  = $this->userRepo->resendActiveCode($request->validated());
    $data = ['modal_id' => 'forget-update-modal'];
    return $this->ApiResponse($res['key'], $res['msg'], [], $data);
  }

  public function forgetPasswordUpdate(ForgetPasswordUpdateRequest $request) {
    $res  = $this->userRepo->updatePasswordByCode($request->validated());
    $data = 'success' == $res['key'] ? ['modal_id' => 'login-modal'] : [];
    return $this->ApiResponse($res['key'], $res['msg'], [], $data);
  }

  public function profile(UpdateProfileRequest $request) {
    auth()->user()->update($request->validated());
    return $this->ApiResponse('success', awtTrans('تم التحديث بنجاح'));
  }
}
