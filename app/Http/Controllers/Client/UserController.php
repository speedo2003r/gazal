<?php

namespace App\Http\Controllers\Client;

use App\Entities\OrderProduct;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\IFavourite;
use App\Repositories\Interfaces\IOrder;

class UserController extends Controller {
  public $favouriteRepo, $orderRepo;

  public function __construct(
    IFavourite $favourite,
    IOrder $order
  ) {
    $this->favouriteRepo = $favourite;
    $this->orderRepo     = $order;
  }

  public function prevOrders() {
    // todo: get real data
    return view('front.client.prev-orders');
  }

  public function favourite() {
    $favs     = $this->favouriteRepo->getFavs();
    $items    = $favs['items']->pluck('favoritable');
    $branches = $favs['branches']->pluck('favoritable');
    return view('front.client.favourite', compact('items', 'branches'));
  }

  public function wallet() {
    // todo: get real data
    return view('front.client.wallet');
  }

  public function orderSummary() {
    $cart          = $this->orderRepo->getCart();
    $count         = OrderProduct::where('order_id', $cart->id)->sum('qty');
    $cartPrice     = $cart->price();
    $orderProducts = $cart->orderProducts;
    $provider      = $cart->provider->provider;
    $branch        = $cart->branch;
    return view('front.client.order-summary', compact(
      'cart',
      'count',
      'cartPrice',
      'orderProducts',
      'provider',
      'branch',
    ));
  }

  public function createPaymentCard() {
    // todo: get real data
    return view('front.client.create-payment-card');
  }
}
