<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\IPage;
use App\Repositories\Interfaces\IQuestion;
use App\Repositories\Interfaces\ISocial;

class PageController extends Controller {
  public $pageRepo, $socialRepo, $questionRepo;

  public function __construct(
    IPage $page,
    IQuestion $question,
    ISocial $social
  ) {
    $this->pageRepo     = $page;
    $this->questionRepo = $question;
    $this->socialRepo   = $social;
  }

  public function aboutUs() {
    $about = $this->pageRepo->getAbout();
    return view('front.client.about-us', compact('about'));
  }

  public function contactUs() {
    // todo: update getContactInfo from repo
    $contactInfo = $this->pageRepo->getContactInfo();

    // todo: put socials in icon links
    $socialsArr = $this->socialRepo->getSocialsLinks();
    return view('front.client.contact-us', compact('contactInfo', 'socialsArr'));
  }

  public function privacyPolicy() {
    // todo: update getPrivacyPolicy from repo
    $privacyPolicy = $this->pageRepo->getPrivacyPolicy();
    return view('front.client.privacy-policy', compact('privacyPolicy'));
  }

  public function usagePolicy() {
    // todo: update getUsagePolicy from repo
    $usagePolicy = $this->pageRepo->getUsagePolicy();
    return view('front.client.usage-policy', compact('usagePolicy'));
  }

  public function faqs() {
    $faqs = $this->questionRepo->getQuestions();
    return view('front.client.faqs', compact('faqs'));
  }
}
