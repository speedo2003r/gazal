<?php

namespace App\Http\Controllers;

use App\Entities\CarType;
use App\Entities\Category;
use App\Entities\City;
use App\Entities\Company;
use App\Entities\Country;
use App\Entities\Delegate;
use App\Entities\Order;
use App\Entities\Provider;
use App\Entities\Zone;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Repositories\CityRepository;
use App\Repositories\CountryRepository;
use App\Repositories\Interfaces\ICity;
use App\Repositories\Interfaces\ICountry;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AjaxController extends Controller {
  use ResponseTrait;
  protected $city, $country;

  public function __construct(ICountry $country, ICity $city) {
    $this->country = $country;
    $this->city    = $city;
  }

  public function getCities(Request $request) {
    if ($request->ajax()) {
      $country_id = $request['id'];
      $country    = $this->country->find($country_id);
      $cities     = $country->Cities;
      return $this->successResponse($cities);
    }
  }

  public function getBranches(Request $request) {
    if ($request->ajax()) {
      $provider_id = $request['id'];
      $provider    = Provider::find($provider_id);
      $branches     = $provider->branches;
      return $this->successResponse($branches);
    }
  }

  public function getZones(Request $request) {
    if ($request->ajax()) {
      $city_id = $request['id'];
      $city    = $this->city->find($city_id);
      $zones   = $city->zones;
      return $this->successResponse($zones);
    }
  }

  public function getClientCount(Request $request) {
    if ($request->ajax()) {
      $country_id = $request['country_id'];
      $count      = User::where('user_type', 'client')->where('country_id', $country_id)->count();
      return $this->successResponse($count);
    }
  }

  public function getGroups(Request $request) {
    if ($request->ajax()) {
      $group_id = $request['id'];
      $category = Category::find($group_id);
      $groups   = $category->groups;
      return $this->successResponse($groups);
    }
  }

  public function getUserByZone(Request $request)
  {
      if($request->ajax()){
          if($request['id'] == null){
            $users = User::all();
          }else{
              $zone = Zone::find($request['id']);
              $users = $zone->users;
          }
          return response()->json($users);
      }
  }
  public function getSellers(Request $request) {
      if(isset($request['id'])){
          $sellers = Provider::where('city_id', $request['id'])->where('category_id', $request['category_id'])->get();
      }else{
          $sellers = Provider::where('category_id', $request['category_id'])->get();
      }
    return response()->json($sellers);
  }

  public function getProviders(Request $request) {
    if(isset($request['id'])){
        $sellers = Provider::where('city_id', $request['id'])->get();
    }
    return response()->json($sellers);
  }

  public function getItems(Request $request) {
    if ($request->ajax()) {
      $id    = $request->id;
      $category_id    = $request->category_id;
      $user  = Provider::find($id);
      $items = $user->items()->where('category_id',$category_id)->get();
      return response()->json($items);
    }
  }

  public function getCategories(Request $request) {
    if ($request->ajax()) {
      $id       = $request->id;
      $category = Category::find($id);
      $subs     = $category->children;
      return response()->json($subs);
    }
  }


  public function getAdminByRole(Request $request) {
    if ($request->ajax()) {
      $role       = $request->role;
      $role = Role::find($role);
      $subs     = $role->admins;
      return response()->json($subs);
    }
  }

  public function getDelegatesCarType(Request $request) {
    if ($request->ajax()) {
      $car_type_id       = $request->car_type_id;
      $carType = CarType::find($car_type_id);
      if(isset($request['hire_id'])){
          $hire = $request['hire_id'];
          $delegates = $carType->delegates()->whereDoesntHave('hires',function ($query) use ($hire){
              $query->whereDate('hire.from','<=',Carbon::now()->format('Y-m-d'))
              ->whereDate('hire.to','>=',Carbon::now()->format('Y-m-d'))
              ->where('hire_id','!=',$hire);
          })->get();
      }else{
          $delegates = $carType->delegates()->whereDoesntHave('hires',function ($query){
              $query->whereDate('hire.from','<=',Carbon::now()->format('Y-m-d'))->whereDate('hire.to','>=',Carbon::now()->format('Y-m-d'));
          })->get();
      }
      return response()->json($delegates);
    }
  }

  public function changeAccepted(Request $request) {
      if($request['type'] == 'delegate'){
          $user = Delegate::find($request['id']);
      }elseif($request['type'] == 'provider'){
          $user = Provider::find($request['id']);
      }elseif($request['type'] == 'company'){
          $user = Company::find($request['id']);
      }else{
          $user = User::find($request['id']);
      }
    if ($user->accepted == 1) {
      $user->accepted = 0;
    } else {
      $user->accepted = 1;
    }
    $user->save();
    return response()->json($user->accepted);
  }

    public function getDelegates(Request $request)
    {
        $order = Order::find($request['id']);
        $branch = $order->branch;
        if($branch){
            $lat = $branch->lat;
            $lng = $branch->lng;
            $delegates = Delegate::selectRaw("delegates.*,
                ( 6371 * acos( cos( radians(" . $lat . ") ) *
                cos( radians(lat) ) *
                cos( radians(lng) - radians(" . $lng . ") ) +
                sin( radians(" . $lat . ") ) *
                sin( radians(lat) ) ) )
             AS distance")
                ->orderBy("distance")->get();
            return response()->json($delegates);
        }
    }
}
