<?php

namespace App\Http\Middleware\Api;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json([
                    'key'=>0,
                    'value'=>0,
                    'msg'=>'Token is Invalid'
                ]);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json([
                    'key'=>0,
                    'value'=>-2,
                    'msg'=>'Token is Expired'
                ]);
            }else{
                return response()->json([
                    'key'=>0,
                    'value'=>0,
                    'msg'=>'Authorization Token not found'
                ]);
            }
        }
        return $next($request);
    }
}
