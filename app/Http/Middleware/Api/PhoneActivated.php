<?php

namespace App\Http\Middleware\Api;

use App\Traits\ResponseApi;
use App\Traits\ResponseTrait;
use Closure;

class PhoneActivated
{
    use ResponseTrait;
    public function handle($request, Closure $next)
    {
        if (auth('api')->check() && auth('api')->user()->active != 1) {
            return $this->ApiResponse( 'success', 'برجاء تفعيل الهاتف');
        }
        if (auth('api')->check() && auth('api')->user()->banned == 1) {
            return $this->ApiResponse( 'success', 'لقد تم حظرك من قبل الاداره');
        }
        if (auth('api')->check() && auth('api')->user()->accepted == 0) {
            return $this->ApiResponse( 'success', 'برجاء الرجوع للاداره لتفعيل حسابك');
        }
        return $next($request);
    }
}
