<?php

namespace App\Http\Middleware\Api;
use App\Http\Resources\Users\UserResource;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Route;

use Auth;
use Closure;
use Tymon\JWTAuth\JWTAuth;

class AuthCheck
{
    use ResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $token = \Tymon\JWTAuth\Facades\JWTAuth::getToken();
            if($token){
                $user = \Tymon\JWTAuth\Facades\JWTAuth::toUser($token);
            }
        }catch (\Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json([
                    'key' => 0,
                    'value' => 0,
                    'msg' => 'Token is Invalid'
                ]);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){

                try{
                    $newToken = \Tymon\JWTAuth\Facades\JWTAuth::refresh();
                    JWTAuth::setToken($newToken);
                    $user = \Tymon\JWTAuth\Facades\JWTAuth::toUser($newToken);
                    return response()->json([
                        'key'=>0,
                        'value'=>-2,
                        'data'=>$newToken
                    ]);
                }catch(\Exception $e){
                    return $this->ApiResponse('fail', 'fail');

                }
            }else{
                return response()->json([
                    'key'=>0,
                    'value'=>0,
                    'msg'=>'Authorization Token not found'
                ]);
            }
        }

        return $next($request);
    }
}
