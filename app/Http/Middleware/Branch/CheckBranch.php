<?php

namespace App\Http\Middleware\Branch;

use App\Entities\Branch;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Route;

use Auth;
use Closure;
use Tymon\JWTAuth\JWTAuth;

class CheckBranch
{
  use ResponseTrait;

  public function handle($request, Closure $next)
  {
    if (!auth('branchApi')->check()
    ) {
      return $this->ApiResponse('fail', trans('api.mustProvider'));
    }

    if ((auth('branchApi')->check()
        && get_class(auth('branchApi')->user()) != Branch::class)
      || (auth('api')->check()
        && auth('api')->user()['user_type'] != 'provider')
    ) {
      return $this->ApiResponse('fail', trans('api.mustProvider'));
    }

    if (auth('branchApi')->check()
      && get_class(auth('branchApi')->user()) == Branch::class
      && auth('branchApi')->user()->status != 1
    ) {
      return $this->ApiResponse('not_active', 'برجاء الرجوع لمسئول الادراه لتفعيلك');
    }

    return $next($request);
  }
}
