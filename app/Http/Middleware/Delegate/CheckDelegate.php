<?php

namespace App\Http\Middleware\Delegate;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Route;

use Auth;
use Closure;
use Tymon\JWTAuth\JWTAuth;

class CheckDelegate
{
    use ResponseTrait;
    public function handle($request, Closure $next)
    {
        if(!auth('delegateApi')->check()){
            return $this->ApiResponse('fail', trans('api.mustDelegate'));
        }
        return $next($request);
    }
}
