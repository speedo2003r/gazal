<?php

namespace App\Http\Middleware;

use Closure;

class FixPhone
{
  public function handle($request, Closure $next)
  {
    if ($request->phone) {
      $request['phone'] = ltrim(convert_to_english($request->phone), '0');
    }

    if ($request->country_code) {
      $request['country_code'] = ltrim(convert_to_english($request->country_code), '00');
    }

    return $next($request);
  }
}
