<?php

namespace App\Http\Middleware\Provider;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SellerAuth
{
    public function handle($request, Closure $next)
    {

        app()->setLocale('ar');
        //if user not login
        if (!Auth::guard('provider')->check()){
            $msg = trans('قم بتسجيل دخولك اولا');
            if ($request->ajax()) return response()->json(['value' => 0, 'msg' => $msg]);
            return redirect()->route('provider.login')->with('danger', $msg);
        }


        if (Auth::guard('provider')->user()->banned == 1) {
            auth('provider')->logout();
            $msg = trans('حسابك غير مفعل ارجو مراجعة الادارة');
            if ($request->ajax()) return response()->json(['value' => 0, 'msg' => $msg]);
            return redirect()->route('provider.login')->with('danger', $msg);
        }
        if (Auth::guard('provider')->user()->accepted == 0) {
            auth('provider')->logout();
            $msg = trans('حسابك غير مفعل ارجع للفرع الرئيسي لاظهاره');
            if ($request->ajax()) return response()->json(['value' => 0, 'msg' => $msg]);
            return redirect()->route('provider.login')->with('danger', $msg);
        }
        if (Auth::guard('provider')->user()->active == 0) {
            return redirect()->route('provider.active');
        }

        return $next($request);
    }
}
