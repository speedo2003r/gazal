<?php

namespace App\Http\Middleware\Provider;

use App\Entities\Branch;
use Closure;
use Illuminate\Http\Request;

class Branches
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $branch = Branch::find($request->route('branch'));
        if ($branch instanceof Branch) {
            if (auth('provider')->user()->isProvider() && $branch->provider_id != auth('provider')->user()->provider_id) {
                return abort(404);
            }

            if (!auth('provider')->user()->isProvider() && !in_array($branch->id, auth('provider')->user()->employee_branches()->pluck('branches.id'))) {
                return abort(404);
            }
        }

        return $next($request);
    }
}
