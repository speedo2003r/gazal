<?php

namespace App\Http\Middleware\Provider;

use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\App;

class SellerLang
{
    use ResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $local = session()->get('lang');
        if ($local == null){
            $local = session()->put('lang','ar');
        }
        App::setLocale($local);
        Carbon::setLocale($local);

        return $next($request);
    }
}
