<?php

namespace App\Http\Middleware;

use App\Traits\ResponseTrait;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckUserType {
  use ResponseTrait;

  public function handle(Request $request, Closure $next, $type) {
    if (auth()->check() && auth()->user()->user_type != $type) {
      $msg = awtTrans('لاتمتلك حساب على موقع العميل');
      Auth::logout();
      $request->session()->flash('danger', $msg);
      return redirect()->route('client.home');
    }

    return $next($request);
  }
}
