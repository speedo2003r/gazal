<?php

namespace App\Http\Requests\Api\Delegate;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class DelegateRegisterRequest extends FormRequest {
  use ResponseTrait;
  public function __construct(Request $request) {
    $request['v_code']    = generateCode();
    $request['user_type'] = 'delegate';
    // $request['lang']      = 'ar';
    // $request['role_id']   = null;
  }

  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'name'              => 'required|min:4|max:191',
      'country_code'      => 'required|digits_between:2,7',
      'phone'             => 'required|numeric|unique:delegates,phone,NULL,id,deleted_at,NULL',
      'email'             => 'required|email|unique:delegates,email,NULL,id,deleted_at,NULL',
      'password'          => 'required|min:6|max:255',
      'birth_date'        => 'required|date_format:Y-m-d',
      'city_id'           => 'required|exists:cities,id',
      'car_type_id'       => 'required|exists:car_types,id',
      'avatar'            => 'required',
      'licence_car_image' => 'required',
      'licence_image'     => 'required',
      'car_front_image'   => 'required',
      'car_back_image'    => 'required',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
