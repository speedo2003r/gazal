<?php

namespace App\Http\Requests\Api\register;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class ActivationRequest extends FormRequest {
  use ResponseTrait;
    protected $model;
    public function __construct(Request $request)
    {
        if ($request['user_type'] == 'client'){
            $this->model = 'users';
        }elseif($request['user_type'] == 'delegate'){
            $this->model = 'delegates';
        }
    }
  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'code'         => 'required',
      'user_type'    => 'required|in:client,delegate',
      'country_code' => 'required|digits_between:2,7',
      'phone'        => 'required|exists:'.$this->model.',phone,deleted_at,NULL',
      'uuid'         => 'required',
      'device_id'    => 'required',
      'device_type'  => 'required:in,ios,android,web',
      'lang'         => 'required',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
