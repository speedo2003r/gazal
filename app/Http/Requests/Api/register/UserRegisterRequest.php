<?php

namespace App\Http\Requests\Api\register;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class UserRegisterRequest extends FormRequest {
  use ResponseTrait;

  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'name'              => 'required|min:2|max:191',
      'country_code'      => 'required|digits_between:2,7',
      'phone'             => 'required|numeric|digits_between:9,13|unique:users,phone,NULL,id,deleted_at,NULL',
      'email'             => 'required|email|max:191|unique:users,email,NULL,id,deleted_at,NULL',
      'password'          => 'required|min:6|max:255',
      'birth_date'        => 'required|date_format:Y-m-d',
      'gender'            => 'required|in:male,female',
      'friend_share_code' => 'nullable',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
