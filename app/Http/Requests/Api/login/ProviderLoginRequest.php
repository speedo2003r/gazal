<?php

namespace App\Http\Requests\Api\login;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class ProviderLoginRequest extends FormRequest {
  use ResponseTrait;


  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'uuid'        => 'required',
      'email'       => 'required|email|exists:branches,email,deleted_at,NULL',
      'password'    => 'required|max:100',
      'device_id'   => 'required|max:200',
      'device_type' => 'required|in:android,ios,web',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
