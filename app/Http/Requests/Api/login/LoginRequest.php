<?php

namespace App\Http\Requests\Api\login;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class LoginRequest extends FormRequest {
  use ResponseTrait;

    protected $model;
    public function __construct(Request $request)
    {
        if ($request['user_type'] == 'client'){
            $this->model = 'users';
        }elseif($request['user_type'] == 'delegate'){
            $this->model = 'delegates';
        }
    }
  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'uuid'         => 'required',
      'country_code' => 'required|exists:'.$this->model.',country_code',
      'phone'        => 'exists:'.$this->model.',phone,deleted_at,NULL',
      'password'     => 'required|max:100',
      'device_id'    => 'required|max:200',
      'device_type'  => 'required|in:android,ios,web',
      'user_type'    => 'required|in:delegate,client',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
