<?php

namespace App\Http\Requests\Client;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class SuggestRequest extends FormRequest {
  use ResponseTrait;

  public function __construct(Request $request) {
    if (auth()->check()) {
      $request['user_id'] = auth()->id();
    }
  }

  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'name'  => 'required|string|max:30',
      'notes' => 'required|string|max:500',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
