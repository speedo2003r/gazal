<?php

namespace App\Http\Requests\Client;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class BeGreepRequest extends FormRequest {
  use ResponseTrait;

  public function __construct(Request $request) {
    // if (auth()->check()) {
    //   $request['user_id'] = auth()->id();
    // }
  }

  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'avatar'            => 'required|image',
      'name'              => 'required|max:50',
      'phone'             => 'required|max:20',
      'phone2'            => 'required|max:20',
      'email'             => 'required|email|max:30',
      'birth_date'        => 'required|date',
    //   'category_id'       => 'required|exists:categories,id',
      'city_id'           => 'required|exists:cities,id',
      'car_type_id'       => 'required|exists:car_types,id',
      'licence_car_image' => 'required|image',
      'licence_image'     => 'required|image',
      'car_back_image'    => 'required|image',
    //   'password'          => 'required|confirmed|max:191',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
