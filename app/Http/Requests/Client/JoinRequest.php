<?php

namespace App\Http\Requests\Client;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class JoinRequest extends FormRequest {
  use ResponseTrait;

  public function __construct(Request $request) {
    // if (auth()->check()) {
    //   $request['user_id'] = auth()->id();
    // }
  }

  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'name'            => 'required|string|max:30',
      'phone'           => 'required|string|max:20',
      'city_id'         => 'required|exists:cities,id',
      'job_description' => 'required|string|max:500',
      'email'           => 'required|email|max:30',
      'desc'            => 'required',
      'cv'              => 'required|mimes:pdf',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
