<?php

namespace App\Http\Requests\Client\User;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class ForgetPasswordUpdateRequest extends FormRequest {
  use ResponseTrait;

  public function __construct(Request $request) {
    $request['phone'] = $request->session()->get('user_phone');
  }

  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'phone'    => 'required|exists:users,phone',
      'code'     => 'required|max:6',
      'password' => 'required|min:6|max:255|confirmed',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
