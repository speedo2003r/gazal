<?php

namespace App\Http\Requests\Client\User;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UpdateProfileRequest extends FormRequest {
  use ResponseTrait;

  public function __construct(Request $request) {
    $request['phone'] ? $request['phone'] = convert_to_english($request['phone']) : '';
  }

  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'name'         => 'sometimes|required|min:2|max:191',
      'phone'        => 'sometimes|required|numeric|digits_between:9,13|unique:users,phone,' . auth()->id(),
      'email'        => 'sometimes|required|email|max:191|unique:users,email,' . auth()->id(),
      'birth_date'   => 'sometimes|required',
      'gender'       => 'sometimes|required|in:male,female',
      'password'     => 'sometimes|required|min:6|max:255|confirmed',
      'old_password' => 'sometimes|required|min:6|max:255',
    ];
  }

  public function withValidator($validator) {
    $validator->after(function ($validator) {
      if ($this->has('old_password') && !Hash::check($this->old_password, auth()->user()->password)) {
        $validator->errors()->add('old_password', trans('api.wrongPassword'));
      }
    });
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
