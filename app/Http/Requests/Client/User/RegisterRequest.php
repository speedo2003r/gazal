<?php

namespace App\Http\Requests\Client\User;

use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class RegisterRequest extends FormRequest {
  use ResponseTrait;

  public function __construct(Request $request) {
    $request['v_code']    = generateCode();
    $request['user_type'] = 'client';
    $request['lang']      = 'ar';
    $request['role_id']   = null;

    if (!request()->has('email')) {
      $lastId           = User::all()->last()->id ?? 0;
      $sum              = $lastId + 1;
      $request['email'] = 'example' . $sum . '@gazal.com';
    }
  }

  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'name'       => 'required|min:2|max:191',
      'phone'      => 'required|numeric|digits_between:9,13|unique:users,phone',
      'email'      => 'required|email|max:191|unique:users,email',
      'birth_date' => 'required',
      'gender'     => 'required|in:male,female',
      'password'   => 'required|min:6|max:255|confirmed',
      'v_code'     => 'nullable',
      'user_type'  => 'nullable',
      'lang'       => 'nullable',
      'role_id'    => 'nullable',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
