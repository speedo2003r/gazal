<?php

namespace App\Http\Requests\Client\User;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class LoginRequest extends FormRequest {
  use ResponseTrait;

  public function __construct(Request $request) {
  }

  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'phone'    => 'required|exists:users,phone',
      'password' => 'required|min:6',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
