<?php

namespace App\Http\Requests\Client\User;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class ActivationRequest extends FormRequest {
  use ResponseTrait;

  public function __construct(Request $request) {
    $request['phone'] = $request->session()->get('user_phone');
    $request['code']  = implode("",$request->code);
  }

  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'code'  => 'required|min:6|max:10',
      'phone' => 'required|exists:users,phone',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
