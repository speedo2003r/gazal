<?php

namespace App\Http\Requests\Client;

use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class BePartnerRequest extends FormRequest {
  use ResponseTrait;

  public function __construct(Request $request) {
    // if (auth()->check()) {
    //   $request['user_id'] = auth()->id();
    // }
  }

  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'name'        => 'required|max:50',
      'store_name'  => 'required|max:50',
      'category_id' => 'required|exists:categories,id',
      'phone'       => 'required|max:20',
      'email'       => 'required|email|max:50',
      'city_id'     => 'required|exists:cities,id',
    ];
  }

  protected function failedValidation(Validator $validator) {
    throw new HttpResponseException($this->ApiResponse('fail', $validator->errors()->first()));
  }
}
