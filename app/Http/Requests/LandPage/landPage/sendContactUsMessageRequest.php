<?php

namespace App\Http\Requests\LandPage\landPage;

use Illuminate\Foundation\Http\FormRequest;

class sendContactUsMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST': {

                    return [
                        'name' => 'required',
                        'phone' => 'required',
                        'address' => 'required',
                        'title' => 'required',
                        'message' => 'required',
                    ];
                }
            case 'PUT': {
                    return [
                        'description_ar' => 'required',
                        'description_en' => 'nullable',
                        'status' => 'nullable|in:available,unavailable',
                        'image' => 'nullable|image|mimes:jpg,jpeg,png',
                    ];
                }
            default:
                break;
        }
    }
}