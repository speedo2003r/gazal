<?php

namespace App\Http\Requests\LandPage\Admin;

use Illuminate\Foundation\Http\FormRequest;

class statementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST': {
                    return [
                        'title_ar' => 'required',
                        'title_en' => 'nullable',
                        'text_ar' => 'required',
                        'text_en' => 'nullable',
                        'place' => 'nullable',
                        'image'   => 'nullable|image|mimes:jpg,jpeg,png',
                        'status'  => 'nullable|in:available,unavailable'
                    ];
                }
                break;
            case 'PUT': {
                    return [
                        'title_ar' => 'required',
                        'title_en' => 'nullable',
                        'text_ar' => 'required',
                        'text_en' => 'nullable',
                        'place' => 'nullable',
                        'image'   => 'nullable|image|mimes:jpg,jpeg,png',
                        'status'  => 'nullable|in:available,unavailable'
                    ];
                }
                break;
            default:
                break;
        }
    }
}