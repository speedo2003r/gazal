<?php

namespace App\Http\Requests\LandPage\Admin;

use Illuminate\Foundation\Http\FormRequest;

class contentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST': {

                    return [
                        'title_ar' => 'required',
                        'title_en' => 'nullable',
                        'text_ar' => 'required',
                        'text_en' => 'nullable',
                        'status' => 'nullable|in:available,unavailable',
                        'image' => 'required|image|mimes:jpg,jpeg,png',
                    ];
                }
            case 'PUT': {
                    return [
                        'title_ar' => 'required',
                        'title_en' => 'nullable',
                        'text_ar' => 'required',
                        'text_en' => 'nullable',
                        'status' => 'nullable|in:available,unavailable',
                        'image' => 'nullable|image|mimes:jpg,jpeg,png',
                    ];
                }
            default:
                break;
        }
    }
}