<?php

namespace App\Http\Requests\LandPage\Admin;

use Illuminate\Foundation\Http\FormRequest;

class contactDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST': {

                    return [
                        'location' => 'required',
                        'lat' => 'nullable',
                        'lng' => 'nullable',
                        'address' => 'nullable',
                        'phone1' => 'required',
                        'phone2' => 'nullable',
                        'email' => 'required',
                        'work_time_from' => 'required',
                        'work_time_to' => 'required',
                        'work_day_from' => 'nullable',
                        'work_day_to' => 'nullable',
                        'note1' => 'required',
                        'note2' => 'required',
                    ];
                }
            case 'PUT': {
                    return [
                        'location' => 'required',
                        'lat' => 'nullable',
                        'lng' => 'nullable',
                        'address' => 'nullable',
                        'phone1' => 'required',
                        'phone2' => 'nullable',
                        'email' => 'required',
                        'work_time_from' => 'required',
                        'work_time_to' => 'required',
                        'work_day_from' => 'nullable',
                        'work_day_to' => 'nullable',
                        'note1' => 'required',
                        'note2' => 'required',
                    ];
                }
            default:
                break;
        }
    }
}