<?php

namespace App\Http\Requests\LandPage\Admin;

use Illuminate\Foundation\Http\FormRequest;

class fqsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST': {

                    return [
                        'question_ar' => 'required',
                        'question_en' => 'nullable',
                        'answer_ar' => 'required',
                        'answer_en' => 'nullable',
                        'status' => 'nullable|in:available,unavailable',
                    ];
                }
            case 'PUT': {
                    return [
                        'question_ar' => 'required',
                        'question_en' => 'nullable',
                        'answer_ar' => 'required',
                        'answer_en' => 'nullable',
                        'status' => 'nullable|in:available,unavailable',
                    ];
                }
            default:
                break;
        }
    }
}