<?php

namespace App\Http\Requests\LandPage\Admin;

use Illuminate\Foundation\Http\FormRequest;

class aboutStatementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST': {

                    return [
                        'text_ar' => 'required',
                        'text_en' => 'required',
                        'status' => 'nullable|in:available,unavailable',
                    ];
                }
            case 'PUT': {
                    return [
                        'text_ar' => 'required',
                        'text_en' => 'required',
                        'status' => 'nullable|in:available,unavailable',
                    ];
                }
            default:
                break;
        }
    }
}