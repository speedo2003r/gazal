<?php

namespace App\Http\Requests\LandPage\Admin;

use Illuminate\Foundation\Http\FormRequest;

class slidersRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        switch ($this->method()) {

            case 'POST': {
                    return [
                        // 'title_ar' => 'required',
                        // 'title_en' => 'required',
                        'image'   => 'required|image|mimes:jpg,jpeg,png',
                        'status'  => 'required|in:available,unavailable'
                    ];
                }
                break;
            case 'PUT': {
                    return [
                        // 'title_ar' => 'required',
                        // 'title_en' => 'required',
                        'image'   => 'nullable|image|mimes:jpg,jpeg,png',
                        'status'  => 'required|in:available,unavailable'
                    ];
                }
                break;
            default:
                break;
        }
    }
}