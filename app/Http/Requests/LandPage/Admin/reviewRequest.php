<?php

namespace App\Http\Requests\LandPage\Admin;

use Illuminate\Foundation\Http\FormRequest;

class reviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST': {

                    return [
                        'comment' => 'required',
                        'name' => 'required',
                        'phone' => 'nullable',
                        'about' => 'required',
                        'status' => 'nullable|in:available,unavailable',
                        'image' => 'nullable|image|mimes:jpg,jpeg,png',
                    ];
                }
            case 'PUT': {
                    return [
                        'comment' => 'required',
                        'name' => 'required',
                        'phone' => 'nullable',
                        'about' => 'required',
                        'status' => 'nullable|in:available,unavailable',
                        'image' => 'nullable|image|mimes:jpg,jpeg,png',
                    ];
                }
            default:
                break;
        }
    }
}