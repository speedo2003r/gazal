<?php

namespace App\Http\Requests\Admin\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function __construct(Request $request)
    {
        $request['active'] = 1;
        $request['v_code']  = generateCode();
    }
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required|max:191',
            'phone'     => 'required|numeric|unique:users,phone,NULL,id,deleted_at,NULL',
            'email'     => 'required|email|max:191|unique:admins,email,NULL,id,deleted_at,NULL',
            'password'  => 'required|max:191',
            'image'    => 'nullable|image|mimes:jpeg,jpg,png',
            'role_id'   => 'required|exists:roles,id',
            'address'   => 'required|min:3|max:254',
            'country_id'   => 'required|exists:countries,id,deleted_at,NULL',
            'city_id'   => 'required|exists:cities,id,deleted_at,NULL',
        ];
    }
}
