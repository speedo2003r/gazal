<?php

namespace App\Http\Requests\Admin\Banner;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Update extends FormRequest
{

    public function __construct(Request $request)
    {
        $request['user_id'] = $request['provider_id'];
    }
    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'title.ar' => 'required|max:191',
            'title.en'  => 'required|max:191',
            'type'  => 'required|in:seller,item',
            'image'  => 'nullable|file|image|mimes:jpeg,jpg,png',
            'country_id'  => 'required|exists:countries,id',
            'city_id'  => 'required|exists:cities,id',
            'start_date'  => 'required|before:end_date',
            'end_date'  => 'required|after:start_date',
            'category_id'  => 'required|exists:categories,id',
            'user_id'  => 'required|exists:users,id',
            'item_id'  => 'nullable|exists:items,id',
        ];
    }
}
