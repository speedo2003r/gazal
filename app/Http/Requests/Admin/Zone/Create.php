<?php

namespace App\Http\Requests\Admin\Zone;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest {
  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'title.ar' => 'required|max:191',
      'title.en' => 'required|max:191',
      'city_id'  => 'required|exists:cities,id',
    ];
  }
}
