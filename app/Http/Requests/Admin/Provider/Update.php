<?php

namespace App\Http\Requests\Admin\Provider;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function __construct()
    {
        $request['v_code']  = generateCode();
    }
    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'username'        => 'required|max:191',
            'name' => 'required|max:191',
            'slogan' => 'sometimes|max:191',
            'phone'      => "required|numeric|digits_between:9,13|unique:providers,phone,{$this->id},id,deleted_at,NULL",
            'phones[]'        => 'sometimes|numeric|digits_between:9,13',
            'email'      => "required|email|max:191|unique:providers,email,{$this->id},id,deleted_at,NULL",
            'emails[]'        => 'sometimes|email|max:191',
            'password'   => 'nullable|confirmed|max:191',
            'store_name.ar'   => 'required|max:191',
            'store_name.en'   => 'required|max:191',
            'category_id'   => 'required|exists:categories,id,deleted_at,NULL',
            'group_id'   => 'required',
            'image'      => 'nullable|file|image|mimes:jpeg,jpg,png',
            'country_id'    => 'required',
            'city_id'    => 'required',
            'address'    => 'required',
            'lat'        => 'required',
            'lng'        => 'required',
            'processing_time'        => 'required',
            'commercial_num' => 'nullable|digits_between:7,10',
            'acc_bank' => 'nullable|digits_between:20,24',
            'commission' => 'nullable',
        ];
    }
    public function attributes(){
        return [
            'commercial_num' => 'رقم السجل التجاري',
            'acc_bank' => 'رقم الحساب البنكي',
            'group_id' => 'الفئه',
            'processing_time' => 'وقت التجهيز',
            'store_name.ar' => 'اسم المتجر بالعربي',
            'store_name.en' => 'اسم المتجر بالانجليزي',
        ];
    }

}
