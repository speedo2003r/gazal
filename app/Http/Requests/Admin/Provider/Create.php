<?php

namespace App\Http\Requests\Admin\Provider;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function __construct(Request $request)
    {
        $request['active'] = 1;
        $request['v_code']  = generateCode();
    }
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:191',
            'name' => 'required|max:191',
            'slogan' => 'sometimes|max:191',
            'phone'      => 'required|numeric|digits_between:9,13|unique:providers,phone,NULL,id,deleted_at,NULL',
            'email'      => 'required|email|max:191|unique:providers,email,NULL,id,deleted_at,NULL',
            'password'   => 'required|min:6|max:191',
            'store_name.ar'   => 'required|max:191',
            'store_name.en'   => 'required|max:191',
            'category_id'   => 'required|exists:categories,id,deleted_at,NULL',
            'group_id[]'   => 'required',
            'image'      => 'nullable|file|image|mimes:jpeg,jpg,png',
            'country_id'    => 'required',
            'city_id'    => 'required',
            'address'    => 'required',
            'lat'        => 'required',
            'lng'        => 'required',
            'processing_time'        => 'required',
            'commercial_num' => 'nullable|digits_between:7,10',
            'acc_bank' => 'nullable|digits_between:20,24',
            'commission' => 'nullable|min:1|max:99|size:2',
            'representative_id' => 'required|exists:employees,id',
            'account_manager_id' => 'required|exists:admins,id,deleted_at,NULL',
            'active_responsible_id' => 'required|exists:admins,id,deleted_at,NULL',
            'accountant_id' => 'required|exists:admins,id,deleted_at,NULL',
        ];
    }
    public function attributes(){
        return [
            'commercial_num' => 'رقم السجل التجاري',
            'acc_bank' => 'رقم الحساب البنكي',
            'group_id' => 'الفئه',
            'processing_time' => 'وقت التجهيز',
            'store_name.ar' => 'اسم المتجر بالعربي',
            'store_name.en' => 'اسم المتجر بالانجليزي',
            'representative_id' => 'مندوب التسويق',
            'account_manager_id' => 'مدير الحساب',
            'active_responsible_id' => 'مسئول التفعيل',
            'accountant_id' => 'المحاسب',
        ];
    }
}
