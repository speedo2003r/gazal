<?php

namespace App\Http\Requests\Admin\Coupon;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'title' => 'required',
            'limit' => 'required',
            'country_id' => 'required|exists:countries,id,deleted_at,NULL',
            'city_id' => 'required|exists:cities,id,deleted_at,NULL',
            'max_percent_amount' => 'required',
            'min_order_amount' => 'required',
            'kind' => 'required',
            'code' => 'required',
            'type' => 'required',
            'count'  => 'sometimes',
            'value'  => 'required',
            'start_date'  => 'required|before:end_date',
            'end_date'  => 'required|after:start_date',
        ];
    }
    public function messages()
    {
        return [
            'limit' => __('admin.usedCount'),
            'code' => __('admin.CouponCode'),
            'max_percent_amount' => __('admin.maxPercentAmount'),
            'min_order_amount' => __('admin.MinimumReduction'),
        ];
    }
}
