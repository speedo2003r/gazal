<?php

namespace App\Http\Requests\Admin\Page;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'title.ar' => 'required|max:191',
            'title.en'  => 'required|max:191',
            'desc.ar'  => 'required',
            'desc.en'  => 'required',
        ];
    }
}
