<?php

namespace App\Http\Requests\Admin\TopList;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'provider_id'      => 'required|exists:providers,id,deleted_at,NULL',
            'sort'      => 'required',
            'from' => 'required|before:to',
            'to' => 'required|after:from',
        ];
    }

}
