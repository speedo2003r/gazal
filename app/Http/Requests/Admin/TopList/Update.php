<?php

namespace App\Http\Requests\Admin\TopList;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'provider_id'      => 'required|exists:providers,id,deleted_at,NULL',
            'sort'      => 'required',
            'from' => 'required|before:to',
            'to' => 'required|after:from',
        ];
    }

}
