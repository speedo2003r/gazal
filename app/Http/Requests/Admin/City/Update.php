<?php

namespace App\Http\Requests\Admin\City;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'title.ar' => 'required|max:191',
            'title.en'  => 'required|max:191',
            'country_id'  => 'required|exists:countries,id',
        ];
    }
}
