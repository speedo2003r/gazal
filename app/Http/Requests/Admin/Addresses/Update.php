<?php

namespace App\Http\Requests\Admin\Addresses;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'country_id'      => 'required|exists:countries,id,deleted_at,NULL',
            'city_id'      => 'required|exists:cities,id,deleted_at,NULL',
            'name' => 'required',
            'street'      => 'required',
            'building'   => 'required',
            'floor'      => 'required',
            'flat'      => 'required',
            'unique_sign'      => 'required',
            'lat'      => 'required',
            'lng'      => 'required',
            'map_desc'      => 'required',
            'phone'      => 'sometimes|digits_between:9,13',
            'type'      => 'required|in:work,home,esteraha',
        ];
    }

    public function attributes()
    {
        return [
            'street'      => awtTrans('الشارع'),
            'building'   => awtTrans('رقم المبني'),
            'floor'      => awtTrans('رقم الطابق'),
            'flat'      => awtTrans('رقم الشقه'),
            'unique_sign'      => awtTrans('علامه مميزه'),
            'type'      => awtTrans('نوع العنوان'),
        ];
    }
}
