<?php

namespace App\Http\Requests\Admin\CallCenter;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lat'  => 'required',
            'lng'  => 'required',
            'phone'  => 'required|numeric|digits_between:9,13|unique:branches,phone,'.$this->id.',id,deleted_at,NULL',
            'email'  => 'required|email|max:191|unique:branches,email,'.$this->id.',id,deleted_at,NULL',
            'password'  => 'nullable|confirmed|min:3,max:191',
            'country_id'  => 'required|exists:countries,id,deleted_at,NULL',
            'city_id'  => 'required|exists:cities,id,deleted_at,NULL',
            'address'  => 'required|max:191',
        ];
    }
}
