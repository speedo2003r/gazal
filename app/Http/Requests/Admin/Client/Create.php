<?php

namespace App\Http\Requests\Admin\Client;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function __construct(Request $request)
    {
        $request['active'] = 1;
        $request['v_code']  = generateCode();
    }
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name' => 'required|max:191',
            'phone'      => 'required|numeric|digits_between:9,13|unique:users,phone,NULL,id,deleted_at,NULL',
            'phone_code'      => 'required',
            'email'      => 'required|email|max:191|unique:users,email,NULL,id,deleted_at,NULL',
            'password'   => 'required|confirmed|max:191',
            'image'      => 'sometimes|image|mimes:jpeg,jpg,png',
            'birth_date'      => 'required|date',
            'gender'      => 'required|in:male,female',
            'country_id'      => 'nullable|exists:countries,id,deleted_at,NULL',
            'city_id'      => 'nullable|exists:cities,id,deleted_at,NULL',
        ];
    }

    public function attributes()
    {
        return [
            'phone_code' => awtTrans('كود الدوله'),
            'birth_date' => awtTrans('تاريخ الميلاد'),
        ];
    }
}
