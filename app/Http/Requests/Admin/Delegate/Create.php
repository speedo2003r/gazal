<?php

namespace App\Http\Requests\Admin\Delegate;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'phone'      => 'required|numeric|digits_between:9,13|unique:delegates,phone,NULL,id,deleted_at,NULL',
            'email'      => 'required|email|max:191|unique:delegates,email,NULL,id,deleted_at,NULL',
            'password'   => 'required|max:191|confirmed',
            'birth_date'   => 'required',
            'image'      => 'nullable|mimes:jpeg,jpg,png',
            'car_type_id'    => 'required|exists:car_types,id,deleted_at,NULL',
            'country_id'    => 'required|exists:countries,id,deleted_at,NULL',
            'city_id'    => 'required|exists:cities,id,deleted_at,NULL',
            'company_id'    => 'required|exists:companies,id,deleted_at,NULL',
            'lat'    => 'required',
            'lng'    => 'required',
            'address'    => 'required',
            'commission' => 'required',
            'commission_status' => 'required',
            'max_dept' => 'required',
        ];
    }
    public function attributes(){
        return [
            'commission' => 'العموله',
            'commission_status' => 'العموله قيمه أو نسبه',
            'birth_date' => 'تاريخ الميلاد',
            'car_type_id' => 'نوع السياره',
        ];
    }
}
