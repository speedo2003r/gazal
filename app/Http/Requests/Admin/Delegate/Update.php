<?php

namespace App\Http\Requests\Admin\Delegate;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'name' => 'required|max:191',
            'phone'      => "required|numeric|digits_between:9,13|unique:delegates,phone,{$this->id},id,deleted_at,NULL",
            'email'      => "required|email|max:191|unique:delegates,email,{$this->id},id,deleted_at,NULL",
            'password'   => 'nullable|max:191|confirmed',
            'birth_date'   => 'required',
            'image'      => 'nullable|mimes:jpeg,jpg,png',
            'car_type_id'    => 'required|exists:car_types,id,deleted_at,NULL',
            'country_id'    => 'required|exists:countries,id,deleted_at,NULL',
            'city_id'    => 'required|exists:cities,id,deleted_at,NULL',
            'company_id'    => 'required|exists:companies,id,deleted_at,NULL',
            'lat'    => 'required',
            'lng'    => 'required',
            'address'    => 'required',
            'commission' => 'required',
            'commission_status' => 'required',
            'max_dept' => 'required',
        ];
    }
    public function attributes(){
        return [
            'commission' => 'العموله',
            'commission_status' => 'العموله قيمه أو نسبه',
        ];
    }
}
