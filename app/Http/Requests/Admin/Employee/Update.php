<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'avatar' => 'nullable|mimes:jpg,png,jpeg',
            'name' => 'required|max:191',
            'phone'      => "required|numeric|digits_between:9,13|unique:employees,phone,{$this->id},id",
            'email'      => "required|email|max:191|unique:employees,email,{$this->id},id",
            'address'      => 'required',
        ];
    }
}
