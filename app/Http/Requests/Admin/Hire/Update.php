<?php

namespace App\Http\Requests\Admin\Hire;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'provider_id'      => 'required|exists:providers,id,deleted_at,NULL',
            'car_type_id'      => 'required|exists:car_types,id,deleted_at,NULL',
            'amount'      => 'required',
            'delegates_count'      => 'required',
            'from'      => 'required',
            'to'      => 'required',
        ];
    }
}
