<?php

namespace App\Http\Requests\Admin\Company;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'name' => 'required|max:191',
            'phone'      => "required|numeric|digits_between:9,13|unique:companies,phone,{$this->id},id,deleted_at,NULL",
            'email'      => "required|email|max:191|unique:companies,email,{$this->id},id,deleted_at,NULL",
            'password'   => 'nullable|max:191|confirmed',
            'image'      => 'nullable|mimes:jpeg,jpg,png',
            'country_id'    => 'required|exists:countries,id,deleted_at,NULL',
            'city_id'    => 'required|exists:cities,id,deleted_at,NULL',
            'lat'    => 'required',
            'lng'    => 'required',
            'address'    => 'required',
        ];
    }
}
