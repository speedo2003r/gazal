<?php

namespace App\Http\Requests\Admin\Slider;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Update extends FormRequest
{

    public function __construct(Request $request)
    {
        $request['user_id'] = $request['provider_id'];
    }
    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'title.ar' => 'required|max:191',
            'title.en'  => 'required|max:191',
            'type'  => 'required|in:seller,item',
            'image'  => 'nullable|file|image|mimes:jpeg,jpg,png',
            'city_id'  => 'required|exists:cities,id',
            'category_id'  => 'required|exists:categories,id',
            'user_id'  => 'required|exists:users,id',
            'item_id'  => 'nullable|exists:items,id',
        ];
    }
    public function messages()
    {
        return [
            'type' => 'النوع',
            'title.ar' => 'الاسم بالعربي',
            'title.en' => 'الاسم بالانجليزي',
        ];
    }
}
