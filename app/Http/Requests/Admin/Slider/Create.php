<?php

namespace App\Http\Requests\Admin\Slider;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function __construct(Request $request)
    {
        $request['user_id'] = $request['provider_id'];
    }
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title.ar' => 'required|max:191',
            'title.en'  => 'required|max:191',
            'type'  => 'required|in:seller,item',
            'image'  => 'required|file|image|mimes:jpeg,jpg,png',
            'country_id'  => 'required|exists:countries,id',
            'city_id'  => 'required|exists:cities,id',
            'category_id'  => 'required|exists:categories,id',
            'provider_id'  => 'required|exists:users,id',
            'item_id'  => 'nullable|exists:items,id',
        ];
    }

    public function messages()
    {
        return [
            'type' => 'النوع',
            'title.ar' => 'الاسم بالعربي',
            'title.en' => 'الاسم بالانجليزي',
            'user_id' => 'المتجر',
            'item_id' => 'المنتج',
        ];
    }
}
