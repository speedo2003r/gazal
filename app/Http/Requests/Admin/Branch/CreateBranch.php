<?php

namespace App\Http\Requests\Admin\Branch;

use Illuminate\Foundation\Http\FormRequest;

class CreateBranch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider_id' => 'required|exists:users,id',
            'lat'  => 'required',
            'lng'  => 'required',
            'phone'  => 'required|numeric|digits_between:9,13|unique:branches,phone,NULL,id,deleted_at,NULL',
            'email'  => 'required|email|max:191|unique:branches,email,NULL,id,deleted_at,NULL',
            'password'  => 'required|confirmed|min:3,max:191',
            'name'  => 'required|max:191',
            'title.ar'  => 'required|max:191',
            'title.en'  => 'required|max:191',
            'country_id'  => 'required|exists:countries,id,deleted_at,NULL',
            'city_id'  => 'required|exists:cities,id,deleted_at,NULL',
            'address'  => 'required|max:191',
        ];
    }
}
