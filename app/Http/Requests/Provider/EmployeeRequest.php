<?php

namespace App\Http\Requests\Provider;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:191',
            'phone' => 'required|numeric|digits:10|unique:providers,phone,'.$this->route('employee'),
            'email' => 'required|email|unique:providers,email,'.$this->route('employee'),
            'password' => 'required|string|max:191|min:6|confirmed',
            'avatar' => 'required|image',
            'address' => 'required|string|max:191',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'branches' => 'required|array'
        ];
        if ($this->method() == 'PUT') {
            $rules['password'] = 'nullable|string|max:191|min:6|confirmed';
            $rules['avatar'] = 'nullable|image';
        }

        return $rules;
    }
}
