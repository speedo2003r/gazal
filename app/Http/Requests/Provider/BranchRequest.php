<?php

namespace App\Http\Requests\Provider;

use Illuminate\Foundation\Http\FormRequest;

class BranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = $this->onCreate();
        if ($this->method() == 'PUT') {
            $rules = $this->onUpdate();
        }
        return $rules;
    }

    public function onCreate()
    {
        return [
            //'name' => 'required|string|min:2|max:191',
            'title' => 'required|array',
            'title.*' => 'required|string|min:2|max:191',
            'phone' => 'required|digits:10|numeric',
            'email' => 'required|email|unique:branches,email',
            'password' => 'required|confirmed|min:6|max:191',
            'country_id' => 'required|exists:countries,id,deleted_at,NULL',
            'city_id' => 'required|exists:cities,id,deleted_at,NULL',
            'address' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'image' => 'required|image|mimes:jpg,jpeg,png',
        ];
    }

    public function onUpdate()
    {
        return [
            'title' => 'required|array',
            'title.*' => 'required|string|min:2|max:191',
            'phone' => 'required|digits:10|numeric',
            'email' => 'required|email|unique:branches,email,'.$this->route('branch'),
            'password' => 'nullable|confirmed|min:6|max:191',
            'country_id' => 'required|exists:countries,id,deleted_at,NULL',
            'city_id' => 'required|exists:cities,id,deleted_at,NULL',
            'address' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'image' => 'nullable|image|mimes:jpg,jpeg,png',
        ];
    }

}
