<?php

namespace App\Http\Livewire;

use App\Entities\Item;
use App\Repositories\FavouriteRepositoryEloquent as favRepo;
use Livewire\Component;

class BranchCategoryItems extends Component {
  public $providerCategories = [];
  public $items              = [];
  public $category_id        = 0;
  public $user;
  public $branch;
  public $provider;
  public $search;

  public function updateItems($category_id) {
    $this->category_id = $category_id;
  }

  public function getItems() {
    //todo: paginate results - move to repo

    if (0 == $this->category_id) {
      $this->items = Item::where('user_id', $this->user->id)
        ->whereHas('branches', function ($br) {
          $br->where('branches.id', $this->branch->id);
        })
        ->whereHas('types')
        ->whereHas('selling_views')
        ->when($this->search, function ($q) {
          return $q->where(function ($item) {
            return $item->where('title->ar', 'like', "%$this->search%")
              ->orWhere('title->en', 'like', "%$this->search%")
              ->orWhere('description->ar', 'like', "%$this->search%")
              ->orWhere('description->en', 'like', "%$this->search%");
          });
        })
        ->get();

    } else {
      $this->items = $this->user->items()
        ->where('items.subcategory_id', $this->category_id)
        ->whereHas('branches', function ($br) {
          $br->where('branches.id', $this->branch->id);
        })
        ->whereHas('types')
        ->when($this->search, function ($q) {
          return $q->where(function ($item) {
            return $item->where('title->ar', 'like', "%$this->search%")
              ->orWhere('title->en', 'like', "%$this->search%")
              ->orWhere('description->ar', 'like', "%$this->search%")
              ->orWhere('description->en', 'like', "%$this->search%");
          });
        })
        ->get();
    }
    return;
  }

  public function toggleFav($item_id) {
    (new favRepo(app()))->toggleItemFav($item_id);
    return;
  }

  public function render() {
    $this->getItems();
    return view('livewire.branch-category-items');
  }
}
