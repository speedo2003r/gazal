<?php

namespace App\Http\Livewire;

use App\Entities\OrderProduct;
use App\Repositories\OrderRepositoryEloquent;
use Livewire\Component;

class CartDetails extends Component {
  public $cart          = [];
  public $count         = 0;
  public $cartPrice     = 0;
  public $orderProducts = [];
  public $showCart      = false;

  protected $listeners = ['updateCartData' => 'cartData'];

  public function mount() {
    $this->cartData();
  }

  public function cartData() {
    $cartData = (new OrderRepositoryEloquent(app()))->getCart();
    if ($cartData) {
      $this->cart          = $cartData;
      $this->count         = OrderProduct::where('order_id', $this->cart->id)->sum('qty');
      $this->cartPrice     = $cartData->price();
      $this->orderProducts = $this->cart->orderProducts;
      $this->showCart      = true;
    } else {
      $this->reset();
    }
  }

  public function increaseItemQuantity($orderProductId) {
    (new OrderRepositoryEloquent(app()))->increaseCartProduct($orderProductId);
    $this->cartData();
  }

  public function decreaseItemQuantity($orderProductId) {
    (new OrderRepositoryEloquent(app()))->decreaseCartProduct($orderProductId);
    $this->cartData();
  }

  public function render() {
    return view('livewire.cart-details');
  }
}
