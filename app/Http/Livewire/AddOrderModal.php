<?php

namespace App\Http\Livewire;

use App\Entities\Item;
use App\Repositories\OrderRepositoryEloquent;
use Livewire\Component;

class AddOrderModal extends Component {
  public $choosenItem;
  public $branch_id;
  public $notes;
  public $itemTypes    = [];
  public $itemFeatures = [];

  protected $listeners = ['add-item' => 'openModal'];

  public function openModal($item_id, $branch_id) {
    $item = Item::find($item_id);
    $item->load([
      'types',
      'types.type', // title
      'types.children', // id, title, price
      'features', // id, title, price
    ]);
    $this->choosenItem = $item;
    $this->branch_id   = $branch_id;
    $this->reset(['itemTypes', 'itemFeatures', 'notes']);
    $this->dispatchBrowserEvent('openAddOrderModal');
  }

  public function addToCart() {
    $request['seller_id']   = $this->choosenItem->user_id;
    $request['branch_id']   = $this->branch_id;
    $request['item_id']     = $this->choosenItem->id;
    $request['count']       = 1;
    $request['options']     = $this->itemTypes;
    $request['notes']       = $this->notes;
    $request['features']    = $this->itemFeatures;
    $request['device_type'] = 'web';

    $res = (new OrderRepositoryEloquent(app()))->addToCart($request);
    if ('success' == $res['key']) {
      $this->dispatchBrowserEvent('closeAddOrderModal');
      $this->emit('updateCartData');
    }

    $this->dispatchBrowserEvent('showToastr', ['key' => $res['key'], 'msg' => $res['msg']]);
  }

  public function render() {
    return view('livewire.add-order-modal');
  }
}
