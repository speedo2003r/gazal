<?php

namespace App\Http\Livewire;

use App\Repositories\FavouriteRepositoryEloquent as favRepo;
use Livewire\Component;

class BranchToggleFav extends Component {
  public $branch;

  public function toggleFav($branch_id) {
    (new favRepo(app()))->toggleBranchFav($branch_id);
    $this->branch->refresh();
    return;
  }

  public function render() {
    return view('livewire.branch-toggle-fav');
  }
}
