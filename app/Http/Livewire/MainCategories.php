<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MainCategories extends Component {
  public $mainCategories;
  public function render() {
    return view('livewire.main-categories');
  }
}
