<?php

namespace App\Http\Livewire;

use App\Repositories\FavouriteRepositoryEloquent as favRepo;
use Livewire\Component;

class MealCard extends Component {
  public $item;
  public $branch;
  public $showPlus;
  public $hide = false;

  public function toggleFav($item_id) {
    (new favRepo(app()))->toggleItemFav($item_id);
    $this->hide = true;
    return;
  }

  public function render() {
    return view('livewire.meal-card');
  }
}
