<?php

namespace App\Http\Livewire;

use App\Entities\Group;
use App\Repositories\BranchRepositoryEloquent as branchRepo;
use Livewire\Component;

class CategoryBranches extends Component {
  public $categoryBranches;
  public $category_id;
  public $requestLatLngCity = [];
  public $category_title;

  protected $listeners = [
    'updateCategoryBranches' => 'updateCategoryBranches',
  ];

  public function updateCategoryBranches($category, $requestLatLngCity) {
    $this->category_id       = $category['id'];
    $this->category_title    = $category['title'][app()->getlocale()];
    $this->requestLatLngCity = $requestLatLngCity;
  }

  public function categoryBranches() {
    // TODO: lat lng filter zone in repo
    // TODO: filter from $categoryBranches no need to send request to server
    $group = Group::where(['category_id' => $this->category_id])->first();

    if ($group) {
      $categoryBranches = (new branchRepo(app()))->getGroupProviders($this->requestLatLngCity, $group->id);
    } else {
      $categoryBranches = [];
    }

    $this->categoryBranches = $categoryBranches;
  }

  public function render() {
    $this->categoryBranches();
    return view('livewire.category-branches');
  }
}
