<?php

namespace App\Http\Livewire;

use App\Entities\Group;
use App\Repositories\BranchRepositoryEloquent as branchRepo;
use Livewire\Component;

class CategoryBranchesFilter extends Component {
  public $mainCategory;
  public $categoryBranches;
  public $providersCategories;
  public $requestLatLngCity = [];

  public function updateBranches($sub_category_id) {
    // TODO: lat lng filter zone in repo
    // TODO: filter from $categoryBranches no need to send request to server

    $group = Group::where(['category_id' => $this->mainCategory->id])->first();

    if ($group) {
      $categoryBranches = (new branchRepo(app()))
        ->getGroupProviders($this->requestLatLngCity, $group->id);

      // todo: filter where has $sub_category_id
      $categoryBranches->load(['user', 'user.categories']);
    //   dd($categoryBranches);
    } else {
      $categoryBranches = [];
    }

    $this->categoryBranches = $categoryBranches;
  }

  public function render() {
    return view('livewire.category-branches-filter');
  }
}
