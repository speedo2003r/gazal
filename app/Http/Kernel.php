<?php

namespace App\Http;

use App\Http\Middleware\Branch\CheckBranch;
use App\Http\Middleware\Delegate\CheckDelegate;
use App\Http\Middleware\Provider\Branches;
use App\Http\Middleware\Provider\SellerAuth;
use App\Http\Middleware\Provider\SellerLang;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
  protected $middleware = [
    // \App\Http\Middleware\TrustHosts::class,
    \App\Http\Middleware\TrustProxies::class,
    \Fruitcake\Cors\HandleCors::class,
    \App\Http\Middleware\PreventRequestsDuringMaintenance::class,
    \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
    \App\Http\Middleware\TrimStrings::class,
    \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
  ];

  protected $middlewareGroups = [
    'web' => [
      \App\Http\Middleware\EncryptCookies::class,
      \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
      \Illuminate\Session\Middleware\StartSession::class,
      // \Illuminate\Session\Middleware\AuthenticateSession::class,
      \Illuminate\View\Middleware\ShareErrorsFromSession::class,
      \App\Http\Middleware\VerifyCsrfToken::class,
      \Illuminate\Routing\Middleware\SubstituteBindings::class,
    ],

    'api' => [
      'throttle:api',
      \Illuminate\Routing\Middleware\SubstituteBindings::class,
    ],
  ];

  protected $routeMiddleware = [
    'auth'             => \App\Http\Middleware\Authenticate::class,
    'auth.basic'       => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
    'cache.headers'    => \Illuminate\Http\Middleware\SetCacheHeaders::class,
    'can'              => \Illuminate\Auth\Middleware\Authorize::class,
    'guest'            => \App\Http\Middleware\RedirectIfAuthenticated::class,
    'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
    'signed'           => \Illuminate\Routing\Middleware\ValidateSignature::class,
    'throttle'         => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    'verified'         => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
    'admin'            => \App\Http\Middleware\Admin\AdminMiddleware::class,
    'check-role'       => \App\Http\Middleware\Admin\CheckRoleMiddleware::class,
    'admin-lang'       => \App\Http\Middleware\Admin\AdminLang::class,
    'web-lang'         => \App\Http\Middleware\WebLang::class,
    'api-lang'         => \App\Http\Middleware\Api\ApiLang::class,
    'cors'             => \App\Http\Middleware\Api\cors::class,
    'jwt.verify'       => \App\Http\Middleware\Api\JwtMiddleware::class,
    'phone-activated'  => \App\Http\Middleware\Api\PhoneActivated::class,
    'auth-check'       => \App\Http\Middleware\Api\AuthCheck::class,
    'jwt.refresh'      => \App\Http\Middleware\Api\JwtRefreshMiddleware::class,
    'SellerAuth'       => SellerAuth::class,
    'SellerLang'       => SellerLang::class,
    'CheckDelegate'    => CheckDelegate::class,
    'CheckBranch'      => CheckBranch::class,
    'check-user-type'  => \App\Http\Middleware\CheckUserType::class,
    'fix-phone'        => \App\Http\Middleware\FixPhone::class,
    'my-branches'        => Branches::class,
  ];
}
