<?php

namespace App\Http\Resources\Branches;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductBranchResource extends JsonResource
{
    public function toArray($request)
    {
//        $list = count($this->options) > 0 ? $this->options->map(function ($option){
//            $var = $option->ItemTypeDetail['title'] .' '.($option->ItemTypeDetail->itemType ? $option->ItemTypeDetail->itemType->type['title'] : '');
//            return $var;
//        }) : [];
        $var = '';
        if(count($this->options) > 0){
            foreach ($this->options as $option){
                $var .= ' '.$option->ItemTypeDetail['title'] .' '.($option->ItemTypeDetail->itemType ? $option->ItemTypeDetail->itemType->type['title'] : '');
            }
        };
        return [
            'id'                => $this->id,
            'title'             => $this->item->title,
            'options'           => $var,
            'singlePrice'       => (string) round($this->_single_price(),2),
            'price'             => (string) round($this->_price(),2),
            'currency'          => trans('api.SAR'),
            'count'             => $this->qty,
        ];
    }
}
