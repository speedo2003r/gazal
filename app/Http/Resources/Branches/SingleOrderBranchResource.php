<?php

namespace App\Http\Resources\Branches;

use App\Entities\Order;
use App\Http\Resources\Branches\OrderProductBranchResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleOrderBranchResource extends JsonResource {

  public function toArray($request) {

    return [
      'id'               => $this->id,
      'order_num'        => $this->order_num,
      'store_name'       => $this->provider ? $this->provider['store_name'] : '',
      'branch_name'      => $this->branch_id ? $this->branch['title'] : '',
      'delivered_at'     => \Carbon\Carbon::parse($this->date . ' ' . $this->time)->diffforhumans(),
      'notes'            => $this->notes ?? '',
      'orderType'        => $this->order_type,
      'orderTypeName'    => Order::orderType($this->order_type),
      'items'            => OrderProductBranchResource::collection($this->orderProducts),
      'order_total'      => $this['final_total'] + $this['shipping_price'],
      'coupon_amount'    => $this['coupon_amount'] ?? 0,
      'vat'              => $this['vat_amount'] ?? 0,
      'total'            => $this->_price(),
      'status'           => $this->status,
      'order_status'     => $this->order_status,
      'order_status_msg' => $this->order_status_msg,
    ];
  }
}
