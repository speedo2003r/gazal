<?php

namespace App\Http\Resources\Branches;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchResource extends JsonResource
{

    public function toArray($request)
    {

        return [
            'id'                        => $this->id,
            'name'                      => $this->name,
            'title'                      => $this->title ?? '',
            'status'                      => $this->status,
            'email'                      => $this->email ?? '',
            'phone'                     => $this->phone,
            'wallet'                     => $this->wallet ?? 0,
//            'birth_date'                     => '',
            'lang'                      => $this->lang,
            'avatar'                    => $this->avatar,
        ];
    }
}
