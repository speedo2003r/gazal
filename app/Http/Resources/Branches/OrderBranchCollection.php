<?php

namespace App\Http\Resources\Branches;

use App\Http\Resources\Branches\OrderBranchResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderBranchCollection extends ResourceCollection {
  public function toArray($request) {
    return [
      'data'       => OrderBranchResource::collection($this->collection),
      'pagination' => [
        'total'        => $this->total(),
        'count'        => $this->count(),
        'per_page'     => $this->perPage(),
        'current_page' => $this->currentPage(),
        'total_pages'  => $this->lastPage(),
        //'next_page_url' => $this->nextPageUrl(),
      ],
    ];
  }
}
