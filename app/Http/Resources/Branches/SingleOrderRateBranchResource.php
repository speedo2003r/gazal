<?php

namespace App\Http\Resources\Branches;

use App\Entities\Branch;
use App\Entities\Order;
use App\Entities\ReviewRate;
use App\Http\Resources\Branches\OrderProductBranchResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleOrderRateBranchResource extends JsonResource {

  public function toArray($request) {

    $user = auth('branchApi')->user();
    $data = ReviewRate::where('order_id', $this['id'])->where('rateable_type', Branch::class)->where('rateable_id', $user['id'])->get();
    $rate = '0';
    if (count($data) > 0) {
      $rate = (string) round($data->sum('rate') / $data->count(), 2);
    }
    return [
      'id'            => $this->id,
      'order_num'     => $this->order_num,
      'store_name'    => $this->provider ? $this->provider['store_name'] : '',
      'branch_name'   => $this->branch_id ? $this->branch['title'] : '',
      'delivered_at'  => \Carbon\Carbon::parse($this->date . ' ' . $this->time)->diffforhumans(),
      'notes'         => $this->notes ?? '',
      'orderType'     => $this->order_type,
      'orderTypeName' => Order::orderType($this->order_type),
      'items'         => OrderProductBranchResource::collection($this->orderProducts),
      'rate'          => $rate,
      'ratingLists'   => $this->ratingLists(),
    ];
  }

  protected function ratingLists() {
    $ratings = $this->ratings->map(function ($query) {
      return [
        'title' => $query->ratingList['title'],
        'rate'  => $query['rate'],
      ];
    });
    return $ratings;
  }
}
