<?php

namespace App\Http\Resources\Branches;

use App\Entities\Branch;
use App\Entities\Order;
use App\Entities\ReviewRate;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderBranchResource extends JsonResource {

  public function toArray($request) {
    $user = auth('branchApi')->user();
    $data = ReviewRate::where('order_id', $this['id'])->where('rateable_type', Branch::class)->where('rateable_id', $user['id'])->get();
    $rate = '0';
    if (count($data) > 0) {
      $rate = (string) round($data->sum('rate') / $data->count(), 2);
    }
    return [
      'id'            => $this->id,
      'order_num'     => $this->order_num,
      'branch_name'   => $this->branch_id ? $this->branch['title'] : '',
      'store_name'    => $this->provider ? $this->provider['store_name'] : '',
      'created_at'    => \Carbon\Carbon::parse($this->created_date)->diffforhumans(),
      'delivered_at'  => \Carbon\Carbon::parse($this->date . ' ' . $this->time)->diffforhumans(),
      'date'          => $this->date,
      'time'          => arabicDate(date('h:i a', strtotime($this->time))),
      'orderType'     => $this->order_type,
      'orderTypeName' => Order::orderType($this->order_type),
      'rate'          => $rate,
    ];
  }
}
