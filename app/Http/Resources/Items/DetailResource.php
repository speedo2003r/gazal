<?php

namespace App\Http\Resources\Items;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
class DetailResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id ?? '',
            'title'              => $this->option->title ?? '',
            'price'              => ($this->price ?? 0) + ($this->type ? $this->type->item['price'] : 0),
            'currency'           => 'SAR',
            'value'              => $this->value ?? '',
            'size'              => $this->size->title ?? '',
        ];
    }
}