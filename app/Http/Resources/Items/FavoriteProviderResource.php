<?php

namespace App\Http\Resources\Items;

use App\Entities\Branch;
use App\Entities\Favourite;
use App\Entities\Group;
use App\Entities\Item;
use App\Http\Resources\Settings\SubCategoryResource;
use App\Http\Resources\Users\AddressResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
class FavoriteProviderResource extends JsonResource
{
    public function toArray($request)
    {
        $checkfav = false;
        $fav = null;
        if(auth()->check()){
            $fav = Favourite::where('user_id',auth()->id())->where('favoritable_id',$this->branch->id)->where('favoritable_type',Branch::class)->first();
            $checkfav = $fav != null ? true : false;
        }else{
            $fav = Favourite::where('user_id',null)->where('uuid',$request['uuid'])->where('favoritable_id',$this->branch->id)->where('favoritable_type',Branch::class)->first();
            $checkfav = $fav != null ? true : false;
        }
        return [
            'id'              => $this->id,
            'provider_id'            => $this->branch ? ($this->branch->user['id']) : '',
            'store_name'      => $this->branch ? ($this->branch->user->provider['store_name'] ?? '') : '',
            'branch_id'      => $this->branch['id'] ?? '',
            'banner'            => $this->branch ? ($this->branch->user->provider['banner']) : '',
            'category'        => $this->branch ? (SubCategoryResource::collection($this->branch->user->categories)) : [],
            'processing_time'          =>  $this->branch ? ($this->branch->user->provider['processing_time'] ?? 0) : 0,
            'favorite'        => $checkfav,
        ];
    }
}
