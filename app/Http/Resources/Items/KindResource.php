<?php

namespace App\Http\Resources\Items;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
class KindResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'title'           => $this->type['title'] ?? '',
            'options'         => OptionResource::collection($this->children),
        ];
    }
}
