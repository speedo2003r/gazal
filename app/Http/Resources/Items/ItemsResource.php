<?php

namespace App\Http\Resources\Items;

use App\Entities\Favourite;
use App\Entities\Item;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemsResource extends JsonResource
{
    public function toArray($request)
    {
        if(auth()->check()){
            $fav = Favourite::where('user_id',auth()->id())->where('favoritable_id',$this['id'])->where('favoritable_type',Item::class)->first();
            $checkfav = $fav != null ? true : false;
        }else{
            $fav = Favourite::where('uuid',$request['uuid'])->where('user_id',null)->where('favoritable_id',$this['id'])->where('favoritable_type',Item::class)->first();
            $checkfav = $fav != null ? true : false;
        }
        return [
            'id'              => $this->id,
            'title'           => $this->title ?? '',
            'desc'            => $this->description ?? '',
            'price'           => (string) $this->price(),
            'currency'        => 'SAR',
            'image'           => $this->main_image,
            'fav'           => $checkfav,
        ];
    }
}
