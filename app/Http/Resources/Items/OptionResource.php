<?php

namespace App\Http\Resources\Items;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
class OptionResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id ?? '',
            'title'           => $this->title ?? '',
            'price'           => $this->price ? $this->price() : 0,
            'currency'        => trans('api.SAR'),
        ];
    }
}
