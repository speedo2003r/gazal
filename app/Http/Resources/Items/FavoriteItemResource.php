<?php

namespace App\Http\Resources\Items;

use App\Entities\Favourite;
use App\Entities\Group;
use App\Entities\Item;
use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
class FavoriteItemResource extends JsonResource
{
    public function toArray($request)
    {
        $checkfav = false;
        $fav = null;
        if(auth()->check()){
            $fav = Favourite::where('user_id',auth()->id())->where('favoritable_id',$this->item->id)->where('favoritable_type',Group::class)->first();
            $checkfav = $fav != null ? true : false;
        }else{
            $fav = Favourite::where('user_id',null)->where('uuid',$request['uuid'])->where('favoritable_id',$this->item->id)->where('favoritable_type',Group::class)->first();
            $checkfav = $fav != null ? true : false;
        }
        return [
            'id'              => $this->id,
            'image'           => $this->item->main_image ,
            'item_id'           => $this->item->id ,
            'title'           => $this->item->title ,
            'desc'           => $this->item->description ,
            'main_price'      => $this->item->price(),
            'discount'        => ($this->item->discount() ?? 0),
            'currency'        => app()->getLocale() == 'ar'? 'ر.س' : 'SAR',
            'favorite'        => $checkfav,

        ];
    }
}
