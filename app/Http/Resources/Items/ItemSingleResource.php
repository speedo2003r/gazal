<?php

namespace App\Http\Resources\Items;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemSingleResource extends JsonResource {
  public function toArray($request) {
    return [
      'id'        => $this->id,
      'title'     => $this->title ?? '',
      'desc'      => $this->description ?? '',
      'seller_id' => (string) $this['user_id'],
      'branch_id' => (string) $this['branch_id'],
      'price'     => (string) $this->price(),
      'currency'  => trans('api.SAR'),
      'image'     => $this->main_image,
      'types'     => KindResource::collection($this->types),
      'features'  => FeatureResource::collection($this->features),
    ];
  }
}
