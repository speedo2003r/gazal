<?php

namespace App\Http\Resources\Delegates;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Delegates\delegatorHomePageResource;


class DelegateOrdersCollection extends ResourceCollection {
  public function toArray($request) {
    return [
      // 'data'       => DelegateOrdersResource::collection($this->collection),
      'data'       => delegatorHomePageResource::collection($this->collection),
      'pagination' => [
        'total'         => $this->total(),
        'count'         => $this->count(),
        'per_page'      => $this->perPage(),
        'current_page'  => $this->currentPage(),
        'total_pages'   => $this->lastPage(),
        'next_page_url' => $this->nextPageUrl() ?? '',
      ],
    ];
  }
}
