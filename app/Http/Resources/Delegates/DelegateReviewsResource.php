<?php

namespace App\Http\Resources\Delegates;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
class DelegateReviewsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id ?? '',
            'order_num'              => $this->order_num ?? '',
            'created_at'              => $this->ratings()->first() ? $this->ratings()->first()['created_at']->format('d-m-Y h:i a') : '',
            'client_name'              => $this->user['name'],
            'provider_name'              => $this->branch['name'],
            'rate'              => $this->ratings()->first() ? round($this->ratings()->sum('rate') / $this->ratings()->count() , 1) : 0.0,
        ];
    }
}
