<?php

namespace App\Http\Resources\Delegates;

use Illuminate\Http\Resources\Json\JsonResource;

class delegatorHomePageResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'id'             => $this->id,
      'order_num'      => $this->order_num,
      'branch_name'    => $this->branch->name,
      'shipping_price' => $this->shipping_price,
      'pay_type'       => $this->pay_type ? $this->paymentsMethod($this->pay_type) : '',
      'time'           => 15
    ];
  }
}
