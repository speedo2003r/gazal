<?php

namespace App\Http\Resources\Delegates;

use Illuminate\Http\Resources\Json\JsonResource;

class DelegateOrdersResource extends JsonResource
{
  public function toArray($request)
  {
    return [
      'id'             => $this->id,
      'order_num'      => $this->order_num,
      'created_at'     => date('d-m-Y h:i a', strtotime($this->created_date)),
      'branch_name'    => $this->branch ? $this->branch['title'] : '',
      'user_id'        => $this['user_id'] ?? 0,
      'shipping_price' => $this->shipping_price,
      'pay_type'       => $this->pay_type ? $this->paymentsMethod($this->pay_type) : '',
      'rate'           => $this->ratings()->first() ? round($this->ratings()->sum('rate') / $this->ratings()->count(), 1) : 0.0,
      'is_rate'        => $this->ratings()->first() ? true : false,
    ];
  }
}
