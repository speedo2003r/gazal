<?php

namespace App\Http\Resources\Delegates;

use App\Entities\Order;
use App\Http\Resources\Orders\OrderProductResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Branches\SingleOrderBranchResource;
use App\Http\Resources\Users\AddressResource;


class DelegateOrderDetailResource extends JsonResource
{
  public function toArray($request)
  {
    $user = auth()->user();
   // dd($this->get()->toArray());

    return [
      'id'               => $this->id,
      'created_at'       => \Carbon\Carbon::parse($this->created_date)->diffforhumans(),
      'order_num'        => $this->order_num,
      'branch_name'      => $this->branch ? $this->branch['title'] : '',
      'branch_logo'      => $this->branch ? $this->branch['avatar'] : '',
      'branch_lat'       => $this->branch ? $this->branch['lng'] : '31.51515151',
      'branch_lng'       => $this->branch ? $this->branch['lat'] : '31.51515151',
      'branch_address'   => $this->branch ? $this->branch['address'] : '',
      'branch_phone'     => $this->branch ? $this->branch['phone'] : '',
      'shipCost'         => $this->shipping_price,
      'items'            => OrderProductResource::collection($this->orderProducts),
      'totalWithOutShip' => (string)$this->_priceWithOutShip(),
      'delivered_date'   => $this->date . ' ' . $this->time,
      'order_type'       => $this['order_type'] ? Order::orderType($this['order_type']) : '',
      'order_status'     => $this['order_status'],
      'order_status_msg' => $this['order_status_msg'],
      'pay_type'         => $this->pay_type ? $this->paymentsMethod($this->pay_type) : '',
      'currency'         => trans('api.SAR'),

      'branch_id'   => $this->branch_id,
      'provider_id' => $this->provider_id,

      'user_id'           => $this->user_id,
      'user_name'         => $this->user->name ?? '',
      'user_email'        => $this->user->email ?? '',
      'user_phone'        => $this->user->phone ?? '',
      'user_country_code' => $this->user->country_code ?? '',
      'user_full_phone'   => $this->user->full_phone ?? '',
      'user_avatar'       => $this->user->avatar ?? '',

      'order_lat'      => $this->lat ?? '',
      'order_lng'      => $this->lng ?? '',
      'order_map_desc' => $this->map_desc ?? '',
    ];
  }
}
