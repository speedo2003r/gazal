<?php

namespace App\Http\Resources\Delegates;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
class DelegatePayResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id ?? '',
            'order_id'              => $this->order_id ?? '',
            'total' => (string) ($this->order->tax + $this->order->shippingPrice + $this->order->totalPrice),
            'commission' =>(string)  round($this->price,2),
        ];
    }
}