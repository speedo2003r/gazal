<?php

namespace App\Http\Resources\Settings;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
class CitiesResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id ?? '',
            'title'              => $this->title ?? '',
        ];
    }
}
