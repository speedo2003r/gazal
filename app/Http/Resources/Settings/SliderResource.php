<?php

namespace App\Http\Resources\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

class SliderResource extends JsonResource {
  public function toArray($request) {
    return [
      'id'          => $this->id,
      'branch_id'   => $this['branch_id'] ?? 0,
      'provider_id' => $this->user_id ? (string) $this->user_id : '',
      'item_id'     => $this->item_id ? (string) $this->item_id : '',
      'type'        => $this->type,
      'image'       => $this->image,
      // store_name
      // banner
      // categories
      // fav
      // processing_time
    ];
  }
}
