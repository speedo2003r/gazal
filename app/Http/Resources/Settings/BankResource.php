<?php

namespace App\Http\Resources\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

class BankResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => (string) $this->name,
            'account_num' => (string) $this->account_num,
            'iban'        => (string) $this->iban,
            'image'       => isset($this->image) ? getImage($this->image):"",
        ];
    }

}
