<?php

namespace App\Http\Resources\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

class AskResource extends JsonResource
{
    
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'ask'       => (string) $this->ask,
            'answer'    => (string) $this->answer,
        ];

    }
}
