<?php

namespace App\Http\Resources\Groups;

use App\Entities\Branch;
use App\Http\Resources\Users\ProviderResource;
use App\Models\Order;
use Illuminate\Http\Resources\Json\JsonResource;

class GroupResource extends JsonResource
{
  public function toArray($request)
  {
    $ship_radius_client = settings('minkm_shippingPrice') ?? 0;
    $group              = $this;
    /*
    $providers          = Branch::selectRaw("*,branches.id as branch_id,
                ( 6371 * acos( cos( radians(" . $request['lat'] . ") ) *
                cos( radians(branches.lat) ) *
                cos( radians(branches.lng) - radians(" . $request['lng'] . ") ) +
                sin( radians(" . $request['lat'] . ") ) *
                sin( radians(branches.lat) ) ) )
                AS distance")
      ->having("distance", "<", $ship_radius_client)
      ->orderBy("distance")
      ->join('providers', function ($q) use ($group) {
        $q->on('providers.id', '=', 'branches.provider_id');
        $q->join('provider_groups', 'provider_groups.user_id', 'providers.id');
        $q->where('provider_groups.group_id', $group['id']);
      })
      ->where(['providers.active' => 1, 'providers.banned' => 0, 'providers.accepted' => 1])
      ->get();
    */
    return [
      'id'    => $this->id,
      'title' => $this->title,
      //'providers'  => ProviderResource::collection($providers),
    ];
  }
}
