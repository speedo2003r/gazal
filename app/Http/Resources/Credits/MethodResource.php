<?php

namespace App\Http\Resources\Credits;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
class MethodResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id ?? '',
            'title'              => $this->title ?? '',
            'type'              => $this->type ?? '',
        ];
    }
}
