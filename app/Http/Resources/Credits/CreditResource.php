<?php

namespace App\Http\Resources\Credits;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
class CreditResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id ?? '',
            'card_type'              => 'visa',
            'holder_name'              => $this->holder_name ?? '',
            'card_number'              => substr($this->card_number,-4) ?? '',
            'expire_card'              => arabicDate(date('Y M',strtotime($this->expire_card))) ?? '',
        ];
    }
}