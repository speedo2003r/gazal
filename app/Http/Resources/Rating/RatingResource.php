<?php

namespace App\Http\Resources\Rating;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
class RatingResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id ?? '',
            'title'              => $this->title ?? '',
        ];
    }
}
