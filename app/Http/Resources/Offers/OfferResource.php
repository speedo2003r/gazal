<?php

namespace App\Http\Resources\Offers;

use Illuminate\Http\Resources\Json\JsonResource;

class OfferResource extends JsonResource {
  public function toArray($request) {
    return [
      'id'          => $this->id,
      'title'       => $this->title,
      'desc'        => $this->desc,
      'image'       => $this->image,
      'discount'    => $this->discount,
      'type'        => $this->type,
      'provider_id' => $this['user_id'] ?? '',
      'branch_id'   => $this['branch_id'] ?? 0,
      'item_id'     => 'item' == $this->type ? $this['item_id'] : '',
      'store_name'  => $this->user->provider['store_name'] ?? '',
      'categories'  => $this->user->categories ? $this->user->categories->map(function ($category) {
        return [
          'id'    => $category['id'],
          'title' => $category['title'],
        ];
      }) : [],
      // categories
      // banner
      // fav
      // processing_time
    ];
  }
}
