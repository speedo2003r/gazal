<?php

namespace App\Http\Resources\Offers;

use App\Http\Resources\Orders\OrderResource;
use App\Http\Resources\Users\ProviderResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OfferCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => OfferResource::collection($this->collection),
            'pagination' => [
                'total'         => $this->total(),
                'count'         => $this->count(),
                'per_page'      => $this->perPage(),
                'current_page'  => $this->currentPage(),
                'total_pages'   => $this->lastPage(),
                //'next_page_url' => $this->nextPageUrl(),
            ],
        ];
    }
}
