<?php

namespace App\Http\Resources\Orders;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailResource extends JsonResource {
  public function toArray($request) {
    $user = auth()->user();
    return [
      'id'           => $this->id,
      'created_at'   => date('d-m-Y h:i a',strtotime($this->created_date)),
      'lat'          => null != $this->lat ? $this->lat : $this->address['lat'],
      'lng'          => null != $this->lng ? $this->lng : $this->address['lat'],
      'shipCost'     => $this->shipping_price,
      'phone'        => $this->phone ?? $this->user['phone'],
      'tax'          => $this->vat_amount,
      'coupon_value' => $this->coupon_amount,
      'totalPrice'   => $this->final_total,
      'total'        => (string) $this->_price(),
      'currency'     => trans('api.SAR'),
    ];
  }
}
