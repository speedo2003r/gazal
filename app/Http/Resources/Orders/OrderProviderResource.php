<?php

namespace App\Http\Resources\Orders;

use App\Entities\Order;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderProviderResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_num' => $this->order_num,
            'store_name' => $this->provider ? $this->provider['store_name'] : '',
            'created_at' => date('d-m-Y h:i a',strtotime($this->created_at)),
            'status' => $this->status,
            'statusName' => Order::userStatus($this->order_status),
            'reorder' => $this->orderProducts()->whereHas('item')->exists() ? false : true ,
        ];
    }
}
