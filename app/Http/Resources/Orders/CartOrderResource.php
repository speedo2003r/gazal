<?php

namespace App\Http\Resources\Orders;

use App\Entities\Order;
use App\Entities\OrderProduct;
use Illuminate\Http\Resources\Json\JsonResource;

class CartOrderResource extends JsonResource
{
    public function toArray($request)
    {
        $count = OrderProduct::where('order_id',$this->id)->sum('qty');
        return [
            'id'                => $this->id,
            'notes'                => $this->notes ?? '',
            'count'                => $count ?? 0,
            'store_name'        => $this->provider ? $this->provider['store_name'] : '',
            'items'             => OrderProductCartResource::collection($this->orderProducts),
            'date'             => $this['date'] ?? '',
            'time'             => $this['time'] ?? '',
            'order_type'             => $this['order_type'] ?? '',
            'orderType'             => Order::orderType($this['order_type']),
            'coupon_amount'             => $this['coupon_amount'],
            'existCoupon'             => $this['coupon_amount'] != null ? true : false,
            'tax'             => $this['vat_amount'],
            'shipPrice'             => $this['shipping_price'],
            'total'             => $this->_price(),
        ];
    }
}
