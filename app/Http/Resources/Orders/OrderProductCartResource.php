<?php

namespace App\Http\Resources\Orders;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductCartResource extends JsonResource
{
    public function toArray($request)
    {
        $var = [];
        if(count($this->options) > 0){
            foreach ($this->options as $option){
                $var[] = [
                    'id' => $option['id'],
                    'title' => $option->ItemTypeDetail['title'],
                ];
            }
        };
        return [
            'id'                => $this->id,
            'title'             => $this->item->title,
            'image'             => $this->item->main_image,
            'options'           => $var,
            'singlePrice'       => (string) round($this->_single_price(),2),
            'price'             => (string) round($this->_price(),2),
            'currency'          => trans('api.SAR'),
            'count'             => $this->qty,
        ];
    }
}
