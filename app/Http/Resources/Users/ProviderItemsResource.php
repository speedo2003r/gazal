<?php

namespace App\Http\Resources\Users;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
class ProviderItemsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'seller_id'       => $this->user['id'],
            'branch'       => $this->user['parent_id'] != null ? $this->user->seller['store_name'] .'('.$this->parent->seller['store_name'].')' :'',
            'is_open'          =>  $this->user->isOpen() == trans('api.open') ? 1 : 0,
            'title'           => $this->title,
            'price'           => $this->main_price() .' SAR',
            'image'           => $this->main_image,
        ];
    }
}