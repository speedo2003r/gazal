<?php

namespace App\Http\Resources\Users;

use App\Http\Resources\Users\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DelegateResource extends JsonResource
{

    public function toArray($request)
    {
        $token = "";
        if($request['device_id']){
            $device   =  $this->resource->Devices ? $this->resource->Devices->where('device_id',$request['device_id'])->first() : null;
            $device ? $token = $device->token : "";
        }

        return [
            'id'                        => $this->id,
            'name'                      => $this->name,
            'email'                      => $this->email ?? '',
            'phone'                     => $this->phone,
            'wallet'                     => $this->wallet ?? 0,
            'birth_date'                     => $this->birth_date ?? '',
            'rate'                      => 0,
            'lang'                      => $this->lang,
            'avatar'                    => $this->avatar,
            'online'                    => $this->online ?? 0,
            'user_type'                    => $this->user_type ?? '',
        ];
    }
}
