<?php

namespace App\Http\Resources\Users;

use App\Entities\Branch;
use App\Entities\Favourite;
use App\Http\Resources\Settings\SubCategoryResource;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Groups\GroupResource;

class ProviderResource extends JsonResource
{
  public function toArray($request)
  {
    if (auth()->check()) {
      $fav      = Favourite::where('user_id', auth()->id())->where('favoritable_id', request()->get('branch_id'))->where('favoritable_type', Branch::class)->first();
      $checkfav = null != $fav ? true : false;
    } else {
      $fav      = Favourite::where('uuid', $request['uuid'])->where('user_id', null)->where('favoritable_id', request()->get('branch_id'))->where('favoritable_type', Branch::class)->first();
      $checkfav = null != $fav ? true : false;
    }
    $existLabel = $this->whereDate('created_at', '>', now()->subDays(30))->exists();
    return [
      'id'              => $this->id,
      'store_name'      => $this['store_name'] ?? '',
      'branch_id'       => $this['id'] ?? 0,
      'provider_id'     => $this['provider_id'] ?? 0,
      'banner'          => $this['banner'] ?? '',
      'categories'      => $this->categories ? SubCategoryResource::collection($this->categories) : [],
      'processing_time' => $this['processing_time'] ?? 0,
      'label'           => $existLabel,
      'fav'             => $checkfav,
      'discount'        => $this->discount ?? 0,
      'discountExist'   => ($this->from <= Carbon::now()->format('Y-m-d') && $this->to >= Carbon::now()->format('Y-m-d')),
      'groups'          => $this->user ? GroupResource::collection($this->user->groups) : []
    ];
  }
}
