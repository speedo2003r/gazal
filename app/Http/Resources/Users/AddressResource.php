<?php

namespace App\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'lat'       => $this->lat,
            'lng'       => $this->lng,
            'map_desc'  => $this->map_desc ?? '',
            'flag'  => $this->flag ?? '',
            'name'      => $this->name,
            'is_main'   => (bool) $this->is_main,
            'unique_sign'    => $this->unique_sign??'',
            'type'     => $this->type,
        ];
    }
}
