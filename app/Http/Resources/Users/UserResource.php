<?php

namespace App\Http\Resources\Users;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource {

  public function toArray($request) {
    $token = "";
    if ($request['device_id']) {
      $device          = $this->resource->Devices ? $this->resource->Devices->where('device_id', $request['device_id'])->first() : null;
      $device ? $token = $device->token : "";
    }

    return [
      'id'           => $this->id,
      'name'         => $this->name,
      'email'        => $this->email ?? '',
      'phone'        => $this->phone,
      'full_phone'   => $this->full_phone,
      'wallet'       => $this->wallet ?? 0,
      'country_code' => $this->country_code ?? 0,
      'birth_date'   => $this->birth_date ?? '',
      'lang'         => $this->lang,
      'avatar'       => $this->avatar,
      'online'       => $this->online ?? 0,
      'user_type'    => get_class($this) == User::class ? 'client' : 'delegate',
    ];
  }
}
