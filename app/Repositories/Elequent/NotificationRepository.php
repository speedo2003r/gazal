<?php

namespace App\Repositories\Elequent;

use App\Entities\Notification;
use App\Repositories\Interfaces\INotification;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class NotificationRepository extends BaseRepository implements INotification {
  public function model() {
    return Notification::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
