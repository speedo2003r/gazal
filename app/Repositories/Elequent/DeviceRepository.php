<?php

namespace App\Repositories\Elequent;

use App\Entities\Device;
use App\Repositories\Interfaces\IDevice;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class DeviceRepository extends BaseRepository implements IDevice {

  public function model() {
    return Device::class;
  }
  public function makeNewDevice($model)
  {
      $device = $this->model->where([
          'uuid'    => request('uuid'),
          'model_id' => $model['id'],
          'model_type' => get_class($model),
      ])->first();
      if ($device) {
          $device->delete();
      }
      $device = $this->model->create(
          ['device_id' => request('device_id'),'device_type' => request('device_type'),'uuid' => request('uuid'),'model_id' => $model->id,'model_type'=>get_class($model)]
      );
      return $device;
  }
  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
