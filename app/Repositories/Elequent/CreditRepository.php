<?php

namespace App\Repositories\Elequent;

use App\Entities\Credit;
use App\Repositories\Interfaces\ICredit;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class CreditRepository extends BaseRepository implements ICredit {

  public function model() {
    return Credit::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
