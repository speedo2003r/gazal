<?php

namespace App\Repositories\Elequent;

use App\Entities\Country;
use App\Repositories\Interfaces\ICountry;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class CountryRepository extends BaseRepository implements ICountry {
  public function model() {
    return Country::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
