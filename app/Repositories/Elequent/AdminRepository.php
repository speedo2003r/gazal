<?php

namespace App\Repositories\Elequent;

use App\Entities\Admin;
use App\Repositories\Interfaces\IAdmin;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class AdminRepository extends BaseRepository implements IAdmin {

  public function model() {
    return Admin::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
