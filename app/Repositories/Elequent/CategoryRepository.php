<?php

namespace App\Repositories\Elequent;

use App\Entities\Category;
use App\Repositories\Interfaces\ICategory;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class CategoryRepository extends BaseRepository implements ICategory {
  public function model() {
    return Category::class;
  }

  public function mainCategories() {
    return $this->model->where('parent_id', null)->take(7)->get();
  }
  public function allMainCategories() {
    return $this->model->where('parent_id', null)->get();
  }

  public function getByBranchesIds($branchesIds)
  {
    return $this->model
        ->query()
        ->select('categories.*')
        ->where('categories.parent_id' , null)
        ->join('providers', function ($q) use ($branchesIds) {
            $q->on('providers.category_id', '=', 'categories.id');
            $q->join('branches', 'branches.provider_id', '=', 'providers.id');
            $q->where(['providers.active' => 1, 'providers.banned' => 0, 'providers.accepted' => 1]);
            $q->where(['branches.appear' => 1, 'branches.status' => 1]);
            $q->whereIn('branches.id' , $branchesIds);
        })
        ->distinct()
        ->get();
  }
  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
