<?php

namespace App\Repositories\Elequent;

use App\Entities\Feature;
use App\Repositories\Interfaces\IFeature;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class FeatureRepository extends BaseRepository implements IFeature {

  public function model() {
    return Feature::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
