<?php

namespace App\Repositories\Elequent;

use App\Entities\OrderProductFeature;
use App\Repositories\Interfaces\IOrderProductFeature;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class OrderProductFeatureRepository extends BaseRepository implements IOrderProductFeature {
  public function model() {
    return OrderProductFeature::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
