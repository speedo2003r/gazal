<?php

namespace App\Repositories\Elequent;

use App\Entities\DelegatePay;
use App\Repositories\Interfaces\IDelegatePay;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class DelegatePayRepository extends BaseRepository implements IDelegatePay {

  public function model() {
    return DelegatePay::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
