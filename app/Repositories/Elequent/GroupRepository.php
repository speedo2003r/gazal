<?php

namespace App\Repositories\Elequent;

use App\Entities\Group;
use App\Repositories\Interfaces\IGroup;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class GroupRepository extends BaseRepository implements IGroup {

  public function model() {
    return Group::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
