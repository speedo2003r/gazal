<?php

namespace App\Repositories\Elequent;

use App\Entities\ItemType;
use App\Repositories\Interfaces\IItemType;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class ItemTypeRepository extends BaseRepository implements IItemType {
  public function model() {
    return ItemType::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
