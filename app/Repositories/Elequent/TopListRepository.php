<?php

namespace App\Repositories\Elequent;

use App\Entities\TopList;
use App\Repositories\Interfaces\ITopList;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class TopListRepository extends BaseRepository implements ITopList {

  public function model() {
    return TopList::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
