<?php

namespace App\Repositories\Elequent;

use App\Entities\Social;
use App\Repositories\Interfaces\ISocial;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class SocialRepository extends BaseRepository implements ISocial {

  public function model() {
    return Social::class;
  }

  public function getSocialsLinks() {
    return $this->model->get(['key', 'value'])->toArray();
  }

  public function updateall($attributes = []) {
    if (null != $attributes && count($attributes) > 0) {
      foreach ($attributes as $key => $attribute) {
        $setting          = $this->model->firstOrCreate(['key' => $key]);
        $setting['value'] = $attribute;
        $setting->save();
      }
    }

  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
