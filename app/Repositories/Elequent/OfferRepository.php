<?php

namespace App\Repositories\Elequent;

use App\Entities\Offer;
use App\Repositories\Interfaces\IOffer;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class OfferRepository extends BaseRepository implements IOffer {
  public function model() {
    return Offer::class;
  }

  public function getByNearestBranchesIds($request,$branchesIds)
    {
        $ship_radius_client = settings('minkm_shippingPrice') ?? 0;
        return $this->model->where('offers.category_id', $request['category_id'])
            ->selectRaw("offers.*,branches.id as branch_id,
            ( 6371 * acos( cos( radians(" . $request['lat'] . ") ) *
            cos( radians(branches.lat) ) *
            cos( radians(branches.lng) - radians(" . $request['lng'] . ") ) +
            sin( radians(" . $request['lat'] . ") ) *
            sin( radians(branches.lat) ) ) )
         AS distance")
            ->having("distance", "<", $ship_radius_client)
            ->orderBy("distance")
            ->join('providers', function ($q) use ($branchesIds) {
                $q->on('providers.id', '=', 'offers.user_id');
                $q->join('branches', 'branches.provider_id', '=', 'providers.id');
                $q->where(['providers.active' => 1, 'providers.banned' => 0, 'providers.accepted' => 1]);
                $q->where(['branches.appear' => 1, 'branches.status' => 1]);
                $q->whereIn('branches.id' , $branchesIds);
            })
            ->get()->unique('id');
    }
  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
