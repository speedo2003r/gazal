<?php

namespace App\Repositories\Elequent;

use App\Entities\Complaint;
use App\Repositories\Interfaces\IComplaint;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class ComplaintRepository extends BaseRepository implements IComplaint {
  public function model() {
    return Complaint::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
