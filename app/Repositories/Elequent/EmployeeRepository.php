<?php

namespace App\Repositories\Elequent;

use App\Entities\Employee;
use App\Repositories\Interfaces\IEmployee;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class EmployeeRepository extends BaseRepository implements IEmployee {

  public function model() {
    return Employee::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
