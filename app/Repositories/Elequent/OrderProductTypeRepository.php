<?php

namespace App\Repositories\Elequent;

use App\Entities\OrderProductType;
use App\Repositories\Interfaces\IOrderProductType;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class OrderProductTypeRepository extends BaseRepository implements IOrderProductType {
  public function model() {
    return OrderProductType::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
