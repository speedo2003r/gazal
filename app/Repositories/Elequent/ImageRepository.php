<?php

namespace App\Repositories\Elequent;

use App\Entities\Image;
use App\Repositories\Interfaces\IImage;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class ImageRepository extends BaseRepository implements IImage {

  public function model() {
    return Image::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
