<?php

namespace App\Repositories\Elequent;

use App\Entities\Admin;
use App\Entities\DiscountReason;
use App\Repositories\Interfaces\IAdmin;
use App\Repositories\Interfaces\IDiscountReason;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class DiscountReasonRepository extends BaseRepository implements IDiscountReason {

  public function model() {
    return DiscountReason::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
