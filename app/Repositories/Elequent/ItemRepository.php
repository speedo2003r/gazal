<?php

namespace App\Repositories\Elequent;

use App\Entities\Item;
use App\Repositories\Interfaces\IItem;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class ItemRepository extends BaseRepository implements IItem {

  public function model() {
    return Item::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
