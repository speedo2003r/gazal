<?php

namespace App\Repositories\Elequent;

use App\Entities\Report;
use App\Repositories\Interfaces\IReport;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class ReportRepository extends BaseRepository implements IReport {
  public function model() {
    return Report::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
