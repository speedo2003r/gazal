<?php

namespace App\Repositories\Elequent;

use App\Entities\Income;
use App\Repositories\Interfaces\IIncome;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class IncomeRepository extends BaseRepository implements IIncome {

  public function model() {
    return Income::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
