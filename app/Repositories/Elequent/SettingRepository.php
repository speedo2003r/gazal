<?php

namespace App\Repositories\Elequent;

use App\Entities\Setting;
use App\Repositories\Interfaces\ISetting;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class SettingRepository extends BaseRepository implements ISetting {
  public function model() {
    return Setting::class;
  }

  public function updateall($attributes = []) {
    if (null != $attributes && count($attributes) > 0) {
      foreach ($attributes as $key => $attribute) {
        $setting          = $this->model->firstOrCreate(['key' => $key]);
        $setting['value'] = $attribute;
        $setting->save();
      }
    }

  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
