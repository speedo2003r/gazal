<?php

namespace App\Repositories\Elequent;

use App\Entities\Company;
use App\Repositories\Interfaces\ICompany;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class CompanyRepository extends BaseRepository implements ICompany {

  public function model() {
    return Company::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
