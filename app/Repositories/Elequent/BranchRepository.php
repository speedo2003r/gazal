<?php

namespace App\Repositories\Elequent;

use App\Entities\Branch;
use App\Entities\Group;
use App\Repositories\Interfaces\IBranch;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class BranchRepository extends BaseRepository implements IBranch
{
  public function model()
  {
    return Branch::class;
  }

  public function find($id, $columns = ['*'])
  {
    $model = $this->model->find($id, $columns);
    return $this->parserResult($model);
  }

  public function getCategoryBranchesInZone($category_id, $zoneBranchesIds)
  {
    $categoryGroupsIds = Group::where(['category_id' => $category_id])->pluck('id');
    return $this->model
      ->where([
        'branches.appear' => 1,
        'branches.status' => 1
      ])
      ->join('providers', function ($q) use ($categoryGroupsIds) {
        $q->on('providers.id', '=', 'branches.provider_id');
        $q->leftJoin('provider_groups', 'provider_groups.user_id', 'providers.id');
        $q->whereIn('provider_groups.group_id', $categoryGroupsIds);
      })
      ->whereIn('branches.id', $zoneBranchesIds)
      ->where([
        'providers.active'      => 1,
        'providers.banned'      => 0,
        'providers.accepted'    => 1,
        'providers.category_id' => $category_id
      ])
      ->get()->unique('id');
  }

  public function getByGroupsBranchesIds($request, $branchesIds)
  {
    $groups             = Group::where(['category_id' => $request['category_id']])->get();
    $group              = $groups->first();
    $ship_radius_client = settings('minkm_shippingPrice') ?? 0;
    return $this->model
      ->where(['branches.appear' => 1, 'branches.status' => 1])
      ->selectRaw("branches.*,
            ( 6371 * acos( cos( radians(" . $request['lat'] . ") ) *
            cos( radians(branches.lat) ) *
            cos( radians(branches.lng) - radians(" . $request['lng'] . ") ) +
            sin( radians(" . $request['lat'] . ") ) *
            sin( radians(branches.lat) ) ) )
         AS distance")
      ->having("distance", "<", $ship_radius_client)
      ->orderBy("distance")
      ->join('providers', function ($q) use ($group) {
        $q->on('providers.id', '=', 'branches.provider_id');
        $q->leftJoin('provider_groups', 'provider_groups.user_id', 'providers.id');
        $q->where('provider_groups.group_id', $group['id']);
      })
      ->whereIn('branches.id', $branchesIds)
      ->where(['providers.active' => 1, 'providers.banned' => 0, 'providers.accepted' => 1, 'providers.category_id' => $request['category_id']])
      ->get();
  }

  public function checkPassword($model, $password)
  {
    $isSamePassword = Hash::check($password, $model['password']);
    return $isSamePassword;
  }

  public function getGroupProviders($request, $group_id)
  {
    //todo: select data needed in view and next route

    $branches = $this->model->query();
    $branches = $branches->whereHas('user', function ($q) use ($group_id) {
      $q->active();
      $q->where('group_id', $group_id);
    });

    //! from model has left join users will return user data first
    //todo: make filter by distance and update it

    $users = $branches->distance($request['lat'] ?? '', $request['lng'] ?? '', $request['city_id'] ?? null);
    $users = $users->get();
    $users = $users->unique('user_id');

    return $users;
  }

  public function getBranchesDates($id, $type)
  {
    $branch = $this->model->find($id);
    $date   = $branch->branchDates()->where('type', $type)->orderBy('day', 'asc')->get();
    return $date;
  }

  public function boot()
  {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
