<?php

namespace App\Repositories\Elequent;

use App\Entities\Delegate;
use App\Repositories\Interfaces\IDelegate;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class DelegateRepository extends BaseRepository implements IDelegate {

  public function model() {
    return Delegate::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
