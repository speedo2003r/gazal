<?php

namespace App\Repositories\Elequent;

use App\Entities\Governorate;
use App\Repositories\Interfaces\IGovernorate;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class GovernorateRepository extends BaseRepository implements IGovernorate {
  public function model() {
    return Governorate::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
