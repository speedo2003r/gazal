<?php

namespace App\Repositories\Elequent;

use App\Entities\Branch;
use App\Entities\Feature;
use App\Entities\Item;
use App\Entities\ItemTypeDetail;
use App\Entities\Order;
use App\Entities\OrderProduct;
use App\Entities\OrderProductFeature;
use App\Entities\OrderProductType;
use App\Entities\UserAddress;
use App\Models\User;
use App\Repositories\Interfaces\IOrder;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class OrderRepository extends BaseRepository implements IOrder {
  public function model() {
    return Order::class;
  }

  public function getCart() {
    $user    = auth()->check() ? auth()->user() : null;
    $user_id = $user ? $user['id'] : null;
    $uuid    = $request['uuid'] ?? (getBrowserUuid() ?? null);

    if ($user) {
      $order = $this->model
        ->where('uuid', $uuid)
        ->where('user_id', $user_id)
        ->where('live', 0)
        ->first();
    } else {
      $order = $this->model
        ->where('uuid', $uuid)
        ->where('user_id', null)
        ->where('live', 0)
        ->first();
    }

    return $order;
  }

  public function addToCart($request) {

    /**
    'uuid'      => ['required'],
    'seller_id' => 'required|exists:users,id,deleted_at,NULL',
    'branch_id' => 'required|exists:branches,id,deleted_at,NULL',
    'item_id'   => 'required|exists:items,id,deleted_at,NULL',
    'count'     => ['required'],
    'options'   => 'required',
    'notes'     => ['nullable'],
    'features'  => 'sometimes',
     */
    // dd($request);

    # get user and provider user data
    $user    = auth()->check() ? auth()->user() : null;
    $user_id = $user ? $user['id'] : null;
    $seller  = User::find($request['seller_id']);
    $uuid    = $request['uuid'] ?? (getBrowserUuid() ?? null);

    # get branch
    $branch = Branch::find($request['branch_id']);
    if ($branch['provider_id'] != $seller['id']) {
      $msg = 'هذا الفرع غير تابع لهذا المستخدم';
      return ['key' => 'fail', 'msg' => $msg];
    }

    $address = null;

    if ($user) {

      # user cart -> open order
      $order = $this->model
        ->where('uuid', $uuid)
        ->where('provider_id', $request['seller_id'])
        ->where('branch_id', $branch['id'])
        ->where('user_id', $user_id)
        ->where('live', 0)
        ->first();

      $address = $user->addresses()->where('is_main', 1)->first();
    } else {

      # guest open order
      $order = $this->model
        ->where('uuid', $uuid)
        ->where('provider_id', $request['seller_id'])
        ->where('branch_id', $branch['id'])
        ->where('user_id', null)
        ->where('live', 0)->first();

      $address = UserAddress::where('user_id', null)->where('is_main', 1)->first();
    }

    if (!$order) {
      $order = $this->model->create([
        'uuid'        => $uuid,
        'status'      => 'new',
        'live'        => 0,
        'total_items' => $request['count'] ?? 1,
        'provider_id' => $seller['id'],
        'branch_id'   => $branch['id'],
        'user_id'     => $user_id,
      ]);

      $this->orderProductStore($order, $request);

    } else {

      if (($order['provider_id'] != $seller['id']) || ($order['branch_id'] != $branch['id'])) {
        return ['key' => 'fail', 'msg' => trans('api.you_cannot_addCArt')];
      } else {
        $this->updateOrder($order, $request);
      }
    }

    // $count  = OrderProduct::where('order_products.order_id', $order['id'])->sum('qty');

    $sumAll = 0;
    foreach ($order->refresh()->orderProducts as $orderProduct) {
      $sumAll += $orderProduct->_price();
    }

    $tax       = settings('tax') ?? 0;
    $orderTax  = $sumAll * $tax / 100;
    $shipPrice = 0;

    if ($address) {
      $distance = distance($address['lat'], $address['lng'], $branch['lat'], $branch['lng']);

      $shipPrice = ($distance > settings('minkm_shippingPrice'))
      ? ($distance * settings('shippingValue')) + settings('shippingPrice')
      : (double) settings('shippingPrice');
    }

    $order->update([
      'final_total'    => $sumAll,
      'vat_amount'     => round($orderTax, 2),
      'vat_per'        => $tax,
      'shipping_price' => $shipPrice,
      'address_id'     => $address ? $address['id'] : null,
      'map_desc'       => $address ? $address['street'] . ',' . $address['building'] . ',' . $address['floor'] . ',' . $address['flat'] . ',' . $address['unique_sign'] : null,
      'lat'            => $address ? $address['lat'] : null,
      'lng'            => $address ? $address['lng'] : null,
      'device_type'    => $request['device_type'],
    ]);

    return [
      'key'   => 'success',
      'msg'   => awtTrans('تم التحديث بنجاح'),
      'order' => $order->refresh(),
    ];
  }

  private function orderProductStore($order, $request): void {
    $item = Item::find($request['item_id']);

    $orderProduct = OrderProduct::create([
      'order_id' => $order['id'],
      'item_id'  => $item['id'],
      'qty'      => $request['count'],
      'notes'    => isset($request['notes']) ? $request['notes'] : null,
    ]);

    if (isset($request['features'])) {
      $features = Feature::whereIn('id', $request['features'])->get();
      foreach ($features as $feature) {
        OrderProductFeature::create([
          'order_product_id' => $orderProduct['id'],
          'feature_id'       => $feature['id'],
          'price'            => $feature['price'],
        ]);
      }
    }

    # add to product option details
    if (isset($request['options'])) {
      $options = ItemTypeDetail::whereIn('id', $request['options'])->get();
      foreach ($options as $option) {
        OrderProductType::create([
          'order_product_id'     => $orderProduct['id'],
          'item_types_detail_id' => $option['id'],
          'price'                => $option->price(),
        ]);
      }
    }

    $orderProduct->update([
      'price' => $orderProduct->_single_price(),
    ]);
  }

  private function updateOrder($order, $request) {
    $item = Item::find($request['item_id']);

    $order->update([
      'total_items' => $order['total_items'] + $request['count'],
    ]);

    # add to order product
    $orderProduct = OrderProduct::where('order_id', $order['id'])
      ->where('item_id', $item['id'])->first();

    if (!$orderProduct) {
      $this->orderProductStore($order, $request);
    } else {

      $optionsCount = isset($request['options']) ? count($request['options']) : 0;
      $featureCount = isset($request['features']) ? count($request['features']) : 0;

      if ($optionsCount > 0 && $featureCount > 0) {

        $count = OrderProduct::where('order_id', $order['id'])
          ->where('item_id', $item['id'])
          ->whereHas('options', function ($option) use ($request) {
            $option->whereIn('item_types_detail_id', $request['options']);
          })
          ->whereHas('features', function ($feature) use ($request) {
            $feature->whereIn('feature_id', $request['features']);
          })
          ->count();

        if ($count > 1) {

          $orderProducts = OrderProduct::where('order_id', $order['id'])
            ->where('item_id', $item['id'])
            ->whereHas('options', function ($option) use ($request) {
              $option->whereIn('item_types_detail_id', $request['options']);
            })
            ->whereHas('features', function ($feature) use ($request) {
              $feature->whereIn('feature_id', $request['features']);
            })
            ->get();

          $itemArrs = [];
          foreach ($orderProducts as $orderProduct) {
            $arr1 = $orderProduct->features()
              ->pluck('order_product_features.feature_id')
              ->toArray();

            $arr2 = $request['features'];

            if (array_equal($arr1, $arr2)) {
              $itemArrs[] = $orderProduct['id'];
              $orderProduct->update([
                'qty' => $orderProduct['qty'] + $request['count'],
              ]);
              break;
            }
          }

          if (count($itemArrs) == 0) {
            $this->orderProductStore($order, $request);
          }

        } else {

          $orderProduct = OrderProduct::where('order_id', $order['id'])
            ->where('item_id', $item['id'])
            ->whereHas('options', function ($option) use ($request) {
              $option->whereIn('item_types_detail_id', $request['options']);
            })
            ->whereHas('features', function ($feature) use ($request) {
              $feature->whereIn('feature_id', $request['features']);
            })
            ->first();

          $arr1 = $orderProduct
          ? (count($orderProduct->features) > 0
            ? $orderProduct->features()->pluck('order_product_features.feature_id')->toArray()
            : [])
          : [];

          $arr2 = $request['features'];

          if ($orderProduct && array_equal($arr1, $arr2) == true) {

            $orderProduct->update([
              'qty' => $orderProduct['qty'] + $request['count'],
            ]);

          } else {
            $this->orderProductStore($order, $request);
          }
        }

      } elseif ($optionsCount > 0 && 0 == $featureCount) {

        $orderProduct = OrderProduct::where('order_id', $order['id'])
          ->where('item_id', $item['id'])
          ->whereHas('options', function ($option) use ($request) {
            $option->whereIn('item_types_detail_id', $request['options']);
          })
          ->whereDoesntHave('features')
          ->first();

        if ($orderProduct) {
          $orderProduct->update([
            'qty' => $orderProduct['qty'] + $request['count'],
          ]);
        } else {
          $this->orderProductStore($order, $request);
        }
      } else {
        $this->orderProductStore($order, $request);
      }
    }
  }

  public function increaseCartProduct($orderProductId): void {
    $orderProduct = OrderProduct::find($orderProductId);
    $order        = $this->model->find($orderProduct->order_id);

    $orderProduct->update([
      'qty' => $orderProduct['qty'] + 1,
    ]);

    $order->update([
      'total_items' => $order['total_items'] + 1,
    ]);
  }

  public function decreaseCartProduct($orderProductId): void {
    $orderProduct = OrderProduct::find($orderProductId);
    $order        = $this->model->find($orderProduct->order_id);

    if (1 == $orderProduct->qty) {
      $orderProduct->delete();
      if (count($order->orderProducts) == 0) {
        $order->delete();
      }

    } else {
      $orderProduct->update([
        'qty' => $orderProduct['qty'] - 1,
      ]);

      $order->update([
        'total_items' => $order['total_items'] - 1,
      ]);
    }
  }

  public function getCountOrdersByUuid($model)
  {
      $exist = $this->model->where(['uuid' => request('uuid'), 'user_id' => $model['id'], 'live' => 0])->get();
      if (count($exist) == 0) {
          $orders = $this->model->where(['uuid' => request('uuid'), 'user_id' => null, 'live' => 0])->get();
      } else {
          $newOrders = $this->model->where(['uuid' => request('uuid'), 'user_id' => null, 'live' => 0])->get();
          if (count($newOrders) == 0) {
              $orders = $this->model->where(['uuid' => request('uuid'), 'user_id' => $model['id'], 'live' => 0])->get();
          } else {
              $oldOrders = $this->model->where(['uuid' => request('uuid'), 'user_id' => $model['id'], 'live' => 0])->get();
              if (count($oldOrders) > 0) {
                  foreach ($oldOrders as $old) {
                      $old->forceDelete();
                  }
              }
              $orders = $newOrders;
          }
      }
      $arrOrders = [];
      $cartCount = 0;
      if (count($orders) > 0) {
          foreach ($orders as $value) {
              $value->user_id = $model['id'];
              $value->save();
              $arrOrders[] = $value['id'];
          }
          $cartCount = OrderProduct::whereIn('order_products.order_id', $arrOrders)->sum('qty');
      }
      return $cartCount;
  }
  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
