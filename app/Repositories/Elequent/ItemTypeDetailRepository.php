<?php

namespace App\Repositories\Elequent;

use App\Entities\ItemTypeDetail;
use App\Repositories\Interfaces\IItemTypeDetail;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class ItemTypeDetailRepository extends BaseRepository implements IItemTypeDetail {

  public function model() {
    return ItemTypeDetail::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
