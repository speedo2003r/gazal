<?php

namespace App\Repositories\Elequent;

use App\Entities\Admin;
use App\Entities\DiscountAmount;
use App\Repositories\Interfaces\IAdmin;
use App\Repositories\Interfaces\IDiscountAmount;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class DiscountAmountRepository extends BaseRepository implements IDiscountAmount {

  public function model() {
    return DiscountAmount::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
