<?php

namespace App\Repositories\Elequent;

use App\Entities\EmployeeJoinUs;
use App\Repositories\Interfaces\IEmployeeJoinUs;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class EmployeeJoinUsRepository extends BaseRepository implements IEmployeeJoinUs {

  public function model() {
    return EmployeeJoinUs::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
