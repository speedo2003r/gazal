<?php

namespace App\Repositories\Elequent;

use App\Entities\Question;
use App\Repositories\Interfaces\IQuestion;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class QuestionRepository extends BaseRepository implements IQuestion {
  public function model() {
    return Question::class;
  }

  public function getQuestions() {
    return $this->model->get();
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
