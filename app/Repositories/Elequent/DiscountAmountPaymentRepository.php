<?php

namespace App\Repositories\Elequent;

use App\Entities\Admin;
use App\Entities\DiscountAmountPayment;
use App\Repositories\Interfaces\IAdmin;
use App\Repositories\Interfaces\IDiscountAmountPayment;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class DiscountAmountPaymentRepository extends BaseRepository implements IDiscountAmountPayment {

  public function model() {
    return DiscountAmountPayment::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
