<?php

namespace App\Repositories\Elequent;

use App\Entities\CarType;
use App\Repositories\Interfaces\ICarType;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class CarTypeRepository extends BaseRepository implements ICarType {

  public function model() {
    return CarType::class;
  }

  public function carTypes() {
    return $this->model->get();
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
