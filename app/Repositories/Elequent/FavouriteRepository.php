<?php

namespace App\Repositories\Elequent;

use App\Entities\Favourite;
use App\Repositories\Interfaces\IFavourite;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class FavouriteRepository extends BaseRepository implements IFavourite {
  public function model() {
    return Favourite::class;
  }

  public function getFavorites($favoritable_type) {
      if(auth()->check()){
          $items = $this->model->where([
              'user_id' => auth()->id(),
              'favoritable_type' => $favoritable_type,
          ])->get();
      }else{
          $items = $this->model->where([
              'uuid' => request('uuid'),
              'user_id' => null,
              'favoritable_type' => $favoritable_type,
          ])->get();
      }
    return $items;
  }

  public function toggleFav($favoritable_type,$favoritable_id) {
    $item = null;
    if (auth()->check()) {
        $old = $this->model->where([
            'favoritable_type' => $favoritable_type,
            'favoritable_id'   => $favoritable_id,
            'user_id'          => auth()->id(),
        ])->first();

        if (!$old) {
            $item = $this->model->create([
                'user_id'          => auth()->id(),
                'favoritable_id'   => $favoritable_id,
                'favoritable_type' => $favoritable_type,
            ]);

        } else {
            $old->delete();
        }
    } elseif (request('uuid')) {
        $old = $this->model->where([
            'favoritable_type' => $favoritable_type,
            'favoritable_id'   => $favoritable_id,
            'uuid'             => request('uuid'),
        ])->first();

        if (!$old) {
            $item = $this->model->create([
                'uuid'             => request('uuid'),
                'favoritable_id'   => $favoritable_id,
                'favoritable_type' => $favoritable_type,
            ]);

        } else {
            $old->delete();
        }
    }
    return $item;
  }
  public function toggleItemFav($item_id) {
    if (auth()->check()) {
      $this->authToggleFav('App\Entities\Item', $item_id);
    } elseif (getBrowserUuid()) {
      $this->uuidToggleFav('App\Entities\Item', $item_id);
    }
    return;
  }

  public function toggleBranchFav($branch_id) {
    if (auth()->check()) {
      $this->authToggleFav('App\Entities\Branch', $branch_id);
    } elseif (getBrowserUuid()) {
      $this->uuidToggleFav('App\Entities\Branch', $branch_id);
    }
    return;
  }

  private function authToggleFav($favoritable_type, $favoritable_id) {
    $old = $this->model->where([
      'favoritable_type' => $favoritable_type,
      'favoritable_id'   => $favoritable_id,
      'user_id'          => auth()->id(),
    ])->first();

    if (!$old) {
      $this->model->create([
        'user_id'          => auth()->id(),
        'favoritable_id'   => $favoritable_id,
        'favoritable_type' => $favoritable_type,
      ]);

    } else {
      $old->delete();
    }
    return;
  }

  private function uuidToggleFav($favoritable_type, $favoritable_id) {
    $old = $this->model->where([
      'favoritable_type' => $favoritable_type,
      'favoritable_id'   => $favoritable_id,
      'uuid'             => getBrowserUuid(),
    ])->first();

    if (!$old) {
      $this->model->create([
        'uuid'             => getBrowserUuid(),
        'favoritable_id'   => $favoritable_id,
        'favoritable_type' => $favoritable_type,
      ]);

    } else {
      $old->delete();
    }
    return;
  }

  public function getFavs() {
    if (auth()->check()) {
      $favs = $this->getAuthFavs();
    } elseif (getBrowserUuid()) {
      $favs = $this->getUuidFavs();
    }
    return $favs;
  }

  private function getAuthFavs() {
    $favs = $this->model->where([
      'user_id' => auth()->id(),
    ])->with('favoritable')->get();

    $items    = $favs->where('favoritable_type', 'App\Entities\Item');
    $branches = $favs->where('favoritable_type', 'App\Entities\Branch');

    $data = [
      'items'    => $items,
      'branches' => $branches,
    ];

    return $data;
  }

  private function getUuidFavs() {
    $favs = $this->model->where([
      'uuid' => getBrowserUuid(),
    ])->with('favoritable')->get();

    $items    = $favs->where('favoritable_type', 'App\Entities\Item');
    $branches = $favs->where('favoritable_type', 'App\Entities\Branch');

    $data = [
      'items'    => $items,
      'branches' => $branches,
    ];

    return $data;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
