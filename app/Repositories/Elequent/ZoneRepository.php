<?php

namespace App\Repositories\Elequent;

use App\Entities\Zone;
use App\Repositories\Interfaces\IZone;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class ZoneRepository extends BaseRepository implements IZone {

    public function model() {
        return Zone::class;
    }

    public function zones() {
        return $this->model->get();
    }

    public function cityOtherZones($zone) {
        return $this->model->where('city_id', $zone->city_id)
            ->where('id', '!=', $zone->id)
            ->pluck('geometry')
            ->toArray();
    }

    public function getZoneBranches() {
        $branches = [];

        $lat = request()->lat;
        $lng = request()->lng;

        if ($lat && $lng) {
            $zones = $this->model
                ->whereRaw("ST_Contains(p_geometry, ST_GeomFromText(?))", ["POINT($lng $lat)"])
                ->with('branches', 'branches.user')
                ->get();

            $branches = $zones->pluck('branches')->flatten()->unique('id');
        }

        return $branches;
    }

    public function checkZonePoint()
    {
        $exist = false;
        $lat = request()->lat;
        $lng = request()->lng;

        if ($lat && $lng) {
            $exist = $this->model
                ->whereRaw("ST_Contains(p_geometry, ST_GeomFromText(?))", ["POINT($lng $lat)"])
                ->whereHas('branches')
                ->exists();
        }
        return $exist;
    }
    public function getZoneName()
    {
        $name = null;
        $lat = request()->lat;
        $lng = request()->lng;

        if ($lat && $lng) {
            $model = $this->model
                ->whereRaw("ST_Contains(p_geometry, ST_GeomFromText(?))", ["POINT($lng $lat)"])
                ->first();
            if($model){
                $name = $model['title'];
            }
        }
        return $name;
    }
    public function getZoneId()
    {
        $id = null;
        $lat = request()->lat;
        $lng = request()->lng;

        if ($lat && $lng) {
            $model = $this->model
                ->whereRaw("ST_Contains(p_geometry, ST_GeomFromText(?))", ["POINT($lng $lat)"])
                ->first();
            if($model){
                $id = $model['id'];
            }
        }
        return $id;
    }
    public function getZone()
    {
        $zone = null;
        $lat = request()->lat;
        $lng = request()->lng;

        if ($lat && $lng) {
            $model = $this->model
                ->whereRaw("ST_Contains(p_geometry, ST_GeomFromText(?))", ["POINT($lng $lat)"])
                ->first();
            if($model){
                $zone = $model;
            }
        }
        return $zone;
    }
    public function checkBranchZonePoint($request, $branch) {
        $check = false;
        $lat   = $request->lat;
        $lng   = $request->lng;

        foreach ($branch->zones as $zone) {
            if (isset($zone->p_geometry[0])) {
                $check = $this->pointInPolygon($lat, $lng, $zone->p_geometry[0]);
                if ($check) {
                    break;
                }
            }
        }

        return $check;
    }

    public function pointInPolygon($lat, $lng, $polygon) {

        $point = new Point($lat, $lng);

        //if you operates with (hundred)thousands of points
        set_time_limit(60);

        $c      = 0;
        $point1 = $polygon[0];
        $n      = count($polygon);

        for ($i = 1; $i <= $n; $i++) {
            $point2 = $polygon[$i % $n];
            if ($point->getLng() > min($point1->getLng(), $point2->getLng())
                && $point->getLng() <= max($point1->getLng(), $point2->getLng())
                && $point->getLat() <= max($point1->getLat(), $point2->getLat())
                && $point1->getLng() != $point2->getLng()) {
                $xinters = ($point->getLng() - $point1->getLng()) * ($point2->getLat() - $point1->getLat()) / ($point2->getLng() - $point1->getLng()) + $point1->getLat();
                if ($point1->getLat() == $point2->getLat() || $point->getLat() <= $xinters) {
                    $c++;
                }
            }
            $point1 = $point2;
        }

        // if the number of edges we passed through is even, then it's not in the poly.
        return $c % 2 != 0;
    }

    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
