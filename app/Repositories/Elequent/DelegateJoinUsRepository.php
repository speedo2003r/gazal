<?php

namespace App\Repositories\Elequent;

use App\Entities\DelegateJoinUs;
use App\Repositories\Interfaces\IDelegateJoinUs;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class DelegateJoinUsRepository extends BaseRepository implements IDelegateJoinUs {

  public function model() {
    return DelegateJoinUs::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
