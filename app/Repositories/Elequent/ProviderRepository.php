<?php

namespace App\Repositories\Elequent;

use App\Entities\Provider;
use App\Repositories\Interfaces\IProvider;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class ProviderRepository extends BaseRepository implements IProvider {
  public function model() {
    return Provider::class;
  }

  public function providers()
  {
      return $this->model->where('user_type',Provider::PROVIDER)->get();
  }
  public function employees()
  {
      return $this->model->where('user_type',Provider::EMPLOYEE)->get();
  }
  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
