<?php

namespace App\Repositories\Elequent;

use App\Entities\ContactUs;
use App\Repositories\Interfaces\IContactUs;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class ContactUsRepository extends BaseRepository implements IContactUs {

  public function model() {
    return ContactUs::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
