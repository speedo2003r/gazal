<?php

namespace App\Repositories\Elequent;

use App\Entities\ReviewRate;
use App\Repositories\Interfaces\IReviewRate;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class ReviewRateRepository extends BaseRepository implements IReviewRate {
  public function model() {
    return ReviewRate::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
