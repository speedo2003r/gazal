<?php

namespace App\Repositories\Elequent;

use App\Entities\Hire;
use App\Repositories\Interfaces\IHire;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class HireRepository extends BaseRepository implements IHire {

  public function model() {
    return Hire::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
