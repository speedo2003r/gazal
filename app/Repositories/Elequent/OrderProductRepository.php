<?php

namespace App\Repositories\Elequent;

use App\Entities\OrderProduct;
use App\Repositories\Interfaces\IOrderProduct;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class OrderProductRepository extends BaseRepository implements IOrderProduct {
  public function model() {
    return OrderProduct::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
