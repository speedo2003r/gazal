<?php

namespace App\Repositories\Elequent;

use App\Entities\SellerPay;
use App\Repositories\Interfaces\ISellerPay;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class SellerPayRepository extends BaseRepository implements ISellerPay {
  public function model() {
    return SellerPay::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
