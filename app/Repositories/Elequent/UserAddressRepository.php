<?php

namespace App\Repositories\Elequent;

use App\Entities\UserAddress;
use App\Repositories\Interfaces\IUserAddress;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class UserAddressRepository extends BaseRepository implements IUserAddress {
  public function model() {
    return UserAddress::class;
  }
  public function saveAddressesToUser($model)
  {
      $addressExist = $this->model->where(['uuid' => request('uuid'), 'user_id' => null])->get();
      if (count($addressExist) > 0) {
          $addresses = $this->model->where(['uuid' => request('uuid'), 'user_id' => null])->get();
          $array     = [];
          foreach ($addresses as $address) {
              $array[] = $address['id'];
          }
          DB::table('user_addresses')->whereIn('id', $array)->update([
              'user_id' => $model['id'],
          ]);
      }
  }
  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
