<?php

namespace App\Repositories\Elequent;

use App\Entities\JoinUs;
use App\Repositories\Interfaces\IJoinUs;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class JoinUsRepository extends BaseRepository implements IJoinUs {
  public function model() {
    return JoinUs::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
