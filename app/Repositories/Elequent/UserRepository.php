<?php

namespace App\Repositories\Elequent;

use App\Entities\Branch;
use App\Entities\Delegate;
use App\Entities\Provider;
use App\Models\User;
use App\Repositories\Interfaces\IUser;
use App\Traits\SmsTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class UserRepository extends BaseRepository implements IUser {
  use SmsTrait;

  public function model() {
    return User::class;
  }

  public function getCurrentModel()
  {
      $user_type = request('user_type');
      if($user_type == 'client'){
          $data = [
              'table'=>'users',
              'model'=>new User(),
              'guard' => 'api'
          ];
      }elseif($user_type == 'delegate'){
          $data = [
              'table'=>'delegates',
              'model'=>new Delegate(),
              'guard' => 'delegateApi'
          ];
      }elseif($user_type == 'branch'){
          $data = [
              'table'=>'branches',
              'model'=>new Branch(),
              'guard' => 'branchApi'
          ];
      }elseif($user_type == 'provider'){
          $data = [
              'table'=>'providers',
              'model'=>new Provider(),
              'guard' => 'api'
          ];
      }
      return $data;
  }
  public function find($id, $columns = ['*']) {
    $model = $this->model->find($id, $columns);
    return $this->parserResult($model);
  }

  public function checkPassword($model, $password) {
    $isSamePassword = Hash::check($password, $model['password']);
    return $isSamePassword;
  }

  public function activeUserPhone($request) {
    $code  = $request['code'];
    $phone = $request['phone'];
    $user  = $this->model->where(['phone' => $phone])->first();

    if ($code == $user->v_code) {
      $user->update(['active' => true, 'online' => true]);

      Auth::login($user);

      return [
        'key' => 'success',
        'msg' => awtTrans('تم تفعيل الهاتف بنجاح'),
      ];
    }

    return ['key' => 'fail', 'msg' => awtTrans('كود التحقق غير صحيح')];
  }

  public function login($attributes, $type = 'web_client') {
    $user = $this->model->where('phone', $attributes['phone'])->first();

    if ('client' != $user->user_type) {
      return ['key' => 'fail', 'msg' => awtTrans('برجاء التأكد من بيانات المستخدم')];
    }

    # If password wrong
    if (!Hash::check($attributes['password'], $user->password)) {
      return ['key' => 'fail', 'msg' => awtTrans('كلمة المرور غير صحيحة')];
    }

    if ($user->banned) {
      $key = 'web_client' == $type ? 'fail' : 'is_banned';
      return ['key' => $key, 'msg' => trans('api.blocked')];
    }

    # If phone not active yet
    if (!$user->active) {

      $this->sendActiveCodeToUserSms($user);

      if ('web_client' == $type) {
        request()->session()->put('user_phone', $user->phone);
      }

      return [
        'key'  => 'not_active',
        'msg'  => awtTrans('يرجى ادخال الكود المرسل الى رقم الجوال'),
        'data' => ['phone' => $user->phone],
      ];
    }

    if ('web_client' == $type) {
      # auth login
      Auth::login($user);

    } else {
      # api service mobile login
    }

    $user->update(['online' => true]);

    return [
      'key'  => 'success',
      'msg'  => awtTrans('تم تسجيل الدخول بنجاح'),
      'user' => $user,
    ];
  }

  public function resendActiveCode($attributes, $type = 'web_client') {
    $user = $this->model
      ->where('phone', $attributes['phone'])
      ->first();

    $this->sendActiveCodeToUserSms($user);

    if ('web_client' == $type) {
      request()->session()->put('user_phone', $user->phone);
    }

    return [
      'key' => 'success',
      'msg' => awtTrans('يرجى ادخال الكود المرسل الى رقم الجوال'),
    ];
  }

  public function updatePasswordByCode($attributes) {
    $user = $this->model
      ->where('phone', $attributes['phone'])
      ->first();

    if ($attributes['code'] != $user->v_code) {
      $msg = trans('api.wrongCode');
      return ['key' => 'fail', 'msg' => $msg];
    }

    $user->update(['v_code' => null, 'password' => $attributes['password']]);

    $msg = awtTrans('تم تعديل كلمة المرور بنجاح, يرجى اعادة تسجيل الدخول');
    return ['key' => 'success', 'msg' => $msg];
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
