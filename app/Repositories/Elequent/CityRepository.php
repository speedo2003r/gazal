<?php

namespace App\Repositories\Elequent;

use App\Entities\City;
use App\Repositories\Interfaces\ICity;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class CityRepository extends BaseRepository implements ICity {

  public function model() {
    return City::class;
  }

  public function getCitiesWithProviders() {
    return $this->model->whereHas('providers')->select('title')->get();
  }

  public function cities() {
    return $this->model->get();
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
