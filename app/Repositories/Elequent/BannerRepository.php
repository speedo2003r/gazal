<?php

namespace App\Repositories\Elequent;

use App\Entities\Banner;
use App\Repositories\Interfaces\IBanner;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class BannerRepository extends BaseRepository implements IBanner {
  public function model() {
    return Banner::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
