<?php

namespace App\Repositories\Elequent;

use App\Entities\RatingList;
use App\Repositories\Interfaces\IRatingList;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class RatingListRepository extends BaseRepository implements IRatingList {
  public function model() {
    return RatingList::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
