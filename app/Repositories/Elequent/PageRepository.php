<?php

namespace App\Repositories\Elequent;

use App\Entities\Page;
use App\Repositories\Interfaces\IPage;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class PageRepository extends BaseRepository implements IPage {

  public function model() {
    return Page::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

  public function getAbout() {
    return $this->model->findOrFail($this->model::ABOUTID);
  }

  public function getContactInfo() {
    return $this->model->findOrFail($this->model::CONTACTID);
  }

  public function getPrivacyPolicy() {
    return $this->model->findOrFail($this->model::PRIVACYID);
  }

  public function getUsagePolicy() {
    return $this->model->findOrFail($this->model::USAGEID);
  }

}
