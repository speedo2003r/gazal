<?php

namespace App\Repositories\Elequent;

use App\Entities\Type;
use App\Repositories\Interfaces\IType;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class TypeRepository extends BaseRepository implements IType {
  public function model() {
    return Type::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
