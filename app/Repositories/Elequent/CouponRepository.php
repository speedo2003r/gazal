<?php

namespace App\Repositories\Elequent;

use App\Entities\Coupon;
use App\Repositories\Interfaces\ICoupon;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class CouponRepository extends BaseRepository implements ICoupon {

  public function model() {
    return Coupon::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
