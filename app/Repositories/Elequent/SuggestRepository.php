<?php

namespace App\Repositories\Elequent;

use App\Entities\Suggest;
use App\Repositories\Interfaces\ISuggest;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class SuggestRepository extends BaseRepository implements ISuggest {
  public function model() {
    return Suggest::class;
  }

  public function boot() {
    $this->pushCriteria(app(RequestCriteria::class));
  }

}
