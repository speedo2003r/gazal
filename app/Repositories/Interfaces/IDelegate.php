<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

interface IDelegate extends RepositoryInterface {
  //
}
