<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

interface ISocial extends RepositoryInterface {
  public function updateall($attributes = []);
}
