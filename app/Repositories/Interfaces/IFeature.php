<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

interface IFeature extends RepositoryInterface {
  //
}
