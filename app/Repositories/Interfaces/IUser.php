<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

interface IUser extends RepositoryInterface {
  public function checkPassword($model, $password);
}
