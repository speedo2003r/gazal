<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

interface ISetting extends RepositoryInterface {
  public function updateall($attributes = []);
}
