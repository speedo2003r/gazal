<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

interface IProvider extends RepositoryInterface {
  //
}
