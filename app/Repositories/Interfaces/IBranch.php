<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

interface IBranch extends RepositoryInterface {
  public function checkPassword($model, $password);
}
