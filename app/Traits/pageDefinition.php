<?php


namespace App\Traits;

use Illuminate\Support\Facades\Route;



trait pageDefinition
{
    public function pageTitle($resource, $single_resource, $resource_en = null)
    {

        if (Route::is('*.*.index')) {
            return $page_title = $resource;
        } elseif (Route::is('*.*.create')) {
            return $page_title = 'اضافه ' . $single_resource;
        } else {
            return $page_title = 'تعديل ' . $single_resource;
        }
    }
    public function pageDescription($resource)
    {
        if (Route::is('*.*.index')) {
            return $page_description = ' يمكنك عرض و تعديل و حذف ايا/ كل    ' . $resource . '';
        } elseif (Route::is('*.*.create')) {
            return $page_description = ' يمكنك انشاء ' . $resource . '';
        } else {
            return $page_description = ' يمكنك تعديل ' . $resource . '';
        }
    }
}
