<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

trait CountryCodeTrait {
  function getCountries() {
    $allCountries = DB::table('all_countries')->get();
    $allCountries = $allCountries->map(function ($country) {
      $imageName = Str::lower($country->iso);
      return [
        'id'         => $country->id,
        'iso'        => $country->iso,
        'nice_name'  => $country->nice_name,
        'phone_code' => $country->phone_code,
        'flage'      => asset("/images/flags/$imageName.png"),
      ];
    });

    return $allCountries;
  }
}
