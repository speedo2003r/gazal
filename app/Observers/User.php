<?php

namespace App\Observers;

use App\Entities\Audit;
use App\Models\User as UserModel;
use App\Repositories\Interfaces\IReport;
use App\Repositories\ReportRepository;
use App\Traits\UploadTrait;
use Illuminate\Support\Str;

class User {
  use UploadTrait;
  public $report, $user;

  public function __construct(IReport $report) {
    $this->report = $report;
    $this->user   = auth()->check() ? auth()->user()['name'] : 'admin';
  }

  public function creating(UserModel $user) {
    # store share_code
    $user->share_code = Str::random(10);
  }

  public function created(UserModel $user) {
    $text = 'قام ' . $this->user . ' ب' . 'أضافة العميل ' . $user->name;
    $this->report->create(['desc' => $text]);
  }

  public function updating(UserModel $user) {
      if($user->isDirty('phone') || $user->isDirty('email') || $user->isDirty('name')){
          $data = [
              'name' => $user->getOriginal('name') ?? $user['name'],
              'email' => $user->getOriginal('email') ?? $user['email'],
              'phone' => $user->getOriginal('phone') ?? $user['phone'],
          ];
          $user->audits()->create([
              'audit_data' => json_encode($data)
          ]);
      }
  }
  public function updated(UserModel $user) {
    $text = 'قام ' . $this->user . ' ب' . 'تحديث العميل ' . $user->name;
    $this->report->create(['desc' => $text]);
  }

  public function deleted(UserModel $user) {
    if (null != $user['avatar']) {
      $this->deleteFile($user['avatar'], 'users');
    }
    $text = 'قام ' . $this->user . ' ب' . 'حذف العميل ' . $user->name;
    $this->report->create(['desc' => $text]);
  }

  public function restored(UserModel $user) {
    //
  }

  public function forceDeleted(UserModel $user) {
    $text = 'قام ' . $this->user . ' ب' . 'حذف العميل نهائيا ' . $user->name;
    $this->report->create(['desc' => $text]);
  }
}
