<?php

namespace App\Observers;

use App\Entities\Offer as OfferModel;
use App\Repositories\Interfaces\IReport;
use App\Repositories\ReportRepository;

class Offer
{
    public $report,$user;
    public function __construct(IReport $report)
    {
        $this->report = $report;
        $this->user = auth()->check() ? auth()->user()['name'] : 'admin';
    }
    /**
     * Handle the Page "created" event.
     *
     * @param  \App\Models\Page  $page
     * @return void
     */
    public function created(OfferModel $offer)
    {
        $text = 'قام ' . $this->user . '' . 'أضافة عرض ' . $offer->title;
        $this->report->create(['desc' => $text]);
    }

    /**
     * Handle the OfferModel "updated" event.
     *
     * @param  \App\Models\OfferModel  $offer
     * @return void
     */
    public function updated(OfferModel $offer)
    {
        $text = 'قام ' . $this->user . '' . 'تحديث عرض ' . $offer->title;
        $this->report->create(['desc' => $text]);
    }

    /**
     * Handle the OfferModel "deleted" event.
     *
     * @param  \App\Models\OfferModel  $offer
     * @return void
     */
    public function deleted(OfferModel $offer)
    {
        $text = 'قام ' . $this->user . '' . 'حذف عرض ' . $offer->title;
        $this->report->create(['desc' => $text]);
    }

    /**
     * Handle the OfferModel "restored" event.
     *
     * @param  \App\Models\OfferModel  $offer
     * @return void
     */
    public function restored(OfferModel $offer)
    {
        //
    }

    /**
     * Handle the OfferModel "force deleted" event.
     *
     * @param  \App\Models\OfferModel  $offer
     * @return void
     */
    public function forceDeleted(OfferModel $offer)
    {
        $text = 'قام ' . $this->user . '' . 'حذف عرض نهائيا ' . $offer->title;
        $this->report->create(['desc' => $text]);
    }
}
