<?php

namespace App\Observers;

use App\Entities\Slider as SliderModel;
use App\Repositories\Interfaces\IReport;
use App\Repositories\ReportRepository;

class Slider
{
    public $report,$user;
    public function __construct(IReport $report)
    {
        $this->report = $report;
        $this->user = auth()->check() ? auth()->user()['name'] : 'admin';
    }
    /**
     * Handle the Page "created" event.
     *
     * @param  \App\Models\Page  $page
     * @return void
     */
    public function created(SliderModel $slider)
    {
        $text = 'قام ' . $this->user . '' . ' بأضافة بانر ' . $slider->title;
        $this->report->create(['desc' => $text]);
    }

    /**
     * Handle the SliderModel "updated" event.
     *
     * @param  \App\Models\SliderModel  $slider
     * @return void
     */
    public function updated(SliderModel $slider)
    {
        $text = 'قام ' . $this->user . '' . ' بتحديث بانر ' . $slider->title;
        $this->report->create(['desc' => $text]);
    }

    /**
     * Handle the SliderModel "deleted" event.
     *
     * @param  \App\Models\SliderModel  $slider
     * @return void
     */
    public function deleted(SliderModel $slider)
    {
        $text = 'قام ' . $this->user . '' . ' بحذف بانر ' . $slider->title;
        $this->report->create(['desc' => $text]);
    }

    /**
     * Handle the SliderModel "restored" event.
     *
     * @param  \App\Models\SliderModel  $slider
     * @return void
     */
    public function restored(SliderModel $slider)
    {
        //
    }

    /**
     * Handle the SliderModel "force deleted" event.
     *
     * @param  \App\Models\SliderModel  $slider
     * @return void
     */
    public function forceDeleted(SliderModel $slider)
    {
        $text = 'قام ' . $this->user . '' . ' بحذف بانر نهائيا ' . $slider->title;
        $this->report->create(['desc' => $text]);
    }
}
