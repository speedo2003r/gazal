<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UpdateDriverLocation implements ShouldBroadcast {
  use Dispatchable, InteractsWithSockets, SerializesModels;

  public $driver_id;
  public $lat;
  public $lng;

  public function __construct($driver_id, $lat, $lng) {
    $this->driver_id = $driver_id;
    $this->lat       = $lat;
    $this->lng       = $lng;
  }

  public function broadcastOn() {
    return new PrivateChannel('driver.' . $this->driver_id);
  }

  public function broadcastAs() {
    return 'location.updated';
  }

  public function broadcastWith() {
    return [
      'lat' => $this->lat,
      'lng' => $this->lng,
    ];
  }
}
