<?php

namespace App\Events;

use App\Entities\Order;
use App\Http\Resources\Orders\OrderDetailResource;
use App\Traits\ResponseTrait;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UpdateOrderEvent implements ShouldBroadcast {
  use Dispatchable, InteractsWithSockets, SerializesModels, ResponseTrait;

  public $order_id;

  public function __construct($order_id) {
    $this->order_id = $order_id;
  }

  public function broadcastOn() {
    return new PrivateChannel('order.' . $this->order_id);
  }

  public function broadcastAs() {
    return 'order.updated';
  }

  public function broadcastWith() {
    $order = Order::find($this->order_id);

    if ($order) {
      return $this->successResponse([
        'order_status'     => $order['order_status'],
        'order_status_msg' => $order['order_status_msg'],
        'details'          => new OrderDetailResource($order),
      ]);
    }

    return [
      'order_id' => $this->order_id,
    ];

  }
}
