<?php

namespace App\Models;

use App\Entities\Audit;
use App\Entities\BlockUser;
use App\Entities\City;
use App\Entities\Country;
use App\Entities\Credit;
use App\Entities\Device;
use App\Entities\Favourite;
use App\Entities\Gift;
use App\Entities\Notification;
use App\Entities\Order;
use App\Entities\ReviewRate;
use App\Entities\Suggest;
use App\Entities\UserAddress;
use App\Entities\Wallet;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject {
  use HasFactory, Notifiable, UploadTrait, SoftDeletes, CascadeSoftDeletes;

  // protected $cascadeDeletes = ['sliders', 'provider', 'addresses'];
  // protected $with           = ['delegate'];
  protected $fillable = [
    // 'uuid',
    'name',
    // 'last_name',
    // 'username',
    'avatar',
    'email',
    'wallet',
    'country_code',
    'phone',
    'replace_phone',
    'birth_date',
    'gender',
    'v_code',
    'password',
    'lang',
    'active',
    'banned',
    'accepted',
    'notify',
    'online',
    'country_id',
    'city_id',
    'address',
    'lat',
    'lng',
    'share_code',
    'friend_share_code',
    'is_friend_share_code_used',
    'zone_id',
  ];

  protected $hidden = [
    'password',
  ];


  public function audits() {
    return $this->morphMany(Audit::class, 'audit', 'modal_type', 'modal_id');
  }

  public function ScopeActive($query) {
    return $query->where('active', 1)
      ->where('banned', 0)
      ->where('accepted', 1);
  }

  public static function workDays($index = null) {
    $arr = [
      1 => "السبت",
      2 => "الأحد",
      3 => "الاثنين",
      4 => "الثلاثاء",
      5 => "الأربعاء",
      6 => "الخميس",
      7 => "الجمعه",
    ];

    if (null != $index) {
      return $arr[$index];
    } else {
      return $arr;
    }
  }

  public function getJWTIdentifier() {
    return $this->getKey();
  }

  public function getJWTCustomClaims() {
    return [];
  }

  public function getAvatarAttribute($value) {
    if ('/default.png' == $value || null == $value) {
      return dashboard_url('images/users/default.png');
    }
    return dashboard_url('storage/images/users/' . $value);
  }

  public function getFullPhoneAttribute() {
    return $this->attributes['country_code'] . ltrim($this->attributes['phone'], '0');
  }

  public function setPasswordAttribute($value) {
    $this->attributes['password'] = Hash::make($value);
  }
    public function gift() {
        return $this->gifts()->where('expire_date', '>=', Carbon::now()->format('Y-m-d'))->sum('amount');
    }

  public function setCountryCodeAttribute($value) {
    if (!empty($value)) {
      $country_code                     = convert_to_english($value);
      $country_code                     = ltrim($country_code, "00");
      $this->attributes['country_code'] = $country_code;
    }
  }

  public function setPhoneAttribute($value) {
    if (!empty($value)) {
      $this->attributes['phone'] = ltrim(convert_to_english($value), '0');
    }
  }
  public function suggests() {
    return $this->hasMany(Suggest::class, 'user_id');
  }

  public function wallets() {
    return $this->hasMany(Wallet::class, 'user_id');
  }

  public function gifts() {
    return $this->hasMany(Gift::class, 'user_id');
  }

  public function blockUsers() {
    return $this->morphMany(BlockUser::class, 'modalable','modal_type','modal_id');
  }

  //  public function offers() {
  //     return $this->hasMany(Offer::class, 'user_id');
  //   }


  public function reviews() {
    return $this->morphMany(ReviewRate::class, 'modelable','model_type','model_id');
  }
  public function userRatings() {
    return $this->morphMany(ReviewRate::class, 'rateable', 'rateable_type', 'rateable_id');
  }

  //  public function items() {
  //     return $this->hasMany(Item::class, 'user_id', 'id');
  //   }

  public function credits() {
    return $this->morphMany(Credit::class, 'modelable', 'model_type','model_id');
  }

  public function ordersAsUser() {
    return $this->hasMany(Order::class, 'user_id', 'id');
  }

  public function notifications() {
    return $this->morphMany(Notification::class, 'tomodel','tomodel_type','tomodel_id')->orderByDesc('id');
  }


  public function country() {
    return $this->belongsTo(Country::class);
  }

  public function city() {
    return $this->belongsTo(City::class);
  }

  public function addresses() {
    return $this->hasMany(UserAddress::class);
  }

  public function devices() {
    return $this->morphMany(Device::class,'modelable','model_type','model_id');
  }

  public function favs() {
    return $this->hasMany(Favourite::class);
  }

  public function getFavsItemsCountAttribute() {
    return $this->favs()->where('favoritable_type', 'App\Entities\Item')->count();
  }

  public function getFavsBranchesCountAttribute() {
    return $this->favs()->where('favoritable_type', 'App\Entities\Branch')->count();
  }
}
