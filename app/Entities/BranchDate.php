<?php

namespace App\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Branch.
 *
 * @package namespace App\Entities;
 */
class BranchDate extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    const NORMAL = 1;
    const RAMADAN = 2;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'timefrom',
        'timeto',
        'day',
        'shift',
        'type',
        'branch_id',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function getTimefromAttribute($value)
    {
        return date('h:i a',strtotime($value));
    }
    public function getTimetoAttribute($value)
    {
        return date('h:i a',strtotime($value));
    }

    public static function Days($index = null)
    {
        $arr = [
            1 => __('Saturday'),
            2 => __('Sunday'),
            3 => __('Monday'),
            4 => __('Tuesday'),
            5 => __('Wednesday'),
            6 => __('Thursday'),
            7 => __('Friday'),
        ];
        if($index != null){
            return $arr[$index];
        }
        return $arr;
    }
}
