<?php

namespace App\Entities;

use App\Models\User;
use Database\Factories\SliderFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class Slider extends Model implements Transformable
{
    use HasFactory;
    use TransformableTrait;
    use SoftDeletes;
    use HasTranslations;

    public $translatable = ['title'];
    protected $fillable = [
        'title',
        'image',
        'country_id',
        'city_id',
        'category_id',
        'user_id',
        'item_id',
        'active',
        'type',
    ];

    protected static function newFactory()
    {
        return new SliderFactory();
    }
    public function getImageAttribute($value)
    {
        if(filter_var($value, FILTER_VALIDATE_URL)){
            return $value;
        }
        return dashboard_url('storage/images/sliders/'.$value);
    }
    public static function rsine($coordinates)
    {
        return '(6371 * acos(cos(radians(' . $coordinates['lat'] . '))
        * cos(radians(`lat`))
        * cos(radians(`lng`)
        - radians(' . $coordinates['lng'] . '))
        + sin(radians(' . $coordinates['lat'] . '))
        * sin(radians(`lat`))))';
    }

    public function scopeDistance($query, $lat, $lng, $city_id = null, $unit = "km")
    {

        $unit = ($unit === "km") ? 6378.10 : 3963.17;
        $lat = (float) $lat;
        $lng = (float) $lng;
        $sql =  "($unit * ACOS(COS(RADIANS($lat))
                * COS(RADIANS(branches.lat))
                * COS(RADIANS($lng) - RADIANS(branches.lng))
                + SIN(RADIANS($lat))
                * SIN(RADIANS(branches.lat))))";
        if($city_id != null){
            return Slider::query()->where('branches.city_id',$city_id)
                ->with('user')
                ->select(DB::raw("sliders.*,branches.id as branch_id, $sql AS distance")
                )
                ->leftjoin('users','users.id','=','sliders.user_id')
                ->leftjoin('branches','users.id','=','branches.provider_id')
                ->orderBy('distance','asc');
        }else{
            return Slider::query()
                ->with('user')
                ->select(DB::raw("sliders.*,branches.id as branch_id, $sql AS distance")
                )
                ->leftjoin('users','users.id','=','sliders.user_id')
                ->leftjoin('branches','users.id','=','branches.provider_id')
                ->orderBy('distance','asc');
        }

    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function ScopeActive($query)
    {
        return $query->where('active',1)->where('city_id',request()->get('city_id'))->where('category_id',request()->get('category_id'));
    }
}
