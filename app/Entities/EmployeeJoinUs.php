<?php

namespace App\Entities;

use App\Traits\UploadTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class EmployeeJoinUs extends Model implements Transformable {
  use TransformableTrait, SoftDeletes, UploadTrait;

  protected $fillable = [
    'name',
    'phone',
    'email',
    'job_description',
    'city_id',
    'cv',
    'desc',
    'status',
  ];

  public function getCvAttribute() {
    if ($this->attributes['cv']) {
      $file = $this->getFile($this->attributes['cv'], 'employJoins');
    } else {
      $file = '';
    }
    return $file;
  }

  public function setCvAttribute($value) {
    if (!empty($value)) {
      $this->attributes['cv'] = $this->uploadFileExt($value, 'employJoins');
    }
  }

  public function city() {
    return $this->belongsTo(City::class, 'city_id');
  }

}
