<?php

namespace App\Entities;

use Database\Factories\BannerFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class DiscountAmount extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $fillable = [
        'amount',
        'pay_type',
        'created_date',
        'provider_id',
        'discount_reason_id',
    ];

    public function discountReason()
    {
        return $this->belongsTo(DiscountReason::class);
    }

    public function payments()
    {
        return $this->hasMany(DiscountAmountPayment::class);
    }

    public static function methods($value = null)
    {
        $arr = [
            1=>__('cash'),
            2=>__('visa'),
            3=>__('master'),
        ];
        if($value == null){
            return $arr;
        }
        return $arr[$value];
    }
}
