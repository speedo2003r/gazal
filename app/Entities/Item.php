<?php

namespace App\Entities;

use App\Entities\Favourite;
use App\Models\User;
use Database\Factories\ItemFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Item.
 *
 * @package namespace App\Entities;
 */
class Item extends Model implements Transformable {
  use HasFactory;
  use TransformableTrait;
  use SoftDeletes;
  use HasTranslations;

  public $translatable = ['title', 'description'];

  protected $fillable = [
    'title',
    'description',
    'price',
    'discount_price',
    'from',
    'to',
    'user_id',
    'exist',
    'views',
    'status',
    'category_id',
    'subcategory_id',
  ];

  public function ScopeSubexist($query) {
    return $query->whereHas('user', function ($value) {
      $value->subscribe();
    });
  }
  protected static function newFactory()
   {
       return new ItemFactory();
   }

  public function getMainImageAttribute($value) {
      if(filter_var($value, FILTER_VALIDATE_URL)){
          return $value;
      }
    if ($this->main() == null) {
      return dashboard_url('images/placeholder.png');
    }
    return dashboard_url('storage/images/items/' . $this->main()['image']);
  }

  public function getIsFavAttribute(): bool {
    $isFav = false;

    if (auth()->check()) {
      $isFav = Favourite::where('user_id', auth()->id())
        ->where('favoritable_id', $this['id'])
        ->where('favoritable_type', Item::class)
        ->exists();
    } elseif (getBrowserUuid()) {
      $isFav = Favourite::where('uuid', getBrowserUuid())
        ->where('user_id', null)
        ->where('favoritable_id', $this['id'])
        ->where('favoritable_type', Item::class)
        ->exists();
    }

    return $isFav;
  }

  public function price() {
    $price          = $this->price;
    $discount_price = $this->discount_price;
    $from           = $this->from;
    $to             = $this->to;
    if (NULL != $discount_price && $discount_price > 0) {

      $paymentDate = date('Y-m-d');
      $paymentDate = date('Y-m-d', strtotime($paymentDate));

      if (NULL == $from) {
        $contractDateBegin = date('Y-m-d');
      } else {
        $contractDateBegin = date('Y-m-d', strtotime($from));
      }

      if (NULL == $to) {
        $datetime        = new \DateTime('tomorrow');
        $contractDateEnd = $datetime->format('Y-m-d');
      } else {
        $contractDateEnd = date('Y-m-d', strtotime($to));
      }

      if (($paymentDate >= $contractDateBegin) && ($paymentDate <= $contractDateEnd)) {
        $price = $this->price - ($this->price * $discount_price / 100);
      }
    }
    return (string) round($price, 2);
  }
  public function discount() {
    $discount_price = $this->discount_price;
    $from           = $this->from;
    $to             = $this->to;
    if (NULL != $discount_price && $discount_price > 0) {

      $paymentDate = date('Y-m-d');
      $paymentDate = date('Y-m-d', strtotime($paymentDate));

      if (NULL == $from) {
        $contractDateBegin = date('Y-m-d');
      } else {
        $contractDateBegin = date('Y-m-d', strtotime($from));
      }

      if (NULL == $to) {
        $datetime        = new \DateTime('tomorrow');
        $contractDateEnd = $datetime->format('Y-m-d');
      } else {
        $contractDateEnd = date('Y-m-d', strtotime($to));
      }

      if (($paymentDate >= $contractDateBegin) && ($paymentDate <= $contractDateEnd)) {
        return $discount_price;
      }
    }
    return 0;
  }
  public function _price_() {
    $price       = $this->price;
    $hasDiscount = 0;
    if (NULL != $this->discount_price && $this->discount_price > 0) {

      $paymentDate = date('Y-m-d');
      $paymentDate = date('Y-m-d', strtotime($paymentDate));

      if (NULL == $this->from) {
        $contractDateBegin = date('Y-m-d');
      } else {
        $contractDateBegin = date('Y-m-d', strtotime($this->from));
      }

      if (NULL == $this->to) {
        $datetime        = new DateTime('tomorrow');
        $contractDateEnd = $datetime->format('Y-m-d');
      } else {
        $contractDateEnd = date('Y-m-d', strtotime($this->to));
      }

      if (($paymentDate >= $contractDateBegin) && ($paymentDate <= $contractDateEnd)) {
        $price       = $this->discount_price;
        $hasDiscount = 1;
      }
    }
    if (1 == $hasDiscount) {
      return $this->price;
    } else {
      return 0;
    }
  }
  public function qty() {
    if ('single' == $this->type) {
      $qty = $this->count;
    } else {
      $qty = $this->groups()->where('properties', '!=', NULL)->sum('count');
    }
    return $qty;
  }

  public function files() {
    return $this->morphMany(Image::class, 'imageable', 'image_type', 'image_id');
  }
  public function main() {
    return $this->files()->where('main', 1)->first();
  }

  public function user() {
    return $this->belongsTo(Provider::class);
  }

  public function category() {
    return $this->belongsTo(Category::class);
  }

  public function subcategory() {
    return $this->belongsTo(Category::class, 'subcategory_id');
  }

  public function brand() {
    return $this->belongsTo(Category::class, 'brand_id');
  }

  public function coupon() {
    return $this->hasOne(Coupon::class, 'item_id', 'id')->withTrashed();
  }

  public function types() {
    return $this->hasMany(ItemType::class, 'item_id', 'id');
  }

  public function selling_views() {
    return $this->hasMany(SellingView::class, 'item_id', 'id');
  }

  public function features() {
    return $this->hasMany(Feature::class, 'item_id', 'id');
  }

  public function details() {
    return $this->hasMany(ItemType::class, 'item_id', 'id');
  }

  public function orders() {
    return $this->hasMany(OrderProduct::class, 'item_id', 'id');
  }

  public function reviews() {
    return $this->morphMany(ReviewRate::class, 'rateable', 'rateable_type', 'rateable_id');
  }

  public function branches() {
    return $this->belongsToMany(Branch::class, 'item_branches', 'item_id', 'branch_id')->withPivot('category_id');
  }
}
