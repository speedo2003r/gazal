<?php

namespace App\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class Complaint extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'user_id',
        'notes',
        'status',
        'solution',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function allStatus($index)
    {
        $arr = [
            'created'=>'تم الاضافه',
            'accepted'=>'تم الاستلام',
            'underWork'=>'جاري المتابعه',
            'solved'=>'تم الحل',
        ];
        if($index != null){
            return $arr[$index];
        }
        return $arr;
    }
}
