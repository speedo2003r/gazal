<?php

namespace App\Entities;

use App\Models\User;
use Database\Factories\GroupFactory;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Group.
 *
 * @package namespace App\Entities;
 */
class Group extends Model implements Transformable
{
    use HasFactory;
    use TransformableTrait;
    use HasTranslations;
    use SoftDeletes;
    use CascadeSoftDeletes;
    protected $cascadeDeletes = ['category'];
    public $translatable = ['title'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'category_id',
    ];

    protected static function newFactory()
    {
        return new GroupFactory();
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
