<?php

namespace App\Entities;

use App\Models\User;
use Database\Factories\CategoryFactory;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Category.
 *
 * @package namespace App\Entities;
 */
class Category extends Model implements Transformable
{
    use HasFactory;
    use TransformableTrait;
    use HasTranslations;
    use SoftDeletes;
    use CascadeSoftDeletes;
    protected $cascadeDeletes = ['children'];
    public $translatable = ['title'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'icon',
        'parent_id',
    ];

    protected static function newFactory()
    {
        return new CategoryFactory();
    }
    public function getIconAttribute($value)
    {
        if(filter_var($value, FILTER_VALIDATE_URL)){
            return $value;
        }
        if($value == null){
            return  dashboard_url('images/placeholder.png');
        }
        return  dashboard_url('storage/images/categories/'. $value);
    }

    public function children()
    {
        return $this->hasMany(Category::class,'parent_id','id');
    }
    public function groups()
    {
        return $this->hasMany(Group::class,'category_id','id');
    }
    public function parent()
    {
        return $this->belongsTo(Category::class,'parent_id');
    }
    public function users()
    {
        return $this->belongsToMany(User::class,'user_categories','subcategory_id','user_id');
    }
    public function items()
    {
        return $this->hasMany(Item::class,'category_id','id');
    }
    public function providers()
    {
        return $this->belongsToMany(Provider::class,'provider_categories','subcategory_id','user_id')
            ->withPivot('is_active', 'is_main');
    }
    public function subitems()
    {
        return $this->hasMany(Item::class,'subcategory_id','id');
    }
}
