<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Notification.
 *
 * @package namespace App\Entities;
 */
class Notification extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use HasTranslations;

    public $translatable = ['title'];
    protected $fillable = [
        'tomodel_id',
        'tomodel_type',
        'frommodel_id',
        'frommodel_type',
        'image',
        'message_ar',
        'message_en',
        'type',
        'order_id',
        'order_status',
        'seen',
    ];

    public function getMessageAttribute()
    {
        $attr = 'message_' . app()->getLocale();
        return $this->attributes[$attr];
    }

    public function getImageAttribute($value)
    {
        if($value == null){
            return  dashboard_url('images/placeholder.png');
        }
        return  dashboard_url('storage/images/notifications/'.$value);
    }
    public function tomodelable()
    {
        return $this->morphTo('tomodel');
    }
    public function frommodelable()
    {
        return $this->morphTo('frommodel');
    }
    public function To()
    {
        return $this->belongsTo($this->attributes['tomodel_type'],'tomodel_id');
    }
    public function From()
    {
        return $this->belongsTo($this->attributes['frommodel_type'],'frommodel_id');
    }

}
