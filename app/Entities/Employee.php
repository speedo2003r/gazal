<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class Employee extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'avatar',
        'name',
        'email',
        'phone',
        'address',
    ];

    public function getAvatarAttribute($value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL)) {
            return $value;
        }
        if ('/default.png' == $value || null == $value) {
            return dashboard_url('images/users/default.png');
        }

        return dashboard_url('storage/images/employees/'.$value);
    }

}
