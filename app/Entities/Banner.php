<?php

namespace App\Entities;

use Database\Factories\BannerFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class Banner extends Model implements Transformable
{
    use HasFactory;
    use TransformableTrait;
    use SoftDeletes;
    use HasTranslations;

    public $translatable = ['title'];
    protected $fillable = [
        'title',
        'image',
        'country_id',
        'city_id',
        'category_id',
        'user_id',
        'start_date',
        'end_date',
        'item_id',
        'active',
        'type',
    ];
    protected static function newFactory()
    {
        return new BannerFactory();
    }
    public function getImageAttribute($value)
    {
        if(filter_var($value, FILTER_VALIDATE_URL)){
            return $value;
        }
        return dashboard_url('storage/images/banners/'.$value);
    }

}
