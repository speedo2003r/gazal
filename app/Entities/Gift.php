<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class Gift extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $fillable = [
        'amount',
        'notes',
        'expire_date',
        'user_id',
    ];

    public function providers()
    {
        return $this->belongsToMany(Provider::class,'gift_providers','gift_id','provider_id');
    }
    public function user()
    {
        return $this->belongsTo();
    }
}
