<?php

namespace App\Entities;

use App\Models\User;
use Database\Factories\DelegateFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class Delegate.
 *
 * @package namespace App\Entities;
 */
class Delegate extends Authenticatable implements JWTSubject
{
    use HasFactory;
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'avatar',
        'email',
        'wallet',
        'commission_status',
        'income',
        'balance',
        'commission',
        'country_code',
        'phone',
        'replace_phone',
        'v_code',
        'password',
        'birth_date',
        'address',
        'lat',
        'lng',
        'country_id',
        'city_id',
        'id_avatar',
        'licence_car_image',
        'licence_image',
        'car_front_image',
        'car_back_image',
        'car_type_id',
        'lang',
        'active',
        'banned',
        'accepted',
        'notify',
        'online',
        'max_dept',
        'share_code',
        'friend_share_code',
        'is_friend_share_code_used',
    ];

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }

    public function ScopeActive($query)
    {
        return $query->where('active', 1)->where('banned', 0)->where('accepted', 1);
    }

    public function reviews() {
        return $this->morphMany(ReviewRate::class, 'modelable','model_type','model_id');
    }
    public function userRatings() {
        return $this->morphMany(ReviewRate::class, 'rateable', 'rateable_type', 'rateable_id');
    }
    public function credits() {
        return $this->morphMany(Credit::class, 'modelable', 'model_type','model_id');
    }
    public function notifications() {
        return $this->morphMany(Notification::class, 'tomodel','tomodel_type','tomodel_id')->orderByDesc('id');
    }
    public function devices() {
        return $this->morphMany(Device::class,'modelable','model_type','model_id');
    }

    public function hires()
    {
        return $this->hasManyThrough(Hire::class,ProvidersDelegate::class,'delegate_id','id','id','hire_id');
    }
    protected static function newFactory()
    {
        return new DelegateFactory();
    }

    public function refuseOrders() {
        return $this->belongsToMany(Order::class, 'refuse_orders', 'user_id', 'order_id');
    }
    public function ratings() {
        return $this->morphMany(ReviewRate::class, 'rateable', 'rateable_type', 'rateable_id');
    }
    public function carTypes()
    {
        return $this->hasMany(CarType::class,'car_type_id');
    }
    public function getFullPhoneAttribute() {
        return $this->attributes['country_code'] . ltrim($this->attributes['phone'], '0');
    }
    public function setPasswordAttribute($value) {
        $this->attributes['password'] = Hash::make($value);
    }
    public function getAvatarAttribute($value) {
        if(filter_var($value, FILTER_VALIDATE_URL)){
            return $value;
        }
        if ('/default.png' == $value || null == $value) {
            return dashboard_url('images/users/default.png');
        }
        return dashboard_url('storage/images/delegates/' . $value);
    }
    public function getLicenceImageAttribute($value)
    {
        if(filter_var($value, FILTER_VALIDATE_URL)){
            return $value;
        }
        if($value == '/images/placeholder.png' || $value == null){
            return  dashboard_url('images/placeholder.png');
        }
        return  dashboard_url('storage/images/delegates/'. $value);
    }
    public function getIdAvatarAttribute($value)
    {
        if(filter_var($value, FILTER_VALIDATE_URL)){
            return $value;
        }
        if($value == '/images/placeholder.png' || $value == null){
            return  dashboard_url('images/placeholder.png');
        }
        return  dashboard_url('storage/images/delegates/'. $value);
    }
    public function getLicenceCarImageAttribute($value)
    {
        if(filter_var($value, FILTER_VALIDATE_URL)){
            return $value;
        }
        if($value == '/images/placeholder.png' || $value == null){
            return  dashboard_url('images/placeholder.png');
        }
        return  dashboard_url('storage/images/delegates/'. $value);
    }
    public function getCarFrontImageAttribute($value)
    {
        if(filter_var($value, FILTER_VALIDATE_URL)){
            return $value;
        }
        if($value == '/images/placeholder.png' || $value == null){
            return  dashboard_url('images/placeholder.png');
        }
        return  dashboard_url('storage/images/delegates/'. $value);
    }
    public function getCarBackImageAttribute($value)
    {
        if(filter_var($value, FILTER_VALIDATE_URL)){
            return $value;
        }
        if($value == '/images/placeholder.png' || $value == null){
            return  dashboard_url('images/placeholder.png');
        }
        return  dashboard_url('storage/images/delegates/'. $value);
    }
    public function orders() {
        return $this->hasMany(Order::class, 'delegate_id', 'id');
    }
    public function blockUsers() {
        return $this->morphMany(BlockUser::class, 'modalable','modal_type','modal_id');
    }

}
