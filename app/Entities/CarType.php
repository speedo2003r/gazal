<?php

namespace App\Entities;

use Database\Factories\CarTypeFactory;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Category.
 *
 * @package namespace App\Entities;
 */
class CarType extends Model implements Transformable
{
    use HasFactory;
    use TransformableTrait;
    use HasTranslations;
    use SoftDeletes;
    public $translatable = ['title'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'image',
    ];

    protected static function newFactory()
    {
        return new CarTypeFactory();
    }

    public function delegates()
    {
        return $this->hasMany(Delegate::class,'car_type_id');
    }
    public function getImageAttribute($value)
    {
        if($value == null){
            return  dashboard_url('images/placeholder.png');
        }
        return  dashboard_url('storage/images/carTypes/'. $value);
    }

}
