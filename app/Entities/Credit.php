<?php

namespace App\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Bank.
 *
 * @package namespace App\Entities;
 */
class Credit extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'holder_name',
        'card_number',
        'expire_card',
        'model_id',
        'model_type',
    ];

    public function modelable()
    {
        return $this->morphTo();
    }
    public function user()
    {
        return $this->belongsTo($this->attributes['model_type'],'model_id');
    }

}
