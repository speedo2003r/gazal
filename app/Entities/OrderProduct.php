<?php

namespace App\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Order.
 *
 * @package namespace App\Entities;
 */
class OrderProduct extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    protected $fillable = [
        'order_id',
        'item_id',
        'price',
        'qty',
        'notes'
    ];

    public function _price()
    {
        $count = $this->qty;
        $price = $this->price;
        $total = ($price) * $count;
        return $total;
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function item()
    {
        return $this->belongsTo(Item::class)->withTrashed();
    }
    public function features()
    {
        return $this->hasMany(OrderProductFeature::class,'order_product_id','id');
    }
    public function options()
    {
        return $this->hasMany(OrderProductType::class,'order_product_id','id');
    }
    public function _single_price()
    {
        $featuresSum = $this->features->sum('price');
        $optionsSum = $this->options->sum('price');
        $total = ($featuresSum + $optionsSum);
        return $total;
    }
}
