<?php

namespace App\Entities;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Coupon.
 *
 * @package namespace App\Entities;
 */
class Coupon extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'limit',
        'type',
        'kind',
        'country_id',
        'city_id',
        'code',
        'value',
        'count',
        'min_order_amount',
        'max_percent_amount',
        'start_date',
        'end_date',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function providers()
    {
        return $this->belongsToMany(Provider::class,'coupon_providers','coupon_id','provider_id');
    }

    public function couponValue($value,$seller_id = null)
    {
        $price = 0;
        if($this->start_date <= Carbon::now()->format('Y-m-d') && $this->end_date >= Carbon::now()->format('Y-m-d')){
            if($this->type == 'public'){
                if($this->kind == 'percent'){
                    $price = $value * $this->value / 100;
                }elseif ($this->kind == 'fixed'){
                    $price = $value - $this->value;
                }
            }elseif ($this->type == 'private'){
                if(in_array($seller_id,$this->providers()->pluck('id')->toArray())){
                    if($this->kind == 'percent'){
                        $price = $value * $this->value / 100;
                    }elseif ($this->kind == 'fixed'){
                        $price = $value - $this->value;
                    }
                }
            }
        }
        return (string) round($price,2);
    }
}
