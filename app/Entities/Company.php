<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class Company extends Model implements Transformable
{
    use HasFactory;
    use TransformableTrait;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'avatar',
        'email',
        'phone',
        'replace_phone',
        'v_code',
        'password',
        'lat',
        'lng',
        'address',
        'tax_num',
        'commercial_num',
        'contract_num',
        'country_id',
        'city_id',
        'id_avatar',
        'lang',
        'active',
        'banned',
        'accepted',
        'notify',
        'online',
        'max_dept',
    ];
//    protected static function newFactory()
//    {
//        return new BannerFactory();
//    }
    public function getAvatarAttribute($value)
    {
        if(filter_var($value, FILTER_VALIDATE_URL)){
            return $value;
        }
        if ('/default.png' == $value || null == $value) {
            return dashboard_url('images/users/default.png');
        }
        return dashboard_url('storage/images/companies/'.$value);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function blockUsers() {
        return $this->morphMany(BlockUser::class, 'modalable','modal_type','modal_id');
    }

    public function contactDetails()
    {
        return $this->morphMany(UserContactDetail::class,'modelable','model_type','model_id');
    }
    public function orders()
    {
        return $this->hasManyThrough(Order::class,Delegate::class,'company_id','delegate_id');
    }
}
