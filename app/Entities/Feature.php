<?php

namespace App\Entities;

use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

class Feature extends Model implements Transformable
{
    use TransformableTrait;
    use HasTranslations;
    public $translatable = ['title'];
    use SoftDeletes;
    protected $fillable = [
        'title',
        'price',
        'item_id'
    ];


    public function item()
    {
        return $this->belongsTo(Item::class);
    }

}
