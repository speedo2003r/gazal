<?php

namespace App\Entities;

use Database\Factories\ItemBranchFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ItemBranch.
 *
 * @package namespace App\Entities;
 */
class ItemBranch extends Model implements Transformable
{
    use HasFactory;
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_id',
        'item_id',
        'category_id',
    ];

    protected static function newFactory()
    {
        return new ItemBranchFactory();
    }
}
