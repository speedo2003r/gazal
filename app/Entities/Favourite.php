<?php

namespace App\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Favourite.
 *
 * @package namespace App\Entities;
 */
class Favourite extends Model implements Transformable
{
    use TransformableTrait;
    protected $table = 'favourites';
    protected $fillable = [
        'uuid',
        'user_id',
        'favoritable_id',
        'favoritable_type',
    ];

    public function favoritable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function provider()
    {
        return $this->belongsTo(User::class,'favoritable_id');
    }
    public function item()
    {
        return $this->belongsTo(Item::class,'favoritable_id');
    }
    public function branch()
    {
        return $this->belongsTo(Branch::class,'favoritable_id');
    }

}
