<?php

namespace App\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Order.
 *
 * @package namespace App\Entities;
 */
class Order extends Model implements Transformable {
  use TransformableTrait;
  use SoftDeletes;
  protected $fillable = [
    'uuid',
    'order_num',
    'user_id',
    'provider_id',
    'delegate_id',
    'branch_id',
    'total_items',
    'coupon_id',
    'coupon_num',
    'coupon_type',
    'coupon_amount',
    'vat_per',
    'vat_amount',
    'shipping_price',
    'final_total',
    'commission',
    'live',
    'status',
    'delegate_status',
    'provider_status',
    'order_type',
    'pay_type',
    'pay_status',
    'pay_data',
    'lat',
    'lng',
    'map_desc',
    'receiving_lat',
    'receiving_lng',
    'receiving_map_desc',
    'order_type',
    'order_value',
    'address_id',
    'accepted_date',
    'pickup_date',
    'delivered_date',
    'created_date',
    'time',
    'date',
    'notes',
    'progress_start',
    'progress_end',
    'refused_notes',
    'user_delete',
    'provider_delete',
    'admin_delete',
    'device_type',
    'order_status',
  ];

  protected $casts = [
    'pay_data' => 'array',
  ];

  const STATUS_WAITING             = 0;
  const STATUS_ACCEPTED            = 1;
  const STATUS_PREPARED            = 2;
  const STATUS_GIVE_TO_DELEGATE    = 3;
  const STATUS_ON_WAY              = 4;
  const STATUS_ARRIVED_TO_CLIENT   = 5;
  const STATUS_FINISH              = 6;
  const STATUS_REFUSED             = 7;
  const STATUS_CANCELED            = 8;

  public function scopeExists($value) {
    return $value->where('live', 1)->where('status', '!=', 'user_cancel')->whereHas('provider', function ($provider) {
      $provider->where('deleted_at', '=', null);
    });
  }

  public function getOrderStatusMsgAttribute() {
    return trans("order.$this->order_status");
  }

  public function user() {
    return $this->belongsTo(User::class, 'user_id')->withTrashed();
  }

  public function provider() {
    return $this->belongsTo(Provider::class, 'provider_id');
  }
  public function delegate() {
    return $this->belongsTo(Delegate::class, 'delegate_id');
  }

  public function images() {
    return $this->morphMany(Image::class, 'imageable', 'image_type', 'image_id');
  }

  public function branch() {
    return $this->belongsTo(Branch::class, 'branch_id');
  }

    public function scopeUnderPrepareOrder($value)
    {
        $value->whereNotIn('order_status',[Order::STATUS_REFUSED,Order::STATUS_CANCELED,Order::STATUS_WAITING]);
  }
  public function address() {
    return $this->belongsTo(UserAddress::class, 'address_id');
  }

  public function refuseOrders() {
    return $this->belongsToMany(Delegate::class, 'refuse_orders', 'order_id', 'delegate_id')->withPivot('notes');
  }

  public function orderProducts() {
    return $this->hasMany(OrderProduct::class, 'order_id');
  }
  public function price() {
    $total         = 0;
    $orderProducts = $this->orderProducts;
    foreach ($orderProducts as $orderProduct) {
      $total += (($orderProduct->features()->sum('price') * $orderProduct['qty']) + ($orderProduct->options()->sum('price') * $orderProduct['qty']));
    }
    return $total;
  }

  public function _price() {
    if ($this->coupon_amount > 0) {
      $total = ((string) round($this->vat_amount, 2) + $this->shipping_price + $this->final_total) - $this->coupon_amount;
    } else {
      $total = ((string) round($this->vat_amount, 2) + $this->shipping_price + $this->final_total);
    }
    return (string) round($total, 2);
  }

  public function _priceWithOutShip() {
    if ($this->coupon_amount > 0) {
      $total = ((string) round($this->vat_amount, 2) + $this->final_total) - $this->coupon_amount;
    } else {
      $total = ((string) round($this->vat_amount, 2) + $this->final_total);
    }
    return (string) round($total, 2);
  }

    public static function userStatus($index = null) {
        $arr = [
            '0'         => trans('order.0'),
            '1'     => trans('order.1'),
            '2'    => trans('order.2'),
            '3'       => trans('order.3'),
            '4'   => trans('order.4'),
            '5' => trans('order.5'),
            '6'     => trans('order.6'),
            '7'     => trans('order.7'),
            '8'     => trans('order.8'),
        ];
        if (null != $index) {
            return $arr[$index];
        }
        return $arr;
    }

  public static function orderType($index = null) {
    $arr = [
      'deliver'   => 'توصيل',
      'branch'    => 'من الفرع',
      'postponed' => 'طلب مؤجل',
    ];
    if (null != $index) {
      return $arr[$index];
    }
    return $arr;
  }

  public function ratings() {
    return $this->hasMany(ReviewRate::class, 'order_id');
  }
//  public static function delegateStatus($index = null) {
//    $arr = [
//      'arrived'         => 'تم الوصول الي المطعم',
//      'received'        => 'تم الاستلام',
//      'arrivedToClient' => 'الوصول بالطلب',
//      'delivered'       => 'تم التسليم',
//    ];
//    if (null != $index) {
//      return $arr[$index];
//    }
//    return $arr;
//  }

  public static function paymentsMethod($index = null) {
    $arr = [
      'cash'   => 'دفع عند الاستلام',
      'bank'   => 'بنك',
      'online' => 'الكتروني',
    ];
    if (null != $index) {
      return $arr[$index];
    }
    return $arr;
  }

  public static function boot() {
    parent::boot();
    $lastId = self::all()->last()->id ?? 0;
    self::creating(function ($model) use ($lastId) {
      $model->order_num = \intval(date('Y') . $lastId) + 1;
    });
  }
}
