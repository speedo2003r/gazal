<?php

namespace App\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class SellerPay.
 *
 * @package namespace App\Entities;
 */
class SellerPay extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'acc_owner_name',
        'acc_number',
        'bank_name',
        'price',
        'image',
        'order_id',
        'user_id',
        'parent_id',
        'income_id',
        'pay_status',
        'status',
    ];

    public function sellerpays()
    {
        return $this->hasMany(SellerPay::class,'parent_id');
    }
    public function order()
    {
        return $this->belongsTo(Order::class)->withTrashed();
    }
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function getImageAttribute($value)
    {
        if($value == null){
            return  dashboard_url('images/placeholder.png');
        }
        return  dashboard_url('storage/images/banks/'. $value);
    }
}
