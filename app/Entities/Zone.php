<?php

namespace App\Entities;

use App\Models\User;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

class Zone extends Model implements Transformable {
  use TransformableTrait, HasTranslations, SpatialTrait;
  public $translatable = ['title'];
  protected $fillable  = [
    'title',
    'city_id',
    'geometry',
    'p_geometry',
  ];

  protected $spatialFields = [
    'p_geometry',
  ];

  public function City() {
    return $this->belongsTo(City::class);
  }

  public function users() {
    return $this->hasMany(User::class,'zone_id');
  }

  public function branches() {
    return $this->belongsToMany(Branch::class);
  }

}
