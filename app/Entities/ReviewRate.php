<?php

namespace App\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ReviewRate.
 *
 * @package namespace App\Entities;
 */
class ReviewRate extends Model implements Transformable
{
    use TransformableTrait;
    public $fillable = [
        'model_id',
        'model_type',
        'order_id',
        'rating_list_id',
        'rateable_id',
        'rateable_type',
        'rate',
    ];

    public function ratingList()
    {
        return $this->belongsTo(RatingList::class);
    }
    public function rateable()
    {
        return $this->morphTo();
    }
    public function modelable()
    {
        return $this->morphTo('model');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function user()
    {
        return $this->belongsTo($this->attributes['model_type'],'model_id');
    }
}
