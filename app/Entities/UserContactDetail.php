<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class UserContactDetail extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'model_id',
        'model_type',
        'type',
        'data',
    ];

    public function modelable()
    {
        return $this->morphTo();
    }

}
