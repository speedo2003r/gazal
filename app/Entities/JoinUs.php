<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class JoinUs extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    protected $with = ['category','country','city'];
    protected $fillable = [
        'name',
        'store_name',
        'category_id',
        'phone',
        'email',
        'country_id',
        'city_id',
        'status',
    ];
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }

}
