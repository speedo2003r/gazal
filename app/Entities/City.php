<?php

namespace App\Entities;

use App\Models\User;
use Database\Factories\CityFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class City.
 *
 * @package namespace App\Entities;
 */
class City extends Model implements Transformable {
  use HasFactory;
  use TransformableTrait;
  use SoftDeletes;
  use HasTranslations;
  public $translatable = ['title'];
  protected $fillable  = [
    'title',
    'country_id',
  ];

  protected static function newFactory() {
    return new CityFactory();
  }

  public function Country() {
    return $this->belongsTo(Country::class);
  }

  public function providers() {
    return $this->hasMany(User::class, 'city_id');
  }

  public function zones() {
    return $this->hasMany(Zone::class);
  }

}
