<?php

namespace App\Entities;

use App\Models\User;
use Database\Factories\GroupFactory;
use Database\Factories\ProviderFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class Bank.
 *
 * @package namespace App\Entities;
 */
class Provider extends Authenticatable implements Transformable, JWTSubject
{
    use HasFactory;
    use TransformableTrait;
    use SoftDeletes;
    use HasTranslations;

    public $translatable = ['store_name'];

    protected $appends = ['provider_id'];

    const PROVIDER = 1;
    const EMPLOYEE = 2;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'store_name',
        'avatar',
        'email',
        'username',
        'slogan',
        'wallet',
        'parent_id',
        'user_type',
        'commission_status',
        'income',
        'balance',
        'commission',
        'phone',
        'replace_phone',
        'v_code',
        'password',
        'category_id',
        'country_id',
        'city_id',
        'logo',
        'banner',
        'commercial_num',
        'free_ship',
        'processing_time',
        'id_avatar',
        'acc_bank',
        'commercial_image',
        'tax_certificate',
        'municipal_license',
        'end_date_contract',
        'lang',
        'active',
        'banned',
        'accepted',
        'notify',
        'online',
        'lat',
        'lng',
        'address',
        'representative_id',
        'account_manager_id',
        'active_responsible_id',
        'accountant_id',
    ];

    protected static function newFactory()
    {
        return new ProviderFactory();
    }

    public function getAvatarAttribute($value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL)) {
            return $value;
        }
        if ('/default.png' == $value || null == $value) {
            return dashboard_url('images/users/default.png');
        }

        return dashboard_url('storage/images/providers/'.$value);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function contactDetails()
    {
        return $this->morphMany(UserContactDetail::class, 'modelable', 'model_type', 'model_id');
    }

    public function delegates()
    {
        return $this->belongsToMany(Delegate::class,'providers_delegates','provider_id','delegate_id')->withPivot('hire_id');
    }
    public function items()
    {
        return $this->hasMany(Item::class, 'user_id', 'id');
    }

    public function sliders()
    {
        return $this->hasMany(Slider::class, 'user_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ScopeActive($query)
    {
        return $query->where('active', 1)->where('banned', 0)->where('accepted', 1);
    }

    public function blockUsers()
    {
        return $this->morphMany(BlockUser::class, 'modalable', 'modal_type', 'modal_id');
    }

    public function ratings()
    {
        return $this->morphMany(ReviewRate::class, 'rateable', 'rateable_type', 'rateable_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'provider_id', 'id');
    }

    public function branches()
    {
        return $this->hasMany(Branch::class, 'provider_id');
    }
    public function parent()
    {
        return $this->belongsTo(Provider::class,'parent_id');
    }
    public function employees()
    {
        return $this->hasMany(Provider::class,'parent_id', 'id');
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'tomodel', 'tomodel_type', 'tomodel_id')->orderByDesc('id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'provider_categories', 'user_id', 'subcategory_id')
                    ->withPivot('is_active', 'is_main')
                    ->wherePivot('is_main', 1);
    }

    public function subcategories()
    {
        return $this->belongsToMany(Category::class, 'provider_categories', 'user_id', 'subcategory_id')
                    ->withPivot('is_active', 'is_main')
                    ->wherePivot('is_main', 0);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'provider_groups', 'user_id', 'group_id');
    }

    public function getBannerAttribute($value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL)) {
            return $value;
        }
        if ($value == null) {
            return dashboard_url('images/placeholder.png');
        }

        return dashboard_url('storage/images/providers/'.$value);
    }

    public function getLogoAttribute($value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL)) {
            return $value;
        }
        if ($value == null) {
            return dashboard_url('images/placeholder.png');
        }

        return dashboard_url('storage/images/providers/'.$value);
    }

    public function getCommercialImageAttribute($value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL)) {
            return $value;
        }
        if ($value == null) {
            return dashboard_url('images/placeholder.png');
        }

        return dashboard_url('storage/images/providers/'.$value);
    }

    public function getTaxCertificateAttribute($value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL)) {
            return $value;
        }
        if ($value == null) {
            return dashboard_url('images/placeholder.png');
        }

        return dashboard_url('storage/images/providers/'.$value);
    }

    public function getIdAvatarAttribute($value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL)) {
            return $value;
        }
        if ($value == null) {
            return dashboard_url('images/placeholder.png');
        }

        return dashboard_url('storage/images/providers/'.$value);
    }

    public function getMunicipalLicenseAttribute($value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL)) {
            return $value;
        }
        if ($value == null) {
            return dashboard_url('images/placeholder.png');
        }

        return dashboard_url('storage/images/providers/'.$value);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function isProvider() : bool
    {
        return $this->user_type == 1 && $this->parent_id == null;
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class, 'parent_id');
    }

    public function getProviderIdAttribute()
    {
        if ($this->isProvider()) {
            return $this->id;
        }
        return $this->parent_id;
    }

    public function employee_branches()
    {
        return $this->belongsToMany(Branch::class, 'branch_employee', 'employee_id','branch_id');
    }

}
