<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class Hire extends Model implements Transformable
{
    use TransformableTrait;
    protected $table = 'hire';
    protected $fillable = [
        'provider_id',
        'car_type_id',
        'amount',
        'delegates_count',
        'from',
        'to',
    ];

    public function providersDelegates()
    {
        return $this->hasMany(ProvidersDelegate::class,'hire_id');
    }

    public function carType()
    {
        return $this->belongsTo(CarType::class,'car_type_id');
    }
    public function provider()
    {
        return $this->belongsTo(Provider::class,'provider_id');
    }
    public function delegates()
    {
        return $this->hasManyThrough(Delegate::class,ProvidersDelegate::class,'hire_id','id');
    }
}
