<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class TopList extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'provider_id',
        'sort',
        'from',
        'to',
    ];

}
