<?php

namespace App\Entities;

use App\Models\User;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class City.
 *
 * @package namespace App\Entities;
 */
class ItemTypeDetail extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use HasTranslations;
    protected $table = 'item_types_details';
    public $translatable = ['title'];
    protected $fillable = [
        'title',
        'item_type_id',
        'price',
    ];
    public function itemType()
    {
        return $this->belongsTo(ItemType::class,'item_type_id','id');
    }
    public function price()
    {
        $item = $this->itemType->item;
        $types = $item->types;
        $firstType = $types->first();
        $price = ($firstType['type_id'] == $this->itemType['type_id'] ? (string) round($this->price + $item->price(),2) : (string) round($this->price,2));
        return $price;
    }

}
