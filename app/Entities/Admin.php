<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class Admin extends Authenticatable implements Transformable
{
    use Notifiable;
    use TransformableTrait;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'avatar',
        'email',
        'phone',
        'password',
        'banned',
        'country_id',
        'city_id',
        'role_id',
        'address',
    ];

    public function notifications() {
        return $this->morphMany(Notification::class, 'tomodel','tomodel_type','tomodel_id')->orderByDesc('id');
    }

    public function role() {
        return $this->belongsTo(Role::class);
    }
    public function getAvatarAttribute($value) {
        if(filter_var($value, FILTER_VALIDATE_URL)){
            return $value;
        }
        if ('/default.png' == $value || null == $value) {
            return dashboard_url('images/users/default.png');
        }
        return dashboard_url('storage/images/admins/' . $value);
    }
    public function setPasswordAttribute($value) {
        $this->attributes['password'] = Hash::make($value);
    }
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
