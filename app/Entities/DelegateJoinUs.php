<?php

namespace App\Entities;

use App\Traits\UploadTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class DelegateJoinUs extends Model implements Transformable {
  use TransformableTrait, SoftDeletes, UploadTrait;

  protected $fillable = [
    'avatar',
    'name',
    'phone',
    'phone2',
    'email',
    'car_type_id',
    'birth_date',
    'city_id',
    'category_id',
    'password',
    'licence_car_image',
    'licence_image',
    'car_back_image',
    'status',
  ];

  public function getAvatarAttribute() {
    if ($this->attributes['avatar']) {
      $file = $this->getFile($this->attributes['avatar'], 'delegateJoins');
    } else {
      $file = '';
    }
    return $file;
  }
  public function setAvatarAttribute($value) {
    if (!empty($value)) {
      $this->attributes['avatar'] = $this->uploadFileExt($value, 'delegateJoins');
    }
  }

  public function getLicenceCarImageAttribute() {
    if ($this->attributes['licence_car_image']) {
      $file = $this->getFile($this->attributes['licence_car_image'], 'delegateJoins');
    } else {
      $file = '';
    }
    return $file;
  }
  public function setLicenceCarImageAttribute($value) {
    if (!empty($value)) {
      $this->attributes['licence_car_image'] = $this->uploadFileExt($value, 'delegateJoins');
    }
  }

  public function getLicenceImageAttribute() {
    if ($this->attributes['licence_image']) {
      $file = $this->getFile($this->attributes['licence_image'], 'delegateJoins');
    } else {
      $file = '';
    }
    return $file;
  }
  public function setLicenceImageAttribute($value) {
    if (!empty($value)) {
      $this->attributes['licence_image'] = $this->uploadFileExt($value, 'delegateJoins');
    }
  }

  public function getCarBackImageAttribute() {
    if ($this->attributes['car_back_image']) {
      $file = $this->getFile($this->attributes['car_back_image'], 'delegateJoins');
    } else {
      $file = '';
    }
    return $file;
  }
  public function setCarBackImageAttribute($value) {
    if (!empty($value)) {
      $this->attributes['car_back_image'] = $this->uploadFileExt($value, 'delegateJoins');
    }
  }

  public function city() {
    return $this->belongsTo(City::class, 'city_id');
  }

  public function CarType() {
    return $this->belongsTo(CarType::class, 'car_type_id');
  }

  public function category() {
    return $this->belongsTo(Category::class);
  }
}
