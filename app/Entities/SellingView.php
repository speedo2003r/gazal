<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class SellingView extends Model implements Transformable
{
    use TransformableTrait;
    protected $table = 'selling_view';
    protected $fillable = [
        'item_id',
        'order_id',
        'order_product_id',
        'price',
        'count',
    ];

}
