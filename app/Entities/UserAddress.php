<?php

namespace App\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class UserAddress.
 *
 * @package namespace App\Entities;
 */
class UserAddress extends Model implements Transformable
{
    use TransformableTrait;
    protected $table    = 'user_addresses';
    protected $fillable = [
        'user_id',
        'country_id',
        'city_id',
        'uuid',
        'name',
        'street',
        'building',
        'floor',
        'flat',
        'unique_sign',
        'lat',
        'lng',
        'map_desc',
        'phone',
        'type',
        'is_main',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function City()
    {
        return $this->belongsTo(City::class);
    }

    public function mapDesc()
    {
        $address = $this->name.','.trans('api.street').' '.$this->street.','.trans('api.building').' '.$this->building.','.trans('api.floor').' '.$this->floor.','.trans('api.flat').' '.$this->flat.','.$this->unique_sign;
        return $address;
    }
}
