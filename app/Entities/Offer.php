<?php

namespace App\Entities;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Offer.
 *
 * @package namespace App\Entities;
 */
class Offer extends Model implements Transformable
{
    use TransformableTrait;
    use HasTranslations;
    public $translatable = ['title','desc'];
    protected $fillable = [
        'title',
        'desc',
        'discount',
        'image',
        'country_id',
        'city_id',
        'category_id',
        'user_id',
        'start_date',
        'end_date',
        'item_id',
        'active',
        'type',
        'page',
    ];
    public function getImageAttribute()
    {
        return dashboard_url('storage/images/offers/'.$this->attributes['image']);
    }

    public static function rsine($coordinates)
    {
        return '(6371 * acos(cos(radians(' . $coordinates['latitude'] . '))
        * cos(radians(`lat`))
        * cos(radians(`lng`)
        - radians(' . $coordinates['longitude'] . '))
        + sin(radians(' . $coordinates['latitude'] . '))
        * sin(radians(`lat`))))';
    }
    public function scopeDistance($query, $lat, $lng, $city_id = null, $unit = "km")
    {
        $unit = ($unit === "km") ? 6378.10 : 3963.17;
        $lat = (float) $lat;
        $lng = (float) $lng;
        $sql =  "($unit * ACOS(COS(RADIANS($lat))
                * COS(RADIANS(branches.lat))
                * COS(RADIANS($lng) - RADIANS(branches.lng))
                + SIN(RADIANS($lat))
                * SIN(RADIANS(branches.lat))))";
        if($city_id != null){
            return Offer::query()->where('offers.city_id',$city_id)
                ->with('user')
                ->select(DB::raw("*,branches.id as branch_id, $sql AS distance"))
                ->leftjoin('users','users.id','=','offers.user_id')
                ->leftjoin('branches','branches.provider_id','=','users.id')
                ->orderBy('distance','asc');
        }else{
            return Offer::query()
                ->with('user')
                ->select(DB::raw("*,branches.id as branch_id, $sql AS distance"))
                ->leftjoin('users','users.id','=','offers.user_id')
                ->leftjoin('branches','branches.provider_id','=','users.id')
                ->orderBy('distance','asc');
        }

    }
    public function user()
    {
        return $this->belongsTo(Provider::class,'user_id');
    }
    public function ScopeSpecialActive($query)
    {
        return $query->where('offers.active',1)->whereDate('offers.start_date','<=',Carbon::now()->format('Y-m-d'))->whereDate('offers.end_date','>=',Carbon::now()->format('Y-m-d'))->where('offers.page','special')->where('offers.category_id',request()->get('category_id'));
    }
    public function ScopeOfferActive($query)
    {
        return $query->where('offers.active',1)->whereDate('offers.start_date','<=',Carbon::now()->format('Y-m-d'))->whereDate('offers.end_date','>=',Carbon::now()->format('Y-m-d'))->where('offers.page','offer')->where('offers.city_id',request()->get('city_id'));
    }
}
