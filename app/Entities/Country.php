<?php

namespace App\Entities;

use Database\Factories\CountryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class Country.
 *
 * @package namespace App\Entities;
 */
class Country extends Model implements Transformable
{
    use HasFactory;
    use TransformableTrait;
    use SoftDeletes;
    use HasTranslations;

    public $translatable = ['title'];
    protected $fillable = [
        'title',
        'code',
    ];
    protected $with = ['Cities'];

    protected static function newFactory()
    {
        return new CountryFactory();
    }

    public function Cities()
    {
        return $this->hasMany(City::class,'country_id');
    }

}
