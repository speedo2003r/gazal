<?php

namespace App\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Order.
 *
 * @package namespace App\Entities;
 */
class OrderProductType extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    protected $fillable = [
        'order_product_id',
        'item_types_detail_id',
        'price',
    ];

    public function ItemTypeDetail()
    {
        return $this->belongsTo(ItemTypeDetail::class,'item_types_detail_id')->withTrashed();
    }
}
