<?php

namespace App\Entities;

use App\Models\User;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

/**
 * Class City.
 *
 * @package namespace App\Entities;
 */
class ItemType extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use CascadeSoftDeletes;
    protected $cascadeDeletes = ['children'];
    protected $table = 'item_types';
    protected $fillable = [
        'type_id',
        'item_id',
    ];
    public function item()
    {
        return $this->belongsTo(Item::class);
    }
    public function type()
    {
        return $this->belongsTo(Type::class);
    }
    public function children()
    {
        return $this->hasMany(ItemTypeDetail::class);
    }

}
