<?php

namespace App\Entities;

use App\Entities\BranchDate;
use App\Entities\Favourite;
use App\Models\User;
use Database\Factories\BranchFactory;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class Branch.
 *
 * @package namespace App\Entities;
 */
class Branch extends Authenticatable implements JWTSubject
{
    use HasFactory;
    use TransformableTrait;
    use SoftDeletes, CascadeSoftDeletes;
    use HasTranslations;

    const NORMAL = 1;
    const RAMADAN = 2;
    protected $cascadeDeletes = ['branchDates'];

    public $translatable = ['title'];

    protected $casts = ['created_at' => 'datetime:Y-m-d h:i a', 'from' => 'date', 'to' => 'date'];

    protected $appends = ['current_status', 'all_dates','current_city'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'avatar',
        'phone',
        'email',
        'password',
        'v_code',
        'title',
        'address',
        'lat',
        'lng',
        'lang',
        'discount',
        'from',
        'to',
        'provider_id',
        'country_id',
        'city_id',
        'status',
        'appear',
        'banned',
        'notify',
        'online',
        'schedule_switch',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    protected static function newFactory()
    {
        return new BranchFactory();
    }


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAvatarAttribute($value)
    {
        if ('/default.png' == $value || null == $value) {
            return dashboard_url('images/users/default.png');
        }

        return dashboard_url('storage/images/branches/'.$value);
    }

    public function blockUsers() {
        return $this->morphMany(BlockUser::class, 'modalable','modal_type','modal_id');
    }
    public function getIsFavAttribute(): bool
    {
        $isFav = false;

        if (auth()->check()) {
            $isFav = Favourite::where('user_id', auth()->id())
                              ->where('favoritable_id', $this['id'])
                              ->where('favoritable_type', Branch::class)
                              ->exists();
        } elseif (getBrowserUuid()) {
            $isFav = Favourite::where('uuid', getBrowserUuid())
                              ->where('user_id', null)
                              ->where('favoritable_id', $this['id'])
                              ->where('favoritable_type', Branch::class)
                              ->exists();
        }

        return $isFav;
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'tomodel', 'tomodel_type', 'tomodel_id')->orderByDesc('id');
    }

    public static function rsine($coordinates)
    {
        return '(6371 * acos(cos(radians('.$coordinates['latitude'].'))
        * cos(radians(`lat`))
        * cos(radians(`lng`)
        - radians('.$coordinates['longitude'].'))
        + sin(radians('.$coordinates['latitude'].'))
        * sin(radians(`lat`))))';
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'branch_id', 'id');
    }

    public function scopeDistance($query, $lat, $lng, $city_id = null, $unit = "km")
    {
        $unit = ("km" === $unit) ? 6378.10 : 3963.17;
        $lat = (float)$lat;
        $lng = (float)$lng;
        $sql = "($unit * ACOS(COS(RADIANS($lat))
                * COS(RADIANS(branches.lat))
                * COS(RADIANS($lng) - RADIANS(branches.lng))
                + SIN(RADIANS($lat))
                * SIN(RADIANS(branches.lat))))";
        if (null != $city_id) {
            return $query->where('branches.city_id', $city_id)
                         ->with('user')
                         ->select(DB::raw("*,branches.id as branch_id, $sql AS distance"))
                         ->leftjoin('users', 'users.id', '=', 'branches.provider_id')
                         ->orderBy('distance', 'asc');
        } else {
            return $query->with('user')
                         ->select(DB::raw("*,branches.id as branch_id, $sql AS distance"))
                         ->leftjoin('users', 'users.id', '=', 'branches.provider_id')
                         ->orderBy('distance', 'asc');
        }

        return $query->when($city_id, function ($q) use ($city_id) {
                return $q->where('branches.city_id', $city_id);
            })
                     ->when($lat && $lng, function ($q) use ($lat, $lng) {
                         return $q->whereHas('zone', function ($zone) use ($lat, $lng) {
                             return $zone->whereRaw("ST_Contains(zones.p_geometry, ST_GeomFromText(?))",
                                 ["POINT($lng $lat)"]);
                         });
                     })
                     ->with('user')
                     ->select(DB::raw("*,branches.id as branch_id, $sql AS distance"))
                     ->leftjoin('users', 'users.id', '=', 'branches.provider_id')
                     ->orderBy('distance', 'asc');
    }

    public function reviews()
    {
        return $this->morphMany(ReviewRate::class, 'rateable', 'rateable_type', 'rateable_id');
    }

    public function scopeActive($value)
    {
//    if (request()->has('city_id')) {
//      return $value->where('status', 1)->where('appear', 1)->where('city_id', request()['city_id']);
//    }
        return $value->where('status', 1)->where('appear', 1);
    }

    public function user()
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function zones()
    {
        return $this->belongsToMany(Zone::class);
    }

    public function branchDates()
    {
        return $this->hasMany(BranchDate::class, 'branch_id');
    }

    public function items()
    {
        return $this->belongsToMany(Item::class, 'item_branches', 'branch_id', 'item_id');
    }

    public function devices()
    {
        return $this->morphMany(Device::class, 'modelable', 'model_type', 'model_id');
    }

    public function getCurrentStatusAttribute()
    {
        if ($this->from && $this->to && now() < $this->to && now() > $this->from) {
            return __('Permanently closed');
        }

        if ($this->branchDates()->exists() && $this->branchDates()
                                                   ->where('day', dayIndex(date('l')))
                                                   ->whereTime('timefrom', '<', date('H:i'))
                                                   ->whereTime('timeto', '>', date('H:i'))
                                                   ->exists()) {
            return __('Open');
        }

        return __('Closed');
    }

    public function getAllDatesAttribute()
    {
        return $this->branchDates;
    }

    public function scopeWorks($q, $status)
    {
        if ($status == 'open') {
            return $q->whereHas('branchDates', function ($q) {
                $q->where('day', dayIndex(date('l')))
                  ->whereTime('timefrom', '<', date('H:i'))
                  ->whereTime('timeto', '>', date('H:i'));
            });
        } else {
            return $q->whereDoesntHave('branchDates', function ($q) {
                $q->where('day', dayIndex(date('l')))
                  ->whereTime('timefrom', '<', date('H:i'))
                  ->whereTime('timeto', '>', date('H:i'));
            });
        }
    }
    public function getCurrentCityAttribute()
    {
        if ($this->city) {
            return $this->city['title'];
        }
    }

}
