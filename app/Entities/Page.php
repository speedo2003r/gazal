<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Spatie\Translatable\HasTranslations;

class Page extends Model implements Transformable {
  use TransformableTrait;
  use HasTranslations;
  use SoftDeletes;

  public $translatable = ['title', 'desc'];

  protected $fillable = [
    'title',
    'desc',
  ];

  const ABOUTID   = 1;
  const PRIVACYID = 2;
  const USAGEID   = 3;
  const CONTACTID = 4;
}
