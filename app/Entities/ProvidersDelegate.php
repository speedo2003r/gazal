<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Slider.
 *
 * @package namespace App\Entities;
 */
class ProvidersDelegate extends Model implements Transformable
{
    use TransformableTrait;
    protected $fillable = [
        'provider_id',
        'delegate_id',
        'hire_id',
    ];

}
