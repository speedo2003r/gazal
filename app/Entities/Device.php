<?php

namespace App\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Device extends Model implements Transformable {
  use TransformableTrait;

  protected $fillable = [
    'uuid',
    'device_id',
    'device_type',
    'model_id',
    'model_type',
  ];

  public function user() {
    return $this->belongsTo($this->attributes['model_type'],'model_id');
  }

  public function modelable()
  {
      return $this->morphTo();
  }

}
