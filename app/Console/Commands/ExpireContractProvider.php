<?php

namespace App\Console\Commands;

use App\Entities\Admin;
use App\Entities\Notification;
use App\Entities\Provider;
use App\Traits\NotifyTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExpireContractProvider extends Command
{
    use NotifyTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire:contract';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check if contract will end after one month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $providers = Provider::whereDate('end_date_contract','=',Carbon::now()->addDays(30)->format('Y-m-d'))->get();
        $admins = Admin::all();
        foreach ($admins as $admin){
            foreach ($providers as $provider){
                $message_ar = 'سوف ينتهي اشتراك مقدم خدمه : '.$provider['name'].' رقم حسابه '.$provider['id'] .' بعد 30 يوم من الأن';
                $message_en = 'The service provider\'s subscription :'.$provider['name'].' will expire account number is '.$provider['id'].' after 30 days from now';
                $notification = new Notification();
                $notification->tomodel_id        = $admin['id'];
                $notification->tomodel_type        = get_class($admin);
                $notification->frommodel_id        = $admin['id'];
                $notification->frommodel_type        = get_class($admin);
                $notification->message_ar   = $message_ar;
                $notification->message_en   = $message_en;
                $notification->seen         = 0;
                $notification->save();
            }
        }
    }
}
