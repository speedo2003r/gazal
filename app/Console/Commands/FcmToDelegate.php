<?php

namespace App\Console\Commands;

use App\Entities\Notification;
use App\Entities\Provider;
use App\Traits\NotifyTrait;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class FcmToDelegate extends Command
{
    use NotifyTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delegate:notify {delegate} {order}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send fcm to delegate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $content_ar = 'هناك طلب جديد رقم '.$this->argument('order')['id'].' مناسب لك';
        $content_en = 'A new order number '.$this->argument('order')['id'].' is right for you';
        $notification = new Notification();
        $notification->tomodel_id        = $this->argument('delegate')['id'];
        $notification->tomodel_type        = get_class($this->argument('delegate'));
        $notification->frommodel_id        = $this->argument('order')['provider_id'];
        $notification->frommodel_type        = Provider::class;
        $notification->message_ar   = $content_ar;
        $notification->message_en   = $content_en;
        $notification->type         = is_null($this->argument('order')) ? 'notify' : 'order';
        $notification->order_id     = $this->argument('order')['id'];
        $notification->order_status = $this->argument('order')['order_status'];
        $notification->seen         = 0;
        $notification->save();
        $data['title'] = app()->getLocale() == 'ar' ? 'طلب جديد': 'new order';
        $data['body'] = app()->getLocale() == 'ar' ? $content_ar: $content_en;
        if($this->argument('delegate')->devices){
            foreach ($this->argument('delegate')->devices as $device) {
                if($device->device_id){
                    $this->send_fcm($device->device_id, $data, $device->device_type);
                }
            }
        }
//        Log::debug('dispatched complete '.$notification['id']);
    }
}
