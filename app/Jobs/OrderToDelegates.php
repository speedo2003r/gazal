<?php

namespace App\Jobs;

use App\Entities\Delegate;
use App\Entities\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class OrderToDelegates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $order;
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $branch = $this->order->branch;
        $delegates = Delegate::query()->active()->selectRaw("delegates.*,
            ( 6371 * acos( cos( radians(" . $branch['lat'] . ") ) *
            cos( radians(delegates.lat) ) *
            cos( radians(delegates.lng) - radians(" . $branch['lng'] . ") ) +
            sin( radians(" . $branch['lat'] . ") ) *
            sin( radians(delegates.lat) ) ) )
         AS distance")
            ->having("distance", "<", 1700)
            ->where(function ($query){
                $query->whereHas('orders',function ($order){
                    $order->whereNotIn('orders.order_status',[
                        Order::STATUS_PREPARED,
                        Order::STATUS_GIVE_TO_DELEGATE,
                        Order::STATUS_ON_WAY,
                        Order::STATUS_ARRIVED_TO_CLIENT,
                    ]);
                });
                $query->orDoesntHave('orders');
            })
            ->groupBy('delegates.id')
            ->orderBy("distance")->get();
            foreach($delegates as $delegate){
                if ($this->order['delegate_id'] != null) {
                    // will leave the foreach loop and also the if statement
                    break;
                }
//                Log::debug('dispatched complete '.$delegate['id']);
                Artisan::call('delegate:notify', [
                    'delegate' => $delegate, 'order' => $this->order
                ]);
                sleep(10);
            }
    }

}
