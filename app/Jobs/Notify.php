<?php

namespace App\Jobs;

use App\Entities\Notification;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Notify implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $from;
    private $to;
    private $message_ar;
    private $message_en;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($from,$to,$message_ar,$message_en)
    {
        $this->from = $from;
        $this->to = $to;
        $this->message_ar = $message_ar;
        $this->message_en = $message_en;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notification = new Notification();
        $notification->tomodel_id        = $this->to['id'];
        $notification->tomodel_type        = get_class($this->to);
        $notification->frommodel_id        = $this->from['id'];
        $notification->frommodel_type        = get_class($this->from);
        $notification->message_ar   = $this->message_ar;
        $notification->message_en   = $this->message_en;
        $notification->type         = 'notify';
        $notification->seen         = 0;
        $notification->save();

    }
}
