'use strict';

const dotenv = require('dotenv')
const envData = dotenv.config({ path: './../.env' }).parsed;

const express = require('express');
const http = require('https');
const socketio = require('socket.io');
const socketEvents = require('./utils/socket');

const path = require('path');
const fs = require('fs');
const utf8 = require('utf8');

class Server {
    constructor() {
        // this.host = `abaar.aait-sa.com`;
        this.host = envData.NODE_HOST;
        this.port = envData.NODE_PORT;
        // console.log('port: ', this.port)
        // console.log('host: ', this.host)

        this.app = express();

        // Certificate
        const privateKey = fs.readFileSync(`/etc/letsencrypt/live/${this.host}/privkey.pem`, 'utf8');
        const certificate = fs.readFileSync(`/etc/letsencrypt/live/${this.host}/cert.pem`, 'utf8');
        const ca = fs.readFileSync(`/etc/letsencrypt/live/${this.host}/chain.pem`, 'utf8');
        const credentials = {
            key: privateKey,
            cert: certificate,
            ca: ca
        };

        this.http = http.createServer(credentials, this.app);
        // this.http = http.createServer(this.app);
        this.socket = socketio(this.http);
    }

    appRun() {

        this.app.use((req, res, next) => {
            res.header("Access-Control-Allow-Origin", `${this.host}`);
            res.header("Access-Control-Allow-Credentials", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });

        // this.app.use(express.static(__dirname + '/uploads'));

        new socketEvents(this.socket).socketConfig();

        this.http.listen(this.port, this.host, () => {
            console.log(`Listening on https://${this.host}:${this.port}`);
        });

    }
}

const app = new Server();
app.appRun();