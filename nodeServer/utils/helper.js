'user strict';

const DB = require('./db');
const path = require('path');
const fs = require('fs');

class Helper {

    constructor(app) {
        this.db = DB;
    }

    async addSocketId(userId, userSocketId) {
        try {
            // return await this.db.query(`UPDATE users SET socket_id = ?, online= ? WHERE id = ?`, [userSocketId, '1', userId]);
        } catch (error) {
            console.log(error);
            return null;
        }
    }

    async logoutUser(userSocketId) {
        try {
            // return await this.db.query(`UPDATE users SET socket_id = ?, online= ? WHERE id = ?`, ['', '0', userSocketId]);
        } catch (error) {
            console.warn(error);
            return null;
        }

    }

    getOrderData(orderId) {
        try {
            return Promise.all([
                this.db.query(`SELECT id, user_id, order_status, updated_at FROM orders WHERE id = ?`, [orderId])
            ]).then((response) => {
                return {
                    orderData: response[0]
                };
            }).catch((error) => {
                console.warn(error);
                return (null);
            });
        } catch (error) {
            console.warn(error);
            return null;
        }
    }
}
module.exports = new Helper();