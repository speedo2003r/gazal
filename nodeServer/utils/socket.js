'use strict';

const moment = require('moment');
const path = require('path');
const fs = require('fs');
const helper = require('./helper');

var Redis = require('ioredis');

class Socket {

  constructor(socket) {
    this.io = socket;
  }

  socketEvents() {
    this.io.on('connection', (socket) => {
      const ConnectedUserID = Number(socket.request._query['id']);

      socket.on('update-driver-location', (req) => {
        console.log(`user : ${ConnectedUserID} send update location to user : ${req.toUserId}`);
        socket.to(req.toUserId).emit('update-driver-location-res', req);
      });

      socket.on('get-order-data', async (req) => {
        console.log(`user : ${ConnectedUserID} ask for order ${req.orderId}`);

        // get order data  and send emit res to order user for update order
        const result = await helper.getOrderData(req.orderId);
        let userId = result.orderData[0].user_id;
        console.log('order data: ' , result.orderData[0]);
        console.log('order user id: ' , userId);

        socket.to(userId).emit('order-data-res', {
          'orderData': result.orderData[0]
        });

        // for test only resend to same connector
        socket.emit('order-data-res', {
          'orderData': result.orderData[0]
        });

      });




      var redis = new Redis();

      redis.subscribe('channel-order-data', function () {
        console.log('subscribed to channel-order-data');
      });

      redis.on('message', async function (channel, message) {
        console.log('redis on channel: ', channel);
        // console.log('on message: ', message);

        if ('channel-order-data' == channel) {
          message = JSON.parse(message);
          let data = message.data
          let orderId = data.order_id;
          let event = message.event;

          // get order data  and send emit res to order user for update order
          const result = await helper.getOrderData(orderId);
          let userId = result.orderData[0].user_id;
          console.log('order data: ' , result);
          console.log('order user id: ' , userId);

          socket.to(userId).emit('order-data-res', {
            'orderData': result.orderData[0]
          });

          // for test only resend to same connector
          socket.emit('order-data-res', {
            'orderData': result.orderData[0]
          });
        }
      });




      socket.on('disconnect', async () => {
        console.log('user_id ', ConnectedUserID, ' disConnected');
      });
    });
  }

  getOrderDataRes(){

  }

  // start user connecting then call socketEvents
  socketConfig() {
    this.io.use(async (socket, next) => {
      let userId = socket.request._query['id'];
      socket.id = parseInt(userId);
      // let userSocketId = socket.id;

      // make user online and save socket id in DB
      // const response = await helper.addSocketId(userId, userSocketId);
      // if (userId != 0 && response && response !== null) {
      if (userId != 0) {
        console.log('user_id ', userId, ' connected');
        next();
      } else {
        console.error(`
                                            Socket connection failed,
                                            for user Id $ { userId }.
                                            `);
      }
    });
    this.socketEvents();
  }
}

module.exports = Socket;