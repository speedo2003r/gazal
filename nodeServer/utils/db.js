'use strict';

const dotenv = require('dotenv')
const envData = dotenv.config({ path: './../.env' }).parsed;

const mysql = require('mysql');
//const config = require('config');

class Db {
    constructor() {
        this.connection = mysql.createPool({
            connectionLimit: 100,
            host: envData.DB_HOST,
            user: envData.DB_USERNAME,
            password: envData.DB_PASSWORD,
            database: envData.DB_DATABASE,
            debug: false
        });
    }
    query(sql, args) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, args, (err, rows) => {
                if (err)
                    return reject(err);
                resolve(rows);
            });
        });
    }
    close() {
        return new Promise((resolve, reject) => {
            this.connection.end(err => {
                if (err)
                    return reject(err);
                resolve();
            });
        });
    }
}
module.exports = new Db();